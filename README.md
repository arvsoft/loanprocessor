# LoanProcessor360

a loan origination software

## Сборка

```
mvn clean package -P <DatabaseVersion> -P <Language>
```

Для успешной сборки необходимо указать профиль с выбранной базой данных и с языковым пакетом по-умолчанию.

Например:
 
```
mvn clean package -P H2 -P Z-RUSSIAN
```

## Запуск

```
java -jar serverparent/core/target/<CurrentVersion>.jar
```

При запуске необходимо указать текущую собранную версию модуля **Core**

Например:

```
java -jar serverparent/core/target/core-1.1.2.jar  
```

## Docker

Сборка контейнера. С обязательной точкой в конце. *version* - текущая версия контейнера.
```
docker build -t lp360container:<version> .
```

Запуск контейнера. Первый ключ *-v* указывает путь к директории, в которой будут размещены ресурсы и база данных. Второй
ключ указывает путь к директории с шрифтами ОС.
```
docker run -d -p 9000:8080 -v /opt/container:/opt -v /usr/share/fonts:/usr/share/fonts --name containername lp360container:<version>
```

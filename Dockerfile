FROM java:8
WORKDIR /
VOLUME ["/opt", "/usr/share/fonts"]
ADD serverparent/core/target/core-1.1.2.jar app.jar
EXPOSE 8080
RUN fc-cache -f -v
ENTRYPOINT ["java","-jar","/app.jar", \
            "--spring.datasource.url=jdbc:h2:file:./opt/database", \
            "--system.directory.avatars=/opt/avatars", \
            "--system.directory.printed=/opt/printed", \
            "--system.directory.scanned=/opt/scanned", \
            "--system.directory.template=/opt/templates"]
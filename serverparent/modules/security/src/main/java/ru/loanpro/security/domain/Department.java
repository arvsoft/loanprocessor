package ru.loanpro.security.domain;

import org.hibernate.Hibernate;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.annotation.EntityName;
import ru.loanpro.security.domain.converter.CurrenciesConverter;
import ru.loanpro.sequence.domain.Sequence;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Currency;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Подразделение системы.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.department")
@Table(name = "departments", schema = DatabaseSchema.SECURITY)
public class Department extends BaseSettingsEntity {

    /**
     * Название подразделения.
     */
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Описание подразделения.
     */
    @Column(name = "description")
    private String description;

    /**
     * Имя иконки подразделения.
     */
    @Column(name = "icon")
    private String icon;

    /**
     * Ссылка на предка данного подразделения. <code>null</code>, если отдел
     * является корневым.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_id")
    private Department parent;

    /**
     * Коллекция потомков текущего отдела.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "parent")
    private Set<Department> childs = new HashSet<>();

    /**
     * Ссылка на последовательность номеров для генерации номера займа в текущем
     * отделе.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "loan_sequence_id", nullable = false)
    private Sequence loanSequence;

    /**
     * Ссылка на последовательность номеров для генерации номера заявки в текущем
     * отделе.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "request_sequence_id", nullable = false)
    private Sequence requestSequence;

    /**
     * Ссылка на последовательность номеров для счетов клиентам
     * отделе.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_sequence_id", nullable = false)
    private Sequence accountSequence;

    /**
     * Ссылка на последовательность номеров для пролонгаций в отделе.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "prolong_sequence_id", nullable = false)
    private Sequence prolongSequence;

    /**
     * Ссылка на последовательность номеров для операций в отделе.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "operation_sequence_id", nullable = false)
    private Sequence operationSequence;

    /**
     * Список валют, с которыми работает текущий отдел.
     */
    @Column(name = "currencies")
    @Convert(converter = CurrenciesConverter.class)
    private Set<Currency> currencies = new HashSet<>();

    /**
     * Адрес отдела.
     */
    @Column(name = "address")
    private String address;

    /**
     * Руководитель отдела.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "director_user_id")
    private User director;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Department getParent() {
        return parent;
    }

    public void setParent(Department parent) {
        this.parent = parent;
    }

    public Set<Department> getChilds() {
        return childs;
    }

    public void setChilds(Set<Department> childs) {
        this.childs = childs;
    }

    public Stream<Department> flattened() {
        return Stream.concat(
                Stream.of(this),
                childs.stream().flatMap(Department::flattened));
    }

    public Sequence getRequestSequence() {
        return requestSequence;
    }

    public void setRequestSequence(Sequence requestSequence) {
        this.requestSequence = requestSequence;
    }

    public Sequence getLoanSequence() {
        return loanSequence;
    }

    public void setLoanSequence(Sequence loanSequence) {
        this.loanSequence = loanSequence;
    }

    public Sequence getAccountSequence() {
        return accountSequence;
    }

    public void setAccountSequence(Sequence accountSequence) {
        this.accountSequence = accountSequence;
    }

    public Sequence getProlongSequence() {
        return prolongSequence;
    }

    public void setProlongSequence(Sequence prolongSequence) {
        this.prolongSequence = prolongSequence;
    }

    public Sequence getOperationSequence() {
        return operationSequence;
    }

    public void setOperationSequence(Sequence operationSequence) {
        this.operationSequence = operationSequence;
    }

    public Set<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(Set<Currency> currencies) {
        this.currencies = currencies;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getDirector() {
        return director;
    }

    public void setDirector(User director) {
        this.director = director;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Department that = (Department) o;
        boolean equal = Objects.equals(name, that.name) &&
            Objects.equals(description, that.description) &&
            Objects.equals(icon, that.icon) &&
            Objects.equals(loanSequence, that.loanSequence) &&
            Objects.equals(requestSequence, that.requestSequence) &&
            Objects.equals(accountSequence, that.accountSequence) &&
            Objects.equals(prolongSequence, that.prolongSequence) &&
            Objects.equals(operationSequence, that.operationSequence) &&
            Objects.equals(currencies, that.currencies) &&
            Objects.equals(address, that.address);

        if (Hibernate.isInitialized(parent) && parent != null)
            equal = equal && Objects.equals(parent.getId(), that.parent.getId());

        if (Hibernate.isInitialized(childs))
            equal = equal && Objects.equals(childs, that.childs);

        if (Hibernate.isInitialized(director) && director != null)
            equal = equal && Objects.equals(director.getId(), that.director.getId());

        return equal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            super.hashCode(),
            name,
            description,
            icon,
            Hibernate.isInitialized(parent) && parent != null
                ? parent.getId()
                : null,
            Hibernate.isInitialized(childs)
                ? childs
                : null,
            loanSequence,
            requestSequence,
            accountSequence,
            prolongSequence,
            operationSequence,
            currencies,
            address,
            Hibernate.isInitialized(director) && director != null
                ? director.getId()
                : null);
    }
}

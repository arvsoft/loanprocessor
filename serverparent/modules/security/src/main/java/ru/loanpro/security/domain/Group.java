package ru.loanpro.security.domain;

import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.annotation.EntityName;
import ru.loanpro.global.domain.AbstractIdEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Группа ролей.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.group")
@Table(name = "groups", schema = DatabaseSchema.SECURITY)
public class Group extends AbstractIdEntity {

    /**
     * Название группы ролей.
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * Описание группы ролей.
     */
    @Column(name = "description")
    private String description;

    /**
     * Коллекция ролей содержащихся в группе.
     */
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinTable(
        name = "role_group",
        schema = DatabaseSchema.SECURITY,
        joinColumns = {@JoinColumn(name = "group_id")},
        inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles = new ArrayList<>();

    public Group() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(name, group.name) &&
            Objects.equals(description, group.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }
}

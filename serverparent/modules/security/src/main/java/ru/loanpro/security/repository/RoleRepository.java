package ru.loanpro.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.loanpro.security.domain.Role;

import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Set<Role> findAllByAuthority(String authority);
}

package ru.loanpro.security.settings.domain;

import ru.loanpro.security.domain.User;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Сущность персональных настроек пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("USER")
public class UserSettings extends SystemSettings {

    /**
     * Ссылка на пользователя. {@link User}.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public UserSettings() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

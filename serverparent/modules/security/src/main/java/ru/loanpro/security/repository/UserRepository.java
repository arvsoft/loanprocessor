package ru.loanpro.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ru.loanpro.security.domain.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Репозиторий сущности {@link User}
 *
 * @author Oleg Brizhevatikh
 */
public interface UserRepository extends JpaRepository<User, UUID> {
    /**
     * Возвращает пользователя по его имени.
     *
     * @param username имя пользователя.
     * @return пользователь.
     */
    Optional<User> findByUsername(String username);

    /**
     * Возвращает количество пользователей с переданным именем.
     *
     * @param username имя пользователей.
     * @return количество пользователей.
     */
    long countByUsername(String username);

    /**
     * Возвращет количество активных пользователей.
     *
     * @return количество активных пользователей.
     */
    @Query("select count(u) from User u where u.enabled = true")
    Integer getActiveUserCount();

    /**
     * Возвращает список активных пользователей.
     *
     * @return список активных пользователей.
     */
    @Query("select u from User u where u.enabled = true order by u.username")
    List<User> findAllActiveUsers();
}

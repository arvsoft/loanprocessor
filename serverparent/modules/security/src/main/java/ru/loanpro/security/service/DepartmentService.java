package ru.loanpro.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.global.service.AbstractIdEntityService;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.QDepartment;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.event.CreateDepartmentEvent;
import ru.loanpro.security.event.UpdateDepartmentEvent;
import ru.loanpro.security.event.UpdateDepartmentLoanSequenceEvent;
import ru.loanpro.security.event.UpdateDepartmentOperationSequenceEvent;
import ru.loanpro.security.event.UpdateDepartmentProlongSequenceEvent;
import ru.loanpro.security.event.UpdateDepartmentRequestSequenceEvent;
import ru.loanpro.security.repository.DepartmentRepository;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.service.SequenceService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Сервис по работе с сущностью отделов {@link Department}.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class DepartmentService extends AbstractIdEntityService<Department, DepartmentRepository> {

    private final ApplicationEventBus applicationEventBus;
    private final SequenceService sequenceService;

    @Autowired
    public DepartmentService(DepartmentRepository repository, ApplicationEventBus applicationEventBus,
                             SequenceService sequenceService) {
        super(repository);
        this.applicationEventBus = applicationEventBus;
        this.sequenceService = sequenceService;
    }

    /**
     * Возвращает список корневых отделов, не имеющих предка.
     *
     * @return Список отделов.
     */
    public List<Department> getRootDepartments() {
        return repository.findAllByParentIsNull();
    }

    /**
     * Возвращает список всех неудалённых отделов.
     *
     * @return Список отделов.
     */
    public List<Department> findAll() {
        return (List<Department>) repository.findAll(QDepartment.department.deleted.isFalse());
    }

    /**
     * Возвращает отдел по идентификатору со всеми инициализированными полями.
     *
     * @param id идентификатор отдела.
     * @return отдел.
     */
    @Transactional
    public Department findOneFull(UUID id) {
        return repository.findOneFull(id);
    }

    /**
     * Сохраняет переданный отдел. При отсутствии идентификатора, создаёт новый отдел.
     *
     * @param department Сохраняемый отдел.
     * @return Сохранённый отдел.
     */
    @Transactional
    public Department save(Department department) {
        Object event = (department.getId() == null)
                ? new CreateDepartmentEvent(department)
                : new UpdateDepartmentEvent(department);

        Sequence oldLoanSequence = new Sequence();
        if (department.getId() != null) {
            oldLoanSequence = repository.getOne(department.getId()).getLoanSequence();
        }
        Sequence oldRequestSequence = new Sequence();
        if (department.getId() != null) {
            oldRequestSequence = repository.getOne(department.getId()).getRequestSequence();
        }
        Sequence oldProlongSequence = new Sequence();
        if (department.getId() != null) {
            oldProlongSequence = repository.getOne(department.getId()).getProlongSequence();
        }
        Sequence oldOperationSequence = new Sequence();
        if (department.getId() != null) {
            oldOperationSequence = repository.getOne(department.getId()).getOperationSequence();
        }


        // Если включено наследование настроек, удаляем лишние настройки
        if (department.getInheritance()) {
            department.setTheme(null);
            department.setZoneId(null);
            department.setLocale(null);
            department.setCurrencies(null);
        }

        Department savedDepartment = repository.save(department);
        applicationEventBus.post(event);

        // Если у отдела была изменена последовательность генерации номеров для займов, отправляем соответствующее событие.
        if (savedDepartment.getLoanSequence().getId().equals(oldLoanSequence.getId()))
            applicationEventBus.post(new UpdateDepartmentLoanSequenceEvent(department));

        // Если у отдела была изменена последовательность генерации номеров для заявок, отправляем соответствующее событие.
        if (savedDepartment.getRequestSequence().getId().equals(oldRequestSequence.getId()))
            applicationEventBus.post(new UpdateDepartmentRequestSequenceEvent(department));

        // Если у отдела была изменена последовательность генерации номеров для пролонгаций, отправляем соответствующее событие.
        if (savedDepartment.getProlongSequence().getId().equals(oldProlongSequence.getId()))
            applicationEventBus.post(new UpdateDepartmentProlongSequenceEvent(department));

        // Если у отдела была изменена последовательность генерации номеров для операций, отправляем соответствующее событие.
        if (savedDepartment.getOperationSequence().getId().equals(oldOperationSequence.getId()))
            applicationEventBus.post(new UpdateDepartmentOperationSequenceEvent(department));

        return savedDepartment;
    }

    public Optional<User> getDepartmentDirector(Department department) {
        return repository.findOne(department);
    }
}

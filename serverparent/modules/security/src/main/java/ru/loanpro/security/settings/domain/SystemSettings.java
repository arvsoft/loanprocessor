package ru.loanpro.security.settings.domain;

import org.hibernate.annotations.GenericGenerator;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Сущность основных настроек системы.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "settings", schema = DatabaseSchema.SECURITY)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "settings_type",
    discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("SYSTEM")
public class SystemSettings {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "value", nullable = false)
    private String value;

    public SystemSettings() {
    }

    public SystemSettings(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

package ru.loanpro.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.global.service.AbstractIdEntityService;
import ru.loanpro.security.domain.Credentials;
import ru.loanpro.security.repository.CredentialsRepository;

/**
 * Сервис сущности {@link Credentials}
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class CredentialsService extends AbstractIdEntityService<Credentials, CredentialsRepository> {

    @Autowired
    public CredentialsService(CredentialsRepository repository) {
        super(repository);
    }

    /**
     * Изменяет параметр дефолтности настроек.
     *
     * @param credentials свзяка пользовател-отдел-права
     * @param value новое значение для параметра {@link Credentials#isDefault}
     */
    @Transactional
    public void setCredentialsDefault(Credentials credentials, Boolean value) {
        repository.setCredentialsDefault(credentials, value);
    }
}

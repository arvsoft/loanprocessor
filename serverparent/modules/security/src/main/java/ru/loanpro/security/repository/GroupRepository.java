package ru.loanpro.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.loanpro.security.domain.Group;

import java.util.Set;
import java.util.UUID;

public interface GroupRepository extends JpaRepository<Group, UUID> {
    Set<Group> findAllByName(String name);
}

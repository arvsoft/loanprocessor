package ru.loanpro.security.settings.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Исключение, возникающее при попытке сохранить или прочитать элемент настроек не поддерживаемого типа.
 *
 * @author Oleg Brizhevatikh
 */
public class SettingsUnsupportedTypeException extends Exception {
    private static final Logger LOGGER = Logger.getLogger(SettingsUnsupportedTypeException.class.getName());

    private Class aClass;

    public SettingsUnsupportedTypeException(Class aClass) {
        this.aClass = aClass;
        LOGGER.log(Level.ALL, String.format("Class %s not supported in settings", aClass.getName()));
    }

    public Class getaClass() {
        return aClass;
    }
}

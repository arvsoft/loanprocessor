package ru.loanpro.security.domain;

import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.domain.AbstractIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Связка пользователя с группами, правами и отделами.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "credentials", schema = DatabaseSchema.SECURITY)
public class Credentials extends AbstractIdEntity {

    /**
     * Ссылка на пользователя. {@link User}.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * Ссылка на отдел. {@link Department}.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id", nullable = false)
    private Department department;

    /**
     * Маркер отдела по-умолчанию. <code>true</code>, если текущий отдел является
     * основным отделом пользователя. Иначе <code>false</code>.
     */
    @Column(name = "is_default", nullable = false)
    private Boolean isDefault = false;

    /**
     * Ссылка на коллекцию групп безопасности пользователя в текущем отделе. {@link Group}.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "credential_group",
        schema = DatabaseSchema.SECURITY,
        joinColumns = {@JoinColumn(name = "credential_id")},
        inverseJoinColumns = {@JoinColumn(name = "group_id")})
    private Set<Group> groups = new HashSet<>();

    /**
     * Ссылка на коллекцию прав пользователя в текущем отделе. {@link Role}.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "credential_role",
        schema = DatabaseSchema.SECURITY,
        joinColumns = {@JoinColumn(name = "credential_id")},
        inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<Role> roles = new HashSet<>();

    public Credentials() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Department getDepartment() {
        return department;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}

package ru.loanpro.security.event;

import ru.loanpro.security.domain.Department;

/**
 * Событие создания отдела {@link Department}.
 *
 * @author Oleg Brizhevatikh
 */
public class CreateDepartmentEvent {

    private Department department;

    /**
     * Конструктор события. Принимает созданный отдел.
     *
     * @param department созданный отдел.
     */
    public CreateDepartmentEvent(Department department) {
        this.department = department;
    }

    /**
     * Возвращает созданный отдел.
     *
     * @return созданный отдел.
     */
    public Department getDepartment() {
        return department;
    }
}

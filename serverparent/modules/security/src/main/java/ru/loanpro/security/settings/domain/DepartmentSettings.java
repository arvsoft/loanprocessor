package ru.loanpro.security.settings.domain;


import ru.loanpro.security.domain.Department;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Сущность настроек отдела.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("DEPARTMENT")
public class DepartmentSettings extends SystemSettings {

    /**
     * Ссылка на отдел. {@link Department}.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    public DepartmentSettings() {
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}

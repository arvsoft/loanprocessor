package ru.loanpro.security.service;

import com.google.common.eventbus.Subscribe;
import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.vaadin.spring.security.VaadinSecurity;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.security.domain.Credentials;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.Role;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.event.UpdateDepartmentEvent;
import ru.loanpro.security.exceptions.NotHavePermissionToAccessException;
import ru.loanpro.sequence.event.UpdateSequenceEvent;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Сервис безопасности. Обеспечивает работу с отделами и правами пользователя.
 * Для каждого пользователя создаётся отдельный экземпляр на время существования сессии
 * пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@VaadinSessionScope
public class SecurityService {

    private static final Logger LOGGER = Logger.getLogger(SecurityService.class.getName());

    private final VaadinSecurity vaadinSecurity;
    private final ApplicationEventBus applicationEventBus;
    private final CredentialsService credentialsService;

    private User user;

    private Department department;
    private Set<Department> departments;

    private Set<Credentials> credentials;

    private Set<String> currentAuthorities;

    @Autowired
    public SecurityService(VaadinSecurity vaadinSecurity, ApplicationEventBus applicationEventBus,
                           CredentialsService credentialsService) {
        this.vaadinSecurity = vaadinSecurity;
        this.applicationEventBus = applicationEventBus;
        this.applicationEventBus.register(this);
        this.credentialsService = credentialsService;

        LOGGER.info("SecurityService constructor");
    }

    @PostConstruct
    public void init() {
        user = (User) vaadinSecurity.getAuthentication().getPrincipal();

        credentials = user.getCredentials();
        Credentials currentCredentials = credentials.stream()
            .filter(Credentials::getDefault)
            .findFirst()
            .orElseGet(() -> credentials.stream().findFirst().get());

        department = currentCredentials.getDepartment();
        departments = credentials.stream().map(Credentials::getDepartment).collect(Collectors.toSet());

        reloadAuthorities(currentCredentials);

        LOGGER.info("Initialize");
    }

    /**
     * Возвращает текущего авторизованного пользователя.
     *
     * @return Текущий пользователь.
     */
    public User getUser() {
        return user;
    }

    /**
     * Возвращает текущий отдел, под которым авторизован пользователь.
     *
     * @return Текущий отдел.
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * Возвращает коллекцию отделов текущего авторизованного пользователя.
     *
     * @return Коллекция отделов текущего пользователя.
     */
    public Set<Department> getDepartments() {
        return departments;
    }

    /**
     * Вызов метода завершения сессии текущего пользователя.
     */
    public void logout() {
        vaadinSecurity.logout();
    }

    /**
     * Проверка наличия роли у текущего пользователя в текущем отделе.
     *
     * @param role Строковое представление роли пользователя.
     *
     * @return <code>true</code> при наличии роли и <code>false</code> при её отсутствии.
     */
    public boolean hasAuthority(String role) {
        return currentAuthorities.contains(role);
    }

    /**
     * Смена текущего отдела пользователя. Сбрасывает текущие настройки безопасности
     * и загружает настройки из указанного отдела.
     *
     * @param departmentId Идентификатор отдела на который необходимо переключиться.
     *
     * @throws NotHavePermissionToAccessException если у пользователя нет прав на доступ к
     *         этому отделу.
     */
    @Transactional
    public void changeDepartment(UUID departmentId) throws NotHavePermissionToAccessException {
        Credentials currentCredentials = this.credentials.stream()
            .filter(item -> item.getDepartment().getId().equals(departmentId))
            .findFirst()
            .orElseThrow(NotHavePermissionToAccessException::new);

        user.getCredentials().stream()
            .filter(item -> item.getDefault())
            .forEach(item -> {
                credentialsService.setCredentialsDefault(item, false);
                item.setDefault(false);
            });

        credentialsService.setCredentialsDefault(currentCredentials, true);
        currentCredentials.setDefault(true);

        department = currentCredentials.getDepartment();

        reloadAuthorities(currentCredentials);
    }

    /**
     * Обработчик события обновления последовательности номеров. Проходит по всем отделам
     * текущей сессии и заменяет старую последовательность на обновлённую.
     *
     * @param event событие обновления последовательности номеров.
     */
    @Subscribe
    private void updateSequenceEventHandler(UpdateSequenceEvent event) {
        departments.forEach(item -> {
            if (item.getLoanSequence().getId().equals(event.getSequence().getId()))
                item.setLoanSequence(event.getSequence());
        });

        if (department.getLoanSequence().getId().equals(event.getSequence().getId()))
            department.setLoanSequence(event.getSequence());
    }

    /**
     * Обработчик события обновления отдела. Проходит по всем отделам текущей сессии и
     * обновляет отдел, если он был изменён.
     *
     * @param event событие обновления отдела.
     */
    @Subscribe
    private void updateDepartmentEventHandler(UpdateDepartmentEvent event) {
        departments.forEach(item -> {
            if (item.getId().equals(event.getDepartment().getId()))
                item = event.getDepartment();
        });

        if (department.getId().equals(event.getDepartment().getId()))
            department = event.getDepartment();
    }

    private void reloadAuthorities(Credentials currentCredentials) {
        currentAuthorities = Stream.concat(
            currentCredentials.getGroups().stream()
                .flatMap(p -> p.getRoles().stream())
                .map(Role::getAuthority)
                .map(String::new),
            currentCredentials.getRoles().stream()
                .map(Role::getAuthority)
                .map(String::new)).collect(Collectors.toSet());
    }
}

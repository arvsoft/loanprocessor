package ru.loanpro.security.domain;

import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.domain.RemovableEntity;
import ru.loanpro.security.domain.converter.ZoneIdConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.time.ZoneId;
import java.util.Objects;

/**
 * Сущность модуля безопасности, расширяющая базовую сущность. Содержит настройки
 * системы.
 *
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public class BaseSettingsEntity extends RemovableEntity {

    @Column(name = "inheritance", nullable = false)
    private Boolean inheritance = true;

    /**
     * Установленная для пользователя языковая раскладка.
     */
    @Column(name = "locale", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private SystemLocale locale;

    /**
     * Установленная для пользователя тема интерфейса.
     */
    @Column(name = "theme", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private UiTheme theme;

    /**
     * Часовой пояс пользователя.
     */
    @Column(name = "time_zone", nullable = false)
    @Convert(converter = ZoneIdConverter.class)
    private ZoneId zoneId;

    /**
     * Номер телефона.
     */
    @Column(name = "phone")
    private String phone;

    /**
     * Электронная почта.
     */
    @Column(name = "email")
    private String email;

    public Boolean getInheritance() {
        return inheritance;
    }

    public void setInheritance(Boolean inheritance)
    {
        this.inheritance = inheritance;
    }

    public SystemLocale getLocale() {
        return locale;
    }

    public void setLocale(SystemLocale locale) {
        this.locale = locale;
    }

    public UiTheme getTheme() {
        return theme;
    }

    public void setTheme(UiTheme theme) {
        this.theme = theme;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BaseSettingsEntity that = (BaseSettingsEntity) o;
        return Objects.equals(inheritance, that.inheritance) &&
            locale == that.locale &&
            theme == that.theme &&
            Objects.equals(zoneId, that.zoneId) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            super.hashCode(),
            inheritance,
            locale,
            theme,
            zoneId,
            phone,
            email);
    }
}

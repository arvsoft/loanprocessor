package ru.loanpro.security.event;

import ru.loanpro.security.domain.Department;

/**
 * Событие обновления последовательности номеров операций отдела {@link Department}.
 *
 * @author Maksim Askhaev
 */
public class UpdateDepartmentOperationSequenceEvent {

    private Department department;

    /**
     * Конструктор события. Принимает обновлённый отдел.
     *
     * @param department обновлённый отдел.
     */
    public UpdateDepartmentOperationSequenceEvent(Department department) {
        this.department = department;
    }

    /**
     * Возвращает обновлённый отдел.
     *
     * @return обновлённый отдел.
     */
    public Department getDepartment() {
        return department;
    }
}

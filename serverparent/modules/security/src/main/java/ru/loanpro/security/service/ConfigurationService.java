package ru.loanpro.security.service;

import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.loanpro.global.Settings;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.global.directory.OverdueDaysCalculationType;
import ru.loanpro.global.directory.PaymentSplitType;
import ru.loanpro.global.directory.RecalculationType;
import ru.loanpro.global.directory.RepaymentOrderScheme;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.settings.service.SettingsService;
import ru.loanpro.security.settings.utils.CurrencyComparator;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.service.SequenceService;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Сервис настроек пользователя, отдела и системы. Обеспечивает удобный доступ
 * к настройкам пользователя, отдела и системы, с учётом иерархии наследования
 * настроек.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@VaadinSessionScope
public class ConfigurationService {

    private static final Logger LOGGER = Logger.getLogger(ConfigurationService.class.getName());

    private final SecurityService securityService;
    private final SettingsService settingsService;
    private final SequenceService sequenceService;

    /**
     * Конструктор, принимающий сервисы безопасности и базовых настроек системы.
     *
     * @param securityService сервис безопасности.
     * @param settingsService сервис базовых настроек системы.
     */
    @Autowired
    public ConfigurationService(SecurityService securityService, SettingsService settingsService,
                                SequenceService sequenceService) {
        this.securityService = securityService;
        this.settingsService = settingsService;
        this.sequenceService = sequenceService;
    }

    /**
     * Возвращает системную локаль {@link SystemLocale}.
     *
     * @return системная локаль.
     */
    public SystemLocale getSystemLocale() {
        User user = securityService.getUser();

        if (user.getInheritance()) {
            return getSettingsItem(securityService.getDepartment(), Department::getLocale,
                SystemLocale.class, Settings.LOCALE, SystemLocale.ENGLISH);
        } else {
            return user.getLocale();
        }
    }

    /**
     * Возвращает локаль {@link Locale}
     *
     * @return локаль.
     */
    public Locale getLocale() {
        return getSystemLocale().getLocale();
    }

    /**
     * Возвращает строковое название темы интерфейса {@link UiTheme}.
     *
     * @return название темы интерфейса.
     */
    public String getTheme() {
        User user = securityService.getUser();

        if (user.getInheritance()) {
            return getSettingsItem(securityService.getDepartment(), Department::getTheme,
                UiTheme.class, Settings.THEME, UiTheme.LIGHT).getTheme();
        } else {
            return user.getTheme().getTheme();
        }
    }

    /**
     * Возвращает часовой пояс {@link ZoneId}.
     *
     * @return часовой пояс.
     */
    public ZoneId getZoneId() {
        User user = securityService.getUser();

        if (user.getInheritance()) {
            return getAndConvertSettingsItem(securityService.getDepartment(), Department::getZoneId,
                String.class, Settings.TIME_ZONE, "UTC", ZoneId::of);
        } else {
            return user.getZoneId();
        }
    }

    /**
     * Возвращает формат даты в строковом виде.
     *
     * @return формат даты.
     */
    public String getDateFormat() {
        return settingsService.get(String.class, Settings.DATE_FORMAT, "yyyy-MM-dd");
    }

    /**
     * Возвращает последовательность номеров счетов для отделов
     *
     * @return последовательность
     */
    public Sequence getDepartmentAccountSequence() {
        return sequenceService.getOneFull(UUID.fromString(settingsService.get(String.class, Settings.DEPARTMENT_ACCOUNT,
            sequenceService.findAll().get(0).getId().toString())));
    }

    /**
     * Возвращает коллекцию валют {@link Currency}.
     *
     * @return коллекция валют.
     */
    public List<Currency> getCurrencies() {
        Department department = securityService.getDepartment();
        Set<Currency> currencySet;
        if (department.getInheritance()) {
            if (department.getParent() != null) {
                currencySet = getAndConvertSettingsItem(department.getParent(), Department::getCurrencies,
                    String.class, Settings.CURRENCIES, "RUB,USD,EUR", this::stringToCurrencySet);
            } else {
                currencySet = stringToCurrencySet(settingsService.get(String.class, Settings.CURRENCIES, "RUB,USD,EUR"));
            }
        } else {
            currencySet = department.getCurrencies();
        }

        return currencySet.stream().sorted(new CurrencyComparator(getSystemLocale())).collect(Collectors.toList());
    }

    /**
     * Возвращает валюту по умолчанию текущего отдела
     *
     * @return валюта Currency
     */
    public Currency getCurrencyDefault(){
        return Stream.of(
            settingsService.get(securityService.getDepartment(), String.class,
                Settings.CURRENCY_DEFAULT, getCurrencies().get(0).getCurrencyCode()))
            .map(Currency::getInstance)
            .findFirst().get();
    }

    /**
     * Возвращает финансовую настройку RecalculationType по текущему отделу
     *
     * @return элемент настройки - RecalculationType
     */
    public RecalculationType getRecalculationType() {
        return getFinancySettingItem(Settings.RECALCULATION_TYPE, RecalculationType.class,
            RecalculationType.RECALCULATION_ON_NEW_TERM);
    }

    /**
     * Возвращает финансовую настройку OverdueDaysCalculationType по текущему отделу
     *
     * @return элемент настройки - OverdueDaysCalculationType
     */
    public OverdueDaysCalculationType getOverdueDaysCalculationType() {
        return getFinancySettingItem(Settings.OVERDUE_DAYS_CALCULATION_TYPE, OverdueDaysCalculationType.class,
            OverdueDaysCalculationType.FROM_DATE_ACCORDING_SCHEDULE);
    }

    /**
     * Возвращает финансовую настройку RepaymentOrderScheme по текущему отделу
     *
     * @return элемент настройки - RepaymentOrderScheme
     */
    public RepaymentOrderScheme getRepaymentOrderScheme() {
        return getFinancySettingItem(Settings.REPAYMENT_ORDER_SCHEME, RepaymentOrderScheme.class,
            RepaymentOrderScheme.ORDER1);
    }

    /**
     * Возвращает финансовую настройку SummationPenalty по текущему отделу
     *
     * @return элемент настройки - булевое значение
     */
    public boolean getSummationPenalty() {
        return getFinancySettingItem(Settings.SUMMATION_PENALTY, Boolean.class, true);
    }

    /**
     * Возвращает финансовую настройку SummationFine по текущему отделу
     *
     * @return элемент настройки - булевое значение
     */
    public boolean getSummationFine() {
        return getFinancySettingItem(Settings.SUMMATION_FINE, Boolean.class, true);
    }

    /**
     * Возвращает финансовую настройку AutoOperationIssuedMoneyLoanIssued по текущему отделу
     *
     * @return элемент настройки - булевое значение
     */
    public boolean getAutoOperationIssuedMoneyLoanIssued() {
        return getFinancySettingItem(Settings.AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED, Boolean.class, false);
    }

    /**
     * Возвращает финансовую настройку AutoOperationReplenishMoneyLoanPayment по текущему отделу
     *
     * @return элемент настройки - булевое значение
     */
    public boolean getAutoOperationReplenishMoneyLoanPayment() {
        return getFinancySettingItem(Settings.AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT, Boolean.class, false);
    }

    /**
     * Возвращает финансовую настройку AutoOperationIssuedMoneyLoanIssued по текущему отделу
     *
     * @return элемент настройки - булевое значение
     */
    public boolean getAutoOperationIssuedMoneyLoanIssuedProcessed() {
        return getFinancySettingItem(Settings.AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED_PROCESSED, Boolean.class, false);
    }

    /**
     * Возвращает финансовую настройку AutoOperationReplenishMoneyLoanPayment по текущему отделу
     *
     * @return элемент настройки - булевое значение
     */
    public boolean getAutoOperationReplenishMoneyLoanPaymentProcessed() {
        return getFinancySettingItem(Settings.AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT_PROCESSED, Boolean.class, false);
    }

    /**
     * Возвращает финансовую настройку PaymentSplitType по текущему отделу
     *
     * @return элемент настройки - PaymentSplitType
     */
    public PaymentSplitType getPaymentSplitType() {
        return getFinancySettingItem(Settings.PAYMENT_SPLIT_TYPE, PaymentSplitType.class,
            PaymentSplitType.ONE_TRANSACTION);
    }

    /**
     * Возвращает финансовую настройку KeepSameOperationDateWhenSaveFromPreparedToProcessed по текущему отделу
     *
     * @return элемент настройки - булевое значение
     */
    public boolean getKeepSameOperationDateWhenSaveFromPreparedToProcessed() {
        return getFinancySettingItem(Settings.KEEP_SAME_OPERATION_DATE_WHEN_TURN_FROM_PREPARED_TO_PROCESSED, Boolean.class, false);
    }

    /**
     * Возвращает стиль грида
     *
     * @return элемент настройки - булевое значение
     */
    public GridStyle getGridStyle() {
        User user = securityService.getUser();

        if (user.getInheritance()){
            Department department = securityService.getDepartment();
            return settingsService.get(department, GridStyle.class, Settings.GRID_STYLE, GridStyle.NORMAL);
        } else {
            return settingsService.get(user, GridStyle.class, Settings.GRID_STYLE, GridStyle.NORMAL);
        }
    }

    /**
     * Возвращает элемент настройки для текущего отдела
     *
     * @param key          - ключ согласно Setting
     * @param tClass       - класс
     * @param defaultValue - значение по умолчанию
     * @param <T>          - тип значения
     * @return элемент настройки
     */
    private <T> T getFinancySettingItem(String key, Class<T> tClass, T defaultValue) {
        Department department = securityService.getDepartment();
        return settingsService.get(department, tClass, key, defaultValue);
    }

    /**
     * Возвращает элемент настроек. Осуществляет рекурсивный поиск по дереву
     * отделов. При нахождении отдела, не наследующего настройки вышестоящего
     * отдела, извлекает элемент его настроек с помощью переданной функции. Если
     * подходящий отдел в дереве не найден, вызывает сервис базовых настроек и
     * возвращает настройку системы или значение по-умолчанию.
     *
     * @param department   отдел.
     * @param getter       функция извлечения элемента настроек.
     * @param tClass       класс, к которому должен быть преобразован элемент
     *                     настроек, если он извлекается из базовых настроек.
     * @param key          ключ, для извлечения элемента из базовых настроек.
     * @param defaultValue значение по-умолчанию, если элемент настроек не был
     *                     найден в отделе или в базовых настройках.
     * @param <T>          тип возвращаемого значения настроек.
     * @return элемент настроек.
     */
    private <T> T getSettingsItem(Department department, Function<Department, T> getter,
                                  Class<T> tClass, String key, T defaultValue) {

        if (department.getInheritance()) {
            if (department.getParent() != null) {
                return getSettingsItem(department.getParent(), getter, tClass, key, defaultValue);
            } else {
                return settingsService.get(tClass, key, defaultValue);
            }
        } else {
            return getter.apply(department);
        }
    }

    /**
     * Возвращает элемент настроек. Осуществляет рекурсивный поиск по дереву
     * отделов. При нахождении отдела, не наследующего настройки вышестоящего
     * отдела, извлекает элемент его настроек с помощью переданнй функции. Если
     * подходящий отдел в дереве не найден, вызывает сервис базовых настроек и
     * возвращает настройку системы, преобразовав к нужному типу с помощью
     * переданного конвертера или значение по-умолчанию.
     *
     * @param department   отдел.
     * @param getter       функция извлечения элемента настроек.
     * @param tClass       класс, к которому должен быть преобразован элемент
     *                     настроек, если он извлекается из базовых настроек.
     * @param key          ключ для извлечения элемента из базовых настроек.
     * @param defaultValue значение по-умолчанию, если элемент настроек не был
     *                     найден в отделе или в базовых настройках.
     * @param converter    конвертер, выполняющий преобразование элемента типа
     *                     <code>S</code>, полученного из настроек, к типу
     *                     <code>T</code>, который необходимо вернуть.
     * @param <T>          тип возвращаемого значения настроек.
     * @param <S>          тип извлекаемого из базовых настроек значения.
     * @return элемент настроек.
     */
    private <T, S> T getAndConvertSettingsItem(Department department, Function<Department, T> getter,
                                               Class<S> tClass, String key, S defaultValue,
                                               Function<S, T> converter) {

        if (department.getInheritance()) {
            if (department.getParent() != null) {
                return getAndConvertSettingsItem(department.getParent(), getter, tClass, key, defaultValue, converter);
            } else {
                return converter.apply(settingsService.get(tClass, key, defaultValue));
            }
        } else {
            return getter.apply(department);
        }
    }

    /**
     * Конвертер строки в коллекцию валют.
     *
     * @param string конвертируемая строка.
     * @return коллекция валют.
     */
    private Set<Currency> stringToCurrencySet(String string) {
        return Arrays.stream(string.split(",")).map(Currency::getInstance).collect(Collectors.toSet());
    }
}

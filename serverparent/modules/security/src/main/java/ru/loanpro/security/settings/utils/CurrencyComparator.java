package ru.loanpro.security.settings.utils;

import ru.loanpro.global.SystemLocale;

import java.util.Comparator;
import java.util.Currency;

/**
 * Функция сравнения валют для сортировки валют по алфвавиту. Если передана
 * системная локаль, или валюта, переданная валюта или валюта системной локали
 * будет сортироваться в начало списка.
 *
 * @author Oleg Brizhevatikh
 */
public class CurrencyComparator implements Comparator<Currency> {

    private Currency currentCurrency;

    /**
     * Конструктор по-умолчанию.
     */
    public CurrencyComparator() {
    }

    /**
     * Конструктор, принимающий системную локаль.
     *
     * @param systemLocale системная локаль.
     */
    public CurrencyComparator(SystemLocale systemLocale) {
        currentCurrency = systemLocale.getCurrency();
    }

    /**
     * Конструктор, принимающий валюту.
     *
     * @param currentCurrency валюта.
     */
    public CurrencyComparator(Currency currentCurrency) {
        this.currentCurrency = currentCurrency;
    }

    /**
     * Функция сравнения <code>Comparator#compare(Object, Object)</code>
     *
     * @param o1 первый сравниваемый объект.
     * @param o2 второй сравниваемый объект.
     * @return результат сравнения.
     */
    @Override
    public int compare(Currency o1, Currency o2) {
        if (currentCurrency != null) {
            if (currentCurrency.equals(o1))
                return -1;
            else if (currentCurrency.equals(o2))
                return 1;
        }

        return o1.getCurrencyCode().compareTo(o2.getCurrencyCode());
    }
}
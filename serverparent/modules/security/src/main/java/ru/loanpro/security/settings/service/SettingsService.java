package ru.loanpro.security.settings.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.service.UserService;
import ru.loanpro.security.settings.domain.DepartmentSettings;
import ru.loanpro.security.settings.domain.SystemSettings;
import ru.loanpro.security.settings.domain.UserSettings;
import ru.loanpro.security.settings.exceptions.SettingsNotFoundException;
import ru.loanpro.security.settings.exceptions.SettingsUnsupportedTypeException;
import ru.loanpro.security.settings.repository.SystemSettingsRepository;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Сервис настроек системы.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class SettingsService {

    private static Logger LOGGER = Logger.getLogger(SettingsService.class.getName());

    private final SystemSettingsRepository systemSettingsRepository;
    private final UserService userService;
    private final DepartmentService departmentService;

    private transient Map<UUID, Map<String, SystemSettings>> userMap;
    private transient Map<UUID, Map<String, SystemSettings>> departmentMap;
    private transient Map<String, SystemSettings> systemMap;

    /**
     * Конструктор для инъекции зависимостей Спрингом.
     *
     * @param systemSettingsRepository репозиторий настроек.
     *
     * @param userService сервис пользователя.
     *
     * @param departmentService сервис подразделений.
     */
    @Autowired
    public SettingsService(SystemSettingsRepository systemSettingsRepository,
                           UserService userService,
                           DepartmentService departmentService) {
        this.systemSettingsRepository = systemSettingsRepository;
        this.userService = userService;
        this.departmentService = departmentService;
    }

    /**
     * Метод иниацилизации сервиса. Читает настройки из базы данных и кеширует в мапы системы,
     * пользователей и отделов. При вызове обновляет все закешированные настройки.
     */
    @PostConstruct
    public synchronized void init() {
        LOGGER.info("Settings loading...");

        userMap = new HashMap<>();
        userService.findAllUsers().forEach(u -> userMap.put(u.getId(), new HashMap<>()));

        departmentMap = new HashMap<>();
        departmentService.findAll().forEach(d -> departmentMap.put(d.getId(), new HashMap<>()));

        systemMap = new HashMap<>();

        List<SystemSettings> system = systemSettingsRepository.findAll();
        system.forEach(item -> {
            if (item instanceof UserSettings) {
                userMap.get(((UserSettings) item).getUser().getId())
                    .put(item.getKey(), item);
            } else if (item instanceof DepartmentSettings) {
                departmentMap.get(((DepartmentSettings) item).getDepartment().getId())
                    .put(item.getKey(), item);
            } else {
                systemMap.put(item.getKey(), item);
            }
        });

        LOGGER.info("Settings loaded.");
    }

    /**
     * Возвращает элемент настроек системы по типу требуемого значения и ключу настроек.
     *
     * @param aClass класс, в который необходимо сконвертировать значение.
     *
     * @param key строковый ключ, значение которого необходимо получить.
     *
     * @return значение настройки.
     *
     * @throws SettingsNotFoundException если ключ не найден.
     *
     * @throws SettingsUnsupportedTypeException если невозможно привести значение к указанному типу.
     */
    public <O> O get(Class<O> aClass, String key)
        throws SettingsNotFoundException, SettingsUnsupportedTypeException {

        if (aClass.isEnum())
            return (O) Enum.valueOf((Class) aClass, getFromMap(key, systemMap).getValue());
        else
            return stringToObject(aClass, getFromMap(key, systemMap).getValue());
    }

    /**
     * Возвращает элемент настроек системы, по типу требуемого значения, ключу
     * настроек и значению по-умолчанию, если требуемое значение не было найдено.
     *
     * @param aClass класс, в который необходимо сконвертировать значение.
     * @param key строковый ключ, значение которого необходимо получить.
     * @param defaultValue значение по-умолчанию.
     * @param <O> тип возвращаемого значения.
     *
     * @return значение настройки.
     */
    public <O> O get(Class<O> aClass, String key, O defaultValue) {
        try {
            return get(aClass, key);
        } catch (SettingsNotFoundException | SettingsUnsupportedTypeException e) {
            LOGGER.info(String.format("Key %s throw exception: %s", key, e.getLocalizedMessage()));
            return defaultValue;
        }
    }

    /**
     * Возвращает элемент настроек пользователя по экземпляру пользователя, типу требуемого значения
     * и ключу настроек.
     *
     * @param user {@link User} для которого необходимо получить значение настройки.
     *
     * @param aClass класс, в который необходимо сконвертировать значение.
     *
     * @param key строковый ключ, значение которого необходимо получить.
     *
     * @return значение настройки.
     *
     * @throws SettingsNotFoundException если ключ данного пользователя не найден.
     *
     * @throws SettingsUnsupportedTypeException если невозможно привести значение к указанному типу.
     */
    public <O> O get(User user, Class<O> aClass, String key)
        throws SettingsNotFoundException, SettingsUnsupportedTypeException {

        Map<String, SystemSettings> map = userMap.get(user.getId());

        if (aClass.isEnum())
            return (O) Enum.valueOf((Class) aClass, getFromMap(key, map).getValue());
        else
            return stringToObject(aClass, getFromMap(key, map).getValue());
    }

    /**
     * Возвращает элемент настроек пользователя по экземпляру пользователя, типу
     * требуемого значения, ключу настроек и значению по-умолчанию, если
     * требуемое значение не было найдено.
     *
     * @param user {@link User} для которого необходимо получить значение настройки.
     * @param aClass класс, в который необходимо сконвертировать значение.
     * @param key строковый ключ, значение которого необходимо получить.
     * @param defaultValue значение по-умолчанию.
     * @param <O> тип возвращаемого значения.
     *
     * @return значение настройки.
     */
    public <O> O get(User user, Class<O> aClass, String key, O defaultValue) {
        try {
            return get(user, aClass, key);
        } catch (SettingsNotFoundException | SettingsUnsupportedTypeException e) {
            LOGGER.info(String.format("Key: %s for user: %s throw exception: %s",
                key, user.getUsername(), e.getLocalizedMessage()));
            return defaultValue;
        }
    }

    /**
     * Возвращает элемент настроек отдела по экземпляру отдела, типу требуемого значения
     * и ключу настроек.
     *
     * @param department {@link Department} для которого необходимо получить значение настройки.
     *
     * @param aClass класс, в который необходимо сконвертировать значение.
     *
     * @param key строковый ключ, значение которого необходимо получить.
     *
     * @return значение настройки.
     *
     * @throws SettingsNotFoundException если ключ данного пользователя не найден.
     *
     * @throws SettingsUnsupportedTypeException если невозможно привести значение к указанному типу.
     */
    public <O> O get(Department department, Class<O> aClass, String key)
        throws SettingsNotFoundException, SettingsUnsupportedTypeException {

        Map<String, SystemSettings> map = departmentMap.get(department.getId());
        if (aClass.isEnum())
            return (O) Enum.valueOf((Class) aClass, getFromMap(key, map).getValue());
        else
            return stringToObject(aClass, getFromMap(key, map).getValue());
    }

    /**
     * Возвращает элемент настроек отдела по экземпляру отдела, типу требуемого
     * значения, ключу настроек и значениею по-умолчанию, если требуемое
     * значение не было найдено.
     *
     * @param department {@link Department} для которого необходимо получить значение настройки.
     * @param aClass класс, в который необходимо сконвертировать значение.
     * @param key строковый ключ, значение которого необходимо получить.
     * @param defaultValue значение по-умолчанию.
     * @param <O> тип возвращаемого значения.
     *
     * @return значение настройки
     */
    public <O> O get(Department department, Class<O> aClass, String key, O defaultValue) {
        try {
            return get(department, aClass, key);
        } catch (SettingsNotFoundException | SettingsUnsupportedTypeException e) {
            LOGGER.info(String.format("Key: %s for user: %s throw exception: %s",
                key, department.getName(), e.getLocalizedMessage()));
            return defaultValue;
        }
    }

    /**
     * Сохраняет значение системных настроек по ключу и значению. Поддерживается ключ
     * типа {@link String}. Значением выступает элемент {@link Enum} или
     * любой класс, имеющий конструктор с одним значением типа {@link String}.
     *
     * @param key {@link String} ключ, значение которого необходимо сохранить.
     *
     * @param value значение типа - элемент {@link Enum} или {@link Object}.
     *
     * @throws SettingsUnsupportedTypeException если передано значение неподдерживаемого типа.
     */
    public void set(String key, Object value) throws SettingsUnsupportedTypeException {
        validateValue(value);

        if (value.getClass().isEnum())
            updateSettings(systemMap, key, ((Enum) value).name(), SystemSettings.class, null, null);
        else
            updateSettings(systemMap, key, String.valueOf(value), SystemSettings.class, null, null);
    }

    /**
     * Сохраняет значение настроек пользователя по ключу и значению. Поддерживается ключ
     * типа {@link String}. Значением выступает элемент {@link Enum} или
     * любой класс, имеющий конструктор с одним значением типа {@link String}.
     *
     * @param user {@link User} пользователь, настройки которого сохраняются.
     *
     * @param key {@link String} ключ, значение которого необходимо сохранить.
     *
     * @param value значение типа - элемент {@link Enum} или {@link Object}.
     *
     * @throws SettingsUnsupportedTypeException если передано значение неподдерживаемого типа.
     */
    public void set(User user, String key, Object value) throws SettingsUnsupportedTypeException {
        validateValue(value);

        Map<String, SystemSettings> map = userMap.computeIfAbsent(user.getId(), k -> new HashMap<>());

        if (value.getClass().isEnum())
            updateSettings(map, key, ((Enum) value).name(), UserSettings.class, user, null);
        else
            updateSettings(map, key, String.valueOf(value), UserSettings.class, user, null);
    }

    /**
     * Сохраняет значение настроек подразделения по ключу и значению. Поддерживается ключ
     * типа {@link String}. Значением выступает элемент {@link Enum} или
     * любой класс, имеющий конструктор с одним значением типа {@link String}.
     *
     * @param department {@link Department} подразделение, настройки которого сохраняются.
     *
     * @param key {@link String} ключ, значение которого необходимо сохранить.
     *
     * @param value значение типа - элемент {@link Enum} или {@link Object}.
     *
     * @throws SettingsUnsupportedTypeException если передано значение неподдерживаемого типа.
     */
    public void set(Department department, String key, Object value) throws SettingsUnsupportedTypeException {
        validateValue(value);

        Map<String, SystemSettings> map = departmentMap.computeIfAbsent(department.getId(), k -> new HashMap<>());

        if (value.getClass().isEnum())
            updateSettings(map, key, ((Enum) value).name(), DepartmentSettings.class, null, department);
        else
            updateSettings(map, key, String.valueOf(value), DepartmentSettings.class, null, department);
    }

    /**
     * Получает сущность настроек по ключу из переданной карты.
     *
     * @param key ключ настроек.
     *
     * @param map карта, из которой необходимо получить сущность настроек.
     *
     * @return сущность настроек.
     *
     * @throws SettingsNotFoundException если по переданному ключу не найдена сущность.
     */
    private SystemSettings getFromMap(String key, Map<String, SystemSettings> map) throws SettingsNotFoundException {
        if (map == null)
            throw new SettingsNotFoundException();

        return Optional.ofNullable(map.get(key)).orElseThrow(SettingsNotFoundException::new);
    }

    /**
     * Преобразует строку к переданному классу.
     *
     * @param aClass класс, к которому требуется преобразовать строку.
     *
     * @param value преобразуемая строка.
     *
     * @return экземпляр указанного класса.
     *
     * @throws SettingsUnsupportedTypeException если невозможно преобразовать данную строку
     *         к указанному классу.
     */
    private <O> O stringToObject(Class<O> aClass, String value) throws SettingsUnsupportedTypeException {
        try {
            return aClass.getDeclaredConstructor(String.class).newInstance(value);
        } catch (InstantiationException
            | IllegalAccessException
            | NoSuchMethodException
            | InvocationTargetException e) {
            LOGGER.log(Level.ALL, e.getMessage());
            throw new SettingsUnsupportedTypeException(aClass);
        }
    }

    /**
     * Проверка значения на возможность дальнейшего сохранения и чтения из базы данных.
     *
     * @param value проверяемое значение.
     *
     * @throws SettingsUnsupportedTypeException если переданное значение потенциально невозможно
     *          сохранить или прочитать из базы данных.
     */
    private void validateValue(Object value) throws SettingsUnsupportedTypeException {
        try {
            // Значение может быть элементом Enum или иметь класс с конструктором, принимающим String.
            if (!value.getClass().isEnum() && value.getClass().getDeclaredConstructor(String.class) == null)
                throw new SettingsUnsupportedTypeException(value.getClass());
        } catch (NoSuchMethodException e) {
            LOGGER.log(Level.ALL, e.getMessage());
            throw new SettingsUnsupportedTypeException(value.getClass());
        }
    }

    /**
     * Обновление переданной мапы настроек и сохранение значений в базу данных.
     *
     * Если переданного ключа не найдено, он будет создан с переданным значением,
     * сохранён в базу данных и установлен в переданную мапу.
     *
     * Если переданный ключ содержится в переданной мапе, его значение будет обновлено
     * и сохранено в базу данных.
     *
     * @param map мапа для обновления значения по ключу.
     *
     * @param key обнлвляемый ключ.
     *
     * @param value новое значение ключа.
     *
     * @param typeSettings тип обновляемого справочника.
     *
     * @param user пользователь, чей ключ обновляется. <code>null</code>, если настройки системные или подразделения.
     *
     * @param department подразделение, чей ключ обновляется. <code>null</code>, если настройки системные или пользователя.
     */
    private void updateSettings(Map<String, SystemSettings> map, String key, String value,
                                Class<? extends SystemSettings> typeSettings, User user, Department department) {
        if (map.containsKey(key)) {
            SystemSettings settings = systemSettingsRepository.findById(map.get(key).getId()).get();
            settings.setValue(value);
            systemSettingsRepository.save(settings);

            map.put(key, settings);
        } else try {
            SystemSettings settings = typeSettings.newInstance();
            settings.setKey(key);
            settings.setValue(value);
            if (settings instanceof UserSettings)
                ((UserSettings) settings).setUser(user);
            if (settings instanceof DepartmentSettings)
                ((DepartmentSettings) settings).setDepartment(department);

            settings = systemSettingsRepository.save(settings);
            map.put(key, settings);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}

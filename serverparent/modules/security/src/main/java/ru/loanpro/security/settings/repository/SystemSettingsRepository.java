package ru.loanpro.security.settings.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.loanpro.security.settings.domain.DepartmentSettings;
import ru.loanpro.security.settings.domain.SystemSettings;
import ru.loanpro.security.settings.domain.UserSettings;

import java.util.UUID;

/**
 * Репозиторий сущностей
 * {@link SystemSettings}
 * {@link UserSettings}
 * {@link DepartmentSettings}.
 *
 * @author Oleg Brizhevatikh
 */
public interface SystemSettingsRepository extends JpaRepository<SystemSettings, UUID> {
}

package ru.loanpro.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.loanpro.security.domain.Credentials;
import ru.loanpro.security.domain.Group;
import ru.loanpro.security.domain.Role;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.repository.GroupRepository;
import ru.loanpro.security.repository.RoleRepository;
import ru.loanpro.security.repository.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Сервис по работе с пользователями и их правами.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, GroupRepository groupRepository, RoleRepository roleRepository,
                       PasswordEncoder passwordEncoder) {

        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Возвращает всех пользователей системы.
     *
     * @return Список всех пользователей.
     */
    @Transactional
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Возвращает список активных пользователей.
     *
     * @return список активных пользователей.
     */
    @Transactional
    public List<User> findAllActiveUsers() {
        return userRepository.findAllActiveUsers();
    }

    /**
     * Возвращает все группы безопасности.
     *
     * @return Список групп безопасности.
     */
    public List<Group> findAllGroups() {
        return groupRepository.findAll();
    }

    /**
     * Проверяет наличие в системе пользователя с переданным именем.
     *
     * @param username Проверяемое имя пользователя.
     *
     * @return <code>true</code> если имя свободно, иначе <code>false</code>.
     */
    public boolean usernameIsVacant(String username) {
        return userRepository.countByUsername(username) == 0;
    }

    /**
     * Сохраняет пользователя системы.
     *
     * @param user Пользователь системы.
     *
     * @return Сохранённый пользователь.
     */
    @Transactional
    public User save(User user) {
        if (user.getId() != null) {
            // Обновление существующего пользователя
            User oldUser = userRepository.getOne(user.getId());
            Set<UUID> oldCredentialsIds = oldUser.getCredentials().stream()
                .map(Credentials::getId)
                .collect(Collectors.toSet());

            user.getCredentials().stream()
                .filter(item -> !oldCredentialsIds.contains(item.getId()))
                .forEach(item -> item.setId(null));

            if (!user.getPassword().trim().equals("")) {
                // Если пароль пользователя не пустой, то шифруем его и обновляем.
                user.setPassword(passwordEncoder.encode(user.getPassword()));
            } else {
                // Иначе, заменяем его на пароль пользователя из бд
                String password = oldUser.getPassword();
                user.setPassword(password);
            }
        } else {
            // Создание нового пользователя
            // Шифруем пароль
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.getCredentials().forEach(item -> item.setId(null));
        }

        // Если включено наследование настроек, удаляем лишние настройки.
        if (user.getInheritance()) {
            user.setTheme(null);
            user.setZoneId(null);
            user.setLocale(null);
        }

        return userRepository.save(user);
    }

    /**
     * Сохраняет группу безопасности.
     *
     * @param group Группа безопасности.
     *
     * @return Сохранённая группа
     */
    @Transactional
    public Group save(Group group) {
        return groupRepository.save(group);
    }

    /**
     * Возвращает список ролей содержащихся в группе безопасности по Id группы.
     *
     * @param uuid Идентификатор группы безопасности.
     *
     * @return Список ролей.
     */
    public List<Role> findGroupRoles(UUID uuid) {
        return groupRepository.findById(uuid).map(Group::getRoles).orElseGet(Collections::emptyList);
    }

    /**
     * Возвращает все роли системы.
     *
     * @return Список ролей.
     */
    public List<Role> findAllRoles() {
        return roleRepository.findAll();
    }

    /**
     * Системный метод передачи пользователя из базы данных системе Spring-security.
     *
     * @param username Имя пользователя.
     *
     * @return Информацию о пользователе и его правах.
     *
     * @throws UsernameNotFoundException если пользователь не найден.
     */
    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("Not found " + username));

        return user;
    }

    /**
     * Отключает переданного пользователя.
     *
     * @param user отключаемый пользователь.
     */
    @Transactional
    public void setDisable(User user) {
        user.setEnabled(false);

        userRepository.save(user);
    }

    /**
     * Включает переданного пользователя.
     *
     * @param user включаемый пользователь.
     */
    @Transactional
    public void setEnable(User user) {
        user.setEnabled(true);

        userRepository.save(user);
    }

    /**
     * Возвращает количество активных пользователей.
     *
     * @return количество активных пользователей.
     */
    public Integer getActiveUserCount() {
        return userRepository.getActiveUserCount();
    }
}

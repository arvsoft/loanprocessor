package ru.loanpro.security.domain.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Currency;

/**
 * Hibernate-конвертер валюты в текстовое представление для сохранения в базе данных.
 *
 * @author Oleg Brizhevatikh
 */
@Converter(autoApply = true)
public class CurrencyConverter  implements AttributeConverter<Currency, String> {

    /**
     * Преобразует валюту в строку.
     *
     * @param currency валюта.
     * @return строка.
     */
    @Override
    public String convertToDatabaseColumn(Currency currency) {
        return currency == null ? null : currency.getCurrencyCode();
    }

    /**
     * Преобразует строку в валюту.
     *
     * @param currencyCode строка.
     * @return валюта.
     */
    @Override
    public Currency convertToEntityAttribute(String currencyCode) {
        return currencyCode == null ? null : Currency.getInstance(currencyCode);
    }
}

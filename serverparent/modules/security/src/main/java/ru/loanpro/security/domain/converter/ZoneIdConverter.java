package ru.loanpro.security.domain.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.ZoneId;

/**
 * Конвертер часового пояса {@link ZoneId} в строку {@link String} и обратно,
 * для автоматического преобразования значений при сохранении в базу данных и
 * чтении из неё.
 *
 * @author Oleg Brizhevatikh
 */
@Converter(autoApply = true)
public class ZoneIdConverter implements AttributeConverter<ZoneId, String> {

    /**
     * Возвращает {@link String} из {@link ZoneId} при сохранении часового пояса
     * в базу данных.
     *
     * @param zoneId часовой пояс.
     *
     * @return строковое представление часового пояса.
     */
    @Override
    public String convertToDatabaseColumn(ZoneId zoneId) {
        return zoneId == null ? null : zoneId.getId();
    }

    /**
     * Возвращает {@link ZoneId} из {@link String} при чтении часового пояса из
     * базы данных.
     *
     * @param zone строковое представление часового пояса.
     *
     * @return часовой пояс.
     */
    @Override
    public ZoneId convertToEntityAttribute(String zone) {
        return zone == null ? null : ZoneId.of(zone);
    }
}

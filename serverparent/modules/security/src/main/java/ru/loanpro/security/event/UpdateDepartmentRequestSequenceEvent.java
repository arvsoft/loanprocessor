package ru.loanpro.security.event;

import ru.loanpro.security.domain.Department;

/**
 * Событие обновления последовательности номеров заявок отдела {@link Department}.
 *
 * @author Maksim Askhaev
 */
public class UpdateDepartmentRequestSequenceEvent {

    private Department department;

    /**
     * Конструктор события. Принимает обновлённый отдел.
     *
     * @param department обновлённый отдел.
     */
    public UpdateDepartmentRequestSequenceEvent(Department department) {
        this.department = department;
    }

    /**
     * Возвращает обновлённый отдел.
     *
     * @return обновлённый отдел.
     */
    public Department getDepartment() {
        return department;
    }
}


package ru.loanpro.security.service;

import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Сервис для работы с временем, часовыми поясами и сдвигами времени пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@VaadinSessionScope
public class ChronometerService {

    private final ConfigurationService configurationService;

    private ZoneId currentZoneId;
    private Boolean isTimeShifted = false;
    private Duration timeShift = Duration.ZERO;
    private Boolean isTimeFrozen = false;
    private ZonedDateTime frozenDateTime;

    /**
     * Конструктор, принимающий сервис конфигурации. Устанавливает текущий часовой
     * пояс.
     *
     * @param configurationService сервис конфигурации.
     */
    @Autowired
    public ChronometerService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
        currentZoneId = configurationService.getZoneId();
    }

    /**
     * Обновляет текущую информацию о часовом поясе.
     */
    public void update() {
        currentZoneId = configurationService.getZoneId();
    }

    /**
     * Устанавливает смещение текущего времени сессии пользователя. Получает на
     * вход требуемое для установки время и маркер, должно ли быть данное время
     * заморожено.
     *
     * @param targetDateTime время, устанавливаемое как основное.
     *
     * @param isTimeFrozen должно ли переданное время быть заморожено.
     */
    public void setShiftDateTime(LocalDateTime targetDateTime, boolean isTimeFrozen) {
        ZonedDateTime newDateTime = ZonedDateTime.of(targetDateTime, getCurrentZoneId());

        this.isTimeShifted = true;
        this.timeShift = Duration.between(getSystemTime(), newDateTime);
        this.isTimeFrozen = isTimeFrozen;

        if (isTimeFrozen) {
            this.frozenDateTime = newDateTime;
        }
    }

    /**
     * Сбрасывает настроки смещения времени.
     */
    public void clearShiftDateTime() {
        isTimeShifted = false;
        timeShift = Duration.ZERO;
        isTimeFrozen = false;
    }

    /**
     * Преобразует уникальную точку на временной оси {@link Instant} в текущее
     * время с учётом часового пояса пользователя.
     *
     * @param instant метка времени.
     *
     * @return значение метки времени с учётом часового пояса.
     */
    public ZonedDateTime zonedDateTime(Instant instant) {
        return instant.atZone(currentZoneId);
    }

    /**
     * Преобразует уникальную точку на временной оси {@link Instant} в текущую
     * дату с учётом часового пояса пользователя и возвращает в виде строки,
     * отформатированной по-умолчанию.
     *
     * @param instant метка времени.
     *
     * @return строковое представление метки времени с учётом часового пояса
     *         и формата времени по-умолчанию.
     */
    public String zonedDateToString(Instant instant) {
        return zonedDateTimeToString(instant, DateTimeFormatter.ofPattern(configurationService.getDateFormat()));
    }

    /**
     * Преобразует уникальную точку на временной оси {@link Instant} в текущее
     * время с учётом часового пояса пользователя и возвращает в виде строки,
     * отформатированной по-умолчанию.
     *
     * @param instant метка времени.
     *
     * @return строковое представление метки времени с учётом часового пояса
     *         и формата времени по-умолчанию.
     */
    public String zonedDateTimeToString(Instant instant) {
        return zonedDateTimeToString(instant, DateTimeFormatter.ofPattern(configurationService.getDateFormat()));
    }

    /**
     * Преобразует уникальную точку на временной оси {@link Instant} в текущее
     * время с учётом часового пояса пользователя и возвращает в виде строки,
     * отформатированной переданным форматом даты {@link DateTimeFormatter}.
     *
     * @param instant метка времени.
     *
     * @return строковое представление метки времени с учётом часового пояса
     *         и формата даты.
     */
    public String zonedDateTimeToString(Instant instant, DateTimeFormatter formatter) {
        return zonedDateTime(instant).format(formatter);
    }

    /**
     * Возвращает текущее время системы с учётом сдвига, заморозки времени и
     * часового пояса пользователя.
     *
     * @return текущее время.
     */
    public ZonedDateTime getCurrentTime() {
        if (isTimeShifted) {
            if (isTimeFrozen) {
                return frozenDateTime;
            } else {
                return ZonedDateTime.now(currentZoneId).plus(timeShift.getSeconds(), ChronoUnit.SECONDS);
            }
        } else {
            return ZonedDateTime.now(currentZoneId);
        }
    }

    /**
     * Возвращает метку, сдвинуто ли время.
     *
     * @return сдвинуто ли время.
     */
    public Boolean isTimeShifted() {
        return isTimeShifted;
    }

    /**
     * Возвращает метку, заморожено ли время.
     *
     * @return заморожено ли время.
     */
    public Boolean isTimeFrozen() {
        return isTimeFrozen;
    }

    /**
     * Возвращает текущее время системы с учётом часового пояса пользователя, без
     * сдвигов и заморозки времени.
     *
     * @return текущее время системы.
     */
    public ZonedDateTime getSystemTime() {
        return ZonedDateTime.now(currentZoneId);
    }

    /**
     * Возвращает текущее время системы с учётом часового пояса пользователя, без
     * сдвигов и заморозки времени в виде строки, отформатированной по-умолчанию.
     *
     * @return текущее время системы с учётом часового пояса, отформатированное
     *         по-умолчанию.
     */
    public String getSystemTimeString() {
        return getSystemTime().format(DateTimeFormatter.ofPattern(configurationService.getDateFormat()));
    }

    /**
     * Возвращает текущее время системы с учётом часового пояса пользователя, без
     * сдвигов и заморозки времени в виде строки, отформатированной переданным
     * форматом даты {@link DateTimeFormatter}.
     *
     * @return текущее время системы с учётом часового пояса и формата даты.
     */
    public String getSystemTimeString(DateTimeFormatter formatter) {
        return getSystemTime().format(formatter);
    }

    /**
     * Возвращает текущую метку времени с учётом часового пояса пользователя, сдвига и
     * заморозки времени.
     *
     * @return текущая метка времени.
     */
    public Instant getCurrentInstant() {
        return getCurrentTime().toInstant();
    }

    /**
     * Возвращает текущую временную зону пользователя.
     *
     * @return временная зона.
     */
    public ZoneId getCurrentZoneId() {
        return currentZoneId;
    }

    /**
     * Возвращает список временных зон, отсортированных в порядке увеличения сдвига
     * часового пояса.
     *
     * @return список временных зон.
     */
    public List<ZoneId> getAllZoneIds() {
        Map<ZoneId, ZoneOffset> zones = new HashMap<>();
        Set<String> zoneList = ZoneId.getAvailableZoneIds();

        for (String zoneId : zoneList) {
            ZoneId zone = ZoneId.of(zoneId);
            ZonedDateTime zdt = ZonedDateTime.now(zone);
            ZoneOffset zos = zdt.getOffset();

            zones.put(zone, zos);
        }

        List<ZoneId> result = new LinkedList<>();

        zones.entrySet()
            .stream()
            .sorted(Map.Entry.<ZoneId, ZoneOffset>comparingByValue(new ZoneComparator()).reversed())
            .forEachOrdered(item -> result.add(item.getKey()));

        return result;
    }

    /**
     * Функция сравнения часовых поясов для сортировки.
     */
    private class ZoneComparator implements Comparator<ZoneId> {
        @Override
        public int compare(ZoneId zoneId1, ZoneId zoneId2) {
            LocalDateTime now = LocalDateTime.now();
            ZoneOffset offset1 = now.atZone(zoneId1).getOffset();
            ZoneOffset offset2 = now.atZone(zoneId2).getOffset();

            return offset1.compareTo(offset2);
        }
    }
}

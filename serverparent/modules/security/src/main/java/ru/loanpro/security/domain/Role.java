package ru.loanpro.security.domain;

import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.domain.AbstractIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Роль в системе.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "roles", schema = DatabaseSchema.SECURITY)
public class Role extends AbstractIdEntity {

    /**
     * Название роли.
     */
    @Column(name = "authority", nullable = false)
    private String authority;

    /**
     * Описание роли.
     */
    @Column(name = "description")
    private String description;

    public Role() {
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }

        Role role = (Role) o;

        if (authority != null ? !authority.equals(role.authority) : role.authority != null) {
            return false;
        }
        return description != null ? description.equals(role.description) : role.description == null;
    }

    @Override
    public int hashCode() {
        int result = authority != null ? authority.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}

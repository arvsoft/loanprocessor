package ru.loanpro.security.event;

import ru.loanpro.security.domain.Department;

/**
 * Событие обновления последовательности номеров займов отдела {@link Department}.
 *
 * @author Oleg Brizhevatikh
 */
public class UpdateDepartmentLoanSequenceEvent {

    private Department department;

    /**
     * Конструктор события. Принимает обновлённый отдел.
     *
     * @param department обновлённый отдел.
     */
    public UpdateDepartmentLoanSequenceEvent(Department department) {
        this.department = department;
    }

    /**
     * Возвращает обновлённый отдел.
     *
     * @return обновлённый отдел.
     */
    public Department getDepartment() {
        return department;
    }
}

package ru.loanpro.security.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.annotation.EntityName;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Пользователь системы.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.user")
@Table(name = "users", schema = DatabaseSchema.SECURITY)
public class User extends BaseSettingsEntity implements UserDetails {

    /**
     * Имя учётной записи.
     */
    @Column(name = "userName", nullable = false, unique = true)
    private String username;

    /**
     * MD5-хеш пароля пользователя.
     */
    @Column(name = "password", nullable = false)
    private String password;

    /**
     * Идентификатор, не истёк ли срок действия аккаунта пользователя.
     */
    @Column(name = "account_non_expired")
    private boolean accountNonExpired;

    /**
     * Идентификатор, не заблокирован ли аккаунт пользователя.
     */
    @Column(name = "account_non_locked")
    private boolean accountNonLocked;

    /**
     * Идентификатор, не истекли ли права доступа пользователя.
     */
    @Column(name = "credentials_non_expired")
    private boolean credentialsNonExpired;

    /**
     * Идентификатор, активен ли пользователь.
     */
    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "avatar")
    private String avatarFileName;

    /**
     * Связка с таблицей прав пользователя в отделах.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "user", orphanRemoval = true)
    private Set<Credentials> credentials = new HashSet<>();

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Stream.concat(
            credentials.stream()
                .flatMap(c -> c.getGroups().stream())
                .flatMap(g -> g.getRoles().stream())
                .map(Role::getAuthority),
            credentials.stream()
                .flatMap(c -> c.getRoles().stream())
                .map(Role::getAuthority))
            .distinct()
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toSet());
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setEnabled(boolean enable) {
        this.enabled = enable;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Set<Credentials> getCredentials() {
        return credentials;
    }

    public void setCredentials(Set<Credentials> credentials) {
        this.credentials = credentials;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAvatarFileName() {
        return avatarFileName;
    }

    public void setAvatarFileName(String avatarFileName) {
        this.avatarFileName = avatarFileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        User user = (User) o;
        return accountNonExpired == user.accountNonExpired &&
            accountNonLocked == user.accountNonLocked &&
            credentialsNonExpired == user.credentialsNonExpired &&
            enabled == user.enabled &&
            Objects.equals(username, user.username) &&
            Objects.equals(password, user.password) &&
            Objects.equals(credentials, user.credentials) &&
            Objects.equals(firstName, user.firstName) &&
            Objects.equals(lastName, user.lastName) &&
            Objects.equals(middleName, user.middleName) &&
            Objects.equals(avatarFileName, user.avatarFileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            super.hashCode(),
            username,
            password,
            accountNonExpired,
            accountNonLocked,
            credentialsNonExpired,
            enabled,
            credentials,
            firstName,
            lastName,
            middleName,
            avatarFileName);
    }
}

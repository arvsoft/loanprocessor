package ru.loanpro.security;

import com.vaadin.spring.boot.VaadinAutoConfiguration;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import ru.loanpro.eventbus.EventBusAutoConfiguration;

/**
 * Автоматическая конфигурация модуля Security.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
@AutoConfigureAfter({
    SecurityAutoConfiguration.class,
    VaadinAutoConfiguration.class,
    EventBusAutoConfiguration.class})
@EntityScan(basePackages = {"ru.loanpro.security.domain", "ru.loanpro.security.settings.domain"})
@EnableJpaRepositories(basePackages = {"ru.loanpro.security.repository", "ru.loanpro.security.settings.repository"})
@ComponentScan
public class VaadinSecurityAutoConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

package ru.loanpro.security.domain.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Hibernate-конвертер коллекции валют в текстовое представление для сохранения в базе данных.
 *
 * @author Oleg Brizhevatikh
 */
@Converter(autoApply = true)
public class CurrenciesConverter implements AttributeConverter<Set<Currency>, String> {

    /**
     * Преобразует коллекцию валют в строку.
     *
     * @param currencies коллекция валют.
     * @return строка.
     */
    @Override
    public String convertToDatabaseColumn(Set<Currency> currencies) {
        if (currencies == null || currencies.size() == 0)
            return null;
        else
            return currencies.stream().map(Currency::getCurrencyCode).collect(Collectors.joining(","));
    }

    /**
     * Преобразует строку в коллекцию валют.
     *
     * @param dbData строка.
     * @return коллекция валют.
     */
    @Override
    public Set<Currency> convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.equals(""))
            return new HashSet<>();
        else
            return Arrays.stream(dbData.split(",")).map(Currency::getInstance).collect(Collectors.toSet());
    }
}

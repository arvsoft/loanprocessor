package ru.loanpro.security.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.global.repository.AbstractIdEntityRepository;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Репозиторий сущности {@link Department}.
 *
 * @author Oleg Brizhevatikh
 */
public interface DepartmentRepository extends AbstractIdEntityRepository<Department> {
    /**
     * Возвращает список корневых отделов (отделов без предка).
     *
     * @return список корневых отделов.
     */
    @EntityGraph(attributePaths = {"director"})
    List<Department> findAllByParentIsNull();

    /**
     * Возвращает отдел по его идентификатору с инициализированными Lazy-полями.
     *
     * @param id идентификатор отдела.
     * @return отдел.
     */
    @Query("select d from Department d where d.id = ?1")
    @EntityGraph(attributePaths = {"parent", "director"})
    Department findOneFull(UUID id);

    /**
     * Возвращает руководителя отдела.
     *
     * @param department отдел.
     * @return руководитель отдела.
     */
    @Query("select u from Department d join d.director u where d = ?1")
    Optional<User> findOne(Department department);
}

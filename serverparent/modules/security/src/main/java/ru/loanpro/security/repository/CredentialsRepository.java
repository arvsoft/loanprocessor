package ru.loanpro.security.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.global.repository.AbstractIdEntityRepository;
import ru.loanpro.security.domain.Credentials;

/**
 * Репозиторий сущности {@link Credentials}.
 *
 * @author Oleg Brizhevatikh
 */
public interface CredentialsRepository extends AbstractIdEntityRepository<Credentials> {

    @Modifying
    @Query("update Credentials c set c.isDefault = ?2 where c = ?1")
    void setCredentialsDefault(Credentials credentials, Boolean value);
}

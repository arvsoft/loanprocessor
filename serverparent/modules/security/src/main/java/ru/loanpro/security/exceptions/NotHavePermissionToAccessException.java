package ru.loanpro.security.exceptions;

/**
 * Исключение, возникающее при попытке пользователя обратиться к подсистеме или отделу,
 * на которые у него нет прав.
 *
 * @author Oleg Brizhevatikh
 */
public class NotHavePermissionToAccessException extends Exception {
}

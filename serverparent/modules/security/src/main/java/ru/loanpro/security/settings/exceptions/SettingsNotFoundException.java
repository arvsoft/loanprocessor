package ru.loanpro.security.settings.exceptions;

/**
 * Исключение, возникающее при попытке поиска несуществующего элемента настроек системы.
 *
 * @author Oleg Brizhevatikh
 */
public class SettingsNotFoundException extends Exception {
}

package ru.loanpro.security.settings.service;

import com.google.common.collect.ImmutableList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.service.UserService;
import ru.loanpro.security.settings.domain.DepartmentSettings;
import ru.loanpro.security.settings.domain.SystemSettings;
import ru.loanpro.security.settings.domain.UserSettings;
import ru.loanpro.security.settings.exceptions.SettingsNotFoundException;
import ru.loanpro.security.settings.exceptions.SettingsUnsupportedTypeException;
import ru.loanpro.security.settings.repository.SystemSettingsRepository;

import java.math.BigDecimal;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class SettingsServiceTest {

    private static final UUID UUID_1 = UUID.fromString("00000000-0000-0000-0000-000000000001");
    private static final UUID UUID_2 = UUID.fromString("00000000-0000-0000-0000-000000000002");

    private static final String KEY_STRING = "stringValue";
    private static final String KEY_INT = "Key Int";
    private static final String KEY_DECIMAL = "Key Decimal";
    private static final String KEY_ENUM = "ru.loanpro.global.SystemLocale";
    private static final String KEY_USER_ENUM = "ru.loanpro.global.UiTheme";
    private static final String UNKNOWN_KEY = "unknown key";

    private User user1;
    private User user2;
    private Department department1;
    private Department department2;

    @Mock
    private SystemSettingsRepository systemSettingsRepository;

    @Mock
    private UserService userService;

    @Mock
    private DepartmentService departmentService;

    @InjectMocks
    private SettingsService service;

    @Before
    public void setUp() {
        user1 = new User();
        user1.setId(UUID_1);
        user2 = new User();
        user2.setId(UUID_2);
        Mockito.when(userService.findAllUsers()).thenReturn(ImmutableList.of(user1, user2));

        department1 = new Department();
        department1.setId(UUID_1);
        department2 = new Department();
        department2.setId(UUID_2);
        Mockito.when(departmentService.findAll()).thenReturn(ImmutableList.of(department1, department2));

        SystemSettings systemSettings1 = new SystemSettings();
        systemSettings1.setId(UUID.randomUUID());
        systemSettings1.setKey("url");
        systemSettings1.setValue("http://localhost:8080/");

        SystemSettings systemSettings2 = new SystemSettings();
        systemSettings2.setId(UUID.randomUUID());
        systemSettings2.setKey(KEY_INT);
        systemSettings2.setValue("20");

        SystemSettings systemSettings3 = new SystemSettings();
        systemSettings3.setId(UUID.randomUUID());
        systemSettings3.setKey(KEY_DECIMAL);
        systemSettings3.setValue("30.25");

        SystemSettings systemSettings4 = new SystemSettings();
        systemSettings4.setId(UUID.randomUUID());
        systemSettings4.setKey(KEY_ENUM);
        systemSettings4.setValue("RUSSIAN");

        UserSettings userSettings1 = new UserSettings();
        userSettings1.setId(UUID.randomUUID());
        userSettings1.setUser(user1);
        userSettings1.setKey(KEY_STRING);
        userSettings1.setValue("any string");

        UserSettings userSettings2 = new UserSettings();
        userSettings2.setId(UUID.randomUUID());
        userSettings2.setUser(user1);
        userSettings2.setKey(KEY_INT);
        userSettings2.setValue("21");

        UserSettings userSettings3 = new UserSettings();
        userSettings3.setId(UUID.randomUUID());
        userSettings3.setUser(user1);
        userSettings3.setKey(KEY_DECIMAL);
        userSettings3.setValue("33.25");

        UserSettings userSettings4 = new UserSettings();
        userSettings4.setId(UUID.randomUUID());
        userSettings4.setUser(user1);
        userSettings4.setKey(KEY_USER_ENUM);
        userSettings4.setValue("LIGHT");

        DepartmentSettings departmentSettings1 = new DepartmentSettings();
        departmentSettings1.setId(UUID.randomUUID());
        departmentSettings1.setDepartment(department1);
        departmentSettings1.setKey(KEY_INT);
        departmentSettings1.setValue("22");

        DepartmentSettings departmentSettings2 = new DepartmentSettings();
        departmentSettings2.setId(UUID.randomUUID());
        departmentSettings2.setDepartment(department1);
        departmentSettings2.setKey(KEY_INT);
        departmentSettings2.setValue("22");

        DepartmentSettings departmentSettings3 = new DepartmentSettings();
        departmentSettings3.setId(UUID.randomUUID());
        departmentSettings3.setDepartment(department1);
        departmentSettings3.setKey(KEY_DECIMAL);
        departmentSettings3.setValue("34.75");

        DepartmentSettings departmentSettings4 = new DepartmentSettings();
        departmentSettings4.setId(UUID.randomUUID());
        departmentSettings4.setDepartment(department1);
        departmentSettings4.setKey(KEY_ENUM);
        departmentSettings4.setValue("ENGLISH");

        Mockito.when(systemSettingsRepository.findAll()).thenReturn(ImmutableList.of(
            systemSettings1, systemSettings2, systemSettings3, systemSettings4,
            userSettings1, userSettings2, userSettings3, userSettings4,
            departmentSettings1, departmentSettings2, departmentSettings3, departmentSettings4));

        service.init();
    }

    @Test
    public void testGetInteger() throws Exception {
        Integer expected = 20;
        Integer actual = service.get(Integer.class, KEY_INT);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetDecimal() throws Exception {
        BigDecimal expected = new BigDecimal(30.25);
        BigDecimal actual = service.get(BigDecimal.class, KEY_DECIMAL);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetEnum() throws Exception {
        SystemLocale expected = SystemLocale.RUSSIAN;
        SystemLocale actual = service.get(SystemLocale.class, KEY_ENUM);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = SettingsNotFoundException.class)
    public void testGetSettingsNotFoundException() throws Exception {
        service.get(Integer.class, UNKNOWN_KEY);
    }

    @Test(expected = SettingsUnsupportedTypeException.class)
    public void testGetSettingsUnsupportedTypeException() throws Exception {
        service.get(Object.class, KEY_INT);
    }

    @Test
    public void testGetUserInteger() throws Exception {
        Integer expected = 21;
        Integer actual = service.get(user1, Integer.class, KEY_INT);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetUserDecimal() throws Exception {
        BigDecimal expected = new BigDecimal(33.25);
        BigDecimal actual = service.get(user1, BigDecimal.class, KEY_DECIMAL);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetUserEnum() throws Exception {
        UiTheme expected = UiTheme.LIGHT;
        UiTheme actual = service.get(user1, UiTheme.class, KEY_USER_ENUM);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = SettingsNotFoundException.class)
    public void testGetUserSettingsNotFoundException() throws Exception {

        service.get(user1, Integer.class, UNKNOWN_KEY);
    }

    @Test(expected = SettingsUnsupportedTypeException.class)
    public void testGetUserSettingsUnsupportedTypeException() throws Exception {
        service.get(user1, Object.class, KEY_INT);
    }

    @Test
    public void testGetDepartmentInteger() throws Exception {
        Integer expected = 22;
        Integer actual = service.get(department1, Integer.class, KEY_INT);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetDepartmentDecimal() throws Exception {
        BigDecimal expected = new BigDecimal(34.75);
        BigDecimal actual = service.get(department1, BigDecimal.class, KEY_DECIMAL);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetDepartmentEnum() throws Exception {
        SystemLocale expected = SystemLocale.ENGLISH;
        SystemLocale actual = service.get(department1, SystemLocale.class, KEY_ENUM);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = SettingsNotFoundException.class)
    public void testGetDepartmentSettingsNotFoundException() throws Exception {

        service.get(department1, Integer.class, UNKNOWN_KEY);
    }

    @Test(expected = SettingsUnsupportedTypeException.class)
    public void testGetDepartmentSettingsUnsupportedTypeException() throws Exception {
        service.get(department1, Object.class, KEY_INT);
    }

    @Test
    public void testSetString() throws Exception {
        String expected = "any string";

        SystemSettings settings = new SystemSettings();
        settings.setKey(UNKNOWN_KEY);
        settings.setValue(expected);

        Mockito.when(systemSettingsRepository.save(Mockito.refEq(settings)))
            .thenReturn(settings);

        service.set(UNKNOWN_KEY, expected);
        String actual = service.get(String.class, UNKNOWN_KEY);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testSetInteger() throws Exception {
        Integer expected = 100;

        SystemSettings settings = new SystemSettings();
        settings.setKey(UNKNOWN_KEY);
        settings.setValue(String.valueOf(expected));

        Mockito.when(systemSettingsRepository.save(Mockito.refEq(settings)))
            .thenReturn(settings);

        service.set(UNKNOWN_KEY, expected);
        Integer actual = service.get(Integer.class, UNKNOWN_KEY);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testSetEnum() throws Exception {
        SystemLocale expected = SystemLocale.ENGLISH;

        SystemSettings settings = new SystemSettings();
        settings.setKey(UNKNOWN_KEY);
        settings.setValue(expected.name());

        Mockito.when(systemSettingsRepository.save(Mockito.refEq(settings)))
            .thenReturn(settings);


        service.set(UNKNOWN_KEY, expected);
        SystemLocale actual = service.get(SystemLocale.class, UNKNOWN_KEY);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = SettingsUnsupportedTypeException.class)
    public void testSetEnumSettingsUnsupportedTypeException() throws Exception {
        service.set(UNKNOWN_KEY, new Object());
    }

    @Test
    public void testSetUserString() throws SettingsUnsupportedTypeException, SettingsNotFoundException {
        String expected = "any string";

        UserSettings settings = new UserSettings();
        settings.setUser(user1);
        settings.setKey(UNKNOWN_KEY);
        settings.setValue(expected);

        Mockito.when(systemSettingsRepository.save(Mockito.refEq(settings)))
            .thenReturn(settings);

        service.set(user1, UNKNOWN_KEY, expected);
        String actual = service.get(user1, String.class, UNKNOWN_KEY);

        Assert.assertEquals(expected, actual);
    }
}
package ru.loanpro.sequence.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("YEAR")
public class SequenceElementYear extends SequenceElement {
}

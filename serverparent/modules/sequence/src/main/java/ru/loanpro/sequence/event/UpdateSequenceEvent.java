package ru.loanpro.sequence.event;

import ru.loanpro.sequence.domain.Sequence;

/**
 * Событие обновления последовательности {@link Sequence}.
 *
 * @author Oleg Brizhevatikh
 */
public class UpdateSequenceEvent {

    private Sequence sequence;

    /**
     * Конструктор события. Принимает обновлённую последовательность.
     *
     * @param sequence обновлённая последовательность.
     */
    public UpdateSequenceEvent(Sequence sequence) {
        this.sequence = sequence;
    }

    /**
     * Возвращает обновлённую последовательность.
     *
     * @return обновлённая последоваетльность.
     */
    public Sequence getSequence() {
        return sequence;
    }
}

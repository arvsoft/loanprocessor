package ru.loanpro.sequence.domain;

import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.annotation.EntityName;
import ru.loanpro.global.domain.AbstractIdEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Сущность последовательностей номеров.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.sequence")
@Table(name = "sequences", schema = DatabaseSchema.CORE)
public class Sequence extends AbstractIdEntity {

    /**
     * Название последовательности.
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * Текущее значение последовательности. Хранит следующий выдаваемый номер.
     */
    @Column(name = "current_value", nullable = false)
    private Long currentValue = 1L;

    /**
     * Список элементов последовательности.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "sequence", orphanRemoval = true)
    private List<SequenceElement> elements = new LinkedList<>();

    public Sequence() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Long currentValue) {
        this.currentValue = currentValue;
    }

    public List<SequenceElement> getElements() {
        return elements;
    }

    public void setElements(List<SequenceElement> elements) {
        this.elements = elements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Sequence sequence = (Sequence) o;
        return Objects.equals(name, sequence.name) &&
            Objects.equals(currentValue, sequence.currentValue) &&
            Objects.equals(elements, sequence.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, currentValue, elements);
    }
}

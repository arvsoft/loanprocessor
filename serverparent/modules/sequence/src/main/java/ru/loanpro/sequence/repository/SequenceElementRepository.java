package ru.loanpro.sequence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.loanpro.sequence.domain.SequenceElement;

public interface SequenceElementRepository extends JpaRepository<SequenceElement, Long> {
}

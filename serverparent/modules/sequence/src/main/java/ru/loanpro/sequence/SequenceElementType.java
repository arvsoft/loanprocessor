package ru.loanpro.sequence;

import ru.loanpro.sequence.domain.SequenceElement;
import ru.loanpro.sequence.domain.SequenceElementConstant;
import ru.loanpro.sequence.domain.SequenceElementDay;
import ru.loanpro.sequence.domain.SequenceElementMonth;
import ru.loanpro.sequence.domain.SequenceElementNumber;
import ru.loanpro.sequence.domain.SequenceElementYear;

/**
 * Перечисление поддерживаемых типов элементов последовательности номеров.
 *
 * @author Oleg Brizhevatikh
 */
public enum SequenceElementType{

    NUMBER("sequence.element.number", SequenceElementNumber.class),
    CONSTANT("sequence.element.constant", SequenceElementConstant.class),
    CURRENT_YEAR("sequence.element.year", SequenceElementYear.class),
    CURRENT_MONTH("sequence.element.month", SequenceElementMonth.class),
    CURRENT_DAY("sequence.element.day", SequenceElementDay.class);

    private String name;

    private Class<? extends SequenceElement> aClass;

    SequenceElementType(String name, Class<? extends SequenceElement> aClass) {
        this.name = name;
        this.aClass = aClass;
    }

    public String getName() {
        return name;
    }

    public Class<? extends SequenceElement> getAClass() {
        return aClass;
    }
}

package ru.loanpro.sequence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.sequence.domain.Sequence;

import java.util.UUID;

/**
 * Репозиторий сущности {@link Sequence}
 *
 * @author Oleg Brizhevatikh
 */
public interface SequenceRepository extends JpaRepository<Sequence, UUID> {
    @Query("select s from Sequence s where s.id = ?1")
    Sequence getOneFull(UUID id);
}

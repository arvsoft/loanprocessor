package ru.loanpro.sequence.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("DAY")
public class SequenceElementDay extends SequenceElement {
}

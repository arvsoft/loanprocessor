package ru.loanpro.sequence.service;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.domain.SequenceElement;
import ru.loanpro.sequence.domain.SequenceElementConstant;
import ru.loanpro.sequence.domain.SequenceElementDay;
import ru.loanpro.sequence.domain.SequenceElementMonth;
import ru.loanpro.sequence.domain.SequenceElementNumber;
import ru.loanpro.sequence.domain.SequenceElementYear;
import ru.loanpro.sequence.event.CreateSequenceEvent;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.sequence.repository.SequenceRepository;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Сервис для работы с генерируемыми последовательностями номеров {@link Sequence}.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class SequenceService {

    private final SequenceRepository repository;
    private final ApplicationEventBus applicationEventBus;

    // Список всех последовательностей.
    private transient List<Sequence> sequenceList;
    // Отображение id последовательности на BiMap<Номер, Строковый номер>
    private transient Map<UUID, BiMap<Long, String>> draftMap;

    @Autowired
    public SequenceService(SequenceRepository repository, ApplicationEventBus applicationEventBus) {
        this.repository = repository;
        this.applicationEventBus = applicationEventBus;

        sequenceList = repository.findAll();
        draftMap = sequenceList.stream().collect(Collectors.toMap(Sequence::getId, v -> HashBiMap.create()));
    }

    /**
     * Сохраняет последовательность номеров вместе со всеми вложенными элементами последовательности.
     * При отсутствии идентификатора последовательности, создаёт новую последовательность.
     *
     * @param sequence последовательность номеров.
     *
     * @return сохранённая последовательность номеров.
     */
    @Transactional
    public Sequence save(Sequence sequence) {
        if (sequence.getId() != null) {
            sequenceList.stream()
                .filter(item -> item.getId().equals(sequence.getId()))
                .findFirst()
                .ifPresent(item -> sequenceList.remove(item));
            draftMap.remove(sequence.getId());
        }
        sequence.getElements().forEach(element -> element.setSequence(sequence));

        Object event = (sequence.getId() == null)
            ? new CreateSequenceEvent(sequence)
            : new UpdateSequenceEvent(sequence);

        Sequence savedSequence = repository.save(sequence);
        sequenceList.add(savedSequence);
        draftMap.put(savedSequence.getId(), HashBiMap.create());
        applicationEventBus.post(event);

        return savedSequence;
    }

    /**
     * Возвращает последовательность по UUID.
     *
     * @return последовательность.
     */
    @Transactional
    public Sequence getOneFull(UUID uuid) {
        return repository.getOneFull(uuid);
    }

    /**
     * Возвращает список всех последоваетельностей номеров системы.
     *
     * @return список последовательностей.
     */
    @Transactional
    public List<Sequence> findAll() {
        return repository.findAll();
    }

    /**
     * Возвращает черновик номера из указанной последовательности. Сохраняет черновик,
     * чтобы он не был выдан повторно до изменения текущего номера последовательности.
     *
     * @param sequence последовательность номеров.
     *
     * @return черновик номера.
     */
    public synchronized String getDraftNumber(Sequence sequence) {
        Sequence currentSequence = getCurrentSequence(sequence);
        Long currentLong = currentSequence.getCurrentValue();

        Set<Long> draftLongSet = draftMap.get(currentSequence.getId()).keySet();

        while (draftLongSet.contains(currentLong)) {
            currentLong++;
        }

        String value = generateSequenceValue(currentSequence.getElements(), currentLong);
        draftMap.get(currentSequence.getId()).put(currentLong, value);

        return value;
    }

    /**
     * Удаляет черновик номера из списка выданных ранее, чтобы его можно было выдать вновь.
     *
     * @param sequence последовательность номеров.
     *
     * @param draftNumber черновик номера.
     */
    public synchronized void clearDraftNumber(Sequence sequence, String draftNumber) {
        Sequence currentSequence = getCurrentSequence(sequence);
        BiMap<String, Long> targetDraftMap = draftMap.get(currentSequence.getId()).inverse();
        targetDraftMap.remove(draftNumber);
    }

    /**
     * Актуализирует текущий номер последовательности номеров путём сдвига текущего номера
     * на единицу, обновления последовательности в БД и в списке последовательностей.
     * Все черновики данной последовательности стираются.
     *
     * @param sequence последовательность номеров.
     *
     * @return номер, сгенерированный по текущему номеру последовательности.
     */
    public synchronized String actualizeNextNumber(Sequence sequence) {
        Sequence currentSequence = getCurrentSequence(sequence);
        Long actualLong = currentSequence.getCurrentValue();
        currentSequence.setCurrentValue(actualLong + 1);
        repository.save(currentSequence);

        draftMap.get(currentSequence.getId()).clear();

        return generateSequenceValue(currentSequence.getElements(), actualLong);
    }

    /**
     * Сдвигает на единицу назад текущий номер последовательности номеров.
     *
     * @param sequence последовательность номеров.
     */
    public void resetLastNumber(Sequence sequence) {
        Sequence currentSequence = getCurrentSequence(sequence);
        currentSequence.setCurrentValue(currentSequence.getCurrentValue() - 1);
    }

    /**
     * Генерирует пример последовательности номеров.
     *
     * @param startValue начальный номер последовательности.
     *
     * @param sequences список элементов последовательности.
     *
     * @return список номеров.
     */
    public List<String> generateExampleValues(Long startValue, List<? extends SequenceElement> sequences) {
        List<String> result = new ArrayList<>();

        for (long i = startValue; i <= startValue + 200; i++) {
            result.add(generateSequenceValue(sequences, i));
        }

        return result;
    }

    /**
     * Генерирует номер из списка элементов последовательности и значения номера.
     *
     * @param sequences Список элементов последовательности.
     *
     * @param value значение номера.
     *
     * @return номер.
     */
    private String generateSequenceValue(List<? extends SequenceElement> sequences, Long value){
        return sequences.stream().map(s -> {
            if (s instanceof SequenceElementYear)
                return String.valueOf(ZonedDateTime.now().getYear());

            if (s instanceof SequenceElementMonth)
                return String.valueOf(ZonedDateTime.now().getMonthValue());

            if (s instanceof SequenceElementDay)
                return String.valueOf(ZonedDateTime.now().getDayOfMonth());

            if (s instanceof SequenceElementConstant)
                return ((SequenceElementConstant) s).getValue();

            if (s instanceof SequenceElementNumber) {
                SequenceElementNumber number = (SequenceElementNumber) s;

                if (number.isFixed()) {
                    return String.format("%0" + number.getLength() + "d", value);
                } else {
                    return String.valueOf(value);
                }
            }

            return "";
        }).collect(Collectors.joining());
    }

    /**
     * Возвращает последовательность номеров из списка текущих последовательностей
     * по переданной извне последовательности.
     *
     * @param sequence последовательность номеров.
     *
     * @return текущая последовательность номеров.
     */
    private Sequence getCurrentSequence(Sequence sequence) {
        return sequenceList
            .stream()
            .filter(item -> item.getId().equals(sequence.getId()))
            .findFirst()
            .get();
    }
}

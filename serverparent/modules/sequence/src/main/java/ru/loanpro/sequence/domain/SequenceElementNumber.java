package ru.loanpro.sequence.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("NUMBER")
public class SequenceElementNumber extends SequenceElement {

    @Column(name = "number_length_fixed")
    private boolean fixed;

    @Column(name = "number_length")
    private int length = 5;

    public SequenceElementNumber() {
    }

    public boolean isFixed() {
        return fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}

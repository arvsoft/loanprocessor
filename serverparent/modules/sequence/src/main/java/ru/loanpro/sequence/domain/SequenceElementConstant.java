package ru.loanpro.sequence.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CONSTANT")
public class SequenceElementConstant extends SequenceElement {

    @Column(name = "constant_value")
    private String value = "-";

    public SequenceElementConstant() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

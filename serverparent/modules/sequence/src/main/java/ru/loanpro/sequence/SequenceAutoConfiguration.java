package ru.loanpro.sequence;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import ru.loanpro.eventbus.EventBusAutoConfiguration;

/**
 * Автоматическая конфигурация модуля Sequence.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
@AutoConfigureAfter(EventBusAutoConfiguration.class)
@EntityScan(basePackages = "ru.loanpro.sequence.domain")
@EnableJpaRepositories(basePackages = "ru.loanpro.sequence.repository")
@ComponentScan(basePackages = "ru.loanpro.sequence.service")
public class SequenceAutoConfiguration {}

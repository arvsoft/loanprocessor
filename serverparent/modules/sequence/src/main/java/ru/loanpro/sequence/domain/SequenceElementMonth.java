package ru.loanpro.sequence.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("MONTH")
public class SequenceElementMonth extends SequenceElement {
}

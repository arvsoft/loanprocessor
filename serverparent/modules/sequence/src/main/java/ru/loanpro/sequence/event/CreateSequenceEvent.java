package ru.loanpro.sequence.event;

import ru.loanpro.sequence.domain.Sequence;

/**
 * Событие создания последовательности {@link Sequence}.
 *
 * @author Oleg Brizhevatikh
 */
public class CreateSequenceEvent {

    private Sequence sequence;

    /**
     * Конструктор события. Принимает созданную последовательность.
     *
     * @param sequence созданная последовательность.
     */
    public CreateSequenceEvent(Sequence sequence) {
        this.sequence = sequence;
    }

    /**
     * Возвращает созданную последовательность.
     *
     * @return созданная последоваетльность.
     */
    public Sequence getSequence() {
        return sequence;
    }
}

package ru.loanpro.base.unload.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.security.domain.Department;

import java.time.Instant;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

@Repository
public interface UnloadCashBookRepository extends JpaRepository<Operation, UUID> {

    /**
     * Получаем список операций расхода за указанный период по операциям "перевод со счетОВ отдела (наличный и/или безналичный)"
     *
     * @param dateMin  - начальная дата операции
     * @param dateMax  - конечная дата операции
     * @param department - отдел, владелец счетов
     * @param currency - валюта операции
     * @return список операций
     */
    @Query("select o from Operation o join o.accountPayer p where o.status = ru.loanpro.global.directory.OperationStatus.PROCESSED and o.operationDateTime >= ?1 and o.operationDateTime <= ?2 and p.department = ?3 and p.cashAccount = true and o.currency = ?4")
    @EntityGraph(attributePaths = {"department"})
    List<Operation> getExpenseOperationsForDepartment(Instant dateMin, Instant dateMax, Department department, Currency currency);

    /**
     * Получаем список операций прихода за указанный период по операциям "перевод на счетА отдела (наличный и/или безналичный)"
     * @param dateMin  - начальная дата операции
     * @param dateMax  - конечная дата операции
     * @param department - отдел, владелец счетов
     * @param currency - валюта операции
     * @return список операций
     */
    @Query("select o from Operation o join o.accountRecipient p where o.status = ru.loanpro.global.directory.OperationStatus.PROCESSED and o.operationDateTime >= ?1 and o.operationDateTime <= ?2 and p.department = ?3  and p.cashAccount = true and o.currency = ?4")
    @EntityGraph(attributePaths = {"department"})
    List<Operation> getIncomeOperationsForDepartment(Instant dateMin, Instant dateMax, Department department, Currency currency);

    @Query(value = "SELECT * FROM OPERATION.OPERATIONS O, OPERATION.ACCOUNTS A WHERE O.OPERATION_DATETIME >= ?1 AND O.OPERATION_DATETIME <= ?2 AND ((O.ACCOUNT_PAYER_ID=A.ID AND A.DEPARTMENT_ID = ?3) OR (O.ACCOUNT_RECIPIENT_ID=A.ID AND A.DEPARTMENT_ID=?3)) AND O.STATUS = 'PROCESSED' AND O.CURRENCY = ?4 AND (O.PAYMENT_METHOD = 'CASH_ISSUE' OR O.PAYMENT_METHOD = 'CASH_REPLENISH') ORDER BY O.OPERATION_DATETIME", nativeQuery = true)
    List<Operation> getOperationsForDepartment(Instant dateMin, Instant dateMax, Department department, Currency currency);
}

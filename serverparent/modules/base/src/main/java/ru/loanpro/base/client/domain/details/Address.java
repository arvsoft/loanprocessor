package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.AddressUseStatus;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * Адрес
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "address", schema = DatabaseSchema.CLIENT)
public class Address extends AbstractIdEntity {

    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Статус адреса.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private AddressUseStatus status;

    /**
     * Страна.
     */
    @Column(name = "country")
    private String country;

    /**
     * Индекс
     */
    @Column(name = "zip")
    private String zip;

    /**
     * Штат/регион.
     */
    @Column(name = "state")
    private String state;

    /**
     * County/Район
     */
    @Column(name = "county")
    private String county;

    /**
     * City/Город
     */
    @Column(name = "city")
    private String city;

    /**
     * Borough/Район
     */
    @Column(name = "borough")
    private String borough;

    /**
     * Street/Улица
     */
    @Column(name = "street")
    private String street;

    /**
     * House/Дом
     */
    @Column(name = "house")
    private String house;

    /**
     * Block/Блок
     */
    @Column(name = "block")
    private String block;

    /**
     * Appartment/Квартира
     */
    @Column(name = "apartment")
    private String appartment;

    /**
     * Дата жительства/регистрации.
     */
    @Column(name = "reside_date")
    private LocalDate resideDate;

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "address_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "address_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public Address() {
    }

    public String getAll() {
        return String.format("%s %s %s %s %s %s %s %s %s %s", zip, country, state, county, city, borough, street,
            house, block, appartment).replace("null", "").replaceAll("( +)", " ").trim();
    }

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public AddressUseStatus getStatus() {
        return status;
    }

    public void setStatus(AddressUseStatus status) {
        this.status = status;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getAppartment() {
        return appartment;
    }

    public void setAppartment(String appartment) {
        this.appartment = appartment;
    }

    public LocalDate getResideDate() {
        return resideDate;
    }

    public void setResideDate(LocalDate resideDate) {
        this.resideDate = resideDate;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Address address = (Address) o;
        return
            Objects.equals(status, address.status) &&
                Objects.equals(country, address.country) &&
                Objects.equals(zip, address.zip) &&
                Objects.equals(state, address.state) &&
                Objects.equals(county, address.county) &&
                Objects.equals(city, address.city) &&
                Objects.equals(borough, address.borough) &&
                Objects.equals(street, address.street) &&
                Objects.equals(house, address.house) &&
                Objects.equals(block, address.block) &&
                Objects.equals(appartment, address.appartment) &&
                Objects.equals(resideDate, address.resideDate) &&
                Objects.equals(scans, address.scans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), status, country, zip, state, county, city, borough, street,
            house, block, appartment, resideDate, scans);
    }
}

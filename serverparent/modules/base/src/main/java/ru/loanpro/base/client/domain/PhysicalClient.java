package ru.loanpro.base.client.domain;

import com.google.common.base.Objects;
import org.hibernate.Hibernate;
import ru.loanpro.global.directory.Gender;
import ru.loanpro.global.annotation.EntityName;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDate;

/**
 * Сущность физического лица.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.physicalClient")
@DiscriminatorValue("PHYSICAL")
public class PhysicalClient extends Client {

    /**
     * Подробная информация о физическом лице.
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "physical_client_details_id")
    private PhysicalClientDetails details;

    /**
     * Имя.
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * Фамилия.
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * Отчество.
     */
    @Column(name = "middle_name")
    private String middleName;

    /**
     * Дата рождения.
     */
    @Column(name = "birthdate")
    private LocalDate birthDate;

    /**
     * Пол.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    /**
     * Имя файла с фотографией клиента.
     */
    @Column(name = "photo_file_name")
    private String photoFileName;

    public PhysicalClient() {
    }

    /**
     * Метод удобного получения отображаемого имени для размещения на карточках или в списках.
     *
     * @return отображаемое имя.
     */
    @Override
    public String getDisplayName() {
        return String.format("%s %s %s",
            lastName == null ? "" : lastName,
            firstName == null ? "" : firstName,
            middleName == null ? "" : middleName
        ).trim();
    }

    public PhysicalClientDetails getDetails() {
        return details;
    }

    public void setDetails(PhysicalClientDetails details) {
        this.details = details;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhotoFileName() {
        return photoFileName;
    }

    public void setPhotoFileName(String photoFileName) {
        this.photoFileName = photoFileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        PhysicalClient that = (PhysicalClient) o;
        boolean equal = Objects.equal(firstName, that.firstName) &&
            Objects.equal(lastName, that.lastName) &&
            Objects.equal(middleName, that.middleName) &&
            Objects.equal(birthDate, that.birthDate) &&
            Objects.equal(gender, that.gender) &&
            Objects.equal(photoFileName, that.photoFileName);

        if (Hibernate.isInitialized(details))
            equal = equal && Objects.equal(details, that.details);

        return equal;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
            super.hashCode(),
            firstName,
            lastName,
            middleName,
            birthDate,
            gender,
            Hibernate.isInitialized(details)
                ? details
                : null,
            photoFileName);
    }
}

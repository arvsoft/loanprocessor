package ru.loanpro.base.account.domain;

import ru.loanpro.base.client.domain.PhysicalClientDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;

/**
 * Сущность счета физического лица.
 *
 * @author Maksim Askhaev
 */
@Entity
@DiscriminatorValue("PHYSICAL")
public class PhysicalAccount extends BaseAccount {
    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "physical_client_details_id")
    private PhysicalClientDetails clientDetails;

    public PhysicalAccount() {
    }

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhysicalAccount)) return false;
        if (!super.equals(o)) return false;
        PhysicalAccount that = (PhysicalAccount) o;
        return Objects.equals(clientDetails, that.clientDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }
}

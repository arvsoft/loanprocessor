package ru.loanpro.base.client.domain;

import ru.loanpro.base.client.domain.details.Address;
import ru.loanpro.base.client.domain.details.Document;
import ru.loanpro.base.client.domain.details.Education;
import ru.loanpro.base.client.domain.details.Estate;
import ru.loanpro.base.client.domain.details.Job;
import ru.loanpro.base.client.domain.details.Note;
import ru.loanpro.base.client.domain.details.Relative;
import ru.loanpro.base.client.domain.details.Vehicle;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.directory.domain.MaritalStatus;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Подробная информация о физическом лице {@link PhysicalClient}.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "physical_client_details", schema = DatabaseSchema.CLIENT)
public class PhysicalClientDetails extends ClientDetails {

    /**
     * Место рождения.
     */
    @Column(name = "birthplace", length = 512)
    private String birthPlace;

    /**
     * Семейное положение.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "marital_status_id")
    private MaritalStatus maritalStatus;

    /**
     * Количество детей.
     */
    @Column(name = "kids")
    private Integer kids;

    /**
     * Мобильный
     */
    @Column(name = "mobile_phone")
    private String mobilePhone;

    /**
     * Домашний
     */
    @Column(name = "home_phone")
    private String homePhone;

    /**
     * Рабочий
     */
    @Column(name = "work_phone")
    private String workPhone;

    /**
     * Email
     */
    @Column(name = "email")
    private String email;

    /**
     * Social security number
     */
    @Column(name = "ssn_number")
    private String ssnNumber;

    /**
     * ИНН.
     */
    @Column(name = "tax_number")
    private String taxNumber;

    /**
     * Чистый годовой доход.
     */
    @Column(name = "year_income")
    private BigDecimal yearIncome;

    /**
     * Коллекция документов физического лица.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Document> documents = new HashSet<>();

    /**
     * Коллекция адресов физического лица.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Address> addresses = new HashSet<>();

    /**
     * Коллекция оконченных образовательных учреждений (уровень образования)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Education> educations = new HashSet<>();

    /**
     * Коллекция Job (трудовая биография)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Job> jobs = new HashSet<>();

    /**
     * Коллекция Estate (недвижимость)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Estate> estates = new HashSet<>();

    /**
     * Коллекция Vehicle (транспортные средства)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Vehicle> vehicles = new HashSet<>();

    /**
     * Коллекция Relative (список знакомых/родственников)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Relative> relatives = new HashSet<>();

    /**
     * Коллекция Note (заметок)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<Note> notes = new HashSet<>();

    /**
     * Коллекция Account (счетов физического лица)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<PhysicalAccount> accounts = new HashSet<>();

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "physical_client_details_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "physical_client_details_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public PhysicalClientDetails() {
    }

    public String getSsnNumber() {
        return ssnNumber;
    }

    public void setSsnNumber(String ssnNumber) {
        this.ssnNumber = ssnNumber;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Integer getKids() {
        return kids;
    }

    public void setKids(Integer kids) {
        this.kids = kids;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getSSNNumber() {
        return ssnNumber;
    }

    public void setSSNNumber(String ssnNumber) {
        this.ssnNumber = ssnNumber;
    }

    public BigDecimal getYearIncome() {
        return yearIncome;
    }

    public void setYearIncome(BigDecimal yearIncome) {
        this.yearIncome = yearIncome;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public Set<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<Education> getEducations() {
        return educations;
    }

    public void setEducations(Set<Education> educations) {
        this.educations = educations;
    }

    public Set<Job> getJobs() {
        return jobs;
    }

    public void setJobs(Set<Job> jobs) {
        this.jobs = jobs;
    }

    public Set<Estate> getEstates() {
        return estates;
    }

    public void setEstates(Set<Estate> estates) {
        this.estates = estates;
    }

    public Set<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public Set<Relative> getRelatives() {
        return relatives;
    }

    public void setRelatives(Set<Relative> relatives) {
        this.relatives = relatives;
    }

    public Set<Note> getNotes() {
        return notes;
    }

    public void setNotes(Set<Note> notes) {
        this.notes = notes;
    }

    public Set<PhysicalAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<PhysicalAccount> accounts) {
        this.accounts = accounts;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        PhysicalClientDetails that = (PhysicalClientDetails) o;

        boolean equal = Objects.equals(kids, that.kids) &&
            Objects.equals(birthPlace, that.birthPlace) &&
            Objects.equals(maritalStatus, that.maritalStatus) &&
            Objects.equals(mobilePhone, that.mobilePhone) &&
            Objects.equals(homePhone, that.homePhone) &&
            Objects.equals(workPhone, that.workPhone) &&
            Objects.equals(email, that.email) &&
            Objects.equals(taxNumber, that.taxNumber) &&
            Objects.equals(ssnNumber, that.ssnNumber) &&
            Objects.equals(yearIncome, that.yearIncome) &&
            Objects.equals(documents, that.documents) &&
            Objects.equals(addresses, that.addresses) &&
            Objects.equals(educations, that.educations) &&
            Objects.equals(jobs, that.jobs) &&
            Objects.equals(estates, that.estates) &&
            Objects.equals(vehicles, that.vehicles) &&
            Objects.equals(relatives, that.relatives) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(accounts, that.accounts) &&
            Objects.equals(scans, that.scans);

        return equal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), birthPlace, maritalStatus, kids, mobilePhone, homePhone,
            workPhone, email, taxNumber, ssnNumber, yearIncome, documents, addresses, educations, jobs,
            estates, vehicles, relatives, notes, accounts, scans);
    }
}
package ru.loanpro.base.client.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.client.domain.JuridicalClient;

import java.util.List;

/**
 * Репозиторий сущности {@link JuridicalClient}.
 *
 * @author Oleg Brizhevatikh
 */
public interface JuridicalClientRepository extends ClientRepository<JuridicalClient> {

    /**
     * Метод получения отфильтрованного и отсортированного списка юридических лиц.
     * Фильтрация осуществляется по полному и сокращенному названиям юридического лица.
     *
     * @param filter строка фильтра.
     *
     * @param pageable параметры сортироваки и постраничной группировки результатов.
     *
     * @return список клентов.
     */
    @Query("select c from JuridicalClient c where c.fullName like %?1% or c.shortName like %?1%")
    List<JuridicalClient> findAllByQuery(String filter, Pageable pageable);

    /**
     * Метод получения количества юридических лиц, соответствующих фильтру.
     * Фильтрация осуществляется по ФИО клиента.
     *
     * @param filter строка фильтра
     *
     * @return количество клиентов соответствующих фильтру.
     */
    @Query("select count(c) from JuridicalClient c where c.fullName like %?1% or c.shortName like %?1%")
    int countFindAllByQuery(String filter);
}

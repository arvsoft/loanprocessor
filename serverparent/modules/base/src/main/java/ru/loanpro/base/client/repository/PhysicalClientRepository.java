package ru.loanpro.base.client.repository;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.Nullable;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientDetails;

import java.util.List;
import java.util.UUID;

/**
 * Репозиторий сущности {@link PhysicalClient}.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public interface PhysicalClientRepository extends ClientRepository<PhysicalClient> {

    /**
     * Возвращает одного физического клиента по идентификатору. Производит
     * инициализацию поля деталей клиента.
     *
     * @param id идентификатор клиента.
     * @return клиент.
     */
    @Query("select c from PhysicalClient c where c.id = ?1")
    @EntityGraph(attributePaths = {"details"})
    PhysicalClient getOneFull(UUID id);

    @EntityGraph(attributePaths = "details")
    @Override
    Page<PhysicalClient> findAll(@Nullable Predicate predicate, @Nullable Pageable pageable);

    /**
     * Возвращает клиента по его Details
     * @param details
     * @return
     */
    PhysicalClient findOneByDetails(PhysicalClientDetails details);

    /**
     * Метод получения отфильтрованного и отсортированного списка физических лиц.
     * Фильтрация осуществляется по ФИО клиента.
     *
     * @param filter строка фильтра.
     *
     * @param pageable параметры сортироваки и постраничной группировки результатов.
     *
     * @return список клентов.
     */
    @Query("select c from PhysicalClient c where c.firstName like %?1% or c.lastName like %?1% or c.middleName like %?1%")
    List<PhysicalClient> findAllByQuery(String filter, Pageable pageable);

    /**
     * Метод получения количества физических лиц, соответствующих фильтру.
     * Фильтрация осуществляется по ФИО клиента.
     *
     * @param filter строка фильтра
     *
     * @return количество клиентов соответствующих фильтру.
     */
    @Query("select count(c) from PhysicalClient c where c.firstName like %?1% or c.lastName like %?1% or c.middleName like %?1%")
    int countFindAllByQuery(String filter);
}

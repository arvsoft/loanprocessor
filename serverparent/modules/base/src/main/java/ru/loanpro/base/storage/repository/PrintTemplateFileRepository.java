package ru.loanpro.base.storage.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.base.storage.domain.PrintTemplateFile;

/**
 * Репозиторий сущности {@link PrintTemplateFile}
 *
 * @author Oleg Brizhevatikh
 */
public interface PrintTemplateFileRepository extends BaseStorageFileRepository<PrintTemplateFile> {

    @Query(value = "select t from PrintTemplateFile t where t.templateType = ?1 and t.isEnabled = true order by t.displayName")
    List<PrintTemplateFile> findAllByTemplateType(PrintDocumentType templateType);

    @Query(value = "select t from PrintTemplateFile t where t.templateType in ?1 and t.isEnabled = true order by t.displayName")
    List<PrintTemplateFile> findAllByTemplateType(PrintDocumentType... templateTypes);
}

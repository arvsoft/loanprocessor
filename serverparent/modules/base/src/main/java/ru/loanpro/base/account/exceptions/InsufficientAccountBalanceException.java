package ru.loanpro.base.account.exceptions;

public class InsufficientAccountBalanceException extends Exception {
    public InsufficientAccountBalanceException(String message) {
        super(message);
    }
}

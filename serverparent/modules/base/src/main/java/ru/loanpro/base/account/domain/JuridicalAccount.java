package ru.loanpro.base.account.domain;

import ru.loanpro.base.client.domain.JuridicalClientDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;

/**
 * Сущность счета юридического лица.
 *
 * @author Maksim Askhaev
 */
@Entity
@DiscriminatorValue("JURIDICAL")
public class JuridicalAccount extends BaseAccount {
    /**
     * Ссылка на подробную информацию об юридическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "juridical_client_details_id")
    private JuridicalClientDetails clientDetails;

    public JuridicalAccount() {
    }

    public JuridicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(JuridicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JuridicalAccount)) return false;
        if (!super.equals(o)) return false;
        JuridicalAccount that = (JuridicalAccount) o;
        return Objects.equals(clientDetails, that.clientDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }
}

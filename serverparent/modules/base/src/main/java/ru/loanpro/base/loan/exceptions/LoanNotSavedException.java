package ru.loanpro.base.loan.exceptions;

/**
 * Исключение, возникающее при попытке выполнить сохранение займа в сервисе сохранения заявки в рамках транзакции
 *
 * @author Oleg Brizhevatikh
 */
public class LoanNotSavedException extends BaseLoanNotSavedException {
    public LoanNotSavedException(Exception e) {
        super(e);
    }
}

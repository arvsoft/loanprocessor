package ru.loanpro.base.operation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.account.exceptions.AccountNotFoundException;
import ru.loanpro.base.account.exceptions.InsufficientAccountBalanceException;
import ru.loanpro.base.account.service.DepartmentAccountService;
import ru.loanpro.base.account.service.PhysicalAccountService;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.operation.exceptions.OperationNotSavedException;
import ru.loanpro.base.operation.repository.OperationRepository;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.global.directory.OperationStatus;
import ru.loanpro.global.directory.PaymentMethod;
import ru.loanpro.global.service.AbstractIdEntityService;
import ru.loanpro.security.domain.Department;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.sequence.service.SequenceService;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

@Service
public class OperationService extends AbstractIdEntityService<Operation, OperationRepository> {

    private final DepartmentAccountService departmentAccountService;
    private final PhysicalAccountService physicalAccountService;
    private final SequenceService sequenceService;
    private final ApplicationEventBus applicationEventBus;

    @Autowired
    public OperationService(OperationRepository repository, DepartmentAccountService departmentAccountService,
                            PhysicalAccountService physicalAccountService, SequenceService sequenceService,
                            ApplicationEventBus applicationEventBus) {
        super(repository);
        this.departmentAccountService = departmentAccountService;
        this.physicalAccountService = physicalAccountService;
        this.sequenceService = sequenceService;
        this.applicationEventBus = applicationEventBus;
    }

    public Operation getOne(Operation operation){
        return repository.getOne(operation.getId());
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteOne(Operation operation){
        repository.delete(operation);
    }

    public List<Operation> saveAll(List<Operation> operations) {
        return repository.saveAll(operations);
    }

    /**
     * Сохраняет операцию в подготовительном состоянии
     * Проверка, изменение баланса не выполняется
     *
     * @param operation операция
     */
    @Transactional(rollbackFor = Exception.class)
    public Operation savePrepared(Operation operation, Instant instant) throws OperationNotSavedException {
        try {
            operation.setStatus(OperationStatus.PREPARED);
            operation.setOperationDateTime(instant);
            return repository.save(operation);
        } catch (Exception e) {
            e.printStackTrace();
            throw new OperationNotSavedException("operation.message.operationSavedException");
        }
    }

    /**
     * Сохраняет операцию, обновляет состояние счетов с проверкой баланса плательщика
     *
     * @param operation операция
     */
    @Transactional(rollbackFor = Exception.class)
    public synchronized Operation save(Operation operation, Instant instant)
        throws InsufficientAccountBalanceException, OperationNotSavedException {
        boolean additionalOperation = false;

        try {
            BigDecimal operationValue = operation.getOperationValue();
            BigDecimal payerBalance;
            BigDecimal recipientBalance;

            PaymentMethod paymentMethod = operation.getPaymentMethod();
            if (paymentMethod == PaymentMethod.CASH_REPLENISH) {
                if (operation.getAccountRecipient().getClass() == DepartmentAccount.class) {
                    recipientBalance = departmentAccountService.getBalanceForAccount(operation.getAccountRecipient());
                } else if (operation.getAccountRecipient().getClass() == PhysicalAccount.class) {
                    recipientBalance = physicalAccountService.getBalanceForAccount(operation.getAccountRecipient());
                } else recipientBalance = new BigDecimal(0);

                recipientBalance = recipientBalance.add(operationValue);
                UUID id = operation.getAccountRecipient().getId();
                departmentAccountService.updateAccountBalance(id, recipientBalance);
            } else {
                if (operation.getAccountPayer().getClass() == DepartmentAccount.class) {
                    payerBalance = departmentAccountService.getBalanceForAccount(operation.getAccountPayer());
                } else if (operation.getAccountPayer().getClass() == PhysicalAccount.class) {
                    payerBalance = physicalAccountService.getBalanceForAccount(operation.getAccountPayer());
                } else payerBalance = new BigDecimal(0);

                if (payerBalance.compareTo(operationValue) < 0) {
                    throw new InsufficientAccountBalanceException("operation.message.insufficientAccountBalanceException");
                }
                if (paymentMethod == PaymentMethod.CASH_ISSUE) {
                    payerBalance = payerBalance.subtract(operationValue);
                    departmentAccountService.updateAccountBalance(operation.getAccountPayer().getId(), payerBalance);
                } else {
                    recipientBalance = operation.getAccountRecipient().getBalance();

                    payerBalance = payerBalance.subtract(operationValue);
                    recipientBalance = recipientBalance.add(operationValue);
                    departmentAccountService.updateAccountBalance(operation.getAccountPayer().getId(), payerBalance);
                    departmentAccountService.updateAccountBalance(operation.getAccountRecipient().getId(), recipientBalance);
                }
            }
            operation.setStatus(OperationStatus.PROCESSED);
            operation.setOperationDateTime(instant);

            //проверяем нужно ли выполнить дополнительную операцию
            if ((paymentMethod == PaymentMethod.CASH_REPLENISH &&
                operation.getAccountRecipient().getClass() == PhysicalAccount.class) ||
                (paymentMethod == PaymentMethod.CASH_ISSUE &&
                    operation.getAccountPayer().getClass() == PhysicalAccount.class)) {
                saveAdditionalOperation(operation);
                additionalOperation = true;
            }

            String number = sequenceService.actualizeNextNumber(operation.getDepartment().getOperationSequence());
            operation.setOperationNumber(number);

            Operation operation1 = repository.save(operation);

            // Если всё сохранилось корректно - оповещаем систему об обновлении последовательности
            applicationEventBus.post(new UpdateSequenceEvent(operation.getDepartment().getOperationSequence()));

            return operation1;

        } catch (InsufficientAccountBalanceException e) {
            sequenceService.resetLastNumber(operation.getDepartment().getOperationSequence());
            throw new InsufficientAccountBalanceException(e.getMessage());
        } catch (Exception e) {
            sequenceService.resetLastNumber(operation.getDepartment().getOperationSequence());
            if (additionalOperation) sequenceService.resetLastNumber(operation.getDepartment().getOperationSequence());
            throw new OperationNotSavedException("operation.message.operationSavedException");
        }
    }

    /**
     * Сохраняет дополнительную операцию взноса/выдачи наличных на/с счета-кассы отдела
     * при выполнении операции взноса/выдачи наличных на/с счета клиента
     * Выполняется только если исходная операция сохраняется в статусе PROCESSED
     *
     * @param operation - сущность исходной операции
     * @return
     */
    public Operation saveAdditionalOperation(Operation operation) throws AccountNotFoundException {

        DepartmentAccount account = departmentAccountService.getAccounts(operation.getDepartment()).stream()
            .filter(item -> item.getCurrency() == operation.getCurrency() && item.isCashAccount())
            .findFirst().orElseThrow(() -> new AccountNotFoundException("exception.message.departmentCashAccountNotFound"));

        Operation operation1 = new Operation();
        operation1.setUser(operation.getUser());
        operation1.setStatus(OperationStatus.PROCESSED);
        operation1.setOperationDateTime(operation.getOperationDateTime());
        operation1.setPaymentMethod(operation.getPaymentMethod());
        operation1.setPaymentPart(operation.getPaymentPart());
        operation1.setOperationValue(operation.getOperationValue());
        operation1.setPayment(operation.getPayment());
        operation1.setLoan(operation.getLoan());
        operation1.setCurrency(operation.getCurrency());
        operation1.setDepartment(operation.getDepartment());
        operation1.setNotes(null);
        operation1.setPayerFixed(operation.isPayerFixed());
        operation1.setRecipientFixed(operation.isRecipientFixed());

        if (operation.getPaymentMethod() == PaymentMethod.CASH_REPLENISH) {
            operation1.setPayer(operation.getPayer());
            operation1.setAccountPayer(operation.getAccountPayer());
            operation1.setRecipient(null);
            operation1.setAccountRecipient(account);

            BigDecimal balance = departmentAccountService.getBalanceForAccount(account);
            balance = balance.add(operation.getOperationValue());
            departmentAccountService.updateAccountBalance(account.getId(), balance);
        }
        if (operation.getPaymentMethod() == PaymentMethod.CASH_ISSUE) {
            operation1.setPayer(null);
            operation1.setAccountPayer(account);
            operation1.setRecipient(operation.getRecipient());
            operation1.setAccountRecipient(operation.getAccountRecipient());

            BigDecimal balance = departmentAccountService.getBalanceForAccount(account);
            balance = balance.subtract(operation.getOperationValue());
            departmentAccountService.updateAccountBalance(account.getId(), balance);
        }

        String number = sequenceService.actualizeNextNumber(operation.getDepartment().getOperationSequence());
        operation1.setOperationNumber(number);

        return repository.save(operation1);
    }

    @Transactional
    public List<Operation> getExpenseOperations(Instant dateMin, Instant dateMax, Department department, Currency currency) {
        return repository.getExpenseOperations(dateMin, dateMax, department, currency);
    }

    @Transactional
    public List<Operation> getIncomeOperations(Instant dateMin, Instant dateMax, Department department, Currency currency) {
        return repository.getIncomeOperations(dateMin, dateMax, department, currency);
    }

    @Transactional
    public List<Operation> getExpenseOperationsForPeriod(Instant dateMin, Instant dateMax, Department department) {
        return repository.getExpenseOperationsForPeriod(dateMin, dateMax, department);
    }

    @Transactional
    public List<Operation> getIncomeOperationsForPeriod(Instant dateMin, Instant dateMax, Department department) {
        return repository.getIncomeOperationsForPeriod(dateMin, dateMax, department);
    }

    public Instant getMinDateFromOperations(Department department, Currency currency) {
        return repository.getMinDateFromOperations(department, currency);
    }

    public Instant getMaxDateFromOperations(Department department, Currency currency) {
        return repository.getMaxDateFromOperations(department, currency);
    }
}

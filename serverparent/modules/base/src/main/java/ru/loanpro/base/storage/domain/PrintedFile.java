package ru.loanpro.base.storage.domain;

import javax.persistence.MappedSuperclass;

/**
 * Абстрактный класс распечатанного документа.
 *
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public abstract class PrintedFile extends BaseStorageFile {

    public PrintedFile() {
    }
}

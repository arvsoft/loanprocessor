package ru.loanpro.base.loan.domain;

import ru.loanpro.global.annotation.EntityName;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Класс сущности займа. Расширяет абстрактный класс займа.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.loan")
@DiscriminatorValue("LOAN")
public class Loan extends AbstractLoan {

    /**
     * Конструктор по-умолчанию.
     */
    public Loan() {
    }

    /**
     * Конструктор, принимающий экземпляр абстрактного займа {@link AbstractLoan} для
     * заполнения своих полей.
     *
     * @param loan заявка.
     */
    public Loan(AbstractLoan loan) {
        super(loan);
    }

    /**
     * Конструктор, принимающий экземпляр абстрактной заявки {@link AbstractRequest} для
     * заполнения своих полей.
     *
     * @param request заявка.
     */
    public Loan(AbstractRequest request) {
        super(request);
    }

    /**
     * Конструктор, принимающий экземпляр абстрактного кредитного продукта {@link AbstractLoanProduct}
     * для заполнения своих полей.
     *
     * @param loanProduct кредитный продукт.
     */
    public Loan(AbstractLoanProduct loanProduct) {
        super(loanProduct);
    }
}

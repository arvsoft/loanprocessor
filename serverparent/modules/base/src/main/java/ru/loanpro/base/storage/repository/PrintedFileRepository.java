package ru.loanpro.base.storage.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.base.storage.domain.connector.ClientPrintedFile;
import ru.loanpro.base.storage.domain.connector.LoanPrintedFile;
import ru.loanpro.base.storage.domain.connector.OperationPrintedFile;
import ru.loanpro.base.storage.domain.connector.PaymentPrintedFile;

/**
 * Репозиторий сущности {@link PrintedFile}
 *
 * @author Oleg Brizhevatikh
 */
public interface PrintedFileRepository extends BaseStorageFileRepository<PrintedFile> {

    /**
     * Возвращает список документов, распечатанных по клиенту.
     *
     * @param client клиент.
     * @return список распечатанных документов.
     */
    @Query(value = "select f from ClientPrintedFile f where f.client = ?1 order by f.dateTime desc")
    List<ClientPrintedFile> findAllClientPrintedFiles(Client client);

    /**
     * Возвращает список документов, распечатанных по займу.
     *
     * @param loan займ.
     * @return список распечатанных документов.
     */
    @Query(value = "select f from LoanPrintedFile f where f.loan = ?1 order by f.dateTime desc")
    List<LoanPrintedFile> findAllLoanPrintedFiles(Loan loan);

    /**
     * Возвращает список документов, распечатанных по платежу.
     *
     * @param payment платеж.
     * @return список распечатанных документов.
     */
    @Query(value = "select f from PaymentPrintedFile f where f.payment = ?1 order by f.dateTime desc")
    List<PaymentPrintedFile> findAllPaymentPrintedFiles(Payment payment);

    /**
     * Возвращает список документов, распечатанных по операции.
     *
     * @param operation операция.
     * @return список распечатанных документов.
     */
    @Query(value = "select f from OperationPrintedFile f where f.operation = ?1 order by f.dateTime desc")
    List<OperationPrintedFile> findAllOperationPrintedFiles(Operation operation);
}

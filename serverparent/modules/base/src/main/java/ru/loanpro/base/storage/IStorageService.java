package ru.loanpro.base.storage;

import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import ru.loanpro.base.storage.exception.FileSystemErrorException;

/**
 * Базовый интерфейс для работы с внешними хранилищами файлов.
 *
 * @author Oleg Brizhevatikh
 */
public interface IStorageService {
    /**
     * Возвращает аватар пользователя в фомате {@link Resource}. Если аватар не найден
     * или произошла ошибка файловой системы, возвращается стандартный аватар пользователя.
     *
     * @param avatarName название файла аватара пользователя.
     * @return аватар пользователя.
     */
    Resource getAvatar(String avatarName);

    /**
     * Сохраняет аватар из {@link ByteArrayOutputStream}.
     *
     * @param bas поток содержащий аватар пользователя.
     * @return {@link FileResource} сохранённого аватара пользователя.
     * @throws FileSystemErrorException если произошла ошибка сохранения аватара.
     */
    FileResource saveAvatar(ByteArrayOutputStream bas) throws FileSystemErrorException;

    /**
     * Возвращает сканированный образ документа по его имени.
     *
     * @param scannedName имя сканированного образа.
     * @return {@link FileResource} сканированного образа.
     * @throws FileSystemErrorException если произошла ошибка загрузки сканированного образа.
     */
    FileResource getScannedFile(String scannedName) throws FileSystemErrorException;

    /**
     * Возвращает сканированный образ документа по его имени. Если образ не найден или произошла
     * ошибка файловой системы, возвращается стандартный аватар пользователя.
     *
     * @param scannedName имя сканированного образа.
     * @return {@link Resource} сканированного образа.
     */
    Resource getScannedFileOrDefault(String scannedName);

    /**
     * Сохраняет сканированный образ документа по его имени из {@link ByteArrayOutputStream}.
     *
     * @param fileName оригинальное имя сканированного образа документа.
     * @param bas поток содержащий сканированный образ документа.
     * @return {@link FileResource} сохранённого сканированного образа документа.
     * @throws FileSystemErrorException если произошла ошибка сохранения сканированного образа документа.
     */
    FileResource saveScannedFile(String fileName, ByteArrayOutputStream bas) throws FileSystemErrorException;

    /**
     * Создаёт в хранилище и возвращает новый файл для записи в него печатаемого
     * документа.
     *
     * @return созданный для печати файл.
     */
    File getNewFileForPrint();

    /**
     * Возвращает сохранённый документ в виде {@link InputStream}.
     *
     * @param fileName имя документа.
     * @return InputStream документа.
     * @throws FileNotFoundException если документ не был найден.
     */
    InputStream getPrintedFileStream(String fileName) throws FileNotFoundException;

    /**
     * Возвращает размер сохранённого документа в байтах.
     *
     * @param fileName имя документа.
     * @return размер документа в байтах.
     * @throws FileNotFoundException если документ не был найден.
     */
    long getPrintedFileLength(String fileName) throws FileNotFoundException;

    /**
     * Возвращает сканированный документ в виде {@link InputStream}
     * @param fileName имя скана.
     * @return InputStream скана.
     * @throws FileNotFoundException если скан не был найден.
     */
    InputStream getScanFileStream(String fileName) throws FileNotFoundException;

    /**
     * Возвращает размер сохранённого скана в байтах.
     *
     * @param fileName имя скана.
     * @return размер скана в байтах.
     * @throws FileNotFoundException если скан не был найден.
     */
    long getScanFileLength(String fileName) throws FileNotFoundException;

    /**
     * Сохраняет шаблон печати из {@link ByteArrayOutputStream}.
     *
     * @param bas поток содержащий шаблон печати.
     * @return {@link FileResource} сохранённого шаблона печати.
     * @throws FileSystemErrorException если произошла ошибка сохранения шаблона печати.
     */
    FileResource savePrintTemplate(ByteArrayOutputStream bas) throws FileSystemErrorException;

    /**
     * Возвращает шаблон документа для печати в виде {@link InputStream}
     *
     * @param fileName имя шаблона.
     * @return InputStream шаблона документа
     * @throws FileNotFoundException если шаблон не был найден.
     */
    InputStream getPrintTemplateStream(String fileName) throws FileNotFoundException;

    /**
     * Возвращает шаблона докумета для печати в виде  {@link StreamResource}.
     *
     * @param fileName имя шаблона.
     * @return  StreamResource шаблона документа.
     * @throws FileNotFoundException если шаблон не был найден.
     */
    StreamResource getPrintTemplateStreamResource(String fileName) throws FileNotFoundException;
}

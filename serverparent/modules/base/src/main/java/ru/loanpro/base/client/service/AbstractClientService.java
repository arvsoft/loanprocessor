package ru.loanpro.base.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.repository.ClientRepository;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.global.service.AbstractIdEntityService;
import ru.loanpro.security.domain.Department;

import java.util.UUID;
import java.util.stream.Stream;

/**
 * Абстрактный сервис по работе с клиентами.
 *
 * @param <T> тип сущности клиента.
 * @param <R> тип репозитория клиента.
 * @author Oleg Brizhevatikh
 */
@NoRepositoryBean
public abstract class AbstractClientService<T extends Client, R extends ClientRepository<T>>
    extends AbstractIdEntityService<T, R> {

    @Autowired
    public AbstractClientService(R repository) {
        super(repository);
    }

    /**
     * Возвращает сущность клиента по идентификатору.
     *
     * @param id идентификатор сущности клиента.
     * @return клиент типа <code>T</code>.
     */
    @Transactional
    public T getOneFull(UUID id) {
        return repository.getOneFull(id);
    }

    /**
     * Сохраняет сущность клиента в базу данных.
     *
     * @param client сохраняемый клиент.
     * @return сохранённый клиент.
     */
    @Transactional
    public T save(T client) {
        return repository.save(client);
    }

    /**
     * Сохраняет сущность клиента в базу данных. Перегруженный вариант save
     *
     * @param client сохраняемый клиент.
     * @param d текущий отдел, здесь не используется, только в наследнике
     * @return сохранённый клиент.
     */
    @Transactional
    public T save(T client, Department d) {
        return repository.save(client);
    }

    /**
     * Метод вычисляющий количество клиентов в базе данных, совпадающих с переданным фильтром.
     *
     * @param filter фильтр клиентов.
     * @return количество клиентов, подходящих под фильтр.
     */
    public abstract Integer count(String filter);

    /**
     * Возвращает список клиентов, отфильтрованный и разбитый на страницы.
     *
     * @param filter фильтр клиента.
     * @param offset смещение.
     * @param limit требуемое количество клиентов.
     * @return список клиентов.
     */
    public abstract Stream<T> findAll(String filter, int offset, int limit);

    /**
     * Возвращает значение, можно ли удалить переданного клиента.
     *
     * @param client клиент.
     * @return можно ли удалить клиента.
     */
    public abstract Boolean canBeDeleted(T client);

    /**
     * Выполняет логическое удаление клиента.
     *
     * @param client удаляемый клиент.
     * @throws EntityDeleteException если не удалось удалить клиента.
     */
    public abstract void delete(T client) throws EntityDeleteException;
}

package ru.loanpro.base.client.domain;

import com.querydsl.core.types.Predicate;
import com.vaadin.data.HasValue;
import ru.loanpro.global.domain.BaseQueryDSL;

/**
 * Помощник для генерации предикатов, специфичных для сущности {@link Client}.
 *
 * @author Oleg Brizhevatikh
 */
public abstract class ClientQueryDSL extends BaseQueryDSL {

    /**
     * Возвращет предикат соответствия строкового поля полям сущности клиента, отображаемых группой.
     *
     * @param qClient клиент, для которого требуется проверка.
     * @param field   поле ввода строки.
     * @return предикат.
     */
    public static Predicate displayNameLike(QClient qClient, HasValue<String> field) {
        return PhysicalClientQueryDSL.fullNameLike(qClient.as(QPhysicalClient.class), field);
    }
}

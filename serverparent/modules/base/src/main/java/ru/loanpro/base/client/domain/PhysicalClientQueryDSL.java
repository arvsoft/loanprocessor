package ru.loanpro.base.client.domain;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.vaadin.data.HasValue;

/**
 * Помощник для генерации предикатов, специфичных для сущности {@link PhysicalClient}.
 *
 * @author Oleg Brizhevatikh
 */
public abstract class PhysicalClientQueryDSL extends ClientQueryDSL {

    /**
     * Возвращает предикат соответствия строкового поля полям сущности физического лица, отображаемых группой.
     *
     * @param client физическое лицо, для которого требуется проверка.
     * @param field  поле ввода строки.
     * @return предикат.
     */
    public static Predicate fullNameLike(QPhysicalClient client, HasValue<String> field) {
        String value = field.getValue();
        if (value == null || value.equals(""))
            return null;

        return new BooleanBuilder()
            .or(client.lastName.likeIgnoreCase("%" + value + "%"))
            .or(client.firstName.likeIgnoreCase("%" + value + "%"))
            .or(client.middleName.likeIgnoreCase("%" + value + "%"));
    }

    /**
     * Возвращает предикат соответствия строкового поля полям сущности физического лица, отображаемых группой.
     *
     * @param client физическое лицо, для которого требуется проверка.
     * @param field  поле ввода строки.
     * @return предикат.
     */
    public static Predicate fullDocumentLike(QPhysicalClient client, HasValue<String> field) {
        String value = field.getValue();
        if (value == null || value.equals(""))
            return null;

        return new BooleanBuilder()
            .or(client.details.documents.any().number.likeIgnoreCase("%" + value + "%"))
            .or(client.details.documents.any().series.likeIgnoreCase("%" + value + "%"));
    }

    /**
     * Возвращает предикат соответствия строкового поля полям сущности физического лица, отображаемых группой.
     *
     * @param client физическое лицо, для которого требуется проверка.
     * @param field  поле ввода строки.
     * @return предикат.
     */
    public static Predicate fullAddressLike(QPhysicalClient client, HasValue<String> field) {
        String value = field.getValue();
        if (value == null || value.equals(""))
            return null;

        return new BooleanBuilder()
            .or(client.details.addresses.any().street.likeIgnoreCase("%" + value + "%"))
            .or(client.details.addresses.any().borough.likeIgnoreCase("%" + value + "%"))
            .or(client.details.addresses.any().city.likeIgnoreCase("%" + value + "%"))
            .or(client.details.addresses.any().house.likeIgnoreCase("%" + value + "%"))
            .or(client.details.addresses.any().zip.likeIgnoreCase("%" + value + "%"));
    }

    /**
     * Возвращает предикат быстрого поиска физического лица по строке.
     *
     * @param filter фильтр.
     * @return предикат.
     */
    public static Predicate quickSearch(String filter) {
        if (filter == null || filter.equals(""))
            return null;

        String value = "%" + filter + "%";

        QPhysicalClient client = QPhysicalClient.physicalClient;
        QPhysicalClientDetails details = client.details;

        return new BooleanBuilder()
            .or(client.lastName.likeIgnoreCase(value))
            .or(client.firstName.likeIgnoreCase(value))
            .or(client.middleName.likeIgnoreCase(value))
            .or(details.ssnNumber.like(value))
            .or(details.taxNumber.like(value))
            .getValue();
    }
}

package ru.loanpro.base.operation.domain;

import java.math.BigDecimal;
import java.util.Currency;

public class Balance {
    private Currency currency;
    private BigDecimal balance;
    private BigDecimal income;
    private BigDecimal expense;

    public Balance() {
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getExpense() {
        return expense;
    }

    public void setExpense(BigDecimal expense) {
        this.expense = expense;
    }
}


package ru.loanpro.base.schedule.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.directory.ScheduleStatus;

import java.time.LocalDate;
import java.util.List;

/**
 * Репозиторий сущности {@link Schedule}
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    List<Schedule> deleteAllByLoan(BaseLoan baseLoan);

    List<Schedule> findAllByLoan(BaseLoan baseLoan);

    /**
     * Возвращает список платежей, принадлежащих займам со статусом {@link ScheduleStatus#NOT_PAID_NO_OVERDUE} и с датой
     * платежа меньше переданной даты.
     *
     * @param date дата, относительно которой платежи считаются просроченными.
     * @return список просроченных платежей.
     */
    @Query("select s from Schedule s where s.date < ?1 and type(s.loan.class) = Loan and s.status = ru.loanpro.global.directory.ScheduleStatus.NOT_PAID_NO_OVERDUE")
    @EntityGraph(attributePaths = "loan")
    List<Schedule> findOverdueSchedules(LocalDate date);

    /**
     * Возвращает список платежей, принадлежащих займу со статусами платежей {@link ScheduleStatus#NOT_PAID_NO_OVERDUE}
     * и датами, меньшими входной даты - date, т.е. все вновь обнаруженные просроченные платежи.
     *
     * @param date дата, относительно которой платежи считаются просроченными.
     * @param loan заем, для которого будет выполнен поиск
     * @return список просроченных платежей.
     */
    @Query("select s from Schedule s where s.date < ?1 and type(s.loan.class) = Loan and s.loan = ?2 and s.status = ru.loanpro.global.directory.ScheduleStatus.NOT_PAID_NO_OVERDUE")
    @EntityGraph(attributePaths = "loan")
    List<Schedule> findOverdueSchedulesByLoan(LocalDate date, Loan loan);

    /**
     * Возвращает список просроченных платежей {@link ScheduleStatus#NOT_PAID_OVERDUE}
     * но датами, большими входной даты - date,
     * т.е. платежей-кандидатов на снятие статуса просрочен
     *
     * @param date дата, относительно которой платежи считаются просроченными.
     * @param loan заем, для которого будет выполнен поиск
     * @return список уже просроченных платежей-кандидатов
     */
    @Query("select s from Schedule s where s.date >= ?1 and type(s.loan.class) = Loan and s.loan = ?2 and s.status = ru.loanpro.global.directory.ScheduleStatus.NOT_PAID_OVERDUE")
    @EntityGraph(attributePaths = "loan")
    List<Schedule> findNoOverdueSchedulesByLoan(LocalDate date, Loan loan);
}

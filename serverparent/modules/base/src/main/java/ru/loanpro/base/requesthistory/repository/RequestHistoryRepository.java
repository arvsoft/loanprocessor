package ru.loanpro.base.requesthistory.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.requesthistory.domain.RequestHistory;

import java.util.List;

public interface RequestHistoryRepository extends JpaRepository<RequestHistory, Long> {

    @EntityGraph(attributePaths = {"user", "loan"})
    List<RequestHistory> findAllByRequest(Request request);

}

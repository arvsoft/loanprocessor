package ru.loanpro.base.loan.repository;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.Nullable;
import ru.loanpro.base.loan.domain.Loan;

import java.util.Optional;
import java.util.UUID;

/**
 * Репозиторий сущности {@link Loan}
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public interface LoanRepository extends BaseLoanRepository<Loan> {
    /**
     * Возвращает один займ по идентификатору. Производит инициализацию полей созаёмщика и
     * поручителей в запросе.
     *
     * @param id идентификатор займа.
     * @return займ.
     */
    @Query("select l from Loan l where l.id = ?1")
    @EntityGraph(attributePaths = {"coBorrower", "guarantors"})
    Loan getOneFull(UUID id);

    /**
     * Устанавливает свойство поля IsProlonged в TRUE для займа по его Id
     *
     * @param id - id займа.
     * @return заем.
     */
    @Modifying
    @Query("update Loan l set l.isProlonged = true where l.id = ?1")
    void setIsProlongedToTrueById(UUID id);

    /**
     * {@inheritDoc}
     */
    @EntityGraph(attributePaths = "borrower")
    @Override
    Page<Loan> findAll(@Nullable Predicate predicate, @Nullable Pageable pageable);

    /**
     * Возвращает предка займа по его пролонгированному наследнику.
     *
     * @param loan займ.
     * @return предок займа.
     */
    @Query("select p from Loan l join l.parent p where l = ?1")
    Optional<Loan> findByChild(Loan loan);
}


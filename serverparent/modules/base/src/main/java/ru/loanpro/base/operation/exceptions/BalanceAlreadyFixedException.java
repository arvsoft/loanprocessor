package ru.loanpro.base.operation.exceptions;

public class BalanceAlreadyFixedException extends Exception {
    public BalanceAlreadyFixedException(String message) {
        super(message);
    }
}

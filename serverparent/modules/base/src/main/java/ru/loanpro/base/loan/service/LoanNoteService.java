package ru.loanpro.base.loan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.base.loan.details.LoanNote;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.repository.LoanNoteRepository;

import java.util.List;

@Service
public class LoanNoteService {

    LoanNoteRepository repository;

    @Autowired
    public LoanNoteService(LoanNoteRepository loanNoteRepository) {
        this.repository = loanNoteRepository;
    }

    public List<LoanNote> findAllByLoan(Loan loan){
        List<LoanNote> list = repository.findAllByLoanOrderByNoteDateDesc(loan);
        return list;
    }

    @Transactional
    public LoanNote save(LoanNote loanNote){
        return repository.save(loanNote);
    }

    @Transactional
    public void delete(LoanNote loanNote) {
        repository.delete(loanNote);
    }
}

package ru.loanpro.base.schedule;

import ru.loanpro.base.schedule.domain.Schedule;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для расчета полной стоимости кредита (ПСК) согласно инструкции ЦБ РФ
 *
 * @author Maksim Askhaev
 */
public class CalculatorPSK {

    final private List<Schedule> scheduleList;
    final private BigDecimal loanBalance;
    final private LocalDate currentDate;

    public CalculatorPSK(BigDecimal loanBalance, LocalDate currentDate, List<Schedule> scheduleList) {
        this.loanBalance = loanBalance;
        this.currentDate = currentDate;
        this.scheduleList = scheduleList;
    }

    /**
     * Метод выполняет расчет полной стоимости кредита (ПСК)
     * Для расчет использует дату выдачи займа, сумму займа и график платежей
     *
     * @return Полную стоимость кредита в виде BigDecimal с точностью до 3-го знака после запятой.
     * Точность определена инструкией ЦБ РФ
     */
    public BigDecimal calculate() {
        LocalDate[] ar_date;//даты платежей по графику + дата выдачи
        double[] ar_sum;//сумма платежей, в 0-ой позиции стоит "минус" суммы займа
        long[] ar_days;//кол-во дней между датами текущих платежей и датой первого платежа
        double[] ar_E;//Ek - см. формулу ЦБ
        double[] ar_Q;//Qk - см. формулу ЦБ
        long[] ar_diff_days;//разница кол-ва дней
        ArrayList<Long> ar_temp;//период в днях
        ArrayList<Long> ar_temp_count;//кол-во таких периодов

        if (scheduleList.size() == 0) {
            return new BigDecimal(0).setScale(3, BigDecimal.ROUND_HALF_DOWN);
        }

        int m = scheduleList.size() + 1;//кол-во периодов, включая дату выдачи

        ar_date = new LocalDate[m];
        ar_sum = new double[m];
        ar_days = new long[m];
        ar_E = new double[m];
        ar_Q = new double[m];
        ar_diff_days = new long[m - 1];
        ar_temp = new ArrayList<>();
        ar_temp_count = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            if (i == 0) {
                ar_date[0] = currentDate;
                ar_sum[0] = -loanBalance.doubleValue();
                ar_days[i] = 0;
            } else {
                ar_date[i] = scheduleList.get(i - 1).getDate();
                ar_sum[i] = scheduleList.get(i - 1).getPayment().doubleValue();
                ar_days[i] = ChronoUnit.DAYS.between(currentDate, scheduleList.get(i - 1).getDate());
                ar_diff_days[i - 1] = ChronoUnit.DAYS.between(ar_date[i - 1], ar_date[i]);
            }
        }
        boolean f;
        for (int i = 0; i < m - 1; i++) {
            if (i == 0) {
                ar_temp.add(ar_diff_days[0]);
                ar_temp_count.add(1L);
            } else {
                f = false;

                for (int j = 0; j < ar_temp.size(); j++) {
                    if (ar_temp.get(j) == ar_diff_days[i]) {
                        ar_temp_count.set(j, ar_temp_count.get(j) + 1);
                        f = true;
                        break;
                    }
                }
                if (!f) {
                    ar_temp.add(ar_diff_days[i]);
                    ar_temp_count.add(1L);
                }
            }
        }
        //вычисляем, какой период чаще всего используется
        long count_max = 0;
        int count_max_i = 0;
        if (ar_temp.size() > 1) {
            for (int i = 0; i < ar_temp.size(); i++) {
                if (ar_temp_count.get(i) > count_max) {
                    count_max = ar_temp_count.get(i);
                    count_max_i = i;
                }
            }
        } else count_max_i = 0;

        if (ar_temp.size() > 1) {
            for (int i = 0; i < ar_temp.size(); i++) {
                if (i != count_max_i) {
                    if (ar_temp_count.get(i) == count_max) {
                        if (ar_temp.get(i) < ar_temp.get(count_max_i)) {
                            count_max_i = i;
                        }
                    }
                }
            }
        }
        //значение базового периода
        long bp = ar_temp.get(count_max_i);
        double cbp = 365 / (double) bp;
        //рассчитаем Ek и Qk для каждого платежа
        for (int i = 0; i < m; i++) {
            ar_E[i] = (ar_days[i] % bp) / (double) bp;
            ar_Q[i] = (ar_days[i] / bp);
        }
        double ix = 0;
        double x = 1;
        double xm = 0;
        double s = 0.0001;//
        //находим минимальный ix методом перебора, начиная с нуля до максимального приближения с шагом s
        while (x > 0) {
            xm = x;
            x = 0;
            for (int k = 0; k < m; k++) {
                x = x + ar_sum[k] / ((1 + ar_E[k] * ix) * Math.pow(1 + ix, ar_Q[k]));
            }
            ix = ix + s;
        }
        if (x > xm) {
            ix = ix - s;
        }

        return new BigDecimal(cbp * 100 * (new BigDecimal(ix).setScale(3, BigDecimal.ROUND_HALF_DOWN).doubleValue())).setScale(3, BigDecimal.ROUND_HALF_DOWN);
    }
}

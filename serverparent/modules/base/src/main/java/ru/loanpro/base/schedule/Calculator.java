package ru.loanpro.base.schedule;

import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.directory.ScheduleStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Класс Builder для расчета автоматического графика платежей.
 * <p>
 * Выполняет расчет аннуитетного и дифференцированного графиков платежей и
 * возвращает коллекцию платежей с начальным статусом
 * <p>
 * Передаваемые параметры должны быть согласованы
 * Правила согласования представлены в документации.
 *
 * @author Maksim Askhaev
 */
public class Calculator {

    private int scaleValue;             //точность для денежного типа
    private int scaleMode;              //режим округления
    private LocalDate loanIssueDate;    //дата выдачи займа
    private BigDecimal loanBalance;     //размер займа
    private CalcScheme calcScheme;      //тип расчета дифференцированный/аннуитетный
    private ReturnTerm returnTerm;      //тип срока возврата - ежемесячно/еженедельно/в конце срока/индивидуальный
    private BigDecimal interestRate;    //размер процентной ставки, например 5%
    private InterestType interestType;  //тип процентной ставки, %/месяц, % в неделю, % в день, % за весь период и пр.
    private int loanTerm;               //срок займа, например 5,
    private boolean replaceDate;        //идентификатор, смещать ли дату первого платежа на заданное число месяца, например, платеж будет 10 числа каждого месяца
    private int replaceDateValue;       //смещенная дата первого платежа, например 10-е число, если включен параметр ReplaceDate
    private boolean replaceOverMonth;   //идентификатор, смещать ли дату через один месяц, если срок первого периода меньше одного месяца,
    private boolean principalLastPay;   //идентификатор, включать ли оплату основной части полностью в последний платеж
    private boolean interestBalance;    //идентификатор, рассчитывать ли проценты от суммы займа, а не от остатка долга
    private boolean noInterestFD;       //идентификатор, не рассчитывать ли в первые дни проценты
    private int noInterestFDValue;      //кол-во дней в первом платеже без расчета процентов
    private boolean includeIssueDate;   //включать ли дату выдачи в расчет процентов
    private int daysInYear;             //кол-во дней в году, берется из настроек

    private Calculator() {
    }

    public static Calculator instance() {
        return new Calculator();
    }

    public Calculator setScaleValue(int scaleValue) {
        this.scaleValue = scaleValue;
        return this;
    }

    public Calculator setScaleMode(int scaleMode) {
        this.scaleMode = scaleMode;
        return this;
    }

    public Calculator setLoanIssueDate(LocalDate loanIssueDate) {
        this.loanIssueDate = loanIssueDate;
        return this;
    }

    public Calculator setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
        return this;
    }

    public Calculator setCalcScheme(CalcScheme calcScheme) {
        this.calcScheme = calcScheme;
        return this;
    }

    public Calculator setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
        return this;
    }

    public Calculator setInterestRateType(InterestType interestType) {
        this.interestType = interestType;
        return this;
    }

    public Calculator setLoanTerm(int loanTerm) {
        this.loanTerm = loanTerm;
        return this;
    }

    public Calculator setReturnTerm(ReturnTerm returnTerm) {
        this.returnTerm = returnTerm;
        return this;
    }

    public Calculator setReplaceDate(boolean replaceDate) {
        this.replaceDate = replaceDate;
        return this;
    }

    public Calculator setReplaceDateValue(int replaceDateValue) {
        this.replaceDateValue = replaceDateValue;
        return this;
    }

    public Calculator setReplaceOverMonth(boolean replaceOverMonth) {
        this.replaceOverMonth = replaceOverMonth;
        return this;
    }

    public Calculator setPrincipalLastPay(boolean principalLastPay) {
        this.principalLastPay = principalLastPay;
        return this;
    }

    public Calculator setInterestBalance(boolean interestBalance) {
        this.interestBalance = interestBalance;
        return this;
    }

    public Calculator setNoInterestFD(boolean noInterestFD) {
        this.noInterestFD = noInterestFD;
        return this;
    }

    public Calculator setNoInterestFDValue(int noInterestFDValue) {
        this.noInterestFDValue = noInterestFDValue;
        return this;
    }

    public Calculator setIncludeIssueDate(boolean includeIssueDate) {
        this.includeIssueDate = includeIssueDate;
        return this;
    }

    public Calculator setDaysInYear(int daysInYear) {
        this.daysInYear = daysInYear;
        return this;
    }

    public Calculator build() {
        return this;
    }

    /**
     * Метод выполняет расчет автоматического графика платежей
     * Следует вызывать для расчета графика.
     *
     * @return Коллекцию платежей с начальным статусом.
     */
    public List<Schedule> calculate() {
        switch (calcScheme) {
            case DIFFERENTIAL:
                return calculateDifferential();
            case ANNUITY:
                return calculateAnnuity();
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Приватный метод выполняет расчет дат платежей для автоматического графика платежей
     *
     * @return Коллекцию рассчитанных дат платежей
     */
    private List<LocalDate> calculateDates() {
        List<LocalDate> dates = new ArrayList<>();
        LocalDate firstPayDate = loanIssueDate;
        LocalDate tempDate;
        int firstDay, nextDay = 1;
        boolean b;

        if (returnTerm == ReturnTerm.MONTHLY) {//ежемесячно
            //определяем firstPayDate
            if (replaceDate) {//включена замена даты на указанное число
                if (replaceDateValue > 0 && replaceDateValue <= 28) {
                    if (replaceOverMonth) {//перенос через месяц включен
                        if (loanIssueDate.getDayOfMonth() < replaceDateValue) {
                            if (loanIssueDate.getMonthValue() == 12) {
                                firstPayDate = LocalDate.of(loanIssueDate.getYear() + 1, 1, replaceDateValue);
                            } else {
                                firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1, replaceDateValue);
                            }
                        } else {
                            if (loanIssueDate.getMonthValue() == 11) {
                                firstPayDate = LocalDate.of(loanIssueDate.getYear() + 1, 1, replaceDateValue);
                            }
                            if (loanIssueDate.getMonthValue() == 12) {
                                firstPayDate = LocalDate.of(loanIssueDate.getYear() + 1, 2, replaceDateValue);
                            }
                            if (loanIssueDate.getMonthValue() < 11) {
                                firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 2, replaceDateValue);
                            }
                        }
                    } else {//перенос через месяц отключен
                        if (loanIssueDate.getDayOfMonth() < replaceDateValue) {
                            firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue(),
                                replaceDateValue);
                        } else {
                            if (loanIssueDate.getMonthValue() == 12) {
                                firstPayDate = LocalDate.of(loanIssueDate.getYear() + 1, 1, replaceDateValue);
                            } else {
                                firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1,
                                    replaceDateValue);
                            }
                        }
                    }
                    b = false;
                } else b = true;
            } else b = true;

            if (b) {//замена даты на указанное число ОТКЛЮЧЕНА или дата не лежит в пределах от 1 до 28
                if (loanIssueDate.getMonthValue() == 1) {
                    if (loanIssueDate.getYear() % 4 == 0) {//високосный год
                        if (Arrays.asList(30, 31).contains(loanIssueDate.getDayOfMonth())) {
                            firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1, 29);
                        } else { //если день не 30, 31
                            firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1,
                                loanIssueDate.getDayOfMonth());
                        }
                    } else {//обычный год
                        if (Arrays.asList(29, 30, 31).contains(loanIssueDate.getDayOfMonth())) {
                            firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1, 28);
                        } else { //если день не 30, 31
                            firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1,
                                loanIssueDate.getDayOfMonth());
                        }
                    }
                }

                if (Arrays.asList(2, 4, 6, 7, 9, 11).contains(loanIssueDate.getMonthValue())) {
                    firstPayDate = LocalDate.of(loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1,
                        loanIssueDate.getDayOfMonth());
                }
                if (Arrays.asList(3, 5, 8, 10).contains(loanIssueDate.getMonthValue())) {
                    if (loanIssueDate.getDayOfMonth() == 31) {
                        firstPayDate = LocalDate.of(
                            loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1, loanIssueDate.getDayOfMonth() - 1);
                    } else {
                        firstPayDate = LocalDate.of(
                            loanIssueDate.getYear(), loanIssueDate.getMonthValue() + 1, loanIssueDate.getDayOfMonth());
                    }
                }
                if (loanIssueDate.getMonthValue() == 12) {
                    firstPayDate = LocalDate.of(loanIssueDate.getYear() + 1, 1, loanIssueDate.getDayOfMonth());
                }
            }
            dates.add(firstPayDate);
            //определяем остальные дни, включая lastPayDate - дату последнего платежа
            tempDate = firstPayDate;
            firstDay = firstPayDate.getDayOfMonth();
            for (int i = 1; i < loanTerm; i++) {
                if (tempDate.getMonthValue() == 1) {
                    if (tempDate.getYear() % 4 == 0) {//високосный год
                        if (Arrays.asList(30, 31).contains(tempDate.getDayOfMonth())) {
                            nextDay = 29;
                        } else nextDay = firstDay;
                    } else {//обычный год
                        if (Arrays.asList(29, 30, 31).contains(tempDate.getDayOfMonth())) {
                            nextDay = 28;
                        } else nextDay = firstDay;
                    }
                }
                if (Arrays.asList(2, 4, 6, 7, 9, 11, 12).contains(tempDate.getMonthValue())) {
                    nextDay = firstDay;
                }
                if (Arrays.asList(3, 5, 8, 10).contains(tempDate.getMonthValue())) {
                    if (firstDay == 31) {
                        nextDay = 30;
                    } else {
                        nextDay = firstDay;
                    }
                }
                //определяем дату
                if (tempDate.getMonthValue() + 1 <= 12) {
                    tempDate = LocalDate.of(tempDate.getYear(), tempDate.getMonthValue() + 1, nextDay);
                } else {
                    tempDate = LocalDate.of(tempDate.getYear() + 1, 1, nextDay);
                }
                dates.add(tempDate);
            }
        } else if (returnTerm == ReturnTerm.WEEKLY) {
            firstPayDate = loanIssueDate.plusDays(7);
            dates.add(firstPayDate);
            tempDate = firstPayDate;

            for (int i = 1; i < loanTerm; i++) {
                tempDate = tempDate.plusDays(7);
                dates.add(tempDate);
            }
        } else if (returnTerm == ReturnTerm.IN_THE_END_OF_TERM) {
            firstPayDate = loanIssueDate.plusDays(loanTerm);
            dates.add(firstPayDate);
        }
        return dates;
    }

    /**
     * Приватный метод выполняет расчет дифференцированного графика платежей.
     * Предварительно проверяет значения полей размера займа, процентной ставки и срока займа на 0
     *
     * @return Коллекцию платежей в виде графика платежей.
     */
    private List<Schedule> calculateDifferential() {
        double interestDay = 1;//процентная ставка в день
        double payBalance1;//размер платежа осн. части
        LocalDate currentDate;//текущая дата

        BigDecimal currentBalance;
        BigDecimal payBalance;

        List<Schedule> scheduleList = new ArrayList<>(loanTerm);

        if (loanTerm <= 0 || loanBalance.doubleValue() <= 0d || interestRate.doubleValue() <= 0)
            return scheduleList;

        List<LocalDate> dates = calculateDates();

        currentDate = loanIssueDate;
        payBalance1 = loanBalance.doubleValue() / loanTerm;

        currentBalance = new BigDecimal(loanBalance.doubleValue()).setScale(scaleValue, scaleMode);
        payBalance = new BigDecimal(payBalance1).setScale(scaleValue, scaleMode);

        if (returnTerm == ReturnTerm.MONTHLY || returnTerm == ReturnTerm.WEEKLY) {
            if (interestType == InterestType.PERCENT_A_YEAR) {
                interestDay = interestRate.doubleValue() / (daysInYear * 100d);
            }
            if (interestType == InterestType.PERCENT_A_MONTH || interestType == InterestType.PERCENT_A_WEEK) {
                interestDay = interestRate.doubleValue() / 100d;
            }

            for (int i = 0; i <= loanTerm - 1; i++) {
                Schedule schedulePayment = new Schedule();
                schedulePayment.setOrdNo(i + 1);
                schedulePayment.setStatus(ScheduleStatus.NOT_PAID_NO_OVERDUE);//1-not paid, no overdue
                schedulePayment.setDate(dates.get(i));
                long per = ChronoUnit.DAYS.between(currentDate, dates.get(i));
                schedulePayment.setTerm(per);

                if (interestType == InterestType.PERCENT_A_YEAR) {
                    if (!interestBalance) {//от суммы остатка
                        schedulePayment.setInterest(new BigDecimal(currentBalance.doubleValue() * per * interestDay)
                            .setScale(scaleValue, scaleMode));
                    } else {//от суммы займа
                        schedulePayment.setInterest(new BigDecimal(loanBalance.doubleValue() * per * interestDay)
                            .setScale(scaleValue, scaleMode));
                    }
                }

                if (interestType == InterestType.PERCENT_A_MONTH || interestType == InterestType.PERCENT_A_WEEK) {
                    if (!interestBalance) {//от суммы остатка
                        schedulePayment.setInterest(new BigDecimal(currentBalance.doubleValue() * interestDay)
                            .setScale(scaleValue, scaleMode));
                    } else {//от суммы займа
                        schedulePayment.setInterest(new BigDecimal(loanBalance.doubleValue() * interestDay)
                            .setScale(scaleValue, scaleMode));
                    }
                }

                if (principalLastPay) {
                    if (i < loanTerm - 1) {
                        schedulePayment.setPrincipal(new BigDecimal(0).setScale(scaleValue, scaleMode));
                        schedulePayment.setBalance(currentBalance);
                    } else {
                        schedulePayment.setPrincipal(currentBalance);
                        schedulePayment.setBalance(loanBalance.subtract(schedulePayment.getPrincipal()));
                    }
                } else {
                    if (i < loanTerm - 1) {
                        schedulePayment.setPrincipal(payBalance);
                        currentBalance = currentBalance.subtract(payBalance);
                        schedulePayment.setBalance(currentBalance);
                    } else {
                        schedulePayment.setPrincipal(currentBalance);
                        schedulePayment.setBalance(new BigDecimal(0).setScale(scaleValue, scaleMode));
                    }
                }
                schedulePayment.setPayment(schedulePayment.getPrincipal().add(schedulePayment.getInterest()));
                currentDate = schedulePayment.getDate();
                scheduleList.add(schedulePayment);
            }
        }
        if (returnTerm == ReturnTerm.IN_THE_END_OF_TERM) {
            Schedule schedulePayment = new Schedule();
            schedulePayment.setOrdNo(1);
            schedulePayment.setStatus(ScheduleStatus.NOT_PAID_NO_OVERDUE);//1-not paid, no overdue
            schedulePayment.setDate(dates.get(0));
            payBalance = currentBalance;
            long gAddingDay = 0;
            long gExcludeDay = 0;

            if (includeIssueDate) {
                gAddingDay = 1;
            }

            long per = ChronoUnit.DAYS.between(currentDate, dates.get(0));
            schedulePayment.setTerm(per + gAddingDay);

            if (noInterestFD) {
                if (noInterestFDValue < schedulePayment.getTerm()) {
                    gExcludeDay = noInterestFDValue;
                }
            }

            if (interestType == InterestType.PERCENT_A_YEAR) {//% в год
                interestDay = interestRate.doubleValue() / (daysInYear * 100d);
                schedulePayment.setInterest(new BigDecimal(loanBalance.doubleValue() * (schedulePayment.getTerm() - gExcludeDay) * interestDay)
                    .setScale(scaleValue, scaleMode));
            }

            if (interestType == InterestType.PERCENT_A_DAY) {//% в день
                interestDay = interestRate.doubleValue() / 100d;
                schedulePayment.setInterest(new BigDecimal(loanBalance.doubleValue() * (schedulePayment.getTerm() - gExcludeDay) * interestDay)
                    .setScale(scaleValue, scaleMode));
            }

            if (interestType == InterestType.PERCENT_A_WHOLE_PERIOD) {//% за весь период
                schedulePayment.setInterest(new BigDecimal(loanBalance.doubleValue() * interestRate.doubleValue() / 100d)
                    .setScale(scaleValue, scaleMode));
            }

            schedulePayment.setPrincipal(payBalance);
            schedulePayment.setPayment(schedulePayment.getPrincipal().add(schedulePayment.getInterest()).setScale(scaleValue, scaleMode));
            schedulePayment.setBalance(new BigDecimal(0).setScale(scaleValue, scaleMode));
            scheduleList.add(schedulePayment);
        }
        return scheduleList;
    }

    /**
     * Приватный метод выполняет расчет аннуитетного графика платежей
     *
     * @return Коллекцию платежей в виде графика платежей
     */
    private List<Schedule> calculateAnnuity() {
        double interestDay = 0;//процентная ставка в день
        double interestTerm = 0;//процентная ставка за период
        double currentBalance;//остаток долга
        double payTotal;//размер платежа (осн. часть + проценты)
        LocalDate currentDate;//текущая дата
        double extraInterest;//размер процентов за дополн. дни в первом платеже
        long extraDays = 0;
        double ca, cq;

        List<Schedule> scheduleList = new ArrayList<>(loanTerm);

        if (loanTerm <= 0 || loanBalance.doubleValue() <= 0d || interestRate.doubleValue() <= 0)
            return scheduleList;

        List<LocalDate> dates = calculateDates();

        currentBalance = loanBalance.doubleValue();
        currentDate = loanIssueDate;

        if (interestType == InterestType.PERCENT_A_YEAR) {
            interestTerm = interestRate.doubleValue() / 1200d;
        }
        if (interestType == InterestType.PERCENT_A_MONTH || interestType == InterestType.PERCENT_A_WEEK) {
            interestTerm = interestRate.doubleValue() / 100d;
        }

        cq = interestTerm + 1;
        ca = (cq - 1) / (1 - 1 / Math.pow(cq, loanTerm));
        payTotal = loanBalance.doubleValue() * ca;//размер аннуитетного платежа

        if (interestType == InterestType.PERCENT_A_YEAR) {
            interestDay = (interestRate.doubleValue() / (daysInYear * 100d));
        }
        if (interestType == InterestType.PERCENT_A_MONTH) {
            interestDay = interestRate.doubleValue() * 12 / (daysInYear * 100d);
        }
        if (interestType == InterestType.PERCENT_A_WEEK) {
            interestDay = interestRate.doubleValue() / 700d;
        }

        for (int i = 0; i <= loanTerm - 1; i++) {
            Schedule schedulePayment = new Schedule();
            schedulePayment.setOrdNo(i + 1);
            schedulePayment.setStatus(ScheduleStatus.NOT_PAID_NO_OVERDUE);
            schedulePayment.setDate(dates.get(i));
            long per = ChronoUnit.DAYS.between(currentDate, dates.get(i));
            schedulePayment.setTerm(per);
            schedulePayment.setPayment(new BigDecimal(payTotal).setScale(scaleValue, scaleMode));

            if (i == 0) {
                if (replaceDate && replaceOverMonth) {
                    if (Arrays.asList(1, 2, 4, 6, 8, 9, 11).contains(dates.get(i).getMonthValue())) {
                        extraDays = per - 31;
                    }
                    if (Arrays.asList(5, 7, 10, 12).contains(dates.get(i).getMonthValue())) {
                        extraDays = per - 30;
                    }
                    if (dates.get(i).getMonthValue() == 3) {
                        if (dates.get(i).getYear() % 4 == 0) {
                            extraDays = per - 29;
                        } else extraDays = per - 28;

                    }

                    extraInterest = currentBalance * interestDay * extraDays;
                    schedulePayment.setInterest(new BigDecimal(currentBalance * interestDay * (schedulePayment.getTerm() - extraDays)));
                    schedulePayment.setPrincipal(schedulePayment.getPayment().subtract(schedulePayment.getInterest())
                        .setScale(scaleValue, scaleMode));
                    schedulePayment.setInterest(schedulePayment.getInterest().add(new BigDecimal(extraInterest))
                        .setScale(scaleValue, scaleMode));
                    schedulePayment.setPayment(schedulePayment.getPayment().add(new BigDecimal(extraInterest))
                        .setScale(scaleValue, scaleMode));
                } else {
                    schedulePayment.setInterest(new BigDecimal(currentBalance * interestDay * schedulePayment.getTerm())
                        .setScale(scaleValue, scaleMode));
                    schedulePayment.setPrincipal(schedulePayment.getPayment().subtract(schedulePayment.getInterest())
                        .setScale(scaleValue, scaleMode));
                }
            } else {
                schedulePayment.setInterest(new BigDecimal(currentBalance * interestDay * schedulePayment.getTerm())
                    .setScale(scaleValue, scaleMode));
                schedulePayment.setPrincipal(schedulePayment.getPayment().subtract(schedulePayment.getInterest())
                    .setScale(scaleValue, scaleMode));
            }

            currentBalance = currentBalance - schedulePayment.getPrincipal().doubleValue();
            schedulePayment.setBalance(new BigDecimal(currentBalance).setScale(scaleValue, scaleMode));

            if (i == loanTerm - 1) {
                schedulePayment.setPrincipal(schedulePayment.getPrincipal().add(schedulePayment.getBalance())
                    .setScale(scaleValue, scaleMode));
                schedulePayment.setPayment(schedulePayment.getPayment().add(schedulePayment.getBalance())
                    .setScale(scaleValue, scaleMode));
                schedulePayment.setBalance(new BigDecimal(0)
                    .setScale(scaleValue, scaleMode));
            }
            currentDate = schedulePayment.getDate();
            scheduleList.add(schedulePayment);
        }

        return scheduleList;
    }
}

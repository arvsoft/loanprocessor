package ru.loanpro.base.loan.repository;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.Nullable;
import ru.loanpro.base.loan.domain.Request;

import java.util.UUID;

/**
 * Репозиторий сущности {@link Request}
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public interface RequestRepository extends BaseLoanRepository<Request> {
    /**
     * Возвращает одну заявку по идентификатору. Производит инициализацию полей созаёмщика и
     * поручителей в запросе.
     *
     * @param id идентификатор заявки.
     * @return заявка.
     */
    @Query("select r from Request r where r.id = ?1")
    @EntityGraph(attributePaths = {"coBorrower", "guarantors"})
    Request getOneFull(UUID id);

    /**
     * {@inheritDoc}
     */
    @EntityGraph(attributePaths = "borrower")
    @Override
    Page<Request> findAll(@Nullable Predicate predicate, @Nullable Pageable pageable);
}

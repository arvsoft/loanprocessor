package ru.loanpro.base.unload.domain;

import java.math.BigDecimal;

/**
 * Сущность кассовой книги
 * Используется для формирования выгрузки кассовой книги
 */
public class CashBook {

    private int ordNo;//порядковый номер
    private String documentNumber;//номер документа
    private String clientName;//от кого получено или кому выдано
    private String accountNumber;//номер счета или субсчета
    private BigDecimal income;//приход
    private BigDecimal expense;//расход

    public CashBook() {
    }

    public int getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(int ordNo) {
        this.ordNo = ordNo;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getExpense() {
        return expense;
    }

    public void setExpense(BigDecimal expense) {
        this.expense = expense;
    }
}

package ru.loanpro.base.report.domain.reports;

import ru.loanpro.global.directory.ClientRating;

import java.math.BigDecimal;

/**
 * Класс сущности отчета - Отчет о распределении статусов клиентов
 * Группа отчета - Клиенты.
 * Идентификатор отчета - CLIENTS.REPORT1
 *
 * @author Maksim Askhaev
 */
public class ClientReport1 {

    /**
     * Рейтинг клиента
     */
    private ClientRating rating;

    /**
     * Кол-во клиентов
     */
    private int number;

    /**
     * Кол-во мужчин
     */
    private int maleNumber;

    /**
     * Кол-во женщин
     */
    private int femaleNumber;
    /**
     * Кол-во другого пола
     */
    private int otherNumber;

    /**
     * Кол-во в возрасте до 25 лет включительно
     */
    private int number25;

    /**
     * Кол-во в возврасте от 26 до 35 лет включительно
     */
    private int number26_35;

    /**
     * Кол-во в возрасте от 36 до 50 лет включительно
     */
    private int number36_50;

    /**
     * Кол-во в возрасте от 51 и старше
     */
    private int number51;

    /**
     * Совокупный объем выданных займов
     */
    private BigDecimal sumLoanBalance;

    public ClientRating getRating() {
        return rating;
    }

    public void setRating(ClientRating rating) {
        this.rating = rating;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getMaleNumber() {
        return maleNumber;
    }

    public void setMaleNumber(int maleNumber) {
        this.maleNumber = maleNumber;
    }

    public int getFemaleNumber() {
        return femaleNumber;
    }

    public void setFemaleNumber(int femaleNumber) {
        this.femaleNumber = femaleNumber;
    }

    public int getOtherNumber() {
        return otherNumber;
    }

    public void setOtherNumber(int otherNumber) {
        this.otherNumber = otherNumber;
    }

    public int getNumber25() {
        return number25;
    }

    public void setNumber25(int number25) {
        this.number25 = number25;
    }

    public int getNumber26_35() {
        return number26_35;
    }

    public void setNumber26_35(int number26_35) {
        this.number26_35 = number26_35;
    }

    public int getNumber36_50() {
        return number36_50;
    }

    public void setNumber36_50(int number36_50) {
        this.number36_50 = number36_50;
    }

    public int getNumber51() {
        return number51;
    }

    public void setNumber51(int number51) {
        this.number51 = number51;
    }

    public BigDecimal getSumLoanBalance() {
        return sumLoanBalance;
    }

    public void setSumLoanBalance(BigDecimal sumLoanBalance) {
        this.sumLoanBalance = sumLoanBalance;
    }
}

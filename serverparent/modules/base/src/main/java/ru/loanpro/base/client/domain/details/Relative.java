package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.directory.domain.RelativeType;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

/**
 * Родственники
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "relative", schema = DatabaseSchema.CLIENT)
public class Relative extends AbstractIdEntity {

    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Тип отношений, родственники/знакомые
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "relative_type")
    private RelativeType relativeType;

    /**
     * ФИО
     */
    @Column(name = "name")
    private String name;

    /**
     * Контакты
     */
    @Column(name = "contacts")
    private String contacts;

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "relative_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "relative_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public RelativeType getRelativeType() {
        return relativeType;
    }

    public void setRelativeType(RelativeType relativeType) {
        this.relativeType = relativeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Relative relative = (Relative) o;
        return Objects.equals(relativeType, relative.relativeType) &&
            Objects.equals(name, relative.name) &&
            Objects.equals(contacts, relative.contacts) &&
            Objects.equals(scans, relative.scans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), relativeType, name, contacts, scans);
    }

}

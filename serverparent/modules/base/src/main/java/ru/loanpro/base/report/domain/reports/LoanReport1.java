package ru.loanpro.base.report.domain.reports;

import ru.loanpro.security.domain.Department;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Класс сущности отчета - Отчет о просроченных займах
 * Группа отчета - Займы.
 * Идентификатор отчета - LOANS.REPORT1
 *
 * @author Maksim Askhaev
 */
public class LoanReport1 {

    /**
     * Отдел выдачи займа
     */
    private Department loanDepartment;

    /**
     * Номер займа
     */
    private String loanNumber;

    /**
     * Дата выдачи займа
     */
    private LocalDate loanIssueDate;

    /**
     * Сумма займа
     */
    private BigDecimal loanBalance;

    /**
     * Текущий остаток долга
     */
    private BigDecimal currentBalance;

    /**
     * Общее кол-во просроченных платежей по графику рассчитанных платежей
     */
    private int scheduleCount;

    /**
     * Номер просроченного платежа (по графику рассчитанных платежей),
     * по которому сформировалась задолженность
     */
    private int scheduleOrdNo;

    /**
     * Дата платежа (по графику рассчитанных платежей)
     * по которому сформировалась задолженность
     */
    private LocalDate scheduleDate;

    /**
     * Кол-во дней просрочки
     */
    private long overdueDays;

    /**
     * Задолженность по осн. части
     */
    private BigDecimal indebtednessPrincipal;

    /**
     * Задолженность по процентам
     */
    private BigDecimal indebtednessInterest;

    /**
     * Задолженность по пени
     */
    private BigDecimal indebtednessPenalty;

    /**
     * Задолженность суммарно
     */
    private BigDecimal indebtednessSum;

    /**
     * Задолженность по штрафу
     */
    private BigDecimal indebtednessFine;

    /**
     * ФИО заемщика - физлица или юрлица
     */
    private String borrower;

    /**
     * Дополнительные данные о заемщике
     * Может содержать номера телефонов, адреса проживания
     */
    private String borrowerDetails;

    public LoanReport1() {
    }

    public Department getLoanDepartment() {
        return loanDepartment;
    }

    public void setLoanDepartment(Department loanDepartment) {
        this.loanDepartment = loanDepartment;
    }

    public String getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(String loanNumber) {
        this.loanNumber = loanNumber;
    }

    public LocalDate getLoanIssueDate() {
        return loanIssueDate;
    }

    public void setLoanIssueDate(LocalDate loanIssueDate) {
        this.loanIssueDate = loanIssueDate;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public String getBorrowerDetails() {
        return borrowerDetails;
    }

    public void setBorrowerDetails(String borrowerDetails) {
        this.borrowerDetails = borrowerDetails;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public BigDecimal getIndebtednessPrincipal() {
        return indebtednessPrincipal;
    }

    public void setIndebtednessPrincipal(BigDecimal indebtednessPrincipal) {
        this.indebtednessPrincipal = indebtednessPrincipal;
    }

    public BigDecimal getIndebtednessInterest() {
        return indebtednessInterest;
    }

    public void setIndebtednessInterest(BigDecimal indebtednessInterest) {
        this.indebtednessInterest = indebtednessInterest;
    }

    public BigDecimal getIndebtednessPenalty() {
        return indebtednessPenalty;
    }

    public void setIndebtednessPenalty(BigDecimal indebtednessPenalty) {
        this.indebtednessPenalty = indebtednessPenalty;
    }

    public BigDecimal getIndebtednessFine() {
        return indebtednessFine;
    }

    public void setIndebtednessFine(BigDecimal indebtednessFine) {
        this.indebtednessFine = indebtednessFine;
    }

    public BigDecimal getIndebtednessSum() {
        return indebtednessSum;
    }

    public void setIndebtednessSum(BigDecimal indebtednessSum) {
        this.indebtednessSum = indebtednessSum;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public int getScheduleCount() {
        return scheduleCount;
    }

    public void setScheduleCount(int scheduleCount) {
        this.scheduleCount = scheduleCount;
    }

    public int getScheduleOrdNo() {
        return scheduleOrdNo;
    }

    public void setScheduleOrdNo(int scheduleOrdNo) {
        this.scheduleOrdNo = scheduleOrdNo;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public long getOverdueDays() {
        return overdueDays;
    }

    public void setOverdueDays(long overdueDays) {
        this.overdueDays = overdueDays;
    }
}

package ru.loanpro.base.payment.event;

public class PaymentDeletedException extends Exception{
    public PaymentDeletedException(String message) {
        super(message);
    }
}

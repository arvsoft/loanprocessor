package ru.loanpro.base.loan.repository;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.lang.Nullable;
import ru.loanpro.base.loan.domain.LoanProduct;

/**
 * Репозиторий сущности кредитного продукта {@link LoanProduct}
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public interface LoanProductRepository extends BaseLoanRepository<LoanProduct> {

    /**
     * {@inheritDoc}
     */
    @EntityGraph(attributePaths = "loanType")
    @Override
    Page<LoanProduct> findAll(@Nullable Predicate predicate, @Nullable Pageable pageable);
}

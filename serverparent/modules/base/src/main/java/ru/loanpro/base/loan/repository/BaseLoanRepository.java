package ru.loanpro.base.loan.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.global.repository.AbstractIdEntityRepository;

import java.util.UUID;

/**
 * Базовый репозиторий сущностей наследующих {@link BaseLoan}
 *
 * @param <T> тип сущности, наследующей {@link BaseLoan}
 * @author Oleg Brizhevatikh
 */
@NoRepositoryBean
public interface BaseLoanRepository<T extends BaseLoan> extends AbstractIdEntityRepository<T> {

    /**
     * Возвращает сущность по идентификатору с иницализацией всех Lazy-полей
     *
     * @param id идентификатор сущности.
     * @return сущность типа <code>T</code>.
     */
    @Query("select t from LoanProduct t where t.id = ?1")
    @EntityGraph(attributePaths = "schedules")
    T getOneFull(UUID id);
}


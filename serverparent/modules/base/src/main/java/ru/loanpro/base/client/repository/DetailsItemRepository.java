package ru.loanpro.base.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.loanpro.global.domain.AbstractIdEntity;

/**
 * Репозиторий сущностей деталей клиента.
 *
 * @author Oleg Brizhevatikh
 */
public interface DetailsItemRepository extends JpaRepository<AbstractIdEntity, Long> {
}
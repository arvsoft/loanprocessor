package ru.loanpro.base;

import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.security.service.SecurityService;
import ru.loanpro.sequence.service.SequenceService;

import java.util.logging.Logger;

@Component
@VaadinSessionScope
public class NumberService {

    private static final Logger LOGGER = Logger.getLogger(NumberService.class.getName());

    private final SequenceService sequenceService;
    private final SecurityService securityService;
    private final ConfigurationService configurationService;

    @Autowired
    public NumberService(SequenceService sequenceService, SecurityService securityService,
                         ConfigurationService configurationService) {
        this.sequenceService = sequenceService;
        this.securityService = securityService;
        this.configurationService = configurationService;
    }

    public String getLoanSequenceDraftNumber() {
        return sequenceService.getDraftNumber(securityService.getDepartment().getLoanSequence());
    }

    public String getRequestSequenceDraftNumber() {
        return sequenceService.getDraftNumber(securityService.getDepartment().getRequestSequence());
    }

    public String getAccountSequenceDraftNumber() {
        return sequenceService.getDraftNumber(securityService.getDepartment().getAccountSequence());
    }

    public String getDepartmentAccountSequenceDraftNumber() {
        return sequenceService.getDraftNumber(configurationService.getDepartmentAccountSequence());
    }

    public String getProlongSequenceDraftNumber() {
        return sequenceService.getDraftNumber(securityService.getDepartment().getProlongSequence());
    }

    public String getOperationSequenceDraftNumber() {
        return sequenceService.getDraftNumber(securityService.getDepartment().getOperationSequence());
    }

    public void clearLoanSequenceDraftNumber(String draftNumber) {
        sequenceService.clearDraftNumber(securityService.getDepartment().getLoanSequence(), draftNumber);
    }

    public void clearRequestSequenceDraftNumber(String draftNumber) {
        sequenceService.clearDraftNumber(securityService.getDepartment().getRequestSequence(), draftNumber);
    }

    public void clearAccountSequenceDraftNumber(String draftNumber) {
        sequenceService.clearDraftNumber(securityService.getDepartment().getAccountSequence(), draftNumber);
    }

    public void clearDepartmentAccountSequenceDraftNumber(String draftNumber) {
        sequenceService.clearDraftNumber(configurationService.getDepartmentAccountSequence(), draftNumber);
    }

    public void clearProlongSequenceDraftNumber(String draftNumber) {
        sequenceService.clearDraftNumber(securityService.getDepartment().getProlongSequence(), draftNumber);
    }

    public void clearOperationSequenceDraftNumber(String draftNumber) {
        sequenceService.clearDraftNumber(securityService.getDepartment().getOperationSequence(), draftNumber);
    }
}

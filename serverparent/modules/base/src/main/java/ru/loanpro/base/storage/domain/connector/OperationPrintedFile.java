package ru.loanpro.base.storage.domain.connector;

import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

/**
 * Сущность распечатанного документа об операции.
 *
 * @author Maksim Askhaev
 */
@Entity
@DiscriminatorValue("PRINTED_OPERATION_DOCUMENT")
@SecondaryTable(name = DatabaseSchema.Operation.OPERATION_PRINTED, schema = DatabaseSchema.OPERATION,
    pkJoinColumns = @PrimaryKeyJoinColumn(name = "printed_file_id"))
public class OperationPrintedFile extends PrintedFile {
    /**
     * Распечатанная операция.
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(table = DatabaseSchema.Operation.OPERATION_PRINTED, name = "operation_id")
    private Operation operation;

    public OperationPrintedFile() {
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}


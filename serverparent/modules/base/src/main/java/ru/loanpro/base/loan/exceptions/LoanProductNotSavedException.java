package ru.loanpro.base.loan.exceptions;

/**
 * Исключение, возникающее при попытке выполнить сохранение кредитного продукта
 *
 * @author Maksim Askhaev
 */
public class LoanProductNotSavedException extends BaseLoanNotSavedException {
    public LoanProductNotSavedException(Exception e){
        super(e);
    }
}

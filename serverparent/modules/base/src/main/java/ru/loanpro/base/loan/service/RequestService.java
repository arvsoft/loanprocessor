package ru.loanpro.base.loan.service;

import com.querydsl.core.types.Predicate;
import com.vaadin.data.provider.QuerySortOrder;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.base.loan.domain.QRequest;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.loan.exceptions.ObjectNotSavedException;
import ru.loanpro.base.loan.exceptions.RequestNotSavedException;
import ru.loanpro.base.loan.repository.RequestRepository;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.sequence.service.SequenceService;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Сервис работы с заявкой
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Service
public class RequestService extends BaseLoanService<Request, RequestRepository> {

    private final SequenceService sequenceService;
    private final ApplicationEventBus applicationEventBus;

    @Autowired
    public RequestService(RequestRepository repository, SequenceService sequenceService, ApplicationEventBus applicationEventBus) {
        super(repository);
        this.sequenceService = sequenceService;
        this.applicationEventBus = applicationEventBus;
    }

    /**
     * Возвращает одну заявку по её идентификатору. Инициализация Lazy-полей осуществляется в частично в репозитории
     * и частично в методе, по причине наличия нескольких полей типа <code>List</code>.
     *
     * @param id идентификатор сущности.
     * @return сущность типа <code>T</code>.
     */
    @Transactional
    @Override
    public Request getOneFull(UUID id) {
        Request one = super.getOneFull(id);
        Hibernate.initialize(one.getSchedules());

        return one;
    }

    /**
     * Сохраняет заявку. Если заявка в БД отсутствует, создаёт новую заявку.
     *
     * @param request заявка.
     * @param requestDraftNumber временный выданный номер заявки.
     * @return сохранённая заявка.
     * @throws ObjectNotSavedException если заявка не сохранена.
     */
    @Transactional
    public synchronized Request save(Request request, String requestDraftNumber) throws ObjectNotSavedException {

        if (request.getId() != null) {
            return super.save(request);
        } else
            return saveNewRequest(request, requestDraftNumber);
    }

    @Override
    public Stream<Request> findAll(Predicate predicate, List<QuerySortOrder> sortOrder, int offset, int limit) {
        return super.findAll(QRequest.request.deleted.isFalse().and(predicate), sortOrder, offset, limit);
    }

    @Override
    public Integer count(Predicate predicate) {
        return super.count(QRequest.request.deleted.isFalse().and(predicate));
    }

    /**
     * {@inheritDoc}
     * Ограничение по удалению заявки - существование заявки.
     */
    @Override
    public boolean canBeDeleted(Request entity) {
        return entity.getId() != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Request entity) throws EntityDeleteException {
        entity.setDeleted(true);
        repository.save(entity);
    }

    private synchronized Request saveNewRequest(Request request, String requestDraftNumber)
        throws RequestNotSavedException {
        try {
            if (request.getRequestNumber().equals(requestDraftNumber)) {
                // Номер заявки совпадает с выданным временным номером, значит пользователь не изменял номер
                String number = sequenceService.actualizeNextNumber(request.getDepartment().getRequestSequence());
                request.setRequestNumber(number);
            }

            Request result = super.save(request);

            // Если всё сохранилось корректно - оповещаем систему об обновлении последовательности
            applicationEventBus.post(new UpdateSequenceEvent(request.getDepartment().getRequestSequence()));

            return result;
        } catch (Exception e) {
            // При любом исключении из бд - откатываем назад номер и бросаем исключение.
            sequenceService.resetLastNumber(request.getDepartment().getRequestSequence());
            throw new RequestNotSavedException(e);
        }
    }
}

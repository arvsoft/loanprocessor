package ru.loanpro.base.printer.dpo;

/**
 * Печатаемая операция по кассе
 *
 * @author Maksim Askhaev
 */
public class PrintOperation {

    private String department;//отдел проведения операции
    private String paymentMethod;//способ платежа /наличный/безналичный

    private String accountPayer;//номер счета плательщика
    private String accountPayerName;//имя владельца счета плательщика
    private String payer;//имя плательщика при наличном взносе

    private String accountRecipient;//номер счета получателя
    private String accountRecipientName;//имя владельца счета получателя
    private String recipient;//имя получателя при наличной выдачи

    private String payerTotal;//сборные данные о плательщике
    private String recipientTotal;//сборные данные о получателе

    private String operationDate;//дата операции
    private String operationNumber;//номер операции
    private String operationTime;//время операции
    private String operationValue;//сумма операции
    private String operationValueS;//сумма операции строкой
    private String currency;//валюта операции
    private String currencyRate;//коэффициент конвертации валюты
    private String loanNumber;//номер займа
    private String loanDate;//дата выдачи займа
    private String paymentNumber;//номер платежа
    private String paymentDate;//дата платежа
    private String paymentPart;//перечень входящих составляющих платежа
    private String notes;//заметки

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAccountPayer() {
        return accountPayer;
    }

    public void setAccountPayer(String accountPayer) {
        this.accountPayer = accountPayer;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getAccountRecipient() {
        return accountRecipient;
    }

    public void setAccountRecipient(String accountRecipient) {
        this.accountRecipient = accountRecipient;
    }

    public String getAccountPayerName() {
        return accountPayerName;
    }

    public void setAccountPayerName(String accountPayerName) {
        this.accountPayerName = accountPayerName;
    }

    public String getAccountRecipientName() {
        return accountRecipientName;
    }

    public void setAccountRecipientName(String accountRecipientName) {
        this.accountRecipientName = accountRecipientName;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getPayerTotal() {
        return payerTotal;
    }

    public void setPayerTotal(String payerTotal) {
        this.payerTotal = payerTotal;
    }

    public String getRecipientTotal() {
        return recipientTotal;
    }

    public void setRecipientTotal(String recipientTotal) {
        this.recipientTotal = recipientTotal;
    }

    public String getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(String operationDate) {
        this.operationDate = operationDate;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public String getOperationValue() {
        return operationValue;
    }

    public void setOperationValue(String operationValue) {
        this.operationValue = operationValue;
    }

    public String getOperationValueS() {
        return operationValueS;
    }

    public void setOperationValueS(String operationValueS) {
        this.operationValueS = operationValueS;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(String currencyRate) {
        this.currencyRate = currencyRate;
    }

    public String getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(String loanNumber) {
        this.loanNumber = loanNumber;
    }

    public String getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(String loanDate) {
        this.loanDate = loanDate;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentPart() {
        return paymentPart;
    }

    public void setPaymentPart(String paymentPart) {
        this.paymentPart = paymentPart;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}

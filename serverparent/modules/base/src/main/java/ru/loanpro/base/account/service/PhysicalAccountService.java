package ru.loanpro.base.account.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.base.account.domain.BaseAccount;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.account.repository.PhysicalAccountRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * Сервис по работе со счетами физического лица
 *
 * @author Maksim Askhaev
 */
@Service
public class PhysicalAccountService extends AbstractAccountService<PhysicalAccount> {

    @Autowired
    public PhysicalAccountService(PhysicalAccountRepository repository) {
        super(repository);
    }

    @Transactional
    public PhysicalAccount getOneFull(UUID id) {
        PhysicalAccount one = repository.getOne(id);
        Hibernate.initialize(one.getClientDetails());

        return one;
    }

    @Override
    public List<PhysicalAccount> getAccounts(Object object) {
        return repository.findAllByEntity(object);
    }

    public BigDecimal getBalanceForAccount(BaseAccount account){
        return super.getBalanceForEntity(account);
    };

    public int updateAccountBalance(UUID id, BigDecimal balance){
        return super.updateAccountBalance(id, balance);
    }
}

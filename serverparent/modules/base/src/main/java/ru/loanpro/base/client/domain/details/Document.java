package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.directory.domain.DocumentType;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.DocumentStatus;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * Документ физического лица.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "document", schema = DatabaseSchema.CLIENT)
public class Document extends AbstractIdEntity {

    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Статус документа.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DocumentStatus status;

    /**
     * Тип документа.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "document_type")
    private DocumentType documentType;

    /**
     * Серия.
     */
    @Column(name = "series")
    private String series;

    /**
     * Номер.
     */
    @Column(name = "number", nullable = false)
    private String number;

    /**
     * Место выдачи.
     */
    @Column(name = "issue_place")
    private String issuePlace;

    /**
     * Дата выдачи.
     */
    @Column(name = "issue_date")
    private LocalDate issueDate;

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "document_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "document_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public Document() {
    }

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public DocumentStatus getStatus() {
        return status;
    }

    public void setStatus(DocumentStatus status) {
        this.status = status;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIssuePlace() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Document document = (Document) o;
        return Objects.equals(status, document.status) &&
            Objects.equals(documentType, document.documentType) &&
            Objects.equals(series, document.series) &&
            Objects.equals(number, document.number) &&
            Objects.equals(issuePlace, document.issuePlace) &&
            Objects.equals(issueDate, document.issueDate) &&
            Objects.equals(scans, document.scans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), status, documentType, series, number, issuePlace, issueDate, scans);
    }
}

package ru.loanpro.base.storage.domain.connector;

import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

/**
 * Сущность распечатанного документа о платеже.
 *
 * @author Maksim Askhaev
 */
@Entity
@DiscriminatorValue("PRINTED_PAYMENT_DOCUMENT")
@SecondaryTable(name = DatabaseSchema.Loan.PAYMENT_PRINTED, schema = DatabaseSchema.LOAN,
    pkJoinColumns = @PrimaryKeyJoinColumn(name = "printed_file_id"))
public class PaymentPrintedFile extends PrintedFile {
    /**
     * Распечатанный платеж.
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(table = DatabaseSchema.Loan.PAYMENT_PRINTED, name = "payment_id")
    private Payment payment;

    public PaymentPrintedFile() {
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}

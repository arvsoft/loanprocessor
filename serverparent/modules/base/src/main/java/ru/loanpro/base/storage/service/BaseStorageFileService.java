package ru.loanpro.base.storage.service;

import org.springframework.data.repository.NoRepositoryBean;

import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.base.storage.domain.BaseStorageFile;
import ru.loanpro.base.storage.repository.BaseStorageFileRepository;

import java.util.List;

/**
 * Базовый сервис сущности файла, хранящегося в хранилище файлов {@link BaseStorageFile}.
 *
 * @param <T> тип сущности файла в хранилище файлов.
 *
 * @author Oleg Brizhevatikh
 */
@NoRepositoryBean
public class BaseStorageFileService<T extends BaseStorageFile> {

    protected BaseStorageFileRepository<T> repository;

    public BaseStorageFileService(BaseStorageFileRepository<T> repository) {
        this.repository = repository;
    }


    /**
     * Возвращает список всех файлов типа <code>T</code> в хранилище файлов.
     *
     * @return список сущностей файлов.
     */
    public List<T> findAll() {
        return repository.findAll();
    }

    /**
     * Сохраняет информацию о файле в хранилище файлов.
     *
     * @param storageFile сущность сохраняемого файла.
     */
    @Transactional
    public void save(T storageFile) {
        repository.save(storageFile);
    }

    /**
     * Удаляет информацию о файле в хранилище файлов.
     *
     * @param storageFile сущность удаляемого файла.
     */
    @Transactional
    public void delete(T storageFile) {
        repository.delete(storageFile);
    }
}

package ru.loanpro.base.account.domain;

import ru.loanpro.security.domain.Department;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Сущность счета отдела.
 *
 * @author Maksim Askhaev
 */
@Entity
@DiscriminatorValue("DEPARTMENT")
public class DepartmentAccount extends BaseAccount {

    /**
     * Ссылка на отдел
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    Department department;

    public DepartmentAccount() {
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

}
package ru.loanpro.base.client.domain;

import ru.loanpro.base.account.domain.JuridicalAccount;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Подробная информация о юридическом лице {@link JuridicalClient}.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "juridical_client_details", schema = DatabaseSchema.CLIENT)
public class JuridicalClientDetails extends ClientDetails {
    /**
     * Коллекция Account (счетов юридического лица)
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "clientDetails", orphanRemoval = true)
    private Set<JuridicalAccount> accounts = new HashSet<>();

    public JuridicalClientDetails() {
    }

    public Set<JuridicalAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<JuridicalAccount> accounts) {
        this.accounts = accounts;
    }
}

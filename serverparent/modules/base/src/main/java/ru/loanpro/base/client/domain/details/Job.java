package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * Работа
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "job", schema = DatabaseSchema.CLIENT)
public class Job extends AbstractIdEntity {

    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Род занятий/должность.
     */
    @Column(name = "occupation")
    private String occupation;

    /**
     * Организация
     */
    @Column(name = "organization")
    private String organization;

    /**
     * Телефон приемной
     */
    @Column(name = "reception_phone")
    private String receptionPhone;

    /**
     * Дата трудоустройства
     */
    @Column(name = "employment_date")
    private LocalDate employmentDate;

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "job_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "job_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getReceptionPhone() {
        return receptionPhone;
    }

    public void setReceptionPhone(String receptionPhone) {
        this.receptionPhone = receptionPhone;
    }

    public LocalDate getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(LocalDate employmentDate) {
        this.employmentDate = employmentDate;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Job job = (Job) o;
        return Objects.equals(occupation, job.occupation) &&
            Objects.equals(organization, job.organization) &&
            Objects.equals(receptionPhone, job.receptionPhone) &&
            Objects.equals(employmentDate, job.employmentDate) &&
            Objects.equals(scans, job.scans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), occupation, organization, receptionPhone, employmentDate, scans);
    }

}

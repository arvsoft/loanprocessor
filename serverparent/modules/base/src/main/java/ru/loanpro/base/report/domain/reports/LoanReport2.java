package ru.loanpro.base.report.domain.reports;

import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.security.domain.Department;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Класс сущности отчета - Отчет о распределении статусов займов
 * Группа отчета - Займы.
 * Идентификатор отчета - LOANS.REPORT2
 *
 * @author Maksim Askhaev
 */
public class LoanReport2 {

    /**
     * Статус займов
     */
    private LoanStatus loanStatus;

    /**
     * Валюта займа
     */
    private Currency currency;

    /**
     * Отдел выдачи займа
     */
    private Department department;

    /**
     * Кол-во займов
     */
    private int numberLoans;

    /**
     * Совокупный объем выданных займов
     */
    private BigDecimal sumLoanBalance;

    /**
     * Совокупный объем текущего остатка
     */
    private BigDecimal sumCurrentBalance;

    /**
     * Запланировано по графику
     */
    private BigDecimal sumScheduleBalance;

    /**
     * Оплачено фактически всего
     */
    private BigDecimal sumPaymentBalance;

    /**
     * Процентное соотношение
     */
    private BigDecimal percentage;

    public LoanReport2() {
    }

    public LoanStatus getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(LoanStatus loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getNumberLoans() {
        return numberLoans;
    }

    public void setNumberLoans(int numberLoans) {
        this.numberLoans = numberLoans;
    }

    public BigDecimal getSumLoanBalance() {
        return sumLoanBalance;
    }

    public void setSumLoanBalance(BigDecimal sumLoanBalance) {
        this.sumLoanBalance = sumLoanBalance;
    }

    public BigDecimal getSumCurrentBalance() {
        return sumCurrentBalance;
    }

    public void setSumCurrentBalance(BigDecimal sumCurrentBalance) {
        this.sumCurrentBalance = sumCurrentBalance;
    }

    public BigDecimal getSumScheduleBalance() {
        return sumScheduleBalance;
    }

    public void setSumScheduleBalance(BigDecimal sumScheduleBalance) {
        this.sumScheduleBalance = sumScheduleBalance;
    }

    public BigDecimal getSumPaymentBalance() {
        return sumPaymentBalance;
    }

    public void setSumPaymentBalance(BigDecimal sumPaymentBalance) {
        this.sumPaymentBalance = sumPaymentBalance;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}
package ru.loanpro.base.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.account.domain.BaseAccount;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * Репозиторий сущности {@link BaseAccount}.
 *
 * @author Maksim Askhaev
 */
public interface BaseAccountRepository<T extends BaseAccount> extends JpaRepository<T, UUID> {

    @Query("select a from BaseAccount a")
    List<T> findAllByEntity(Object entity);

    @Modifying
    @Query("update BaseAccount a set a.balance = ?2 where a.id=?1")
    int updateAccountBalance(UUID id, BigDecimal balance);

    @Query("select a.balance from BaseAccount a where a = ?1")
    BigDecimal getBalanceForEntity(Object entity);
}

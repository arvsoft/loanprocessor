package ru.loanpro.base.printer.dpo;

/**
 * Печатаемый платеж по займу
 *
 * @author Maksim Askhaev
 */
public class PrintPayment {

    private String department;//Отдел проведения платежа
    private String ordNo;//Порядковый номер
    private String scheduleOrdNo;//Порядковый номер по графику
    private String status;//Статус платежа
    private String payDate;//Дата платежа фактическая
    private String payDateS;//Дата платежа фактическая
    private String term;//Срок платежа
    private String termS;//Срок платежа
    private String overdueTerm;//Срок просрочки
    private String overdueTermS;//Срок просрочки

    private String balanceBefore;//Остаток основной части долга перед данным платежом
    private String balanceBeforeS;//Остаток основной части долга перед данным платежом строкой
    private String balanceAfter;//Остаток основной части долга после данного платежа
    private String balanceAfterS;//Остаток основной части долга после данного платежа строкой

    private String principalB;//Размер основной части (остаток на начало платежа)
    private String principalBS;//Размер основной части (остаток на начало платежа) строкой
    private String interestB;//Размер процентов (остаток на начало платежа)
    private String interestBS;//Размер процентов (остаток на начало платежа) строкой
    private String penaltyB;//Размер неустойки (остаток на начало платежа)
    private String penaltyBS;//Размер неустойки (остаток на начало платежа) строкой
    private String fineB;//Размер штрафа (остаток на начало платежа)
    private String fineBS;//Размер штрафа (остаток на начало платежа) строкой
    private String paymentB;//Размер платежа (остаток на начало платежа)
    private String paymentBS;//Размер платежа (остаток на начало платежа) строкой

    private String principalC;//Размер основной части (расчет на дату платежа)
    private String principalCS;//Размер основной части (расчет на дату платежа) строкой
    private String interestC;//Размер процентов (расчет на дату платежа)
    private String interestCS;//Размер процентов (расчет на дату платежа) строкой
    private String penaltyC;//Размер неустойки (расчет на дату платежа)
    private String penaltyCS;//Размер неустойки (расчет на дату платежа) строкой
    private String fineC;//Размер штрафа (расчет на дату платежа)
    private String fineCS;//Размер штрафа (расчет на дату платежа) строкой
    private String paymentC;//Размер платежа (расчет на дату платежа)
    private String paymentCS;//Размер платежа (расчет на дату платежа) строкой

    private String principalP;//Размер основной части (к оплате)
    private String principalPS;//Размер основной части (к оплате) строкой
    private String interestP;//Размер процентов (к оплате)
    private String interestPS;//Размер процентов (к оплате) строкой
    private String penaltyP;//Размер неустойки (к оплате)
    private String penaltyPS;//Размер неустойки (к оплате) строкой
    private String fineP;//Размер штрафа (к оплате)
    private String finePS;//Размер штрафа (к оплате) строкой
    private String paymentP;//Размер платежа (к оплате)
    private String paymentPS;//Размер платежа (к оплате) строкой

    private String principalF;//Размер основной части (оплачено фактически)
    private String principalFS;//Размер основной части (оплачено фактически) строкой
    private String interestF;//Размер процентов (оплачено фактически)
    private String interestFS;//Размер процентов (оплачено фактически) строкой
    private String penaltyF;//Размер неустойки (оплачено фактически)
    private String penaltyFS;//Размер неустойки (оплачено фактически) строкой
    private String fineF;//Размер штрафа (оплачено фактически)
    private String fineFS;//Размер штрафа (оплачено фактически) строкой
    private String paymentF;//Размер платежа (оплачено фактически)
    private String paymentFS;//Размер платежа (оплачено фактически) строкой

    private String principalE;//Размер основной части (остаток после платежа)
    private String principalES;//Размер основной части (остаток после платежа) строкой
    private String interestE;//Размер процентов (остаток после платежа)
    private String interestES;//Размер процентов (остаток после платежа) строкой
    private String penaltyE;//Размер неустойки (остаток после платежа)
    private String penaltyES;//Размер неустойки (остаток после платежа) строкой
    private String fineE;//Размер штрафа (остаток после платежа)
    private String fineES;//Размер штрафа (остаток после платежа) строкой
    private String paymentE;//Размер платежа (остаток после платежа)
    private String paymentES;//Размер платежа (остаток после платежа) строкой

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getScheduleOrdNo() {
        return scheduleOrdNo;
    }

    public void setScheduleOrdNo(String scheduleOrdNo) {
        this.scheduleOrdNo = scheduleOrdNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getPayDateS() {
        return payDateS;
    }

    public void setPayDateS(String payDateS) {
        this.payDateS = payDateS;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTermS() {
        return termS;
    }

    public void setTermS(String termS) {
        this.termS = termS;
    }

    public String getOverdueTerm() {
        return overdueTerm;
    }

    public void setOverdueTerm(String overdueTerm) {
        this.overdueTerm = overdueTerm;
    }

    public String getOverdueTermS() {
        return overdueTermS;
    }

    public void setOverdueTermS(String overdueTermS) {
        this.overdueTermS = overdueTermS;
    }

    public String getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(String balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public String getBalanceBeforeS() {
        return balanceBeforeS;
    }

    public void setBalanceBeforeS(String balanceBeforeS) {
        this.balanceBeforeS = balanceBeforeS;
    }

    public String getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(String balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public String getBalanceAfterS() {
        return balanceAfterS;
    }

    public void setBalanceAfterS(String balanceAfterS) {
        this.balanceAfterS = balanceAfterS;
    }

    public String getPrincipalB() {
        return principalB;
    }

    public void setPrincipalB(String principalB) {
        this.principalB = principalB;
    }

    public String getPrincipalBS() {
        return principalBS;
    }

    public void setPrincipalBS(String principalBS) {
        this.principalBS = principalBS;
    }

    public String getInterestB() {
        return interestB;
    }

    public void setInterestB(String interestB) {
        this.interestB = interestB;
    }

    public String getInterestBS() {
        return interestBS;
    }

    public void setInterestBS(String interestBS) {
        this.interestBS = interestBS;
    }

    public String getPenaltyB() {
        return penaltyB;
    }

    public void setPenaltyB(String penaltyB) {
        this.penaltyB = penaltyB;
    }

    public String getPenaltyBS() {
        return penaltyBS;
    }

    public void setPenaltyBS(String penaltyBS) {
        this.penaltyBS = penaltyBS;
    }

    public String getFineB() {
        return fineB;
    }

    public void setFineB(String fineB) {
        this.fineB = fineB;
    }

    public String getFineBS() {
        return fineBS;
    }

    public void setFineBS(String fineBS) {
        this.fineBS = fineBS;
    }

    public String getPaymentB() {
        return paymentB;
    }

    public void setPaymentB(String paymentB) {
        this.paymentB = paymentB;
    }

    public String getPaymentBS() {
        return paymentBS;
    }

    public void setPaymentBS(String paymentBS) {
        this.paymentBS = paymentBS;
    }

    public String getPrincipalC() {
        return principalC;
    }

    public void setPrincipalC(String principalC) {
        this.principalC = principalC;
    }

    public String getPrincipalCS() {
        return principalCS;
    }

    public void setPrincipalCS(String principalCS) {
        this.principalCS = principalCS;
    }

    public String getInterestC() {
        return interestC;
    }

    public void setInterestC(String interestC) {
        this.interestC = interestC;
    }

    public String getInterestCS() {
        return interestCS;
    }

    public void setInterestCS(String interestCS) {
        this.interestCS = interestCS;
    }

    public String getPenaltyC() {
        return penaltyC;
    }

    public void setPenaltyC(String penaltyC) {
        this.penaltyC = penaltyC;
    }

    public String getPenaltyCS() {
        return penaltyCS;
    }

    public void setPenaltyCS(String penaltyCS) {
        this.penaltyCS = penaltyCS;
    }

    public String getFineC() {
        return fineC;
    }

    public void setFineC(String fineC) {
        this.fineC = fineC;
    }

    public String getFineCS() {
        return fineCS;
    }

    public void setFineCS(String fineCS) {
        this.fineCS = fineCS;
    }

    public String getPaymentC() {
        return paymentC;
    }

    public void setPaymentC(String paymentC) {
        this.paymentC = paymentC;
    }

    public String getPaymentCS() {
        return paymentCS;
    }

    public void setPaymentCS(String paymentCS) {
        this.paymentCS = paymentCS;
    }



    public String getPrincipalP() {
        return principalP;
    }

    public void setPrincipalP(String principalP) {
        this.principalP = principalP;
    }

    public String getPrincipalPS() {
        return principalPS;
    }

    public void setPrincipalPS(String principalPS) {
        this.principalPS = principalPS;
    }

    public String getInterestP() {
        return interestP;
    }

    public void setInterestP(String interestP) {
        this.interestP = interestP;
    }

    public String getInterestPS() {
        return interestPS;
    }

    public void setInterestPS(String interestPS) {
        this.interestPS = interestPS;
    }

    public String getPenaltyP() {
        return penaltyP;
    }

    public void setPenaltyP(String penaltyP) {
        this.penaltyP = penaltyP;
    }

    public String getPenaltyPS() {
        return penaltyPS;
    }

    public void setPenaltyPS(String penaltyPS) {
        this.penaltyPS = penaltyPS;
    }

    public String getFineP() {
        return fineP;
    }

    public void setFineP(String fineP) {
        this.fineP = fineP;
    }

    public String getFinePS() {
        return finePS;
    }

    public void setFinePS(String finePS) {
        this.finePS = finePS;
    }

    public String getPaymentP() {
        return paymentP;
    }

    public void setPaymentP(String paymentP) {
        this.paymentP = paymentP;
    }

    public String getPaymentPS() {
        return paymentPS;
    }

    public void setPaymentPS(String paymentPS) {
        this.paymentPS = paymentPS;
    }

    public String getPrincipalF() {
        return principalF;
    }

    public void setPrincipalF(String principalF) {
        this.principalF = principalF;
    }

    public String getPrincipalFS() {
        return principalFS;
    }

    public void setPrincipalFS(String principalFS) {
        this.principalFS = principalFS;
    }

    public String getInterestF() {
        return interestF;
    }

    public void setInterestF(String interestF) {
        this.interestF = interestF;
    }

    public String getInterestFS() {
        return interestFS;
    }

    public void setInterestFS(String interestFS) {
        this.interestFS = interestFS;
    }

    public String getPenaltyF() {
        return penaltyF;
    }

    public void setPenaltyF(String penaltyF) {
        this.penaltyF = penaltyF;
    }

    public String getPenaltyFS() {
        return penaltyFS;
    }

    public void setPenaltyFS(String penaltyFS) {
        this.penaltyFS = penaltyFS;
    }

    public String getFineF() {
        return fineF;
    }

    public void setFineF(String fineF) {
        this.fineF = fineF;
    }

    public String getFineFS() {
        return fineFS;
    }

    public void setFineFS(String fineFS) {
        this.fineFS = fineFS;
    }

    public String getPaymentF() {
        return paymentF;
    }

    public void setPaymentF(String paymentF) {
        this.paymentF = paymentF;
    }

    public String getPaymentFS() {
        return paymentFS;
    }

    public void setPaymentFS(String paymentFS) {
        this.paymentFS = paymentFS;
    }

    public String getPrincipalE() {
        return principalE;
    }

    public void setPrincipalE(String principalE) {
        this.principalE = principalE;
    }

    public String getPrincipalES() {
        return principalES;
    }

    public void setPrincipalES(String principalES) {
        this.principalES = principalES;
    }

    public String getInterestE() {
        return interestE;
    }

    public void setInterestE(String interestE) {
        this.interestE = interestE;
    }

    public String getInterestES() {
        return interestES;
    }

    public void setInterestES(String interestES) {
        this.interestES = interestES;
    }

    public String getPenaltyE() {
        return penaltyE;
    }

    public void setPenaltyE(String penaltyE) {
        this.penaltyE = penaltyE;
    }

    public String getPenaltyES() {
        return penaltyES;
    }

    public void setPenaltyES(String penaltyES) {
        this.penaltyES = penaltyES;
    }

    public String getFineE() {
        return fineE;
    }

    public void setFineE(String fineE) {
        this.fineE = fineE;
    }

    public String getFineES() {
        return fineES;
    }

    public void setFineES(String fineES) {
        this.fineES = fineES;
    }

    public String getPaymentE() {
        return paymentE;
    }

    public void setPaymentE(String paymentE) {
        this.paymentE = paymentE;
    }

    public String getPaymentES() {
        return paymentES;
    }

    public void setPaymentES(String paymentES) {
        this.paymentES = paymentES;
    }
}

package ru.loanpro.base.report.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.security.domain.Department;

import java.time.Instant;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

/**
 * Репозиторий работы с отчетами
 *
 * @author Maksim Askhaev
 */
public interface ReportRepository extends JpaRepository<Report, UUID> {
    //****************************************************************************************************************//
    @Query("select l from Loan l where l.isProlonged = false and l.loanStatus = ru.loanpro.global.directory.LoanStatus.OPENED_CURRENT_OVERDUE")
    List<Loan> findOverdueLoans();

    @Query("select l from Loan l where l.isProlonged = false and l.loanStatus = ru.loanpro.global.directory.LoanStatus.OPENED_CURRENT_OVERDUE and l.department=?1")
    List<Loan> findOverdueLoansByDepartment(Department department);

    @Query("select l from Loan l where l.isProlonged = false and l.loanStatus = ru.loanpro.global.directory.LoanStatus.OPENED_CURRENT_OVERDUE and l.loanIssueDate>=?1 and l.loanIssueDate<=?2")
    List<Loan> findOverdueLoansByDates(Instant min, Instant max);

    @Query("select l from Loan l where l.isProlonged = false and l.loanStatus = ru.loanpro.global.directory.LoanStatus.OPENED_CURRENT_OVERDUE and l.department=?1 and l.loanIssueDate>=?2 and l.loanIssueDate<=?3")
    List<Loan> findOverdueLoansByDepartmentAndDates(Department department, Instant min, Instant max);

    //****************************************************************************************************************//
    @Query("select distinct l.currency from Loan l where l.isProlonged = false")
    List<Currency> findDistinctCurrency();

    @Query("select distinct l.currency from Loan l where l.isProlonged = false and l.department = ?1")
    List<Currency> findDistinctCurrencyByDepartment(Department department);

    @Query("select distinct l.currency from Loan l where l.isProlonged = false and l.loanIssueDate>=?1 and l.loanIssueDate<=?2")
    List<Currency> findDistinctCurrencyByDates(Instant min, Instant max);

    @Query("select distinct l.currency from Loan l where l.isProlonged = false and l.department = ?1 and l.loanIssueDate>=?2 and l.loanIssueDate<=?3")
    List<Currency> findDistinctCurrencyByDepartmentAndDates(Department department, Instant min, Instant max);

    //****************************************************************************************************************//
    @Query("select l from Loan l where l.isProlonged = false and l.currency = ?1 and l.loanStatus = ?2")
    List<Loan> findAllLoansByCurrencyAndStatus(Currency currency, LoanStatus status);

    @Query("select l from Loan l where l.isProlonged = false and l.currency = ?1 and l.loanStatus = ?2 and l.department=?3")
    List<Loan> findAllLoansByCurrencyAndStatusAndDepartment(Currency currency, LoanStatus status, Department department);

    @Query("select l from Loan l where l.isProlonged = false and l.currency = ?1 and l.loanStatus = ?2 and l.loanIssueDate>=?3 and l.loanIssueDate<=?4")
    List<Loan> findAllLoansByCurrencyAndStatusAndDates(Currency currency, LoanStatus status, Instant min, Instant max);

    @Query("select l from Loan l where l.isProlonged = false and l.currency = ?1 and l.loanStatus = ?2 and l.department=?3 and l.loanIssueDate>=?4 and l.loanIssueDate<=?5")
    List<Loan> findAllLoansByCurrencyAndStatusAndDepartmentAndDates(Currency currency, LoanStatus status, Department department, Instant min, Instant max);

    //****************************************************************************************************************//
    @Query("select c from Client c where c.rating = ?1 and type(c.class) = PhysicalClient")
    List<PhysicalClient> findAllClientsByRating(ClientRating rating, Pageable pageable);

    @Query("select count(c) from Client c where c.rating = ?1 and type(c.class) = PhysicalClient")
    int countFindAllClientsByRating(ClientRating rating);
}

package ru.loanpro.base.storage.exception;

/**
 * Исключение, возникающее при невозможности получения или передачи файла в подсистему хранения файлов.
 *
 * @author Oleg Brizhevatikh
 */
public class FileSystemErrorException extends Exception {

    public FileSystemErrorException(String message) {
        super(message);
    }
}

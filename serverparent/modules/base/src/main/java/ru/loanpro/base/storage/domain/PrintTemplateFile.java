package ru.loanpro.base.storage.domain;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.global.DatabaseSchema;

/**
 * Сущность шаблонов печати.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("PRINT_TEMPLATE")
@SecondaryTable(name = DatabaseSchema.Core.PRINT_TEMPLATE_FILE, schema = DatabaseSchema.CORE,
    pkJoinColumns = @PrimaryKeyJoinColumn(name = "id"))
public class PrintTemplateFile extends BaseStorageFile {

    /**
     * Тип печатаемого документа.
     */
    @Column(table = DatabaseSchema.Core.PRINT_TEMPLATE_FILE, name = "template_type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PrintDocumentType templateType;

    /**
     * Доступен ли шаблон для печати.
     */
    @Column(table = DatabaseSchema.Core.PRINT_TEMPLATE_FILE, name = "is_enabled", nullable = false)
    private boolean isEnabled;

    public PrintDocumentType getTemplateType() {
        return templateType;
    }

    public void setTemplateType(PrintDocumentType templateType) {
        this.templateType = templateType;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        PrintTemplateFile that = (PrintTemplateFile) o;
        return isEnabled == that.isEnabled &&
            templateType == that.templateType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), templateType, isEnabled);
    }
}

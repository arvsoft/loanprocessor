package ru.loanpro.base.storage.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.base.storage.domain.PrintTemplateFile;
import ru.loanpro.base.storage.repository.PrintTemplateFileRepository;

/**
 * Сервис шаблонов печати.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class PrintTemplateFileService extends BaseStorageFileService<PrintTemplateFile> {

    private final PrintTemplateFileRepository templateRepository;

    @Autowired
    public PrintTemplateFileService(PrintTemplateFileRepository templateRepository) {
        super(templateRepository);
        this.templateRepository = templateRepository;
    }

    /**
     * Возвращает список активных шаблонов печати по типу шаблона. Шаблоны отсортированы
     * по имени шаблона.
     *
     * @param templateType тип шаблона.
     * @return список шаблонов.
     */
    public List<PrintTemplateFile> getActiveTemplates(PrintDocumentType templateType) {
        return templateRepository.findAllByTemplateType(templateType);
    }

    public Map<PrintDocumentType, List<PrintTemplateFile>> getActiveTemplates(PrintDocumentType... documentTypes) {
        Map<PrintDocumentType, List<PrintTemplateFile>> result = new HashMap<>();
        for (PrintDocumentType documentType : documentTypes)
            result.put(documentType, new ArrayList<>());

        templateRepository.findAllByTemplateType(documentTypes)
            .forEach(template -> result.get(template.getTemplateType()).add(template));

        return result;
    }
}

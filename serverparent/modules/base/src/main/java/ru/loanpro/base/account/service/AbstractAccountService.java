package ru.loanpro.base.account.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.base.account.domain.BaseAccount;
import ru.loanpro.base.account.repository.BaseAccountRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * Абстрактный сервис по работе со счетами
 *
 * @param <T> - тип сущности счета
 * @author Maksim Askhaev
 */
@NoRepositoryBean
public abstract class AbstractAccountService<T extends BaseAccount> {

    protected final BaseAccountRepository<T> repository;

    @Autowired
    public AbstractAccountService(BaseAccountRepository<T> repository) {
        this.repository = repository;
    }

    public abstract List<T> getAccounts(Object entity);

    protected BigDecimal getBalanceForEntity(Object entity){
        return repository.getBalanceForEntity(entity);
    }

    protected int updateAccountBalance(UUID id, BigDecimal balance){
        return repository.updateAccountBalance(id, balance);
    }

}

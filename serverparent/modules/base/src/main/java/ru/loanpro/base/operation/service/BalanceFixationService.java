package ru.loanpro.base.operation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.base.operation.domain.Balance;
import ru.loanpro.base.operation.domain.BalanceFixation;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.operation.exceptions.BalanceAlreadyFixedException;
import ru.loanpro.base.operation.exceptions.DeleteLastFixationException;
import ru.loanpro.base.operation.exceptions.FixBalanceException;
import ru.loanpro.base.operation.exceptions.ForbidToFixBalanceException;
import ru.loanpro.base.operation.repository.BalanceFixationRepository;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.List;

@Service
public class BalanceFixationService {

    private final OperationService operationService;
    private final BalanceFixationRepository repository;

    @Autowired
    public BalanceFixationService(BalanceFixationRepository repository, OperationService operationService) {
        this.repository = repository;
        this.operationService = operationService;
    }

    public BalanceFixation findOneByFixationDate(Department department, Currency currency, LocalDate date) {
        return repository.findOneByFixationDate(department, currency, date);
    }

    @Transactional
    public BalanceFixation save(BalanceFixation balanceFixation) {
        return repository.save(balanceFixation);
    }

    @Transactional
    public void delete(BalanceFixation balanceFixation) {
        repository.delete(balanceFixation);
    }

    public LocalDate getLastFixDate(Department department, Currency currency) {
        return repository.getLastFixDate(department, currency);
    }

    public LocalDate getPreLastFixDate(Department department, Currency currency, LocalDate lastFixDate) {
        return repository.getPreLastFixDate(department, currency, lastFixDate);
    }

    public List<BalanceFixation> findAllBalanceFixationsByDepartment(Department department) {
        return repository.findAllBalanceFixationsByDepartment(department);
    }

    public List<BalanceFixation> findAllBalanceFixationsByDepartment(Department department, Pageable pageable){
        return repository.findAllBalanceFixationsByDepartment(department, pageable);
    }

    public int countFindAllBalanceFixationsByDepartment(Department department){
        return repository.countFindAllBalanceFixationsByDepartment(department);
    }

    @Transactional
    public synchronized void fixBalance(
        Department department, LocalDate systemDate, ZoneId zoneId, Balance balance, User user)
        throws FixBalanceException, ForbidToFixBalanceException, BalanceAlreadyFixedException {

        Currency currency = balance.getCurrency();
        BigDecimal balanceEnd = balance.getBalance();
        LocalDate maxOperationDate;

        Instant instant = operationService.getMaxDateFromOperations(department, currency);
        if (instant != null) {
            maxOperationDate = instant.atZone(zoneId).toLocalDate();
        } else throw new ForbidToFixBalanceException("cashdesk.message.forbidToFix");

        LocalDate minOperationDate =
            operationService.getMinDateFromOperations(department, currency).atZone(zoneId).toLocalDate();

        LocalDate lastFixDate = getLastFixDate(department, balance.getCurrency());

        final LocalDate minDate, maxDate;
        maxDate = maxOperationDate.compareTo(systemDate) >= 0 ? maxOperationDate : systemDate;

        long days;
        if (lastFixDate != null) {//фиксация была
            days = ChronoUnit.DAYS.between(lastFixDate, maxDate);
            if (days == 0) throw new BalanceAlreadyFixedException("cashdesk.message.balanceAlreadyFixed");
            minDate = lastFixDate.plusDays(1);
        } else {//фиксации не было
            days = ChronoUnit.DAYS.between(minOperationDate, maxDate);
            if (days < 0) throw new ForbidToFixBalanceException("cashdesk.message.forbidToFix");
            minDate = minOperationDate;
        }
        try {
            List<Operation> operationList;

            long period = ChronoUnit.DAYS.between(minDate, maxDate) + 1;

            //организуем цикл по дням в обратном порядке
            for (int i = 0; i < period; i++) {
                Instant instantDateMin = minDate.plusDays(period - i - 1).atStartOfDay(zoneId).toInstant();
                Instant instantDateMax = minDate.plusDays(period - i).atStartOfDay(zoneId).toInstant();
                LocalDate tempDate = minDate.plusDays(period - i - 1);

                //расходы
                operationList = operationService.getExpenseOperations(instantDateMin, instantDateMax, department, currency);
                BigDecimal expense = new BigDecimal(0);
                if (operationList != null && operationList.size() > 0) {
                    for (Operation operation : operationList) {
                        operation.setPayerFixed(true);
                        expense = expense.add(operation.getOperationValue());
                    }
                    operationService.saveAll(operationList);
                }
                //доходы
                operationList = operationService.getIncomeOperations(instantDateMin, instantDateMax, department, currency);
                BigDecimal income = new BigDecimal(0);
                if (operationList != null && operationList.size() > 0) {
                    for (Operation operation : operationList) {
                        operation.setRecipientFixed(true);
                        income = income.add(operation.getOperationValue());
                    }
                    operationService.saveAll(operationList);
                }

                BigDecimal balanceStart = balanceEnd.subtract(income.subtract(expense));

                BalanceFixation balanceFixation = new BalanceFixation();
                balanceFixation.setCurrency(currency);
                balanceFixation.setExpensePeriod(expense);
                balanceFixation.setIncomePeriod(income);
                balanceFixation.setDepartment(department);
                balanceFixation.setFixationDate(tempDate);
                balanceFixation.setUser(user);
                balanceFixation.setBalanceStart(balanceStart);
                balanceFixation.setBalanceEnd(balanceEnd);

                balanceEnd = balanceStart;

                save(balanceFixation);
            }
        } catch (Exception e) {
            throw new FixBalanceException("cashdesk.message.errorFixBalance");
        }
    }

    @Transactional
    public synchronized void deleleLastFixation(Balance balance, Department department,
                                                ZoneId zoneId) throws DeleteLastFixationException {

        Currency currency = balance.getCurrency();
        LocalDate preLastFixDate;

        LocalDate lastFixDate = getLastFixDate(department, balance.getCurrency());
        Instant instantLastFixDate;
        Instant instantPreLastFixDate;

        if (lastFixDate != null) {
            instantLastFixDate = lastFixDate.plusDays(1).atStartOfDay(zoneId).toInstant();

            BalanceFixation balanceFixation = findOneByFixationDate(department, currency, lastFixDate);

            preLastFixDate = getPreLastFixDate(department, currency, lastFixDate);
            if (preLastFixDate != null) {
                instantPreLastFixDate = preLastFixDate.plusDays(1).atStartOfDay(zoneId).toInstant();
            } else {
                LocalDate minDate = operationService.getMinDateFromOperations(department, currency).atZone(zoneId).toLocalDate();
                instantPreLastFixDate = minDate.atStartOfDay().atZone(zoneId).toInstant();
            }

            try {
                List<Operation> operationList;

                operationList = operationService.getExpenseOperations(instantPreLastFixDate, instantLastFixDate, department, currency);

                if (operationList != null && operationList.size() > 0) {
                    for (Operation operation : operationList) {
                        operation.setPayerFixed(false);
                    }
                }
                operationService.saveAll(operationList);

                operationList = operationService.getIncomeOperations(instantPreLastFixDate, instantLastFixDate, department, currency);

                if (operationList != null && operationList.size() > 0) {
                    for (Operation operation : operationList) {
                        operation.setRecipientFixed(false);
                    }
                }
                operationService.saveAll(operationList);
                delete(balanceFixation);
            } catch (Exception e) {
                e.printStackTrace();
                throw new DeleteLastFixationException("cashdesk.message.errorDeleteLastFixationBalance");
            }
        }
    }
}

package ru.loanpro.base.printer.dpo;

/**
 * Печатаемый пользователь системы.
 *
 * @author Oleg Brizhevatikh
 */
public class PrintUser {

    private String name;
    private String initName;

    public PrintUser() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitName() {
        return initName;
    }

    public void setInitName(String initName) {
        this.initName = initName;
    }
}

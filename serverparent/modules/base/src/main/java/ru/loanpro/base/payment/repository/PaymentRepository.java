package ru.loanpro.base.payment.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.global.repository.AbstractIdEntityRepository;

import java.util.List;

public interface PaymentRepository extends AbstractIdEntityRepository<Payment> {

    @EntityGraph(attributePaths = {"loan"})
    List<Payment> findAllByLoan(Loan loan);

}

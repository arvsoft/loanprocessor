package ru.loanpro.base.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.exceptions.AccountNotFoundException;
import ru.loanpro.base.account.exceptions.InsufficientAccountBalanceException;
import ru.loanpro.base.account.service.DepartmentAccountService;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.service.JuridicalClientService;
import ru.loanpro.base.client.service.PhysicalClientService;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.exceptions.ObjectNotSavedException;
import ru.loanpro.base.loan.service.LoanService;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.operation.event.UpdateOperationEvent;
import ru.loanpro.base.operation.exceptions.OperationNotSavedException;
import ru.loanpro.base.operation.service.OperationService;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.payment.event.PaymentDeletedException;
import ru.loanpro.base.payment.repository.PaymentRepository;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.directory.PaymentMethod;
import ru.loanpro.global.directory.PaymentSplitType;
import ru.loanpro.global.directory.ScheduleStatus;
import ru.loanpro.global.service.AbstractIdEntityService;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Сервис работы с графиком фактических платежей
 *
 * @author Maksim Askhaev
 */
@Service
public class PaymentService extends AbstractIdEntityService<Payment, PaymentRepository> {

    private final ApplicationEventBus applicationEventBus;
    private final LoanService loanService;
    private final PhysicalClientService physicalClientService;
    private final JuridicalClientService juridicalClientService;
    private final OperationService operationService;
    private final DepartmentAccountService departmentAccountService;
    private final I18N i18n;

    @Autowired
    public PaymentService(ApplicationEventBus applicationEventBus,
                          PaymentRepository repository, LoanService loanService,
                          PhysicalClientService physicalClientService,
                          JuridicalClientService juridicalClientService,
                          OperationService operationService,
                          DepartmentAccountService departmentAccountService,
                          I18N i18n) {
        super(repository);
        this.applicationEventBus = applicationEventBus;
        this.loanService = loanService;
        this.physicalClientService = physicalClientService;
        this.juridicalClientService = juridicalClientService;
        this.operationService = operationService;
        this.departmentAccountService = departmentAccountService;
        this.i18n = i18n;

    }

    public List<Payment> findAllByLoan(Loan loan) {
        return repository.findAllByLoan(loan);
    }

    @Transactional
    public synchronized void save(Payment p, Schedule s, Loan l) throws ObjectNotSavedException {
        try {
            //если заем закрыт досрочно, то меняем статусы у рассчитанных платежей
            //до которых не дошел платеж
            boolean isLoanClosed = Stream.of(
                LoanStatus.CLOSED_ANNULED,
                LoanStatus.CLOSED_FROZEN,
                LoanStatus.CLOSED_NO_OVERDUE,
                LoanStatus.CLOSED_WERE_OVERDUES
            ).anyMatch(item -> item == l.getLoanStatus());

            if (isLoanClosed) {
                l.getSchedules().forEach(item -> {
                    if (item.getOrdNo() > s.getOrdNo()) {
                        item.setStatus(ScheduleStatus.EARLY_CLOSED);
                    }
                });
            }
            l.getPayments().add(repository.save(p));
            loanService.save(l);
        } catch (Exception e) {
            throw new ObjectNotSavedException(e);
        }
    }

    @Transactional
    public synchronized void saveOperations(
        Payment p, Loan l,
        boolean autoOperationReplenishMoneyLoanPayment,
        boolean autoOperationReplenishMoneyLoanPaymentProcessed,
        PaymentSplitType paymentSplitType,
        Instant instant,
        User user,
        Department department)
        throws AccountNotFoundException,
        InsufficientAccountBalanceException, OperationNotSavedException {

        try {
            if (autoOperationReplenishMoneyLoanPayment) {
                //формируем операцию взноса наличных в кассу отдела
                Operation operation = new Operation();
                operation.setDepartment(department);
                operation.setLoan(l);
                operation.setPayment(p);
                operation.setPaymentMethod(PaymentMethod.CASH_REPLENISH);
                operation.setOperationValue(p.getPayment());
                operation.setCurrency(l.getCurrency());
                operation.setUser(user);

                if (l.getBorrower().getClass() == PhysicalClient.class) {

                    PhysicalClient client = physicalClientService.getOneFull(l.getBorrower().getId());

                    Optional<DepartmentAccount> accountDepartment = departmentAccountService.findAllByDepartment(
                        department).stream()
                        .filter(item ->
                            item.getCurrency() == l.getCurrency() &&
                                item.isCashAccount())
                        .findFirst();
                    if (!accountDepartment.isPresent()) throw new AccountNotFoundException(
                        i18n.get("exception.message.departmentCashAccountNotFound"));
                    accountDepartment.ifPresent(operation::setAccountRecipient);
                    operation.setPayer(client);
                } else {
                    //JuridicalClient client = juridicalClientService.getOneFull(l.getBorrower().getId());
                }
                if (autoOperationReplenishMoneyLoanPaymentProcessed) {
                    operationService.save(operation, instant);
                } else operationService.savePrepared(operation, instant);

                applicationEventBus.post(new UpdateOperationEvent(operation));
            }

        } catch (InsufficientAccountBalanceException e){
            throw new InsufficientAccountBalanceException(e.getLocalizedMessage());
        } catch (OperationNotSavedException e){
            throw new OperationNotSavedException(e.getLocalizedMessage());
        } catch (AccountNotFoundException e){
            throw new AccountNotFoundException(e.getLocalizedMessage());
        }
    }

    private void copyOperation(Operation operationTemp, Operation operation) {
        if (operationTemp != null && operation != null) {
            operation.setStatus(operationTemp.getStatus());
            operation.setDepartment(operationTemp.getDepartment());
            operation.setPaymentMethod(operationTemp.getPaymentMethod());
            operation.setCurrency(operationTemp.getCurrency());
            operation.setLoan(operationTemp.getLoan());
            operation.setPayment(operationTemp.getPayment());
            operation.setAccountPayer(operationTemp.getAccountPayer());
            operation.setAccountRecipient(operationTemp.getAccountRecipient());
            operation.setUser(operationTemp.getUser());
        }
    }

    /**
     * Находит и удаляет последний платеж по данному займу
     * В одной транзакции также выполняется обновление
     * статусов займов и рассчитанных платежей с сохранение в базе.
     *
     * @param l - заем в котором следует удалить платеж
     * @throws PaymentDeletedException - исключение
     */
    @Transactional
    public void deleteLast(Loan l) throws PaymentDeletedException {
        LoanStatus loanStatusBeforeTransaction = l.getLoanStatus();
        BigDecimal loanCurrentBalanceBeforeTransaction = l.getCurrentBalance();

        try {
            if (l.getPayments() != null && l.getPayments().size() > 0) {
                //Находим последний платеж в List-списке
                Optional<Payment> p = l.getPayments().stream().reduce((first, second) -> second);

                p.ifPresent(payment -> {
                    //получаем данные
                    LoanStatus loanStatusBefore1 = payment.getLoanStatusBefore();
                    ScheduleStatus scheduleStatusBefore1 = payment.getScheduleStatusBefore();
                    BigDecimal loanBalanceBefore1 = payment.getBalanceBefore();
                    int ordScheduleNo = payment.getScheduleOrdNo();

                    //обновляем данные по займу
                    l.setLoanStatus(loanStatusBefore1);
                    l.setCurrentBalance(loanBalanceBefore1);
                    //обновляем данные по рассчитанному платежу
                    l.getSchedules().forEach(item -> {
                        if (item.getOrdNo() == ordScheduleNo) {
                            item.setStatus(scheduleStatusBefore1);
                        }
                        if (item.getOrdNo() > ordScheduleNo) {
                            item.setStatus(ScheduleStatus.NOT_PAID_NO_OVERDUE);
                        }
                        //scheduleService.save(item);
                    });
                    l.getPayments().remove(payment);
                    loanService.save(l);
                });
            }
        } catch (Exception e) {
            l.setLoanStatus(loanStatusBeforeTransaction);
            l.setCurrentBalance(loanCurrentBalanceBeforeTransaction);
            throw new PaymentDeletedException("exception.message.paymentdeleted");
        }
    }

    public void delete(UUID id) {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

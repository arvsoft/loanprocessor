package ru.loanpro.base.client.repository;

import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.global.repository.AbstractIdEntityRepository;

import java.util.UUID;

/**
 * Репозиторий сущности {@link Client}.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public interface ClientRepository<T extends Client> extends AbstractIdEntityRepository<T> {

    /**
     * Возвращает одного клиента по идентификатору.
     *
     * @param id идентификатор клиента.
     * @return клиент.
     */
    @Query("select c from Client c where c.id = ?1")
    T getOneFull(UUID id);
}

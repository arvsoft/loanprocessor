package ru.loanpro.base.requesthistory.serivce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.requesthistory.domain.RequestHistory;
import ru.loanpro.base.requesthistory.repository.RequestHistoryRepository;

import java.util.List;

/**
 * Сервис работы с историей принятия решений по заявке
 *
 * @author Maksim Askhaev
 */
@Service
public class RequestHistoryService {

    RequestHistoryRepository repository;

    @Autowired
    public RequestHistoryService(RequestHistoryRepository repository) {
        this.repository = repository;
    }

    public RequestHistory getOne(Long id) {
        return repository.getOne(id);
    }

    public List<RequestHistory> findAll() {
        return repository.findAll();
    }

    public List<RequestHistory> findAllByRequest(Request request){
        return repository.findAllByRequest(request);
    }

    public RequestHistory save(RequestHistory d) {
        return repository.save(d);
    }

    @Transactional
    public List<RequestHistory> saveAll(List<RequestHistory> d) {
        return repository.saveAll(d);
    }

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        repository.deleteAll();
    }

}

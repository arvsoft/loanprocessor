package ru.loanpro.base.client.domain;

import ru.loanpro.global.domain.AbstractIdEntity;

import javax.persistence.MappedSuperclass;

/**
 * Базовый класс для подробной информации о клиенте. Содержит поля, совпадающие
 * у физического и юридического лица, но не входящие в список основных полей,
 * требующихся для отображения в общем списке клиентов.
 *
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public class ClientDetails extends AbstractIdEntity {}

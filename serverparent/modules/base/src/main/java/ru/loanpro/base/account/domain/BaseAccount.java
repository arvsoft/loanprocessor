package ru.loanpro.base.account.domain;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.AccountType;
import ru.loanpro.security.domain.converter.CurrencyConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Objects;

/**
 * Класс сущности базового счета
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "accounts", schema = DatabaseSchema.OPERATION)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "entity_type",
    discriminatorType = DiscriminatorType.STRING)
public class BaseAccount extends AbstractIdEntity {
    /**
     * Номер счета
     */
    @Column(name = "number")
    private String number;

    /**
     * Тип счета
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "account_type")
    private AccountType typeAccount;

    /**
     * Банковские реквизиты (для внешнего счета)
     */
    @Column(name = "bank_details")
    private String bankDetails;

    /**
     * Дата открытия счета
     */
    @Column(name = "registration_date")
    private LocalDate registrationDate;

    /**
     * Баланс счета
     */
    @Column(name = "balance", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal balance;

    /**
     * Валюта
     */
    @Column(name = "currency")
    @Convert(converter = CurrencyConverter.class)
    private Currency currency;

    /**
     * Счет по умолчанию, у одного клиента может быть только один счет по умолчанию.
     * Необходимо отслеживать единичность счета по умолчанию при создании/изменении счетов
     */
    @Column(name = "default_account")
    private boolean defaultAccount;

    /**
     * Счет-касса отдела, у одного отдела по данной валюте может быть только один счет-касса
     */
    @Column(name = "cash_account")
    private boolean cashAccount;

    public BaseAccount() {
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setTypeAccount(AccountType typeAccount) {
        this.typeAccount = typeAccount;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(String bankDetails) {
        this.bankDetails = bankDetails;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getNumber() {
        return number;
    }

    public AccountType getTypeAccount() {
        return typeAccount;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public boolean isDefaultAccount() {
        return defaultAccount;
    }

    public void setDefaultAccount(boolean defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    public boolean isCashAccount() {
        return cashAccount;
    }

    public void setCashAccount(boolean cashAccount) {
        this.cashAccount = cashAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BaseAccount that = (BaseAccount) o;
        return Objects.equals(number, that.number) &&
            typeAccount == that.typeAccount &&
            Objects.equals(bankDetails, that.bankDetails) &&
            Objects.equals(registrationDate, that.registrationDate) &&
            Objects.equals(balance, that.balance) &&
            Objects.equals(currency, that.currency) &&
            Objects.equals(defaultAccount, that.defaultAccount) &&
            Objects.equals(cashAccount, that.cashAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), number, typeAccount, bankDetails, registrationDate, balance, currency,
            defaultAccount, cashAccount);
    }
}

package ru.loanpro.base.loan.exceptions;

import ru.loanpro.base.loan.domain.BaseLoan;

/**
 * Базовое исключение, возникающее при попытке выполнить сохранение кредитного продукта, заявки или займа
 *
 * @author Maksim Askhaev
 */
public class BaseLoanNotSavedException extends ObjectNotSavedException {
    public BaseLoanNotSavedException(Exception e){
        super(e);
    }
}

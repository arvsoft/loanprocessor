package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.EstateType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

/**
 * Недвижимость
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "estate", schema = DatabaseSchema.CLIENT)
public class Estate extends AbstractIdEntity {
    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Тип недвижимости.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "estate_type")
    private EstateType estateType;

    /**
     * Описание.
     */
    @Column(name = "description")
    private String description;

    /**
     * Адрес.
     */
    @Column(name = "address")
    private String address;

    /**
     * Рег. номер.
     */
    @Column(name = "inventory_number")
    private String inventoryNumber;

    /**
     * Документ на право собственности
     */
    @Column(name = "document")
    private String document;

    /**
     * Оценочная стоимость
     */
    @Column(name = "estimated_value", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal estimatedValue;

    /**
     * Идентификатор, является ли имущество в залоге
     */
    @Column(name = "pledge")
    private boolean pledge;

    /**
     * Владелец залога
     */
    @Column(name = "pledge_hostage")
    private String pledgeHostage;

    /**
     * Документ о залоге
     */
    @Column(name = "pledge_document")
    private String pledgeDocument;

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "estate_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "estate_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public Estate() {
    }

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public EstateType getEstateType() {
        return estateType;
    }

    public void setEstateType(EstateType estateType) {
        this.estateType = estateType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public BigDecimal getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(BigDecimal estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public boolean isPledge() {
        return pledge;
    }

    public void setPledge(boolean pledge) {
        this.pledge = pledge;
    }

    public String getPledgeHostage() {
        return pledgeHostage;
    }

    public void setPledgeHostage(String pledgeHostage) {
        this.pledgeHostage = pledgeHostage;
    }

    public String getPledgeDocument() {
        return pledgeDocument;
    }

    public void setPledgeDocument(String pledgeDocument) {
        this.pledgeDocument = pledgeDocument;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Estate estate = (Estate) o;
        return Objects.equals(estateType, estate.estateType) &&
            Objects.equals(description, estate.description) &&
            Objects.equals(address, estate.address) &&
            Objects.equals(inventoryNumber, estate.inventoryNumber) &&
            Objects.equals(document, estate.document) &&
            Objects.equals(estimatedValue, estate.estimatedValue) &&
            Objects.equals(pledge, estate.pledge) &&
            Objects.equals(pledgeHostage, estate.pledgeHostage) &&
            Objects.equals(pledgeDocument, estate.pledgeDocument) &&
            Objects.equals(scans, estate.scans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), estateType, description, address, inventoryNumber, document,
            estimatedValue, pledge, pledgeHostage, pledgeDocument, scans);
    }
}

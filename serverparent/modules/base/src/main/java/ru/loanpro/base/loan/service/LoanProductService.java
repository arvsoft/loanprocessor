package ru.loanpro.base.loan.service;

import com.querydsl.core.types.Predicate;
import com.vaadin.data.provider.QuerySortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.loanpro.base.loan.domain.LoanProduct;
import ru.loanpro.base.loan.domain.QLoanProduct;
import ru.loanpro.base.loan.repository.LoanProductRepository;
import ru.loanpro.global.exceptions.EntityDeleteException;

import java.util.List;
import java.util.stream.Stream;

/**
 * Сервис работы с кредитным продуктом
 *
 * @author Maksim Askhaev
 */
@Service
public class LoanProductService extends BaseLoanService<LoanProduct, LoanProductRepository> {

    @Autowired
    public LoanProductService(LoanProductRepository repository) {
        super(repository);
    }

    /**
     * Сохраняет кредитный продукт.
     * Временный номер не используется по причине отсутствия у кредитного продукта номера.
     *
     * @param loanProduct кредитный продукт.
     * @param draftNumber временный номер. Не используется.
     * @return сохранённый кредитный продукт.
     */
    @Override
    public LoanProduct save(
        LoanProduct loanProduct, String draftNumber){
        return super.save(loanProduct);
    }

    /**
     * {@inheritDoc}
     * Отфильтровываются все удалённые кредитные продукты.
     */
    @Override
    public Integer count(Predicate predicate) {
        return super.count(QLoanProduct.loanProduct.deleted.isFalse().and(predicate));
    }

    /**
     * {@inheritDoc}
     * Отфильтровываются все удалённые кредитные продукты.
     */
    @Override
    public Stream<LoanProduct> findAll(Predicate predicate, List<QuerySortOrder> sortOrder, int offset, int limit) {
        return super.findAll(QLoanProduct.loanProduct.deleted.isFalse().and(predicate), sortOrder, offset, limit);
    }

    /**
     * {@inheritDoc}
     * Ограничение по удалению кредитного продукта - существование кредитного продукта.
     */
    @Override
    public boolean canBeDeleted(LoanProduct entity) {
        return entity.getId() != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(LoanProduct entity) throws EntityDeleteException {
        entity.setDeleted(true);
        repository.save(entity);
    }
}

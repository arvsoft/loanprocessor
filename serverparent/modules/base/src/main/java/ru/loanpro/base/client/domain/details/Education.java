package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.directory.domain.EducationLevel;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

/**
 * Образование
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "education", schema = DatabaseSchema.CLIENT)
public class Education extends AbstractIdEntity {

    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Уровень образования
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "level")
    private EducationLevel level;

    /**
     * Специальность
     */
    @Column(name = "specialty")
    private String specialty;

    /**
     * Образовательное учреждение
     */
    @Column(name = "institution")
    private String institution;

    /**
     * Год выпуска
     */
    @Column(name = "issue_year")
    private int issueYear;

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "education_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "education_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public Education() {
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public EducationLevel getLevel() {
        return level;
    }

    public void setLevel(EducationLevel level) {
        this.level = level;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public int getIssueYear() {
        return issueYear;
    }

    public void setIssueYear(int issueYear) {
        this.issueYear = issueYear;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Education education = (Education) o;
        return Objects.equals(level, education.level) &&
            Objects.equals(specialty, education.specialty) &&
            Objects.equals(institution, education.institution) &&
            Objects.equals(issueYear, education.issueYear) &&
            Objects.equals(scans, education.scans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), level, specialty, institution, issueYear, scans);
    }
}
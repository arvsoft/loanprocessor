package ru.loanpro.base.client.domain;

import com.google.common.base.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDate;

/**
 * Сущность юридического лица.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("JURIDICAL")
public class JuridicalClient extends Client {

    /**
     * Подробная информация о юридическом лице.
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "juridical_client_details_id")
    private JuridicalClientDetails details;

    /**
     * Полное название.
     */
    @Column(name = "full_name")
    private String fullName;

    /**
     * Сокращённое название.
     */
    @Column(name = "short_name")
    private String shortName;

    /**
     * ИНН юридического лица.
     */
    @Column(name = "inn")
    private String inn;

    /**
     * ОГРН юридического лица.
     */
    @Column(name = "ogrn")
    private String ogrn;

    /**
     * Дата регистрации юридического лица.
     */
    @Column(name = "register_date")
    private LocalDate registerDate;

    public JuridicalClient() {
    }

    /**
     * Метод удобного получения отображаемого имени для размещения на карточках или в списках.
     *
     * @return отображаемое имя.
     */
    @Override
    public String getDisplayName() {
        return shortName;
    }

    public JuridicalClientDetails getDetails() {
        return details;
    }

    public void setDetails(JuridicalClientDetails details) {
        this.details = details;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        JuridicalClient that = (JuridicalClient) o;
        return Objects.equal(fullName, that.fullName) &&
            Objects.equal(shortName, that.shortName) &&
            Objects.equal(inn, that.inn) &&
            Objects.equal(ogrn, that.ogrn) &&
            Objects.equal(registerDate, that.registerDate);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
            super.hashCode(),
            fullName,
            shortName,
            inn,
            ogrn,
            registerDate);
    }
}

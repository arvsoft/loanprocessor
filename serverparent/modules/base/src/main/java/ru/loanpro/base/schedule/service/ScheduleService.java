package ru.loanpro.base.schedule.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.service.LoanService;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.base.schedule.repository.ScheduleRepository;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.directory.ScheduleStatus;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервис работы с графиком рассчитанных платежей
 *
 * @author Maksim Askhaev
 */
@Service
public class ScheduleService {

    private final ScheduleRepository repository;
    private final LoanService loanService;

    @Autowired
    public ScheduleService(ScheduleRepository repository, LoanService loanService) {
        this.repository = repository;
        this.loanService = loanService;
    }

    public Schedule getOne(Long id) {
        return repository.getOne(id);
    }

    public List<Schedule> findAll() {
        return repository.findAll();
    }

    public List<Schedule> findAllByLoan(BaseLoan baseLoan) {
        return repository.findAllByLoan(baseLoan);
    }

    public Schedule save(Schedule d) { return repository.save(d); }

    @Transactional
    public List<Schedule> saveAll(List<Schedule> d) {return repository.saveAll(d);}

    public void delete(Long id) {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll(){
        repository.deleteAll();
    }

    @Transactional
    public void deleteAllScheduleByLoan(BaseLoan baseLoan) {
        repository.deleteAllByLoan(baseLoan);
    }

    /**
     * Обновляет статусы  рассчитанных платежей и займов.
     *
     * @param date дата, на которую необходимо проверить наличие просроченных платежей и займов
     */
    @Transactional
    public void updateOverdueScheduleStatuses(LocalDate date) {
        List<Schedule> schedules = repository.findOverdueSchedules(date);

        schedules.forEach(item -> item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
        repository.saveAll(schedules);

        HashSet<Loan> loans = schedules.stream()
            .map(Schedule::getLoan)
            .map(Loan.class::cast)
            .distinct()
            .collect(Collectors.toCollection(HashSet::new));

        if (schedules.size()>0){
            loans.forEach(item -> item.setLoanStatus(LoanStatus.OPENED_CURRENT_OVERDUE));
            loanService.saveAll(loans);
        }
    }

    /**
     *  Обновляет статусы рассчитанных платежей для займа, включая статус займа.

     * @param date дата, на которую необходимо проверить наличие просроченных платежей для займа
     * @param loan заем, который необходимо проверить на просрочку
     */
    @Transactional
    public void updateOverdueScheduleStatusesByLoan(LocalDate date, Loan loan) {
        List<Schedule> schedules = repository.findOverdueSchedulesByLoan(date, loan);

        if (schedules.size() > 0) {
            schedules.forEach(item -> {
                loan.getSchedules().forEach(item1->{
                    if (item.getOrdNo() == item1.getOrdNo()){
                        item1.setStatus(ScheduleStatus.NOT_PAID_OVERDUE);
                    }
                });
                item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE);
            });
            repository.saveAll(schedules);

            loan.setLoanStatus(LoanStatus.OPENED_CURRENT_OVERDUE);
            loanService.save(loan);
        }

        List<Schedule> schedules1 = repository.findNoOverdueSchedulesByLoan(date, loan);
        if (schedules1.size() > 0) {
            schedules1.forEach(item -> {
                loan.getSchedules().forEach(item1->{
                    if (item.getOrdNo() == item1.getOrdNo()){
                        item1.setStatus(ScheduleStatus.NOT_PAID_NO_OVERDUE);
                    }
                });
                item.setStatus(ScheduleStatus.NOT_PAID_NO_OVERDUE);
            });
            repository.saveAll(schedules);
        }

        if (loan.getSchedules().stream().anyMatch(item->item.getStatus()==ScheduleStatus.NOT_PAID_OVERDUE)){
            loan.setLoanStatus(LoanStatus.OPENED_CURRENT_OVERDUE);
            loanService.save(loan);
        } else {
            if (loan.getLoanStatus() == LoanStatus.OPENED_CURRENT_OVERDUE ||
                loan.getLoanStatus() == LoanStatus.OPENED_WERE_OVERDUES){
                loan.setLoanStatus(LoanStatus.OPENED_WERE_OVERDUES);
            } else loan.setLoanStatus(LoanStatus.OPENED_NO_OVERDUE);
            loanService.save(loan);
        }
    }
}

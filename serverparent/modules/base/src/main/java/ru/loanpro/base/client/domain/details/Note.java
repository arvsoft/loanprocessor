package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.global.DatabaseSchema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Заметки клиента
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "note", schema = DatabaseSchema.CLIENT)
public class Note extends AbstractIdEntity {

    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Заметка
     */
    @Column(name = "content")
    private String content;

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Note note = (Note) o;
        return Objects.equals(content, note.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), content);
    }

}


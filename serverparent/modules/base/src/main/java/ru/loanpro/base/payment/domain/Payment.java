package ru.loanpro.base.payment.domain;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.directory.PaymentStatus;
import ru.loanpro.global.directory.ScheduleStatus;
import ru.loanpro.security.domain.Department;
import ru.loanpro.global.annotation.EntityName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Класс сущности графика фактических платежей
 *
 * @author Maksim Askhaev
 */
@Entity
@EntityName("entity.name.payment")
@Table(name = "payments", schema = DatabaseSchema.LOAN)
public class Payment extends AbstractIdEntity {

    /**
     * Займ
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id", nullable = false)
    private Loan loan;

    /**
     * Департамент(отдел) платежа (не путать с отделом оформления займа)
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id", nullable = false)
    private Department departmentPayment;

    /**
     * Порядковый номер
     */
    @Column(name = "ord_no")
    private int ordNo;

    /**
     * Порядковый номер по графику
     */
    @Column(name = "schedule_ord_no")
    private int scheduleOrdNo;

    /**
     * Порядковый номер пролонгации
     * в которой выполнен данный платеж
     */
    @Column(name = "prolong_ord_no")
    private int prolongOrdNo;

    /**
     * Статус платежа
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PaymentStatus status;

    /**
     * Дата платежа фактическая
     */
    @Column(name = "paydate")
    private LocalDate payDate;

    /**
     * Срок платежа
     */
    @Column(name = "term")
    private long term;

    /**
     * Срок просрочки
     */
    @Column(name = "overdue_term")
    private long overdueTerm;

    /**
     * Размер основной части (оплачено фактически)
     */
    @Column(name = "principal")
    private BigDecimal principal;

    /**
     * Размер процентов (оплачено фактически)
     */
    @Column(name = "interest")
    private BigDecimal interest;

    /**
     * Размер неустойки (оплачено фактически)
     */
    @Column(name = "penalty")
    private BigDecimal penalty;

    /**
     * Размер штрафа (оплачено фактически)
     */
    @Column(name = "fine")
    private BigDecimal fine;

    /**
     * Размер платежа (оплачено фактически)
     */
    @Column(name = "payment")
    private BigDecimal payment;

    /**
     * Остаток основной части долга после данным платежом
     */
    @Column(name = "balance_before")
    private BigDecimal balanceBefore;

    /**
     * Остаток основной части долга после данного платежа
     */
    @Column(name = "balance_after")
    private BigDecimal balanceAfter;

    /**
     * Состояние оплачиваемой позиции в графике рассчитанных платежей до платежа
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "schedule_status_before")
    private ScheduleStatus scheduleStatusBefore;

    /**
     * Состояние займа до платежа
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "loan_status_before")
    private LoanStatus loanStatusBefore;

    /**
     * Размер основной части (остаток на начало платежа)
     */
    @Column(name = "principal_b")
    private BigDecimal principalB;

    /**
     * Размер процентов (остаток на начало платежа)
     */
    @Column(name = "interest_b")
    private BigDecimal interestB;

    /**
     * Размер неустойки (остаток на начало платежа)
     */
    @Column(name = "penalty_b")
    private BigDecimal penaltyB;

    /**
     * Размер штрафа (остаток на начало платежа)
     */
    @Column(name = "fine_b")
    private BigDecimal fineB;

    /**
     * Размер платежа (остаток на начало платежа)
     */
    @Column(name = "payment_b")
    private BigDecimal paymentB;

    /**
     * Размер основной части (рассчитанный на дату платежа)
     */
    @Column(name = "principal_c")
    private BigDecimal principalC;

    /**
     * Размер процентов (рассчитанный на дату платежа)
     */
    @Column(name = "interest_c")
    private BigDecimal interestC;

    /**
     * Размер неустойки (рассчитанный на дату платежа)
     */
    @Column(name = "penalty_c")
    private BigDecimal penaltyC;

    /**
     * Размер штрафа (рассчитанный на дату платежа)
     */
    @Column(name = "fine_c")
    private BigDecimal fineC;

    /**
     * Размер платежа (рассчитанный на дату платежа)
     */
    @Column(name = "payment_c")
    private BigDecimal paymentC;

    /**
     * Размер основной части (сумма к оплате)
     */
    @Column(name = "principal_s")
    private BigDecimal principalS;

    /**
     * Размер процентов (сумма к оплате)
     */
    @Column(name = "interest_s")
    private BigDecimal interestS;

    /**
     * Размер неустойки (сумма к оплате)
     */
    @Column(name = "penalty_s")
    private BigDecimal penaltyS;

    /**
     * Размер штрафа (сумма к оплате)
     */
    @Column(name = "fine_s")
    private BigDecimal fineS;

    /**
     * Размер платежа (сумма к оплате)
     */
    @Column(name = "payment_s")
    private BigDecimal paymentS;

    /**
     * Размер основной части (остаток на конец платежа)
     */
    @Column(name = "principal_e")
    private BigDecimal principalE;

    /**
     * Размер процентов (остаток на конец платежа)
     */
    @Column(name = "interest_e")
    private BigDecimal interestE;

    /**
     * Размер неустойки (остаток на конец платежа)
     */
    @Column(name = "penalty_e")
    private BigDecimal penaltyE;

    /**
     * Размер штрафа (остаток на конец платежа)
     */
    @Column(name = "fine_e")
    private BigDecimal fineE;

    /**
     * Размер платежа (остаток на конец платежа)
     */
    @Column(name = "payment_e")
    private BigDecimal paymentE;

    public Payment() {
    }

    public Payment(BigDecimal balance){
        ordNo = 0;
        scheduleOrdNo = 0;
        term = 0;
        overdueTerm = 0;

        principal = new BigDecimal(0);
        interest = new BigDecimal(0);
        penalty = new BigDecimal(0);
        fine = new BigDecimal(0);
        payment = new BigDecimal(0);

        principalB = new BigDecimal(0);
        interestB = new BigDecimal(0);
        penaltyB = new BigDecimal(0);
        fineB = new BigDecimal(0);
        paymentB = new BigDecimal(0);

        principalE = new BigDecimal(0);
        interestE = new BigDecimal(0);
        penaltyE = new BigDecimal(0);
        fineE = new BigDecimal(0);
        paymentE = new BigDecimal(0);

        balanceBefore = balance;
        balanceAfter = balance;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public Department getDepartmentPayment() {
        return departmentPayment;
    }

    public void setDepartmentPayment(Department departmentPayment) {
        this.departmentPayment = departmentPayment;
    }

    public int getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(int ordNo) {
        this.ordNo = ordNo;
    }

    public int getScheduleOrdNo() {
        return scheduleOrdNo;
    }

    public void setScheduleOrdNo(int scheduleOrdNo) {
        this.scheduleOrdNo = scheduleOrdNo;
    }

    public int getProlongOrdNo() {
        return prolongOrdNo;
    }

    public void setProlongOrdNo(int prolongOrdNo) {
        this.prolongOrdNo = prolongOrdNo;
    }

    public LocalDate getPayDate() {
        return payDate;
    }

    public void setPayDate(LocalDate payDate) {
        this.payDate = payDate;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public long getTerm() {
        return term;
    }

    public void setTerm(long term) {
        this.term = term;
    }

    public long getOverdueTerm() {
        return overdueTerm;
    }

    public void setOverdueTerm(long overdueTerm) {
        this.overdueTerm = overdueTerm;
    }

    public BigDecimal getPrincipal() {
        return principal;
    }

    public void setPrincipal(BigDecimal principal) {
        this.principal = principal;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getPenalty() {
        return penalty;
    }

    public void setPenalty(BigDecimal penalty) {
        this.penalty = penalty;
    }

    public BigDecimal getFine() {
        return fine;
    }

    public void setFine(BigDecimal fine) {
        this.fine = fine;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public BigDecimal getPrincipalB() {
        return principalB;
    }

    public void setPrincipalB(BigDecimal principalB) {
        this.principalB = principalB;
    }

    public BigDecimal getInterestB() {
        return interestB;
    }

    public void setInterestB(BigDecimal interestB) {
        this.interestB = interestB;
    }

    public BigDecimal getPenaltyB() {
        return penaltyB;
    }

    public void setPenaltyB(BigDecimal penaltyB) {
        this.penaltyB = penaltyB;
    }

    public BigDecimal getFineB() {
        return fineB;
    }

    public void setFineB(BigDecimal fineB) {
        this.fineB = fineB;
    }

    public BigDecimal getPaymentB() {
        return paymentB;
    }

    public void setPaymentB(BigDecimal paymentB) {
        this.paymentB = paymentB;
    }

    public BigDecimal getPrincipalC() {
        return principalC;
    }

    public void setPrincipalC(BigDecimal principalC) {
        this.principalC = principalC;
    }

    public BigDecimal getInterestC() {
        return interestC;
    }

    public void setInterestC(BigDecimal interestC) {
        this.interestC = interestC;
    }

    public BigDecimal getPenaltyC() {
        return penaltyC;
    }

    public void setPenaltyC(BigDecimal penaltyC) {
        this.penaltyC = penaltyC;
    }

    public BigDecimal getFineC() {
        return fineC;
    }

    public void setFineC(BigDecimal fineC) {
        this.fineC = fineC;
    }

    public BigDecimal getPaymentC() {
        return paymentC;
    }

    public void setPaymentC(BigDecimal paymentC) {
        this.paymentC = paymentC;
    }

    public BigDecimal getPrincipalS() {
        return principalS;
    }

    public void setPrincipalS(BigDecimal principalS) {
        this.principalS = principalS;
    }

    public BigDecimal getInterestS() {
        return interestS;
    }

    public void setInterestS(BigDecimal interestS) {
        this.interestS = interestS;
    }

    public BigDecimal getPenaltyS() {
        return penaltyS;
    }

    public void setPenaltyS(BigDecimal penaltyS) {
        this.penaltyS = penaltyS;
    }

    public BigDecimal getFineS() {
        return fineS;
    }

    public void setFineS(BigDecimal fineS) {
        this.fineS = fineS;
    }

    public BigDecimal getPaymentS() {
        return paymentS;
    }

    public void setPaymentS(BigDecimal paymentS) {
        this.paymentS = paymentS;
    }

    public BigDecimal getPrincipalE() {
        return principalE;
    }

    public void setPrincipalE(BigDecimal principalE) {
        this.principalE = principalE;
    }

    public BigDecimal getInterestE() {
        return interestE;
    }

    public void setInterestE(BigDecimal interestE) {
        this.interestE = interestE;
    }

    public BigDecimal getPenaltyE() {
        return penaltyE;
    }

    public void setPenaltyE(BigDecimal penaltyE) {
        this.penaltyE = penaltyE;
    }

    public BigDecimal getFineE() {
        return fineE;
    }

    public void setFineE(BigDecimal fineE) {
        this.fineE = fineE;
    }

    public BigDecimal getPaymentE() {
        return paymentE;
    }

    public void setPaymentE(BigDecimal paymentE) {
        this.paymentE = paymentE;
    }

    public ScheduleStatus getScheduleStatusBefore() {
        return scheduleStatusBefore;
    }

    public void setScheduleStatusBefore(ScheduleStatus scheduleStatusBefore) {
        this.scheduleStatusBefore = scheduleStatusBefore;
    }

    public LoanStatus getLoanStatusBefore() {
        return loanStatusBefore;
    }

    public void setLoanStatusBefore(LoanStatus loanStatusBefore) {
        this.loanStatusBefore = loanStatusBefore;
    }

    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", ordNo=" + ordNo +
            ", scheduleOrdNo=" + scheduleOrdNo +
            ", payDate=" + payDate +
            ", state=" + status +
            ", term=" + term +
            ", overdueTerm=" + overdueTerm +
            ", principal=" + principal +
            ", interest=" + interest +
            ", penalty=" + penalty +
            ", fine=" + fine +
            ", payment=" + payment +
            ", balancebefore=" + balanceBefore +
            ", balanceAfter=" + balanceAfter +
            ", scheduleStatusBefore=" + scheduleStatusBefore +
            ", loanStatusBefore=" + loanStatusBefore +
            ", principalB=" + principalB +
            ", interestB=" + interestB +
            ", penaltyB=" + penaltyB +
            ", fineB=" + fineB +
            ", paymentB=" + paymentB +
            ", principalE=" + principalE +
            ", interestE=" + interestE +
            ", penaltyE=" + penaltyE +
            ", fineE=" + fineE +
            ", paymentE=" + paymentE +
            ", principalC=" + principalC +
            ", interestC=" + interestC +
            ", penaltyC=" + penaltyC +
            ", fineC=" + fineC +
            ", paymentC=" + paymentC +
            ", principalS=" + principalS +
            ", interestS=" + interestS +
            ", penaltyS=" + penaltyS +
            ", fineS=" + fineS +
            ", paymentS=" + paymentS +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Payment payment1 = (Payment) o;
        return Objects.equals(ordNo, payment1.ordNo) &&
            Objects.equals(scheduleOrdNo, payment1.scheduleOrdNo) &&
            Objects.equals(prolongOrdNo, payment1.prolongOrdNo) &&
            Objects.equals(term, payment1.term) &&
            Objects.equals(overdueTerm, payment1.overdueTerm) &&
            Objects.equals(departmentPayment, payment1.departmentPayment) &&
            Objects.equals(status, payment1.status) &&
            Objects.equals(payDate, payment1.payDate) &&
            Objects.equals(principal, payment1.principal) &&
            Objects.equals(interest, payment1.interest) &&
            Objects.equals(penalty, payment1.penalty) &&
            Objects.equals(fine, payment1.fine) &&
            Objects.equals(payment, payment1.payment) &&
            Objects.equals(balanceBefore, payment1.balanceBefore) &&
            Objects.equals(balanceAfter, payment1.balanceAfter) &&
            Objects.equals(scheduleStatusBefore, payment1.scheduleStatusBefore) &&
            Objects.equals(loanStatusBefore, payment1.loanStatusBefore) &&
            Objects.equals(principalB, payment1.principalB) &&
            Objects.equals(interestB, payment1.interestB) &&
            Objects.equals(penaltyB, payment1.penaltyB) &&
            Objects.equals(fineB, payment1.fineB) &&
            Objects.equals(paymentB, payment1.paymentB) &&
            Objects.equals(principalC, payment1.principalC) &&
            Objects.equals(interestC, payment1.interestC) &&
            Objects.equals(penaltyC, payment1.penaltyC) &&
            Objects.equals(fineC, payment1.fineC) &&
            Objects.equals(paymentC, payment1.paymentC) &&
            Objects.equals(principalS, payment1.principalS) &&
            Objects.equals(interestS, payment1.interestS) &&
            Objects.equals(penaltyS, payment1.penaltyS) &&
            Objects.equals(fineS, payment1.fineS) &&
            Objects.equals(paymentS, payment1.paymentS) &&
            Objects.equals(principalE, payment1.principalE) &&
            Objects.equals(interestE, payment1.interestE) &&
            Objects.equals(penaltyE, payment1.penaltyE) &&
            Objects.equals(fineE, payment1.fineE) &&
            Objects.equals(paymentE, payment1.paymentE);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), departmentPayment, ordNo, scheduleOrdNo, prolongOrdNo, status, payDate, term,
            overdueTerm, principal, interest, penalty, fine, payment, balanceBefore, balanceAfter, scheduleStatusBefore,
            loanStatusBefore, principalB, interestB, penaltyB, fineB, paymentB, principalC, interestC, penaltyC, fineC,
            paymentC, principalS, interestS, penaltyS, fineS, paymentS, principalE, interestE, penaltyE, fineE, paymentE);
    }
}

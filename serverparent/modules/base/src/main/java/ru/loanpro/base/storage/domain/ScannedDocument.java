package ru.loanpro.base.storage.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Сущность сканированных образов документов.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("SCANNED_DOCUMENT")
public class ScannedDocument extends BaseStorageFile {

    public ScannedDocument() {
    }
}

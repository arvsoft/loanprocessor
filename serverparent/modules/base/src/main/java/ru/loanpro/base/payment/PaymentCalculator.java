package ru.loanpro.base.payment;

import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.OverdueDaysCalculationType;
import ru.loanpro.global.directory.PaymentStatus;
import ru.loanpro.global.directory.PenaltyType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.directory.ScheduleStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Класс Builder для расчета:
 * 1) размера первоочередного платежа на дату погашения (для расчета платежа по займу).
 * 2) размера всех платежей на дату погашения (для расчета общей задолженности по займу - в отчет о просроченных займах)
 *
 * <p>
 * Выполняет расчет размера платежа на дату погашения для всех видов графиков платежей
 * с учетом определения неустойки, штрафа при просрочках. Формирует сущность платежа (Payment)
 * с состоянием.
 * <p>
 * Передаваемые параметры должны быть согласованы
 * Правила согласования представлены в документации.
 *
 * @author Maksim Askhaev
 */
public class PaymentCalculator {

    private int scaleValue;             //точность для денежного типа
    private int scaleMode;              //режим округления (enum BigDecimal)

    private LocalDate loanIssueDate;    //дата выдачи займа
    private LocalDate calculationDate;  //дата расчетная
    private BigDecimal loanBalance;     //размер займа
    private CalcScheme calcScheme;      //тип расчета дифференцированный/аннуитетный
    private ReturnTerm returnTerm;      //тип срока возврата - ежемесячно/еженедельно/в конце срока/индивидуальный
    private BigDecimal interestRate;    //размер процентной ставки, например 5%
    private InterestType interestType;  //тип процентной ставки, %/месяц, % в неделю, % в день, % за весь период и пр.
    private int loanTerm;               //срок займа, например 5,
    private boolean replaceDate;        //идентификатор, смещать ли дату первого платежа на заданное число месяца, например, платеж будет 10 числа каждого месяца
    private int replaceDateValue;       //смещенная дата первого платежа, например 10-е число, если включен параметр ReplaceDate
    private boolean replaceOverMonth;   //идентификатор, смещать ли дату через один месяц, если срок первого периода меньше одного месяца,
    private boolean principalLastPay;   //идентификатор, включать ли оплату основной части полностью в последний платеж
    private boolean interestBalance;    //идентификатор, рассчитывать ли проценты от суммы займа, а не от остатка долга
    private boolean noInterestFD;       //идентификатор, не рассчитывать ли в первые дни проценты
    private int noInterestFDValue;      //кол-во дней в первом платеже без расчета процентов
    private boolean includeIssueDate;   //включать ли дату выдачи в расчет процентов
    private int daysInYear;             //кол-во дней в году, берется из настроек

    private PenaltyType penaltyType;    //тип неустойки
    private BigDecimal penaltyValue;    //размер неустойки, % в день
    private boolean fine;               //берется ли штраф
    private BigDecimal fineValue;       //размер штрафа
    private boolean fineOneTime;        //единоразовый штраф
    private boolean interestStrictly;   //рассчитывать ли проценты строго по графику
    private boolean interestOverdue;    //рассчитывать ли проценты в просроченный период
    private boolean interestFirstDays;  //рассчитывать ли проценты в любой из первых дней как за N дней
    private int interestFirstDaysValue; //кол-во первых N дней

    //из настроек
    private boolean summationPenalties; //суммировать ли неустойку из предыдущего платежа с текущим
    private boolean summationFines;     //суммировать ли штраф из предыдущего платежа с текущим
    private OverdueDaysCalculationType overdueDaysCalculationType;//способ расчета

    private LocalDate datePayment;
    private LocalDate dateLastPayment;
    private double sumBalanceAfter;
    private double sumBalanceBefore;
    private int numberSchedule;
    private long days;
    private long overdueDays;
    private PaymentStatus paymentStatus;
    private ScheduleStatus scheduleStatusBefore;

    private double sumPrincipal;//составляющие платежа фактически к оплате
    private double sumInterest; //равна группе S
    private double sumPenalty;
    private double sumFine;
    private double sumPayment;

    private double sumPrincipalB;//составляющие платежа на начало платежа (недоплата по предыдущим платежам
    private double sumInterestB;
    private double sumPenaltyB;
    private double sumFineB;
    private double sumPaymentB;

    private double sumPrincipalC;//составляющие рассчитанного платежа согласно графику
    private double sumInterestC; //или на дату погашения с учетом просрочек
    private double sumPenaltyC;
    private double sumFineC;
    private double sumPaymentC;

    private double sumPrincipalS;//составляющие платежа (сумма групп B и C)
    private double sumInterestS;
    private double sumPenaltyS;
    private double sumFineS;
    private double sumPaymentS;

    private double sumPrincipalE;//составляющие платежа остаток после оплаты
    private double sumInterestE; //разность (группа С - группа фактически к оплате)
    private double sumPenaltyE;
    private double sumFineE;
    private double sumPaymentE;

    private List<Payment> payments;
    private List<Schedule> schedules;

    private List<Schedule> schedulesClone;
    private List<Payment> paymentsClone;

    private PaymentCalculator() {
    }

    public static PaymentCalculator instance() {
        return new PaymentCalculator();
    }

    public ScheduleStatus getScheduleStatusBefore() {
        return scheduleStatusBefore;
    }

    public PaymentCalculator setScaleValue(int scaleValue) {
        this.scaleValue = scaleValue;
        return this;
    }

    public double getSumBalanceAfter() {
        return sumBalanceAfter;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public int getNumberSchedule() {
        return numberSchedule;
    }

    public PaymentCalculator setScaleMode(int scaleMode) {
        this.scaleMode = scaleMode;
        return this;
    }

    public PaymentCalculator setLoanIssueDate(LocalDate loanIssueDate) {
        this.loanIssueDate = loanIssueDate;
        return this;
    }

    public PaymentCalculator setCalculationDate(LocalDate calculationDate) {
        this.calculationDate = calculationDate;
        return this;
    }

    public PaymentCalculator setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
        return this;
    }

    public PaymentCalculator setCalcScheme(CalcScheme calcScheme) {
        this.calcScheme = calcScheme;
        return this;
    }

    public PaymentCalculator setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
        return this;
    }

    public PaymentCalculator setInterestRateType(InterestType interestType) {
        this.interestType = interestType;
        return this;
    }

    public PaymentCalculator setLoanTerm(int loanTerm) {
        this.loanTerm = loanTerm;
        return this;
    }

    public PaymentCalculator setReturnTerm(ReturnTerm returnTerm) {
        this.returnTerm = returnTerm;
        return this;
    }

    public PaymentCalculator setReplaceDate(boolean replaceDate) {
        this.replaceDate = replaceDate;
        return this;
    }

    public PaymentCalculator setReplaceDateValue(int replaceDateValue) {
        this.replaceDateValue = replaceDateValue;
        return this;
    }

    public PaymentCalculator setReplaceOverMonth(boolean replaceOverMonth) {
        this.replaceOverMonth = replaceOverMonth;
        return this;
    }

    public PaymentCalculator setPrincipalLastPay(boolean principalLastPay) {
        this.principalLastPay = principalLastPay;
        return this;
    }

    public PaymentCalculator setInterestBalance(boolean interestBalance) {
        this.interestBalance = interestBalance;
        return this;
    }

    public PaymentCalculator setNoInterestFD(boolean noInterestFD) {
        this.noInterestFD = noInterestFD;
        return this;
    }

    public PaymentCalculator setNoInterestFDValue(int noInterestFDValue) {
        this.noInterestFDValue = noInterestFDValue;
        return this;
    }

    public PaymentCalculator setIncludeIssueDate(boolean includeIssueDate) {
        this.includeIssueDate = includeIssueDate;
        return this;
    }

    public PaymentCalculator setDaysInYear(int daysInYear) {
        this.daysInYear = daysInYear;
        return this;
    }

    public PaymentCalculator setPayments(List<Payment> payments) {
        this.payments = payments;
        return this;
    }

    public PaymentCalculator setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
        return this;
    }

    public PaymentCalculator setPenaltyType(PenaltyType penaltyType) {
        this.penaltyType = penaltyType;
        return this;
    }

    public PaymentCalculator setPenaltyValue(BigDecimal penaltyValue) {
        this.penaltyValue = penaltyValue;
        return this;
    }

    public PaymentCalculator setFine(boolean fine) {
        this.fine = fine;
        return this;
    }

    public PaymentCalculator setFineValue(BigDecimal fineValue) {
        this.fineValue = fineValue;
        return this;
    }

    public PaymentCalculator setFineOneTime(boolean fineOneTime) {
        this.fineOneTime = fineOneTime;
        return this;
    }

    public PaymentCalculator setInterestStrictly(boolean interestStrictly) {
        this.interestStrictly = interestStrictly;
        return this;
    }

    public PaymentCalculator setInterestOverdue(boolean interestOverdue) {
        this.interestOverdue = interestOverdue;
        return this;
    }

    public PaymentCalculator setInterestFirstDays(boolean interestFirstDays) {
        this.interestFirstDays = interestFirstDays;
        return this;
    }

    public PaymentCalculator setInterestFirstDaysValue(int interestFirstDaysValue) {
        this.interestFirstDaysValue = interestFirstDaysValue;
        return this;
    }

    public PaymentCalculator setSummationPenalties(boolean summationPenalties) {
        this.summationPenalties = summationPenalties;
        return this;
    }

    public PaymentCalculator setSummationFines(boolean summationFines) {
        this.summationFines = summationFines;
        return this;
    }

    public PaymentCalculator setOverdueDaysCalculationType(OverdueDaysCalculationType overdueDaysCalculationType) {
        this.overdueDaysCalculationType = overdueDaysCalculationType;
        return this;
    }

    public PaymentCalculator build() {
        return this;
    }

    /**
     * Вычисляет платеж
     *
     * @return - сущность платежа
     */
    public Payment calculate() {

        paymentsClone = new ArrayList<>(payments.size());
        paymentsClone.addAll(payments);

        schedulesClone = new ArrayList<>(schedules.size());
        schedulesClone.addAll(schedules);

        int ordNo = paymentsClone == null || paymentsClone.size() == 0 ? 1 : paymentsClone.size() + 1;

        if (paymentsClone != null && paymentsClone.size() > 0) {
            Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
            lastPayment.ifPresent(payment -> sumBalanceBefore = payment.getBalanceAfter().doubleValue());
        } else {
            sumBalanceBefore = loanBalance.doubleValue();
        }

        overdueDaysCalculationType = overdueDaysCalculationType == null ?
            OverdueDaysCalculationType.FROM_DATE_ACCORDING_SCHEDULE : overdueDaysCalculationType;

        switch (penaltyType) {
            case NOT_DEFINED:
                calculateT0(true);
                break;
            case FROM_LOAN_VALUE:
                calculateT1(true);
                break;
            case FROM_PAYMENT:
                calculateT2(true);
                break;
            case FROM_BALANCE:
                calculateT3(true);
                break;
            case FROM_SUM_OF_INDEBTEDNESS:
                calculateT4(true);
                break;
        }

        sumPaymentC = sumPrincipalC + sumInterestC + sumPenaltyC + sumFineC;
        sumPaymentB = sumPrincipalB + sumInterestB + sumPenaltyB + sumFineB;

        sumPrincipalS = sumPrincipalB + sumPrincipalC;
        sumInterestS = sumInterestB + sumInterestC;

        if (summationFines) sumFineS = sumFineB + sumFineC;
        else sumFineS = sumFineC;
        if (summationPenalties) sumPenaltyS = sumPenaltyB + sumPenaltyC;
        else sumPenaltyS = sumPenaltyC;

        sumPenaltyS = sumPenaltyB + sumPenaltyC;
        sumPaymentS = sumPaymentB + sumPaymentC;

        sumPrincipal = sumPrincipalS;
        sumInterest = sumInterestS;
        sumFine = sumFineS;
        sumPenalty = sumPenaltyS;
        sumPayment = sumPaymentS;

        sumPrincipalE = 0;
        sumInterestE = 0;
        sumPenaltyE = 0;
        sumFineE = 0;
        sumPaymentE = 0;

        sumBalanceAfter = sumBalanceBefore - sumPrincipal;

        Payment payment = new Payment();
        payment.setPayDate(calculationDate);
        payment.setOrdNo(ordNo);
        payment.setScheduleOrdNo(numberSchedule);
        payment.setTerm(days);
        payment.setStatus(paymentStatus);
        payment.setOverdueTerm(overdueDays);
        payment.setBalanceBefore(new BigDecimal(sumBalanceBefore).setScale(scaleValue, scaleMode));
        payment.setBalanceAfter(new BigDecimal(sumBalanceAfter).setScale(scaleValue, scaleMode));
        payment.setPrincipal(new BigDecimal(sumPrincipal).setScale(scaleValue, scaleMode));
        payment.setInterest(new BigDecimal(sumInterest).setScale(scaleValue, scaleMode));
        payment.setPenalty(new BigDecimal(sumPenalty).setScale(scaleValue, scaleMode));
        payment.setFine(new BigDecimal(sumFine).setScale(scaleValue, scaleMode));
        payment.setPayment(new BigDecimal(sumPayment).setScale(scaleValue, scaleMode));
        payment.setPrincipalB(new BigDecimal(sumPrincipalB).setScale(scaleValue, scaleMode));
        payment.setInterestB(new BigDecimal(sumInterestB).setScale(scaleValue, scaleMode));
        payment.setPenaltyB(new BigDecimal(sumPenaltyB).setScale(scaleValue, scaleMode));
        payment.setFineB(new BigDecimal(sumFineB).setScale(scaleValue, scaleMode));
        payment.setPaymentB(new BigDecimal(sumPaymentB).setScale(scaleValue, scaleMode));
        payment.setPrincipalC(new BigDecimal(sumPrincipalC).setScale(scaleValue, scaleMode));
        payment.setInterestC(new BigDecimal(sumInterestC).setScale(scaleValue, scaleMode));
        payment.setPenaltyC(new BigDecimal(sumPenaltyC).setScale(scaleValue, scaleMode));
        payment.setFineC(new BigDecimal(sumFineC).setScale(scaleValue, scaleMode));
        payment.setPaymentC(new BigDecimal(sumPaymentC).setScale(scaleValue, scaleMode));
        payment.setPrincipalS(new BigDecimal(sumPrincipalS).setScale(scaleValue, scaleMode));
        payment.setInterestS(new BigDecimal(sumInterestS).setScale(scaleValue, scaleMode));
        payment.setPenaltyS(new BigDecimal(sumPenaltyS).setScale(scaleValue, scaleMode));
        payment.setFineS(new BigDecimal(sumFineS).setScale(scaleValue, scaleMode));
        payment.setPaymentS(new BigDecimal(sumPaymentS).setScale(scaleValue, scaleMode));
        payment.setPrincipalE(new BigDecimal(sumPrincipalE).setScale(scaleValue, scaleMode));
        payment.setInterestE(new BigDecimal(sumInterestE).setScale(scaleValue, scaleMode));
        payment.setPenaltyE(new BigDecimal(sumPenaltyE).setScale(scaleValue, scaleMode));
        payment.setFineE(new BigDecimal(sumFineE).setScale(scaleValue, scaleMode));
        payment.setPaymentE(new BigDecimal(sumPaymentE).setScale(scaleValue, scaleMode));
        return payment;
    }

    /**
     * Вычисляет платеж
     *
     * @return - сущность платежа
     */
    public List<Payment> calculateOverduePayments() {

        List<Payment> paymentList = new ArrayList<>();

        paymentsClone = new ArrayList<>(payments.size());
        paymentsClone.addAll(payments);
        schedulesClone = new ArrayList<>(schedules.size());
        schedulesClone.addAll(schedules);

        if (paymentsClone != null && paymentsClone.size() > 0) {
            Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
            lastPayment.ifPresent(payment -> sumBalanceBefore = payment.getBalanceAfter().doubleValue());
        } else {
            sumBalanceBefore = loanBalance.doubleValue();
        }

        overdueDaysCalculationType = overdueDaysCalculationType == null ?
            OverdueDaysCalculationType.FROM_DATE_ACCORDING_SCHEDULE : overdueDaysCalculationType;

        int ordNo = paymentsClone == null || paymentsClone.size() == 0 ? 1 : paymentsClone.size() + 1;

        int count = 0;//считаем кол-во недоплаченных/просроченных платежей
        int scheduleIndex = 0;//индекс первого недоплаченного/просроченного платежа
        for (int i = 0; i < schedulesClone.size(); i++) {
            final int k = i;
            boolean b = Stream.of(
                ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());

            if (b) count++;
            if (count == 1) {
                scheduleIndex = k;
            }
        }

        //цикл по недоплаченным/просроченным платежам
        for (int i = 0; i < count; i++) {
            boolean bFirst = true;

            if (i > 0) {
                bFirst = false;
                //schedulesClone.get(scheduleIndex).setStatus(ScheduleStatus.PAID_FULLY);

                Payment paymentAdd = new Payment();
                paymentAdd.setOrdNo(ordNo);
                paymentAdd.setScheduleOrdNo(scheduleIndex);
                paymentAdd.setPayDate(schedulesClone.get(scheduleIndex).getDate());
                paymentAdd.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
                paymentAdd.setPrincipalB(new BigDecimal(0));
                paymentAdd.setInterestB(new BigDecimal(0));
                paymentAdd.setPenaltyB(new BigDecimal(0));
                paymentAdd.setFineB(new BigDecimal(0));
                paymentAdd.setBalanceBefore(new BigDecimal(sumBalanceBefore));
                paymentsClone.add(paymentAdd);
            }

            switch (penaltyType) {
                case NOT_DEFINED:
                    calculateT0(bFirst);
                    break;
                case FROM_LOAN_VALUE:
                    calculateT1(bFirst);
                    break;
                case FROM_PAYMENT:
                    calculateT2(bFirst);
                    break;
                case FROM_BALANCE:
                    calculateT3(bFirst);
                    break;
                case FROM_SUM_OF_INDEBTEDNESS:
                    calculateT4(bFirst);
                    break;
            }
            schedulesClone.get(scheduleIndex).setStatus(ScheduleStatus.PAID_FULLY);

            sumPaymentC = sumPrincipalC + sumInterestC + sumPenaltyC + sumFineC;
            sumPaymentB = sumPrincipalB + sumInterestB + sumPenaltyB + sumFineB;

            sumPrincipalS = sumPrincipalB + sumPrincipalC;
            sumInterestS = sumInterestB + sumInterestC;

            if (summationFines) sumFineS = sumFineB + sumFineC;
            else sumFineS = sumFineC;
            if (summationPenalties) sumPenaltyS = sumPenaltyB + sumPenaltyC;
            else sumPenaltyS = sumPenaltyC;

            sumPenaltyS = sumPenaltyB + sumPenaltyC;
            sumPaymentS = sumPaymentB + sumPaymentC;

            sumPrincipal = sumPrincipalS;
            sumInterest = sumInterestS;
            sumFine = sumFineS;
            sumPenalty = sumPenaltyS;
            sumPayment = sumPaymentS;

            sumPrincipalE = 0;
            sumInterestE = 0;
            sumPenaltyE = 0;
            sumFineE = 0;
            sumPaymentE = 0;

            sumBalanceAfter = sumBalanceBefore - sumPrincipal;

            Payment payment = new Payment();
            payment.setPayDate(schedulesClone.get(scheduleIndex).getDate());
            payment.setOrdNo(ordNo);
            payment.setScheduleOrdNo(numberSchedule);
            payment.setTerm(days);
            payment.setStatus(paymentStatus);
            payment.setOverdueTerm(overdueDays);
            payment.setBalanceBefore(new BigDecimal(sumBalanceBefore).setScale(scaleValue, scaleMode));
            payment.setBalanceAfter(new BigDecimal(sumBalanceAfter).setScale(scaleValue, scaleMode));
            payment.setPrincipal(new BigDecimal(sumPrincipal).setScale(scaleValue, scaleMode));
            payment.setInterest(new BigDecimal(sumInterest).setScale(scaleValue, scaleMode));
            payment.setPenalty(new BigDecimal(sumPenalty).setScale(scaleValue, scaleMode));
            payment.setFine(new BigDecimal(sumFine).setScale(scaleValue, scaleMode));
            payment.setPayment(new BigDecimal(sumPayment).setScale(scaleValue, scaleMode));
            payment.setPrincipalB(new BigDecimal(sumPrincipalB).setScale(scaleValue, scaleMode));
            payment.setInterestB(new BigDecimal(sumInterestB).setScale(scaleValue, scaleMode));
            payment.setPenaltyB(new BigDecimal(sumPenaltyB).setScale(scaleValue, scaleMode));
            payment.setFineB(new BigDecimal(sumFineB).setScale(scaleValue, scaleMode));
            payment.setPaymentB(new BigDecimal(sumPaymentB).setScale(scaleValue, scaleMode));
            payment.setPrincipalC(new BigDecimal(sumPrincipalC).setScale(scaleValue, scaleMode));
            payment.setInterestC(new BigDecimal(sumInterestC).setScale(scaleValue, scaleMode));
            payment.setPenaltyC(new BigDecimal(sumPenaltyC).setScale(scaleValue, scaleMode));
            payment.setFineC(new BigDecimal(sumFineC).setScale(scaleValue, scaleMode));
            payment.setPaymentC(new BigDecimal(sumPaymentC).setScale(scaleValue, scaleMode));
            payment.setPrincipalS(new BigDecimal(sumPrincipalS).setScale(scaleValue, scaleMode));
            payment.setInterestS(new BigDecimal(sumInterestS).setScale(scaleValue, scaleMode));
            payment.setPenaltyS(new BigDecimal(sumPenaltyS).setScale(scaleValue, scaleMode));
            payment.setFineS(new BigDecimal(sumFineS).setScale(scaleValue, scaleMode));
            payment.setPaymentS(new BigDecimal(sumPaymentS).setScale(scaleValue, scaleMode));
            payment.setPrincipalE(new BigDecimal(sumPrincipalE).setScale(scaleValue, scaleMode));
            payment.setInterestE(new BigDecimal(sumInterestE).setScale(scaleValue, scaleMode));
            payment.setPenaltyE(new BigDecimal(sumPenaltyE).setScale(scaleValue, scaleMode));
            payment.setFineE(new BigDecimal(sumFineE).setScale(scaleValue, scaleMode));
            payment.setPaymentE(new BigDecimal(sumPaymentE).setScale(scaleValue, scaleMode));
            paymentList.add(payment);

            ordNo++;
            scheduleIndex++;
        }

        return paymentList;
    }


    /**
     * Расчет платежа по типу "без неустойки"
     */
    private void calculateT0(boolean bFirst) {
        long addingDay = 0;
        long excludeDay = 0;
        int index;

        index = paymentsClone != null ? paymentsClone.size() : 0;

        if (index > 0) {//если есть хотя бы один платеж
            dateLastPayment = paymentsClone.get(index - 1).getPayDate();
        } else {//платежей не было
            dateLastPayment = loanIssueDate;
            if (includeIssueDate) addingDay = 1;
            if (noInterestFD) excludeDay = noInterestFDValue;
        }

        //кол-во дней для начисления процентов
        days = DAYS.between(dateLastPayment, calculationDate) + addingDay - excludeDay;

        if (index == 0 && interestFirstDays && (returnTerm == ReturnTerm.IN_THE_END_OF_TERM ||
            returnTerm == ReturnTerm.INDIVIDUALLY)) {
            if (DAYS.between(dateLastPayment, calculationDate) < interestFirstDaysValue) {
                days = interestFirstDaysValue + addingDay - excludeDay;
            }
        }
        days = days >= 0 ? days : 0;

        for (int i = 0; i < schedulesClone.size(); i++) {
            final int k = i;
            boolean b;

            if (bFirst) {
                b = Stream.of(
                    ScheduleStatus.NOT_PAID_NO_OVERDUE,
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            } else {
                b = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            }

            if (b) {
                boolean b1 = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT).anyMatch(e -> e == schedulesClone.get(k).getStatus());

                if (b1) {//частично оплаченный платеж
                    numberSchedule = k + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE; //доплата

                    //находим последний платеж по текущему номеру платежа
                    index = -1;
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        for (int j = 0; j < paymentsClone.size(); j++) {
                            if (paymentsClone.get(j).getScheduleOrdNo() == schedulesClone.get(i).getOrdNo()) {
                                index = j;
                            }
                        }
                    }
                    if (index >= 0) {
                        sumPrincipalB = paymentsClone.get(index).getPrincipalE().doubleValue();
                        sumInterestB = paymentsClone.get(index).getInterestE().doubleValue();
                        sumPenaltyB = paymentsClone.get(index).getPenaltyE().doubleValue();
                        sumFineB = paymentsClone.get(index).getFineE().doubleValue();
                        sumPaymentB = paymentsClone.get(index).getPaymentE().doubleValue();
                    }
                    sumPrincipalC = 0;
                }

                b1 = Stream.of(ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
                if (b1) {//просроченный платеж без оплаты
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE;
                    sumPrincipalC = schedulesClone.get(k).getPrincipal().doubleValue();
                    if (sumBalanceBefore < sumPrincipalC) sumPrincipalC = sumBalanceBefore;
                }

                b1 = Stream.of(ScheduleStatus.NOT_PAID_NO_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
                if (b1) {//платеж без оплаты, без просрочки
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_FULL;
                    sumPrincipalC = schedulesClone.get(k).getPrincipal().doubleValue();
                    if (sumBalanceBefore < sumPrincipalC) sumPrincipalC = sumBalanceBefore;
                }

                overdueDays = DAYS.between(schedulesClone.get(k).getDate(), calculationDate);
                overdueDays = overdueDays >= 0 ? overdueDays : 0;

                if (!interestOverdue) days = days - overdueDays;

                if (interestStrictly) {
                    days = schedulesClone.get(k).getTerm();
                    sumInterestC = schedulesClone.get(k).getInterest().doubleValue();
                } else {
                    if (interestBalance) {
                        if (days == schedulesClone.get(k).getTerm()) {
                            if (paymentStatus != PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE) {
                                sumInterestC = schedulesClone.get(k).getInterest().doubleValue();
                            } else
                                sumInterestC = calculateInterest(loanBalance.doubleValue(), interestRate.doubleValue(), interestType, days, daysInYear);
                        } else
                            sumInterestC = calculateInterest(loanBalance.doubleValue(), interestRate.doubleValue(), interestType, days, daysInYear);
                    } else {
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                    }
                }

                sumPaymentC = sumPrincipalC + sumInterestC;

                if (fine) {
                    if (overdueDays > 0) {
                        boolean f = true;
                        if (fineOneTime) {
                            if (index > 0) {
                                for (int j = 0; j < paymentsClone.size() - 1; j++) {
                                    if (paymentsClone.get(j).getFine().doubleValue() > 0) {
                                        f = false;
                                    }
                                }
                            }
                        }
                        if (f) sumFineC = fineValue.doubleValue();
                    }
                }
                sumPenaltyC = 0;
                numberSchedule = k + 1;
                datePayment = schedulesClone.get(k).getDate();
                break;
            }
        }
        scheduleStatusBefore = schedulesClone.get(numberSchedule - 1).getStatus();
    }

    /**
     * Расчет платежа по типу неустойки "от суммы займа"
     */
    private void calculateT1(boolean bFirst) {
        long addingDay = 0;
        long excludeDay = 0;
        int index;

        //в графике рассч. платежей ищем первый платеж, который оплачен
        //частично или полностью не оплачен
        for (int i = 0; i < schedulesClone.size(); i++) {
            final int k = i;

            boolean b;

            if (bFirst) {
                b = Stream.of(
                    ScheduleStatus.NOT_PAID_NO_OVERDUE,
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            } else {
                b = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            }

            if (b) {
                boolean b1 = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT).anyMatch(e -> e == schedulesClone.get(k).getStatus());
                if (b1) {//частично оплаченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE; //доплата

                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    //находим последний платеж по текущему номеру платежа
                    index = -1;
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        for (int j = 0; j < paymentsClone.size(); j++) {
                            if (paymentsClone.get(j).getScheduleOrdNo() == schedulesClone.get(i).getOrdNo()) {
                                index = j;
                            }
                        }
                    }
                    if (index >= 0) {
                        sumPrincipalB = paymentsClone.get(index).getPrincipalE().doubleValue();
                        sumInterestB = paymentsClone.get(index).getInterestE().doubleValue();
                        sumPenaltyB = paymentsClone.get(index).getPenaltyE().doubleValue();
                        sumFineB = paymentsClone.get(index).getFineE().doubleValue();
                        sumPaymentB = paymentsClone.get(index).getPaymentE().doubleValue();
                        dateLastPayment = paymentsClone.get(index).getPayDate();
                    }

                    //зануляем все, кроме процентов и пени
                    sumPrincipalC = 0;
                    sumFineC = 0;

                    if (overdueDays > 0) {//есть просрочка
                        days = DAYS.between(dateLastPayment, calculationDate);
                        days = days > 0 ? days : 0;

                        if (!interestOverdue) {//расчет процентов в просроченный период отключен
                            sumInterestC = 0;
                            //расчет неустойки
                            if (days >= overdueDays) {
                                sumPenaltyC = calculatePenalty(loanBalance.doubleValue(), penaltyValue.doubleValue(), overdueDays);
                            } else {
                                sumPenaltyC = calculatePenalty(loanBalance.doubleValue(), penaltyValue.doubleValue(), days);
                            }
                        } else {//расчет процентов в просроченный период включен
                            sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                            sumPaymentC = sumPrincipalC + sumInterestC;
                            //расчет неустойки
                            if (days >= overdueDays) {
                                sumPenaltyC = calculatePenalty(loanBalance.doubleValue(), penaltyValue.doubleValue(), overdueDays);
                            } else {
                                sumPenaltyC = calculatePenalty(loanBalance.doubleValue(), penaltyValue.doubleValue(), days);
                            }

                            if (fine) {
                                if (overdueDays > 0) {
                                    boolean f = true;
                                    if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                        if (paymentsClone != null) {
                                            f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                        }
                                    }
                                    if (f) sumFineC = fineValue.doubleValue();
                                }
                            }
                        }
                    } else {//просрочки нет
                        days = DAYS.between(dateLastPayment, calculationDate);
                        days = days > 0 ? days : 0;
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                        sumPaymentC = sumPrincipalC + sumInterestC;
                        sumPenaltyC = 0;
                    }
                    days = days > 0 ? days : 0;
                    break;
                }

                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_OVERDUE) {//просроченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE;//по графику с просрочкой

                    //для просроченного займа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    days = schedulesClone.get(i).getTerm();
                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    //расчет неустойки
                    if (!interestOverdue) {
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                    } else {
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days + overdueDays, daysInYear);
                    }

                    //расчет неустойки
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                        lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                        if (DAYS.between(schedulesClone.get(i).getDate(), dateLastPayment) > 0) {
                            overdueDays = DAYS.between(dateLastPayment, calculationDate);
                            overdueDays = overdueDays > 0 ? overdueDays : 0;
                        }
                    }
                    sumPenaltyC = calculatePenalty(loanBalance.doubleValue(), penaltyValue.doubleValue(), overdueDays);

                    //расчет штрафа
                    if (fine) {
                        if (overdueDays > 0) {
                            boolean f = true;
                            if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                if (paymentsClone != null && paymentsClone.size() > 0) {
                                    f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                }
                            }
                            if (f) sumFineC = fineValue.doubleValue();
                        }
                    }
                    break;
                }
                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_NO_OVERDUE) {//НЕОПЛАЧЕННЫЙ, НЕПРОСРОЧЕННЫЙ ПЛАТЕЖ
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_FULL;//по графику в полном объеме

                    //для нормального платежа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    if (interestStrictly) {//проценты строго по графику
                        days = schedulesClone.get(i).getTerm();
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                    } else {
                        if (paymentsClone != null && paymentsClone.size() > 0) {//факт. платежи были
                            Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                            lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                            days = DAYS.between(dateLastPayment, calculationDate);
                        } else {//первый платеж
                            dateLastPayment = loanIssueDate;
                            if (includeIssueDate) addingDay = 1;
                            if (noInterestFD) excludeDay = noInterestFDValue;

                            days = DAYS.between(dateLastPayment, calculationDate) + addingDay - excludeDay;

                            if (interestFirstDays && (returnTerm == ReturnTerm.IN_THE_END_OF_TERM ||
                                returnTerm == ReturnTerm.INDIVIDUALLY)) {
                                if (DAYS.between(dateLastPayment, calculationDate) < interestFirstDaysValue) {
                                    days = interestFirstDaysValue + addingDay - excludeDay;
                                }
                            }
                            days = days - excludeDay;
                        }
                        days = days > 0 ? days : 0;

                        if (schedulesClone.get(i).getTerm() == days) {
                            sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                        } else {
                            if (interestBalance) {
                                sumInterestC = calculateInterest(
                                    loanBalance.doubleValue(), interestRate.doubleValue(), interestType, days, daysInYear);
                            } else {
                                sumInterestC = calculateInterest(
                                    sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                            }
                        }
                    }
                    overdueDays = 0;
                    sumPenaltyC = 0;
                    sumFineC = 0;
                    break;
                }
            }
        }
        scheduleStatusBefore = schedulesClone.get(numberSchedule - 1).getStatus();
    }

    /**
     * Расчет платежа по типу неустойки "от суммы платежа"
     */
    private void calculateT2(boolean bFirst) {
        long addingDay = 0;
        long excludeDay = 0;
        int index;
        LocalDate calculationDate1;
        Optional<Payment> lastPayment;

        //в графике рассч. платежей ищем первый платеж, который оплачен
        //частично или полностью не оплачен
        for (int i = 0; i < schedulesClone.size(); i++) {
            final int k = i;

            boolean b;

            if (bFirst) {
                b = Stream.of(
                    ScheduleStatus.NOT_PAID_NO_OVERDUE,
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            } else {
                b = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            }

            if (b) {
                boolean b1 = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT).anyMatch(e -> e == schedulesClone.get(k).getStatus());
                if (b1) {//частично оплаченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE; //доплата

                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    //находим последний платеж по текущему номеру платежа
                    index = -1;
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        for (int j = 0; j < paymentsClone.size(); j++) {
                            if (paymentsClone.get(j).getScheduleOrdNo() == schedulesClone.get(i).getOrdNo()) {
                                index = j;
                            }
                        }
                    }
                    if (index >= 0) {
                        sumPrincipalB = paymentsClone.get(index).getPrincipalE().doubleValue();
                        sumInterestB = paymentsClone.get(index).getInterestE().doubleValue();
                        sumPenaltyB = paymentsClone.get(index).getPenaltyE().doubleValue();
                        sumFineB = paymentsClone.get(index).getFineE().doubleValue();
                        sumPaymentB = paymentsClone.get(index).getPaymentE().doubleValue();
                        dateLastPayment = paymentsClone.get(index).getPayDate();
                    }

                    if (overdueDays > 0) {//есть просрочка
                        calculationDate1 = interestOverdue ? calculationDate : schedulesClone.get(i).getDate();

                        days = DAYS.between(dateLastPayment, calculationDate1);
                        days = days > 0 ? days : 0;

                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);

                        //расчет штрафа
                        if (fine) {
                            boolean f = true;
                            if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                if (paymentsClone != null && paymentsClone.size() > 0) {
                                    f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                }
                            }
                            if (f) sumFineC = fineValue.doubleValue();
                        }
                    } else {
                        days = DAYS.between(dateLastPayment, calculationDate);
                        days = days > 0 ? days : 0;
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                        sumFineC = 0;
                    }

                    //зануляем все, кроме пени
                    sumPrincipalC = 0;
                    double sumPaymentB1 = sumPrincipalB + sumInterestB;
                    double sumPaymentC1 = sumPrincipalC + sumInterestC;

                    //расчет пени
                    sumPenaltyC = calculatePenalty(sumPaymentB1 + sumPaymentC1, penaltyValue.doubleValue(), overdueDays);
                    break;
                }
                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_OVERDUE) {//просроченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE;//по графику с просрочкой

                    //для просроченного займа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    days = schedulesClone.get(i).getTerm();

                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    if (!interestOverdue) {
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                    } else {
                        if (paymentsClone != null && paymentsClone.size() > 0) {
                            lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                            lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                            days = DAYS.between(dateLastPayment, calculationDate);
                            overdueDays = 0;
                        }
                        sumInterestC = calculateInterest(
                            sumBalanceBefore, interestRate.doubleValue(), interestType, days + overdueDays, daysInYear);
                    }

                    //расчет неустойки
                    double sumPaymentC1 = sumPrincipalC + sumInterestC;

                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                        lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());

                        if (DAYS.between(schedulesClone.get(i).getDate(), dateLastPayment) > 0) {//если дата фактического платежа была позже даты
                            if (overdueDaysCalculationType == OverdueDaysCalculationType.FROM_DATE_ACCORDING_SCHEDULE) {
                                overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                                overdueDays = overdueDays > 0 ? overdueDays : 0;
                            } else if (overdueDaysCalculationType == OverdueDaysCalculationType.FROM_DATE_OF_LAST_PAYMENT) {
                                lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                                lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                                overdueDays = DAYS.between(dateLastPayment, calculationDate);
                            }
                        } else {
                            overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                            overdueDays = overdueDays > 0 ? overdueDays : 0;
                        }
                    }
                    sumPenaltyC = calculatePenalty(sumPaymentC1, penaltyValue.doubleValue(), overdueDays);

                    //расчет штрафа
                    if (fine) {
                        if (overdueDays > 0) {
                            boolean f = true;
                            if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                if (paymentsClone != null && paymentsClone.size() > 0) {
                                    f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                }
                            }
                            if (f) sumFineC = fineValue.doubleValue();
                        }
                    }
                    break;
                }
                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_NO_OVERDUE) {//не просроченный, не оплаченный
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_FULL;//по графику в полном объеме

                    //для нормального платежа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    if (interestStrictly) {//проценты строго по графику
                        days = schedulesClone.get(i).getTerm();
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                    } else {
                        if (paymentsClone != null && paymentsClone.size() > 0) {//факт. платежи были
                            lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                            lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                            days = DAYS.between(dateLastPayment, calculationDate);
                            days = days > 0 ? days : 0;
                        } else {//первый платеж
                            dateLastPayment = loanIssueDate;
                            if (includeIssueDate) addingDay = 1;
                            if (noInterestFD) excludeDay = noInterestFDValue;

                            days = DAYS.between(dateLastPayment, calculationDate) + addingDay - excludeDay;

                            if (interestFirstDays && (returnTerm == ReturnTerm.IN_THE_END_OF_TERM ||
                                returnTerm == ReturnTerm.INDIVIDUALLY)) {
                                if (DAYS.between(dateLastPayment, calculationDate) < interestFirstDaysValue) {
                                    days = interestFirstDaysValue + addingDay - excludeDay;
                                }
                            }
                        }
                        days = days > 0 ? days : 0;

                        if (schedulesClone.get(i).getTerm() == days) {
                            sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                        } else {
                            if (interestBalance) {
                                sumInterestC = calculateInterest(
                                    loanBalance.doubleValue(), interestRate.doubleValue(), interestType, days, daysInYear);
                            } else {
                                sumInterestC = calculateInterest(
                                    sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                            }
                        }
                    }
                    overdueDays = 0;
                    sumPenaltyC = 0;
                    sumFineC = 0;
                    break;
                }
            }
        }
        scheduleStatusBefore = schedulesClone.get(numberSchedule - 1).getStatus();
    }

    /**
     * Расчет платежа по типу неустойки "от суммы остатка"
     */
    private void calculateT3(boolean bFirst) {
        long addingDay = 0;
        long excludeDay = 0;
        int index;
        LocalDate calculationDate1;

        //в графике рассч. платежей ищем первый платеж, который оплачен
        //частично или полностью не оплачен
        for (int i = 0; i < schedulesClone.size(); i++) {
            final int k = i;

            boolean b;

            if (bFirst) {
                b = Stream.of(
                    ScheduleStatus.NOT_PAID_NO_OVERDUE,
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            } else {
                b = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            }

            if (b) {
                boolean b1 = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT).anyMatch(e -> e == schedulesClone.get(k).getStatus());
                if (b1) {//частично оплаченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE; //доплата

                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    //находим последний платеж по текущему номеру платежа
                    index = -1;
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        for (int j = 0; j < paymentsClone.size(); j++) {
                            if (paymentsClone.get(j).getScheduleOrdNo() == schedulesClone.get(i).getOrdNo()) {
                                index = j;
                            }
                        }
                    }
                    if (index >= 0) {
                        sumPrincipalB = paymentsClone.get(index).getPrincipalE().doubleValue();
                        sumInterestB = paymentsClone.get(index).getInterestE().doubleValue();
                        sumPenaltyB = paymentsClone.get(index).getPenaltyE().doubleValue();
                        sumFineB = paymentsClone.get(index).getFineE().doubleValue();
                        sumPaymentB = paymentsClone.get(index).getPaymentE().doubleValue();
                        dateLastPayment = paymentsClone.get(index).getPayDate();
                    }

                    if (overdueDays > 0) {//есть просрочка
                        calculationDate1 = interestOverdue ? calculationDate : schedulesClone.get(i).getDate();
                        days = DAYS.between(dateLastPayment, calculationDate1);
                        days = days > 0 ? days : 0;
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);

                        if (fine) {
                            boolean f = true;
                            if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                if (paymentsClone != null) {
                                    f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                }
                            }
                            if (f) sumFineC = fineValue.doubleValue();
                        }
                    } else {
                        days = DAYS.between(dateLastPayment, calculationDate);
                        days = days > 0 ? days : 0;
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                    }

                    //зануляем все, кроме пени
                    sumPrincipalC = 0;
                    sumPaymentC = sumPrincipalC + sumInterestC;

                    //расчет пени
                    if (DAYS.between(schedulesClone.get(i).getDate(), dateLastPayment) > 0) {//если дата фактического платежа была позже даты
                        days = DAYS.between(dateLastPayment, calculationDate);
                    } else {
                        days = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    }
                    days = days > 0 ? days : 0;
                    sumPenaltyC = calculatePenalty(sumBalanceBefore, penaltyValue.doubleValue(), days);
                    break;
                }

                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_OVERDUE) {//просроченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE;//по графику с просрочкой

                    //для просроченного займа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    days = schedulesClone.get(i).getTerm();
                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    if (!interestOverdue) {
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                    } else {
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days + overdueDays, daysInYear);
                    }

                    sumPaymentC = sumPrincipalC + sumInterestC;

                    //расчет неустойки
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                        lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                        if (DAYS.between(schedulesClone.get(i).getDate(), dateLastPayment) > 0) {
                            overdueDays = DAYS.between(dateLastPayment, calculationDate);
                            overdueDays = overdueDays > 0 ? overdueDays : 0;
                        }
                    }
                    sumPenaltyC = calculatePenalty(sumBalanceBefore, penaltyValue.doubleValue(), overdueDays);

                    //расчет штрафа
                    if (fine) {
                        if (overdueDays > 0) {
                            boolean f = true;
                            if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                if (paymentsClone != null && paymentsClone.size() > 0) {
                                    f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                }
                            }
                            if (f) sumFineC = fineValue.doubleValue();
                        }
                    }
                    break;
                }
                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_NO_OVERDUE) {//неоплаченный, непросроченный
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_FULL;//по графику в полном объеме

                    //для нормального платежа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    if (interestStrictly) {//проценты строго по графику
                        days = schedulesClone.get(i).getTerm();
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                    } else {
                        if (paymentsClone != null && paymentsClone.size() > 0) {//факт. платежи были
                            Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                            lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                            days = DAYS.between(dateLastPayment, calculationDate);
                        } else {//первый платеж
                            dateLastPayment = loanIssueDate;
                            if (includeIssueDate) addingDay = 1;
                            if (noInterestFD) excludeDay = noInterestFDValue;

                            days = DAYS.between(dateLastPayment, calculationDate) + addingDay - excludeDay;

                            if (interestFirstDays && (returnTerm == ReturnTerm.IN_THE_END_OF_TERM ||
                                returnTerm == ReturnTerm.INDIVIDUALLY)) {
                                if (DAYS.between(dateLastPayment, calculationDate) < interestFirstDaysValue) {
                                    days = interestFirstDaysValue + addingDay - excludeDay;
                                }
                            }
                        }
                        days = days > 0 ? days : 0;

                        if (schedulesClone.get(i).getTerm() == days) {
                            sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                        } else {
                            if (interestBalance) {
                                sumInterestC = calculateInterest(
                                    loanBalance.doubleValue(), interestRate.doubleValue(), interestType, days, daysInYear);
                            } else {
                                sumInterestC = calculateInterest(
                                    sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                            }
                        }
                    }
                    overdueDays = 0;
                    sumPenaltyC = 0;
                    sumFineC = 0;
                    sumPaymentC = sumPrincipalC + sumInterestC;
                    break;
                }
            }
        }
        scheduleStatusBefore = schedulesClone.get(numberSchedule - 1).getStatus();
    }

    /**
     * Расчет платежа по типу неустойки "от суммы задолженности"
     */
    private void calculateT4(boolean bFirst) {
        long addingDay = 0;
        long excludeDay = 0;
        int index;
        double sumInterestC1 = 0;

        //в графике рассч. платежей ищем первый платеж, который оплачен
        //частично или полностью не оплачен
        for (int i = 0; i < schedulesClone.size(); i++) {
            final int k = i;

            boolean b;

            if (bFirst) {
                b = Stream.of(
                    ScheduleStatus.NOT_PAID_NO_OVERDUE,
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            } else {
                b = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT,
                    ScheduleStatus.NOT_PAID_OVERDUE).anyMatch(e -> e == schedulesClone.get(k).getStatus());
            }

            if (b) {
                boolean b1 = Stream.of(
                    ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_FINE_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT,
                    ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT).anyMatch(e -> e == schedulesClone.get(k).getStatus());
                if (b1) {//частично оплаченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE; //доплата

                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    //находим последний платеж по текущему номеру платежа
                    index = -1;
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        for (int j = 0; j < paymentsClone.size(); j++) {
                            if (paymentsClone.get(j).getScheduleOrdNo() == schedulesClone.get(i).getOrdNo()) {
                                index = j;
                            }
                        }
                    }
                    if (index >= 0) {
                        sumPrincipalB = paymentsClone.get(index).getPrincipalE().doubleValue();
                        sumInterestB = paymentsClone.get(index).getInterestE().doubleValue();
                        sumPenaltyB = paymentsClone.get(index).getPenaltyE().doubleValue();
                        sumFineB = paymentsClone.get(index).getFineE().doubleValue();
                        sumPaymentB = paymentsClone.get(index).getPaymentE().doubleValue();
                        dateLastPayment = paymentsClone.get(index).getPayDate();
                    }

                    //зануляем все, кроме неустойки и процентов
                    sumPrincipalC = 0;
                    sumFineC = 0;

                    if (overdueDays > 0) {//есть просрочка
                        if (DAYS.between(schedulesClone.get(i).getDate(), dateLastPayment) > 0) {
                            days = DAYS.between(dateLastPayment, calculationDate);
                            days = days > 0 ? days : 0;

                            if (!interestOverdue) {
                                sumInterestC = 0;
                            } else {
                                sumInterestC = calculateInterest(
                                    sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                            }
                            sumPaymentC = sumPrincipalC + sumInterestC;
                            sumPenaltyC = calculatePenalty(sumBalanceBefore, penaltyValue.doubleValue(), days);
                        } else {
                            if (!interestOverdue) {
                                //здесь проценты только с даты посл. факт. платежа до даты закрытия займа
                                days = DAYS.between(dateLastPayment, schedulesClone.get(i).getDate());
                                days = days > 0 ? days : 0;
                                sumInterestC = calculateInterest(
                                    sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                                sumPaymentC = sumPrincipalC + sumInterestC;
                                //расчет неустойки
                                days = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                                days = days > 0 ? days : 0;
                                sumPenaltyC = calculatePenalty(sumBalanceBefore + sumInterestC, penaltyValue.doubleValue(), days);
                            } else {
                                days = DAYS.between(dateLastPayment, calculationDate);
                                days = days > 0 ? days : 0;
                                sumInterestC = calculateInterest(
                                    sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                                sumPaymentC = sumPrincipalC + sumInterestC;
                                //расчет неустойки
                                days = DAYS.between(dateLastPayment, schedulesClone.get(i).getDate());
                                days = days > 0 ? days : 0;
                                sumPenaltyC = calculatePenalty(sumBalanceBefore + sumInterestC, penaltyValue.doubleValue(), days);
                            }
                        }

                        if (fine) {
                            boolean f = true;
                            if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                if (paymentsClone != null) {
                                    f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                }
                            }
                            if (f) sumFineC = fineValue.doubleValue();
                        }
                    } else {
                        days = DAYS.between(dateLastPayment, calculationDate);
                        days = days > 0 ? days : 0;
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                        sumPaymentC = sumPrincipalC + sumInterestC;
                        sumPenaltyC = 0;
                    }
                    break;
                }

                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_OVERDUE) {//просроченный платеж
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE;//по графику с просрочкой

                    //для просроченного займа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    days = schedulesClone.get(i).getTerm();
                    overdueDays = DAYS.between(schedulesClone.get(i).getDate(), calculationDate);
                    overdueDays = overdueDays > 0 ? overdueDays : 0;

                    if (!interestOverdue) {
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                        sumInterestC1 = sumInterestC;
                    } else {
                        sumInterestC = calculateInterest(sumBalanceBefore, interestRate.doubleValue(), interestType, days + overdueDays, daysInYear);
                        sumInterestC1 = schedulesClone.get(i).getInterest().doubleValue();
                    }

                    sumPaymentC = sumPrincipalC + sumInterestC;

                    //расчет неустойки
                    if (paymentsClone != null && paymentsClone.size() > 0) {
                        Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                        lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                        if (DAYS.between(schedulesClone.get(i).getDate(), dateLastPayment) > 0) {
                            overdueDays = DAYS.between(dateLastPayment, calculationDate);
                            overdueDays = overdueDays > 0 ? overdueDays : 0;
                        }
                    }
                    sumPenaltyC = calculatePenalty(sumBalanceBefore + sumInterestC1, penaltyValue.doubleValue(), overdueDays);

                    //расчет штрафа
                    if (fine) {
                        if (overdueDays > 0) {
                            boolean f = true;
                            if (fineOneTime) {//если единоразовый штраф, то проверяем были ли штрафы в ранних платежах
                                if (paymentsClone != null && paymentsClone.size() > 0) {
                                    f = paymentsClone.stream().noneMatch(item -> item.getFine().doubleValue() > 0);
                                }
                            }
                            if (f) sumFineC = fineValue.doubleValue();
                        }
                    }
                    break;
                }
                if (schedulesClone.get(k).getStatus() == ScheduleStatus.NOT_PAID_NO_OVERDUE) {//неоплаченный, непросроченный
                    numberSchedule = i + 1;
                    paymentStatus = PaymentStatus.PAYMENT_BY_SCHEDULE_FULL;//по графику в полном объеме

                    //для нормального платежа нет долгов на начало
                    sumPrincipalB = 0;
                    sumInterestB = 0;
                    sumPenaltyB = 0;
                    sumFineB = 0;
                    sumPaymentB = 0;

                    sumPrincipalC = schedulesClone.get(i).getPrincipal().doubleValue();
                    if (interestStrictly) {//проценты строго по графику
                        days = schedulesClone.get(i).getTerm();
                        sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                    } else {
                        if (paymentsClone != null && paymentsClone.size() > 0) {//факт. платежи были
                            Optional<Payment> lastPayment = paymentsClone.stream().reduce((first, second) -> second);
                            lastPayment.ifPresent(payment -> dateLastPayment = payment.getPayDate());
                            days = DAYS.between(dateLastPayment, calculationDate);
                        } else {//первый платеж
                            dateLastPayment = loanIssueDate;

                            if (includeIssueDate) addingDay = 1;
                            if (noInterestFD) excludeDay = noInterestFDValue;

                            days = DAYS.between(dateLastPayment, calculationDate) + addingDay - excludeDay;

                            if (interestFirstDays && (returnTerm == ReturnTerm.IN_THE_END_OF_TERM ||
                                returnTerm == ReturnTerm.INDIVIDUALLY)) {
                                if (DAYS.between(dateLastPayment, calculationDate) < interestFirstDaysValue) {
                                    days = interestFirstDaysValue + addingDay - excludeDay;
                                }
                            }
                        }
                        days = days > 0 ? days : 0;

                        if (schedulesClone.get(i).getTerm() == days) {
                            sumInterestC = schedulesClone.get(i).getInterest().doubleValue();
                        } else {
                            if (interestBalance) {
                                sumInterestC = calculateInterest(
                                    loanBalance.doubleValue(), interestRate.doubleValue(), interestType, days, daysInYear);
                            } else {
                                sumInterestC = calculateInterest(
                                    sumBalanceBefore, interestRate.doubleValue(), interestType, days, daysInYear);
                            }
                        }
                    }
                    overdueDays = 0;
                    sumPenaltyC = 0;
                    sumFineC = 0;
                    sumPaymentC = sumPrincipalC + sumInterestC + sumPenaltyC + sumFineC;
                    break;
                }
            }
        }
        scheduleStatusBefore = schedulesClone.get(numberSchedule - 1).getStatus();
    }

    /**
     * Выполняет расчет суммы по процентам
     *
     * @param sum          - сумма для расчета
     * @param interest     - процентная ставка
     * @param interestType - тип ставки
     * @param days         - кол-во дней
     * @param daysInYear   - кол-во дней в году
     * @return - сумму по процентам
     */
    private double calculateInterest(double sum, double interest, InterestType interestType, long days,
                                     int daysInYear) {
        double interestDay = 0;

        switch (interestType) {
            case PERCENT_A_DAY:
                interestDay = interest / 100d;
                break;
            case PERCENT_A_WEEK:
                interestDay = interest / 700d;
                break;
            case PERCENT_A_MONTH:
                interestDay = (interest * 12) / (daysInYear * 100d);
                break;
            case PERCENT_A_YEAR:
                interestDay = interest / (daysInYear * 100d);
                break;
            case PERCENT_A_WHOLE_PERIOD:
                return sum;
        }
        return sum * days * interestDay;
    }

    /**
     * Выполняет расчет неустойки
     *
     * @param sum      - сумма для расчета
     * @param interest = процентная ставка в день
     * @param days     - кол-во дней
     * @return - сумма по неустойке
     */
    private double calculatePenalty(double sum, double interest, long days) {
        return sum * days * interest / 100d;
    }

}

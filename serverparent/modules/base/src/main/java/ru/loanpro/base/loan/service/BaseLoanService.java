package ru.loanpro.base.loan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.loan.domain.LoanProduct;
import ru.loanpro.base.loan.exceptions.ObjectNotSavedException;
import ru.loanpro.base.loan.repository.BaseLoanRepository;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.global.service.AbstractIdEntityService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Базовый сервис работы с базовой сущностью займа (кредитным продуктом, заявкой, займом)
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Service
public abstract class BaseLoanService<T extends BaseLoan, R extends BaseLoanRepository<T>>
    extends AbstractIdEntityService<T, R> {

    @Autowired
    public BaseLoanService(R repository) {
        super(repository);
    }

    /**
     * Возвращает одну сущность по её идентификатору. Инициализация Lazy-полей
     * осуществляется в репозитории.
     *
     * @param id идентификатор сущности.
     * @return сущность типа <code>T</code>.
     */
    @Transactional
    public T getOneFull(UUID id) {
        return repository.getOneFull(id);
    }

    public List<T> findAll() {
        return repository.findAll();
    }

    /**
     * Абстрактный метод сохранения базовой сущности займа с актуализацией временного номера.
     *
     * @param baseLoan                             базовая сущность займа.
     * @param draftNumber                          временный номер. Используется в заявке и займе.
     * @return сохранённая базовая сущность займа.
     * @throws ObjectNotSavedException если при сохранении произошла ошибка.
     */
    public abstract T save(T baseLoan, String draftNumber) throws ObjectNotSavedException;

    /**
     * Сохраняет базовую сущность займа. Присваивает каждому рассчитаному платежу базовую сущность займа.
     *
     * @param baseLoan базовая сущность займа.
     * @return сохраненная базовая сущность займа.
     */
    public synchronized T save(T baseLoan) {

        if ((baseLoan.getClass() == LoanProduct.class) && (
            baseLoan.getReturnTerm() != ReturnTerm.INDIVIDUALLY
        )) {
            baseLoan.setSchedules(new ArrayList<>());
        }

        baseLoan.getSchedules().forEach(item -> item.setLoan(baseLoan));
        return repository.save(baseLoan);
    }

    /**
     * Возвращает значение, можно ли удалить переданную базовую сущность займа.
     *
     * @param entity удаляемая базовая сущность займа.
     * @return можно ли удалить сущность.
     */
    public abstract boolean canBeDeleted(T entity);

    /**
     * Выполняет логическое удаление базовой сущности займа.
     *
     * @param entity удаляемая базовая сущность займа.
     * @throws EntityDeleteException если не удалось удалить базовую сущность займа.
     */
    public abstract void delete(T entity) throws EntityDeleteException;
}

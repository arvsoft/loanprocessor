package ru.loanpro.base.loan.details;

import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.annotation.EntityName;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.security.domain.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Класс сущности заметок для займа
 *
 */
@Entity
@EntityName("entity.name.loannote")
@Table(name = "loan_notes", schema = DatabaseSchema.LOAN)
public class LoanNote extends AbstractIdEntity {

    /**
     * Дата заметки
     */
    @Column(name = "note_date", nullable = false)
    private LocalDate noteDate;

    /**
     * Заметка
     */
    @Column(name = "note")
    private String note;

    /**
     * Ссылка на сущность займа
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id", nullable = false)
    private Loan loan;

    /**
     * Пользователь, добавивший заметку.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public LoanNote() {
    }

    public LocalDate getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(LocalDate noteDate) {
        this.noteDate = noteDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LoanNote loanNote1 = (LoanNote) o;
        return Objects.equals(noteDate, loanNote1.noteDate) &&
            Objects.equals(note, loanNote1.note) &&
            Objects.equals(loan, loanNote1.loan) &&
            Objects.equals(user, loanNote1.user);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), noteDate, note, loan, user);
    }
}

package ru.loanpro.base;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Автоматическая конфигурация модуля Base.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
@EntityScan
@EnableJpaRepositories
@ComponentScan
public class BaseAutoConfiguration {}

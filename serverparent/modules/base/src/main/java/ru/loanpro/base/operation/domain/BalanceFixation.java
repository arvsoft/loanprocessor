package ru.loanpro.base.operation.domain;

import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.domain.converter.CurrencyConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Objects;

/**
 * Класс сущности фиксации баланса (текущего остатка) в отделе
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "balance_fixation", schema = DatabaseSchema.OPERATION)
public class BalanceFixation extends AbstractIdEntity {

    /**
     * Отдел
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    /**
     * Дата на которую фиксируется остаток
     */
    @Column(name = "fixation_date", nullable = false)
    private LocalDate fixationDate;

    /**
     * Валюта
     */
    @Column(name = "currency")
    @Convert(converter = CurrencyConverter.class)
    private Currency currency;

    /**
     * Сумма остатка на начало дня
     */
    @Column(name = "balance_start", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal balanceStart;

    /**
     * Сумма остатка на конец дня
     */
    @Column(name = "balance_end", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal balanceEnd;

    /**
     * Приход за период, прошедший с момента последней фиксации
     */
    @Column(name = "income_period", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal incomePeriod;

    /**
     * Расход за период, прошедший с момента последней фиксации
     */
    @Column(name = "expense_period", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal expensePeriod;

    /**
     * Пользователь, выполнивший фиксацию баланса
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public BalanceFixation() {
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public LocalDate getFixationDate() {
        return fixationDate;
    }

    public void setFixationDate(LocalDate fixationDate) {
        this.fixationDate = fixationDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getBalanceStart() {
        return balanceStart;
    }

    public void setBalanceStart(BigDecimal balanceStart) {
        this.balanceStart = balanceStart;
    }

    public BigDecimal getBalanceEnd() {
        return balanceEnd;
    }

    public void setBalanceEnd(BigDecimal balanceEnd) {
        this.balanceEnd = balanceEnd;
    }

    public BigDecimal getIncomePeriod() {
        return incomePeriod;
    }

    public void setIncomePeriod(BigDecimal incomePeriod) {
        this.incomePeriod = incomePeriod;
    }

    public BigDecimal getExpensePeriod() {
        return expensePeriod;
    }

    public void setExpensePeriod(BigDecimal expensePeriod) {
        this.expensePeriod = expensePeriod;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BalanceFixation that = (BalanceFixation) o;
        return Objects.equals(department, that.department) &&
            Objects.equals(fixationDate, that.fixationDate) &&
            Objects.equals(currency, that.currency) &&
            Objects.equals(balanceStart, that.balanceStart) &&
            Objects.equals(balanceEnd, that.balanceEnd) &&
            Objects.equals(incomePeriod, that.incomePeriod) &&
            Objects.equals(expensePeriod, that.expensePeriod) &&
            Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), department, fixationDate, currency, balanceStart, balanceEnd, incomePeriod, expensePeriod, user);
    }
}

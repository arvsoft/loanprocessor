package ru.loanpro.base.client.domain;

import com.google.common.base.Objects;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.global.domain.RemovableEntity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * Класс сущности клиента
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "clients", schema = DatabaseSchema.CLIENT)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "entity_type",
    discriminatorType = DiscriminatorType.STRING)
public class Client extends RemovableEntity {

    /**
     * Рейтинг клиента.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "rating")
    private ClientRating rating;

    /**
     * Дата регистрации анкеты.
     */
    @Column(name = "registration_date")
    private LocalDate registrationDate;

    public Client() {
    }

    /**
     * Метод удобного получения отображаемого имени для размещения на карточках или в списках.
     *
     * @return отображаемое имя.
     */
    public String getDisplayName() {
        return null;
    }

    public ClientRating getRating() {
        return rating;
    }

    public void setRating(ClientRating rating) {
        this.rating = rating;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        if (!super.equals(o))
        {
            return false;
        }
        Client client = (Client) o;

        return Objects.equal(rating, client.rating) &&
            Objects.equal(registrationDate, client.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), rating, registrationDate);
    }
}

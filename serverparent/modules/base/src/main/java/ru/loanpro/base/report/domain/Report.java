package ru.loanpro.base.report.domain;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.ReportGroup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Класс сущности отчета
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "reports", schema = DatabaseSchema.REPORT)
public class Report  extends AbstractIdEntity {

    /**
     * Идентификатор отчета
     */
    @Column(name = "report_id")
    private String reportId;

    /**
     * Группа отчета
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "report_group")
    private ReportGroup group;

    /**
     * Тип роли из Roles
     */
    @Column(name = "role")
    private String role;

    /**
     * Наименование отчета
     */
    @Column(name = "report_name")
    private String name;

    /**
     * Описание отчета
     */
    @Column(name = "description")
    private String description;

    /**
     * Дата последнего формирования отчета
     */
    @Column(name = "last_date")
    private LocalDate lastDate;

    public Report() {
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public ReportGroup getGroup() {
        return group;
    }

    public void setGroup(ReportGroup group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getLastDate() {
        return lastDate;
    }

    public void setLastDate(LocalDate lastDate) {
        this.lastDate = lastDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Report report = (Report) o;
        return group == report.group &&
            Objects.equals(name, report.name) &&
            Objects.equals(role, report.role) &&
            Objects.equals(description, report.description) &&
            Objects.equals(lastDate, report.lastDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), group, name, role, description, lastDate);
    }
}

package ru.loanpro.base.account.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import ru.loanpro.base.account.domain.BaseAccount;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.repository.DepartmentAccountRepository;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.security.domain.Department;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.sequence.service.SequenceService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Сервис по работе со счетами отдела
 *
 * @author Maksim Askhaev
 */
@Service
public class DepartmentAccountService extends AbstractAccountService<DepartmentAccount> {

    private final DepartmentAccountRepository departmentAccountRepository;
    private final SequenceService sequenceService;
    private final ApplicationEventBus applicationEventBus;

    @Autowired
    public DepartmentAccountService(DepartmentAccountRepository departmentAccountRepository,
                                    SequenceService sequenceService, ApplicationEventBus applicationEventBus) {
        super(departmentAccountRepository);
        this.departmentAccountRepository = departmentAccountRepository;
        this.sequenceService = sequenceService;
        this.applicationEventBus = applicationEventBus;
    }

    @Transactional
    public DepartmentAccount getOneFull(UUID id) {
        DepartmentAccount one = repository.getOne(id);
        Hibernate.initialize(one.getDepartment());

        return one;
    }

    public BigDecimal getBalanceForAccount(BaseAccount account) {
        return super.getBalanceForEntity(account);
    }

    public int updateAccountBalance(UUID id, BigDecimal balance) {
        return super.updateAccountBalance(id, balance);
    }

    @Transactional
    public synchronized void saveAll(Set<DepartmentAccount> departmentAccounts, Department department, Sequence sequence) {
        departmentAccounts.forEach(item -> {
            try {
                if (item.getDepartment() == null) {
                    item.setDepartment(department);
                    String number = sequenceService.actualizeNextNumber(sequence);
                    item.setNumber(number);
                }
                departmentAccountRepository.save(item);
                // Если всё сохранилось корректно - оповещаем систему об обновлении последовательности
                applicationEventBus.post(new UpdateSequenceEvent(sequence));
            } catch (Exception e) {
                // При любом исключении из бд - откатываем назад номер и бросаем исключение.
                sequenceService.resetLastNumber(sequence);
            }
        });
    }

    @Override
    public List<DepartmentAccount> getAccounts(Object object) {
        return repository.findAllByEntity(object);
    }

    public Set<DepartmentAccount> findAllByDepartment(Department department) {
        return departmentAccountRepository.findAllByDepartment(department);
    }


}

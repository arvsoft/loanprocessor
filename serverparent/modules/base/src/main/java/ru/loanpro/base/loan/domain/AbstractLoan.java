package ru.loanpro.base.loan.domain;

import org.hibernate.Hibernate;
import ru.loanpro.base.loan.details.LoanNote;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.directory.RecalculationType;
import ru.loanpro.security.domain.Department;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Абстрактный класс займа.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public abstract class AbstractLoan extends AbstractRequest {

    /**
     * Идентификатор активной записи по займу,
     * используется для отображения списка займов (фильтр по займам)
     * отображаются только займы с идентификатором = false
     * = false, если заем новый или последний вариант пролонгированного займа
     * = true, если заем в промежуточном состоянии
     */
    @Column(name = "is_prolonged")
    private boolean isProlonged;

    /**
     * Ссылка на родительский заем при пролонгации
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Loan parent;

    /**
     * Заявка, по которой был оформлен заем.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id")
    private Request request;

    /**
     * Номер займа
     */
    @Column(name = "loan_number")
    private String loanNumber;

    /**
     * Статус займа
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "loan_status")
    private LoanStatus loanStatus;

    /**
     * Дата выдачи займа
     */
    @Column(name = "loan_issue_date")
    private Instant loanIssueDate;

    /**
     * Текущий остаток основной части
     */
    @Column(name = "current_balance", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal currentBalance;

    /**
     * График фактических платежей
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "loan", orphanRemoval = true)
    private List<Payment> payments = new ArrayList<>();

    /**
     * Департамент(офис) пролонгации
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "prolong_department_id")
    private Department prolongDepartment;

    /**
     * Порядковый номер пролонгации займа
     */
    @Column(name = "prolong_ord_no")
    private int prolongOrdNo;

    /**
     * Дата пролонгации займа
     */
    @Column(name = "prolong_date")
    private LocalDate prolongDate;

    /**
     * Сумма пролонгации
     */
    @Column(name = "prolong_value", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal prolongValue;

    /**
     * Расчет на момент пролонгации (долги)
     */
    @Column(name = "prolong_calculation", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal prolongCalculation;

    /**
     * Номер соглашения о пролонгации
     */
    @Column(name = "prolong_agr_number")
    private String prolongAgrNumber;

    /**
     * Способ пересчета графика платежей при пролонгации
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "recalculation_type")
    private RecalculationType recalculationType;

    /**
     * Заметки по займу
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "loan", orphanRemoval = true)
    private List<LoanNote> loanNotes = new ArrayList<>();

    /**
     * Конструктор по-умолчанию.
     */
    public AbstractLoan() {
    }

    /**
     * Конструктор, принимающий экземпляр абстрактного займа {@link AbstractLoan} для
     * заполнения своих полей.
     *
     * @param loan заем.
     */
    public AbstractLoan(AbstractLoan loan) {
        setDepartment(loan.getDepartment());
        setLoanProduct(loan.getLoanProduct());
        setRequest(loan.getRequest());
        setLoanStatus(loan.getLoanStatus());
        setLoanNumber(loan.getLoanNumber());
        setLoanIssueDate(loan.getLoanIssueDate());
        setCurrentBalance(loan.getCurrentBalance());
        setRequestNumber(loan.getRequestNumber());
        setRequestIssueDate(loan.getRequestIssueDate());
        setLoanBalance(loan.getLoanBalance());
        setCurrency(loan.getCurrency());
        setCalcScheme(loan.getCalcScheme());
        setReturnTerm(loan.getReturnTerm());
        setInterestValue(loan.getInterestValue());
        setInterestType(loan.getInterestType());
        setLoanTerm(loan.getLoanTerm());
        setLoanTermType(loan.getLoanTermType());
        setReplaceDate(loan.isReplaceDate());
        setReplaceDateValue(loan.getReplaceDateValue());
        setPrincipalLastPay(loan.isPrincipalLastPay());
        setInterestBalance(loan.isInterestBalance());
        setNoInterestFD(loan.isNoInterestFD());
        setNoInterestFDValue(loan.getNoInterestFDValue());
        setIncludeIssueDate(loan.isIncludeIssueDate());
        setPenaltyType(loan.getPenaltyType());
        setPenaltyValue(loan.getPenaltyValue());
        setChargeFine(loan.isChargeFine());
        setFineValue(loan.getFineValue());
        setFineOneTime(loan.isFineOneTime());
        setInterestStrictly(loan.isInterestStrictly());
        setInterestOverdue(loan.isInterestOverdue());
        setInterestFirstDays(loan.isInterestFirstDays());
        setInterestNumberFirstDays(loan.getInterestNumberFirstDays());

        setPskValue(loan.getPskValue());
        setInterestTotal(loan.getInterestTotal());
        setBorrower(loan.getBorrower());
        setCoBorrower(loan.getCoBorrower());
        setGuarantors(loan.getGuarantors());
    }

    /**
     * Конструктор, принимающий экземпляр абстрактной заявки {@link AbstractRequest} для
     * заполнения своих полей.
     *
     * @param request заявка.
     */
    public AbstractLoan(AbstractRequest request) {
        super(request);
        setRequest((Request) request);
        setRequestNumber(request.getRequestNumber());
        setRequestIssueDate(request.getRequestIssueDate());
        setRequestStatus(request.getRequestStatus());
        setDepartment(request.getDepartment());
        setBorrower(request.getBorrower());
        setCoBorrower(request.getCoBorrower());
        setGuarantors(request.getGuarantors());
    }

    /**
     * Конструктор, принимающий экземпляр абстрактного кредитного продукта {@link AbstractLoanProduct}
     * для заполнения своих полей.
     *
     * @param loanProduct кредитный продукт.
     */
    public AbstractLoan(AbstractLoanProduct loanProduct) {
        super(loanProduct);
    }

    public Department getProlongDepartment() {
        return prolongDepartment;
    }

    public void setProlongDepartment(Department prolongDepartment) {
        this.prolongDepartment = prolongDepartment;
    }

    public boolean isProlonged() {
        return isProlonged;
    }

    public void setIsProlonged(boolean isProlonged) {
        this.isProlonged = isProlonged;
    }

    public Loan getParent() {
        return parent;
    }

    public void setParent(Loan parentId) {
        this.parent = parentId;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(String loanNumber) {
        this.loanNumber = loanNumber;
    }

    public LoanStatus getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(LoanStatus loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Instant getLoanIssueDate() {
        return loanIssueDate;
    }

    public void setLoanIssueDate(Instant loanIssueDate) {
        this.loanIssueDate = loanIssueDate;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public int getProlongOrdNo() {
        return prolongOrdNo;
    }

    public void setProlongOrdNo(int prolongOrdNo) {
        this.prolongOrdNo = prolongOrdNo;
    }

    public LocalDate getProlongDate() {
        return prolongDate;
    }

    public void setProlongDate(LocalDate prolongDate) {
        this.prolongDate = prolongDate;
    }

    public BigDecimal getProlongValue() {
        return prolongValue;
    }

    public void setProlongValue(BigDecimal prolongValue) {
        this.prolongValue = prolongValue;
    }

    public String getProlongAgrNumber() {
        return prolongAgrNumber;
    }

    public void setProlongAgrNumber(String prolongAgrNumber) {
        this.prolongAgrNumber = prolongAgrNumber;
    }

    public RecalculationType getRecalculationType() {
        return recalculationType;
    }

    public void setRecalculationType(RecalculationType recalculationType) {
        this.recalculationType = recalculationType;
    }

    public void setProlonged(boolean prolonged) {
        isProlonged = prolonged;
    }

    public List<LoanNote> getLoanNotes() {
        return loanNotes;
    }

    public void setLoanNotes(List<LoanNote> loanNotes) {
        this.loanNotes = loanNotes;
    }

    public BigDecimal getProlongCalculation() {
        return prolongCalculation;
    }

    public void setProlongCalculation(BigDecimal prolongCalculation) {
        this.prolongCalculation = prolongCalculation;
    }

    @Override
    public String toString() {
        return "Loan{" +
            "id=" + super.getId() +
            ", loanNumber=" + loanNumber +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractLoan loan = (AbstractLoan) o;


        boolean equal = Objects.equals(loanNumber, loan.loanNumber) &&
            loanStatus == loan.loanStatus &&
            Objects.equals(loanIssueDate, loan.loanIssueDate) &&
            Objects.equals(currentBalance, loan.currentBalance) &&
            Objects.equals(prolongOrdNo, loan.prolongOrdNo) &&
            Objects.equals(prolongDate, loan.prolongDate) &&
            Objects.equals(prolongValue, loan.prolongValue) &&
            Objects.equals(isProlonged, loan.isProlonged);

        if (Hibernate.isInitialized(request))
            equal = equal && Objects.equals(request, loan.request);

        if (Hibernate.isInitialized(payments))
            equal = equal && Objects.equals(payments, loan.payments);

        if (Hibernate.isInitialized(parent))
            equal = equal && Objects.equals(parent, loan.parent);

        return equal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(),
            Hibernate.isInitialized(request)
                ? request
                : null,
            loanNumber,
            loanStatus,
            loanIssueDate,
            currentBalance,
            prolongOrdNo,
            prolongDate,
            prolongValue,
            isProlonged,
            Hibernate.isInitialized(payments)
                ? Arrays.hashCode(payments.toArray())
                : null,
            Hibernate.isInitialized(parent)
                ? parent
                : null);
    }
}

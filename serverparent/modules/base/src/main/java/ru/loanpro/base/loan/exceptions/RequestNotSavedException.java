package ru.loanpro.base.loan.exceptions;

/**
 * Исключение, возникающее при попытке выполнить сохранение заявки в сервисе сохранения заявки в рамках транзакции
 *
 * @author Maksim Askhaev
 */
public class RequestNotSavedException extends BaseLoanNotSavedException {
    public RequestNotSavedException(Exception e) {
        super(e);
    }
}


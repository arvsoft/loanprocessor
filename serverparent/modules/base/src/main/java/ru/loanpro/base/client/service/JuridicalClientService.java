package ru.loanpro.base.client.service;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.loanpro.base.client.domain.JuridicalClient;
import ru.loanpro.base.client.repository.JuridicalClientRepository;
import ru.loanpro.global.exceptions.EntityDeleteException;

/**
 * Сервис работы с сущностью {@link JuridicalClient}.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class JuridicalClientService extends AbstractClientService<JuridicalClient, JuridicalClientRepository> {

    @Autowired
    public JuridicalClientService(JuridicalClientRepository repository){
        super(repository);
    }

    @Override
    public Integer count(String filter) {
        return null;
    }

    @Override
    public Stream<JuridicalClient> findAll(String filter, int offset, int limit) {
        return null;
    }

    @Override
    public Boolean canBeDeleted(JuridicalClient client) {
        return true;
    }

    @Override
    public void delete(JuridicalClient client) throws EntityDeleteException {

    }
}

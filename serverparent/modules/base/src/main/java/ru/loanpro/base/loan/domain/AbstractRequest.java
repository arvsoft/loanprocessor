package ru.loanpro.base.loan.domain;

import org.hibernate.Hibernate;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.RequestStatus;
import ru.loanpro.security.domain.Department;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Абстрактный класс заявки.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public abstract class AbstractRequest extends AbstractLoanProduct {

    /**
     * Продукт, по которому была оформлена заявка.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_product_id")
    private LoanProduct loanProduct;

    /**
     * Департамент(офис)
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    /**
     * Номер заявки
     */
    @Column(name = "request_number")
    private String requestNumber;

    /**
     * Статус заявки, справочные данные
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "request_status")
    private RequestStatus requestStatus;

    /**
     * Дата оформления заявки
     */
    @Column(name = "request_issue_date")
    private Instant requestIssueDate;

    /**
     * Заемщик
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "borrower_id")
    private Client borrower;

    /**
     * Созаемщик
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coborrower_id")
    private Client coBorrower;

    /**
     * Коллекция поручителей, привязанных к заявке
     */
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(
            name = "request_client_guarantors",
            schema = DatabaseSchema.LOAN,
            joinColumns = {@JoinColumn(name = "request_id")},
            inverseJoinColumns = {@JoinColumn(name = "client_id")})
    private List<Client> guarantors = new ArrayList<>();

    /**
     * Конструктор по-умолчанию.
     */
    public AbstractRequest() {}

    /**
     * Конструктор, принимающий экземпляр абстрактного кредитного продукта {@link AbstractLoanProduct}
     * для заполнения своих полей.
     *
     * @param loanProduct кредитный продукт.
     */
    public AbstractRequest(AbstractLoanProduct loanProduct) {
        if (loanProduct instanceof LoanProduct)
            setLoanProduct((LoanProduct) loanProduct);
        setLoanType(loanProduct.getLoanType());
        setCalcScheme(loanProduct.getCalcScheme());
        setReturnTerm(loanProduct.getReturnTerm());
        setLoanBalance(loanProduct.getLoanBalance());
        setCurrency(loanProduct.getCurrency());
        setInterestValue(loanProduct.getInterestValue());
        setInterestType(loanProduct.getInterestType());
        setLoanTerm(loanProduct.getLoanTerm());
        setLoanTermType(loanProduct.getLoanTermType());
        setReplaceDate(loanProduct.isReplaceDate());
        setReplaceDateValue(loanProduct.getReplaceDateValue());
        setReplaceOverMonth(loanProduct.isReplaceOverMonth());
        setPrincipalLastPay(loanProduct.isPrincipalLastPay());
        setInterestBalance(loanProduct.isInterestBalance());
        setNoInterestFD(loanProduct.isNoInterestFD());
        setNoInterestFDValue(loanProduct.getNoInterestFDValue());
        setIncludeIssueDate(loanProduct.isIncludeIssueDate());
        setPenaltyType(loanProduct.getPenaltyType());
        setPenaltyValue(loanProduct.getPenaltyValue());
        setChargeFine(loanProduct.isChargeFine());
        setFineValue(loanProduct.getFineValue());
        setFineOneTime(loanProduct.isFineOneTime());

        setInterestStrictly(loanProduct.isInterestStrictly());
        setInterestOverdue(loanProduct.isInterestOverdue());
        setInterestFirstDays(loanProduct.isInterestFirstDays());
        setInterestNumberFirstDays(loanProduct.getInterestNumberFirstDays());
    }

    public LoanProduct getLoanProduct() {
        return loanProduct;
    }

    public void setLoanProduct(LoanProduct loanProduct) {
        this.loanProduct = loanProduct;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Department getDepartment() {
        return department;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Instant getRequestIssueDate() {
        return requestIssueDate;
    }

    public void setRequestIssueDate(Instant requestIssueDate) {
        this.requestIssueDate = requestIssueDate;
    }

    public Client getBorrower() {
        return borrower;
    }

    public void setBorrower(Client borrower) {
        this.borrower = borrower;
    }

    public Client getCoBorrower() {
        return coBorrower;
    }

    public void setCoBorrower(Client coBorrower) {
        this.coBorrower = coBorrower;
    }

    public List<Client> getGuarantors() {
        return guarantors;
    }

    public void setGuarantors(List<Client> guarantors) {
        this.guarantors = guarantors;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + super.getId() +
                ", department=" + department +
                ", requestNumber=" + requestNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        if (!super.equals(o))
        {
            return false;
        }
        AbstractRequest request = (AbstractRequest) o;

        boolean equal = Objects.equals(department, request.department) &&
            Objects.equals(requestNumber, request.requestNumber) &&
            requestStatus == request.requestStatus &&
            Objects.equals(requestIssueDate, request.requestIssueDate) &&
            Objects.equals(borrower, request.borrower);

        if (Hibernate.isInitialized(loanProduct))
            equal = equal && Objects.equals(loanProduct, request.getLoanProduct());

        if (Hibernate.isInitialized(coBorrower))
            equal = equal && Objects.equals(coBorrower, request.coBorrower);

        if (Hibernate.isInitialized(guarantors))
            equal = equal && Objects.equals(guarantors, request.guarantors);

        return equal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            super.hashCode(),
            Hibernate.isInitialized(loanProduct)
                ? loanProduct
                : null,
            department,
            requestNumber,
            requestIssueDate,
            borrower,
            Hibernate.isInitialized(coBorrower)
                ? coBorrower
                : null,
            Hibernate.isInitialized(guarantors)
                ? Arrays.hashCode(guarantors.toArray())
                : null);
    }
}

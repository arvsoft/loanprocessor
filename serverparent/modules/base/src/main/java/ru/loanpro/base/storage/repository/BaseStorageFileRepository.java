package ru.loanpro.base.storage.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import ru.loanpro.base.storage.domain.BaseStorageFile;

/**
 * Базовый репозиторий сущности файла, хранящегося в хранилище файлов {@link BaseStorageFile}.
 *
 * @param <T> тип сущности файла в хранилище файлов.
 *
 * @author Oleb Brizhevatikh
 */
@NoRepositoryBean
public interface BaseStorageFileRepository<T extends BaseStorageFile> extends JpaRepository<T, UUID> {
}

package ru.loanpro.base.operation.exceptions;

public class OperationNotSavedException extends Exception{
    public OperationNotSavedException(String message) {
        super(message);
    }
}

package ru.loanpro.base.account.repository;

import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.security.domain.Department;

import java.util.List;
import java.util.Set;

/**
 * Репозиторий сущности {@link DepartmentAccount}.
 *
 * @author Maksim Askhaev
 */
public interface DepartmentAccountRepository extends BaseAccountRepository<DepartmentAccount> {

    Set<DepartmentAccount> findAllByDepartment(Department department);

    @Query("select a from DepartmentAccount a where a.department = ?1 order by a.number")
    @Override
    List<DepartmentAccount> findAllByEntity(Object entity);
}

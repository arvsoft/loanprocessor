package ru.loanpro.base.loan.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

/**
 * Абстрактный класс кредитного продукта.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public abstract class AbstractLoanProduct extends BaseLoan {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public AbstractLoanProduct() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractLoanProduct that = (AbstractLoanProduct) o;
        return Objects.equals(name, that.name) &&
            Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, description);
    }
}

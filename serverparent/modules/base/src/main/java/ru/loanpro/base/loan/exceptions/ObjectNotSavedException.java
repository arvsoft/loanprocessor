package ru.loanpro.base.loan.exceptions;

/**
 * Базовое исключение, возникающее при попытке выполнить сохранение любой сущности
 *
 * @author Maksim Askhaev
 */
public class ObjectNotSavedException extends Exception {
    public ObjectNotSavedException(Exception e){
    }
}

package ru.loanpro.base.operation.event;

public class UpdateOperationEvent {

    private Object object;

    public UpdateOperationEvent(Object object) {
        this.object = object;
    }

    public Object getObject() {
        return object;
    }
}

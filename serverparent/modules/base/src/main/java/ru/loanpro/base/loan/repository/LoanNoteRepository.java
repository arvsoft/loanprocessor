package ru.loanpro.base.loan.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import ru.loanpro.base.loan.details.LoanNote;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.global.repository.AbstractIdEntityRepository;

import java.util.List;

/**
 * Репозиторий сущности {@link LoanNote}
 *
 * @author Maksim Askhaev
 */
public interface LoanNoteRepository extends AbstractIdEntityRepository<LoanNote> {

    @EntityGraph(attributePaths = {"loan", "user"})
    List<LoanNote> findAllByLoanOrderByNoteDateDesc(Loan loan);
}

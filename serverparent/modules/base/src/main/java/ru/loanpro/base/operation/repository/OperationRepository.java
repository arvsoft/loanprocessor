package ru.loanpro.base.operation.repository;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.global.repository.AbstractIdEntityRepository;
import ru.loanpro.security.domain.Department;

import javax.annotation.Nullable;
import java.time.Instant;
import java.util.Currency;
import java.util.List;

/**
 * Репозиторий сущности {@link Operation}.
 *
 * @author Maksim Askhaev
 */
public interface OperationRepository extends AbstractIdEntityRepository<Operation> {

    /**
     * {@inheritDoc}
     */
    @EntityGraph(attributePaths = {"loan", "payment"})
    @Override
    Page<Operation> findAll(@Nullable Predicate predicate, @Nullable Pageable pageable);

    /**
     * Получаем список операций расхода по счетам отдела-плательщика с указанной валютой
     * Используется для фиксации баланса
     * @param dateMin  - начальная дата операции
     * @param dateMax  - конечная дата операции
     * @param department - отдел, владелец счета
     * @param currency - валюта счета
     * @return список операций
     */
    @Query("select o from Operation o join o.accountPayer p where o.status = ru.loanpro.global.directory.OperationStatus.PROCESSED and o.operationDateTime >= ?1 and o.operationDateTime <= ?2 and p.department = ?3 and p.currency=?4")
    @EntityGraph(attributePaths = {"department"})
    List<Operation> getExpenseOperations(Instant dateMin, Instant dateMax, Department department, Currency currency);

    /**
     * Получаем список операций прихода по счетам отдела-получателя с указанной валютой
     * Используется для фиксации баланса
     * @param dateMax  - конечная дата операции
     * @param department - отдел, владелец счета
     * @param currency - валюта счета
     * @return список операций
     */
    @Query("select o from Operation o join o.accountRecipient p where o.status = ru.loanpro.global.directory.OperationStatus.PROCESSED and o.operationDateTime >= ?1 and o.operationDateTime <= ?2 and p.department = ?3 and p.currency=?4")
    @EntityGraph(attributePaths = {"department"})
    List<Operation> getIncomeOperations(Instant dateMin, Instant dateMax, Department department, Currency currency);

    /**
     * Получаем список операций расхода за указанный период
     * @param dateMin  - начальная дата операции
     * @param dateMax  - конечная дата операции
     * @param department - отдел, владелец счета
     * @return список операций
     */
    @Query("select o from Operation o join o.accountPayer p where o.status = ru.loanpro.global.directory.OperationStatus.PROCESSED and o.operationDateTime >= ?1 and o.operationDateTime <= ?2 and p.department = ?3")
    @EntityGraph(attributePaths = {"department"})
    List<Operation> getExpenseOperationsForPeriod(Instant dateMin, Instant dateMax, Department department);

    /**
     * Получаем список операций прихода за указанный период
     * @param dateMin  - начальная дата операции
     * @param dateMax  - конечная дата операции
     * @param department - отдел, владелец счета
     * @return список операций
     */
    @Query("select o from Operation o join o.accountRecipient p where o.status = ru.loanpro.global.directory.OperationStatus.PROCESSED and o.operationDateTime >= ?1 and o.operationDateTime <= ?2 and p.department = ?3")
    @EntityGraph(attributePaths = {"department"})
    List<Operation> getIncomeOperationsForPeriod(Instant dateMin, Instant dateMax, Department department);

    //*****************************************************************************************************
    /**
     * Находим минимальную дату в операциях по счетам указанного отдела и валюты
     */
    @Query(value = "SELECT MIN(O.OPERATION_DATETIME) FROM OPERATION.OPERATIONS O, OPERATION.ACCOUNTS A WHERE O.STATUS = 'PROCESSED' AND O.CURRENCY = ?2 AND ((O.ACCOUNT_PAYER_ID=A.ID AND A.DEPARTMENT_ID = ?1) OR (O.ACCOUNT_RECIPIENT_ID=A.ID AND A.DEPARTMENT_ID=?1))", nativeQuery = true)
    Instant getMinDateFromOperations(Department department, Currency currency);

    /**
     * Находим максимальную дату в операциях по счетам указанного отдела и валюты
     */
    @Query(value = "SELECT MAX(O.OPERATION_DATETIME) FROM OPERATION.OPERATIONS O, OPERATION.ACCOUNTS A WHERE O.STATUS = 'PROCESSED' AND O.CURRENCY = ?2 AND ((O.ACCOUNT_PAYER_ID=A.ID AND A.DEPARTMENT_ID = ?1) OR (O.ACCOUNT_RECIPIENT_ID=A.ID AND A.DEPARTMENT_ID=?1))", nativeQuery = true)
    Instant getMaxDateFromOperations(Department department, Currency currency);
}

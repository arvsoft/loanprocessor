package ru.loanpro.base.schedule.domain;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.ScheduleStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Класс сущности графика рассчитанных платежей
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "schedules", schema = DatabaseSchema.LOAN)
public class Schedule extends AbstractIdEntity {

    /**
     * Займ
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id", nullable = false)
    private BaseLoan loan;

    /**
     * Порядковый номер
     */
    @Column(name = "ord_no")
    private int ordNo;

    /**
     * Статус платежа
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ScheduleStatus status;

    /**
     * Дата платежа
     */
    @Column(name = "paydate")
    private LocalDate date;

    /**
     * Срок платежа
     */
    @Column(name = "term")
    private long term;

    /**
     * Размер основной части
     */
    @Column(name = "principal")
    private BigDecimal principal;

    /**
     * Размер процентов
     */
    @Column(name = "interest")
    private BigDecimal interest;

    /**
     * Размер платежа
     */
    @Column(name = "payment")
    private BigDecimal payment;

    /**
     * Остаток основной части долга после данного платежа
     */
    @Column(name = "balance")
    private BigDecimal balance;

    public Schedule() {
    }

    public Schedule(Loan loan, int ordNo, LocalDate date, ScheduleStatus status, long term,
                    BigDecimal principal, BigDecimal interest, BigDecimal payment, BigDecimal balance) {
        this.loan = loan;
        this.ordNo = ordNo;
        this.date = date;
        this.status = status;
        this.term = term;
        this.principal = principal;
        this.interest = interest;
        this.payment = payment;
        this.balance = balance;
    }

    public BaseLoan getLoan() {
        return loan;
    }

    public void setLoan(BaseLoan loan) {
        this.loan = loan;
    }

    public int getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(int order) {
        this.ordNo = order;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ScheduleStatus getStatus() {
        return status;
    }

    public void setStatus(ScheduleStatus status) {
        this.status = status;
    }

    public long getTerm() {
        return term;
    }

    public void setTerm(long term) {
        this.term = term;
    }

    public BigDecimal getPrincipal() {
        return principal;
    }

    public void setPrincipal(BigDecimal principal) {
        this.principal = principal;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Schedule{" +
            ", loan=" + loan +
            ", ordNo=" + ordNo +
            ", status=" + status +
            ", date=" + date +
            ", term=" + term +
            ", principal=" + principal +
            ", interest=" + interest +
            ", payment=" + payment +
            ", balance=" + balance +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Schedule schedule = (Schedule) o;

        if (ordNo != schedule.ordNo) return false;
        if (term != schedule.term) return false;
        if (status != schedule.status) return false;
        if (!date.equals(schedule.date)) return false;
        if (!principal.equals(schedule.principal)) return false;
        if (!interest.equals(schedule.interest)) return false;
        if (!payment.equals(schedule.payment)) return false;
        return balance.equals(schedule.balance);
    }

    @Override
    public int hashCode() {
        int result = ordNo;
        result = 31 * result + status.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + (int) (term ^ (term >>> 32));
        result = 31 * result + principal.hashCode();
        result = 31 * result + interest.hashCode();
        result = 31 * result + payment.hashCode();
        result = 31 * result + balance.hashCode();
        return result;
    }
}

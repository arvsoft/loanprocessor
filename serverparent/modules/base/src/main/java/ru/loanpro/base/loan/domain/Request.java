package ru.loanpro.base.loan.domain;

import ru.loanpro.base.requesthistory.domain.RequestHistory;
import ru.loanpro.global.annotation.EntityName;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс сущности заявки. Расширяет абстрактный класс заявки.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.request")
@DiscriminatorValue("REQUEST")
public class Request extends AbstractRequest {

    /**
     * История изменения статуса заявок.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "request", orphanRemoval = true)
    private Set<RequestHistory> requestHistory = new HashSet<>();

    public Request() {
    }

    public Request(AbstractLoanProduct product) {
        super(product);
    }

    public Set<RequestHistory> getRequestHistory() {
        return requestHistory;
    }

    public void setRequestHistory(Set<RequestHistory> requestHistory) {
        this.requestHistory = requestHistory;
    }
}

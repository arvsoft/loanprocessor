package ru.loanpro.base.loan.domain;

import ru.loanpro.global.annotation.EntityName;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Класс сущности кредитного продукта. Расширяет абстрактный класс кредитного продукта.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Entity
@EntityName("entity.name.loanProduct")
@DiscriminatorValue("PRODUCT")
public class LoanProduct extends AbstractLoanProduct {

    /**
     * Конструктор по-умолчанию.
     */
    public LoanProduct() {}
}

package ru.loanpro.base.operation.exceptions;

public class ForbidToFixBalanceException extends Exception {
    public ForbidToFixBalanceException(String message) {
        super(message);
    }
}

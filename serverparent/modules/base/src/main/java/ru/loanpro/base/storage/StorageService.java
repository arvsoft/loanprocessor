package ru.loanpro.base.storage;

import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import ru.loanpro.base.storage.domain.PrintTemplateFile;
import ru.loanpro.base.storage.exception.FileSystemErrorException;
import ru.loanpro.base.storage.service.PrintTemplateFileService;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Реализация интерфейса {@link IStorageService}, обеспечивающая работу с локальной
 * файловой системой сервера.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class StorageService implements IStorageService {

    private static final Logger LOGGER = Logger.getLogger(StorageService.class.getName());

    @Value("${system.directory.avatars}")
    private String avatarPath;

    @Value("${system.directory.printed}")
    private String printedPath;

    @Value("${system.directory.scanned}")
    private String scannedPath;

    @Value("${system.directory.template}")
    private String templatePath;

    @Value("${system.directory.unload}")
    private String unloadPath;

    @Value("classpath:templates/*.odt")
    org.springframework.core.io.Resource[] resourceTemplates;

    private final PrintTemplateFileService printTemplateFileService;

    @Autowired
    public StorageService(PrintTemplateFileService printTemplateFileService) {
        this.printTemplateFileService = printTemplateFileService;
    }

    /**
     * Производит настройку файловой системы, создание каталогов, проверку доступности
     * катологов и файлов.
     */
    @PostConstruct
    public void init() {
        checkAndCreateFolder(avatarPath);
        checkAndCreateFolder(printedPath);
        checkAndCreateFolder(scannedPath);
        checkAndCreateFolder(templatePath);
        checkAndCreateFolder(unloadPath);
        updateTemplates();
    }

    @Override
    public Resource getAvatar(String avatarName) {
        ThemeResource defaultResource = new ThemeResource("profile-pic-300px.jpg");

        if (avatarName == null)
            return defaultResource;

        String imagePath = String.format("%s/%s", avatarPath, avatarName);
        File file = new File(imagePath);

        return file.exists()
            ? new FileResource(file)
            : defaultResource;
    }

    @Override
    public FileResource saveAvatar(ByteArrayOutputStream bas) throws FileSystemErrorException {
        return saveFile(avatarPath, "jpg", bas);
    }

    @Override
    public FileResource getScannedFile(String scannedName) throws FileSystemErrorException {
        String imagePath = String.format("%s/%s", scannedPath, scannedName);
        File file = new File(imagePath);

        if (file.exists())
            return new FileResource(file);
        else
            throw new FileSystemErrorException("file not found");
    }

    @Override
    public Resource getScannedFileOrDefault(String scannedName) {
        String imagePath = String.format("%s/%s", scannedPath, scannedName);
        File file = new File(imagePath);

        if (file.exists())
            return new FileResource(file);
        else
            return new ThemeResource("profile-pic-300px.jpg");
    }

    @Override
    public FileResource saveScannedFile(String fileName, ByteArrayOutputStream bas) throws FileSystemErrorException {
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);

        return saveFile(scannedPath, extension, bas);
    }

    @Override
    public File getNewFileForPrint() {
        return newFileInPath(printedPath, "pdf");
    }

    @Override
    public InputStream getPrintedFileStream(String fileName) throws FileNotFoundException {
        File file = getFileInPath(printedPath, fileName);
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            LOGGER.info(String.format("Can not read file %s because: %s", fileName, e.getMessage()));
            throw e;
        }
    }

    @Override
    public long getPrintedFileLength(String fileName) throws FileNotFoundException {
        return getFileInPath(printedPath, fileName).length();
    }

    @Override
    public InputStream getScanFileStream(String fileName) throws FileNotFoundException {
        File file = getFileInPath(scannedPath, fileName);
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            LOGGER.info(String.format("Can not read file %s because: %s", fileName, e.getMessage()));
            throw e;
        }
    }

    @Override
    public long getScanFileLength(String fileName) throws FileNotFoundException {
        return getFileInPath(scannedPath, fileName).length();
    }

    @Override
    public FileResource savePrintTemplate(ByteArrayOutputStream bas) throws FileSystemErrorException {
        return saveFile(templatePath, "odt", bas);
    }

    @Override
    public InputStream getPrintTemplateStream(String templateName) throws FileNotFoundException {
        File fileInPath = getFileInPath(templatePath, templateName);
        return new FileInputStream(fileInPath);
    }

    @Override
    public StreamResource getPrintTemplateStreamResource(String fileName) throws FileNotFoundException {
        InputStream inputStream = getPrintTemplateStream(fileName);
        return new StreamResource(() -> inputStream, fileName);
    }

    /**
     * Воздаёт новый файл по указанному пути с указанным расширением и возвращает
     * созданный файл.
     *
     * @param path путь создания файла.
     * @param fileExtension расширение файла.
     * @return созданный файл.
     */
    @SuppressWarnings("unused")
    private File newFileInPath(String path, String fileExtension) {
        // Вероятность, что будет сгенерировано существующее имя файла - почти нулевая,
        // но на всякий случай проверяем существование файла в цикле.
        while (true) {
            File file = new File(String.format("%s/%s.%s", path, UUID.randomUUID().toString(), fileExtension));
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, String.format(
                        "Can not access to folder %s because: %s", path, e.getMessage()));
                }
                return file;
            }
        }
    }

    /**
     * Возвращает файл по указанному пути и имени файла. Проверяет существование файла
     * и возможность чтения.
     *
     * @param path путь файла.
     * @param fileName название файла.
     * @return файл.
     * @throws FileNotFoundException если файл не найден или невозможно его прочитать.
     */
    private File getFileInPath(String path, String fileName) throws FileNotFoundException {
        File file = new File(String.format("%s/%s", path, fileName));
        if (file.exists() && file.canRead()) {
            return file;
        } else {
            LOGGER.info(String.format("File %s not exists or not access", fileName));
            throw new FileNotFoundException(fileName);
        }
    }

    /**
     * Сохраняет файл по указанному пути, с указанным расширением из {@link ByteArrayOutputStream}.
     *
     * @param path путь сохранения файла.
     * @param extension расширение файла.
     * @param bas поток с содержимым файла
     * @return ресурс сохранённого файла.
     * @throws FileSystemErrorException если при сохранении произошла ошибка файловой системы.
     */
    private FileResource saveFile(String path, String extension, ByteArrayOutputStream bas) throws FileSystemErrorException {
        File file = newFileInPath(path, extension);
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(bas.toByteArray());

            return new FileResource(file);
        } catch (Exception e) {
            throw new FileSystemErrorException(e.getMessage());
        }
    }

    /**
     * Проверяет наличие директории по указанному пути. При остутствии директории
     * пытается её создать. Проверяет возможность создания файлов файлов в
     * директории путём создания и удаления пустого файла.
     *
     * @param path путь к директории.
     */
    @SuppressWarnings("unused")
    private void checkAndCreateFolder(String path) {
        File file = new File(path);
        if (!file.exists())
            file.mkdir();

        try {
            String testPath = String.format("%s/%s.test", path, UUID.randomUUID().toString());
            File testFile = new File(testPath);
            testFile.createNewFile();
            testFile.delete();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, String.format(
                "Can not access to folder %s because: %s", path, e.getMessage()));
            System.exit(-1);
        }
    }

    /**
     * Проверяет шаблоны документов на наличие шаблона в файловом хранилище. При отсутствии
     * шаблона пытается скопировать из ресурсов системы шаблон по имени.
     */
    private void updateTemplates() {
        List<PrintTemplateFile> systemTemplates = printTemplateFileService.findAll();
        systemTemplates.forEach(template -> {
            File file = new File(String.format("%s/%s", templatePath, template.getFileName()));
            if (file.exists())
                return;

            Arrays.stream(resourceTemplates).collect(Collectors.toSet()).stream()
                .filter(item -> Objects.equals(item.getFilename(), template.getFileName()))
                .findFirst()
                .ifPresent(item -> copyTemplateFromResource(item, file));

        });
    }

    /**
     * Копирует файл из ресурсов в файловую систему сервера.
     *
     * @param from файл ресурсов, который нужно скопировать.
     * @param to файл в файловой системе, в который нужно скопировать.
     */
    private void copyTemplateFromResource(org.springframework.core.io.Resource from, File to) {
        try {
        InputStream source = from.getInputStream();
        FileOutputStream target = new FileOutputStream(to);

        FileCopyUtils.copy(source, target);

        source.close();
        target.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

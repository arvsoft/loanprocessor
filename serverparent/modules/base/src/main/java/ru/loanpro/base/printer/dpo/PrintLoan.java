package ru.loanpro.base.printer.dpo;

import java.util.List;

/**
 * Печатаемый займ.
 *
 * @author Oleg Brizhevatikh
 * @author Maksim Askhaev
 */
public class PrintLoan {

    private String department;//наименование отдела выдачи займов
    private String loanNumber;//номер займа
    private String loanDate;//дата займа цифрой
    private String loanDateS;//дата займа строкой

    private String ruPSK;//полная стоимость кредита цифрами
    private String ruPSKS;//полная стоимость кредита строкой (без дробной части)

    private String calcScheme;//схема расчета, дифференцированный/аннуитетный
    private String returnTerm;//тип срока возврата

    private String loanBalance;//размер займа цифрами в полном объеме, включая дробную часть
    private String loanBalanceS;//размер займа строкой в полном объеме, включая дробную часть

    private String kzLoanBalance27;//размер займа цифрами в полном объеме, включая дробную часть * 0,27% (для Казахстана)
    private String kzLoanBalance27S;//размер займа строкой в полном объеме, включая дробную часть * 0,27% (для Казахстана)

    private String curr;//валюта займа строкой мн. числа
    private String currPL;//валюта займа строкой мн. числа
    private String currISO;//валюта займа кодом ISO
    private String currS;//валюта займа символом

    private String interestRate;//процентная ставка цифрами
    private String interestRateS;//процентная ставка строкой (без дробной части)

    private String interestBalance;//сумма по процентам цифрами
    private String interestBalanceS;//сумма по процентам строкой (без дробной части)

    private String totalBalance;//сумма займа + сумма по процентам цифрами
    private String totalBalanceS;//сумма займа + сумма по процентам строкой (без дробной части)

    private String interestType;//тип процентной ставки, % в день, % в месяц и т.д.
    private String interestTypeS;//тип процентной ставки строкой, процентов в день, процентов в месяц и т.д.

    private String term;//срок займа цифрами
    private String termS;//срок займа строкой
    private String termType;//тип срока займа

    private String penaltyType;//тип неустойки
    private String penalty;//размер неустойки
    private String penaltyS;//размер неустойки строкой

    private String fine;//размер штрафа цифрами
    private String fineS;//размер штрафа строкой

    private String dateLastPay;//дата последнего платежа цифрами
    private String dateLastPayS;//дата последнего платежа строкой

    //Сведения из Заявки и Кредитного продукта
    private String requestNumber;//номер заявки
    private String requestDate;//дата заявки цифрой
    private String requestDateS;//дата заявки строкой
    private String loanProductName;//наименование Кредитного продукта
    private String loanProductDescription;//описание Кредитного продукта

    //Данные о пролонгации
    private String prolongDepartment;//отдел пролонгации
    private String prolongOrdNo;//порядковый номер пролонгации
    private String prolongDate;//дата пролонгации
    private String prolongDateS;//дата пролонгации строкой
    private String prolongValue;//сумма пролонгации
    private String prolongValueS;//сумма пролонгации строкой
    private String prolongCalculation;//расчет пролонгации
    private String prolongCalculationS;//расчет пролонгации строкой
    private String prolongAgreement;//номер соглашения о пролонгации
    private String recalculationType;//тип пересчета графика

    private List<PrintScheduleList> schedules;

    private List<PrintPaymentList> payments;

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(String loanNumber) {
        this.loanNumber = loanNumber;
    }

    public String getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(String loanDate) {
        this.loanDate = loanDate;
    }

    public String getLoanDateS() {
        return loanDateS;
    }

    public void setLoanDateS(String loanDateS) {
        this.loanDateS = loanDateS;
    }

    public String getRuPSK() {
        return ruPSK;
    }

    public void setRuPSK(String ruPSK) {
        this.ruPSK = ruPSK;
    }

    public String getRuPSKS() {
        return ruPSKS;
    }

    public void setRuPSKS(String ruPSKS) {
        this.ruPSKS = ruPSKS;
    }

    public String getCalcScheme() {
        return calcScheme;
    }

    public void setCalcScheme(String calcScheme) {
        this.calcScheme = calcScheme;
    }

    public String getReturnTerm() {
        return returnTerm;
    }

    public void setReturnTerm(String returnTerm) {
        this.returnTerm = returnTerm;
    }

    public String getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(String loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getLoanBalanceS() {
        return loanBalanceS;
    }

    public void setLoanBalanceS(String loanBalanceS) {
        this.loanBalanceS = loanBalanceS;
    }

    public String getKzLoanBalance27() {
        return kzLoanBalance27;
    }

    public void setKzLoanBalance27(String kzLoanBalance27) {
        this.kzLoanBalance27 = kzLoanBalance27;
    }

    public String getKzLoanBalance27S() {
        return kzLoanBalance27S;
    }

    public void setKzLoanBalance27S(String kzLoanBalance27S) {
        this.kzLoanBalance27S = kzLoanBalance27S;
    }

    public String getInterestBalance() {
        return interestBalance;
    }

    public void setInterestBalance(String interestBalance) {
        this.interestBalance = interestBalance;
    }

    public String getInterestBalanceS() {
        return interestBalanceS;
    }

    public void setInterestBalanceS(String interestBalanceS) {
        this.interestBalanceS = interestBalanceS;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getTotalBalanceS() {
        return totalBalanceS;
    }

    public void setTotalBalanceS(String totalBalanceS) {
        this.totalBalanceS = totalBalanceS;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public String getCurrPL() {
        return currPL;
    }

    public void setCurrPL(String currPL) {
        this.currPL = currPL;
    }

    public String getCurrISO() {
        return currISO;
    }

    public void setCurrISO(String currISO) {
        this.currISO = currISO;
    }

    public String getCurrS() {
        return currS;
    }

    public void setCurrS(String currS) {
        this.currS = currS;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getInterestRateS() {
        return interestRateS;
    }

    public void setInterestRateS(String interestRateS) {
        this.interestRateS = interestRateS;
    }

    public String getInterestType() {
        return interestType;
    }

    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }

    public String getInterestTypeS() {
        return interestTypeS;
    }

    public void setInterestTypeS(String interestTypeS) {
        this.interestTypeS = interestTypeS;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTermS() {
        return termS;
    }

    public void setTermS(String termS) {
        this.termS = termS;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public String getPenaltyType() {
        return penaltyType;
    }

    public void setPenaltyType(String penaltyType) {
        this.penaltyType = penaltyType;
    }

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getPenaltyS() {
        return penaltyS;
    }

    public void setPenaltyS(String penaltyS) {
        this.penaltyS = penaltyS;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public String getFineS() {
        return fineS;
    }

    public void setFineS(String fineS) {
        this.fineS = fineS;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestDateS() {
        return requestDateS;
    }

    public void setRequestDateS(String requestDateS) {
        this.requestDateS = requestDateS;
    }

    public String getLoanProductName() {
        return loanProductName;
    }

    public void setLoanProductName(String loanProductName) {
        this.loanProductName = loanProductName;
    }

    public String getLoanProductDescription() {
        return loanProductDescription;
    }

    public void setLoanProductDescription(String loanProductDescription) {
        this.loanProductDescription = loanProductDescription;
    }

    public String getDateLastPay() {
        return dateLastPay;
    }

    public void setDateLastPay(String dateLastPay) {
        this.dateLastPay = dateLastPay;
    }

    public String getDateLastPayS() {
        return dateLastPayS;
    }

    public void setDateLastPayS(String dateLastPayS) {
        this.dateLastPayS = dateLastPayS;
    }

    public List<PrintScheduleList> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<PrintScheduleList> schedules) {
        this.schedules = schedules;
    }

    public List<PrintPaymentList> getPayments() {
        return payments;
    }

    public void setPayments(List<PrintPaymentList> payments) {
        this.payments = payments;
    }

    public String getProlongDepartment() {
        return prolongDepartment;
    }

    public void setProlongDepartment(String prolongDepartment) {
        this.prolongDepartment = prolongDepartment;
    }

    public String getProlongOrdNo() {
        return prolongOrdNo;
    }

    public void setProlongOrdNo(String prolongOrdNo) {
        this.prolongOrdNo = prolongOrdNo;
    }

    public String getProlongDate() {
        return prolongDate;
    }

    public void setProlongDate(String prolongDate) {
        this.prolongDate = prolongDate;
    }

    public String getProlongDateS() {
        return prolongDateS;
    }

    public void setProlongDateS(String prolongDateS) {
        this.prolongDateS = prolongDateS;
    }

    public String getProlongValue() {
        return prolongValue;
    }

    public void setProlongValue(String prolongValue) {
        this.prolongValue = prolongValue;
    }

    public String getProlongValueS() {
        return prolongValueS;
    }

    public void setProlongValueS(String prolongValueS) {
        this.prolongValueS = prolongValueS;
    }

    public String getProlongCalculation() {
        return prolongCalculation;
    }

    public void setProlongCalculation(String prolongCalculation) {
        this.prolongCalculation = prolongCalculation;
    }

    public String getProlongCalculationS() {
        return prolongCalculationS;
    }

    public void setProlongCalculationS(String prolongCalculationS) {
        this.prolongCalculationS = prolongCalculationS;
    }

    public String getProlongAgreement() {
        return prolongAgreement;
    }

    public void setProlongAgreement(String prolongAgreement) {
        this.prolongAgreement = prolongAgreement;
    }

    public String getRecalculationType() {
        return recalculationType;
    }

    public void setRecalculationType(String recalculationType) {
        this.recalculationType = recalculationType;
    }
}

package ru.loanpro.base.printer.dpo;

/**
 * Печатаемый список фактических платежей по займу
 *
 * @author Maksim Askhaev
 */
public class PrintPaymentList {

    private String ordNo;
    private String date;
    private String term;
    private String overdueTerm;
    private String principal;
    private String interest;
    private String penalty;
    private String fine;
    private String payment;
    private String balance;

    public PrintPaymentList() {
    }

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getOverdueTerm() {
        return overdueTerm;
    }

    public void setOverdueTerm(String overdueTerm) {
        this.overdueTerm = overdueTerm;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}

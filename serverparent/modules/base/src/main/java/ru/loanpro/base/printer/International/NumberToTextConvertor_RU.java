package ru.loanpro.base.printer.International;

import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * Конвертер числа в строковое представление
 * Русский варинт
 *
 * @author Maksim Askhaev
 */
public class NumberToTextConvertor_RU {

    private static final String[] hundNames = {"", " сто", " двести", " триста", " четыреста", " пятьсот", " шестьсот",
        " семьсот", " восемьсот", " девятьсот"};

    private static final String[] tensNames = {"", " десять", " двадцать", " тридцать", " сорок", " пятьдесят", " шестьдесят",
        " семьдесят", " восемьдесят", " девяносто"};

    private static final String[] numNames = {"", " один", " два", " три", " четыре", " пять", " шесть", " семь",
        " восемь", " девять", " десять", " одиннадцать", " двенадцать", " тринадцать", " четырнадцать", " пятнадцать",
        " шестнадцать", " семнадцать", " восемнадцать", " девятнадцать"};

    //для тысяч
    private static final String[] numNamesTh = {"", " одна", " две", " три", " четыре", " пять", " шесть", " семь",
        " восемь", " девять", " десять", " одиннадцать", " двенадцать", " тринадцать", " четырнадцать", " пятнадцать",
        " шестнадцать", " семнадцать", " восемнадцать", " девятнадцать"};

    /**
     * Метод конвертации числа в текстовое предствление
     * Используется для конвертации приставок в разрядах - к миллиардам, миллионам, тысячам
     * Входное значение должно быть меньше тысячи
     *
     * @param number   - число для конвертации (приставка)
     * @param thousand - идентификатор того, что нам передается значение из разряда тысяч,
     *                 в этом случае используется другой массив для формирования текстового
     *                 представления для значений "один", "два"
     * @return текстовое представление числа number
     */
    private static String convertLessThanOneThousandEx(int number, boolean thousand) {
        String soFar;

        if (number < 100) {
            if (number % 100 < 20) {
                if (!thousand) soFar = numNames[number % 100];
                else soFar = numNamesTh[number % 100];
                number /= 100;
            } else {
                if (!thousand) soFar = numNames[number % 10];
                else soFar = numNamesTh[number % 10];
                number /= 10;

                soFar = tensNames[number % 10] + soFar;
                number /= 10;
            }
        } else {
            if (number % 100 < 20) {
                if (!thousand) soFar = numNames[number % 100];
                else soFar = numNamesTh[number % 100];
                number /= 100;

                soFar = hundNames[number] + soFar;
                number /= 10;
            } else {
                if (!thousand) soFar = numNames[number % 10];
                else soFar = numNamesTh[number % 10];
                number /= 10;

                soFar = tensNames[number % 10] + soFar;
                number /= 10;

                soFar = hundNames[number % 10] + soFar;
                number /= 10;
            }

        }
        if (number == 0) return soFar;
        return soFar;
    }

    /**
     * Метод конвертации числа в текстовое представление
     *
     * @param number - число для конвертации
     * @return текстовое представление числа number
     */
    public static String convert(long number) {
        // 0 to 999 999 999 999
        if (number == 0) {
            return "ноль";
        }

        String snumber = Long.toString(number);

        // pad with "0"
        String mask = "000000000000";
        DecimalFormat df = new DecimalFormat(mask);
        snumber = df.format(number);

        // XXXnnnnnnnnn
        int billions = Integer.parseInt(snumber.substring(0, 3));
        // nnnXXXnnnnnn
        int millions = Integer.parseInt(snumber.substring(3, 6));
        // nnnnnnXXXnnn
        int hundredThousands = Integer.parseInt(snumber.substring(6, 9));
        // nnnnnnnnnXXX
        int thousands = Integer.parseInt(snumber.substring(9, 12));

        String mask1 = "000";
        DecimalFormat df1 = new DecimalFormat(mask1);

        String tradBillions;
        if (billions == 0) {
            tradBillions = "";
        } else {
            String value = df1.format(billions);
            if (value.substring(1, 3).equals("11")) {
                tradBillions = convertLessThanOneThousandEx(billions, false)
                    + " миллиардов ";
            } else if (value.substring(2, 3).equals("1")) {
                tradBillions = convertLessThanOneThousandEx(billions, false)
                    + " миллиард ";
            } else if (Arrays.asList(2, 3, 4).contains(Integer.valueOf(value.substring(2, 3)))) {
                tradBillions = convertLessThanOneThousandEx(billions, false)
                    + " миллиарда ";
            } else {
                tradBillions = convertLessThanOneThousandEx(billions, false)
                    + " миллиардов ";
            }
        }
        String result = tradBillions;

        String tradMillions;
        if (millions == 0) {
            tradMillions = "";
        } else {
            String value = df1.format(millions);
            if (Arrays.asList(11, 12, 13, 14).contains(Integer.valueOf(value.substring(1)))) {
                tradMillions = convertLessThanOneThousandEx(millions, false)
                    + " миллионов ";
            } else if (value.substring(2).equals("1")) {
                tradMillions = convertLessThanOneThousandEx(millions, false)
                    + " миллион ";
            } else if (Arrays.asList(2, 3, 4).contains(Integer.valueOf(value.substring(2)))) {
                tradMillions = convertLessThanOneThousandEx(millions, false)
                    + " миллиона ";
            } else {
                tradMillions = convertLessThanOneThousandEx(millions, false)
                    + " миллионов ";
            }
        }
        result = result + tradMillions;

        String tradHundredThousands;
        if (hundredThousands == 0) {
            tradHundredThousands = "";
        } else {
            String value = df1.format(hundredThousands);
            if (Arrays.asList(11, 12, 13, 14).contains(Integer.valueOf(value.substring(1)))) {
                tradHundredThousands = convertLessThanOneThousandEx(hundredThousands, true)
                    + " тысяч ";
            } else if (value.substring(2).equals("1")) {
                tradHundredThousands = convertLessThanOneThousandEx(hundredThousands, true)
                    + " тысяча ";
            } else if (Arrays.asList(2, 3, 4).contains(Integer.valueOf(value.substring(2)))) {
                tradHundredThousands = convertLessThanOneThousandEx(hundredThousands, true)
                    + " тысячи ";
            } else {
                tradHundredThousands = convertLessThanOneThousandEx(hundredThousands, true)
                    + " тысяч ";
            }
        }
        result = result + tradHundredThousands;

        String tradThousand;
        tradThousand = convertLessThanOneThousandEx(thousands, false);
        result = result + tradThousand;

        //удаляем лишнее
        return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
    }

}

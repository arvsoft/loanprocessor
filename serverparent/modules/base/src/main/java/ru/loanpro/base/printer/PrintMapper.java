package ru.loanpro.base.printer;

import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.account.domain.BaseAccount;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.domain.JuridicalAccount;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.account.service.DepartmentAccountService;
import ru.loanpro.base.account.service.PhysicalAccountService;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.domain.JuridicalClient;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.client.domain.details.Address;
import ru.loanpro.base.client.domain.details.Document;
import ru.loanpro.base.client.domain.details.Education;
import ru.loanpro.base.client.domain.details.Job;
import ru.loanpro.base.client.domain.details.Note;
import ru.loanpro.base.client.domain.details.Relative;
import ru.loanpro.base.client.service.PhysicalClientService;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.printer.International.NumberToTextConvertor_EN;
import ru.loanpro.base.printer.International.NumberToTextConvertor_RU;
import ru.loanpro.base.printer.dpo.PrintClient;
import ru.loanpro.base.printer.dpo.PrintDepartment;
import ru.loanpro.base.printer.dpo.PrintJuridicalClient;
import ru.loanpro.base.printer.dpo.PrintLoan;
import ru.loanpro.base.printer.dpo.PrintOperation;
import ru.loanpro.base.printer.dpo.PrintPayment;
import ru.loanpro.base.printer.dpo.PrintPaymentList;
import ru.loanpro.base.printer.dpo.PrintPhysicalClient;
import ru.loanpro.base.printer.dpo.PrintScheduleList;
import ru.loanpro.base.printer.dpo.PrintSettings;
import ru.loanpro.base.printer.dpo.PrintUser;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.Settings;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.directory.DocumentStatus;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.settings.service.SettingsService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Маппер из сущностей системы в печатаемые сущности.
 *
 * @author Oleg Brizhevatikh
 * @author Maksim Askhaev
 */
@Service
@VaadinSessionScope
public class PrintMapper {

    private final ChronometerService chronometerService;
    private final ConfigurationService configurationService;
    private final PhysicalClientService physicalClientService;
    private final PhysicalAccountService physicalAccountService;
    private final DepartmentAccountService departmentAccountService;
    private final SettingsService settingsService;
    private final I18N i18n;
    private final DepartmentService departmentService;
    private final String udostoverenieLName;//удостоверение личности
    private final String passportName;
    private final String driverLicenseName;
    private final SystemLocale systemLocale;
    private final String documentMask;
    private final String addressMask;
    private final String addressRegStatusMask;
    private final String addressLiveStatusMask;
    private final String lastJobMask;

    @Autowired
    public PrintMapper(ChronometerService chronometerService, ConfigurationService configurationService, I18N i18n,
                       PhysicalClientService physicalClientService, PhysicalAccountService physicalAccountService,
                       DepartmentAccountService departmentAccountService, SettingsService settingsService,
                       DepartmentService departmentService) {
        this.chronometerService = chronometerService;
        this.configurationService = configurationService;
        this.physicalClientService = physicalClientService;
        this.physicalAccountService = physicalAccountService;
        this.departmentAccountService = departmentAccountService;
        this.settingsService = settingsService;
        this.i18n = i18n;
        this.departmentService = departmentService;
        systemLocale = configurationService.getSystemLocale();

        switch (systemLocale) {
            case ENGLISH: {
                passportName = "Passport";
                driverLicenseName = "Driver";
                udostoverenieLName = "No";
                documentMask = "%s %s от %s, issued by %s";
                addressRegStatusMask = "reg";
                addressLiveStatusMask = "liv";
                addressMask = "%s, %s %s %s %s, %s";
                lastJobMask = "%s, %s, %s, %s";
                break;
            }
            case RUSSIAN: {
                passportName = "Паспорт";
                driverLicenseName = "Водител";
                udostoverenieLName = "Удостоверение личност";
                documentMask = "%s %s от %s, выдан %s";
                addressRegStatusMask = "reg";
                addressLiveStatusMask = "liv";
                addressMask = "%s, %s %s %s %s, %s";
                lastJobMask = "%s, %s, %s, %s";
                break;
            }
            default: {
                passportName = "Passport";
                driverLicenseName = "Driver";
                udostoverenieLName = "No";
                documentMask = "%s %s от %s, issued by %s";
                addressRegStatusMask = "reg";
                addressLiveStatusMask = "liv";
                addressMask = "%s, %s %s %s %s, %s";
                lastJobMask = "%s, %s, %s, %s";
            }
        }
    }

    /**
     * Маппит данные пользователя системы.
     *
     * @param user пользователь.
     * @return печатаемый пользователь.
     */
    public PrintUser toPrintUser(User user) {
        PrintUser p = new PrintUser();

        p.setName(user.getUsername());

        p.setInitName(String.format("%s %s %s",
            user.getLastName(),
            user.getFirstName() == null ? "" : user.getFirstName().substring(0, 1).concat("."),
            user.getMiddleName() == null ? "" : user.getMiddleName().substring(0, 1).concat("."))
            .replaceAll("( +)", " "));

        return p;
    }

    /**
     * Конвертирует Long в строковое представление
     * с учетом языковой локали
     * Поднимает регистр первого символа строки
     *
     * @param value  входное значение.
     * @param locale языковая локаль
     * @return строковое представление
     */
    private String convertLongToText(Long value, SystemLocale locale) {
        String text;
        switch (locale) {
            case ENGLISH:
                text = NumberToTextConvertor_EN.convert(value);
                break;
            case RUSSIAN:
                text = NumberToTextConvertor_RU.convert(value);
                break;
            default:
                text = "locale is not defined";
        }
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    /**
     * Маппит данные общих настроек.
     *
     * @return общие настройки.
     */
    public PrintSettings toPrintSetting() {
        PrintSettings ps = new PrintSettings();

        ps.setName(settingsService.get(String.class, Settings.INFO_NAME, ""));
        ps.setFullName(settingsService.get(String.class, Settings.INFO_FULL_NAME, ""));
        ps.setRegNumber1(settingsService.get(String.class, Settings.INFO_REG_NUMBER_1, ""));
        ps.setRegNumber2(settingsService.get(String.class, Settings.INFO_REG_NUMBER_2, ""));
        ps.setRegNumber3(settingsService.get(String.class, Settings.INFO_REG_NUMBER_3, ""));
        ps.setRegNumber4(settingsService.get(String.class, Settings.INFO_REG_NUMBER_4, ""));
        ps.setRegNumber5(settingsService.get(String.class, Settings.INFO_REG_NUMBER_5, ""));
        ps.setRegNumber6(settingsService.get(String.class, Settings.INFO_REG_NUMBER_6, ""));
        ps.setPhones(settingsService.get(String.class, Settings.INFO_PHONES, ""));
        ps.setJuridicalAddress(settingsService.get(String.class, Settings.INFO_JURIDICAL_ADDRESS, ""));
        ps.setActualAddress(settingsService.get(String.class, Settings.INFO_ACTUAL_ADDRESS, ""));
        ps.setEmail(settingsService.get(String.class, Settings.INFO_EMAIL, ""));
        ps.setBankAccount1(settingsService.get(String.class, Settings.INFO_BANK_ACCOUNT_1, ""));
        ps.setBankAccount2(settingsService.get(String.class, Settings.INFO_BANK_ACCOUNT_2, ""));
        ps.setDirector(settingsService.get(String.class, Settings.INFO_DIRECTOR, ""));
        ps.setDirectorName(settingsService.get(String.class, Settings.INFO_DIRECTOR_NAME, ""));
        ps.setAssistant1(settingsService.get(String.class, Settings.INFO_ASSISTANT_1, ""));
        ps.setAssistantName1(settingsService.get(String.class, Settings.INFO_ASSISTANT_NAME_1, ""));
        ps.setAssistant2(settingsService.get(String.class, Settings.INFO_ASSISTANT_2, ""));
        ps.setAssistantName2(settingsService.get(String.class, Settings.INFO_ASSISTANT_NAME_2, ""));

        return ps;
    }

    /**
     * Маппит данные займа.
     *
     * @param l займ.
     * @return печатаемый займ.
     */
    public PrintLoan toPrintLoan(Loan l) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());
        DecimalFormat moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        DecimalFormat percentFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        percentFormat.setMinimumFractionDigits(3);
        percentFormat.setMaximumFractionDigits(3);

        PrintLoan p = new PrintLoan();

        p.setDepartment(l.getDepartment().getName());
        p.setLoanNumber(l.getLoanNumber());//loanNumber
        p.setLoanDate(chronometerService.zonedDateTimeToString(l.getLoanIssueDate()));//дата выдачи займа цифрой
        p.setLoanDateS(chronometerService.zonedDateTimeToString(l.getLoanIssueDate()));//дата выдачи займа строкой

        p.setRuPSK(l.getPskValue() == null ? "0.000" : percentFormat.format(l.getPskValue()));//ПСК цифрами
        p.setRuPSKS(l.getPskValue() == null ? "0.000" : percentFormat.format(l.getPskValue()));//ПСК строкой

        p.setCalcScheme(l.getCalcScheme().getName());//Схема расчета//аннуитетный/дифференцированный
        p.setReturnTerm(l.getReturnTerm().getName());//Тип возврата

        p.setLoanBalance(l.getLoanBalance() == null ? "0.00" : moneyFormat.format(l.getLoanBalance()));//размер займа цифрами
        p.setLoanBalanceS(l.getLoanBalance() == null ?
            "0.00" : convertLongToText(l.getLoanBalance().longValue(), systemLocale));//размер займа строкой

        p.setKzLoanBalance27(l.getLoanBalance() == null ?
            "0.00" : moneyFormat.format(l.getLoanBalance().multiply(new BigDecimal(1.027).setScale(3, BigDecimal.ROUND_HALF_EVEN))));//размер займа цифрами
        p.setKzLoanBalance27S(l.getLoanBalance() == null ?
            "0.00" : convertLongToText(l.getLoanBalance().multiply(new BigDecimal(1.027).setScale(3, BigDecimal.ROUND_HALF_EVEN)).longValue(), systemLocale));//размер займа строкой

        p.setCurr(l.getCurrency().getDisplayName());//валюта
        p.setCurrISO(l.getCurrency().getCurrencyCode());
        p.setCurrS(l.getCurrency().getSymbol());
        p.setCurrPL(l.getCurrency().getDisplayName());

        p.setInterestRate(l.getInterestValue() == null ? "0.000" : percentFormat.format(l.getInterestValue()));//процентная ставка цифрами
        p.setInterestRateS(l.getInterestValue() == null ?
            "0.000" : convertLongToText(l.getInterestValue().longValue(), systemLocale));//процентная ставка строкой

        p.setInterestType(l.getInterestType() == null ? "" : i18n.get(l.getInterestType().getName()));//тип процентной ставки цифрами
        p.setInterestTypeS(l.getInterestType() == null ? "" : i18n.get(l.getInterestType().getName2()));//тип процентной ставки строкой

        p.setTerm(String.valueOf(l.getLoanTerm()));//срок займа цифрами
        p.setTermS(convertLongToText((long) l.getLoanTerm(), systemLocale));//срок займа строкой

        p.setTermType(l.getLoanTermType() == null ? "" : i18n.get(l.getLoanTermType().getName()));//тип срока займа

        p.setPenaltyType(i18n.get(l.getPenaltyType().getName()));//тип неустойки
        p.setPenalty(l.getPenaltyValue() == null ? "0.000" : percentFormat.format(l.getPenaltyValue()));

        p.setFine(l.getFineValue() == null ? "0.00" : moneyFormat.format(l.getFineValue()));//штраф цифрами
        p.setFineS(l.getFineValue() == null ?
            "0.00" : convertLongToText(l.getFineValue().longValue(), systemLocale));//штраф строкой

        p.setDateLastPay(l.getLastPayDate() == null ? "" : l.getLastPayDate().format(dateTimeFormatter));//дата последнего платежа цифрами
        p.setDateLastPayS(l.getLastPayDate() == null ? "" : l.getLastPayDate().format(dateTimeFormatter));//дата последнего платежа строкой

        p.setRequestNumber(l.getRequestNumber());
        p.setRequestDate(l.getRequestIssueDate() == null ? "" : chronometerService.zonedDateTimeToString(l.getRequestIssueDate()));

        p.setLoanProductName(l.getName());
        p.setLoanProductDescription(l.getDescription());

        BigDecimal interestBalance = new BigDecimal(0);
        for (Schedule schedule : l.getSchedules()){
            interestBalance = interestBalance.add(schedule.getInterest());
        }
        BigDecimal totalBalance = l.getLoanBalance().add(interestBalance);

        p.setInterestBalance(moneyFormat.format(interestBalance));
        p.setInterestBalanceS(convertLongToText(interestBalance.longValue(), systemLocale));

        p.setTotalBalance(moneyFormat.format(totalBalance));
        p.setTotalBalanceS(convertLongToText(totalBalance.longValue(), systemLocale));

        //данные о пролонгации
        p.setProlongDepartment(l.getProlongDepartment() == null ? "" : l.getProlongDepartment().getName());
        p.setProlongOrdNo(String.valueOf(l.getProlongOrdNo()));
        p.setProlongDate(l.getProlongDate() == null ? "" : l.getProlongDate().format(dateTimeFormatter));
        p.setProlongDateS(l.getProlongDate() == null ? "" : l.getProlongDate().format(dateTimeFormatter));
        p.setProlongValue(l.getProlongValue() == null ? "0.00" : moneyFormat.format(l.getProlongValue()));
        p.setProlongValueS(l.getProlongValue() == null ?
            "0.00" : convertLongToText(l.getProlongValue().longValue(), systemLocale));
        p.setProlongCalculation(l.getProlongCalculation() == null ? "0.00" : moneyFormat.format(l.getProlongCalculation()));
        p.setProlongCalculationS(l.getProlongCalculation() == null ?
            "0.00" : convertLongToText(l.getProlongCalculation().longValue(), systemLocale));
        p.setProlongAgreement(l.getProlongAgrNumber() == null ? "" : l.getProlongAgrNumber());
        p.setRecalculationType(l.getRecalculationType() == null ? "" : i18n.get(l.getRecalculationType().getName()));

        //график рассчитанных платежей
        List<PrintScheduleList> schedules = new ArrayList<>();
        for (Schedule schedule : l.getSchedules()) {
            PrintScheduleList printSchedule = new PrintScheduleList();
            printSchedule.setOrdNo(String.valueOf(schedule.getOrdNo()));
            printSchedule.setDate(dateTimeFormatter.format(schedule.getDate()));
            printSchedule.setTerm(String.valueOf(schedule.getTerm()));
            printSchedule.setPrincipal(moneyFormat.format(schedule.getPrincipal()));
            printSchedule.setInterest(moneyFormat.format(schedule.getInterest()));
            printSchedule.setPayment(moneyFormat.format(schedule.getPayment()));
            printSchedule.setBalance(moneyFormat.format(schedule.getBalance()));
            schedules.add(printSchedule);
        }
        p.setSchedules(schedules);

        //график фактических платежей
        List<PrintPaymentList> payments = new ArrayList<>();
        for (Payment payment : l.getPayments()) {
            PrintPaymentList printPayment = new PrintPaymentList();
            printPayment.setOrdNo(String.valueOf(payment.getOrdNo()));
            printPayment.setDate(dateTimeFormatter.format(payment.getPayDate()));
            printPayment.setTerm(String.valueOf(payment.getTerm()));
            printPayment.setOverdueTerm(String.valueOf(payment.getOverdueTerm()));
            printPayment.setPrincipal(moneyFormat.format(payment.getPrincipal()));
            printPayment.setInterest(moneyFormat.format(payment.getInterest()));
            printPayment.setPenalty(moneyFormat.format(payment.getPenalty()));
            printPayment.setFine(moneyFormat.format(payment.getFine()));
            printPayment.setPayment(moneyFormat.format(payment.getPayment()));
            printPayment.setBalance(moneyFormat.format(payment.getBalanceAfter()));
            payments.add(printPayment);
        }
        p.setPayments(payments);

        return p;
    }

    /**
     * Маппит данные платежа по займу.
     *
     * @param pa платеж по займу.
     * @return печатаемый платеж по займу.
     */
    public PrintPayment toPrintPayment(Payment pa) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());
        DecimalFormat moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        DecimalFormat percentFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        percentFormat.setMinimumFractionDigits(3);
        percentFormat.setMaximumFractionDigits(3);

        PrintPayment p = new PrintPayment();

        p.setDepartment(pa.getDepartmentPayment().getName());
        p.setOrdNo(String.valueOf(pa.getOrdNo()));
        p.setScheduleOrdNo(String.valueOf(pa.getScheduleOrdNo()));
        p.setStatus(i18n.get(pa.getStatus().getName()));
        p.setPayDate(pa.getPayDate() == null ? "" : pa.getPayDate().format(dateTimeFormatter));//дата платежа
        p.setPayDateS(pa.getPayDate() == null ? "" : pa.getPayDate().format(dateTimeFormatter));//дата платежа строкой
        p.setTerm(String.valueOf(pa.getTerm()));
        p.setTermS(convertLongToText(pa.getTerm(), systemLocale));
        p.setOverdueTerm(String.valueOf(pa.getOverdueTerm()));
        p.setOverdueTermS(convertLongToText(pa.getOverdueTerm(), systemLocale));

        p.setBalanceBefore(pa.getBalanceBefore() == null ? "0.00" : moneyFormat.format(pa.getBalanceBefore()));//размер остатка перед цифрами
        p.setBalanceBeforeS(pa.getBalanceBefore() == null ?
            "0.00" : convertLongToText(pa.getBalanceBefore().longValue(), systemLocale));//размер остатка перед строкой
        p.setBalanceAfter(pa.getBalanceAfter() == null ? "0.00" : moneyFormat.format(pa.getBalanceAfter()));//размер остатка после цифрами
        p.setBalanceAfterS(pa.getBalanceAfter() == null ?
            "0.00" : convertLongToText(pa.getBalanceAfter().longValue(), systemLocale));//размер остатка после строкой

        p.setPrincipalB(pa.getPrincipalB() == null ? "0.00" : moneyFormat.format(pa.getPrincipalB()));//размер осн. части перед цифрами
        p.setPrincipalBS(pa.getPrincipalB() == null ?
            "0.00" : convertLongToText(pa.getPrincipalB().longValue(), systemLocale));//размер осн. части перед строкой

        p.setInterestB(pa.getInterestB() == null ? "0.00" : moneyFormat.format(pa.getInterestB()));//размер процентов перед цифрами
        p.setInterestBS(pa.getInterestB() == null ?
            "0.00" : convertLongToText(pa.getInterestB().longValue(), systemLocale));//размер процентов перед строкой

        p.setPenaltyB(pa.getPenaltyB() == null ? "0.00" : moneyFormat.format(pa.getPenaltyB()));//размер неустойки перед цифрами
        p.setPenaltyBS(pa.getPenaltyB() == null ?
            "0.00" : convertLongToText(pa.getPenaltyB().longValue(), systemLocale));//размер неустойки перед строкой

        p.setFineB(pa.getFineB() == null ? "0.00" : moneyFormat.format(pa.getFineB()));//размер штрафа перед цифрами
        p.setFineBS(pa.getFineB() == null ?
            "0.00" : convertLongToText(pa.getFineB().longValue(), systemLocale));//размер штрафа перед строкой

        p.setPaymentB(pa.getPaymentB() == null ? "0.00" : moneyFormat.format(pa.getPaymentB()));//размер платежа перед цифрами
        p.setPaymentBS(pa.getPaymentB() == null ?
            "0.00" : convertLongToText(pa.getPaymentB().longValue(), systemLocale));//размер платежа перед строкой

        p.setPrincipalC(pa.getPrincipalC() == null ? "0.00" : moneyFormat.format(pa.getPrincipalC()));//размер осн. части перед цифрами
        p.setPrincipalCS(pa.getPrincipalC() == null ?
            "0.00" : convertLongToText(pa.getPrincipalC().longValue(), systemLocale));//размер осн. части перед строкой

        p.setInterestC(pa.getInterestC() == null ? "0.00" : moneyFormat.format(pa.getInterestC()));//размер процентов перед цифрами
        p.setInterestCS(pa.getInterestC() == null ?
            "0.00" : convertLongToText(pa.getInterestC().longValue(), systemLocale));//размер процентов перед строкой

        p.setPenaltyC(pa.getPenaltyC() == null ? "0.00" : moneyFormat.format(pa.getPenaltyC()));//размер неустойки перед цифрами
        p.setPenaltyCS(pa.getPenaltyC() == null ?
            "0.00" : convertLongToText(pa.getPenaltyC().longValue(), systemLocale));//размер неустойки перед строкой

        p.setFineC(pa.getFineC() == null ? "0.00" : moneyFormat.format(pa.getFineC()));//размер штрафа перед цифрами
        p.setFineCS(pa.getFineC() == null ?
            "0.00" : convertLongToText(pa.getFineC().longValue(), systemLocale));//размер штрафа перед строкой

        p.setPaymentC(pa.getPaymentC() == null ? "0.00" : moneyFormat.format(pa.getPaymentC()));//размер платежа перед цифрами
        p.setPaymentCS(pa.getPaymentC() == null ?
            "0.00" : convertLongToText(pa.getPaymentC().longValue(), systemLocale));//размер платежа перед строкой

        p.setPrincipalP(pa.getPrincipalS() == null ? "0.00" : moneyFormat.format(pa.getPrincipalS()));//размер осн. части перед цифрами
        p.setPrincipalPS(pa.getPrincipalS() == null ?
            "0.00" : convertLongToText(pa.getPrincipalS().longValue(), systemLocale));//размер осн. части перед строкой

        p.setInterestP(pa.getInterestS() == null ? "0.00" : moneyFormat.format(pa.getInterestS()));//размер процентов перед цифрами
        p.setInterestPS(pa.getInterestS() == null ?
            "0.00" : convertLongToText(pa.getInterestS().longValue(), systemLocale));//размер процентов перед строкой

        p.setPenaltyP(pa.getPenaltyS() == null ? "0.00" : moneyFormat.format(pa.getPenaltyS()));//размер неустойки перед цифрами
        p.setPenaltyPS(pa.getPenaltyS() == null ?
            "0.00" : convertLongToText(pa.getPenaltyS().longValue(), systemLocale));//размер неустойки перед строкой

        p.setFineP(pa.getFineS() == null ? "0.00" : moneyFormat.format(pa.getFineS()));//размер штрафа перед цифрами
        p.setFinePS(pa.getFineS() == null ?
            "0.00" : convertLongToText(pa.getFineS().longValue(), systemLocale));//размер штрафа перед строкой

        p.setPaymentP(pa.getPaymentS() == null ? "0.00" : moneyFormat.format(pa.getPaymentS()));//размер платежа перед цифрами
        p.setPaymentPS(pa.getPaymentS() == null ?
            "0.00" : convertLongToText(pa.getPaymentS().longValue(), systemLocale));//размер платежа перед строкой

        p.setPrincipalF(pa.getPrincipal() == null ? "0.00" : moneyFormat.format(pa.getPrincipal()));//размер осн. части перед цифрами
        p.setPrincipalFS(pa.getPrincipal() == null ?
            "0.00" : convertLongToText(pa.getPrincipal().longValue(), systemLocale));//размер осн. части перед строкой

        p.setInterestF(pa.getInterest() == null ? "0.00" : moneyFormat.format(pa.getInterest()));//размер процентов перед цифрами
        p.setInterestFS(pa.getInterest() == null ?
            "0.00" : convertLongToText(pa.getInterest().longValue(), systemLocale));//размер процентов перед строкой

        p.setPenaltyF(pa.getPenalty() == null ? "0.00" : moneyFormat.format(pa.getPenalty()));//размер неустойки перед цифрами
        p.setPenaltyFS(pa.getPenalty() == null ?
            "0.00" : convertLongToText(pa.getPenalty().longValue(), systemLocale));//размер неустойки перед строкой

        p.setFineF(pa.getFine() == null ? "0.00" : moneyFormat.format(pa.getFine()));//размер штрафа перед цифрами
        p.setFineFS(pa.getFine() == null ?
            "0.00" : convertLongToText(pa.getFine().longValue(), systemLocale));//размер штрафа перед строкой

        p.setPaymentF(pa.getPayment() == null ? "0.00" : moneyFormat.format(pa.getPayment()));//размер платежа перед цифрами
        p.setPaymentFS(pa.getPayment() == null ?
            "0.00" : convertLongToText(pa.getPayment().longValue(), systemLocale));//размер платежа перед строкой

        p.setPrincipalE(pa.getPrincipalE() == null ? "0.00" : moneyFormat.format(pa.getPrincipalE()));//размер осн. части перед цифрами
        p.setPrincipalES(pa.getPrincipalE() == null ?
            "0.00" : convertLongToText(pa.getPrincipalE().longValue(), systemLocale));//размер осн. части перед строкоEй

        p.setInterestE(pa.getInterestE() == null ? "0.00" : moneyFormat.format(pa.getInterestE()));//размер процентов перед цифрами
        p.setInterestES(pa.getInterestE() == null ?
            "0.00" : convertLongToText(pa.getInterestE().longValue(), systemLocale));//размер процентов перед строкой

        p.setPenaltyE(pa.getPenaltyE() == null ? "0.00" : moneyFormat.format(pa.getPenaltyE()));//размер неустойки перед цифрами
        p.setPenaltyES(pa.getPenaltyE() == null ?
            "0.00" : convertLongToText(pa.getPenaltyE().longValue(), systemLocale));//размер неустойки перед строкой

        p.setFineE(pa.getFineE() == null ? "0.00" : moneyFormat.format(pa.getFineE()));//размер штрафа перед цифрами
        p.setFineES(pa.getFineE() == null ?
            "0.00" : convertLongToText(pa.getFineE().longValue(), systemLocale));//размер штрафа перед строкой

        p.setPaymentE(pa.getPaymentE() == null ? "0.00" : moneyFormat.format(pa.getPaymentE()));//размер платежа перед цифрами
        p.setPaymentES(pa.getPaymentE() == null ?
            "0.00" : convertLongToText(pa.getPaymentE().longValue(), systemLocale));//размер платежа перед строкой

        return p;
    }

    /**
     * Маппит данные операции.
     *
     * @param op операция.
     * @return печатаемая операция.
     */
    public PrintOperation toPrintOperation(Operation op) {
        DateTimeFormatter timeFormatter =
            DateTimeFormatter.ofPattern("HH:mm");

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());
        DecimalFormat moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        DecimalFormat percentFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        percentFormat.setMinimumFractionDigits(3);
        percentFormat.setMaximumFractionDigits(3);

        PrintOperation o = new PrintOperation();

        o.setDepartment(op.getDepartment().getName());
        o.setPaymentMethod(i18n.get(op.getPaymentMethod().getName()));
        o.setOperationDate(chronometerService.zonedDateTimeToString(op.getOperationDateTime(), dateTimeFormatter));
        o.setOperationNumber(op.getOperationNumber());
        o.setOperationTime(chronometerService.zonedDateTimeToString(op.getOperationDateTime(), timeFormatter));

        o.setOperationValue(op.getOperationValue() == null ? "0.00" : moneyFormat.format(op.getOperationValue()));
        o.setOperationValueS(op.getOperationValue() == null ?
            "0.00" : convertLongToText(op.getOperationValue().longValue(), systemLocale));
        o.setCurrency(op.getCurrency().getSymbol());

        o.setLoanNumber(op.getLoan() == null ? "" : op.getLoan().getLoanNumber());
        o.setLoanDate(op.getLoan() == null ? "" : chronometerService.zonedDateTimeToString(op.getLoan().getLoanIssueDate()));

        o.setPaymentNumber(op.getPayment() == null ? "" : String.valueOf(op.getPayment().getOrdNo()));
        o.setPaymentDate(op.getPayment() == null ? "" : op.getPayment().getPayDate().format(dateTimeFormatter));
        o.setPaymentPart(op.getPaymentPart() == null ? "" : op.getPaymentPart());

        o.setNotes(op.getNotes() == null ? "" : op.getNotes());

        o.setAccountPayer(op.getAccountPayer() == null ? "" : op.getAccountPayer().getNumber());
        o.setAccountPayerName(getNameFromAccount(op.getAccountPayer()));

        o.setAccountRecipient(op.getAccountRecipient() == null ? "" : op.getAccountRecipient().getNumber());
        o.setAccountRecipientName(getNameFromAccount(op.getAccountRecipient()));

        o.setPayer(op.getPayer() == null ? "" : op.getPayer().getDisplayName());
        o.setRecipient(op.getRecipient() == null ? "" : op.getRecipient().getDisplayName());

        o.setPayerTotal(String.format("%s %s %s", o.getAccountPayer(), o.getAccountPayerName(), o.getPayer()).trim());
        o.setRecipientTotal(String.format("%s %s %s", o.getAccountRecipient(), o.getAccountRecipientName(), o.getRecipient()).trim());

        return o;
    }

    private String getNameFromAccount(BaseAccount account) {

        if (account == null || account.getId() == null) return "";

        UUID id = account.getId();
        if (account.getClass() == PhysicalAccount.class) {
            PhysicalAccount physicalAccount = physicalAccountService.getOneFull(id);
            PhysicalClientDetails clientDetails = physicalAccount.getClientDetails();
            PhysicalClient client = physicalClientService.findOneByPhysicalClientDetails(clientDetails);
            return client.getDisplayName();
        } else if (account.getClass() == DepartmentAccount.class) {
            DepartmentAccount departmentAccount = departmentAccountService.getOneFull(id);
            Department department = departmentAccount.getDepartment();
            return department.getName();
        } else if (account.getClass() == JuridicalAccount.class) {
            return "";
        } else return "";
    }

    /**
     * Маппит данные подразделения системы.
     *
     * @param printedDepartment подразделение.
     * @return печатаемое подразделение.
     */

    public PrintDepartment toPrintDepartment(Department printedDepartment) {
        Department department = departmentService.findOneFull(printedDepartment.getId());
        PrintDepartment p = new PrintDepartment();

        p.setName(department.getName() == null ? "" : department.getName());
        p.setDescription(department.getDescription() == null ? "" : department.getDescription());
        p.setPhoneNumber(department.getPhone() == null ? "" : department.getPhone());
        p.setAddress(department.getAddress() == null ? "" : department.getAddress());
        p.setEmail(department.getEmail() == null ? "" : department.getEmail());

        p.setDirector(department.getDirector() == null ? "" : String.format(
            "%s %s %s",
            department.getDirector().getLastName() == null ? "" : department.getDirector().getLastName(),
            department.getDirector().getFirstName() == null ? "" : department.getDirector().getFirstName(),
            department.getDirector().getMiddleName() == null ? "" : department.getDirector().getMiddleName())
            .trim());

        return p;
    }

    /**
     * Маппит клиента. Выбирает какой маппер использовать для маппинга нужного типа клиента.
     *
     * @param client
     * @return
     */
    public PrintClient toPrintClient(Client client) {
        if (client instanceof PhysicalClient)
            return toPrintPhysicalClient((PhysicalClient) client);
        else if (client instanceof JuridicalClient)
            return toPrintJuridicalClient((JuridicalClient) client);
        else
            return null;
    }

    /**
     * Маппит физическое лицо.
     *
     * @param client физическое лицо.
     * @return печатаемое физическое лицо.
     */
    private PrintPhysicalClient toPrintPhysicalClient(PhysicalClient client) {
        client = physicalClientService.getOneFull(client.getId());

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());
        DecimalFormat moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);

        PrintPhysicalClient p = new PrintPhysicalClient();

        p.setFirstName(client.getFirstName() == null ? "" : client.getFirstName());
        p.setLastName(client.getLastName() == null ? "" : client.getLastName());
        p.setMiddleName(client.getMiddleName() == null ? "" : client.getMiddleName());
        p.setInitName(String.format("%s %s %s",
            client.getLastName(),
            client.getFirstName() == null ? "" : client.getFirstName().substring(0, 1).concat("."),
            client.getMiddleName() == null ? "" : client.getMiddleName().substring(0, 1).concat("."))
            .replaceAll("( +)", " "));
        p.setFullName(String.format("%s %s %s", client.getLastName(), client.getFirstName(), client.getMiddleName())
            .replace("null", "")
            .replaceAll("( +)", " "));
        p.setBirthDate(dateTimeFormatter.format(client.getBirthDate()));
        p.setGender(i18n.get(client.getGender().getName()));

        PhysicalClientDetails details = client.getDetails();
        p.setBirthPlace(details.getBirthPlace() == null ? "" : details.getBirthPlace());
        p.setMaritalStatus(details.getMaritalStatus() == null ? "" : details.getMaritalStatus().getName());
        p.setKids(details.getKids() == null ? "" : details.getKids().toString());
        p.setYearIncome(details.getYearIncome() == null ? "" : moneyFormat.format(details.getYearIncome()));
        p.setMobilePhone(details.getMobilePhone() == null ? "" : details.getMobilePhone());
        p.setHomePhone(details.getHomePhone() == null ? "" : details.getHomePhone());
        p.setWorkPhone(details.getWorkPhone() == null ? "" : details.getWorkPhone());
        p.setEmail(details.getEmail() == null ? "" : details.getEmail());
        p.setSsnNumber(details.getSsnNumber() == null ? "" : details.getSsnNumber());
        p.setTaxNumber(details.getTaxNumber() == null ? "" : details.getTaxNumber());
        p.setYearIncome(
            details.getYearIncome() == null ? "0.00" : moneyFormat.format(client.getDetails().getYearIncome()));

        //документы
        if (details.getDocuments().size() > 0) {
            //паспорт
            p.setPassport(getDocument(details, passportName, documentMask));
            //права
            p.setDriverLicense(getDocument(details, driverLicenseName, documentMask));
            //права
            p.setUdostoverenieL(getDocument(details, udostoverenieLName, documentMask));
        } else {
            p.setPassport("");
            p.setDriverLicense("");
        }

        //адрес регистрации
        if (details.getAddresses().size() > 0) {
            //адрес регистрации
            p.setAddressReg(getAddress(details, addressRegStatusMask, addressMask));
            //адрес проживания
            p.setAddressLive(getAddress(details, addressLiveStatusMask, addressMask));
        } else {
            p.setAddressReg("");
            p.setAddressLive("");
        }

        //лучшее образование
        if (details.getEducations().size() > 0) {
            p.setBestEduLevel(getBestEduLevel(details));
        } else {
            p.setBestEduLevel("");
        }

        //последнее место работы
        if (details.getJobs().size() > 0) {
            p.setLastJob(getLastJob(details, lastJobMask, dateTimeFormatter));
        } else {
            p.setLastJob("");
        }

        //родственники
        if (details.getRelatives().size() > 0) {
            p.setRelatives(getRelatives(details));
        } else {
            p.setRelatives("");
        }

        //заметки
        if (details.getNotes().size() > 0) {
            p.setNotes(getNotes(details));
        } else {
            p.setNotes("");
        }

        return p;
    }

    /**
     * Маппит юридическое лицо.
     *
     * @param client юридическое лицо.
     * @return печатаемое юридическое лицо.
     */
    private PrintJuridicalClient toPrintJuridicalClient(JuridicalClient client) {
        return null;
    }

    /**
     * Формирует строку документа в зависимости от маски (паспорт или вод. удостоверение)
     *
     * @param details      - дополнительные сведения о клиенте
     * @param documentName - маска, какой документ искать
     * @param documentMask - маска формирует вид выходной строки
     * @return строка документа
     */
    private String getDocument(PhysicalClientDetails details, String documentName, String documentMask) {
        Document document = details.getDocuments().stream()
            .filter(e -> e.getStatus() == DocumentStatus.ACTUAL)
            .filter(e -> e.getDocumentType().getName().contains(documentName)).findFirst().orElse(null);

        if (document != null) {
            return String.format(documentMask, document.getSeries(),
                document.getNumber(), document.getIssueDate(), document.getIssuePlace())
                .replace("null", "").replaceAll("( +)", " ");
        } else {
            return "";
        }
    }

    /**
     * Формирует строку адреса в зависимости от маски (адрес регистрации или адрес проживания)
     *
     * @param details           - дополнительные сведения о клиенте
     * @param addressStatusMask - какой тип адреса искать (адрес регистрации или адрес проживания)
     * @param addressMask       - маска формирует вид выходной строки
     * @return строка адреса
     */
    private String getAddress(PhysicalClientDetails details, String addressStatusMask, String addressMask) {
        Address addressReg = details.getAddresses().stream()
            .filter(e -> e.getStatus().getName().contains(addressStatusMask))
            .findFirst().orElse(null);

        if (addressReg != null) {
            return String.format(addressMask,
                addressReg.getZip(), addressReg.getStreet(), addressReg.getHouse(), addressReg.getBlock(),
                addressReg.getAppartment(), addressReg.getCity())
                .replace("null", "")
                .replaceAll("((, )+)", ", ")
                .replaceAll("( +)", " ")
                .replaceAll("^,", "")
                .trim()
                .replaceAll(",$", "");
        } else {
            return "";
        }
    }

    /**
     * Формирует строку лучшего образования клиента, выбирает по уровню образования
     *
     * @param details - дополнительные сведения о клиенте
     * @return строка с уровнем образования
     */
    private String getBestEduLevel(PhysicalClientDetails details) {
        Comparator<Education> comp = Comparator.comparingInt(p1 -> p1.getLevel().getLevel());
        Education education = details.getEducations().stream()
            .max(comp)
            .orElse(null);

        if (education == null || education.getLevel() == null) {
            return "";
        } else return education.getLevel().getName();
    }

    /**
     * Формирует строку последней работы клиента
     * поиск выполняется по последней даты трудоустройства на работу.
     * наличие работ с нулевой датой игнорируется при наличии работ с датой
     * если нет работ с датами трудоустройства, то выбор последней работы произвольный
     *
     * @param details           - дополнительные сведения о клиенте
     * @param lastJobMask       - маска выходной строки
     * @param dateTimeFormatter - форматтер даты
     * @return - строка с последней работы
     */
    private String getLastJob(PhysicalClientDetails details, String lastJobMask, DateTimeFormatter dateTimeFormatter) {
        Comparator<Job> comp =
            Comparator.comparing(Job::getEmploymentDate, Comparator.nullsFirst(LocalDate::compareTo)).reversed();

        Optional<Job> job = details.getJobs().stream()
            .sorted(comp)
            .findFirst();

        if (job.isPresent()) {
            return String.format(lastJobMask, job.get().getOccupation(),
                job.get().getOrganization(), job.get().getReceptionPhone(),
                job.get().getEmploymentDate() == null ? "" : dateTimeFormatter.format(job.get().getEmploymentDate()))
                .replace("null", "")
                .replaceAll("((, )+)", ", ")
                .replaceAll("( +)", " ")
                .replaceAll("^,", "")
                .trim()
                .replaceAll(",$", "");
        } else return "";
    }

    /**
     * Формирует список родственников
     *
     * @param details - дополнительные сведения о клиенте
     * @return - строка со списком родственников
     */
    private String getRelatives(PhysicalClientDetails details) {
        StringBuilder rel = new StringBuilder();
        for (Relative relative : details.getRelatives()) {
            rel.append(String.format("%s, %s, %s\n",
                relative.getRelativeType() == null ? "" : relative.getRelativeType().getName(), relative.getName(),
                relative.getContacts()));
        }
        return rel.toString()
            .replace("null", "")
            .replaceAll("((, )+)", ", ")
            .replaceAll("( +)", " ")
            .replaceAll("^,", "")
            .trim()
            .replaceAll(",$", "");
    }

    /**
     * Формирует список заметок
     *
     * @param details - дополнительные сведения о клиенте
     * @return - строка со списком заметок
     */
    private String getNotes(PhysicalClientDetails details) {
        StringBuilder no = new StringBuilder();
        for (Note note : details.getNotes()) {
            no.append(String.format("%s,\n",
                note.getContent()));
        }
        return no.toString()
            .trim()
            .replaceAll(",$", "");
    }


}

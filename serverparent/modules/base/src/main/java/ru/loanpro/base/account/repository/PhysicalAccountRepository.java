package ru.loanpro.base.account.repository;

import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.account.domain.PhysicalAccount;

import java.util.List;

/**
 * Репозиторий сущности {@link PhysicalAccount}.
 *
 * @author Maksim Askhaev
 */
public interface PhysicalAccountRepository extends BaseAccountRepository<PhysicalAccount> {
    @Query("select a from PhysicalAccount a where a.clientDetails = ?1 order by a.number")
    @Override
    List<PhysicalAccount> findAllByEntity(Object entity);
}

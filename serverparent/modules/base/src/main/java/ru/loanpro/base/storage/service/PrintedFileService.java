package ru.loanpro.base.storage.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.base.storage.domain.connector.ClientPrintedFile;
import ru.loanpro.base.storage.domain.connector.LoanPrintedFile;
import ru.loanpro.base.storage.domain.connector.OperationPrintedFile;
import ru.loanpro.base.storage.domain.connector.PaymentPrintedFile;
import ru.loanpro.base.storage.repository.PrintedFileRepository;

/**
 * Сервис распечатанных документов.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class PrintedFileService extends BaseStorageFileService<PrintedFile> {

    private final PrintedFileRepository fileRepository;

    public PrintedFileService(PrintedFileRepository fileRepository) {
        super(fileRepository);
        this.fileRepository = fileRepository;
    }

    /**
     * Возвращает список документов, распечатанных по клиенту.
     *
     * @param client клиент.
     * @return список распечатанных документов.
     */
    public List<ClientPrintedFile> findClientPrintedFiles(Client client) {
        return fileRepository.findAllClientPrintedFiles(client);
    }

    /**
     * Возвращает список документов, распечатанных по займу.
     *
     * @param loan займ.
     * @return список распечатанных документов.
     */
    public List<LoanPrintedFile> findLoanPrintedFiles(Loan loan) {
        return fileRepository.findAllLoanPrintedFiles(loan);
    }

    /**
     * Возвращает список документов, распечатанных по платежу
     *
     * @param payment займ.
     * @return список распечатанных документов.
     */
    public List<PaymentPrintedFile> findPaymentPrintedFiles(Payment payment) {
        return fileRepository.findAllPaymentPrintedFiles(payment);
    }

    /**
     * Возвращает список документов, распечатанных по операции
     *
     * @param operation операция.
     * @return список распечатанных документов.
     */
    public List<OperationPrintedFile> findOperationPrintedFiles(Operation operation) {
        return fileRepository.findAllOperationPrintedFiles(operation);
    }
}

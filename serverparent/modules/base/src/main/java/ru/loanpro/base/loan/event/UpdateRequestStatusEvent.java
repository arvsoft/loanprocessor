package ru.loanpro.base.loan.event;

import ru.loanpro.base.loan.domain.Request;

/**
 * Класс события изменения статуса заявки.
 *
 * @author Maksim Askhaev
 */
public class UpdateRequestStatusEvent {
    private Request request;

    public UpdateRequestStatusEvent(Request request){
        this.request = request;
    }

    public Request getRequest(){
        return request;
    }
}

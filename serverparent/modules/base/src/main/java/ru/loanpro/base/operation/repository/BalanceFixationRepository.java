package ru.loanpro.base.operation.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.base.operation.domain.BalanceFixation;
import ru.loanpro.security.domain.Department;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

public interface BalanceFixationRepository extends JpaRepository<BalanceFixation, UUID> {

    /**
     * Получаем дату последней фиксации баланса по данному отделу и валюте
     *
     * @param department - отдел
     * @param currency   - валюта
     * @return - дата последней фиксации баланса
     */
    @Query("select max(b.fixationDate) from BalanceFixation b where b.department = ?1 and b.currency = ?2")
    LocalDate getLastFixDate(Department department, Currency currency);

    /**
     * Получаем дату предпоследней фиксации баланса по данному отделу и валюте
     *
     * @param department - отдел
     * @param currency   - валюта
     * @return - дата последней фиксации баланса
     */
    @Query("select max(b.fixationDate) from BalanceFixation b where b.department = ?1 and b.currency = ?2 and b.fixationDate < ?3")
    LocalDate getPreLastFixDate(Department department, Currency currency, LocalDate lastFixDate);

    /**
     * Получаем список фиксаций баланса
     *
     * @param department - отдел
     * @return список
     */
    @Query("select b from BalanceFixation b where b.department = ?1 order by b.fixationDate desc")
    List<BalanceFixation> findAllBalanceFixationsByDepartment(Department department);

    @Query("select b from BalanceFixation b where b.department = ?1 and b.currency = ?2 and b.fixationDate = ?3")
    BalanceFixation findOneByFixationDate(Department department, Currency currency, LocalDate date);

    /**
     * Метод получения отсортированного постраничного списка фиксаций баланса по кассе
     *
     * @param department - отдел
     * @param pageable параметры сортировки и постраничной группировки результатов.
     *
     * @return список клентов.
     */
    @Query("select b from BalanceFixation b where b.department = ?1 order by b.fixationDate desc")
    List<BalanceFixation> findAllBalanceFixationsByDepartment(Department department, Pageable pageable);

    /**
     * Метод получения количества фиксаций баланса, соответствующих отделу.
     *
     * @param department - отдел
     *
     * @return количество клиентов соответствующих фильтру.
     */
    @Query("select count(b) from BalanceFixation b where b.department = ?1")
    int countFindAllBalanceFixationsByDepartment(Department department);
}

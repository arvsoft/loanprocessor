package ru.loanpro.base.printer;

/**
 * Коллекция печатаемых типов документов.
 *
 * @author Oleg Brizhevatikh
 */
public enum PrintDocumentType {
    PHYSICAL_CLIENT_PROFILE("printDocumentType.physicalClient.profile", "physical_client_profile.odt"),
    LOAN_AGREEMENT("printDocumentType.agreement", "loan_agreement.odt"),
    LOAN_PAYMENT("printDocumentType.payment", "payment_receipt.odt"),
    OPERATION("printDocumentType.operation", "operation_receipt.odt");

    private String templateName;
    private String fileName;

    /**
     * Создаёт элемент перечисления типов печатаемых документов.
     *
     * @param templateName название шаблона.
     * @param fileName     название файла шаблона по-умолчанию.
     */
    PrintDocumentType(String templateName, String fileName) {
        this.templateName = templateName;
        this.fileName = fileName;
    }

    /**
     * Возвращает тип шаблона по имени файла по-умолчанию.
     *
     * @param fileName имя файла.
     * @return тип шаблона.
     */
    public static PrintDocumentType getByFileName(String fileName) {
        for (PrintDocumentType documentType : PrintDocumentType.values()) {
            if (documentType.getFileName().equals(fileName))
                return documentType;
        }

        return null;
    }

    /**
     * Название шаблона.
     *
     * @return название шаблона.
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Возвращает имя файла шаблона по-умолчанию.
     *
     * @return имя файла.
     */
    public String getFileName() {
        return fileName;
    }
}

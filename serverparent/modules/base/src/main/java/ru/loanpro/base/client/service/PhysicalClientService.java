package ru.loanpro.base.client.service;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.vaadin.data.provider.QuerySortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.client.domain.PhysicalClientQueryDSL;
import ru.loanpro.base.client.domain.QPhysicalClient;
import ru.loanpro.base.client.repository.DetailsItemRepository;
import ru.loanpro.base.client.repository.PhysicalClientRepository;
import ru.loanpro.base.loan.domain.QLoan;
import ru.loanpro.base.loan.domain.QRequest;
import ru.loanpro.base.loan.service.LoanService;
import ru.loanpro.base.loan.service.RequestService;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.security.domain.Department;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.sequence.service.SequenceService;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Сервис работы с сущностью {@link PhysicalClient}.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Service
public class PhysicalClientService extends AbstractClientService<PhysicalClient, PhysicalClientRepository> {

    private final RequestService requestService;
    private final SequenceService sequenceService;
    private final LoanService loanService;
    private final DetailsItemRepository detailsItemRepository;
    private final ApplicationEventBus applicationEventBus;

    @Autowired
    public PhysicalClientService(PhysicalClientRepository repository, DetailsItemRepository detailsItemRepository,
                                 ApplicationEventBus applicationEventBus, @Lazy RequestService requestService,
                                 @Lazy LoanService loanService,
                                 SequenceService sequenceService) {
        super(repository);

        this.detailsItemRepository = detailsItemRepository;
        this.applicationEventBus = applicationEventBus;
        this.requestService = requestService;
        this.sequenceService = sequenceService;
        this.loanService = loanService;
    }

    /**
     * Возвращает физическое лицо по его идентификатору и производит инициализацию
     * поля <code>details</code> типа {@link PhysicalClientDetails}.
     *
     * @param id идентификатор сущности физического лица.
     * @return физическое лицо.
     */
    @Transactional
    @Override
    public PhysicalClient getOneFull(UUID id) {
        return super.getOneFull(id);
    }

    /**
     * Возвращает физическоге лицо по его {@link PhysicalClientDetails}
     *
     * @param details детали аккаунта клиента.
     * @return физическое лицо.
     */
    public PhysicalClient findOneByPhysicalClientDetails(PhysicalClientDetails details) {
        return repository.findOneByDetails(details);
    }

    /**
     * Сохраняет физическое лицо, предварительно устанавливая в во все зависимые от
     * него поля, ссылку на поле <code>details</code>.
     *
     * @param client сохраняемое физическое лицо.
     * @return сохранённое физическое лицо.
     */
    @Transactional
    @Override
    public synchronized PhysicalClient save(PhysicalClient client, Department department) {
        PhysicalClientDetails details = client.getDetails();

        details.getDocuments().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });
        details.getAddresses().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });

        details.getEducations().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });
        details.getJobs().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });
        details.getEstates().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });
        details.getVehicles().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });
        details.getRelatives().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });
        details.getNotes().forEach(item -> {
            item.setClientDetails(details);
            detailsItemRepository.save(item);
        });

        details.getAccounts().forEach(item -> {
            try {
                if (item.getClientDetails() == null) {
                    Sequence sequence = department.getAccountSequence();
                    String number = sequenceService.actualizeNextNumber(sequence);
                    item.setNumber(number);
                    item.setClientDetails(details);
                    detailsItemRepository.save(item);
                }
                // Если всё сохранилось корректно - оповещаем систему об обновлении последовательности клиентского счета
                applicationEventBus.post(new UpdateSequenceEvent(department.getAccountSequence()));
            } catch (Exception e) {
                // При любом исключении из бд - откатываем назад номер и бросаем исключение.
                sequenceService.resetLastNumber(department.getAccountSequence());
            }
        });

        return super.save(client);
    }

    /**
     * {@inheritDoc}
     * Отфильтровываются все удалённые физические лица.
     */
    @Override
    public Stream<PhysicalClient> findAll(Predicate predicate, List<QuerySortOrder> sortOrder, int offset, int limit) {
        return super.findAll(
            QPhysicalClient.physicalClient.deleted.isFalse().and(predicate), sortOrder, offset, limit);
    }

    /**
     * {@inheritDoc}
     * Отфильтровываются все удалённые физические лица.
     */
    @Override
    public Integer count(Predicate predicate) {
        return super.count(QPhysicalClient.physicalClient.deleted.isFalse().and(predicate));
    }

    /**
     * Возвращает количество физических лиц соответствующих фильтру поиска.
     * Отфильтровываются все удалённые физические лица.
     *
     * @param filter фильтр.
     * @return количество физических лиц.
     */
    @Override
    public Integer count(String filter) {
        return count(QPhysicalClient.physicalClient.deleted.isFalse().and(PhysicalClientQueryDSL.quickSearch(filter)));
    }

    /**
     * Возвращает стрим физических лиц соответствующих фильтру, разбитый на страницы.
     * Отфильтровываются все удалённые физические лица.
     *
     * @param filter фильтр клиента.
     * @param offset смещение.
     * @param limit  требуемое количество клиентов.
     * @return стрим физических лиц.
     */
    @Override
    public Stream<PhysicalClient> findAll(String filter, int offset, int limit) {
        BooleanExpression predicate = QPhysicalClient.physicalClient.deleted.isFalse()
            .and(PhysicalClientQueryDSL.quickSearch(filter));
        return findAll(predicate, Collections.emptyList(), offset, limit);
    }

    /**
     * {@inheritDoc}
     * Удалять можно при условии, что у клиента нет неудалённых займов и заявок.
     */
    @Transactional
    @Override
    public Boolean canBeDeleted(PhysicalClient client) {
        QLoan loan = QLoan.loan;
        QRequest request = QRequest.request;

        return client.getId() != null
            && loanService.count(loan.borrower.eq(client).and(loan.deleted.isFalse())) == 0
            && requestService.count(request.borrower.eq(client).and(request.deleted.isFalse())) == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    public void delete(PhysicalClient client) throws EntityDeleteException {
        QLoan loan = QLoan.loan;
        if (loanService.count(loan.borrower.eq(client).and(loan.deleted.isFalse())) != 0)
            throw new EntityDeleteException("deleting.exception.client.foundLoans");

        QRequest request = QRequest.request;
        if (requestService.count(request.borrower.eq(client).and(request.deleted.isFalse())) != 0)
            throw new EntityDeleteException("deleting.exception.client.foundRequests");

        client.setDeleted(true);
        repository.save(client);
    }
}

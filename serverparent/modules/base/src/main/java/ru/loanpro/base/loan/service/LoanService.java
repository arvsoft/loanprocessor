package ru.loanpro.base.loan.service;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.vaadin.data.provider.QuerySortOrder;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.exceptions.AccountNotFoundException;
import ru.loanpro.base.account.exceptions.InsufficientAccountBalanceException;
import ru.loanpro.base.account.service.DepartmentAccountService;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.service.JuridicalClientService;
import ru.loanpro.base.client.service.PhysicalClientService;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.domain.QLoan;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.loan.event.UpdateRequestStatusEvent;
import ru.loanpro.base.loan.exceptions.LoanNotSavedException;
import ru.loanpro.base.loan.exceptions.ObjectNotSavedException;
import ru.loanpro.base.loan.repository.LoanRepository;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.operation.domain.QOperation;
import ru.loanpro.base.operation.event.UpdateOperationEvent;
import ru.loanpro.base.operation.exceptions.OperationNotSavedException;
import ru.loanpro.base.operation.service.OperationService;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.payment.domain.QPayment;
import ru.loanpro.base.payment.service.PaymentService;
import ru.loanpro.base.requesthistory.domain.RequestHistory;
import ru.loanpro.base.requesthistory.serivce.RequestHistoryService;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.global.directory.PaymentMethod;
import ru.loanpro.global.directory.RequestStatus;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.sequence.service.SequenceService;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Сервис работы с займом/ми
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Service
public class LoanService extends BaseLoanService<Loan, LoanRepository> {

    private final SequenceService sequenceService;
    private final ApplicationEventBus applicationEventBus;
    private final RequestHistoryService requestHistoryService;
    private final RequestService requestService;
    private final DepartmentAccountService departmentAccountService;
    private final OperationService operationService;
    private final PhysicalClientService physicalClientService;
    private final JuridicalClientService juridicalClientService;
    private final PaymentService paymentService;
    private final I18N i18n;

    @Autowired
    public LoanService(LoanRepository repository,
                       SequenceService sequenceService,
                       ApplicationEventBus applicationEventBus,
                       RequestHistoryService requestHistoryService,
                       RequestService requestService,
                       DepartmentAccountService departmentAccountService,
                       OperationService operationService,
                       PhysicalClientService physicalClientService,
                       JuridicalClientService juridicalClientService,
                       @Lazy PaymentService paymentService,
                       I18N i18n) {

        super(repository);
        this.sequenceService = sequenceService;
        this.applicationEventBus = applicationEventBus;
        this.requestHistoryService = requestHistoryService;
        this.requestService = requestService;
        this.departmentAccountService = departmentAccountService;
        this.operationService = operationService;
        this.physicalClientService = physicalClientService;
        this.juridicalClientService = juridicalClientService;
        this.paymentService = paymentService;
        this.i18n = i18n;
    }

    /**
     * Возвращает один займ по его идентификатору. Инициализация Lazy-полей
     * осуществляется частично в репозитории и частично в методе.
     * Дополнительно сортирует коллекцию рассчитанного графика платежей по порядковому номеру
     *
     * @param id идентификатор сущности.
     * @return сущность типа <code>T</code>.
     */
    @Transactional
    @Override
    public Loan getOneFull(UUID id) {
        Loan one = super.getOneFull(id);
        Hibernate.initialize(one.getSchedules());
        Hibernate.initialize(one.getPayments());

        class ScheduleComparator implements Comparator<Schedule> {
            @Override
            public int compare(Schedule schedule, Schedule t1) {
                return schedule.getOrdNo() - t1.getOrdNo();
            }
        }
        ScheduleComparator scheduleComparator = new ScheduleComparator();

        if (one.getSchedules() != null && one.getSchedules().size() > 0) {
            one.getSchedules().sort(scheduleComparator);
        }

        class PaymentComparator implements Comparator<Payment> {
            @Override
            public int compare(Payment payment, Payment t1) {
                return payment.getOrdNo() - t1.getOrdNo();
            }
        }
        PaymentComparator paymentComparator = new PaymentComparator();

        if (one.getPayments() != null && one.getPayments().size() > 0) {
            one.getPayments().sort(paymentComparator);
        }

        return one;
    }

    @Override
    public Stream<Loan> findAll(Predicate predicate, List<QuerySortOrder> sortOrder, int offset, int limit) {
        QLoan loan = QLoan.loan;
        BooleanExpression expression = loan.deleted.isFalse()
            .and(loan.isProlonged.isFalse())
            .and(predicate);
        return super.findAll(expression, sortOrder, offset, limit);
    }

    @Override
    public Integer count(Predicate predicate) {
        QLoan loan = QLoan.loan;
        BooleanExpression expression = loan.deleted.isFalse()
            .and(loan.isProlonged.isFalse())
            .and(predicate);
        return super.count(expression);
    }

    /**
     * Сохраняет заем.
     *
     * @param loan заем
     */
    @Transactional
    public Loan save(Loan loan) {
        return repository.save(loan);
    }

    /**
     * {@inheritDoc}
     * Можно удалять при условии, что займ не пролонгирован и не имеет платежей и операций.
     */
    @Transactional
    @Override
    public boolean canBeDeleted(Loan entity) {
        QOperation operation = QOperation.operation;
        QPayment payment = QPayment.payment1;

        return entity.getId() != null
            && !entity.isProlonged()
            && operationService.count(operation.loan.eq(entity)) == 0
            && paymentService.count(payment.loan.eq(entity)) == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public void delete(Loan entity) throws EntityDeleteException {
        Optional<Loan> optionalLoan = repository.findById(entity.getId());
        if (optionalLoan.isPresent() && optionalLoan.get().isProlonged())
            throw new EntityDeleteException("deleting.exception.loan.loanIsProlonged");

        QOperation operation = QOperation.operation;
        if (operationService.count(operation.loan.eq(entity)) != 0)
            throw new EntityDeleteException("deleting.exception.loan.foundOperations");

        QPayment payment = QPayment.payment1;
        if (paymentService.count(payment.loan.eq(entity)) != 0)
            throw new EntityDeleteException("deleting.exception.loan.foundPayments");

        // Убираем отметку о пролонгации у предка данного займа.
        repository.findByChild(entity).ifPresent(parent -> {
            parent.setIsProlonged(false);
            repository.save(parent);
        });

        entity.setDeleted(true);
        repository.save(entity);
    }

    /**
     * Сохраняет переданную коллекцию займов.
     *
     * @param loans коллекция займов.
     */
    @Transactional
    public void saveAll(Collection<Loan> loans) {
        repository.saveAll(loans);
    }

    /**
     * Сохраняет заем. Если займ в БД отсутствует, создаёт новый заем.
     *
     * @param loan                      заем
     * @param loanDraftNumber           временный номер займа
     * @return сохранённый заем.
     * @throws ObjectNotSavedException  если заем не сохранён
     */
    @Transactional
    public synchronized Loan save(Loan loan, String loanDraftNumber) throws ObjectNotSavedException {
        if (loan.getPayments().size() == 0) {
            loan.setCurrentBalance(loan.getLoanBalance());
        }

        if (loan.getId() != null) {
            return super.save(loan);
        } else
            return saveNewLoan(loan, loanDraftNumber);
    }

    private Loan saveNewLoan(Loan loan, String loanDraftNumber) throws LoanNotSavedException {

        // Если займ оформляется по заявке, меняем статус заявки на 'выдан заем' и
        // записываем в историю заявки информацию о выдаче займа.
        Request request = loan.getRequest();
        if (request != null) {
            RequestHistory historyItem = new RequestHistory();
            historyItem.setRequestStatus(RequestStatus.LOAN_ISSUED);
            historyItem.setDecisionDate(loan.getLoanIssueDate());
            historyItem.setUser(loan.getUser());
            historyItem.setRequest(request);
            historyItem.setLoan(loan);
            requestHistoryService.save(historyItem);

            request.setRequestStatus(RequestStatus.LOAN_ISSUED);
            requestService.save(request);
            applicationEventBus.post(new UpdateRequestStatusEvent(request));
        }

        try {
            if (loan.getLoanNumber().equals(loanDraftNumber)) {
                // Номер займа совпадает с выданным временным номером, значит пользователь не изменял номер
                String number = sequenceService.actualizeNextNumber(loan.getDepartment().getLoanSequence());
                loan.setLoanNumber(number);
            }

            Loan result = super.save(loan);

            // Если всё сохранилось корректно - оповещаем
            applicationEventBus.post(new UpdateSequenceEvent(loan.getDepartment().getLoanSequence()));
            return result;
        } catch (Exception e) {
            sequenceService.resetLastNumber(loan.getDepartment().getLoanSequence());
            throw new LoanNotSavedException(e);
        }
    }

    /**
     *
     * @param loan
     * @param autoOperationIssueMoneyLoanIssued
     * @param autoOperationIssueMoneyLoanIssuedProcessed
     * @param instant
     * @param user
     * @param department
     * @throws AccountNotFoundException
     * @throws InsufficientAccountBalanceException
     * @throws OperationNotSavedException
     */
    @Transactional
    public synchronized void saveOperations(
        Loan loan,
        boolean autoOperationIssueMoneyLoanIssued,
        boolean autoOperationIssueMoneyLoanIssuedProcessed,
        Instant instant,
        User user, Department department)
        throws AccountNotFoundException, InsufficientAccountBalanceException, OperationNotSavedException {
        try {

            if (autoOperationIssueMoneyLoanIssued) {
                //формируем операцию выдачи наличных из кассы (тип "в обработке")
                Operation operation = new Operation();
                operation.setDepartment(department);
                operation.setLoan(loan);
                operation.setPaymentMethod(PaymentMethod.CASH_ISSUE);
                operation.setOperationValue(loan.getLoanBalance());
                operation.setCurrency(loan.getCurrency());
                operation.setUser(user);

                Optional<DepartmentAccount> accountDepartment = departmentAccountService.findAllByDepartment(
                    department).stream()
                    .filter(item ->
                            item.getCurrency() == loan.getCurrency() &&
                            item.isCashAccount())
                    .findFirst();
                if (!accountDepartment.isPresent()) throw new AccountNotFoundException(
                    i18n.get("exception.message.departmentCashAccountNotFound"));
                accountDepartment.ifPresent(operation::setAccountPayer);

                if (loan.getBorrower().getClass() == PhysicalClient.class) {
                    PhysicalClient client = physicalClientService.getOneFull(loan.getBorrower().getId());
                    operation.setRecipient(client);
                } else {
                }

                if (autoOperationIssueMoneyLoanIssuedProcessed) {
                    operationService.save(operation, instant);
                } else operationService.savePrepared(operation, instant);

                applicationEventBus.post(new UpdateOperationEvent(operation));
            }
        // При любом исключении из бд - откатываем назад номер и бросаем исключение.
        } catch (InsufficientAccountBalanceException e) {
            throw new InsufficientAccountBalanceException(e.getLocalizedMessage());
        } catch (OperationNotSavedException e) {
            throw new OperationNotSavedException(e.getLocalizedMessage());
        } catch (AccountNotFoundException e) {
            throw new AccountNotFoundException(e.getLocalizedMessage());
        }
    }

    @Transactional
    public synchronized Loan saveProlongedLoan(Loan loanProlonged, Loan loan, String prolongDraftNumber) throws
        LoanNotSavedException {
        try {
            if (loan.getProlongAgrNumber().equals(prolongDraftNumber)) {
                // Номер пролонгации совпадает с выданным временным номером, значит пользователь не изменял номер
                String number = sequenceService.actualizeNextNumber(loan.getDepartment().getProlongSequence());
                loan.setProlongAgrNumber(number);
            }
            Loan result = super.save(loan);

            repository.setIsProlongedToTrueById(loanProlonged.getId());

            // Если всё сохранилось корректно - оповещаем
            applicationEventBus.post(new UpdateSequenceEvent(loanProlonged.getDepartment().getProlongSequence()));

            return result;

        } catch (Exception e) {
            loanProlonged.setIsProlonged(false);
            sequenceService.resetLastNumber(loan.getDepartment().getProlongSequence());
            throw new LoanNotSavedException(e);
        }
    }

    public List<Loan> getProlongedLoans(Loan loan) {
        List<Loan> loanList = new ArrayList<>();
        loanList.add(loan);
        loadParentLoan(loan, loanList);

        return loanList;
    }

    private void loadParentLoan(Loan loan, List<Loan> loanList) {
        repository.findByChild(loan).ifPresent(parent -> {
            loanList.add(parent);
            loadParentLoan(parent, loanList);
        });
    }
}

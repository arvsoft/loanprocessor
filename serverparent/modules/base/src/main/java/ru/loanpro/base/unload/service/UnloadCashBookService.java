package ru.loanpro.base.unload.service;

import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.operation.domain.BalanceFixation;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.printer.International.NumberToTextConvertor_EN;
import ru.loanpro.base.printer.International.NumberToTextConvertor_RU;
import ru.loanpro.base.unload.repository.UnloadCashBookRepository;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.directory.PaymentMethod;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.List;

@Service
public class UnloadCashBookService {

    @Value("${system.directory.unload}")
    private String unloadPath;

    private final UnloadCashBookRepository unloadCashBookRepository;
    private final I18N i18n;

    private DateTimeFormatter dateFormatter;
    private DecimalFormat moneyFormat;
    private SystemLocale systemLocale;

    @Autowired
    public UnloadCashBookService(UnloadCashBookRepository unloadCashBookRepository, I18N i18n) {
        this.unloadCashBookRepository = unloadCashBookRepository;
        this.i18n = i18n;
    }

    public String getFileInPath(DateTimeFormatter dateFormatter, Department department, LocalDate date){
        return String.format(
            "%s/%s%s%s%s%s%s",
            unloadPath,
            i18n.get("unloadCashBook.cashbook"),
            "_",
            department.getName(),
            "_",
            dateFormatter.format(date),
            ".xlsx");
    }

    public String createUnload(ConfigurationService configurationService,
                             ChronometerService chronometerService,
                             Department department,
                             BalanceFixation balanceFixation) {
        dateFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());
        moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        systemLocale = configurationService.getSystemLocale();

        LocalDate fixationDate = balanceFixation.getFixationDate();
        Instant dateMin = fixationDate.atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
        Instant dateMax = fixationDate.plusDays(1).atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
        Currency currency = balanceFixation.getCurrency();

        //получаем данные для таблицы
        List<Operation> operations = unloadCashBookRepository.getOperationsForDepartment(dateMin, dateMax, department, currency);

        long numberIncomeDocuments = operations.stream().filter(item -> item.getPaymentMethod() == PaymentMethod.CASH_REPLENISH).count();
        long numberExpenseDocuments = operations.stream().filter(item -> item.getPaymentMethod() == PaymentMethod.CASH_ISSUE).count();

        BigDecimal totalIncome = operations.stream().filter(item -> item.getPaymentMethod() == PaymentMethod.CASH_REPLENISH)
            .map(Operation::getOperationValue)
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal totalExpense = operations.stream().filter(item -> item.getPaymentMethod() == PaymentMethod.CASH_ISSUE)
            .map(Operation::getOperationValue)
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        int numberDataRecords = operations.size();//кол-во записей данных

        int titleRowStartIndex = 1;//абсолютный индекс строки верхней части заголовка документа
        int titleColumnStartIndex = 2;//абсолютный индекс колонки левой части заголовка документа
        int tableRowStartIndex = 4;//абсолютный индекс строки верхней части таблицы (включая заголовок таблицы)
        int tableColumnStartIndex = 2;//абсолютный индекс колонки левой части таблицы
        int tableColumnOffset = 7;//сдвиг в количестве колонок для создания правой страницы
        int maxNumberRowsPerPage = 24;//кол-во записей в таблице
        int numberFooterRows = 9;//футер (подписи, кол-во док-тов)
        int numberInfoRows = 4;//остаток на начало и конец дня, итого
        int numberSheets;//кол-во листов
        int numberPages;//кол-во страниц (на каждом листе по две страницы)
        boolean footer = false;//выводился ли футер
        boolean dayInfo = false;//выводилась ли конечная информация

        int currentOperationIndex = 0;//текущий индекс операции

        if (numberDataRecords > 0) {
            //считаем кол-во листов и страниц
            int sum = numberDataRecords + numberFooterRows + numberInfoRows;
            int temp = sum / maxNumberRowsPerPage;
            numberPages = sum % maxNumberRowsPerPage == 0 ? temp : temp + 1;
            numberSheets = numberPages % 2 == 0 ? numberPages / 2 : (numberPages / 2) + 1;

            XSSFWorkbook workbook = new XSSFWorkbook();

            int pageIndex = 1;

            for (int sheetIndex = 0; sheetIndex < numberSheets; sheetIndex++) {
                XSSFSheet sheet = workbook.createSheet(String.format("%s", sheetIndex + 1));

                sheet.getPrintSetup().setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
                sheet.getPrintSetup().setLandscape(true);
                sheet.getPrintSetup().setFitHeight((short) 1);
                sheet.getPrintSetup().setFitWidth((short) 1);
                sheet.setFitToPage(true);
                workbook.setPrintArea(sheetIndex, 0, 14, 0, 31);

                initColumnParameters(sheet, tableColumnStartIndex);

                createTitle(workbook, sheet, sheetIndex + 1, titleRowStartIndex, titleColumnStartIndex, fixationDate, department);
                createTableCaption(workbook, sheet, tableRowStartIndex, tableColumnStartIndex);
                createTableCaption(workbook, sheet, tableRowStartIndex, tableColumnStartIndex + tableColumnOffset);

                createLeftSpace(workbook, sheet, tableRowStartIndex, 0, maxNumberRowsPerPage + 2);
                createDivideSpace(workbook, sheet, tableRowStartIndex, tableColumnStartIndex + 5, maxNumberRowsPerPage + 2);

                if (pageIndex == 1) {//первая страница
                    int numberOperationsForCurrentPage = numberDataRecords - currentOperationIndex + 1 <= maxNumberRowsPerPage ?
                        numberDataRecords - currentOperationIndex : maxNumberRowsPerPage - 1;

                    createBalanceDayRows(workbook, sheet, tableRowStartIndex + 2, tableColumnStartIndex,
                        balanceFixation.getBalanceStart(), i18n.get("unloadCashBook.remainDayStart"));

                    createDataRows(workbook, sheet, tableRowStartIndex + 3, tableColumnStartIndex, operations,
                        currentOperationIndex, numberOperationsForCurrentPage);

                    if (numberOperationsForCurrentPage <= maxNumberRowsPerPage - (numberFooterRows + numberInfoRows)) {
                        createBalanceTotalRows(workbook, sheet, tableRowStartIndex + 3 + numberOperationsForCurrentPage,
                            tableColumnStartIndex, totalIncome, totalExpense);
                        createBalanceDayRows(workbook, sheet, tableRowStartIndex + 5 + numberOperationsForCurrentPage,
                            tableColumnStartIndex, balanceFixation.getBalanceEnd(), i18n.get("unloadCashBook.remainDayEnd"));
                        createFooter(workbook, sheet, tableRowStartIndex + maxNumberRowsPerPage + 2 - numberFooterRows,
                            tableColumnStartIndex, (int) numberIncomeDocuments, (int) numberExpenseDocuments);
                        dayInfo = true;
                        footer = true;
                    } else if (numberOperationsForCurrentPage <= maxNumberRowsPerPage - numberInfoRows) {
                        createBalanceTotalRows(workbook, sheet, tableRowStartIndex + 3 + numberOperationsForCurrentPage,
                            tableColumnStartIndex, totalIncome, totalExpense);
                        createBalanceDayRows(workbook, sheet, tableRowStartIndex + 5 + numberOperationsForCurrentPage,
                            tableColumnStartIndex, balanceFixation.getBalanceEnd(), i18n.get("unloadCashBook.remainDayEnd"));
                        dayInfo = true;
                        footer = false;
                    } else if (numberOperationsForCurrentPage <= maxNumberRowsPerPage - 1) {
                        BigDecimal income = new BigDecimal(0);
                        BigDecimal expense = new BigDecimal(0);
                        for (int i = currentOperationIndex; i < currentOperationIndex + numberOperationsForCurrentPage; i++) {
                            if (operations.get(i).getPaymentMethod() == PaymentMethod.CASH_REPLENISH) {
                                income = income.add(operations.get(i).getOperationValue());
                            }
                            if (operations.get(i).getPaymentMethod() == PaymentMethod.CASH_ISSUE) {
                                expense = expense.add(operations.get(i).getOperationValue());
                            }
                        }
                        createBalanceMoveRows(workbook, sheet, tableRowStartIndex + 3 + numberOperationsForCurrentPage,
                            tableColumnStartIndex, income, expense);
                    }
                    currentOperationIndex = currentOperationIndex + numberOperationsForCurrentPage;
                    pageIndex++;
                }

                if (pageIndex > 1) {//последующие страницы
                    if (currentOperationIndex >= numberDataRecords) {
                        int tempIndex = tableRowStartIndex + 2;
                        if (!dayInfo) {
                            createBalanceTotalRows(workbook, sheet, tempIndex,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                totalIncome, totalExpense);
                            tempIndex = tempIndex + 2;
                            createBalanceDayRows(workbook, sheet, tempIndex,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                balanceFixation.getBalanceEnd(), i18n.get("unloadCashBook.remainDayEnd"));
                            tempIndex++;
                        }
                        if (!footer) {
                            createFooter(workbook, sheet,
                                tableRowStartIndex + maxNumberRowsPerPage + 2 - numberFooterRows,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                (int) numberIncomeDocuments, (int) numberExpenseDocuments);
                        }
                    } else {
                        int numberOperationsForCurrentPage = numberDataRecords - currentOperationIndex <= maxNumberRowsPerPage ?
                            numberDataRecords - currentOperationIndex : maxNumberRowsPerPage;

                        createDataRows(workbook, sheet, tableRowStartIndex + 2,
                            pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                            operations, currentOperationIndex, numberOperationsForCurrentPage);

                        if (numberOperationsForCurrentPage <= maxNumberRowsPerPage - (numberFooterRows + (numberInfoRows - 1))) {
                            createBalanceTotalRows(workbook, sheet, tableRowStartIndex + 2 + numberOperationsForCurrentPage,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                totalIncome, totalExpense);
                            createBalanceDayRows(workbook, sheet, tableRowStartIndex + 4 + numberOperationsForCurrentPage,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                balanceFixation.getBalanceEnd(), i18n.get("unloadCashBook.remainDayEnd"));
                            createFooter(workbook, sheet, tableRowStartIndex + maxNumberRowsPerPage + 2 - numberFooterRows,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                (int) numberIncomeDocuments, (int) numberExpenseDocuments);
                            dayInfo = true;
                            footer = true;
                        } else if (numberOperationsForCurrentPage <= maxNumberRowsPerPage - (numberInfoRows - 1)) {
                            createBalanceTotalRows(workbook, sheet, tableRowStartIndex + 2 + numberOperationsForCurrentPage,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                totalIncome, totalExpense);
                            createBalanceDayRows(workbook, sheet, tableRowStartIndex + 4 + numberOperationsForCurrentPage,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                balanceFixation.getBalanceEnd(), i18n.get("unloadCashBook.remainDayEnd"));
                            dayInfo = true;
                        } else if (numberOperationsForCurrentPage <= maxNumberRowsPerPage) {
                            BigDecimal income = new BigDecimal(0);
                            BigDecimal expense = new BigDecimal(0);
                            for (int i = currentOperationIndex; i < currentOperationIndex + numberOperationsForCurrentPage; i++) {
                                if (operations.get(i).getPaymentMethod() == PaymentMethod.CASH_REPLENISH) {
                                    income = income.add(operations.get(i).getOperationValue());
                                }
                                if (operations.get(i).getPaymentMethod() == PaymentMethod.CASH_ISSUE) {
                                    expense = expense.add(operations.get(i).getOperationValue());
                                }
                            }
                            createBalanceMoveRows(workbook, sheet, tableRowStartIndex + 2 + numberOperationsForCurrentPage,
                                pageIndex % 2 == 0 ? tableColumnStartIndex + tableColumnOffset : tableColumnStartIndex,
                                income, expense);
                        }
                        currentOperationIndex = currentOperationIndex + numberOperationsForCurrentPage;
                        pageIndex++;
                    }
                }
            }

            String fileInPath = getFileInPath(dateFormatter, department, balanceFixation.getFixationDate());

            try {
                FileOutputStream outputStream = new FileOutputStream(fileInPath);
                workbook.write(outputStream);
                outputStream.close();
                return fileInPath;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else return null;
    }

    /**
     * @param workbook     - ссылка на рабочую книгу
     * @param sheet        - ссылка на лист в книге
     * @param sheetNumber  - номер текущего листа
     * @param rowIndex     - абсолютный индекс строки на листе с которого начнется построение
     * @param columnIndex  - абсолютный индекс колонки на листе с которого начнется построение
     * @param fixationDate - дата на которую формируется кассовая книга
     * @param department   - отдел по которому формируется кассовая книга
     */
    private void createTitle(XSSFWorkbook workbook, XSSFSheet sheet, int sheetNumber, int rowIndex, int columnIndex,
                             LocalDate fixationDate, Department department) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateAlignmentCellStyle(style, HorizontalAlignment.LEFT, VerticalAlignment.CENTER);
        generateFontCellStyle(style, workbook, true, 10, false);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, columnIndex, columnIndex + 3));
        Row row = sheet.createRow(rowIndex);
        Cell cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        String caption = String.format("%s %s", i18n.get("unloadCashBook.cashdeskFor"), dateFormatter.format(fixationDate), department.getName());
        cell.setCellStyle(style);
        cell.setCellValue(caption);

        cell = row.createCell(columnIndex + 4);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        caption = String.format("%s %s", i18n.get("unloadCashBook.list"), sheetNumber);
        cell.setCellStyle(style);
        cell.setCellValue(caption);

        row = sheet.createRow(rowIndex + 1);
        cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        caption = String.format("%s %s", i18n.get("unloadCashBook.department"), department.getName());
        cell.setCellStyle(style);
        cell.setCellValue(caption);
    }

    /**
     * Создает заголовок таблицы
     *
     * @param workbook    - ссылка на рабочую книгу
     * @param sheet       - ссылка на лист в книге
     * @param rowIndex    - абсолютный индекс строки на листе с которого начнется построение
     * @param columnIndex - абсолютный индекс колонки на листе с которого начнется построение
     */
    private void createTableCaption(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateAlignmentCellStyle(style, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        generateFontCellStyle(style, workbook, false, 8, true);
        generateBorderCellStyle(style, BorderStyle.THIN, true, true, true, true);

        Row row = sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
        row.setHeight((short) 1000);

        Cell cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.documentNumber"));

        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.fromTo"));

        cell = row.createCell(columnIndex + 2);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.accountCorNumber"));

        cell = row.createCell(columnIndex + 3);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.income"));

        cell = row.createCell(columnIndex + 4);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.expense"));

        //цифры
        row = sheet.getRow(rowIndex + 1) == null ? sheet.createRow(rowIndex + 1) : sheet.getRow(rowIndex + 1);
        cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue("1");

        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue("2");

        cell = row.createCell(columnIndex + 2);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue("3");

        cell = row.createCell(columnIndex + 3);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue("4");

        cell = row.createCell(columnIndex + 4);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue("5");
    }

    private void createLeftSpace(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex,
                                 int tableMaxNumberRows) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateAlignmentCellStyle(style, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        generateFontCellStyle(style, workbook, false, 8, false);
        //generateBorderCellStyle(style, BorderStyle.THIN, true, true, true, true);
        style.setRotation((short) 90);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + tableMaxNumberRows, columnIndex, columnIndex + 1));
        Row row = sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
        Cell cell = row.createCell(columnIndex);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.filingField"));
        cell = row.createCell(columnIndex + 1);
        cell.setCellStyle(style);
    }

    private void createDivideSpace(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex,
                                   int tableMaxNumberRows) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateAlignmentCellStyle(style, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        generateFontCellStyle(style, workbook, false, 8, false);
        generateBorderCellStyle(style, BorderStyle.DASHED, false, true, false, false);
        style.setRotation((short) 90);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + tableMaxNumberRows, columnIndex, columnIndex));
        Row row = sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
        Cell cell = row.createCell(columnIndex);

        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.cuttingLine"));
    }

    private void createDataRows(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex,
                                List<Operation> operations, int operationIndex, int maxNumber) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateFontCellStyle(style, workbook, false, 8, false);
        generateAlignmentCellStyle(style, HorizontalAlignment.LEFT, VerticalAlignment.CENTER);
        generateBorderCellStyle(style, BorderStyle.THIN, true, true, true, true);

        XSSFCellStyle style1 = workbook.createCellStyle();
        generateFontCellStyle(style1, workbook, false, 8, false);
        generateAlignmentCellStyle(style1, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER);
        generateBorderCellStyle(style1, BorderStyle.THIN, true, true, true, true);

        int i;
        for (i = 0; i < maxNumber; i++) {
            if (i + operationIndex > operations.size() - 1) break;

            Row row = sheet.getRow(rowIndex + i) == null ? sheet.createRow(rowIndex + i) : sheet.getRow(rowIndex + i);
            Row row1 = sheet.getRow(rowIndex + i) == null ? sheet.createRow(rowIndex + i) : sheet.getRow(rowIndex + i);

            Cell cell = row.createCell(columnIndex);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellStyle(style1);
            cell.setCellValue(operations.get(operationIndex + i).getOperationNumber());

            cell = row.createCell(columnIndex + 2);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            generateAlignmentCellStyle(style, HorizontalAlignment.LEFT, VerticalAlignment.CENTER);
            cell.setCellStyle(style);
            cell.setCellValue("");

            if (operations.get(operationIndex + i).getPaymentMethod() == PaymentMethod.CASH_REPLENISH) {
                cell = row.createCell(columnIndex + 1);
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellStyle(style);
                cell.setCellValue(operations.get(operationIndex + i).getPayer().getDisplayName());

                cell = row.createCell(columnIndex + 3);
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                cell.setCellStyle(style1);
                cell.setCellValue(moneyFormat.format(operations.get(operationIndex + i).getOperationValue()));

                cell = row.createCell(columnIndex + 4);
                cell.setCellStyle(style);

                cell = row1.createCell(columnIndex + 4);
                cell.setCellStyle(style);
            }

            if (operations.get(operationIndex + i).getPaymentMethod() == PaymentMethod.CASH_ISSUE) {
                cell = row.createCell(columnIndex + 1);
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellStyle(style);
                cell.setCellValue(operations.get(operationIndex + i).getRecipient().getDisplayName());

                cell = row.createCell(columnIndex + 3);
                cell.setCellStyle(style);

                cell = row1.createCell(columnIndex + 3);
                cell.setCellStyle(style);

                cell = row.createCell(columnIndex + 4);
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                cell.setCellStyle(style1);
                cell.setCellValue(moneyFormat.format(operations.get(operationIndex + i).getOperationValue()));
            }
        }
    }

    private void createBalanceDayRows(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex,
                                      BigDecimal balance, String text) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateAlignmentCellStyle(style, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER);
        generateFontCellStyle(style, workbook, true, 8, false);
        generateBorderCellStyle(style, BorderStyle.THIN, true, true, true, true);

        XSSFCellStyle style1 = workbook.createCellStyle();
        generateAlignmentCellStyle(style1, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        generateFontCellStyle(style1, workbook, true, 8, false);
        generateBorderCellStyle(style1, BorderStyle.THIN, true, true, true, true);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, columnIndex + 1, columnIndex + 2));
        Row row = sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);

        Cell cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);

        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(text);

        cell = row.createCell(columnIndex + 3);
        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        cell.setCellStyle(style);
        cell.setCellValue(moneyFormat.format(balance));

        cell = row.createCell(columnIndex + 4);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style1);
        cell.setCellValue("X");
    }

    private void createBalanceTotalRows(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex,
                                        BigDecimal income, BigDecimal expense) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateAlignmentCellStyle(style, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER);
        generateFontCellStyle(style, workbook, true, 8, false);
        generateBorderCellStyle(style, BorderStyle.THIN, true, true, true, true);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 1, columnIndex, columnIndex));
        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 1, columnIndex + 1, columnIndex + 2));
        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 1, columnIndex + 3, columnIndex + 3));
        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 1, columnIndex + 4, columnIndex + 4));

        Row row = sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);

        Cell cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);

        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.dayTotal"));

        cell = row.createCell(columnIndex + 3);
        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        cell.setCellStyle(style);
        cell.setCellValue(moneyFormat.format(income));

        cell = row.createCell(columnIndex + 4);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(moneyFormat.format(expense));
    }

    private void createBalanceMoveRows(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex,
                                       BigDecimal income, BigDecimal expense) {
        XSSFCellStyle style = workbook.createCellStyle();
        generateAlignmentCellStyle(style, HorizontalAlignment.RIGHT, VerticalAlignment.CENTER);
        generateFontCellStyle(style, workbook, true, 8, false);
        generateBorderCellStyle(style, BorderStyle.THIN, true, true, true, true);

        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, columnIndex + 1, columnIndex + 2));
        Row row = sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);

        Cell cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);

        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(i18n.get("unloadCashBook.move"));

        cell = row.createCell(columnIndex + 3);
        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        cell.setCellStyle(style);
        cell.setCellValue(moneyFormat.format(income));

        cell = row.createCell(columnIndex + 4);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(style);
        cell.setCellValue(moneyFormat.format(expense));
    }

    private void createFooter(XSSFWorkbook workbook, XSSFSheet sheet, int rowIndex, int columnIndex,
                              int numberIncomeDocuments, int numberExpenseDocuments) {
        sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, columnIndex + 2, columnIndex + 4));//поле ФИО расшифровки подписи
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 1, rowIndex + 1, columnIndex + 2, columnIndex + 4));//поле "расшифровка подписи"
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 2, rowIndex + 2, columnIndex, columnIndex + 4));//поле "Записи в кассе..."
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 3, rowIndex + 3, columnIndex + 1, columnIndex + 2));//поле с кол-вом приходных
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 4, rowIndex + 4, columnIndex + 1, columnIndex + 2));//поле "прописью" кол-ва приходных
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 5, rowIndex + 5, columnIndex + 1, columnIndex + 2));//поле с кол-вом расходных
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 6, rowIndex + 6, columnIndex + 1, columnIndex + 2));//поле "прописью" кол-ва расходных
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 7, rowIndex + 7, columnIndex + 2, columnIndex + 4));//поле ФИО расшифровки подписи
        sheet.addMergedRegion(new CellRangeAddress(rowIndex + 8, rowIndex + 8, columnIndex + 2, columnIndex + 4));//поле "расшифровка подписи"

        XSSFCellStyle tempStyle = workbook.createCellStyle();

        Row row = sheet.getRow(rowIndex) == null ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
        Cell cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 8, HorizontalAlignment.LEFT, VerticalAlignment.CENTER));
        cell.setCellValue(i18n.get("unloadCashBook.cashier"));
        cell = row.createCell(columnIndex + 1);
        tempStyle = getBorderCellStyle(tempStyle, BorderStyle.THIN, false, false, true, false);
        cell.setCellStyle(tempStyle);
        cell = row.createCell(columnIndex + 2);
        cell.setCellStyle(tempStyle);

        row = sheet.getRow(rowIndex + 1) == null ? sheet.createRow(rowIndex + 1) : sheet.getRow(rowIndex + 1);
        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 6, HorizontalAlignment.CENTER, VerticalAlignment.TOP));
        cell.setCellValue(i18n.get("unloadCashBook.signature"));
        cell = row.createCell(columnIndex + 2);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 6, HorizontalAlignment.CENTER, VerticalAlignment.TOP));
        cell.setCellValue(i18n.get("unloadCashBook.signatureOwner"));

        row = sheet.getRow(rowIndex + 2) == null ? sheet.createRow(rowIndex + 2) : sheet.getRow(rowIndex + 2);
        cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 8, HorizontalAlignment.LEFT, VerticalAlignment.CENTER));
        cell.setCellValue(i18n.get("unloadCashBook.text1"));

        row = sheet.getRow(rowIndex + 3) == null ? sheet.createRow(rowIndex + 3) : sheet.getRow(rowIndex + 3);
        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        tempStyle = getCellStyle(workbook, true, 8, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        tempStyle = getBorderCellStyle(tempStyle, BorderStyle.THIN, false, false, true, false);
        cell.setCellStyle(tempStyle);
        cell.setCellValue(convertLongToText((long) numberIncomeDocuments, systemLocale));
        cell = row.createCell(columnIndex + 3);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 8, HorizontalAlignment.LEFT, VerticalAlignment.CENTER));
        cell.setCellValue(i18n.get("unloadCashBook.text2"));

        row = sheet.getRow(rowIndex + 4) == null ? sheet.createRow(rowIndex + 4) : sheet.getRow(rowIndex + 4);
        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 6, HorizontalAlignment.CENTER, VerticalAlignment.CENTER));
        cell.setCellValue(i18n.get("unloadCashBook.inwords"));

        row = sheet.getRow(rowIndex + 5) == null ? sheet.createRow(rowIndex + 5) : sheet.getRow(rowIndex + 5);
        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        tempStyle = getCellStyle(workbook, true, 8, HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        tempStyle = getBorderCellStyle(tempStyle, BorderStyle.THIN, false, false, true, false);
        cell.setCellStyle(tempStyle);

        cell.setCellValue(convertLongToText((long) numberExpenseDocuments, systemLocale));
        cell = row.createCell(columnIndex + 3);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 8, HorizontalAlignment.LEFT, VerticalAlignment.CENTER));
        cell.setCellValue(i18n.get("unloadCashBook.text3"));

        row = sheet.getRow(rowIndex + 6) == null ? sheet.createRow(rowIndex + 6) : sheet.getRow(rowIndex + 6);
        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 6, HorizontalAlignment.CENTER, VerticalAlignment.CENTER));
        cell.setCellValue(i18n.get("unloadCashBook.inwords"));

        row = sheet.getRow(rowIndex + 7) == null ? sheet.createRow(rowIndex + 7) : sheet.getRow(rowIndex + 7);
        cell = row.createCell(columnIndex);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 8, HorizontalAlignment.LEFT, VerticalAlignment.CENTER));
        cell.setCellValue(i18n.get("unloadCashBook.Accountant"));
        cell = row.createCell(columnIndex + 1);
        tempStyle = getBorderCellStyle(tempStyle, BorderStyle.THIN, false, false, true, false);
        cell.setCellStyle(tempStyle);
        cell = row.createCell(columnIndex + 2);
        cell.setCellStyle(tempStyle);

        row = sheet.getRow(rowIndex + 8) == null ? sheet.createRow(rowIndex + 8) : sheet.getRow(rowIndex + 8);
        cell = row.createCell(columnIndex + 1);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 6, HorizontalAlignment.CENTER, VerticalAlignment.TOP));
        cell.setCellValue(i18n.get("unloadCashBook.signature"));
        cell = row.createCell(columnIndex + 2);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellStyle(getCellStyle(workbook, false, 6, HorizontalAlignment.CENTER, VerticalAlignment.TOP));
        cell.setCellValue(i18n.get("unloadCashBook.signatureOwner"));
    }

    private void initColumnParameters(XSSFSheet sheet, int columnIndexStart) {
        for (int i = 0; i < columnIndexStart; i++) sheet.setColumnWidth(i, 500);

        sheet.setColumnWidth(columnIndexStart, 2000);
        sheet.setColumnWidth(columnIndexStart + 1, 5000);
        sheet.setColumnWidth(columnIndexStart + 2, 2500);
        sheet.setColumnWidth(columnIndexStart + 3, 2500);
        sheet.setColumnWidth(columnIndexStart + 4, 2500);

        sheet.setColumnWidth(columnIndexStart + 5, 500);
        sheet.setColumnWidth(columnIndexStart + 6, 500);

        sheet.setColumnWidth(columnIndexStart + 7, 2000);
        sheet.setColumnWidth(columnIndexStart + 8, 5000);
        sheet.setColumnWidth(columnIndexStart + 9, 2500);
        sheet.setColumnWidth(columnIndexStart + 10, 2500);
        sheet.setColumnWidth(columnIndexStart + 11, 2500);
    }

    private XSSFCellStyle getBorderCellStyle(XSSFCellStyle style, BorderStyle borderStyle, boolean top, boolean right, boolean bottom, boolean left) {

        if (top) style.setBorderTop(borderStyle);
        if (right) style.setBorderRight(borderStyle);
        if (bottom) style.setBorderBottom(borderStyle);
        if (left) style.setBorderLeft(borderStyle);

        return style;
    }

    private XSSFCellStyle getCellStyle(XSSFWorkbook book, boolean bold, int height, HorizontalAlignment ha, VerticalAlignment va) {
        XSSFCellStyle style = book.createCellStyle();

        XSSFFont font = book.createFont();
        font.setFontHeightInPoints((short) height);
        font.setBold(bold);
        style.setFont(font);
        style.setAlignment(ha);
        style.setVerticalAlignment(va);

        return style;
    }

    private XSSFCellStyle generateAlignmentCellStyle(XSSFCellStyle style, HorizontalAlignment ha, VerticalAlignment va) {
        style.setAlignment(ha);
        style.setVerticalAlignment(va);

        return style;
    }

    private XSSFCellStyle generateFontCellStyle(XSSFCellStyle style, XSSFWorkbook book, boolean bold, int height, boolean wrapText) {
        XSSFFont font = book.createFont();
        font.setFontHeightInPoints((short) height);
        font.setBold(bold);
        style.setFont(font);
        style.setWrapText(wrapText);
        return style;
    }

    private XSSFCellStyle generateBorderCellStyle(XSSFCellStyle style, BorderStyle borderStyle, boolean top, boolean right, boolean bottom, boolean left) {
        if (top) {
            style.setBorderTop(borderStyle);
        }
        if (right) {
            style.setBorderRight(borderStyle);
        }
        if (bottom) {
            style.setBorderBottom(borderStyle);
        }
        if (left) {
            style.setBorderLeft(borderStyle);
        }

        return style;
    }

    private String convertLongToText(Long value, SystemLocale locale) {
        String text;
        switch (locale) {
            case ENGLISH:
                text = NumberToTextConvertor_EN.convert(value);
                break;
            case RUSSIAN:
                text = NumberToTextConvertor_RU.convert(value);
                break;
            default:
                text = "locale is not defined";
        }

        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }
}

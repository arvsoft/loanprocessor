package ru.loanpro.base.client.domain.details;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.VehicleCategory;
import ru.loanpro.global.directory.VehicleType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

/**
 * Транспорт
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "vehicle", schema = DatabaseSchema.CLIENT)
public class Vehicle extends AbstractIdEntity {
    /**
     * Ссылка на подробную информацию о физическом лице.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_details_id")
    private PhysicalClientDetails clientDetails;

    /**
     * Тип транспорта.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "vehicle_type")
    private VehicleType vehicleType;

    /**
     * Модель
     */
    @Column(name = "model")
    private String model;

    /**
     * Год выпуска.
     */
    @Column(name = "manufacure_year")
    private int manufactureYear;

    /**
     * Цвет
     */
    @Column(name = "color")
    private String color;

    /**
     * Категория транспорта.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "vehicle_category")
    private VehicleCategory vehicleCategory;

    /**
     * Регистрационный номер
     */
    @Column(name = "reg_number")
    private String registrationNumber;

    /**
     * VIN
     */
    @Column(name = "vin_number")
    private String vinNumber;

    /**
     * Chassis number
     */
    @Column(name = "chassis_number")
    private String chassisNumber;

    /**
     * Body number
     */
    @Column(name = "body_number")
    private String bodyNumber;

    /**
     * Certificate number
     */
    @Column(name = "certificate_number")
    private String certificateNumber;

    /**
     * Certificate issue date
     */
    @Column(name = "certificate_date")
    private LocalDate certificateDate;

    /**
     * Certificate issue organization
     */
    @Column(name = "certificate_organization")
    private String certificateOrganization;

    /**
     * Оценочная стоимость
     */
    @Column(name = "estimated_value", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal estimatedValue;

    /**
     * Идентификатор, является ли имущество в залоге
     */
    @Column(name = "pledge")
    private boolean pledge;

    /**
     * Владелец залога
     */
    @Column(name = "pledge_hostage")
    private String pledgeHostage;

    /**
     * Документ о залоге
     */
    @Column(name = "pledge_document")
    private String pledgeDocument;

    /**
     * Коллекция сканированных образов документов.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "vehicle_scanned",
        schema = DatabaseSchema.CLIENT,
        joinColumns = @JoinColumn(name = "vehicle_id"),
        inverseJoinColumns = @JoinColumn(name = "storage_file_id"))
    private Set<ScannedDocument> scans;

    public Vehicle() {
    }

    public PhysicalClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(PhysicalClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(int manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public VehicleCategory getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(VehicleCategory vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getBodyNumber() {
        return bodyNumber;
    }

    public void setBodyNumber(String bodyNumber) {
        this.bodyNumber = bodyNumber;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public LocalDate getCertificateDate() {
        return certificateDate;
    }

    public void setCertificateDate(LocalDate certificateDate) {
        this.certificateDate = certificateDate;
    }

    public String getCertificateOrganization() {
        return certificateOrganization;
    }

    public void setCertificateOrganization(String certificateOrganization) {
        this.certificateOrganization = certificateOrganization;
    }

    public BigDecimal getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(BigDecimal estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public boolean isPledge() {
        return pledge;
    }

    public void setPledge(boolean pledge) {
        this.pledge = pledge;
    }

    public String getPledgeHostage() {
        return pledgeHostage;
    }

    public void setPledgeHostage(String pledgeHostage) {
        this.pledgeHostage = pledgeHostage;
    }

    public String getPledgeDocument() {
        return pledgeDocument;
    }

    public void setPledgeDocument(String pledgeDocument) {
        this.pledgeDocument = pledgeDocument;
    }

    public Set<ScannedDocument> getScans() {
        return scans;
    }

    public void setScans(Set<ScannedDocument> scans) {
        this.scans = scans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(vehicleType, vehicle.vehicleType) &&
            Objects.equals(model, vehicle.model) &&
            Objects.equals(manufactureYear, vehicle.manufactureYear) &&
            Objects.equals(color, vehicle.color) &&
            Objects.equals(vehicleCategory, vehicle.vehicleCategory) &&
            Objects.equals(registrationNumber, vehicle.registrationNumber) &&
            Objects.equals(vinNumber, vehicle.vinNumber) &&
            Objects.equals(chassisNumber, vehicle.chassisNumber) &&
            Objects.equals(bodyNumber, vehicle.bodyNumber) &&
            Objects.equals(certificateNumber, vehicle.certificateNumber) &&
            Objects.equals(certificateDate, vehicle.certificateDate) &&
            Objects.equals(certificateOrganization, vehicle.certificateOrganization) &&
            Objects.equals(estimatedValue, vehicle.estimatedValue) &&
            Objects.equals(pledge, vehicle.pledge) &&
            Objects.equals(pledgeHostage, vehicle.pledgeHostage) &&
            Objects.equals(pledgeDocument, vehicle.pledgeDocument) &&
            Objects.equals(scans, vehicle.scans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), vehicleType, model, manufactureYear, color, vehicleCategory,
            registrationNumber, vinNumber, chassisNumber, bodyNumber, certificateNumber, certificateDate,
            certificateOrganization, estimatedValue, pledge, pledgeHostage, pledgeDocument, scans);
    }
}

package ru.loanpro.base.operation.domain;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.account.domain.BaseAccount;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.OperationStatus;
import ru.loanpro.global.directory.PaymentMethod;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.domain.converter.CurrencyConverter;
import ru.loanpro.global.annotation.EntityName;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.Objects;

/**
 * Класс сущности денежной операции
 *
 * @author Maksim Askhaev
 */
@Entity
@EntityName("entity.name.operation")
@Table(name = "operations", schema = DatabaseSchema.OPERATION)
public class Operation extends AbstractIdEntity {

    /**
     * Департамент обработки операции
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    /**
     * Статус операции
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OperationStatus status;

    /**
     * Номер операции
     */
    @Column(name = "operation_number")
    private String operationNumber;

    /**
     * Счет плательщика
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_payer_id")
    private BaseAccount accountPayer;

    /**
     * Плательщик (физлицо или юрлицо, в случае, если он оплачивает наличкой)
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payer_id")
    private PhysicalClient payer;

    /**
     * Идентификатор фиксации операции на счету плательщика
     */
    @Column(name="is_payer_fixed")
    private boolean isPayerFixed;

    /**
     * Счет получателя
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_recipient_id")
    private BaseAccount accountRecipient;

    /**
     * Получатель (физлицо или юрлицо, в случае, он получает наличкой)
     * Внимание! Юрлицо убрали временно
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_id")
    private PhysicalClient recipient;

    /**
     * Идентификатор фиксации операции на счету получателя
     */
    @Column(name="is_recipient_fixed")
    private boolean isRecipientFixed;

    /**
     * Дата и время операции, при смене статуса дата/время меняется
     */
    @Column(name = "operation_datetime", nullable = false)
    private Instant operationDateTime;

    /**
     * Размер операции
     */
    @Column(name = "operation_value", nullable = false)
    private BigDecimal operationValue;

    /**
     * Валюта. Указание валюты используется только при взносе наличных - в какой валюте вносят средства
     * или при выдаче наличных - в какой валюте выдаются наличные.
     */
    @Column(name = "currency")
    @Convert(converter = CurrencyConverter.class)
    private Currency currency;

    /**
     * Коэффициент конвертации валюты по данной операции.
     * Если валюты одинаковые то коэффициент равен 1.00
     */
    @Column(name = "currency_rate")
    private BigDecimal currencyRate;

    /**
     * Метод платежа (наличный/безнал)
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;

    /**
     * Заем, если операция связана с займом (выдача или оплата займа)
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id")
    private Loan loan;

    /**
     * Платеж, если операция связана с платежом по займу (только оплата по займу)
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "payment_id")
    private Payment payment;

    /**
     * Составляющие платежа (только если операция связана с платежом по займу
     * с разделением на составляющие платежа)
     */
    @Column(name = "payment_part")
    private String paymentPart;

    /**
     * Примечания
     */
    @Column(name = "notes")
    private String notes;

    /**
     * Департамент обработки операции
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public Operation() {
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public OperationStatus getStatus() {
        return status;
    }

    public void setStatus(OperationStatus status) {
        this.status = status;
    }

    public BaseAccount getAccountPayer() {
        return accountPayer;
    }

    public void setAccountPayer(BaseAccount accountPayer) {
        this.accountPayer = accountPayer;
    }

    public PhysicalClient getPayer() {
        return payer;
    }

    public void setPayer(PhysicalClient payer) {
        this.payer = payer;
    }

    public BaseAccount getAccountRecipient() {
        return accountRecipient;
    }

    public void setAccountRecipient(BaseAccount accountRecipient) {
        this.accountRecipient = accountRecipient;
    }

    public PhysicalClient getRecipient() {
        return recipient;
    }

    public void setRecipient(PhysicalClient recipient) {
        this.recipient = recipient;
    }

    public Instant getOperationDateTime() {
        return operationDateTime;
    }

    public void setOperationDateTime(Instant operationDateTime) {
        this.operationDateTime = operationDateTime;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public BigDecimal getOperationValue() {
        return operationValue;
    }

    public void setOperationValue(BigDecimal operationValue) {
        this.operationValue = operationValue;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getPaymentPart() {
        return paymentPart;
    }

    public void setPaymentPart(String paymentPart) {
        this.paymentPart = paymentPart;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(BigDecimal currencyRate) {
        this.currencyRate = currencyRate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isPayerFixed() {
        return isPayerFixed;
    }

    public void setPayerFixed(boolean payerFixed) {
        isPayerFixed = payerFixed;
    }

    public boolean isRecipientFixed() {
        return isRecipientFixed;
    }

    public void setRecipientFixed(boolean recipientFixed) {
        isRecipientFixed = recipientFixed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;
        if (!super.equals(o)) return false;
        Operation operation = (Operation) o;
        return Objects.equals(department, operation.department) &&
            status == operation.status &&
            Objects.equals(accountPayer, operation.accountPayer) &&
            Objects.equals(operationNumber, operation.operationNumber) &&
            Objects.equals(payer, operation.payer) &&
            Objects.equals(accountRecipient, operation.accountRecipient) &&
            Objects.equals(recipient, operation.recipient) &&
            Objects.equals(operationDateTime, operation.operationDateTime) &&
            Objects.equals(operationValue, operation.operationValue) &&
            Objects.equals(currency, operation.currency) &&
            Objects.equals(isPayerFixed, operation.isPayerFixed) &&
            Objects.equals(isRecipientFixed, operation.isRecipientFixed) &&
            paymentMethod == operation.paymentMethod &&
            Objects.equals(notes, operation.notes);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), department, status, operationNumber, accountPayer, payer, accountRecipient,
            recipient, operationDateTime, operationValue, currency, paymentMethod, notes, isPayerFixed, isRecipientFixed);
    }
}

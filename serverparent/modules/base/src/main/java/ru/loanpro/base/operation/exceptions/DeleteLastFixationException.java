package ru.loanpro.base.operation.exceptions;

public class DeleteLastFixationException extends Exception {
    public DeleteLastFixationException(String message) {
        super(message);
    }
}

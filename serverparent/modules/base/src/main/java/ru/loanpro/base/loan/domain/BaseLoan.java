package ru.loanpro.base.loan.domain;

import org.hibernate.Hibernate;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.directory.domain.LoanType;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.LoanTermType;
import ru.loanpro.global.directory.PenaltyType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.domain.RemovableEntity;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.domain.converter.CurrencyConverter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Objects;

/**
 * Базовая сущность займа
 * Является базовым для классов займа, заявки, кредитного продукта
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "loans", schema = DatabaseSchema.LOAN)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "entity_type",
    discriminatorType = DiscriminatorType.STRING)
public abstract class BaseLoan extends RemovableEntity {

    /**
     * Пользователь, редактирующий сущность.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * Тип займа, справочные данные
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "loan_type_id")
    private LoanType loanType;

    /**
     * Сумма займа
     */
    @Column(name = "loan_balance", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal loanBalance;

    /**
     * Валюта
     */
    @Column(name = "currency")
    @Convert(converter = CurrencyConverter.class)
    private Currency currency;

    /**
     * Схема расчета, справочные данные из Enum
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "calc_scheme")
    private CalcScheme calcScheme;

    /**
     * Тип срока возврата, справочные данные из Enum
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "return_term")
    private ReturnTerm returnTerm;

    /**
     * Процентная ставка
     */
    @Column(name = "interest_value", precision = 8, scale = 3, columnDefinition = "DECIMAL(8,3)")
    private BigDecimal interestValue;

    /**
     * Тип процентной ставки, справочные данные из Enum
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "interest_type")
    private InterestType interestType;

    /**
     * Срок займа
     */
    @Column(name = "loan_term")
    private int loanTerm;

    /**
     * Тип срока займа, справочные данные из Enum
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "loan_term_type")
    private LoanTermType loanTermType;

    /**
     * Идентификатор, заменять ли дату первого платежа на указанное число
     */
    @Column(name = "replace_date")
    private boolean replaceDate;

    /**
     * Дата первого платежа, если включена замена
     */
    @Column(name = "replace_date_value")
    private int replaceDateValue;

    /**
     * Идентификатор, переносить ли дату первого платежа на след. месяц,
     * если длительность первого платежа меньше месяца
     */
    @Column(name = "replace_over_month")
    private boolean replaceOverMonth;

    /**
     * Идентификатор, включать ли оплату основной части полностью в последний платеж
     */
    @Column(name = "principal_last_pay")
    private boolean principalLastPay;

    /**
     * Идентификатор, рассчитывать ли проценты от суммы займа (не от остатка долга)
     */
    @Column(name = "interest_balance")
    private boolean interestBalance;

    /**
     * Идентификатор, исключить расчет процентов в первые N дней
     */
    @Column(name = "no_interest_fd")
    private boolean noInterestFD;

    /**
     * Значение кол-ва исключенных дней, если noInterestFD = true
     */
    @Column(name = "no_interest_fd_value")
    private int noInterestFDValue;

    /**
     * Идентификатор, включить ли дату выдачи в расчетный период
     */
    @Column(name = "include_issue_date")
    private boolean includeIssueDate;

    /**
     * Тип неустойки, справочные данные из Enum
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "penalty_type")
    private PenaltyType penaltyType;

    /**
     * Значение неустойки, в процентах в день
     */
    @Column(name = "penalty_value", precision = 8, scale = 3, columnDefinition = "DECIMAL(8,3)")
    private BigDecimal penaltyValue;

    /**
     * Идентификатор, включать ли штраф
     */
    @Column(name = "charge_fine")
    private boolean chargeFine;

    /**
     * Значение штрафа, если он включен
     */
    @Column(name = "fine_value", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal fineValue;

    /**
     * Идентификатор, является ли штраф единоразовым
     */
    @Column(name = "fine_one_time")
    private boolean fineOneTime;

    /**
     * Идентификатор, рассчитывать ли проценты строго по графику
     * Используется при погашении займа
     */
    @Column(name = "interest_strictly")
    private boolean interestStrictly;

    /**
     * Идентификатор, рассчитывать ли проценты в просроченный период
     * Используется при погашении займа
     */
    @Column(name = "interest_overdue")
    private boolean interestOverdue;

    /**
     * Идентификатор, рассчитывать ли проценты в первые N дней в полном объеме
     */
    @Column(name = "interest_firstdays")
    private boolean interestFirstDays;

    /**
     * Кол-во первых N дней (валидно, если interest_firstdays == true)
     */
    @Column(name = "interest_number_firstdays")
    private int interestNumberFirstDays;

    /**
     * Полная стоимость кредита
     */
    @Column(name = "psk_value", precision = 8, scale = 3, columnDefinition = "DECIMAL(8,3)")
    private BigDecimal pskValue;

    /**
     * Дата последнего платежа согласно графику
     */
    @Column(name = "last_pay_date")
    private LocalDate lastPayDate;

    /**
     * График рассчитанных платежей
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "loan", orphanRemoval = true)
    private List<Schedule> schedules = new ArrayList<>();

    /**
     * Сумма процентов
     */
    @Column(name = "interest_total", precision = 15, scale = 2, columnDefinition = "DECIMAL(15,2)")
    private BigDecimal interestTotal;

    public BaseLoan() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public CalcScheme getCalcScheme() {
        return calcScheme;
    }

    public void setCalcScheme(CalcScheme calcScheme) {
        this.calcScheme = calcScheme;
    }

    public ReturnTerm getReturnTerm() {
        return returnTerm;
    }

    public void setReturnTerm(ReturnTerm returnTerm) {
        this.returnTerm = returnTerm;
    }

    public BigDecimal getInterestValue() {
        return interestValue;
    }

    public void setInterestValue(BigDecimal interestValue) {
        this.interestValue = interestValue;
    }

    public InterestType getInterestType() {
        return interestType;
    }

    public void setInterestType(InterestType interestType) {
        this.interestType = interestType;
    }

    public int getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(int loanTerm) {
        this.loanTerm = loanTerm;
    }

    public LoanTermType getLoanTermType() {
        return loanTermType;
    }

    public void setLoanTermType(LoanTermType loanTermType) {
        this.loanTermType = loanTermType;
    }

    public boolean isReplaceDate() {
        return replaceDate;
    }

    public void setReplaceDate(boolean replaceDate) {
        this.replaceDate = replaceDate;
    }

    public int getReplaceDateValue() {
        return replaceDateValue;
    }

    public void setReplaceDateValue(int replaceDateValue) {
        this.replaceDateValue = replaceDateValue;
    }

    public boolean isReplaceOverMonth() {
        return replaceOverMonth;
    }

    public void setReplaceOverMonth(boolean replaceOverMonth) {
        this.replaceOverMonth = replaceOverMonth;
    }

    public boolean isPrincipalLastPay() {
        return principalLastPay;
    }

    public void setPrincipalLastPay(boolean principalLastPay) {
        this.principalLastPay = principalLastPay;
    }

    public boolean isInterestBalance() {
        return interestBalance;
    }

    public void setInterestBalance(boolean interestBalance) {
        this.interestBalance = interestBalance;
    }

    public boolean isNoInterestFD() {
        return noInterestFD;
    }

    public void setNoInterestFD(boolean noInterestFD) {
        this.noInterestFD = noInterestFD;
    }

    public int getNoInterestFDValue() {
        return noInterestFDValue;
    }

    public void setNoInterestFDValue(int noInterestFDValue) {
        this.noInterestFDValue = noInterestFDValue;
    }

    public boolean isIncludeIssueDate() {
        return includeIssueDate;
    }

    public void setIncludeIssueDate(boolean includeIssueDate) {
        this.includeIssueDate = includeIssueDate;
    }

    public PenaltyType getPenaltyType() {
        return penaltyType;
    }

    public void setPenaltyType(PenaltyType penaltyType) {
        this.penaltyType = penaltyType;
    }

    public BigDecimal getPenaltyValue() {
        return penaltyValue;
    }

    public void setPenaltyValue(BigDecimal penaltyValue) {
        this.penaltyValue = penaltyValue;
    }

    public boolean isChargeFine() {
        return chargeFine;
    }

    public void setChargeFine(boolean chargeFine) {
        this.chargeFine = chargeFine;
    }

    public BigDecimal getFineValue() {
        return fineValue;
    }

    public void setFineValue(BigDecimal fineValue) {
        this.fineValue = fineValue;
    }

    public boolean isFineOneTime() {
        return fineOneTime;
    }

    public void setFineOneTime(boolean fineOneTime) {
        this.fineOneTime = fineOneTime;
    }

    public boolean isInterestStrictly() {
        return interestStrictly;
    }

    public void setInterestStrictly(boolean interestStrictly) {
        this.interestStrictly = interestStrictly;
    }

    public boolean isInterestOverdue() {
        return interestOverdue;
    }

    public void setInterestOverdue(boolean interestOverdue) {
        this.interestOverdue = interestOverdue;
    }

    public boolean isInterestFirstDays() {
        return interestFirstDays;
    }

    public void setInterestFirstDays(boolean interestFirstDays) {
        this.interestFirstDays = interestFirstDays;
    }

    public int getInterestNumberFirstDays() {
        return interestNumberFirstDays;
    }

    public void setInterestNumberFirstDays(int interestNumberFirstDays) {
        this.interestNumberFirstDays = interestNumberFirstDays;
    }

    public BigDecimal getPskValue() {
        return pskValue;
    }

    public void setPskValue(BigDecimal pskValue) {
        this.pskValue = pskValue;
    }

    public LocalDate getLastPayDate() {
        return lastPayDate;
    }

    public void setLastPayDate(LocalDate lastPayDate) {
        this.lastPayDate = lastPayDate;
    }

    public BigDecimal getInterestTotal() {
        return interestTotal;
    }

    public void setInterestTotal(BigDecimal interestTotal) {
        this.interestTotal = interestTotal;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseLoan baseLoan = (BaseLoan) o;
        boolean equal = loanTerm == baseLoan.loanTerm &&
            replaceDate == baseLoan.replaceDate &&
            replaceDateValue == baseLoan.replaceDateValue &&
            replaceOverMonth == baseLoan.replaceOverMonth &&
            principalLastPay == baseLoan.principalLastPay &&
            interestBalance == baseLoan.interestBalance &&
            noInterestFD == baseLoan.noInterestFD &&
            noInterestFDValue == baseLoan.noInterestFDValue &&
            includeIssueDate == baseLoan.includeIssueDate &&
            chargeFine == baseLoan.chargeFine &&
            fineOneTime == baseLoan.fineOneTime &&
            interestStrictly == baseLoan.interestStrictly &&
            interestOverdue == baseLoan.interestOverdue &&
            interestFirstDays == baseLoan.interestFirstDays &&
            interestNumberFirstDays == baseLoan.interestNumberFirstDays &&
            Objects.equals(loanType, baseLoan.loanType) &&
            Objects.equals(loanBalance, baseLoan.loanBalance) &&
            Objects.equals(currency, baseLoan.currency) &&
            calcScheme == baseLoan.calcScheme &&
            returnTerm == baseLoan.returnTerm &&
            Objects.equals(interestValue, baseLoan.interestValue) &&
            interestType == baseLoan.interestType &&
            loanTermType == baseLoan.loanTermType &&
            penaltyType == baseLoan.penaltyType &&
            Objects.equals(penaltyValue, baseLoan.penaltyValue) &&
            Objects.equals(fineValue, baseLoan.fineValue) &&
            Objects.equals(pskValue, baseLoan.pskValue) &&
            Objects.equals(interestTotal, baseLoan.interestTotal) &&
            Objects.equals(lastPayDate, baseLoan.lastPayDate);

        if (Hibernate.isInitialized(schedules))
            equal = equal && Objects.equals(schedules, baseLoan.schedules);

        return equal;
    }

    public int scheduleHashCode() {
        return Objects.hash(loanBalance, currency, calcScheme, returnTerm, interestValue, interestType,
            loanTerm, loanTermType, replaceDate, replaceDateValue, replaceOverMonth, principalLastPay, interestBalance,
            noInterestFD, noInterestFDValue, includeIssueDate, penaltyType, penaltyValue, chargeFine, fineValue,
            fineOneTime, interestStrictly, interestOverdue, interestFirstDays, interestNumberFirstDays);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheduleHashCode(), loanType, pskValue, interestTotal, lastPayDate,
            Hibernate.isInitialized(schedules)
                ? Arrays.hashCode(schedules.toArray())
                : null);
    }
}

package ru.loanpro.base.printer;

import com.vaadin.spring.annotation.VaadinSessionScope;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.ConverterTypeVia;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.images.FileImageProvider;
import fr.opensagres.xdocreport.document.images.IImageProvider;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.printer.dpo.PrintClient;
import ru.loanpro.base.printer.dpo.PrintLoan;
import ru.loanpro.base.printer.dpo.PrintOperation;
import ru.loanpro.base.printer.dpo.PrintPayment;
import ru.loanpro.base.storage.IStorageService;
import ru.loanpro.base.storage.domain.PrintTemplateFile;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.base.storage.domain.connector.ClientPrintedFile;
import ru.loanpro.base.storage.domain.connector.LoanPrintedFile;
import ru.loanpro.base.storage.domain.connector.OperationPrintedFile;
import ru.loanpro.base.storage.domain.connector.PaymentPrintedFile;
import ru.loanpro.base.storage.exception.FileSystemErrorException;
import ru.loanpro.base.storage.service.PrintedFileService;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.SecurityService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Сервис печати. Формирует .pdf-файлы из сущностей системы и сохраняет их на диске.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@VaadinSessionScope
public class PrintService {

    private static Logger LOGGER = Logger.getLogger(PrintService.class.getName());

    private final PrintMapper mapper;
    private final SecurityService securityService;
    private final IStorageService storageService;
    private final PrintedFileService printedFileService;

    private final User user;

    @Value("${system.directory.printed}")
    private String printFolder;

    @Autowired
    public PrintService(PrintMapper mapper, SecurityService securityService, IStorageService storageService,
                        PrintedFileService printedFileService) {
        this.mapper = mapper;
        this.securityService = securityService;
        this.storageService = storageService;
        this.printedFileService = printedFileService;

        user = securityService.getUser();
    }

    /**
     * Печатает переданный объект на переданном шаблоне.
     *
     * @param template шаблон.
     * @param object   печатаемый объект.
     * @return имя документа на диске.
     * @throws IOException         если произошла ошибка файловой системы.
     * @throws XDocReportException если произошла ошибка при формировании документа из шаблона.
     */
    @Transactional
    public String printTemplate(PrintTemplateFile template, Object object)
        throws IOException, XDocReportException {

        String fileName;
        switch (template.getTemplateType()) {
            case PHYSICAL_CLIENT_PROFILE:
                fileName = printClientFromTemplate(template, (PhysicalClient) object);
                savePrintedEntity(ClientPrintedFile.class, ClientPrintedFile::setClient, (PhysicalClient) object,
                    fileName, template.getDisplayName());
                return fileName;

            case LOAN_AGREEMENT:
                fileName = printLoanFromTemplate(template, (Loan) object);
                savePrintedEntity(LoanPrintedFile.class, LoanPrintedFile::setLoan, (Loan) object,
                    fileName, template.getDisplayName());
                return fileName;

            case LOAN_PAYMENT:
                fileName = printPaymentFromTemplate(template, (Payment) object);
                savePrintedEntity(PaymentPrintedFile.class, PaymentPrintedFile::setPayment, (Payment) object,
                    fileName, template.getDisplayName());
                return fileName;

            case OPERATION:
                fileName = printOperationFromTemplate(template, (Operation) object);
                savePrintedEntity(OperationPrintedFile.class, OperationPrintedFile::setOperation, (Operation) object,
                    fileName, template.getDisplayName());
                return fileName;

            default:
                return null;
        }
    }

    /**
     * Маппит необходимые поля переданного клиента и создаёт .pdf-документ.
     * Сохраняет его на диске и возвращает имя сохранённого документа.
     *
     * @param template шаблон документа.
     * @param client   физический клиент.
     * @return имя сохранённого документа.
     * @throws IOException         если произошла ошибка ввода-вывода.
     * @throws XDocReportException если произошла ошибка формирования документа.
     */
    private String printClientFromTemplate(PrintTemplateFile template, PhysicalClient client)
        throws IOException, XDocReportException {
        PrintClient printPhysicalClient = mapper.toPrintClient(client);

        Map<String, Object> map = new HashMap<>();
        map.put("g", mapper.toPrintSetting());
        map.put("d", mapper.toPrintDepartment(securityService.getDepartment()));
        map.put("u", mapper.toPrintUser(user));
        map.put("c", printPhysicalClient);

        FieldsMetadata metadata = new FieldsMetadata();
        setImageData(client == null ? null : client.getPhotoFileName(), map, "imageProfile", metadata);

        return writeAsPdf(storageService.getPrintTemplateStream(template.getFileName()), map, metadata);
    }

    /**
     * Маппит необходимые поля переданного займа и создаёт .pdf-документ.
     * Сохраняет его на диске и возвращает имя сохранённого документа.
     *
     * @param template шаблон займа.
     * @param loan     займ.
     * @return имя сохранённого документа.
     * @throws IOException         если произошла ошибка ввода-вывода.
     * @throws XDocReportException если произошла ошибка формирования документа.
     */
    private String printLoanFromTemplate(PrintTemplateFile template, Loan loan)
        throws IOException, XDocReportException {
        PrintLoan printLoan = mapper.toPrintLoan(loan);
        PrintClient printBorrower = mapper.toPrintClient(loan.getBorrower());
        PrintClient printCoBorrower = mapper.toPrintClient(loan.getCoBorrower());

        Map<String, Object> map = new HashMap<>();
        map.put("g", mapper.toPrintSetting());
        map.put("d", mapper.toPrintDepartment(securityService.getDepartment()));
        map.put("u", mapper.toPrintUser(securityService.getUser()));
        map.put("l", printLoan);
        map.put("st", printLoan.getSchedules());
        map.put("pt", printLoan.getPayments());
        map.put("b", printBorrower);
        map.put("cb", printCoBorrower);

        FieldsMetadata metadata = new FieldsMetadata();
        metadata.addFieldAsList("st.OrdNo");
        metadata.addFieldAsList("st.Date");
        metadata.addFieldAsList("st.Term");
        metadata.addFieldAsList("st.Principal");
        metadata.addFieldAsList("st.Interest");
        metadata.addFieldAsList("st.Payment");
        metadata.addFieldAsList("st.Balance");
        metadata.addFieldAsList("pt.OrdNo");
        metadata.addFieldAsList("pt.Date");
        metadata.addFieldAsList("pt.Term");
        metadata.addFieldAsList("pt.OverdueTerm");
        metadata.addFieldAsList("pt.Principal");
        metadata.addFieldAsList("pt.Interest");
        metadata.addFieldAsList("pt.Penalty");
        metadata.addFieldAsList("pt.Fine");
        metadata.addFieldAsList("pt.Payment");
        metadata.addFieldAsList("pt.Balance");
        setImageData(loan.getBorrower() == null ? null :
            ((PhysicalClient)loan.getBorrower()).getPhotoFileName(), map, "b.ImageProfile", metadata);
        setImageData(loan.getCoBorrower() == null ? null :
            ((PhysicalClient)loan.getCoBorrower()).getPhotoFileName(), map, "cb.ImageProfile", metadata);

        return writeAsPdf(storageService.getPrintTemplateStream(template.getFileName()), map, metadata);
    }

    /**
     * Маппит необходимые поля переданного платежа по займу и создаёт .pdf-документ.
     * Сохраняет его на диске и возвращает имя сохранённого документа.
     *
     * @param template шаблон док-та с данными о платеже.
     * @param payment  платеж.
     * @return имя сохранённого документа.
     * @throws IOException         если произошла ошибка ввода-вывода.
     * @throws XDocReportException если произошла ошибка формирования документа.
     */
    private String printPaymentFromTemplate(PrintTemplateFile template, Payment payment)
        throws IOException, XDocReportException {
        PrintPayment printPayment = mapper.toPrintPayment(payment);
        PrintLoan printLoan = mapper.toPrintLoan(payment.getLoan());
        PrintClient printBorrower = mapper.toPrintClient(payment.getLoan().getBorrower());
        PrintClient printCoBorrower = mapper.toPrintClient(payment.getLoan().getCoBorrower());

        Map<String, Object> map = new HashMap<>();
        map.put("g", mapper.toPrintSetting());
        map.put("d", mapper.toPrintDepartment(securityService.getDepartment()));
        map.put("u", mapper.toPrintUser(securityService.getUser()));
        map.put("l", printLoan);
        map.put("st", printLoan.getSchedules());
        map.put("pt", printLoan.getPayments());
        map.put("p", printPayment);
        map.put("b", printBorrower);
        map.put("cb", printCoBorrower);

        FieldsMetadata metadata = new FieldsMetadata();
        metadata.addFieldAsList("st.OrdNo");
        metadata.addFieldAsList("st.Date");
        metadata.addFieldAsList("st.Term");
        metadata.addFieldAsList("st.Principal");
        metadata.addFieldAsList("st.Interest");
        metadata.addFieldAsList("st.Payment");
        metadata.addFieldAsList("st.Balance");
        metadata.addFieldAsList("pt.OrdNo");
        metadata.addFieldAsList("pt.Date");
        metadata.addFieldAsList("pt.Term");
        metadata.addFieldAsList("pt.OverdueTerm");
        metadata.addFieldAsList("pt.Principal");
        metadata.addFieldAsList("pt.Interest");
        metadata.addFieldAsList("pt.Penalty");
        metadata.addFieldAsList("pt.Fine");
        metadata.addFieldAsList("pt.Payment");
        metadata.addFieldAsList("pt.Balance");
        setImageData(payment.getLoan().getBorrower() == null ? null :
            ((PhysicalClient)payment.getLoan().getBorrower()).getPhotoFileName(), map, "b.ImageProfile", metadata);
        setImageData(payment.getLoan().getCoBorrower() == null ? null :
            ((PhysicalClient)payment.getLoan().getCoBorrower()).getPhotoFileName(), map, "cb.ImageProfile", metadata);

        return writeAsPdf(storageService.getPrintTemplateStream(template.getFileName()), map, metadata);
    }

    /**
     * Маппит необходимые поля переданной операции и создаёт .pdf-документ.
     * Сохраняет его на диске и возвращает имя сохранённого документа.
     *
     * @param template  шаблон док-та с данными о платеже.
     * @param operation операция.
     * @return имя сохранённого документа.
     * @throws IOException         если произошла ошибка ввода-вывода.
     * @throws XDocReportException если произошла ошибка формирования документа.
     */
    private String printOperationFromTemplate(PrintTemplateFile template, Operation operation)
        throws IOException, XDocReportException {
        PrintOperation printOperation = mapper.toPrintOperation(operation);

        Map<String, Object> map = new HashMap<>();
        map.put("g", mapper.toPrintSetting());
        map.put("d", mapper.toPrintDepartment(securityService.getDepartment()));
        map.put("u", mapper.toPrintUser(securityService.getUser()));
        map.put("o", printOperation);

        return writeAsPdf(storageService.getPrintTemplateStream(template.getFileName()), map, null);
    }

    /**
     * Формирует .pdf-файл, записывает на диск и возвращает название файла.
     *
     * @param in       {@link InputStream} из шаблона для формирования документа.
     * @param map      промапленные значения для заполнения шаблона.
     * @param metadata метаданные шаблона.
     * @return имя сохранённого документа.
     * @throws IOException         если произошла ошибка ввода-вывода.
     * @throws XDocReportException если произошла ошибка формирования документа.
     */
    private String writeAsPdf(InputStream in, Map<String, Object> map, FieldsMetadata metadata)
        throws IOException, XDocReportException {
        try {
            File file = storageService.getNewFileForPrint();
            OutputStream out = new FileOutputStream(file);
            PrintWriter writer = new PrintWriter(out, true);

            IXDocReport report = XDocReportRegistry.getRegistry().
                loadReport(in, TemplateEngineKind.Velocity);

            if (metadata != null)
                report.setFieldsMetadata(metadata);

            Options options = Options.getTo(ConverterTypeTo.PDF).via(
                ConverterTypeVia.ODFDOM);

            IContext ctx = report.createContext();
            ctx.putMap(map);

            report.convert(ctx, options, out);
            out.close();
            writer.close();

            return file.getName();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, String.format("File system error: %s", e.getMessage()));
            throw e;
        } catch (XDocReportException e) {
            LOGGER.log(Level.SEVERE, String.format("Error of report generation: %s", e.getMessage()));
            throw e;
        }
    }

    /**
     * Сохраняет сущность распечатанного документа в бд.
     *
     * @param clazz            класс сохраняемой сущности.
     * @param consumer         метод, для передачи печатаемого обхекта в сущность.
     * @param object           печатаемый объект.
     * @param fileName         имя файла в файловой системе.
     * @param originalFileName имя файла для отображения клиенту.
     * @param <T>              тип сущности с информацией о распечатанной сущности.
     * @param <U>              распечатанная сущность.
     */
    private <T extends PrintedFile, U extends AbstractIdEntity> void savePrintedEntity(
        Class<T> clazz, BiConsumer<T, U> consumer, U object, String fileName, String originalFileName) {

        try {
            T printedFile = clazz.newInstance();
            consumer.accept(printedFile, object);
            printedFile.setDateTime(Instant.now());
            printedFile.setUser(user);
            printedFile.setFileName(fileName);
            printedFile.setDisplayName(originalFileName);

            printedFileService.save(printedFile);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, String.format("Printed file %s creation error", clazz.getName()));
        }
    }

    /**
     * Формирует данные для вставки изображения в документ из сканированного образа
     *
     * @param scannedImageFileName - имя файла
     * @param map             - ссылка на мапу с данными для печати
     * @param imageMark       - текстовая метка изображения, которая присваивается шаблону изображения в самом документе
     * @param fieldsMetadata  - ссылка на структуру данных для заполнения и передачи при формировании документа
     */
    private void setImageData(String scannedImageFileName, Map<String, Object> map,
                              String imageMark, FieldsMetadata fieldsMetadata) {
        if (scannedImageFileName != null) {
            try {
                File file = storageService.getScannedFile(scannedImageFileName).getSourceFile();
                if (file.exists()) {
                    fieldsMetadata.addFieldAsImage(imageMark);
                    IImageProvider imageProvider = new FileImageProvider(file, false);
                    map.put(imageMark, imageProvider);
                }
            } catch (FileSystemErrorException e) {
                e.printStackTrace();
            }
        }
    }
}

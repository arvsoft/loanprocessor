package ru.loanpro.base.requesthistory.domain;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.directory.RequestStatus;
import ru.loanpro.security.domain.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

/**
 * История изменения заявок
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "request_history", schema = DatabaseSchema.LOAN)
public class RequestHistory extends AbstractIdEntity {

    /**
     * Статус заявки, справочные данные
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "request_status", nullable = false)
    private RequestStatus requestStatus;

    /**
     * Дата оформления заявки
     */
    @Column(name = "decision_date", nullable = false)
    private Instant decisionDate;

    /**
     * Основание решение
     */
    @Column(name = "basement")
    private String basement;

    /**
     * Ссылка на заявку.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", nullable = false)
    private Request request;

    /**
     * Зарегистрированный займ.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id")
    private Loan loan;

    /**
     * Пользователь, принявший решение.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public RequestHistory() {
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Instant getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Instant decisionDate) {
        this.decisionDate = decisionDate;
    }

    public String getBasement() {
        return basement;
    }

    public void setBasement(String basement) {
        this.basement = basement;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

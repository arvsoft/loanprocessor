package ru.loanpro.base.storage.domain.connector;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.global.DatabaseSchema;

/**
 * Сущность распечатанного документа о займе.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("PRINTED_LOAN_DOCUMENT")
@SecondaryTable(name = DatabaseSchema.Loan.LOANS_PRINTED, schema = DatabaseSchema.LOAN,
    pkJoinColumns = @PrimaryKeyJoinColumn(name = "printed_file_id"))
public class LoanPrintedFile extends PrintedFile {

    /**
     * Распечатанный займ.
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(table = DatabaseSchema.Loan.LOANS_PRINTED, name = "loan_id")
    private Loan loan;

    public LoanPrintedFile() {
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
}

package ru.loanpro.base.report.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.base.report.repository.ReportRepository;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.security.domain.Department;

import java.time.Instant;
import java.util.Currency;
import java.util.List;

/**
 * Cервис работы с отчетами
 *
 * @author Maksim Askhaev
 */
@Service
public class ReportService {

    private final ReportRepository repository;

    @Autowired
    public ReportService(ReportRepository repository) {
        this.repository = repository;
    }

    public List<Report> findAll() {
        return repository.findAll();
    }

    //****************************************************************************************************************//
    public List<Loan> findOverdueLoans() {
        return repository.findOverdueLoans();
    }

    public List<Loan> findOverdueLoansByDepartment(Department department) {
        return repository.findOverdueLoansByDepartment(department);
    }

    public List<Loan> findOverdueLoansByDates(Instant min, Instant max) {
        return repository.findOverdueLoansByDates(min, max);
    }

    public List<Loan> findOverdueLoansByDepartmentAndDates(Department department, Instant min, Instant max) {
        return repository.findOverdueLoansByDepartmentAndDates(department, min, max);
    }

    //****************************************************************************************************************//
    public List<Currency> findDistinctCurrency() {
        return repository.findDistinctCurrency();
    }

    public List<Currency> findDistinctCurrencyByDepartment(Department department) {
        return repository.findDistinctCurrencyByDepartment(department);
    }

    public List<Currency> findDistinctCurrencyByDates(Instant min, Instant max) {
        return repository.findDistinctCurrencyByDates(min, max);
    }

    public List<Currency> findDistinctCurrencyByDepartmentAndDates(Department department, Instant min, Instant max) {
        return repository.findDistinctCurrencyByDepartmentAndDates(department, min, max);
    }

    //****************************************************************************************************************//
    public List<Loan> findAllLoansByCurrencyAndStatus(Currency currency, LoanStatus status) {
        return repository.findAllLoansByCurrencyAndStatus(currency, status);
    }

    public List<Loan> findAllLoansByCurrencyAndStatusAndDepartment(
        Currency currency, LoanStatus status, Department department) {
        return repository.findAllLoansByCurrencyAndStatusAndDepartment(currency, status, department);
    }

    public List<Loan> findAllLoansByCurrencyAndStatusAndDates(
        Currency currency, LoanStatus status, Instant min, Instant max) {
        return repository.findAllLoansByCurrencyAndStatusAndDates(currency, status, min, max);
    }

    public List<Loan> findAllLoansByCurrencyAndStatusAndDepartmentAndDates(
        Currency currency, LoanStatus status, Department department, Instant min, Instant max) {
        return repository.findAllLoansByCurrencyAndStatusAndDepartmentAndDates(currency, status, department, min, max);
    }

    //****************************************************************************************************************//
    public List<PhysicalClient> findAllClientsByRating(ClientRating rating, PageRequest pageRequest){
        return repository.findAllClientsByRating(rating, pageRequest);
    }

    public int countFindAllClientsByRating(ClientRating rating){
        return repository.countFindAllClientsByRating(rating);
    }
    //****************************************************************************************************************//

}

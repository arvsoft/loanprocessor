package ru.loanpro.base.storage.domain;

import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;

import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.security.domain.User;

/**
 * Базовая сущность файла в хранилище файлов.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = DatabaseSchema.Core.STORAGE_FILE, schema = DatabaseSchema.CORE)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "entity_type",
    discriminatorType = DiscriminatorType.STRING)
public class BaseStorageFile extends AbstractIdEntity {

    /**
     * Имя файла на диске.
     */
    @Column(name = "file_name", nullable = false)
    private String fileName;

    /**
     * Оригинальное имя для отображения клиенту.
     */
    @Column(name = "display_name", nullable = false)
    private String displayName;

    /**
     * Пользователь, загрузивший или создавший файл.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * Дата-время загрузки или создания файла.
     */
    @Column(name = "date_time", nullable = false)
    private Instant dateTime;

    public BaseStorageFile() {
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instant getDateTime() {
        return dateTime;
    }

    public void setDateTime(Instant dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BaseStorageFile that = (BaseStorageFile) o;
        boolean equal = Objects.equals(fileName, that.fileName) &&
            Objects.equals(displayName, that.displayName) &&
            Objects.equals(dateTime, that.dateTime);

        if (Hibernate.isInitialized(user))
            equal = equal && Objects.equals(user, that.user);

        return equal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            super.hashCode(),
            fileName,
            displayName,
            Hibernate.isInitialized(user)
                ? user
                : null,
            dateTime);
    }
}

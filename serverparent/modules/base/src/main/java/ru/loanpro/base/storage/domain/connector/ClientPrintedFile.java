package ru.loanpro.base.storage.domain.connector;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.global.DatabaseSchema;

/**
 * Сущность распечатанного документа о клиенте.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("PRINTED_CLIENT_DOCUMENT")
@SecondaryTable(name = DatabaseSchema.Client.CLIENTS_PRINTED, schema = DatabaseSchema.CLIENT,
    pkJoinColumns = @PrimaryKeyJoinColumn(name = "printed_file_id"))
public class ClientPrintedFile extends PrintedFile {

    /**
     * Распечатанный клиент.
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(table = DatabaseSchema.Client.CLIENTS_PRINTED, name = "client_id")
    private Client client;

    public ClientPrintedFile() {
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}

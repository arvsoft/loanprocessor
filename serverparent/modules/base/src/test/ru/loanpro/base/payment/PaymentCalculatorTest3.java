package ru.loanpro.base.payment;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.schedule.Calculator;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.PaymentStatus;
import ru.loanpro.global.directory.PenaltyType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.directory.ScheduleStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Тестирование класса расчета фактического платежа - PaymentCalculator
 * Тестирование для аннуитетного графика типа "ежемесячно"
 *
 * @author Maksim Askhaev
 */
public class PaymentCalculatorTest3 {

        Calculator calculator;
        PaymentCalculator paymentCalculator;
        List<Schedule> schedules;
        Loan loan;
        Payment expected;

        int scaleMode = BigDecimal.ROUND_HALF_EVEN;
        int scaleValue = 2;

        /**
         * Общий метод, выполняемый перед каждым тестовым методом
         * Общие параметры:
         * Дата выдачи займа - 01.01.2018
         * Схема расчета - аннуитетный.
         * Срок - 10 месяцев, периодичность - ежемесячно.
         * Размер процентов - 10% в год. Сумма займа - 1000 рублей.
         */
        @Before
        public void setUp() {
            loan = new Loan();
            loan.setId(UUID.randomUUID());

            calculator = Calculator.instance()
                .setScaleValue(scaleValue)
                .setScaleMode(scaleMode)
                .setLoanIssueDate(LocalDate.of(2018, 1, 1))
                .setLoanBalance(new BigDecimal(1000.00))
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false)
                .setPrincipalLastPay(false)
                .setInterestBalance(false)
                .setNoInterestFD(false)
                .setNoInterestFDValue(0)
                .setIncludeIssueDate(false)
                .setDaysInYear(365)
                .setCalcScheme(CalcScheme.ANNUITY)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(10))
                .setInterestRateType(InterestType.PERCENT_A_YEAR)
                .setLoanTerm(10)
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false)
                .build();
            schedules = calculator.calculate();
            schedules.forEach(e -> e.setLoan(loan));

            paymentCalculator = PaymentCalculator.instance()
                .setScaleValue(scaleValue)
                .setScaleMode(scaleMode)
                .setSummationFines(true)
                .setLoanIssueDate(LocalDate.of(2018, 1, 1))
                .setLoanBalance(new BigDecimal(1000.00))
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false)
                .setPrincipalLastPay(false)
                .setInterestBalance(false)
                .setNoInterestFD(false)
                .setNoInterestFDValue(0)
                .setIncludeIssueDate(false)
                .setDaysInYear(365)
                .setCalcScheme(CalcScheme.ANNUITY)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(10))
                .setInterestRateType(InterestType.PERCENT_A_YEAR)
                .setLoanTerm(10)
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false);

            expected = new Payment();
            expected.setPrincipalB(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setInterestB(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setFineB(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPenaltyB(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPaymentB(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPrincipalE(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setInterestE(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setFineE(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPenaltyE(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPaymentE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        }

        /**
         * Тест 1: проверяем второй платеж
         * Просрочка есть, оплата не по графику - на 02.03.2018
         * Тип неустойки - НЕ ОПРЕДЕЛЕН
         * Штраф - отсутствует
         * Проценты строго по графику - отключены
         * Проценты в просроченный период - включены
         * Наличие предыдущих фактических платежей - один, по графику
         */
        @Test
        public void testCalculate1() {
            //создаем один платеж
            List<Payment> payments = new ArrayList<>();
            Payment payment = new Payment();
            payment.setScheduleOrdNo(1);
            payment.setPayDate(LocalDate.of(2018, 2, 1));
            payment.setTerm(31);
            payment.setPrincipal(new BigDecimal(96.15));
            payment.setInterest(new BigDecimal(8.49));
            payment.setPenalty(new BigDecimal(0));
            payment.setFine(new BigDecimal(0));
            payment.setPayment(new BigDecimal(104.64));
            payment.setBalanceAfter(new BigDecimal(903.85));
            payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
            payments.add(payment);
            payments.forEach(e -> e.setLoan(loan));

            expected.setPayDate(LocalDate.of(2018,3,2));
            expected.setOrdNo(2);
            expected.setScheduleOrdNo(2);
            expected.setTerm(28);
            expected.setOverdueTerm(1);
            expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
            expected.setPrincipal(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
            expected.setInterest(new BigDecimal(7.18).setScale(scaleValue, scaleMode));
            expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPenalty(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPayment(new BigDecimal(104.89).setScale(scaleValue, scaleMode));
            expected.setBalanceAfter(new BigDecimal(806.14).setScale(scaleValue, scaleMode));
            expected.setPrincipalC(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
            expected.setInterestC(new BigDecimal(7.18).setScale(scaleValue, scaleMode));
            expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPenaltyC(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPaymentC(new BigDecimal(104.89).setScale(scaleValue, scaleMode));
            expected.setPrincipalS(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
            expected.setInterestS(new BigDecimal(7.18).setScale(scaleValue, scaleMode));
            expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPenaltyS(new BigDecimal(0).setScale(scaleValue, scaleMode));
            expected.setPaymentS(new BigDecimal(104.89).setScale(scaleValue, scaleMode));

            paymentCalculator
                .setCalculationDate(LocalDate.of(2018, 3, 2))
                .setPenaltyType(PenaltyType.NOT_DEFINED)
                .setPenaltyValue(new BigDecimal(0))
                .setFine(false)
                .setFineValue(new BigDecimal(0))
                .setFineOneTime(false)
                .setInterestStrictly(false)
                .setInterestOverdue(true)
                .build();

            schedules.stream().filter(item->item.getOrdNo()==1).forEach(item->item.setStatus(ScheduleStatus.PAID_FULLY));
            schedules.stream().filter(item->item.getOrdNo()==2).forEach(item->item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
            paymentCalculator.setSchedules(schedules);
            paymentCalculator.setPayments(payments);
            Payment actual = paymentCalculator.calculate();

            Assert.assertEquals(expected, actual);
        }

    /**
     * Тест 2: проверяем второй платеж
     * Просрочка есть, оплата не по графику - на 02.03.2018
     * Тип неустойки - ОТ СУММЫ ЗАЙМА
     * Штраф - отсутствует
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - один, оплачен раньше срока, 20.01.2018
     */
    @Test
    public void testCalculate2() {
        //создаем один платеж
        List<Payment> payments = new ArrayList<>();
        Payment payment = new Payment();
        payment.setScheduleOrdNo(1);
        payment.setPayDate(LocalDate.of(2018, 1, 20));
        payment.setTerm(31);
        payment.setPrincipal(new BigDecimal(96.15));
        payment.setInterest(new BigDecimal(8.49));
        payment.setPenalty(new BigDecimal(0));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(104.64));
        payment.setBalanceAfter(new BigDecimal(903.85));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        payments.add(payment);
        payments.forEach(e -> e.setLoan(loan));

        expected.setPayDate(LocalDate.of(2018, 3, 2));
        expected.setOrdNo(2);
        expected.setScheduleOrdNo(2);
        expected.setTerm(28);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(6.93).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(10.00).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(114.64).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(806.14).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(6.93).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(10.00).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(114.64).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(6.93).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(10.00).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(114.64).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 3, 2))
            .setPenaltyType(PenaltyType.FROM_LOAN_VALUE)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(false)
            .setFineValue(new BigDecimal(0))
            .setFineOneTime(false)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();

        schedules.stream().filter(item->item.getOrdNo()==1).forEach(item->item.setStatus(ScheduleStatus.PAID_FULLY));
        schedules.stream().filter(item->item.getOrdNo()==2).forEach(item->item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(payments);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 3: проверяем второй платеж
     * Просрочка есть, оплата не по графику - на 02.03.2018
     * Тип неустойки - ОТ ПЛАТЕЖА
     * Штраф - отсутствует
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - один, оплачен позже срока, 10.02.2018
     */
    @Test
    public void testCalculate3() {
        //создаем один платеж
        List<Payment> payments = new ArrayList<>();
        Payment payment = new Payment();
        payment.setOrdNo(1);
        payment.setScheduleOrdNo(1);
        payment.setPayDate(LocalDate.of(2018, 2, 10));
        payment.setTerm(31);
        payment.setPrincipal(new BigDecimal(96.15));
        payment.setInterest(new BigDecimal(8.49));
        payment.setPenalty(new BigDecimal(9.42));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(114.04));
        payment.setBalanceAfter(new BigDecimal(903.85));
        payment.setPrincipalE(new BigDecimal(0));
        payment.setInterestE(new BigDecimal(0));
        payment.setFineE(new BigDecimal(0));
        payment.setPenaltyE(new BigDecimal(0));
        payment.setPaymentE(new BigDecimal(0));

        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        payments.add(payment);
        payments.forEach(e -> e.setLoan(loan));

        expected.setPayDate(LocalDate.of(2018, 2, 20));
        expected.setOrdNo(2);
        expected.setScheduleOrdNo(2);
        expected.setTerm(28);
        expected.setOverdueTerm(0);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(6.93).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(104.64).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(806.14).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(6.93).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(0.00).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(104.64).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(97.71).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(6.93).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(0.00).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(104.64).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 2, 20))
            .setPenaltyType(PenaltyType.FROM_PAYMENT)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(false)
            .setFineValue(new BigDecimal(0))
            .setFineOneTime(false)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();

        schedules.stream().filter(item->item.getOrdNo()==1).forEach(item->item.setStatus(ScheduleStatus.PAID_FULLY));
        schedules.stream().filter(item->item.getOrdNo()==2).forEach(item->item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(payments);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 4: проверяем второй платеж для первого рассчитанного платежа
     * Просрочки есть, в первом платеже оплата не по графику - 02.02.2018
     * Вторая оплата (доплата) по первому рассчитанном платежу 10.02.2018
     * Тип неустойки - ОТ ОСТАТКА ДОЛГА
     * Штраф - отсутствует
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     */
    @Test
    public void testCalculate4() {
        //создаем один платеж
        List<Payment> payments = new ArrayList<>();
        Payment payment = new Payment();
        payment.setOrdNo(1);
        payment.setScheduleOrdNo(1);
        payment.setPayDate(LocalDate.of(2018, 2, 2));
        payment.setTerm(31);
        payment.setPrincipal(new BigDecimal(81.51));
        payment.setInterest(new BigDecimal(8.49));
        payment.setPenalty(new BigDecimal(10.00));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(100.00));
        payment.setBalanceAfter(new BigDecimal(918.49));
        payment.setPrincipalE(new BigDecimal(14.64));
        payment.setInterestE(new BigDecimal(0));
        payment.setFineE(new BigDecimal(0));
        payment.setPenaltyE(new BigDecimal(0));
        payment.setPaymentE(new BigDecimal(14.64));

        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_MANUAL);
        payments.add(payment);
        payments.forEach(e -> e.setLoan(loan));

        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE);
        expected.setOrdNo(2);
        expected.setScheduleOrdNo(1);
        expected.setOverdueTerm(9);
        expected.setTerm(0);
        expected.setPayDate(LocalDate.of(2018, 2, 10));
        expected.setPrincipal(new BigDecimal(14.64).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(0.00).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(1.32).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(15.96).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(903.85).setScale(scaleValue, scaleMode));
        expected.setPrincipalB(new BigDecimal(14.64).setScale(scaleValue, scaleMode));
        expected.setInterestB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentB(new BigDecimal(14.64).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(1.32).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(1.32).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(14.64).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(1.32).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(15.96).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 2, 10))
            .setPenaltyType(PenaltyType.FROM_PAYMENT)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(false)
            .setFineValue(new BigDecimal(0))
            .setFineOneTime(false)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();

        schedules.stream().filter(item->item.getOrdNo()==1).forEach(item->item.setStatus(ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT));
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(payments);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }
}

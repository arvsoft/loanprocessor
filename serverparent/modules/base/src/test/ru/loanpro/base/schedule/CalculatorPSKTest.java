package ru.loanpro.base.schedule;

import org.junit.Assert;
import org.junit.Test;
import ru.loanpro.base.schedule.domain.Schedule;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Тестирование класса расчета ПСК - CalculatorPSK
 *
 * @author Maksim Askhaev
 */
public class CalculatorPSKTest {
    /**
     * Тест выполняет расчет ПСК для графика с более чем одним платежом (точно как в примере инструкции ЦБ)
     * <p>
     * Параметры следующие:
     * Схема расчета - дифференцированный. Кол-во платежей - 3. Периодичность возврата - индивидуально
     * Размер процентов - 1% в день.
     */
    @Test
    public void testCalculate1() {
        List<Schedule> scheduleList = new ArrayList<>();

        Schedule schedulePayment = new Schedule();
        schedulePayment.setDate(LocalDate.of(2016, 6, 21));
        schedulePayment.setPayment(new BigDecimal(11500));
        scheduleList.add(schedulePayment);

        schedulePayment = new Schedule();
        schedulePayment.setDate(LocalDate.of(2016, 6, 26));
        schedulePayment.setPayment(new BigDecimal(11500));
        scheduleList.add(schedulePayment);

        schedulePayment = new Schedule();
        schedulePayment.setDate(LocalDate.of(2016, 6, 30));
        schedulePayment.setPayment(new BigDecimal(11200));
        scheduleList.add(schedulePayment);

        BigDecimal actualPSK = new CalculatorPSK(new BigDecimal(30000), LocalDate.of(2016, 6, 16), scheduleList).calculate();
        Assert.assertEquals(new BigDecimal(518.300).setScale(3, BigDecimal.ROUND_HALF_DOWN), actualPSK);
    }

    /**
     * Тест выполняет расчет ПСК для графика с одним платежом (точно как в примере инструкции ЦБ)
     * <p>
     * Параметры следующие:
     * Схема расчета - дифференцированный. Кол-во платежей - 1. Периодичность возврата - в конце срока
     * Размер процентов - 1% в день.
     */
    @Test
    public void testCalculate2() {
        List<Schedule> scheduleList = new ArrayList<>();

        Schedule schedulePayment = new Schedule();
        schedulePayment.setDate(LocalDate.of(2018, 1, 6));
        schedulePayment.setPayment(new BigDecimal(10500));
        scheduleList.add(schedulePayment);

        BigDecimal actualPSK = new CalculatorPSK(new BigDecimal(10000), LocalDate.of(2018, 1, 1), scheduleList).calculate();
        Assert.assertEquals(new BigDecimal(365.000).setScale(3, BigDecimal.ROUND_HALF_DOWN), actualPSK);
    }

    /**
     * Тест проверяет возврат значения ПСК == 0.000 при пустом графике платежей
     * и не пустой сумме займа и дате займа
     */
    @Test
    public void testCalculate3() {
        List<Schedule> scheduleList = new ArrayList<>();

        BigDecimal actualPSK = new CalculatorPSK(new BigDecimal(10000), LocalDate.of(2018, 1, 1), scheduleList).calculate();
        Assert.assertEquals(new BigDecimal(0.000).setScale(3, BigDecimal.ROUND_HALF_DOWN), actualPSK);
    }
}
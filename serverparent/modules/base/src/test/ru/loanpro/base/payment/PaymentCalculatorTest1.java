package ru.loanpro.base.payment;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.schedule.Calculator;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.PaymentStatus;
import ru.loanpro.global.directory.PenaltyType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.directory.ScheduleStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Тестирование класса расчета фактического платежа - PaymentCalculator
 * Тестирование для дифференцированного графика типа "в конце срока"
 *
 * @author Maksim Askhaev
 */
public class PaymentCalculatorTest1 {
    Calculator calculator;
    PaymentCalculator paymentCalculator;
    List<Schedule> schedules;
    Loan loan;
    Payment expected;

    int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    int scaleValue = 2;

    /**
     * Общий метод, выполняемый перед каждым тестовым методом
     * Общие параметры:
     * Дата выдачи займа - 01.01.2018
     * Схема расчета - дифференцированный. Периодичность возврата - в конце срока. Срок - 10 дней.
     * Размер процентов - 1% в день. Сумма займа - 1000 рублей.
     */
    @Before
    public void setUp() {
        loan = new Loan();
        loan.setId(UUID.randomUUID());

        calculator = Calculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setLoanIssueDate(LocalDate.of(2018, 1, 1))
            .setLoanBalance(new BigDecimal(1000.00))
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false)
            .setPrincipalLastPay(false)
            .setInterestBalance(false)
            .setNoInterestFD(false)
            .setNoInterestFDValue(0)
            .setIncludeIssueDate(false)
            .setDaysInYear(365)
            .setCalcScheme(CalcScheme.DIFFERENTIAL)
            .setReturnTerm(ReturnTerm.IN_THE_END_OF_TERM)
            .setInterestRate(new BigDecimal(1))
            .setInterestRateType(InterestType.PERCENT_A_DAY)
            .setLoanTerm(10)
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false)
            .build();
        schedules = calculator.calculate();
        schedules.forEach(e -> e.setLoan(loan));

        paymentCalculator = PaymentCalculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setSummationFines(true)
            .setLoanIssueDate(LocalDate.of(2018, 1, 1))
            .setLoanBalance(new BigDecimal(1000.00))
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false)
            .setPrincipalLastPay(false)
            .setInterestBalance(false)
            .setNoInterestFD(false)
            .setNoInterestFDValue(0)
            .setIncludeIssueDate(false)
            .setDaysInYear(365)
            .setCalcScheme(CalcScheme.DIFFERENTIAL)
            .setReturnTerm(ReturnTerm.IN_THE_END_OF_TERM)
            .setInterestRate(new BigDecimal(1))
            .setInterestRateType(InterestType.PERCENT_A_DAY)
            .setLoanTerm(10)
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false);

        expected = new Payment();
        expected.setPrincipalB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setInterestB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setInterestE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentE(new BigDecimal(0).setScale(scaleValue, scaleMode));
    }

    /**
     * Тест 1:
     * Просрочек нет, оплата по графику - на 11.01.2018
     * Тип неустойки - не определен
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - нет
     */
    @Test
    public void testCalculate1() {
        expected.setPayDate(LocalDate.of(2018, 1, 11));
        expected.setOrdNo(1);
        expected.setScheduleOrdNo(1);
        expected.setTerm(10);
        expected.setOverdueTerm(0);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        expected.setPrincipal(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(100).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(1100).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(100).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(1100).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(100).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(1100).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 1, 11))
            .setPenaltyType(PenaltyType.NOT_DEFINED)
            .setPenaltyValue(new BigDecimal(0))
            .setFine(true)
            .setFineValue(new BigDecimal(500))
            .setFineOneTime(true)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(null);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 2:
     * Просрочка 1 день, оплата - на 12.01.2018
     * Тип неустойки - не определен
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - нет
     */
    @Test
    public void testCalculate2() {
        schedules.forEach(e -> {
            if (ChronoUnit.DAYS.between(e.getDate(), LocalDate.of(2018, 1, 12)) > 0) {
                e.setStatus(ScheduleStatus.NOT_PAID_OVERDUE);
            }
        });

        expected.setPayDate(LocalDate.of(2018, 1, 12));
        expected.setOrdNo(1);
        expected.setScheduleOrdNo(1);
        expected.setTerm(10);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(100).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(1600).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(100).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(1600).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(100).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(1600).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 1, 12))
            .setPenaltyType(PenaltyType.NOT_DEFINED)
            .setPenaltyValue(new BigDecimal(0))
            .setFine(true)
            .setFineValue(new BigDecimal(500))
            .setFineOneTime(true)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(null);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 3:
     * Просрочка 1 день, оплата - на 12.01.2018
     * Тип неустойки - не определен
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - включены
     * Наличие предыдущих фактических платежей - нет
     */
    @Test
    public void testCalculate3() {
        schedules.forEach(e -> {
            if (ChronoUnit.DAYS.between(e.getDate(), LocalDate.of(2018, 1, 12)) > 0) {
                e.setStatus(ScheduleStatus.NOT_PAID_OVERDUE);
            }
        });

        expected.setPayDate(LocalDate.of(2018, 1, 12));
        expected.setOrdNo(1);
        expected.setScheduleOrdNo(1);
        expected.setTerm(10);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(110).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(1610).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(110).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(1610).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(110).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(1610).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 1, 12))
            .setPenaltyType(PenaltyType.NOT_DEFINED)
            .setPenaltyValue(new BigDecimal(0))
            .setFine(true)
            .setFineValue(new BigDecimal(500))
            .setFineOneTime(true)
            .setInterestStrictly(false)
            .setInterestOverdue(true)
            .build();
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(null);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 4:
     * Просрочка 1 день, оплата - на 12.01.2018
     * Тип неустойки - 1% от суммы займа
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - включены
     * Наличие предыдущих фактических платежей - нет
     */
    @Test
    public void testCalculate4() {
        schedules.forEach(e -> {
            if (ChronoUnit.DAYS.between(e.getDate(), LocalDate.of(2018, 1, 12)) > 0) {
                e.setStatus(ScheduleStatus.NOT_PAID_OVERDUE);
            }
        });

        expected.setPayDate(LocalDate.of(2018, 1, 12));
        expected.setOrdNo(1);
        expected.setScheduleOrdNo(1);
        expected.setTerm(10);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(110).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(10).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(1620).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(110).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(10).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(1620).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(110).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(10).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(1620).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 1, 12))
            .setPenaltyType(PenaltyType.FROM_LOAN_VALUE)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(true)
            .setFineValue(new BigDecimal(500))
            .setFineOneTime(true)
            .setInterestStrictly(false)
            .setInterestOverdue(true)
            .build();
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(null);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 6:
     * Просрочка 1 день, оплата - на 13.01.2018
     * Тип неустойки - от суммы займа, размер - 1% в день
     * Штраф - 500 рублей, не единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключена
     * Рассчитанный платеж имеет статус "оплачен частично, долг по осн. части"
     * Наличие предыдущих фактических платежей - есть один, оплачен вручную
     * Фактический платеж есть - 12.01.2018, осн. часть - 500 руб, проценты - 100 руб, неустойка - 10 руб.,
     * штраф - 500 руб., итого - 1110 руб., остаток 500 руб.
     */
    @Test
    public void testCalculate6() {
        schedules.forEach(e -> {
            e.setOrdNo(1);
            e.setStatus(ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT);
        });

        List<Payment> payments = new ArrayList<>();
        Payment payment = new Payment();
        payment.setScheduleOrdNo(1);
        payment.setPayDate(LocalDate.of(2018, 1, 12));
        payment.setTerm(11);
        payment.setPrincipal(new BigDecimal(500));
        payment.setInterest(new BigDecimal(100));
        payment.setPenalty(new BigDecimal(10));
        payment.setFine(new BigDecimal(500));
        payment.setPayment(new BigDecimal(1110));
        payment.setBalanceAfter(new BigDecimal(500));
        payment.setPrincipalE(new BigDecimal(500));
        payment.setInterestE(new BigDecimal(0));
        payment.setPenaltyE(new BigDecimal(0));
        payment.setFineE(new BigDecimal(0));
        payment.setPaymentE(new BigDecimal(0));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_MANUAL);
        payments.add(payment);
        payments.forEach(e -> e.setLoan(loan));

        expected.setPayDate(LocalDate.of(2018, 1, 12));
        expected.setOrdNo(2);
        expected.setScheduleOrdNo(1);
        expected.setTerm(0);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE);
        expected.setPrincipal(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(1000).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalB(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setInterestB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentB(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(00).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(1000).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 1, 12))
            .setPenaltyType(PenaltyType.FROM_LOAN_VALUE)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(true)
            .setFineValue(new BigDecimal(500))
            .setFineOneTime(false)
            .setInterestStrictly(false)
            .setInterestOverdue(true)
            .build();
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(payments);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }
}

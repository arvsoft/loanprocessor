package ru.loanpro.base.payment;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.schedule.Calculator;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.PaymentStatus;
import ru.loanpro.global.directory.PenaltyType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.directory.ScheduleStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Тестирование класса расчета фактического платежа - PaymentCalculator
 * Тестирование для дифференцированного графика типа "ежемесячно"
 *
 * @author Maksim Askhaev
 */
public class PaymentCalculatorTest2 {
    Calculator calculator;
    PaymentCalculator paymentCalculator;
    List<Schedule> schedules;
    Loan loan;
    Payment expected;

    int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    int scaleValue = 2;

    /**
     * Общий метод, выполняемый перед каждым тестовым методом
     * Общие параметры:
     * Дата выдачи займа - 01.01.2018
     * Схема расчета - дифференцированный.
     * Срок - 3 месяца, периодичность - ежемесячно.
     * Размер процентов - 10% в год. Сумма займа - 1000 рублей.
     */
    @Before
    public void setUp() {
        loan = new Loan();
        loan.setId(UUID.randomUUID());

        calculator = Calculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setLoanIssueDate(LocalDate.of(2018, 1, 1))
            .setLoanBalance(new BigDecimal(1000.00))
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false)
            .setPrincipalLastPay(false)
            .setInterestBalance(false)
            .setNoInterestFD(false)
            .setNoInterestFDValue(0)
            .setIncludeIssueDate(false)
            .setDaysInYear(365)
            .setCalcScheme(CalcScheme.DIFFERENTIAL)
            .setReturnTerm(ReturnTerm.MONTHLY)
            .setInterestRate(new BigDecimal(10))
            .setInterestRateType(InterestType.PERCENT_A_YEAR)
            .setLoanTerm(3)
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false)
            .build();
        schedules = calculator.calculate();
        schedules.forEach(e -> e.setLoan(loan));

        paymentCalculator = PaymentCalculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setSummationFines(true)
            .setLoanIssueDate(LocalDate.of(2018, 1, 1))
            .setLoanBalance(new BigDecimal(1000.00))
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false)
            .setPrincipalLastPay(false)
            .setInterestBalance(false)
            .setNoInterestFD(false)
            .setNoInterestFDValue(0)
            .setIncludeIssueDate(false)
            .setDaysInYear(365)
            .setCalcScheme(CalcScheme.DIFFERENTIAL)
            .setReturnTerm(ReturnTerm.MONTHLY)
            .setInterestRate(new BigDecimal(10))
            .setInterestRateType(InterestType.PERCENT_A_YEAR)
            .setLoanTerm(3)
            .setReplaceDate(false)
            .setReplaceDateValue(0)
            .setReplaceOverMonth(false);

        expected = new Payment();
        expected.setPrincipalB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setInterestB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentB(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setInterestE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setFineE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyE(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentE(new BigDecimal(0).setScale(scaleValue, scaleMode));
    }

    /**
     * Тест 1: проверяем первый платеж
     * Просрочек нет, оплата по графику, на 01.02.2018
     * Тип неустойки - НЕ ОПРЕДЕЛЕН
     * Штраф - отсутствует
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - нет
     */
    @Test
    public void testCalculate1() {
        expected.setPayDate(LocalDate.of(2018, 2, 1));
        expected.setOrdNo(1);
        expected.setScheduleOrdNo(1);
        expected.setTerm(31);
        expected.setOverdueTerm(0);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        expected.setPrincipal(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(8.49).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(341.82).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(666.67).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(8.49).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(341.82).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(8.49).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(341.82).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 2, 1))
            .setPenaltyType(PenaltyType.NOT_DEFINED)
            .setPenaltyValue(new BigDecimal(0))
            .setFine(false)
            .setFineValue(new BigDecimal(0))
            .setFineOneTime(false)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(null);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 2: проверяем первый платеж
     * Просрочка есть, оплата на один день позже срока - на 02.02.2018
     * Тип неустойки - ОТ СУММЫ ОСТАТКА
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - нет
     */
    @Test
    public void testCalculate2() {
        expected.setPayDate(LocalDate.of(2018, 2, 2));
        expected.setOrdNo(1);
        expected.setScheduleOrdNo(1);
        expected.setTerm(31);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(8.49).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(10).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(851.82).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(666.67).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(8.49).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(10).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(851.82).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(8.49).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(10).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(851.82).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 2, 2))
            .setPenaltyType(PenaltyType.FROM_BALANCE)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(true)
            .setFineValue(new BigDecimal(500))
            .setFineOneTime(true)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();
        schedules.forEach(schedule -> schedule.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(null);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 3: проверяем последний (третий) платеж
     * Просрочка есть, оплата на один день позже срока - на 02.02.2018
     * Тип неустойки - ОТ СУММЫ ОСТАТКА
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - два, по графику
     */
    @Test
    public void testCalculate3() {
        List<Payment> payments = new ArrayList<>();
        Payment payment = new Payment();
        payment.setScheduleOrdNo(1);
        payment.setPayDate(LocalDate.of(2018, 2, 1));
        payment.setTerm(31);
        payment.setPrincipal(new BigDecimal(333.33));
        payment.setInterest(new BigDecimal(8.49));
        payment.setPenalty(new BigDecimal(0));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(341.82));
        payment.setBalanceAfter(new BigDecimal(666.67));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        payments.add(payment);
        payment = new Payment();
        payment.setScheduleOrdNo(2);
        payment.setPayDate(LocalDate.of(2018, 3, 1));
        payment.setTerm(28);
        payment.setPrincipal(new BigDecimal(333.33));
        payment.setInterest(new BigDecimal(5.11));
        payment.setPenalty(new BigDecimal(0));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(338.45));
        payment.setBalanceAfter(new BigDecimal(333.33));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        payments.add(payment);
        payments.forEach(e -> e.setLoan(loan));

        expected.setPayDate(LocalDate.of(2018, 4, 2));
        expected.setOrdNo(3);
        expected.setScheduleOrdNo(3);
        expected.setTerm(31);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(2.83).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(3.33).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(839.49).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(2.83).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(3.33).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(839.49).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(2.83).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(500).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(3.33).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(839.49).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 4, 2))
            .setPenaltyType(PenaltyType.FROM_BALANCE)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(true)
            .setFineValue(new BigDecimal(500))
            .setFineOneTime(true)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();
        schedules.stream().filter(item -> item.getOrdNo() <= 2).forEach(item -> item.setStatus(ScheduleStatus.PAID_FULLY));
        schedules.stream().filter(item -> item.getOrdNo() == 3).forEach(item -> item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(payments);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 4: проверяем последний (третий) платеж
     * Просрочка есть, оплата на один день позже срока - на 02.02.2018
     * Тип неустойки - ОТ СУММЫ ПЛАТЕЖА
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - включены
     * Наличие предыдущих фактических платежей - два, по графику
     */
    @Test
    public void testCalculate4() {
        List<Payment> payments = new ArrayList<>();
        Payment payment = new Payment();
        payment.setScheduleOrdNo(1);
        payment.setPayDate(LocalDate.of(2018, 2, 1));
        payment.setTerm(31);
        payment.setPrincipal(new BigDecimal(333.33));
        payment.setInterest(new BigDecimal(8.49));
        payment.setPenalty(new BigDecimal(0));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(341.82));
        payment.setBalanceAfter(new BigDecimal(666.67));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        payments.add(payment);
        payment = new Payment();
        payment.setScheduleOrdNo(2);
        payment.setPayDate(LocalDate.of(2018, 3, 1));
        payment.setTerm(28);
        payment.setPrincipal(new BigDecimal(333.33));
        payment.setInterest(new BigDecimal(5.11));
        payment.setPenalty(new BigDecimal(0));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(338.45));
        payment.setBalanceAfter(new BigDecimal(333.33));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        payments.add(payment);
        payments.forEach(e -> e.setLoan(loan));

        expected.setPayDate(LocalDate.of(2018, 4, 2));
        expected.setOrdNo(3);
        expected.setScheduleOrdNo(3);
        expected.setTerm(32);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(2.92).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(3.36).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(339.61).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0.00).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(2.92).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(3.36).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(339.61).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(2.92).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(3.36).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(339.61).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 4, 2))
            .setPenaltyType(PenaltyType.FROM_PAYMENT)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(false)
            .setFineValue(new BigDecimal(0))
            .setFineOneTime(false)
            .setInterestStrictly(false)
            .setInterestOverdue(true)
            .build();
        schedules.stream().filter(item -> item.getOrdNo() <= 2).forEach(item -> item.setStatus(ScheduleStatus.PAID_FULLY));
        schedules.stream().filter(item -> item.getOrdNo() == 3).forEach(item -> item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(payments);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест 5 проверяем последний (третий) платеж
     * Просрочка есть, оплата на один день позже срока - на 02.02.2018
     * Тип неустойки - ОТ СУММЫ ЗАДОЛЖЕННОСТИ
     * Штраф - 500 рублей, единоразово
     * Проценты строго по графику - отключены
     * Проценты в просроченный период - отключены
     * Наличие предыдущих фактических платежей - два, по графику
     */
    @Test
    public void testCalculate5() {
        List<Payment> payments = new ArrayList<>();
        Payment payment = new Payment();
        payment.setScheduleOrdNo(1);
        payment.setPayDate(LocalDate.of(2018, 2, 1));
        payment.setTerm(31);
        payment.setPrincipal(new BigDecimal(333.33));
        payment.setInterest(new BigDecimal(8.49));
        payment.setPenalty(new BigDecimal(0));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(341.82));
        payment.setBalanceAfter(new BigDecimal(666.67));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        payments.add(payment);
        payment = new Payment();
        payment.setScheduleOrdNo(2);
        payment.setPayDate(LocalDate.of(2018, 3, 1));
        payment.setTerm(28);
        payment.setPrincipal(new BigDecimal(333.33));
        payment.setInterest(new BigDecimal(5.11));
        payment.setPenalty(new BigDecimal(0));
        payment.setFine(new BigDecimal(0));
        payment.setPayment(new BigDecimal(338.45));
        payment.setBalanceAfter(new BigDecimal(333.33));
        payment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_FULL);
        payments.add(payment);
        payments.forEach(e -> e.setLoan(loan));

        expected.setPayDate(LocalDate.of(2018, 4, 2));
        expected.setOrdNo(3);
        expected.setScheduleOrdNo(3);
        expected.setTerm(31);
        expected.setOverdueTerm(1);
        expected.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE);
        expected.setPrincipal(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterest(new BigDecimal(2.83).setScale(scaleValue, scaleMode));
        expected.setFine(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenalty(new BigDecimal(3.36).setScale(scaleValue, scaleMode));
        expected.setPayment(new BigDecimal(339.52).setScale(scaleValue, scaleMode));
        expected.setBalanceAfter(new BigDecimal(0.00).setScale(scaleValue, scaleMode));
        expected.setPrincipalC(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestC(new BigDecimal(2.83).setScale(scaleValue, scaleMode));
        expected.setFineC(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyC(new BigDecimal(3.36).setScale(scaleValue, scaleMode));
        expected.setPaymentC(new BigDecimal(339.52).setScale(scaleValue, scaleMode));
        expected.setPrincipalS(new BigDecimal(333.33).setScale(scaleValue, scaleMode));
        expected.setInterestS(new BigDecimal(2.83).setScale(scaleValue, scaleMode));
        expected.setFineS(new BigDecimal(0).setScale(scaleValue, scaleMode));
        expected.setPenaltyS(new BigDecimal(3.36).setScale(scaleValue, scaleMode));
        expected.setPaymentS(new BigDecimal(339.52).setScale(scaleValue, scaleMode));

        paymentCalculator
            .setCalculationDate(LocalDate.of(2018, 4, 2))
            .setPenaltyType(PenaltyType.FROM_SUM_OF_INDEBTEDNESS)
            .setPenaltyValue(new BigDecimal(1))
            .setFine(false)
            .setFineValue(new BigDecimal(0))
            .setFineOneTime(true)
            .setInterestStrictly(false)
            .setInterestOverdue(false)
            .build();
        schedules.stream().filter(item -> item.getOrdNo() <= 2).forEach(item -> item.setStatus(ScheduleStatus.PAID_FULLY));
        schedules.stream().filter(item -> item.getOrdNo() == 3).forEach(item -> item.setStatus(ScheduleStatus.NOT_PAID_OVERDUE));
        paymentCalculator.setSchedules(schedules);
        paymentCalculator.setPayments(payments);
        Payment actual = paymentCalculator.calculate();

        Assert.assertEquals(expected, actual);
    }
}

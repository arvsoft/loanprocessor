package ru.loanpro.base.schedule;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.global.directory.ScheduleStatus;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Тестирование класса расчета автоматического графика платежей - Calculator
 *
 * @author Maksim Askhaev
 */
public class CalculatorTest {

    Calculator calculator;
    Loan loan;

    int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    int scaleValue = 2;

    @Before
    public void setUp() {
        loan = new Loan();
        loan.setId(UUID.randomUUID());

        //настройки по умолчанию
        calculator = Calculator.instance()
                .setScaleValue(scaleValue)
                .setScaleMode(scaleMode)
                .setLoanIssueDate(LocalDate.of(2017, 12, 31))
                .setLoanBalance(new BigDecimal(10000.00))
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false)
                .setPrincipalLastPay(false)
                .setInterestBalance(false)
                .setNoInterestFD(false)
                .setNoInterestFDValue(0)
                .setIncludeIssueDate(false)
                .setDaysInYear(365);
    }

    /**
     * Тест сверяет все поля платежа в рассчитанном графике
     *
     * Параметры следующие:
     * Схема расчета - дифференцированный. Срок - 3. Периодичность возврата - ежемесячно.
     * Размер процентов - 10%, Тип процентной ставки - проценты в месяц,
     * Включены опции: 1) установка даты платежа, 2) значение даты - 10 число каждого месяца,
     * 3) перенос даты первого платежа через месяц, если срок первого платежа меньше месяца.
     */
    @Test
    public void testCalculate1() {
        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 2, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 41,
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(1000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(4333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(6666.67).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);
        schedulePayment = new Schedule(loan, 2, LocalDate.of(2018, 3, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 28,
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(666.67).setScale(scaleValue, scaleMode),
                new BigDecimal(4000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);
        schedulePayment = new Schedule(loan, 3, LocalDate.of(2018, 4, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 31,
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(3666.66).setScale(scaleValue, scaleMode),
                new BigDecimal(0.0).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setCalcScheme(CalcScheme.DIFFERENTIAL)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(10.0))
                .setInterestRateType(InterestType.PERCENT_A_MONTH)
                .setLoanTerm(3)
                .setReplaceDate(true)
                .setReplaceDateValue(10)
                .setReplaceOverMonth(true)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));
        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест сверяет все поля платежа в рассчитанном графике
     *
     * Параметры следующие:
     * Схема расчета - дифференцированный. Срок - 3. Периодичность возврата - ежемесячно.
     * Размер процентов - 10%, Тип процентной ставки - проценты в год,
     * Включены опции: 1) установка даты платежа, 2) значение даты - 10 число каждого месяца,
     * 3) перенос даты первого платежа через месяц, если срок первого платежа меньше месяца.
     */
    @Test
    public void testCalculate2() {

        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 2, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 41,
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(112.33).setScale(scaleValue, scaleMode),
                new BigDecimal(3445.66).setScale(scaleValue, scaleMode),
                new BigDecimal(6666.67).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);
        schedulePayment = new Schedule(loan, 2, LocalDate.of(2018, 3, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 28,
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(51.14).setScale(scaleValue, scaleMode),
                new BigDecimal(3384.47).setScale(scaleValue, scaleMode),
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);
        schedulePayment = new Schedule(loan, 3, LocalDate.of(2018, 4, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 31,
                new BigDecimal(3333.33).setScale(scaleValue, scaleMode),
                new BigDecimal(28.31).setScale(scaleValue, scaleMode),
                new BigDecimal(3361.64).setScale(scaleValue, scaleMode),
                new BigDecimal(0.0).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setCalcScheme(CalcScheme.DIFFERENTIAL)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(10.0))
                .setInterestRateType(InterestType.PERCENT_A_YEAR)
                .setLoanTerm(3)
                .setReplaceDate(true)
                .setReplaceDateValue(10)
                .setReplaceOverMonth(true)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест сверяет суммарные значения составляющих платежа в рассчитанном графике
     *
     * Параметры следующие:
     * Сумма займа - 1000000 рублей
     * Схема расчета - дифференцированный. Срок - 120. Периодичность возврата - ежемесячно.
     * Размер процентов - 10%, Тип процентной ставки - проценты в год,
     * Остальные опции отключены
     * Результат - расхождение менее одного рубля
     */
    @Test
    public void testCalculate3() {
        BigDecimal interest = new BigDecimal(0);
        BigDecimal principal = new BigDecimal(0);
        BigDecimal payment = new BigDecimal(0);

        List<BigDecimal> expected = new ArrayList<>();
        expected.add(new BigDecimal(999999.60).setScale(scaleValue, scaleMode));
        expected.add(new BigDecimal(504274.02).setScale(scaleValue, scaleMode));
        expected.add(new BigDecimal(1504273.62).setScale(scaleValue, scaleMode));

        calculator
                .setCalcScheme(CalcScheme.DIFFERENTIAL)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(10.0))
                .setLoanBalance(new BigDecimal(1000000.00))
                .setInterestRateType(InterestType.PERCENT_A_YEAR)
                .setLoanTerm(120)
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false)
                .build();
        List<Schedule> list = calculator.calculate();
        List<BigDecimal> actual = new ArrayList<>();

        for (Schedule lpayment : list) {
            principal = principal.add(lpayment.getPrincipal());
            interest = interest.add(lpayment.getInterest());
            payment = payment.add(lpayment.getPayment());
        }
        actual.add(principal);
        actual.add(interest);
        actual.add(payment);

        Assert.assertThat(actual, Matchers.is(expected));
    }

    /**
     * Тест сверяет все поля платежа в рассчитанном графике
     *
     * Параметры следующие:
     * Сумма займа - 10000.
     * Схема расчета - дифференцированный. Периодичность возврата - в конце срока.
     * Размер процентов - 1%, Тип процентной ставки - проценты в день,
     * Остальные опции отключены
     */

    @Test
    public void testCalculate4() {
        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 1, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 10,
                new BigDecimal(10000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(1500.00).setScale(scaleValue, scaleMode),
                new BigDecimal(11500.00).setScale(scaleValue, scaleMode),
                new BigDecimal(0.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setCalcScheme(CalcScheme.DIFFERENTIAL)
                .setReturnTerm(ReturnTerm.IN_THE_END_OF_TERM)
                .setInterestRate(new BigDecimal(1.5))
                .setInterestRateType(InterestType.PERCENT_A_DAY)
                .setLoanTerm(10)
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест сверяет все поля платежа в рассчитанном графике (см. опцию)
     *
     * Параметры следующие:
     * Сумма займа 10000
     * Схема расчета - дифференцированный. Периодичность возврата - в конце срока.
     * Размер процентов - 1%, Тип процентной ставки - проценты в день,
     * Включены опции: не вычислять проценты для первых N дней, кол-во дней - 1
     */

    @Test
    public void testCalculate5() {
        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 1, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 10,
                new BigDecimal(10000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(1350.00).setScale(scaleValue, scaleMode),
                new BigDecimal(11350.00).setScale(scaleValue, scaleMode),
                new BigDecimal(0.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setCalcScheme(CalcScheme.DIFFERENTIAL)
                .setReturnTerm(ReturnTerm.IN_THE_END_OF_TERM)
                .setInterestRate(new BigDecimal(1.5))
                .setInterestRateType(InterestType.PERCENT_A_DAY)
                .setLoanTerm(10)
                .setReplaceDate(false)
                .setReplaceDateValue(0)
                .setReplaceOverMonth(false)
                .setNoInterestFD(true)
                .setNoInterestFDValue(1)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));

        Assert.assertEquals(expected, actual);
    }

    /**
     * Тест сверяет первый и последний платеж в рассчитанном графике (см. опцию)
     * Особая опция - включить возврат основной части полностью в последний платеж
     *
     * Параметры следующие:
     * Сумма займа 100000
     * Схема расчета - дифференцированный. Периодичность возврата - еженедельно.
     * Кол-во платежей - 10
     * Размер процентов - 1%, Тип процентной ставки - проценты в неделю,
     * Включены опции: включить возврат основной части полностью в последний платеж
     */

    @Test
    public void testCalculate6() {
        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 1, 7), ScheduleStatus.NOT_PAID_NO_OVERDUE, 7,
                new BigDecimal(0.00).setScale(scaleValue, scaleMode),
                new BigDecimal(1000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(1000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(100000.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);
        schedulePayment = new Schedule(loan, 10, LocalDate.of(2018, 3, 11), ScheduleStatus.NOT_PAID_NO_OVERDUE, 7,
                new BigDecimal(100000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(1000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(101000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(0.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setLoanBalance(new BigDecimal(100000.00))
                .setCalcScheme(CalcScheme.DIFFERENTIAL)
                .setReturnTerm(ReturnTerm.WEEKLY)
                .setInterestRate(new BigDecimal(1.0))
                .setInterestRateType(InterestType.PERCENT_A_WEEK)
                .setLoanTerm(10)
                .setPrincipalLastPay(true)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));

        List<Schedule> actual2 = new ArrayList<>();
        actual2.add(actual.get(0));
        actual2.add(actual.get(9));
        actual2.forEach(e->e.setLoan(loan));

        Assert.assertEquals(expected, actual2);
    }

    /**
     * Тест сверяет первый и последний платеж в рассчитанном графике (см. опцию)
     * Особая опция - включить проценты в каждом платеже вычисляются от суммы займа
     *
     * Параметры следующие:
     * Сумма займа 200000
     * Схема расчета - дифференцированный. Периодичность возврата - ежемесячно.
     * Кол-во платежей - 10
     * Размер процентов - 5%, Тип процентной ставки - проценты в месяц,
     * Включены опции: расчет процентов в каждом платеже от суммы займа
     */

    @Test
    public void testCalculate7() {
        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 1, 31), ScheduleStatus.NOT_PAID_NO_OVERDUE, 31,
                new BigDecimal(20000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(10000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(30000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(180000.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        schedulePayment = new Schedule(loan, 10, LocalDate.of(2018, 10, 31), ScheduleStatus.NOT_PAID_NO_OVERDUE, 31,
                new BigDecimal(20000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(10000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(30000.00).setScale(scaleValue, scaleMode),
                new BigDecimal(0.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setLoanBalance(new BigDecimal(200000.00))
                .setCalcScheme(CalcScheme.DIFFERENTIAL)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(5.0))
                .setInterestRateType(InterestType.PERCENT_A_MONTH)
                .setLoanTerm(10)
                .setInterestBalance(true)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));
        List<Schedule> actual2 = new ArrayList<>();
        actual2.add(actual.get(0));
        actual2.add(actual.get(9));
        actual2.forEach(e->e.setLoan(loan));

        Assert.assertEquals(expected, actual2);
    }

    /**
     * Тест сверяет первый и последний платеж в рассчитанном графике (см. опцию)
     * Особая опция - аннутитетный график
     *
     * Параметры следующие:
     * Сумма займа 2000000
     * Схема расчета - аннуитетный. Периодичность возврата - ежемесячно.
     * Кол-во платежей - 180
     * Размер процентов - 10%, Тип процентной ставки - проценты в год,
     * Отключены дополнительные опции
     */

    @Test
    public void testCalculate8() {
        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 1, 31), ScheduleStatus.NOT_PAID_NO_OVERDUE, 31,
                new BigDecimal(4505.80).setScale(scaleValue, scaleMode),
                new BigDecimal(16986.30).setScale(scaleValue, scaleMode),
                new BigDecimal(21492.10).setScale(scaleValue, scaleMode),
                new BigDecimal(1995494.20).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);
        schedulePayment = new Schedule(loan, 180, LocalDate.of(2032, 12, 31), ScheduleStatus.NOT_PAID_NO_OVERDUE, 31,
                new BigDecimal(22700.50).setScale(scaleValue, scaleMode),
                new BigDecimal(192.80).setScale(scaleValue, scaleMode),
                new BigDecimal(22893.30).setScale(scaleValue, scaleMode),
                new BigDecimal(0.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setLoanBalance(new BigDecimal(2000000.00))
                .setCalcScheme(CalcScheme.ANNUITY)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(10.0))
                .setInterestRateType(InterestType.PERCENT_A_YEAR)
                .setLoanTerm(180)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));
        List<Schedule> actual2 = new ArrayList<>();
        actual2.add(actual.get(0));
        actual2.add(actual.get(179));
        actual2.forEach(e->e.setLoan(loan));

        Assert.assertEquals(expected, actual2);
    }

    /**
     * Тест сверяет первый и последний платеж в рассчитанном графике (см. опцию)
     * Особая опция - аннутитетный график с установкой даты первого платежа + перенос даты
     *
     * Параметры следующие:
     * Сумма займа 2000000
     * Схема расчета - аннуитетный. Периодичность возврата - ежемесячно.
     * Кол-во платежей - 180
     * Размер процентов - 10%, Тип процентной ставки - проценты в год,
     * Дополнительные опции: установка даты на 10 число + перенос даты
     */

    @Test
    public void testCalculate9() {
        List<Schedule> expected = new ArrayList<>();

        Schedule schedulePayment = new Schedule(loan, 1, LocalDate.of(2018, 2, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 41,
                new BigDecimal(4505.80).setScale(scaleValue, scaleMode),
                new BigDecimal(22465.75).setScale(scaleValue, scaleMode),
                new BigDecimal(26971.55).setScale(scaleValue, scaleMode),
                new BigDecimal(1995494.20).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);
        schedulePayment = new Schedule(loan, 180, LocalDate.of(2033, 1, 10), ScheduleStatus.NOT_PAID_NO_OVERDUE, 31,
                new BigDecimal(22700.50).setScale(scaleValue, scaleMode),
                new BigDecimal(192.80).setScale(scaleValue, scaleMode),
                new BigDecimal(22893.30).setScale(scaleValue, scaleMode),
                new BigDecimal(0.00).setScale(scaleValue, scaleMode)
        );
        expected.add(schedulePayment);

        calculator
                .setLoanBalance(new BigDecimal(2000000.00))
                .setCalcScheme(CalcScheme.ANNUITY)
                .setReturnTerm(ReturnTerm.MONTHLY)
                .setInterestRate(new BigDecimal(10.0))
                .setInterestRateType(InterestType.PERCENT_A_YEAR)
                .setLoanTerm(180)
                .setReplaceDate(true)
                .setReplaceDateValue(10)
                .setReplaceOverMonth(true)
                .build();
        List<Schedule> actual = calculator.calculate();
        actual.forEach(e->e.setLoan(loan));
        List<Schedule> actual2 = new ArrayList<>();
        actual2.add(actual.get(0));
        actual2.add(actual.get(179));
        actual2.forEach(e->e.setLoan(loan));

        Assert.assertEquals(expected, actual2);
    }

    @Test
    public void test()
            throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Integer a = get(Integer.class, "10");
        Long b = get(Long.class, "20");
        Double c = get(Double.class, "30.001");
        BigDecimal d = get(BigDecimal.class, "40.25");
    }

    public <O extends Object> O get(Class<O> aClass, String key)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return aClass.getDeclaredConstructor(String.class).newInstance(key);
        /*if (aClass.equals(String.class))
            return (O) key;
        else if (aClass.equals(Integer.class))
            return (O) new Integer(key);
        else if (aClass.equals(Long.class))
            return (O) new Long(key);
        else
            return null;*/
    }
}

package ru.loanpro.eventbus;

import com.google.common.eventbus.EventBus;
import org.springframework.stereotype.Service;

/**
 * Шина событий уровня приложения.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class ApplicationEventBus extends EventBus {}

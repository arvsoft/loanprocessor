package ru.loanpro.eventbus;

import com.google.common.eventbus.EventBus;
import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.stereotype.Component;

/**
 * Шина событий уровня сессии пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@VaadinSessionScope
public class SessionEventBus extends EventBus {}

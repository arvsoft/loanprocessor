package ru.loanpro.eventbus;

import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Конфигурация всех типов шины событий в системе.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
public class EventBusConfiguration {

    /**
     * Конфигурация шины событий уровня приложения.
     *
     * @return Шина событий уровня приложения.
     */
    @Bean
    public ApplicationEventBus getApplicationEventBus() {
        return new ApplicationEventBus();
    }

    /**
     * Конфигурация шины событий уровня сессии пользователя.
     *
     * @return Шина событий уровня сессии пользователя.
     */
    @Bean
    @VaadinSessionScope
    public SessionEventBus getSessionEventBus() {
        return new SessionEventBus();
    }
}

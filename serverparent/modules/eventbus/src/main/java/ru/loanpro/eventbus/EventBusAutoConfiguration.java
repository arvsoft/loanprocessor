package ru.loanpro.eventbus;

import com.vaadin.spring.annotation.VaadinSessionScope;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Автоматическая конфигурация шины событий.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
public class EventBusAutoConfiguration {

    /**
     * Инициализация бина шины событий уровня сессии пользователя.
     *
     * @return бин шины событий.
     */
    @Bean
    @ConditionalOnMissingBean
    @VaadinSessionScope
    public SessionEventBus sessionEventBus() {
        return new SessionEventBus();
    }

    /**
     * Инициализация бина шины событий уровня приложения.
     *
     * @return бин шины событий.
     */
    @Bean
    @ConditionalOnMissingBean
    public ApplicationEventBus applicationEventBus() {
        return new ApplicationEventBus();
    }
}

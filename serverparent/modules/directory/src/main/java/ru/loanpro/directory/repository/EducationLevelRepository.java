package ru.loanpro.directory.repository;

import ru.loanpro.directory.domain.EducationLevel;

public interface EducationLevelRepository extends BaseRepository<EducationLevel> {
}

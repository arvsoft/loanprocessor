package ru.loanpro.directory.repository;

import ru.loanpro.directory.domain.DocumentType;

public interface DocumentTypeRepository extends BaseRepository<DocumentType> {
}

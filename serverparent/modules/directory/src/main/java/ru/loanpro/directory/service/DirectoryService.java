package ru.loanpro.directory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.directory.domain.AbstractIdDirectory;
import ru.loanpro.directory.repository.BaseRepository;

import java.util.List;

/**
 * Базовый класс для сервисов
 *
 * @author Oleg Brizhevatikh
 */
@NoRepositoryBean
public class DirectoryService<D extends AbstractIdDirectory, R extends BaseRepository<D>>  {

    @Autowired
    private R repository;

    public D getOne(Long id){
        return (D)repository.getOne(id);
    }

    public List<D> getAll(){
        return (List<D>)repository.findAll();
    }

    public List<D> getByName(String name){
        return (List<D>) repository.findAll();
    }

    public D save(D d){
        return repository.save(d);
    }

    public void delete(Long id){
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e){
            e.printStackTrace();
        }
    }
}

package ru.loanpro.directory.repository;

import ru.loanpro.directory.domain.RelativeType;

public interface RelativeTypeRepository extends BaseRepository<RelativeType> {
}

package ru.loanpro.directory.service;

import org.springframework.stereotype.Service;
import ru.loanpro.directory.domain.LoanType;
import ru.loanpro.directory.repository.LoanTypeRepository;

/**
 * Сервис Типы займов
 *
 * @author Maksim Askhaev
 */
@Service
public class LoanTypeService extends DirectoryService<LoanType, LoanTypeRepository> {
}

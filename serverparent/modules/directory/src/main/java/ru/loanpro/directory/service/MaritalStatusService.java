package ru.loanpro.directory.service;

import org.springframework.stereotype.Service;
import ru.loanpro.directory.domain.MaritalStatus;
import ru.loanpro.directory.repository.MaritalStatusRepository;

/**
 * Сервис Семейное положение
 *
 * @author Maksim Askhaev
 */
@Service
public class MaritalStatusService extends DirectoryService<MaritalStatus, MaritalStatusRepository> {
}

package ru.loanpro.directory.service;

import org.springframework.stereotype.Service;
import ru.loanpro.directory.domain.EducationLevel;
import ru.loanpro.directory.repository.EducationLevelRepository;

/**
 * Сервис Образование
 *
 * @author Maksim Askhaev
 */
@Service
public class EducationLevelService extends DirectoryService<EducationLevel, EducationLevelRepository> {
}

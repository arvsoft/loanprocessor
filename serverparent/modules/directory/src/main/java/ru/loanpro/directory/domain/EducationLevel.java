package ru.loanpro.directory.domain;

import ru.loanpro.global.DatabaseSchema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Сущность Образование
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "education_level", schema = DatabaseSchema.DIRECTORY)
public class EducationLevel extends AbstractIdDirectory {
    @Column(name = "level")
    private int level;

    public EducationLevel() {
    }

    public EducationLevel(String name, int level) {
        super(name);
        this.level = level;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }
}

package ru.loanpro.directory.repository;

import ru.loanpro.directory.domain.LoanType;

public interface LoanTypeRepository extends BaseRepository<LoanType> {
}

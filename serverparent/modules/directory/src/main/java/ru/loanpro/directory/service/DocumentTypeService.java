package ru.loanpro.directory.service;

import org.springframework.stereotype.Service;
import ru.loanpro.directory.domain.DocumentType;
import ru.loanpro.directory.repository.DocumentTypeRepository;

/**
 * Сервис Типы документов
 *
 * @author Maksim Askhaev
 */
@Service
public class DocumentTypeService extends DirectoryService<DocumentType, DocumentTypeRepository> {
}

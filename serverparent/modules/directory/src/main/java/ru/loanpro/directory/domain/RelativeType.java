package ru.loanpro.directory.domain;

import ru.loanpro.global.DatabaseSchema;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Сущность Отношения
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "relative_type", schema = DatabaseSchema.DIRECTORY)
public class RelativeType extends AbstractIdDirectory {
    public RelativeType() {
    }
}

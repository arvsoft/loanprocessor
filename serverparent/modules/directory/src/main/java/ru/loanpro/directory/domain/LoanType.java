package ru.loanpro.directory.domain;

import ru.loanpro.global.DatabaseSchema;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Сущность Типы займов
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "loan_type", schema = DatabaseSchema.DIRECTORY)
public class LoanType extends AbstractIdDirectory {
    public LoanType() {
    }
}

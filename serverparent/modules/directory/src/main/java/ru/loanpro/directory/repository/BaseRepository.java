package ru.loanpro.directory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.directory.domain.AbstractIdDirectory;

@NoRepositoryBean
public interface BaseRepository<D extends AbstractIdDirectory> extends JpaRepository<D, Long> {
}

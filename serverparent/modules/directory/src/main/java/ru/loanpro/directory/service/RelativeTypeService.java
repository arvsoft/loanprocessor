package ru.loanpro.directory.service;

import org.springframework.stereotype.Service;
import ru.loanpro.directory.domain.RelativeType;
import ru.loanpro.directory.repository.RelativeTypeRepository;

/**
 * Сервис Отношения
 *
 * @author Maksim Askhaev
 */
@Service
public class RelativeTypeService extends DirectoryService<RelativeType, RelativeTypeRepository> {
}

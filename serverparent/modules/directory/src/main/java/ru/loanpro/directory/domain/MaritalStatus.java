package ru.loanpro.directory.domain;

import ru.loanpro.global.DatabaseSchema;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Сущность Семейное положение
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "marital_status", schema = DatabaseSchema.DIRECTORY)
public class MaritalStatus extends AbstractIdDirectory {
    public MaritalStatus() {
    }
}

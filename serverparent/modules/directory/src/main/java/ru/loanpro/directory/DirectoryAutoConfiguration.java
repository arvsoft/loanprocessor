package ru.loanpro.directory;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Автоматическая конфигурация справочников.
 *
 * @author Oleg Brizhevatihk
 */
@Configuration
@EntityScan(basePackages = "ru.loanpro.directory.domain")
@EnableJpaRepositories(basePackages = "ru.loanpro.directory.repository")
@ComponentScan(basePackages = "ru.loanpro.directory.service")
public class DirectoryAutoConfiguration {}

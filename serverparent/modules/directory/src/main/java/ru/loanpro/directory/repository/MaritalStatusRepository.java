package ru.loanpro.directory.repository;

import ru.loanpro.directory.domain.MaritalStatus;

public interface MaritalStatusRepository extends BaseRepository<MaritalStatus> {
}

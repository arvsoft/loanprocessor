package ru.loanpro.directory.domain;

import ru.loanpro.global.DatabaseSchema;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Сущность Типы документов
 *
 * @author Maksim Askhaev
 */
@Entity
@Table(name = "document_type", schema = DatabaseSchema.DIRECTORY)
public class DocumentType extends AbstractIdDirectory {
    public DocumentType() {
    }
}

package ru.loanpro.license.client.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Обработчик ошибок для бина {@link RestTemplate}. Предотвращает выбрасывание исключений при получении от вервера
 * кодов ответа отличных от 200.
 *
 * @author Oleg Brizhevatikh
 */
public class RestTemplateErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return HttpStatus.OK != clientHttpResponse.getStatusCode();
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

    }
}

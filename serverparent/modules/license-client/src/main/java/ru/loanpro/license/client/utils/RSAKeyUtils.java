package ru.loanpro.license.client.utils;

import org.springframework.core.codec.DecodingException;
import org.springframework.security.crypto.codec.Hex;

import javax.crypto.Cipher;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * Утилиты для работы с ключами шифрования.
 *
 * @author Oleg Brizhevatikh
 */
public class RSAKeyUtils {

    /**
     * Генерирует пару RSA-ключей.
     *
     * @return пара ключей.
     */
    public static KeyPair generateKeyPair() {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(2048);
            return generator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * Шифрует сообщение с помощюь ключа в виде строки.
     *
     * @param stringKey ключ шифрования.
     * @param message шифруемое сообщение.
     * @return зашифрованное сообщение.
     */
    public static String encryptString(String stringKey, String message) {
        try {
            PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(
                new X509EncodedKeySpec(Base64.getDecoder().decode(stringKey)));

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] bytes = cipher.doFinal(message.getBytes(Charset.forName(("UTF-8"))));

            return new String(Hex.encode(bytes));
        } catch (Exception e) {
            e.printStackTrace();
            throw new DecodingException(e.getMessage());
        }
    }

    /**
     * Расшифровывает сообщение с помощью ключа в виде строки.
     *
     * @param stringKey ключ шифрования.
     * @param message зашифрованное сообщение.
     * @return расшифрованное сообщение.
     */
    public static String decryptString(String stringKey, String message) {
        try {
            PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(
                new PKCS8EncodedKeySpec(Base64.getDecoder().decode(stringKey)));

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] bytes = cipher.doFinal(Hex.decode(new StringBuffer(message)));

            return new String(bytes, Charset.forName("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new DecodingException(e.getMessage());
        }
    }
}

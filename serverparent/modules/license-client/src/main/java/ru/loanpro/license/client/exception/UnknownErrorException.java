package ru.loanpro.license.client.exception;

/**
 * Исключение, возникающее при получении неизвестного кода ошибки.
 *
 * @author Oleg Brizhevatikh
 */
public class UnknownErrorException extends Exception {
}

package ru.loanpro.license.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * DTO-класс зарегистрированного запроса на получение лицензии.
 *
 * @author Oleg Brizhevatikh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRegisteredDTO {

    /**
     * Идентификатор зарегистрированного запроса.
     */
    @JsonProperty("id")
    private UUID id;

    /**
     * Публичный ключ сервера.
     */
    @JsonProperty("key")
    private String key;

    /**
     * Сообщение для диалога с сервером.
     */
    @JsonProperty("message")
    private String message;

    public RequestRegisteredDTO() {
    }

    public RequestRegisteredDTO(UUID id, String key, String message) {
        this.id = id;
        this.key = key;
        this.message = message;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package ru.loanpro.license.client.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.loanpro.global.DatabaseSchema;

/**
 * Сущность запроса на ручную активацию лицензии.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "manual_request", schema = DatabaseSchema.SECURITY)
public class ManualRequest {
    /**
     * Идентификатор.
     */
    @Id
    private UUID id = UUID.randomUUID();

    /**
     * Публичный ключ клиента.
     */
    @Column(name = "public_key", columnDefinition = "TEXT")
    private String publicKey;

    /**
     * Приватный ключ клиента.
     */
    @Column(name = "private_key", columnDefinition = "TEXT")
    private String privateKey;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}

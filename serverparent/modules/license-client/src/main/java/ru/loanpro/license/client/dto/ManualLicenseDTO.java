package ru.loanpro.license.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * DTO-ручной активации лицензии.
 *
 * @author Oleg Brizhevatikh
 */
public class ManualLicenseDTO {
    /**
     * Идентификатор лицензии.
     */
    @JsonProperty("id")
    private UUID id;

    /**
     * Публичный ключ сервера.
     */
    @JsonProperty("key")
    private String key;

    /**
     * Шифрованная лицензия.
     */
    @JsonProperty("licenseDTO")
    private String licenseDTO;

    public ManualLicenseDTO() {
    }

    public ManualLicenseDTO(UUID id, String key, String licenseDTO) {
        this.id = id;
        this.key = key;
        this.licenseDTO = licenseDTO;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLicenseDTO() {
        return licenseDTO;
    }

    public void setLicenseDTO(String licenseDTO) {
        this.licenseDTO = licenseDTO;
    }
}

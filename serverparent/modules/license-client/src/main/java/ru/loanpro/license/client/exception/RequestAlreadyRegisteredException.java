package ru.loanpro.license.client.exception;

/**
 * Исключение происходящее при попытке зарегистрировать запрос на получение лицензии по одному ключу дважды.
 *
 * @author Oleg Brizhevatikh
 */
public class RequestAlreadyRegisteredException extends Exception {
}

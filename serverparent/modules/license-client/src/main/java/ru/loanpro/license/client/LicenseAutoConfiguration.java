package ru.loanpro.license.client;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.web.client.RestTemplate;
import ru.loanpro.global.LoanProcessor360;
import ru.loanpro.global.ProductType;
import ru.loanpro.license.client.utils.RestTemplateErrorHandler;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

/**
 * Автоматическая конфигурация модуля лицензирования.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
@EnableScheduling
@EntityScan(basePackages = "ru.loanpro.license.client.domain")
@EnableJpaRepositories(basePackages = "ru.loanpro.license.client.repository")
@ComponentScan(basePackages = "ru.loanpro.license.client.service")
@AutoConfigureAfter({WebMvcAutoConfiguration.class})
public class LicenseAutoConfiguration {

    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;

    @Bean
    public RestTemplate restTemplate() {
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

            SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

            CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

            HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);

            RestTemplate restTemplate = new RestTemplate(requestFactory);
            restTemplate.setErrorHandler(new RestTemplateErrorHandler());

            return restTemplate;
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Bean
    @Qualifier("licenseServerPath")
    @Autowired
    public String licenseServerPath() {

        return String.format("%s/%s/%s",
            "https://license.loanprocessor360.com/product",
            ProductType.LOAN_PROCESSOR_360.getProductName(),
            LoanProcessor360.H2.getByDriverName(driverClassName).getVersion());
    }

    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler();
    }
}

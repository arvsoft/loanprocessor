package ru.loanpro.license.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.license.client.domain.License;

import java.util.Optional;
import java.util.UUID;

/**
 * Репозиторий сущности {@link License}
 *
 * @author Oleg Brizhevatikh
 */
public interface LicenseRepository extends JpaRepository<License, UUID> {

    /**
     * Возвращает лицензию. Лицензий не может быть более одной.
     *
     * @return лицензию.
     */
    @Query(value = "SELECT * FROM SECURITY.LICENSE ORDER BY id LIMIT 1", nativeQuery = true)
    Optional<License> getLicense();
}

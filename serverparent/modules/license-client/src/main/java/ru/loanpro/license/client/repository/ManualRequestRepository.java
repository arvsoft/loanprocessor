package ru.loanpro.license.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.loanpro.license.client.domain.ManualRequest;

import java.util.Optional;
import java.util.UUID;

/**
 * Репозиторий сущности {@link ManualRequest}.
 *
 * @author Oleg Brizhevatikh
 */
public interface ManualRequestRepository extends JpaRepository<ManualRequest, UUID> {

    /**
     * Возвращает запрос на ручную активацию лицензии. Запросов не может быть более одного.
     *
     * @return запрос на ручную активацию.
     */
    @Query(value = "SELECT * FROM SECURITY.MANUAL_REQUEST ORDER BY id LIMIT 1", nativeQuery = true)
    Optional<ManualRequest> getManualRequest();
}

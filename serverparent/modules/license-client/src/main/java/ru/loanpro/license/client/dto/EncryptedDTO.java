package ru.loanpro.license.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO-объект лицензии, передающийся в шифрованном состоянии.
 *
 * @author Oleg Brizhevatikh
 */
public class EncryptedDTO {

    /**
     * Количество пользователей лицензии.
     */
    @JsonProperty("userCount")
    private Integer userCount;

    /**
     * Сообщение для диалога с сервером.
     */
    @JsonProperty("message")
    private String message;

    public EncryptedDTO() {
    }

    public EncryptedDTO(Integer userCount, String message) {
        this.userCount = userCount;
        this.message = message;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

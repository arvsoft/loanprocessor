package ru.loanpro.license.client.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.loanpro.license.client.collection.LicenseStatus;
import ru.loanpro.license.client.domain.License;
import ru.loanpro.license.client.domain.ManualRequest;
import ru.loanpro.license.client.dto.EncryptedDTO;
import ru.loanpro.license.client.dto.IncorrectLicenseFileException;
import ru.loanpro.license.client.dto.ManualLicenseDTO;
import ru.loanpro.license.client.dto.RequestRegisteredDTO;
import ru.loanpro.license.client.exception.BadRequestException;
import ru.loanpro.license.client.exception.RequestAlreadyRegisteredException;
import ru.loanpro.license.client.exception.UnknownErrorException;
import ru.loanpro.license.client.repository.LicenseRepository;
import ru.loanpro.license.client.repository.ManualRequestRepository;
import ru.loanpro.license.client.utils.RSAKeyUtils;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

/**
 * Сервис лицензий.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class LicenseService {

    private static Logger LOGGER = Logger.getLogger(LicenseService.class.getName());

    private final LicenseRepository repository;

    private final ManualRequestRepository manualRequestRepository;

    private final RestTemplate restTemplate;

    private final String serverUrl;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final TaskScheduler taskScheduler;

    private ScheduledFuture<?> scheduledTask;

    private LicenseStatus licenseStatus = LicenseStatus.DEMO;

    private Integer userCount = 0;

    @Autowired
    public LicenseService(LicenseRepository repository, ManualRequestRepository manualRequestRepository,
                          RestTemplate restTemplate, TaskScheduler taskScheduler,
                          @Qualifier("licenseServerPath") String serverUrl) {
        this.repository = repository;
        this.manualRequestRepository = manualRequestRepository;
        this.restTemplate = restTemplate;
        this.serverUrl = serverUrl;
        this.taskScheduler = taskScheduler;
    }

    @PostConstruct
    public void init() {
        updateSchedule(getStatus());

        repository.getLicense().ifPresent(license -> {
            if (license.getLicense() != null)
                userCount = encryptLicense(license).getUserCount();
        });
    }

    /**
     * Обновляет график общения с сервером лицензий на основе текущего статуса лицензирования.
     *
     * @param status статус лицензии.
     */
    private synchronized void updateSchedule(LicenseStatus status) {
        // Если статус не изменился, ни чего не делаем.
        if (licenseStatus == status)
            return;
        else
            licenseStatus = status;

        // Если новый сататус - Демо, ни чего не делаем
        if (status == LicenseStatus.DEMO)
            return;

        // Отменяем следующую проверку лицензии.
        if (scheduledTask != null)
            scheduledTask.cancel(true);

        long delay;

        // Устанавливаем интервал проверки
        if (status == LicenseStatus.REQUEST)
            delay = 1000 * 60 * 15; // Проверка наличия новой лицензии - каждые 15 минут.
        else
            delay = 1000 * 60 * 60 * 24; // Подтверждение существующей лицензии - каждые 24 часа

        // Запускаем проверку лицензии сейчас и повторяем с указанным интервалом
        scheduledTask = taskScheduler.scheduleWithFixedDelay(
            new CheckLicenseTask(), new Date(Instant.now().toEpochMilli()), delay);
    }

    /**
     * Отправляет запрос на получение лицензии.
     *
     * @param comment коментарий к запросу.
     * @throws ResourceAccessException           если нет связи с сервером.
     * @throws RequestAlreadyRegisteredException если запрос с таким ключём уже зарегистрирован.
     * @throws BadRequestException               если запрос был некорректно сформирован.
     */
    @Transactional
    public void postRequest(String comment)
        throws ResourceAccessException, RequestAlreadyRegisteredException, BadRequestException {

        KeyPair keyPair = RSAKeyUtils.generateKeyPair();
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        String publicKeyString = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        String privateKeyString = Base64.getEncoder().encodeToString(privateKey.getEncoded());

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("key", publicKeyString);
        map.add("comment", comment);

        ResponseEntity<RequestRegisteredDTO> response = restTemplate
            .postForEntity(serverUrl, map, RequestRegisteredDTO.class);
        if (response.getStatusCode() == HttpStatus.CONFLICT)
            throw new RequestAlreadyRegisteredException();
        if (response.getStatusCode() == HttpStatus.BAD_REQUEST)
            throw new BadRequestException();

        RequestRegisteredDTO requestRegistered = response.getBody();

        String decryptedString = RSAKeyUtils.decryptString(privateKeyString, requestRegistered.getMessage());

        License request = new License();
        request.setId(requestRegistered.getId());
        request.setMessage(decryptedString);
        request.setPrivateKey(privateKeyString);
        request.setServerKey(requestRegistered.getKey());

        repository.save(request);
        updateSchedule(LicenseStatus.REQUEST);
    }

    /**
     * Проверяет лицензию. В случае изменения лицензии обновляет локальную лицензию. С каждым запросом обновляет
     * сообщение диалога с сервером.
     *
     * @return статус лицензирования.
     * @throws ResourceAccessException если нет связи с сервером.
     * @throws UnknownErrorException   в случае неизвестного типа ответа от сервера.
     */
    @Transactional
    public synchronized LicenseStatus checkLicense() throws ResourceAccessException, UnknownErrorException {
        Optional<License> optionalLicense = repository.getLicense();
        if (!optionalLicense.isPresent())
            return LicenseStatus.DEMO;

        License license = optionalLicense.get();

        UriComponentsBuilder builder = UriComponentsBuilder
            .fromUriString(serverUrl + "/" + license.getId().toString())
            .queryParam("message", RSAKeyUtils.encryptString(license.getServerKey(), license.getMessage()));

        ResponseEntity<String> response = restTemplate
            .getForEntity(builder.toUriString(), String.class);

        if (response.getStatusCode() == HttpStatus.NO_CONTENT)
            return LicenseStatus.REQUEST;
        if (response.getStatusCode() != HttpStatus.OK)
            throw new UnknownErrorException();

        String encryptedLicense = response.getBody();
        String decryptedLicense = RSAKeyUtils.decryptString(license.getPrivateKey(), encryptedLicense);

        EncryptedDTO encryptedDTO = mapFromString(decryptedLicense);
        license.setLicense(encryptedLicense);
        license.setMessage(encryptedDTO.getMessage());
        repository.save(license);

        userCount = encryptedDTO.getUserCount();

        return LicenseStatus.LICENSED;
    }

    /**
     * Загружает лицензию из переданного потока из файла лицензии.
     *
     * @param outputStream поток с файлом лицензии.
     * @throws IncorrectLicenseFileException если неудалось прочитать файл лицензии.
     */
    @Transactional
    public void uploadLicense(ByteArrayOutputStream outputStream) throws IncorrectLicenseFileException {
        String fileString = new String(outputStream.toByteArray(), Charset.forName("UTF-8"));
        try {
            ManualLicenseDTO manualLicenseDTO = objectMapper.readValue(fileString, ManualLicenseDTO.class);
            ManualRequest manualRequest = getManualRequest();

            License license = new License();
            license.setId(manualLicenseDTO.getId());
            license.setPrivateKey(manualRequest.getPrivateKey());
            license.setServerKey(manualLicenseDTO.getKey());
            license.setLicense(manualLicenseDTO.getLicenseDTO());

            EncryptedDTO encryptedDTO = mapFromString(RSAKeyUtils.decryptString(
                license.getPrivateKey(), license.getLicense()));
            license.setMessage(encryptedDTO.getMessage());

            repository.save(license);
            manualRequestRepository.delete(manualRequest);

            userCount = encryptedDTO.getUserCount();
            updateSchedule(LicenseStatus.LICENSED);
        } catch (IOException e) {
            throw new IncorrectLicenseFileException();
        }
    }

    /**
     * Расшифровывает лицензию.
     *
     * @param license лицензия с шифрованным содержимым.
     * @return расшифрованная лицензия.
     */
    private EncryptedDTO encryptLicense(License license) {
        return mapFromString(RSAKeyUtils.decryptString(license.getPrivateKey(), license.getLicense()));
    }

    /**
     * Возвращает статус лицензирования.
     *
     * @return статус лицензирования
     */
    public LicenseStatus getStatus() {
        Optional<License> optionalLicense = repository.getLicense();

        if (!optionalLicense.isPresent())
            return LicenseStatus.DEMO;

        License license = optionalLicense.get();

        if (license.getLicense() == null)
            return LicenseStatus.REQUEST;

        return LicenseStatus.LICENSED;
    }

    /**
     * Возвращает количество пользователей в лицензии.
     *
     * @return количество пользователей.
     */
    public Integer getUserCount() {
        return userCount;
    }

    /**
     * Возвращает идентификатор лицензии или запроса.
     *
     * @return идентификатор.
     */
    public UUID getUUID() {
        return repository.getLicense().get().getId();
    }

    /**
     * Возвращает информацию о том, существует ли запрос на получение лицензии через ручную активацию.
     *
     * @return наличие запроса.
     */
    public boolean isManualRequestExists() {
        return manualRequestRepository.getManualRequest().isPresent();
    }

    /**
     * Конвертирует строку в DTO-лицензии.
     *
     * @param string строка.
     * @return DTO-лицензии.
     */
    private EncryptedDTO mapFromString(String string) {
        try {
            return objectMapper.readValue(string, EncryptedDTO.class);
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
            e.printStackTrace();
        }
        return new EncryptedDTO(0, "");
    }

    /**
     * Возвращает запрос на ручную активацию лицензии. В случае отсутствия запроса создаёт новый и возвращает его.
     *
     * @return запрос на ручную активацию лицензии.
     */
    public ManualRequest getManualRequest() {
        return manualRequestRepository.getManualRequest().orElseGet(() -> {
            KeyPair keyPair = RSAKeyUtils.generateKeyPair();

            ManualRequest manualRequest = new ManualRequest();
            manualRequest.setPublicKey(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
            manualRequest.setPrivateKey(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()));

            return manualRequestRepository.save(manualRequest);
        });
    }

    /**
     * Таск, проверяющий лицензию в отдельном потоке.
     */
    private class CheckLicenseTask implements Runnable {

        @Override
        public void run() {
            System.out.println(Instant.now());
            try {
                LicenseStatus status = checkLicense();
                if (licenseStatus != status)
                    updateSchedule(status);
            } catch (UnknownErrorException e) {
                LOGGER.info(e.getMessage());
            } catch (ResourceAccessException e) {
                LOGGER.info("No connection to license-server");
            }
        }
    }
}

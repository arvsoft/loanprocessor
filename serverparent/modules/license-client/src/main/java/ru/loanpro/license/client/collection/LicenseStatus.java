package ru.loanpro.license.client.collection;

/**
 * Статус лицензирования.
 *
 * @author Oleg Brizhevatikh
 */
public enum LicenseStatus {
    DEMO, REQUEST, LICENSED
}

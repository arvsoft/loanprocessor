package ru.loanpro.license.client.dto;

/**
 * Исколючение, возникающее при невозможности прочтения файла с лицензией.
 *
 * @author Oleg Brizhevatikh
 */
public class IncorrectLicenseFileException extends Exception {
}

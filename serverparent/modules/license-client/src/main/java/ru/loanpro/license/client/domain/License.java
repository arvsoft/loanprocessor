package ru.loanpro.license.client.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.loanpro.global.DatabaseSchema;

/**
 * Сущность лицензии.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "license", schema = DatabaseSchema.SECURITY)
public class License {
    /**
     * Идентификатор лицензии.
     */
    @Id
    private UUID id;

    /**
     * Приватный ключ клиента.
     */
    @Column(name = "private_key", columnDefinition = "TEXT")
    private String privateKey;

    /**
     * Публичный ключ сервера.
     */
    @Column(name = "server_key", columnDefinition = "TEXT")
    private String serverKey;

    /**
     * Сообщение для диалога с сервером.
     */
    @Column(name = "message")
    private String message;

    /**
     * Шифрованная лицензия.
     */
    @Column(name = "license", columnDefinition = "TEXT")
    private String license;

    public License() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getServerKey() {
        return serverKey;
    }

    public void setServerKey(String serverKey) {
        this.serverKey = serverKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }
}

package ru.loanpro.license.client.exception;

/**
 * Исключение, происходяще при некорректном формировании строки запроса.
 *
 * @author Oleg Brizhevatikh
 */
public class BadRequestException extends Exception {
}

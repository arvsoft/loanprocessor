package ru.loanpro.uibasis;

import com.vaadin.event.ShortcutAction;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.security.shared.VaadinSharedSecurity;

import javax.naming.AuthenticationException;

/**
 * Абстрактный класс страницы авторизации.
 *
 * @author Oleg Brizhevatikh
 */
public abstract class AbstractLoginUI extends UI {

    protected final VaadinSharedSecurity vaadinSecurity;
    protected final I18N i18N;

    @Autowired
    public AbstractLoginUI(VaadinSharedSecurity vaadinSecurity, I18N i18N) {
        this.vaadinSecurity = vaadinSecurity;
        this.i18N = i18N;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        Label loginFailed = new Label(i18N.get("uiBasis.authorization.failed"));
        loginFailed.setVisible(false);
        loginFailed.addStyleName(ValoTheme.LABEL_FAILURE);

        TextField username = new TextField(i18N.get("uiBasis.authorization.username"));
        TextField password = new PasswordField(i18N.get("uiBasis.authorization.password"));
        CheckBox rememberMe = new CheckBox(i18N.get("uiBasis.authorization.remember"));

        Button button = new Button(i18N.get("uiBasis.authorization.login"));
        button.setDisableOnClick(true);
        button.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        button.addClickListener(event -> {
            try {
                vaadinSecurity.login(username.getValue(), password.getValue(), rememberMe.getValue());
            } catch (AuthenticationException e) {
                username.focus();
                username.selectAll();
                password.setValue("");
            } catch (Exception e) {
                LoggerFactory.getLogger(getClass()).error("Unexpected error while logging in", e);
                loginFailed.setVisible(true);
            } finally {
                button.setEnabled(true);
            }
        });

        FormLayout form = new FormLayout(loginFailed, username, password, rememberMe, button);
        form.setSizeUndefined();
        VerticalLayout layout = new VerticalLayout(form);
        layout.setSizeFull();
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);

        setContent(layout);
        setSizeFull();
    }
}

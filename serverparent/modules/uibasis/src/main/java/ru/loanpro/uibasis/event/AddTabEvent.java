package ru.loanpro.uibasis.event;

import ru.loanpro.uibasis.component.BaseTab;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Событие создания вкладки.
 *
 * @author Oleg Brizhevatikh
 */
public class AddTabEvent {
    private Class<? extends BaseTab> tabClass;
    private List<Object> objectList;

    /**
     * Конструктор события с передачей экземпляра объекта в новую вкладку.
     *
     * @param tabClass Класс вкладки, для которой вызывается событие.
     * @param objects Массив объектов, передающийся в новую вкладку при её вызове.
     */
    public AddTabEvent(Class<? extends BaseTab> tabClass, Object... objects) {
        this.tabClass = tabClass;
        this.objectList = (objects == null) ? Collections.emptyList() : Arrays.asList(objects);
    }

    /**
     * Конструктор события без передачи экземпляра объекта в новую вкладку.
     *
     * @param tabClass Класс вкладки, для которой вызывается событие.
     */
    public AddTabEvent(Class<? extends BaseTab> tabClass) {
        this.tabClass = tabClass;
        this.objectList = Collections.emptyList();
    }

    /**
     * Возвращает класс вкладки.
     *
     * @return Класс вкладки.
     */
    public Class<? extends BaseTab> getTabClass() {
        return tabClass;
    }

    /**
     * Возвращает список переданных объектов.
     *
     * @return Экземпляр объекта.
     */
    public List<Object> getObjectList() {
        return objectList;
    }
}

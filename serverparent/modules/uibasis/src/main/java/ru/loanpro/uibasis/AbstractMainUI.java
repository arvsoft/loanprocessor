package ru.loanpro.uibasis;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.uibasis.view.AccessDeniedView;
import ru.loanpro.uibasis.view.ErrorView;

/**
 * Абстрактный класс основоного рабочего окна.
 *
 * @author Oleg Brizhevatikh
 */
public abstract class AbstractMainUI extends UI {

    protected final SessionEventBus sessionEventBus;
    private final SpringViewProvider springViewProvider;
    protected VerticalLayout navigationBar;

    private Panel menuContent;

    /**
     * Возвращает заголовок главного меню.
     *
     * @return заголовок главного меню.
     */
    protected abstract Layout getMenuHeader();

    /**
     * Возвращает основной контент главного меню.
     *
     * @return основной контент главного меню.
     */
    protected abstract Layout getMenuContent();

    /**
     * Возвращает подвал главного меню.
     *
     * @return подвал главного меню.
     */
    protected abstract Layout getMenuFooter();

    @Autowired
    public AbstractMainUI(SessionEventBus sessionEventBus, SpringViewProvider springViewProvider) {
        this.sessionEventBus = sessionEventBus;
        this.springViewProvider = springViewProvider;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        sessionEventBus.register(this);

        navigationBar = new VerticalLayout();
        navigationBar.setMargin(false);
        navigationBar.setSpacing(false);
        navigationBar.addStyleName("menu-container");
        navigationBar.setHeight(100, Unit.PERCENTAGE);

        Layout menuHeader = getMenuHeader();
        Layout menuFooter = getMenuFooter();
        menuContent = new Panel(getMenuContent());
        menuContent.addStyleName(ValoTheme.PANEL_BORDERLESS);
        menuContent.setHeight(Page.getCurrent().getBrowserWindowHeight() - 300, Unit.PIXELS);

        navigationBar.addComponents(menuHeader, menuContent, menuFooter);
        navigationBar.setExpandRatio(menuContent, 1);

        CssLayout viewContainer = new CssLayout();
        viewContainer.setSizeFull();

        HorizontalLayout layout = new HorizontalLayout(navigationBar, viewContainer);
        layout.setSizeFull();
        layout.setMargin(false);
        layout.setSpacing(false);
        layout.setExpandRatio(viewContainer, 1.0f);

        springViewProvider.setAccessDeniedViewClass(AccessDeniedView.class);

        Navigator navigator = new Navigator(this, viewContainer);
        navigator.addProvider(springViewProvider);
        navigator.setErrorView(ErrorView.class);

        setContent(layout);

        Page.getCurrent().addBrowserWindowResizeListener(this::browserWindowResizeHandler);
    }

    /**
     * Обрабатывает событие изменения размеров окна браузера.
     *
     * @param resizeEvent событие изменения размеров окна браузера.
     */
    protected void browserWindowResizeHandler(BrowserWindowResizeEvent resizeEvent) {
        menuContent.setHeight(resizeEvent.getHeight() - 300, Unit.PIXELS);
    }

    @Override
    public void detach() {
        super.detach();
        sessionEventBus.unregister(this);
    }
}

package ru.loanpro.uibasis.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.component.TabList;
import ru.loanpro.uibasis.event.AddTabEvent;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Главный View для отображения вкладок системы.
 * Является диспетчером вкладок.
 *
 * @author Oleg Brizhevatikh
 */
@SpringView(name = "")
public class MainView extends TabSheet implements View {

    private static Logger LOGGER = Logger.getLogger(MainView.class.getName());

    private final ApplicationContext context;
    private final TabList tabList;
    private final SessionEventBus sessionEventBus;

    @Autowired
    public MainView(ApplicationContext context, TabList tabList,
                    SessionEventBus sessionEventBus) {
        this.context = context;
        this.sessionEventBus = sessionEventBus;
        this.tabList = tabList;
        LOGGER.info("Constructor");
    }

    @Override
    public void attach() {
        super.attach();
    }

    @PostConstruct
    public void init() {
        LOGGER.info("Post construct");
        sessionEventBus.register(this);
        setSizeFull();
        addStyleName(ValoTheme.TABSHEET_FRAMED);
        addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);

        tabList.getMap().keySet().forEach(tab -> {
            addTab(tab, tab.getTabName());
            Tab tabItem = getTab(tab);
            tabItem.setClosable(true);
            tabItem.setStyleName(tab.getTabStyle().getName());
        });

        setCloseHandler((tabSheet, tabContent) -> {
            Tab tab = tabSheet.getTab(tabContent);
            BaseTab baseTab = (BaseTab) tab.getComponent();
            sessionEventBus.post(new RemoveTabEvent<>(baseTab));
        });
    }

    @PreDestroy
    public void destroy() {
        sessionEventBus.unregister(this);
        LOGGER.info("Destroy");
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    @Subscribe
    private void addTabEventHandler(AddTabEvent event) {
        BaseTab selectedTab = (BaseTab) getSelectedTab();

        Class<? extends BaseTab> tabClass = event.getTabClass();
        List<Object> objectList = event.getObjectList();

        BaseTab tab;
        if (tabClass.isAnnotationPresent(SingletonTab.class)) {
            tab = tabList.getTabByClass(tabClass).orElseGet(() -> context.getBean(tabClass));
        } else {
            String className = tabClass.getSimpleName();
            className = Character.toLowerCase(className.charAt(0)) + className.substring(1);
            tab = getBeanWithConstructor(className, objectList);
        }

        sessionEventBus.register(tab);
        addTab(tab, tab.getTabName());
        setSelectedTab(tab);
        getTab(tab).setClosable(true);
        tabList.addTab(tab, objectList, selectedTab);
        getTab(tab).setStyleName(tab.getTabStyle().getName());
        getTabIndex();
    }

    @Subscribe
    private void removeTabEventHandler(RemoveTabEvent event) {
        BaseTab baseTab = event.getTab();
        baseTab.destroy();
        BaseTab previousTab = tabList.getPreviousTab(baseTab);

        // Если закрывается текущая вкладка и известна предыдущяя вкладка, переключаемся на неё.
        // Иначе - поведение по-умолчанию.
        if (getSelectedTab() == baseTab && previousTab != null) {
            setSelectedTab(previousTab);
        }

        sessionEventBus.unregister(baseTab);
        tabList.removeTab(baseTab);
        removeTab(getTab(baseTab));
    }

    private BaseTab getBeanWithConstructor(String tabClassName, List<Object> list) {
        try {
            switch (list.size()) {
            case 0:
                return (BaseTab) context.getBean(tabClassName, (Object) null);
            case 1:
                return (BaseTab) context.getBean(tabClassName, list.get(0));
            case 2:
                return (BaseTab) context.getBean(tabClassName, list.get(0), list.get(1));
            case 3:
                return (BaseTab) context.getBean(tabClassName, list.get(0), list.get(1), list.get(2));
            case 4:
                return (BaseTab) context.getBean(tabClassName, list.get(0), list.get(1), list.get(2), list.get(3));
            case 5:
                return (BaseTab) context.getBean(tabClassName, list.get(0), list.get(1), list.get(2), list.get(3),
                    list.get(4));
            case 6:
                return (BaseTab) context.getBean(tabClassName, list.get(0), list.get(1), list.get(2), list.get(3),
                    list.get(4), list.get(5));
            case 7:
                return (BaseTab) context.getBean(tabClassName, list.get(0), list.get(1), list.get(2), list.get(3),
                    list.get(4), list.get(5), list.get(6));
            case 8:
                return (BaseTab) context.getBean(tabClassName, list.get(0), list.get(1), list.get(2), list.get(3),
                    list.get(4), list.get(5), list.get(6), list.get(7));
            default:
                throw new BeanCreationException("Incorrect parameters count");
            }
        } catch (BeanCreationException e) {
            LOGGER.log(Level.ALL, String.format("%s : %s", e.getBeanName(), e.getMessage()));
            Notification.show(
                String.format("I can not find the constructor with parameters: %s",
                    list.stream().map(Object::getClass).map(Class::getSimpleName).collect(Collectors.joining(", "))),
                Notification.Type.ERROR_MESSAGE);

            return (BaseTab) context.getBean(tabClassName);
        }
    }
}

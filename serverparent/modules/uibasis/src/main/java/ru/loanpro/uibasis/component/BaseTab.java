package ru.loanpro.uibasis.component;

import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.NoRepositoryBean;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.global.UiTheme;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.security.service.SecurityService;
import ru.loanpro.security.settings.service.SettingsService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Базовый класс вкладки. Реализует основную логику вкладок для работы с диспетчером вкладок.
 *
 * @author Oleg Brizhevatikh
 */
@NoRepositoryBean
public abstract class BaseTab extends VerticalLayout {

    @Autowired
    protected SecurityService securityService;

    @Autowired
    protected I18N i18n;

    @Autowired
    protected SessionEventBus sessionEventBus;

    @Autowired
    protected ApplicationEventBus applicationEventBus;

    @Autowired
    protected SettingsService settingsService;

    @Autowired
    protected ConfigurationService configurationService;

    @Autowired
    protected ChronometerService chronometerService;

    public BaseTab() {
        System.out.println(this.getClass().getSimpleName() + " constructor " + this.toString());
    }

    /**
     * Метод извлечения экземпляра объекта диспетчером вкладок из текущей вкладки.
     * Должен быть переопределён во вкладках, которые могут изменять полученные от
     * диспетчера вкладок объекты.
     *
     * @return Экземпляр объекта.
     */
    public Object getObject() {
        return null;
    }

    /**
     * Метод получения имени вкладки диспетчером вкладок для отображения в заголовке вкладки.
     *
     * @return Имя вкладки.
     */
    public String getTabName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Метод получения стиля заголовка вкладки.
     *
     * @return Стиль заголовка.
     */
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.DEFAULT;
    }

    @PostConstruct
    public void init() {
        System.out.println(this.getClass().getSimpleName() + " post construct " + this.toString());
    }

    @PreDestroy
    public void destroy() {
        System.out.println(this.getClass().getSimpleName() + " pre destroy " + this.toString());
    }

    protected void showErrorMessage(Exception e) {
        Notification.show(
            i18n.get("core.criticalError"),
            String.format(i18n.get("core.criticalErrorDescription"), e.getMessage()),
            Notification.Type.ERROR_MESSAGE);
    }
}

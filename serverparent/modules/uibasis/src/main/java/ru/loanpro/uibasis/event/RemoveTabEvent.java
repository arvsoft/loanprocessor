package ru.loanpro.uibasis.event;

import ru.loanpro.uibasis.component.BaseTab;

/**
 * Событие закрытия вкладки, испускаемое закрываемой вкладкой или одной из вкладок.
 *
 * @param <T> тип вкладки, унаследованый от {@link BaseTab}
 *
 * @author Oleg Brizhevatikh
 */
public class RemoveTabEvent<T extends BaseTab> {
    private T tab;

    /**
     * Конструктор события с передачей закрываемой вкладки.
     *
     * @param tab Вкладка, для которой вызывается событие.
     */
    public RemoveTabEvent(T tab) {
        this.tab = tab;
    }

    /**
     * Возвращает переданную вкладку.
     *
     * @return Переданная вкладка.
     */
    public T getTab() {
        return tab;
    }
}

package ru.loanpro.uibasis.component;

import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Бин для хранения открытых вкладок пользователя. Держит Prototype-бины вкладок
 * активными на время работы Vaadin-сессии, не позволяя им очищаться во время
 * обновления страницы. Соханяет порядок открытия вкладок.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@VaadinSessionScope
public class TabList {

    private static Logger LOGGER = Logger.getLogger(TabList.class.getName());

    private LinkedHashMap<BaseTab, TabContainer> map;

    public TabList() {
        map = new LinkedHashMap<>();
        System.out.println("Tab List constructor");
    }

    public void addTab(BaseTab tab, List<Object> objectList, BaseTab previousTab) {
        TabContainer tabContainer = new TabContainer();
        tabContainer.previousTab = previousTab;

        tabContainer.object = (objectList == null)
            ? Collections.emptyList()
            : objectList;

        tabContainer.hash = (objectList == null)
            ? null
            : objectList.stream().map(Object::hashCode).collect(Collectors.toList());

        map.put(tab, tabContainer);
    }

    public Optional<BaseTab> getTabByClass(Class<? extends BaseTab> tabClass) {
        return map.keySet().stream()
            .filter(t -> t.getClass().equals(tabClass))
            .findFirst();
    }

    public Map<BaseTab, TabContainer> getMap() {
        return map;
    }

    public void removeTab(BaseTab tab) {
        // У всех вкладок, ссылающихся на удаляемую вкладку, удаляем предка.
        map.forEach((k, v) -> {
            if (v.previousTab == tab)
                v.previousTab = null;
        });
        map.remove(tab);
    }

    public BaseTab getPreviousTab(BaseTab baseTab) {
        return map.get(baseTab).previousTab;
    }

    @PostConstruct
    public void init() {
        LOGGER.info("Post construct");
    }

    @PreDestroy
    public void destroy() {
        map.clear();
        LOGGER.info("Destroy");
    }

    private class TabContainer {
        List<Object> object;
        List<Integer> hash;
        BaseTab previousTab;
    }
}
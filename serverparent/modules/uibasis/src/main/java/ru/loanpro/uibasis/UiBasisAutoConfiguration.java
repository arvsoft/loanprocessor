package ru.loanpro.uibasis;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Автоматическая конфигурация модуля UIBasis.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
@ComponentScan(basePackages = "ru.loanpro.uibasis")
public class UiBasisAutoConfiguration {}

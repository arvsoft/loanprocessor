package ru.loanpro.uibasis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.vaadin.spring.i18n.MessageProvider;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Configuration
public class LocalizationConfiguration {

    @Value("classpath:localization/*.properties")
    Resource[] resource;

    @Bean
    public MessageProvider messageProvider() {
        List<String> baseNames = Arrays.stream(resource)
            .map(ClassPathResource.class::cast)
            .map(ClassPathResource::getPath)
            .map(item -> "classpath:/" + item.split("_")[0])
            .distinct()
            .collect(Collectors.toList());

        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        baseNames.forEach(messageSource::addBasenames);
        messageSource.setDefaultEncoding("UTF-8");

        return new MessageProvider() {
            @Override
            public MessageFormat resolveCode(String code, Locale locale) {
                String message;
                try {
                    message = messageSource.getMessage(code, null, locale);
                } catch (NoSuchMessageException e) {
                    message = code;
                }

                return getMessageFormat(message, locale);
            }

            private MessageFormat getMessageFormat(String message, Locale locale) {
                if (message == null)
                    return null;

                return new MessageFormat(message, locale);
            }

            @Override
            public void clearCache() {
                ResourceBundle.clearCache();
            }
        };
    }
}

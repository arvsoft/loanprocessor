package ru.loanpro.server.logging.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;

import ru.loanpro.server.logging.annotation.LogName;

/**
 * Лог ошибки системы.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorColumn(name = "ERROR")
@LogName("log.error")
public class LogError extends AbstractLog {

    /**
     * Тест сообщения об ошибке.
     */
    @Column(name = "error")
    private String error;

    public LogError() {
    }

    public LogError(Instant instant, String error) {
        setInstant(instant);
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

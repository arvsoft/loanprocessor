package ru.loanpro.server.ui.module.report;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.base.report.service.ReportService;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.server.ui.module.report.reports.ClientReport1Tab;
import ru.loanpro.server.ui.module.report.reports.LoanReport1Tab;
import ru.loanpro.server.ui.module.report.reports.LoanReport2Tab;
import ru.loanpro.uibasis.event.AddTabEvent;

/**
 * Карточка выбора и отображения отчета. Содержит краткие сведения об отчете.
 *
 * @author Maksim Askhaev
 */
public class ReportCard  extends VerticalLayout {

    private final Report report;
    private final ReportService service;
    private final I18N i18n;
    private final SessionEventBus sessionEventBus;
    private final String role;

    public ReportCard(Report report, ReportService service, I18N i18n, SessionEventBus sessionEventBus) {
        this.report = report;
        this.service = service;
        this.i18n = i18n;
        this.sessionEventBus = sessionEventBus;
        this.role = report.getRole();

        setWidth(400, Unit.PIXELS);
        addStyleNames(MaterialTheme.CARD_2, MaterialTheme.CARD_NO_PADDING, MaterialTheme.CARD_HOVERABLE);
        addStyleName("layout-margin-bottom");
        setMargin(true);
        setSpacing(true);

        Label reportName = new Label(report.getName());

        Button open = new Button(i18n.get("reportCard.button.open"), VaadinIcons.LINE_BAR_CHART);
        open.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        addComponents(reportName, open);
        setComponentAlignment(open, Alignment.BOTTOM_RIGHT);

        open.addClickListener(e->{
            switch (report.getReportId()){
                case "LOANS.REPORT1" :
                    sessionEventBus.post(new AddTabEvent(LoanReport1Tab.class, report));
                    break;
                case "LOANS.REPORT2" :
                    sessionEventBus.post(new AddTabEvent(LoanReport2Tab.class, report));
                    break;
                case "CLIENTS.REPORT1" :
                    sessionEventBus.post(new AddTabEvent(ClientReport1Tab.class, report));
                    break;
            }
        });
    }

    public String getRole() {
        return role;
    }
}

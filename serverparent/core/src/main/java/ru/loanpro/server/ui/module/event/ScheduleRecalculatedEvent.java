package ru.loanpro.server.ui.module.event;

import ru.loanpro.base.loan.domain.BaseLoan;

/**
 * Событие пересчёта графика платежей. Возникает при завершении расчёта автоматического или ручного графика.
 *
 * @param <T> тип базовой сущности займа.
 *
 * @author Oleg Brizhevatikh
 */
public class ScheduleRecalculatedEvent<T extends BaseLoan> {

    private T entity;

    /**
     * Конструктор события с передачей базовой сущности займа, для которой произошёл пересчёт графика.
     *
     * @param entity базовая сущность займа.
     */
    public ScheduleRecalculatedEvent(T entity) {
        this.entity = entity;
    }

    /**
     * Возвраащает базовую сущность займа.
     *
     * @return базовая сущность займа.
     */
    public T getEntity() {
        return entity;
    }
}

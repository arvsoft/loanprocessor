package ru.loanpro.server.configuration.database;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

/**
 * Стратегия именования таблиц в базе данных MySQL. Делает хак, с подстановкой имени схемы в имя таблицы в обход
 * стандартных схем, которые не поддерживаются данной базой, будь она проклята.
 *
 * @author Oleg Brizhevatikh
 */
public class MySQLPhysicalNamingStrategy extends SpringPhysicalNamingStrategy {

    private Identifier currentSchema;

    @Override
    public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        currentSchema = name;
        return name;
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return new Identifier(
            String.format("%s.%s", currentSchema.getText(), name.getText().toUpperCase()),
            name.isQuoted());
    }
}

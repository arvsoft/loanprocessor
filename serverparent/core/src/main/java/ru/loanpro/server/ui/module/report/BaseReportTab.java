package ru.loanpro.server.ui.module.report;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.base.loan.service.LoanService;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.base.report.service.ReportService;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/**
 * Базовая вкладка работы с отчетом
 *
 * @author Maksim Askhaev
 */
@NoRepositoryBean
public abstract class BaseReportTab extends BaseTab {

    @Autowired
    protected LoanService loanService;

    @Autowired
    protected ReportService reportService;

    @Autowired
    private DepartmentService departmentService;

    protected Report report;

    protected DateTimeFormatter dateTimeFormatter;
    protected DecimalFormat moneyFormat;
    protected DecimalFormat percentFormat;
    protected CheckBox cbxUseDepartment;
    protected CheckBox cbxUseMainDate;

    protected abstract Layout initReportInformation();

    protected abstract String getLabelDateDescription();
    protected abstract boolean getVisibleBaseSearch();

    protected abstract Layout getActionLayout(Report report);

    protected ComboBox<Department> cbDepartment;
    protected DateField dfDateMin;
    protected DateField dfDateMax;

    public BaseReportTab(Report report) {
        this.report = report;
    }

    @PostConstruct
    public void init() {
        dateTimeFormatter = DateTimeFormatter.ofPattern(configurationService.getDateFormat(), configurationService.getLocale());
        moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        percentFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        percentFormat.setMinimumFractionDigits(3);
        percentFormat.setMaximumFractionDigits(3);

        Layout reportLayout = initReportInformation();

        GridLayout gridDepartment = new GridLayout();
        gridDepartment.setSpacing(true);
        gridDepartment.removeAllComponents();
        gridDepartment.setColumns(1);
        gridDepartment.setRows(2);

        cbxUseDepartment = new CheckBox(i18n.get("reporttab.field.department"));
        cbxUseDepartment.addStyleName(ValoTheme.CHECKBOX_SMALL);

        gridDepartment.addComponent(cbxUseDepartment, 0, 0);
        cbDepartment = new ComboBox<>();
        cbDepartment.setReadOnly(true);
        cbDepartment.setItemCaptionGenerator(Department::getName);
        cbDepartment.setEmptySelectionAllowed(false);
        cbDepartment.setWidth(500, Unit.PIXELS);
        cbDepartment.setItems(departmentService.findAll());
        cbDepartment.setValue(securityService.getDepartment());
        gridDepartment.addComponent(cbDepartment, 0, 1);

        cbxUseDepartment.addValueChangeListener(e -> {
            cbDepartment.setReadOnly(!e.getValue());
        });

        GridLayout gridDate = new GridLayout();
        gridDate.setSpacing(true);
        gridDate.removeAllComponents();
        gridDate.setColumns(2);
        gridDate.setRows(2);
        Label dateMin = new Label(i18n.get("reporttab.field.dateMin"));
        gridDate.addComponent(dateMin, 0, 0);
        dfDateMin = new DateField();
        dfDateMin.setReadOnly(true);
        dfDateMin.setDateFormat(configurationService.getDateFormat());
        dfDateMin.setValue(chronometerService.getCurrentTime().toLocalDate());
        dfDateMin.setWidth(150, Unit.PIXELS);
        gridDate.addComponent(dfDateMin, 1, 0);
        Label dateMax = new Label(i18n.get("reporttab.field.dateMax"));
        gridDate.addComponent(dateMax, 0, 1);
        dfDateMax = new DateField();
        dfDateMax.setReadOnly(true);
        dfDateMax.setDateFormat(configurationService.getDateFormat());
        dfDateMax.setValue(chronometerService.getCurrentTime().toLocalDate());
        dfDateMax.setWidth(150, Unit.PIXELS);
        gridDate.addComponent(dfDateMax, 1, 1);

        com.vaadin.ui.Component gridCell = gridDate.getComponent(0, 0);
        gridDate.setComponentAlignment(gridCell, Alignment.MIDDLE_RIGHT);
        gridCell = gridDate.getComponent(0, 1);
        gridDate.setComponentAlignment(gridCell, Alignment.MIDDLE_RIGHT);

        cbxUseMainDate = new CheckBox(getLabelDateDescription());
        cbxUseMainDate.addStyleName(ValoTheme.CHECKBOX_SMALL);

        cbxUseMainDate.addValueChangeListener(e -> {
            dfDateMax.setReadOnly(!e.getValue());
            dfDateMin.setReadOnly(!e.getValue());
        });

        VerticalLayout vlDate = new VerticalLayout(cbxUseMainDate, gridDate);
        vlDate.setSpacing(false);
        vlDate.setMargin(false);

        HorizontalLayout baseSearch = new HorizontalLayout(gridDepartment, vlDate);
        baseSearch.setVisible(getVisibleBaseSearch());

        VerticalLayout baseLayout = new VerticalLayout(getActionLayout(report), baseSearch);
        baseLayout.addStyleName(MaterialTheme.CARD_2);

        baseLayout.setMargin(false);
        baseLayout.setSpacing(true);

        addComponents(baseLayout, reportLayout);
        setExpandRatio(reportLayout, 1);
        setSizeFull();
    }

    protected List<Currency> getLoanCurrencies() {

        if (!cbxUseDepartment.getValue() && !cbxUseMainDate.getValue()) {
            return reportService.findDistinctCurrency();
        }

        if (cbxUseDepartment.getValue() && !cbxUseMainDate.getValue()) {
            return reportService.findDistinctCurrencyByDepartment(cbDepartment.getValue());
        }

        if (!cbxUseDepartment.getValue() && cbxUseMainDate.getValue()) {
            Instant minDate = dfDateMin.getValue().atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
            Instant maxDate = dfDateMax.getValue().plusDays(1).atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();

            return reportService.findDistinctCurrencyByDates(minDate, maxDate);
        }

        if (cbxUseDepartment.getValue() && cbxUseMainDate.getValue()) {
            Instant minDate = dfDateMin.getValue().atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
            Instant maxDate = dfDateMax.getValue().plusDays(1).atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();

            return reportService.findDistinctCurrencyByDepartmentAndDates(cbDepartment.getValue(), minDate, maxDate);
        }

        return new ArrayList<>();
    }

}

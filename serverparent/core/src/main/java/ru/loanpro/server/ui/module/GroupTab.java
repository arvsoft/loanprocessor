package ru.loanpro.server.ui.module;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.security.domain.Group;
import ru.loanpro.security.domain.Role;
import ru.loanpro.security.service.UserService;
import ru.loanpro.server.ui.module.event.UpdateSecurityGroupEvent;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@PrototypeScope
@EntityTab(Group.class)
public class GroupTab extends BaseTab {

    @Autowired
    private UserService userService;

    private Group group;

    public GroupTab(Group group) {
        this.group = (group == null) ? new Group() : group;
    }

    @PostConstruct
    public void init() {
        TextField groupName = new TextField(i18n.get("group.name")) ;
        TextField groupDescription = new TextField(i18n.get("group.description"));
        groupDescription.setWidth(100, Unit.PERCENTAGE);

        Button add = new Button(VaadinIcons.ARROW_LEFT);
        add.setEnabled(false);
        Button remove = new Button(VaadinIcons.ARROW_RIGHT);
        remove.setEnabled(false);

        VerticalLayout actionsLayout = new VerticalLayout(add, remove);
        actionsLayout.setSizeUndefined();
        actionsLayout.setMargin(false);

        // Получаем список всех ролей в системе
        List<Role> allRoles = userService.findAllRoles();

        // Получаем список ролей в данной группе
        List<Role> currentRoles = (group.getId() == null)
            ? new ArrayList<>()
            : userService.findGroupRoles(group.getId());

        // Вычитаем из списка всех ролей системы роли, находящиеся в данной группе
        allRoles.removeAll(currentRoles);

        // Grid текущих ролей группы
        Grid<Role> currentRoleGrid = initGrid(i18n.get("group.current_roles"), currentRoles);
        // Grid всех ролей системы
        Grid<Role> allRoleGrid = initGrid(i18n.get("group.all_roles"), allRoles);

        // Layout содержащий Grid'ы с ролями и кнопками переноса ролей
        HorizontalLayout horizontalLayout = new HorizontalLayout(currentRoleGrid, actionsLayout, allRoleGrid);
        horizontalLayout.setSizeFull();
        horizontalLayout.setExpandRatio(currentRoleGrid, 1);
        horizontalLayout.setExpandRatio(allRoleGrid, 1);
        horizontalLayout.setComponentAlignment(actionsLayout, Alignment.MIDDLE_CENTER);

        Button save = new Button(i18n.get("group.save"));
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        save.setEnabled(group.getId() != null);

        Button saveAndClose = new Button(i18n.get("group.saveAndClose"));
        saveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        saveAndClose.setEnabled(group.getId() != null);

        HorizontalLayout saveLayout = new HorizontalLayout(save, saveAndClose);

        VerticalLayout layout = new VerticalLayout(saveLayout, groupName, groupDescription, horizontalLayout);
        layout.setComponentAlignment(saveLayout, Alignment.TOP_RIGHT);
        layout.setMargin(false);
        layout.setSizeFull();
        layout.setExpandRatio(horizontalLayout, 1);

        addComponents(layout);
        setSizeFull();

        // События изменения статуса кнопок переноса ролей при выделении ролей в Grid'ах
        currentRoleGrid.addSelectionListener(event -> remove.setEnabled(!event.getAllSelectedItems().isEmpty()));
        allRoleGrid.addSelectionListener(event -> add.setEnabled(!event.getAllSelectedItems().isEmpty()));

        // Обработчики событий нажатия на кнопки переноса ролей из одного Grid в другой
        itemSwapListenerSetup(add, allRoles, currentRoles, currentRoleGrid, allRoleGrid);
        itemSwapListenerSetup(remove, currentRoles, allRoles, allRoleGrid, currentRoleGrid);

        // Binder для проверки корректности внесённых даных в имя группы
        Binder<Group> binder = new Binder<>();
        binder.setBean(group);

        binder.forField(groupName)
            .asRequired("not empty")
            .bind(Group::getName, Group::setName);

        binder.bind(groupDescription, Group::getDescription, Group::setDescription);

        binder.addValueChangeListener(event -> {
            save.setEnabled(binder.isValid());
            saveAndClose.setEnabled(binder.isValid());
        });

        save.addClickListener(event -> {
            Group bean = binder.getBean();
            bean.setRoles(currentRoles);
            userService.save(bean);
            sessionEventBus.post(new UpdateSecurityGroupEvent());
        });

        saveAndClose.addClickListener(event -> {
            save.click();
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });
    }

    private Grid<Role> initGrid(String caption, List<Role> roles) {
        Grid<Role> grid = new Grid<>(caption, roles);
        grid.setSizeFull();
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        grid.addColumn(Role::getDescription).setCaption(i18n.get("group.role.description"));
        Grid.Column<Role, String> column = grid.addColumn(Role::getAuthority).setCaption(i18n.get("group.role.name"));
        grid.sort(column, SortDirection.ASCENDING);

        return grid;
    }

    private void itemSwapListenerSetup(Button button, List<Role> listFrom, List<Role> listTo, Grid<Role> gridFrom, Grid<Role> gridTo) {
        button.addClickListener(event -> {
            List<Role> selectedItems = new ArrayList(gridTo.getSelectedItems());
            listTo.addAll(selectedItems);
            gridFrom.setItems(listTo);

            listFrom.removeAll(selectedItems);
            gridTo.setItems(listFrom);
            button.setEnabled(false);
        });
    }

    @Override
    public String getTabName() {
        if (group.getId() == null)
            return i18n.get("group.tabName.new");
        else
            return String.format(i18n.get("group.tabName.edit"), group.getName());
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.PINK;
    }
}

package ru.loanpro.server.ui.module;

import com.google.common.eventbus.Subscribe;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.loan.domain.AbstractRequest;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.loan.event.UpdateRequestStatusEvent;
import ru.loanpro.base.loan.exceptions.ObjectNotSavedException;
import ru.loanpro.base.loan.repository.RequestRepository;
import ru.loanpro.base.requesthistory.serivce.RequestHistoryService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.Enable;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.directory.RequestStatus;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.security.event.UpdateDepartmentRequestSequenceEvent;
import ru.loanpro.security.service.SecurityService;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.server.ui.module.additional.ConfirmDialog;
import ru.loanpro.server.ui.module.additional.InstantLocalDateConverter;
import ru.loanpro.server.ui.module.component.RequestDecision;
import ru.loanpro.uibasis.event.AddTabEvent;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;

/**
 * Вкладка работы с заявкой
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(Request.class)
public class RequestTab extends BaseLoanTab<Request, RequestRepository> {

    @Autowired
    private RequestHistoryService requestHistoryService;

    @Autowired
    private SecurityService securityService;

    private TextField status;

    @Visible(Roles.Request.PRINT)
    private Button print;

    @Visible(Roles.Request.DECISION)
    private Button decision;

    @Visible(Roles.Request.CREATE_LOAN)
    private Button createLoan;

    @Enable(Roles.Request.CHANGE_NUMBER)
    private TextField requestNumber;

    public RequestTab(Request request) {
        entity = (request == null) ? new Request() : request;
    }

    @PostConstruct
    @Override
    public void init() {
        super.init();
        applicationEventBus.register(this);
    }

    @Override
    protected Layout initBaseInformation() {
        Label mainInfo = new Label(i18n.get("request.panel.maininfo.title"));

        print = new Button(i18n.get("request.button.print"), VaadinIcons.PRINT);
        print.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        decision = new Button(i18n.get("request.button.decision"), VaadinIcons.GAVEL);
        decision.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        decision.addClickListener(event -> {
            if (entity != null) {
                RequestDecision requestDecision = new RequestDecision(i18n, entity, requestHistoryService,
                    chronometerService, securityService, applicationEventBus);
                UI.getCurrent().addWindow(requestDecision);
            }
        });

        delete = new Button(i18n.get("loan.button.delete"), VaadinIcons.TRASH);
        delete.setEnabled(baseLoanService.canBeDeleted(entity));
        delete.addStyleNames(ValoTheme.BUTTON_BORDERLESS_COLORED);
        delete.addClickListener(event -> {
            ConfirmDialog dialog = new ConfirmDialog(
                i18n.get("request.dialogDelete.caption"),
                i18n.get("request.dialogDelete.description"),
                () -> {
                    try {
                        baseLoanService.delete(entity);
                        Notification.show(i18n.get("request.notification.deleteSuccessful"), Type.TRAY_NOTIFICATION);
                        sessionEventBus.post(new RemoveTabEvent<>(this));
                    } catch (EntityDeleteException e) {
                        Notification.show(i18n.get("request.notification.deletedError"), Type.ERROR_MESSAGE);
                    }
                });
            dialog.setConfirmButtonIcon(VaadinIcons.TRASH);
            dialog.setClosable(true);
            dialog.show();
        });

        createLoan = new Button(i18n.get("loan.button.createLoan"));
        createLoan.setIcon(VaadinIcons.WALLET);
        createLoan.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        createLoan.addClickListener(event -> {
            sessionEventBus.post(new AddTabEvent(LoanTab.class, new Loan(entity)));
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        HorizontalLayout hlButtonAll = getActionButtonsLayout(delete, decision, createLoan);
        edit.setVisible(securityService.hasAuthority(Roles.Request.EDIT));
        delete.setVisible(securityService.hasAuthority(Roles.Request.DELETE));
        save.addClickListener(event -> delete.setEnabled(baseLoanService.canBeDeleted(entity)));

        HorizontalLayout hlActions = new HorizontalLayout(mainInfo, hlButtonAll);
        hlActions.setWidth(100, Unit.PERCENTAGE);
        hlActions.setExpandRatio(hlButtonAll, 1.0f);

        Label currentDepartment = new Label(String.format(
            i18n.get("request.label.department"), entity.getDepartment().getName()));

        status = new TextField(i18n.get("request.textfield.status"));
        status.setValue(i18n.get(entity.getRequestStatus().getName()));
        status.setReadOnly(true);

        requestNumber = new TextField(i18n.get("request.textfield.requestnumber"));
        requestNumber.setWidth(130, Unit.PIXELS);
        requestNumber.setStyleName("v-align-right");

        DateField issueDate = new DateField(i18n.get("request.datefield.issuedate"));
        issueDate.setWidth(200, Unit.PIXELS);
        issueDate.setDateFormat(configurationService.getDateFormat());
        issueDate.setEnabled(false);

        HorizontalLayout horizontalLayout = new HorizontalLayout(status, requestNumber, issueDate);
        VerticalLayout layout = new VerticalLayout(hlActions, currentDepartment, horizontalLayout);
        layout.setSizeFull();

        binder.bind(requestNumber, AbstractRequest::getRequestNumber, AbstractRequest::setRequestNumber);

        binder.forField(issueDate)
            .withConverter(new InstantLocalDateConverter(chronometerService.getCurrentZoneId()))
            .bind(Request::getRequestIssueDate, Request::setRequestIssueDate);

        return layout;
    }

    @Override
    protected void buttonsStatusUpdate() {
        super.buttonsStatusUpdate();

        boolean isNew = entity.getId() == null;
        boolean isApproved = entity.getRequestStatus() == RequestStatus.APPROVED;
        boolean isLoanIssued = entity.getRequestStatus() == RequestStatus.LOAN_ISSUED;

        print.setEnabled(!isNew && edit.isEnabled());
        decision.setEnabled(!isNew && edit.isEnabled() && !isLoanIssued);
        createLoan.setEnabled(isApproved && edit.isEnabled() && !isLoanIssued);
    }

    @Override
    public void destroy() {
        super.destroy();

        applicationEventBus.unregister(this);

        if (entity.getId() == null)
            numberService.clearRequestSequenceDraftNumber(entity.getRequestNumber());
    }

    @Override
    public String getTabName() {
        if (entity.getId() == null) {
            return i18n.get("request.tab.add.caption");
        } else {
            return i18n.get("request.tab.edit.caption") + entity.getRequestNumber();
        }
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.PINK;
    }

    /**
     * Обработчик обновления последовательности номеров. Проверяет, если обновлённая последовательность
     * принадлежит текущему отделу, то запрашивает новый временный номер для заявки.  Распространяется только
     * на создаваемые заявки.
     *
     * @param event событие обновления последовательности.
     */
    @Subscribe
    private void updateSequenceEventHandler(UpdateSequenceEvent event) {
        if (entity.getId() != null)
            return;
        if (securityService.getDepartment().getRequestSequence().getId().equals(event.getSequence().getId())) {
            entity.setRequestNumber(numberService.getRequestSequenceDraftNumber());
            binder.setBean(binder.getBean());
        }
    }

    /**
     * Обработчик изменения последовательности у отдела. Проверяет, если отдел совпадает с текущим отделом,
     * то запрашивает новый временный номер для заявки. Распространяется только на создаваемые заявки.
     *
     * @param event событие изменения последовательности у отдела.
     */
    @Subscribe
    private void updateDepartmentLoanSequenceEventHandler(UpdateDepartmentRequestSequenceEvent event) {
        if (entity.getId() != null)
            return;
        if (securityService.getDepartment().getId().equals(event.getDepartment().getId())) {
            draftNumber = numberService.getRequestSequenceDraftNumber();
            entity.setRequestNumber(draftNumber);
            binder.setBean(binder.getBean());
        }
    }

    /**
     * Обработчик изменения статуса заявки при выполнении функции принятия решения по заявке или при
     * сохранении займа из заявки
     * Меняет надпись Статус заявки
     * AccountNotFoundException не используется
     * @param event сущность заявки с измененным статусом
     */
    @Subscribe
    private void updateRequestStatusEventHandler(UpdateRequestStatusEvent event) {
        if (entity.getId().equals(event.getRequest().getId())) {
            try {
                baseLoanService.save(entity, draftNumber);
                status.setValue(i18n.get(event.getRequest().getRequestStatus().getName()));
                createLoan.setEnabled(entity.getId() != null && entity.getRequestStatus() == RequestStatus.APPROVED);
            } catch (ObjectNotSavedException e) {
                Notification.show(i18n.get(e.getLocalizedMessage()), Type.ERROR_MESSAGE);
                e.printStackTrace();
            }
        }
    }
}

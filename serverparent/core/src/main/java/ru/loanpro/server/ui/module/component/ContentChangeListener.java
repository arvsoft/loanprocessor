package ru.loanpro.server.ui.module.component;


/**
 * Интерфейс, выполняющий роль колбека для передачи события изменения контента из элементов
 * интерфейса в список займов
 *
 * @author Maksim Askhaev
 */
public interface ContentChangeListener {
    /**
     * Индикатор изменения контента.
     */
    void contentChanged();
}

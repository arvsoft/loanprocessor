package ru.loanpro.server.ui.module.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.global.directory.ScheduleStatus;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.event.ScheduleRecalculatedEvent;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Окно конструктора создания индивидуального графика платежей (вариант 2).
 * Конструктор вызывается из формы займа, заявки и конструктора кредитных продуктов.
 * Конструктор открывается при нажатии на кнопку Calculate в форме расчета графика платежей (ScheduleForm)
 * при условии, что в поле ReturnTerm выбран пункт Individually
 *
 * @author Maksim Askhaev
 */
public class ScheduleManual<T extends BaseLoan> extends Window {

    private BigDecimal currentBalance;
    private LocalDate currentDate;
    private int paymentNumber;

    public ScheduleManual(ScheduleForm<T> scheduleForm, T entity, I18N i18n, ConfigurationService configurationService,
                          BigDecimal loanBalance, BigDecimal interestRate, LocalDate issueDate, boolean interestBalance,
                          boolean includeIssueDate, SessionEventBus sessionEventBus) {

        super(i18n.get("scheduleManual.window.caption"));
        setModal(true);
        center();

        DecimalFormat moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);

        paymentNumber = 1;
        currentDate = issueDate;
        currentBalance = loanBalance;

        GridLayout gridLayout = new GridLayout();
        gridLayout.setSpacing(true);
        gridLayout.removeAllComponents();
        gridLayout.setColumns(3);
        gridLayout.setRows(8);

        Label lbPaymentNumber = new Label(i18n.get("scheduleManual.textfield.paymentnumber"));
        gridLayout.addComponent(lbPaymentNumber, 1, 0);
        TextField tfPaymentNumber = new TextField();
        tfPaymentNumber.addStyleName("align-right");
        tfPaymentNumber.setWidth(80, Unit.PIXELS);
        tfPaymentNumber.setReadOnly(true);
        gridLayout.addComponent(tfPaymentNumber, 2, 0);

        Label lbBalanceBefore = new Label(i18n.get("scheduleManual.textfield.balancebefore"));
        gridLayout.addComponent(lbBalanceBefore, 0, 1);
        MoneyField tfBalanceBefore = new MoneyField(null, configurationService.getLocale());
        tfBalanceBefore.setWidth(150, Unit.PIXELS);
        tfBalanceBefore.setReadOnly(true);
        gridLayout.addComponent(tfBalanceBefore, 1, 1);

        Label lbPaymentDate = new Label(i18n.get("scheduleManual.textfield.paymentdate"));
        gridLayout.addComponent(lbPaymentDate, 0, 2);
        DateField dfPaymentDate = new DateField();
        dfPaymentDate.setDateFormat(configurationService.getDateFormat());
        dfPaymentDate.setWidth(150, Unit.PIXELS);
        gridLayout.addComponent(dfPaymentDate, 1, 2);
        Slider slPaymentDays = new Slider();
        slPaymentDays.setWidth(150, Unit.PIXELS);
        gridLayout.addComponent(slPaymentDays, 2, 2);

        Label lbInterest = new Label(i18n.get("scheduleManual.textfield.interest"));
        gridLayout.addComponent(lbInterest, 0, 3);
        MoneyField tfInterest = new MoneyField(null, configurationService.getLocale());
        tfInterest.setWidth(150, Unit.PIXELS);
        tfInterest.setReadOnly(true);
        gridLayout.addComponent(tfInterest, 1, 3);

        Label lbPrincipal = new Label(i18n.get("scheduleManual.textfield.principal"));
        gridLayout.addComponent(lbPrincipal, 0, 4);
        MoneyField tfPrincipal = new MoneyField(null, configurationService.getLocale());
        tfPrincipal.setWidth(150, Unit.PIXELS);
        gridLayout.addComponent(tfPrincipal, 1, 4);
        Button btPrincipalFull = new Button();
        btPrincipalFull.addStyleName(ValoTheme.BUTTON_SMALL);
        btPrincipalFull.setIcon(VaadinIcons.CHECK);
        btPrincipalFull.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        gridLayout.addComponent(btPrincipalFull, 2, 4);

        Label lbAmount = new Label(i18n.get("scheduleManual.textfield.amount"));
        gridLayout.addComponent(lbAmount, 0, 5);
        MoneyField tfAmount = new MoneyField(null, configurationService.getLocale());
        tfAmount.setWidth(150, Unit.PIXELS);
        tfAmount.setReadOnly(true);
        gridLayout.addComponent(tfAmount, 1, 5);
        CheckBox cbxAmountManual = new CheckBox(i18n.get("scheduleManual.checkbox.amountmanual"));
        cbxAmountManual.addStyleName(ValoTheme.CHECKBOX_SMALL);
        gridLayout.addComponent(cbxAmountManual, 2, 5);

        Label lbBalanceAfter = new Label(i18n.get("scheduleManual.textfield.balanceafter"));
        gridLayout.addComponent(lbBalanceAfter, 0, 6);
        MoneyField tfBalanceAfter = new MoneyField(null, configurationService.getLocale());
        tfBalanceAfter.setReadOnly(true);
        tfBalanceAfter.setWidth(150, Unit.PIXELS);
        gridLayout.addComponent(tfBalanceAfter, 1, 6);

        com.vaadin.ui.Component gridCell = gridLayout.getComponent(1, 0);
        if (gridCell != null) gridLayout.setComponentAlignment(gridCell, Alignment.MIDDLE_RIGHT);

        for (int i = 1; i < 7; i++) {
            gridCell = gridLayout.getComponent(0, i);
            if (gridCell != null) gridLayout.setComponentAlignment(gridCell, Alignment.MIDDLE_RIGHT);
        }

        gridCell = gridLayout.getComponent(2, 4);
        if (gridCell != null) gridLayout.setComponentAlignment(gridCell, Alignment.MIDDLE_CENTER);

        gridCell = gridLayout.getComponent(2, 5);
        if (gridCell != null) gridLayout.setComponentAlignment(gridCell, Alignment.MIDDLE_CENTER);

        Button btDeletePayment = new Button(i18n.get("scheduleManual.button.deletepayment"));
        btDeletePayment.setIcon(VaadinIcons.CLOSE_SMALL);
        btDeletePayment.addStyleName(ValoTheme.BUTTON_SMALL);
        btDeletePayment.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        Button btAddPayment = new Button(i18n.get("scheduleManual.button.addpayment"));
        btAddPayment.setIcon(VaadinIcons.PLUS);
        btAddPayment.addStyleName(ValoTheme.BUTTON_SMALL);
        btAddPayment.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        HorizontalLayout hlLayout1 = new HorizontalLayout();
        hlLayout1.setSizeUndefined();
        hlLayout1.addComponents(btDeletePayment, btAddPayment);
        hlLayout1.setExpandRatio(btDeletePayment, 0.5f);
        hlLayout1.setExpandRatio(btAddPayment, 0.5f);

        VerticalLayout vlBlock8 = new VerticalLayout();
        vlBlock8.setSizeUndefined();
        vlBlock8.setMargin(true);

        vlBlock8.addComponents(gridLayout, hlLayout1);

        //начальные параметры
        tfPaymentNumber.setValue(String.valueOf(paymentNumber));
        dfPaymentDate.setRangeStart(currentDate);
        dfPaymentDate.setValue(currentDate);
        slPaymentDays.setMin(0);
        slPaymentDays.setMax(31);
        tfInterest.setValue(moneyFormat.format(0));
        tfPrincipal.setValue(moneyFormat.format(0));
        tfAmount.setValue(moneyFormat.format(0));
        tfBalanceBefore.setValue(moneyFormat.format(loanBalance.doubleValue()));
        tfBalanceAfter.setValue(moneyFormat.format(loanBalance.doubleValue()));
        btAddPayment.setEnabled(false);
        btDeletePayment.setEnabled(false);

        slPaymentDays.addValueChangeListener(e -> {
            long term = e.getValue().longValue();
            dfPaymentDate.setValue(currentDate.plusDays(term));//далее событие dfPaymentDate.addValueChangeListener
            //сбрасываем ручной режим ввода суммы платежа
            cbxAmountManual.setValue(false);

        });

        dfPaymentDate.addValueChangeListener(e -> {
            long term = ChronoUnit.DAYS.between(currentDate, e.getValue());
            if (term <= slPaymentDays.getMax()) {
                slPaymentDays.setValue((double) term);
            }

            long add_day = 0;
            if (scheduleForm.getScheduleList().size() == 0 && includeIssueDate == true) {
                add_day = 1;
            }

            double loanBase = currentBalance.doubleValue();
            if (interestBalance) {
                loanBase = loanBalance.doubleValue();
            }

            double interest = loanBase * (term + add_day) * interestRate.doubleValue() / 100;
            tfInterest.setValue(moneyFormat.format(interest));

            String sPrincipal = tfPrincipal.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double principal = Double.valueOf(sPrincipal);
            double amount = interest + principal;
            tfAmount.setValue(moneyFormat.format(amount));
        });

        btPrincipalFull.addClickListener(e -> {
            String sInterest = tfInterest.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double interest = Double.valueOf(sInterest);
            double amount = currentBalance.doubleValue() + interest;

            tfPrincipal.setValue(moneyFormat.format(currentBalance.doubleValue()));
            tfAmount.setValue(moneyFormat.format(amount));
            tfBalanceAfter.setValue(moneyFormat.format(0));
            //сбрасываем ручной режим ввода суммы платежа
            cbxAmountManual.setValue(false);
        });

        tfPrincipal.addBlurListener(e -> {
            String sInterest = tfInterest.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double interest = Double.valueOf(sInterest);

            String sPrincipal = tfPrincipal.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double principal = Double.valueOf(sPrincipal);
            if (principal > currentBalance.doubleValue()) {
                principal = currentBalance.doubleValue();
            }
            tfPrincipal.setValue(moneyFormat.format(principal));

            double amount = principal + interest;
            tfAmount.setValue(moneyFormat.format(amount));

            double balanceAfter = currentBalance.doubleValue() - principal;
            tfBalanceAfter.setValue(moneyFormat.format(balanceAfter));
        });

        tfAmount.addBlurListener(e -> {
            String sAmount = tfAmount.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double amount = Double.valueOf(sAmount);
            String sInterest = tfInterest.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double interest = Double.valueOf(sInterest);

            if (amount - interest > currentBalance.doubleValue()) {
                amount = currentBalance.doubleValue() - interest;
                tfAmount.setValue(moneyFormat.format(amount));
            }
            if (amount < interest) {
                amount = interest;
                tfAmount.setValue(moneyFormat.format(amount));
            }

            double principal = amount - interest;
            tfPrincipal.setValue(moneyFormat.format(principal));

            double balanceAfter = currentBalance.doubleValue() - principal;
            tfBalanceAfter.setValue(moneyFormat.format(balanceAfter));
        });

        cbxAmountManual.addValueChangeListener(e -> {
            tfAmount.setReadOnly(!e.getValue());
            if (e.getValue() == true) {
                tfAmount.focus();
            }
        });

        tfAmount.addValueChangeListener(e -> {
            String sAmount = tfAmount.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double amount = Double.valueOf(sAmount);
            if (amount > 0) {
                btAddPayment.setEnabled(true);
            } else {
                btAddPayment.setEnabled(false);
            }
        });

        btAddPayment.addClickListener(event -> {
            Schedule schedule = new Schedule();
            schedule.setOrdNo(paymentNumber);
            schedule.setStatus(ScheduleStatus.NOT_PAID_NO_OVERDUE);
            schedule.setDate(dfPaymentDate.getValue());
            long term = ChronoUnit.DAYS.between(currentDate, dfPaymentDate.getValue());
            schedule.setTerm(term);
            String sPrincipal = tfPrincipal.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double principal = Double.valueOf(sPrincipal);
            schedule.setPrincipal(new BigDecimal(principal).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            String sInterest = tfInterest.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double interest = Double.valueOf(sInterest);
            schedule.setInterest(new BigDecimal(interest).setScale(2, BigDecimal.ROUND_HALF_DOWN));
            String sAmount = tfAmount.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double amount = Double.valueOf(sAmount);
            schedule.setPayment(new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_DOWN));

            //String sBalanceAfter = tfBalanceAfter.getValue().replaceAll("[^\\d.]+", "");
            String sBalanceAfter = tfBalanceAfter.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
            double balanceAfter = Double.valueOf(sBalanceAfter);
            schedule.setBalance(new BigDecimal(balanceAfter).setScale(2, BigDecimal.ROUND_HALF_DOWN));

            //вычисляем новые параметры
            paymentNumber++;
            currentBalance = new BigDecimal(balanceAfter).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            tfBalanceBefore.setValue(moneyFormat.format(currentBalance.doubleValue()));
            currentDate = dfPaymentDate.getValue();
            dfPaymentDate.setRangeStart(currentDate);
            slPaymentDays.setValue(0D);
            tfInterest.setValue(moneyFormat.format(0));
            tfPrincipal.setValue(moneyFormat.format(0));
            tfAmount.setValue(moneyFormat.format(0));
            tfBalanceAfter.setValue(moneyFormat.format(currentBalance.doubleValue()));
            btAddPayment.setEnabled(false);
            btDeletePayment.setEnabled(false);
            scheduleForm.getScheduleList().add(schedule);
            scheduleForm.getGridSchedule().setItems(scheduleForm.getScheduleList());

            // Проверка, вычислен ли график полностью
            if (Math.round(balanceAfter) == 0) {
                slPaymentDays.setEnabled(false);
                btPrincipalFull.setEnabled(false);
                dfPaymentDate.setEnabled(false);
                tfPrincipal.setReadOnly(true);
                cbxAmountManual.setEnabled(false);
                scheduleForm.setSchedule();
                scheduleForm.setInfo();

                int loanTerm = (int)scheduleForm.getScheduleList().stream().mapToLong(Schedule::getTerm).reduce(0, Long::sum);
                entity.setLoanTerm(loanTerm);
                scheduleForm.getBinder().setBean(entity);

                sessionEventBus.post(new ScheduleRecalculatedEvent<>(entity));
                Notification.show(i18n.get("scheduleForm.notification.message.scheduleCalculated"), Notification.Type.TRAY_NOTIFICATION);
            } else {
                tfPaymentNumber.setValue(String.valueOf(paymentNumber));
            }

            if (scheduleForm.getScheduleList().size() > 0) {
                btDeletePayment.setEnabled(true);
            }
        });

        btDeletePayment.addClickListener(event -> {
            paymentNumber--;
            if (scheduleForm.getScheduleList().size() == 1) {
                //получаем начальные данные для расчета
                currentDate = issueDate;
                currentBalance = loanBalance;
                tfBalanceBefore.setValue(moneyFormat.format(loanBalance.doubleValue()));
                tfBalanceAfter.setValue(moneyFormat.format(loanBalance.doubleValue()));
                btDeletePayment.setEnabled(false);
            } else if (scheduleForm.getScheduleList().size() > 1) {
                currentDate = scheduleForm.getScheduleList().get(paymentNumber - 2).getDate();
                currentBalance = currentBalance.add(scheduleForm.getScheduleList().get(paymentNumber - 1).getPrincipal());
                tfBalanceBefore.setValue(moneyFormat.format(currentBalance.doubleValue()));
                tfBalanceAfter.setValue(moneyFormat.format(currentBalance.doubleValue()));
                btDeletePayment.setEnabled(true);
            }

            slPaymentDays.setEnabled(true);
            btPrincipalFull.setEnabled(true);
            dfPaymentDate.setEnabled(true);
            tfPrincipal.setReadOnly(false);
            cbxAmountManual.setEnabled(true);

            tfPaymentNumber.setValue(String.valueOf(paymentNumber));
            dfPaymentDate.setRangeStart(currentDate);
            dfPaymentDate.setValue(currentDate);
            slPaymentDays.setValue(0D);
            tfInterest.setValue(moneyFormat.format(0));
            tfPrincipal.setValue(moneyFormat.format(0));
            tfAmount.setValue(moneyFormat.format(0));
            btAddPayment.setEnabled(false);
            scheduleForm.getScheduleList().remove(paymentNumber - 1);
            scheduleForm.getGridSchedule().setItems(scheduleForm.getScheduleList());
        });

        addCloseListener(event -> {
            if (currentBalance.intValue() > 0) {
                //если график рассчитан не до конца, очищаем
                scheduleForm.getScheduleList().clear();
                scheduleForm.getGridSchedule().setItems(scheduleForm.getScheduleList());
            }
        });

        //стиль ко всей странице
        addStyleName(MaterialTheme.LABEL_COLORED);
        setContent(vlBlock8);
    }
}

package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Address;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.global.directory.AddressUseStatus;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования адресов физического лица.
 *
 * @author Maksim Askhaev
 */
public class AddressDetailsLayout extends HorizontalLayout {

    private final Set<Address> addresses;
    private final I18N i18n;
    private final StorageService storageService;
    private final User user;
    private final ChronometerService chronometerService;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Address> grid;
    private ContentChangeListener listener;
    private final ConfigurationService configurationService;

    public AddressDetailsLayout(User user, ChronometerService chronometerService, Set<Address> addresses, I18N i18n,
                                ConfigurationService configurationService, StorageService storageService) {
        this.configurationService = configurationService;
        this.storageService = storageService;

        DateTimeFormatter dateFormatter =
            DateTimeFormatter.ofPattern(configurationService.getDateFormat(), configurationService.getLocale());

        this.user = user;
        this.chronometerService = chronometerService;
        this.i18n = i18n;
        this.addresses = addresses;
        setHeight(200, Unit.PIXELS);

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(addresses);
        grid.addColumn(item -> i18n.get(item.getStatus().getName())).setCaption(i18n.get("client.addressdetails.field.status"));
        grid.addColumn(Address::getAll).setCaption(i18n.get("client.addressdetails.field.alladdress"));
        grid.addColumn(item -> item.getResideDate() == null ? "" : dateFormatter.format(item.getResideDate()))
            .setCaption(i18n.get("client.addressdetails.field.residedate"));
        grid.addItemClickListener(event -> {
            closeForm();

            Address item = event.getItem();
            if (item != null) {
                form = getAddressForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.addressdetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Address address = new Address();
            address.setStatus(AddressUseStatus.LIVING);
            form = getAddressForm(address);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getAddressForm(Address address) {

        ComboBox<AddressUseStatus> status = new ComboBox<>(i18n.get("client.addressdetails.field.status"));
        status.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        status.setEmptySelectionAllowed(false);
        status.setItems(AddressUseStatus.values());
        status.setSelectedItem(address.getStatus());
        TextField zip = new TextField(i18n.get("client.addressdetails.field.zip"));
        TextField street = new TextField(i18n.get("client.addressdetails.field.street"));
        TextField house = new TextField(i18n.get("client.addressdetails.field.house"));
        TextField block = new TextField(i18n.get("client.addressdetails.field.block"));
        TextField appartment = new TextField(i18n.get("client.addressdetails.field.appartment"));
        TextField borough = new TextField(i18n.get("client.addressdetails.field.borough"));
        TextField city = new TextField(i18n.get("client.addressdetails.field.city"));
        TextField county = new TextField(i18n.get("client.addressdetails.field.county"));
        TextField state = new TextField(i18n.get("client.addressdetails.field.state"));
        TextField country = new TextField(i18n.get("client.addressdetails.field.country"));
        DateField residedate = new DateField(i18n.get("client.addressdetails.field.residedate"));
        residedate.setDateFormat(configurationService.getDateFormat());
        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        Button save = new Button(i18n.get("client.addressdetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.addressdetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(status, zip, street, house, block, appartment, borough,
            city, county, state, country, residedate, scansUpload, layout);
        formLayout.setSizeFull();

        Binder<Address> binder = new Binder<>();

        binder.forField(status)
            .bind(Address::getStatus, Address::setStatus);
        binder.forField(zip)
            .bind(Address::getZip, Address::setZip);
        binder.forField(street)
            .bind(Address::getStreet, Address::setStreet);
        binder.forField(house)
            .bind(Address::getHouse, Address::setHouse);
        binder.forField(block)
            .bind(Address::getBlock, Address::setBlock);
        binder.forField(appartment)
            .bind(Address::getAppartment, Address::setAppartment);
        binder.forField(borough)
            .bind(Address::getBorough, Address::setBorough);
        binder.forField(city)
            .bind(Address::getCity, Address::setCity);
        binder.forField(county)
            .bind(Address::getCounty, Address::setCounty);
        binder.forField(state)
            .bind(Address::getState, Address::setState);
        binder.forField(country)
            .bind(Address::getCountry, Address::setCountry);
        binder.forField(residedate)
            .bind(Address::getResideDate, Address::setResideDate);
        binder.forField(scansUpload)
            .bind(Address::getScans, Address::setScans);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(address);

        save.addClickListener(event -> {
            if (addresses.stream().noneMatch(item -> item.hashCode() == address.hashCode()))
                addresses.add(address);

            grid.setItems(addresses);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}
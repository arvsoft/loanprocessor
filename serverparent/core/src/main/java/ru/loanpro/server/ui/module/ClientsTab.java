package ru.loanpro.server.ui.module;

import com.google.common.eventbus.Subscribe;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.SingleSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.domain.ClientQueryDSL;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientQueryDSL;
import ru.loanpro.base.client.domain.QPhysicalClient;
import ru.loanpro.base.client.service.PhysicalClientService;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.global.directory.AddressUseStatus;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.global.directory.DocumentStatus;
import ru.loanpro.global.directory.Gender;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.server.ui.module.event.UpdateClientEvent;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.AddTabEvent;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Вкладка для работы с клиентами.
 *
 * @author Oleg Brizhevatikh
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.clients")
public class ClientsTab extends BaseTab {

    private final PhysicalClientService clientService;

    private Grid<PhysicalClient> gridClients;

    private ComboBox<ClientRating> rating;
    private TextField displayName;
    private DateField birthDate;
    private ComboBox<Gender> gender;
    private TextField mobilePhone;
    private TextField workPhone;
    private TextField SSSNumber;
    private TextField taxNumber;
    private TextField birthPlace;
    private TextField document;
    private TextField address;

    @Autowired
    public ClientsTab(PhysicalClientService clientService) {
        this.clientService = clientService;
    }

    @PostConstruct
    public void init() {
        DateTimeFormatter dateTimeFormatter =
            DateTimeFormatter.ofPattern(configurationService.getDateFormat(), configurationService.getLocale());

        Button btAddNewPhysicalClient = new Button(i18n.get("clients.button.addPhysicalClient"));
        btAddNewPhysicalClient.setIcon(VaadinIcons.PLUS);
        btAddNewPhysicalClient.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        btAddNewPhysicalClient.addClickListener(event ->
            sessionEventBus.post(new AddTabEvent(PhysicalClientTab.class)));

        Button btViewClient = new Button(i18n.get("clients.button.viewclient"));
        btViewClient.setIcon(VaadinIcons.EDIT);
        btViewClient.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        btViewClient.addClickListener(event -> {
            SingleSelect<PhysicalClient> selection = gridClients.asSingleSelect();
            Client client = selection.getValue();

            if (client != null) {
                sessionEventBus.post(new AddTabEvent(PhysicalClientTab.class, client));
            } else {
                Notification.show(i18n.get("clients.warning.needselectitem"));
            }
        });

        HorizontalLayout layout = new HorizontalLayout(btAddNewPhysicalClient, btViewClient);

        gridClients = new Grid<>();
        gridClients.setSizeFull();

        float k = 1.0f;
        if (configurationService.getGridStyle() == GridStyle.SMALL) {
            gridClients.addStyleName("v-grid-style-small");
            gridClients.setRowHeight(40f);
        } else k = 1.3f;

        Column<PhysicalClient, String> colRating = gridClients.addColumn(item -> i18n.get(item.getRating().getName()));
        colRating.setSortProperty("rating")
            .setCaption(i18n.get("clients.grid.title.rating"))
            .setStyleGenerator(item -> item.getRating().getColor())
            .setWidth(150 * k);

        Column<PhysicalClient, String> colDisplayName = gridClients.addColumn(PhysicalClient::getDisplayName);
        colDisplayName.setSortProperty("lastName")
            .setCaption(i18n.get("clients.grid.title.displayName"))
            .setWidth(250 * k);

        Column<PhysicalClient, String> colBirthDate = gridClients.addColumn(item ->
            item.getBirthDate() == null ? "" : item.getBirthDate().format(dateTimeFormatter));
        colBirthDate.setSortProperty("birthDate")
            .setCaption(i18n.get("clients.grid.title.birthdate"))
            .setWidth(120 * k);

        Column<PhysicalClient, String> colGender = gridClients.addColumn(item -> i18n.get(item.getGender().getName()));
        colGender.setSortProperty("gender")
            .setCaption(i18n.get("clients.grid.title.gender"))
            .setWidth(120 * k);

        Column<PhysicalClient, String> colMobilePhone = gridClients.addColumn(item -> item.getDetails().getMobilePhone());
        colMobilePhone
            .setCaption(i18n.get("clients.grid.title.mobilePhone"))
            .setWidth(150 * k);

        Column<PhysicalClient, String> colWorkPhone = gridClients.addColumn(item -> item.getDetails().getWorkPhone());
        colWorkPhone
            .setCaption(i18n.get("clients.grid.title.workPhone"))
            .setWidth(150 * k);

        Column<PhysicalClient, String> colSSNNumber = gridClients.addColumn(item -> item.getDetails().getSSNNumber());
        colSSNNumber
            .setCaption(i18n.get("clients.grid.title.SSNNumber"))
            .setWidth(120 * k);

        Column<PhysicalClient, String> colTaxNumber = gridClients.addColumn(item -> item.getDetails().getTaxNumber());
        colTaxNumber
            .setCaption(i18n.get("clients.grid.title.taxNumber"))
            .setWidth(120 * k);

        Column<PhysicalClient, String> colBirthPlace = gridClients.addColumn(item -> item.getDetails().getBirthPlace());
        colBirthPlace
            .setCaption(i18n.get("clients.grid.title.birthPlace"))
            .setWidth(250 * k);

        Column<PhysicalClient, String> colDocuments = gridClients
            .addColumn(item -> item.getDetails().getDocuments().stream()
                .filter(f -> f.getStatus() == DocumentStatus.ACTUAL)
                .sorted((o1, o2) -> {
                    if (o1.getNumber() == null) return o2.getNumber() == null ? 0 : -1;
                    if (o2.getNumber() == null) return 1;

                    int res = String.CASE_INSENSITIVE_ORDER.compare(o1.getNumber(), o2.getNumber());
                    if (res == 0) {
                        res = o1.getNumber().compareTo(o2.getNumber());
                    }
                    return res;
                })
                .map(i -> Stream.of(i.getDocumentType().getName(), i.getSeries(), i.getNumber())
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList()))
                .map(i -> String.join(" ", i))
                .collect(Collectors.joining("; "))
            );
        colDocuments
            .setCaption(i18n.get("clients.grid.title.documents"))
            .setWidth(500 * k);

        Column<PhysicalClient, String> colAddresses = gridClients
            .addColumn(item -> item.getDetails().getAddresses().stream()
                .filter(f -> f.getStatus() != AddressUseStatus.EXPIRED)
                .sorted((o1, o2) -> {
                    if (o1.getStreet() == null) return o2.getStreet() == null ? 0 : -1;
                    if (o2.getStreet() == null) return 1;

                    int res = String.CASE_INSENSITIVE_ORDER.compare(o1.getStreet(), o2.getStreet());
                    if (res == 0) {
                        res = o1.getStreet().compareTo(o2.getStreet());
                    }
                    return res;
                })
                .map(i -> Stream.of(i.getStreet(), i.getHouse(), i.getBlock(), i.getAppartment(), i.getCity())
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList()))
                .map(i -> String.join(" ", i))
                .collect(Collectors.joining("; "))

            );
        colAddresses
            .setCaption(i18n.get("clients.grid.title.addresses"))
            .setWidth(500 * k);

        HeaderRow headerRow = gridClients.addHeaderRowAt(1);

        rating = new ComboBox<>();
        rating.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        rating.setItems(ClientRating.values());
        rating.setWidth(String.valueOf(colRating.getWidth() * 0.9f));
        headerRow.getCell(colRating).setComponent(rating);

        displayName = new TextField();
        displayName.setWidth(String.valueOf(colDisplayName.getWidth() * 0.9f));
        headerRow.getCell(colDisplayName).setComponent(displayName);

        birthDate = new DateField();
        birthDate.setDateFormat(configurationService.getDateFormat());
        birthDate.setWidth(String.valueOf(colBirthDate.getWidth() * 0.9f));
        headerRow.getCell(colBirthDate).setComponent(birthDate);

        gender = new ComboBox<>();
        gender.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        gender.setItems(Gender.values());
        gender.setWidth(String.valueOf(colGender.getWidth() * 0.9f));
        headerRow.getCell(colGender).setComponent(gender);

        mobilePhone = new TextField();
        mobilePhone.setWidth(String.valueOf(colMobilePhone.getWidth() * 0.9f));
        headerRow.getCell(colMobilePhone).setComponent(mobilePhone);

        workPhone = new TextField();
        mobilePhone.setWidth(String.valueOf(colWorkPhone.getWidth() * 0.9f));
        headerRow.getCell(colWorkPhone).setComponent(workPhone);

        SSSNumber = new TextField();
        SSSNumber.setWidth(String.valueOf(colSSNNumber.getWidth() * 0.9f));
        headerRow.getCell(colSSNNumber).setComponent(SSSNumber);

        taxNumber = new TextField();
        taxNumber.setWidth(String.valueOf(colTaxNumber.getWidth() * 0.9f));
        headerRow.getCell(colTaxNumber).setComponent(taxNumber);

        birthPlace = new TextField();
        birthPlace.setWidth(String.valueOf(colBirthPlace.getWidth() * 0.9f));
        headerRow.getCell(colBirthPlace).setComponent(birthPlace);

        document = new TextField();
        document.setWidth(String.valueOf(colDocuments.getWidth() * 0.9f));
        headerRow.getCell(colDocuments).setComponent(document);

        address = new TextField();
        address.setWidth(String.valueOf(colAddresses.getWidth() * 0.9f));
        headerRow.getCell(colAddresses).setComponent(address);

        headerRow.getComponents().stream()
            .filter(item -> item instanceof HasValue)
            .map(item -> (HasValue) item)
            .forEach(item -> item.addValueChangeListener(event -> updateGridContent()));

        gridClients.addColumnResizeListener(event -> {
            if (event.getColumn() == colRating) {
                rating.setWidth(String.valueOf(colRating.getWidth() * 0.9f));
            } else if (event.getColumn() == colDisplayName) {
                displayName.setWidth(String.valueOf(colDisplayName.getWidth() * 0.9f));
            } else if (event.getColumn() == colBirthDate) {
                birthDate.setWidth(String.valueOf(colBirthDate.getWidth() * 0.9f));
            } else if (event.getColumn() == colGender) {
                gender.setWidth(String.valueOf(colGender.getWidth() * 0.9f));
            } else if (event.getColumn() == colMobilePhone) {
                mobilePhone.setWidth(String.valueOf(colMobilePhone.getWidth() * 0.9f));
            } else if (event.getColumn() == colWorkPhone) {
                workPhone.setWidth(String.valueOf(colWorkPhone.getWidth() * 0.9f));
            } else if (event.getColumn() == colSSNNumber) {
                SSSNumber.setWidth(String.valueOf(colSSNNumber.getWidth() * 0.9f));
            } else if (event.getColumn() == colTaxNumber) {
                taxNumber.setWidth(String.valueOf(colTaxNumber.getWidth() * 0.9f));
            } else if (event.getColumn() == colBirthPlace) {
                birthPlace.setWidth(String.valueOf(colBirthPlace.getWidth() * 0.9f));
            } else if (event.getColumn() == colDocuments) {
                document.setWidth(String.valueOf(colDocuments.getWidth() * 0.9f));
            } else if (event.getColumn() == colAddresses) {
                address.setWidth(String.valueOf(colAddresses.getWidth() * 0.9f));
            }
        });

        updateGridContent();

        addComponents(layout, gridClients);
        setComponentAlignment(layout, Alignment.TOP_RIGHT);
        setExpandRatio(gridClients, 1);
        setSizeFull();
    }

    private void updateGridContent() {
        QPhysicalClient client = QPhysicalClient.physicalClient;

        Predicate predicate = new BooleanBuilder()
            .and(ClientQueryDSL.eq(client.rating, rating))
            .and(PhysicalClientQueryDSL.fullNameLike(client, displayName))
            .and(ClientQueryDSL.eq(client.birthDate, birthDate))
            .and(ClientQueryDSL.eq(client.gender, gender))
            .and(ClientQueryDSL.contains(client.details.mobilePhone, mobilePhone))
            .and(ClientQueryDSL.contains(client.details.workPhone, workPhone))
            .and(ClientQueryDSL.contains(client.details.ssnNumber, SSSNumber))
            .and(ClientQueryDSL.contains(client.details.taxNumber, taxNumber))
            .and(ClientQueryDSL.contains(client.details.birthPlace, birthPlace))
            .and(PhysicalClientQueryDSL.fullDocumentLike(client, document))
            .and(PhysicalClientQueryDSL.fullAddressLike(client, address))
            .getValue();

        gridClients.setDataProvider(
            (sortOrders, offset, limit) -> clientService.findAll(predicate, sortOrders, offset, limit),
            () -> clientService.count(predicate));
    }

    @PreDestroy
    public void destroy() {
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.clients");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.BLUE;
    }

    @Subscribe
    public void editTypeEventHandler(UpdateClientEvent<PhysicalClient> event) {
        gridClients.getDataProvider().refreshItem(event.getClient());
    }
}

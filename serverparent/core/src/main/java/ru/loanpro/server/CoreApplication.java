package ru.loanpro.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Точка входа в приложение LoanProcessor360.
 *
 * @author Oleg Brizhevatikh
 */
@SpringBootApplication
@EnableScheduling
@EntityScan
public class CoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(CoreApplication.class, args);
    }
}

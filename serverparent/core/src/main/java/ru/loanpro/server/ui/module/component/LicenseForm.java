package ru.loanpro.server.ui.module.component;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.io.CharStreams;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.license.client.collection.LicenseStatus;
import ru.loanpro.license.client.dto.IncorrectLicenseFileException;
import ru.loanpro.license.client.exception.BadRequestException;
import ru.loanpro.license.client.exception.RequestAlreadyRegisteredException;
import ru.loanpro.license.client.exception.UnknownErrorException;
import ru.loanpro.license.client.service.LicenseService;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;

/**
 * Форма лицензирования. Осуществляет отправку запроса на получение лицензии, проверку активации лицензии и ручную
 * загрузку файла с лицензией.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
public class LicenseForm extends VerticalLayout {

    @Autowired
    private LicenseService service;

    @Autowired
    private I18N i18n;

    private VerticalLayout actionLayout;

    @Value("classpath:license.txt")
    private Resource resource;

    private enum ActivationType {
        AUTO("security.license.auto"), MANUAL("security.license.manual");

        private String type;

        ActivationType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    @PostConstruct
    public void init() {
        actionLayout = new VerticalLayout();

        LicenseStatus status = service.getStatus();
        switch (status) {
            case DEMO:
                initDemoState();
                break;
            case REQUEST:
                initRequestState();
                break;
            case LICENSED:
                initLicensedState();
                break;
        }

        Layout agreementLayout = initLicenseAgreement();
        addComponents(actionLayout, agreementLayout);
        setComponentAlignment(agreementLayout, Alignment.TOP_CENTER);
    }

    private Layout initLicenseAgreement() {
        Label label = new Label(i18n.get("security.agreement.caption"));

        Label labelText = new Label("", ContentMode.HTML);
        labelText.addStyleName(MaterialTheme.LABEL_BORDERLESS);
        labelText.addStyleName("");
        labelText.setSizeFull();

        try (final Reader reader = new InputStreamReader(resource.getInputStream())) {
            labelText.setValue(CharStreams.toString(reader));
        } catch (IOException e){
            e.printStackTrace();
        }

        VerticalLayout layout = new VerticalLayout(label, labelText);
        layout.setComponentAlignment(label, Alignment.TOP_CENTER);
        layout.setSpacing(true);
        layout.setWidth(800, Unit.PIXELS);

        return layout;
    }

    private void updateDisplay(Layout layout) {
        actionLayout.removeAllComponents();
        layout.setWidth(800, Unit.PIXELS);
        actionLayout.addComponent(layout);
        actionLayout.setComponentAlignment(layout, Alignment.TOP_CENTER);
    }

    private void initDemoState() {
        ComboBox<ActivationType> activationType = new ComboBox<>(i18n.get("security.license.activationType"));
        activationType.setItemCaptionGenerator(item -> i18n.get(item.getType()));
        activationType.setEmptySelectionAllowed(false);
        activationType.setItems(ActivationType.values());

        TextArea publicKey = new TextArea(i18n.get("security.license.publicKey"));
        publicKey.setWidth(100, Unit.PERCENTAGE);
        publicKey.setReadOnly(true);

        TextArea comment = new TextArea(i18n.get("security.license.comment"));
        comment.setWidth(100, Unit.PERCENTAGE);

        Label error = new Label();
        error.addStyleName(MaterialTheme.LABEL_FAILURE);
        error.setVisible(false);

        Button request = new Button(i18n.get("security.license.request"));
        request.setEnabled(false);

        request.addClickListener(event -> {
            try {
                service.postRequest(comment.getValue());
                initRequestState();
            } catch (RequestAlreadyRegisteredException e) {
                error.setValue(i18n.get("security.license.requestAlreadyRegistered"));
                error.setVisible(true);
            } catch (BadRequestException e) {
                error.setValue(i18n.get("security.license.badRequestException"));
                error.setVisible(true);
            } catch (ResourceAccessException e) {
                error.setValue(i18n.get("security.license.connectException"));
                error.setVisible(true);
            }
        });

        LicenseUploadReceiver uploadReceiver = new LicenseUploadReceiver();
        Upload upload = new Upload(i18n.get("security.license.upload.license"), uploadReceiver);
        upload.setImmediateMode(true);
        upload.setEnabled(false);
        upload.setButtonCaption(i18n.get("security.license.upload.button"));
        upload.addStartedListener(event -> {
            if (event.getContentLength() > 2048) {
                // Проверка на превышение установленного размера файла
                Notification.show(i18n.get("security.license.upload.largeFile"), Notification.Type.TRAY_NOTIFICATION);
                upload.interruptUpload();
            }
        });
        upload.addSucceededListener(event -> {
            try {
                service.uploadLicense(uploadReceiver.getOutputStream());
                initLicensedState();
            } catch (IncorrectLicenseFileException e) {
                error.setValue(i18n.get("security.license.incorrectLicenseFile"));
                error.setVisible(true);
            }
        });

        activationType.addValueChangeListener(event -> {
            switch (event.getValue()) {
                case AUTO:
                    publicKey.setVisible(false);
                    comment.setVisible(true);
                    request.setVisible(true);
                    upload.setVisible(false);
                    break;
                case MANUAL:
                    publicKey.setValue(service.getManualRequest().getPublicKey());
                    publicKey.setVisible(true);
                    comment.setVisible(false);
                    request.setVisible(false);
                    upload.setVisible(true);
                    break;
            }
            error.setVisible(false);
        });
        activationType.setValue(service.isManualRequestExists() ? ActivationType.MANUAL : ActivationType.AUTO);

        CheckBox checkBox = new CheckBox(i18n.get("security.agreement.checkbox"));

        checkBox.addValueChangeListener(v->{
            request.setEnabled(v.getValue());
            upload.setEnabled(v.getValue());
        });

        HorizontalLayout hlRequest = new HorizontalLayout(request, upload, checkBox);
        hlRequest.setComponentAlignment(checkBox, Alignment.MIDDLE_LEFT);
        hlRequest.setSpacing(true);

        updateDisplay(new VerticalLayout(activationType, publicKey, comment, error, hlRequest));
    }

    private void initRequestState() {
        Label label = new Label(i18n.get("security.license.requestSent"));

        Label uuid = new Label(service.getUUID().toString());
        uuid.addStyleNames(MaterialTheme.LABEL_LARGE, MaterialTheme.LABEL_COLORED);

        Button check = new Button(i18n.get("security.license.check"), VaadinIcons.REFRESH);
        check.addClickListener(event -> {
            try {
                LicenseStatus status = service.checkLicense();
                if (status == LicenseStatus.LICENSED)
                    initLicensedState();
            } catch (UnknownErrorException e) {
                e.printStackTrace();
            }
        });

        updateDisplay(new VerticalLayout(label, uuid, check));
    }

    private void initLicensedState() {
        Label label1 = new Label(i18n.get("security.license.registered"));

        Label uuid = new Label(service.getUUID().toString());
        uuid.addStyleNames(MaterialTheme.LABEL_LARGE, MaterialTheme.LABEL_COLORED);

        Label label2 = new Label(i18n.get("security.license.userCount"));

        Label userCount = new Label(service.getUserCount().toString());
        userCount.addStyleNames(MaterialTheme.LABEL_LARGE, MaterialTheme.LABEL_COLORED);

        HorizontalLayout layout = new HorizontalLayout(label2, userCount);
        layout.setComponentAlignment(label2, Alignment.MIDDLE_LEFT);

        updateDisplay(new VerticalLayout(label1, uuid, layout));
    }

    private class LicenseUploadReceiver implements Receiver {

        private ByteArrayOutputStream outputStream;

        @Override
        public OutputStream receiveUpload(String filename, String mimeType) {
            outputStream = new ByteArrayOutputStream();
            return outputStream;
        }

        ByteArrayOutputStream getOutputStream() {
            return outputStream;
        }
    }
}

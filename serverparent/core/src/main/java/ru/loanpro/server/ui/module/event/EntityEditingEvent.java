package ru.loanpro.server.ui.module.event;

import ru.loanpro.global.domain.AbstractIdEntity;

/**
 * Событие начала редактирования сущности.
 *
 * @param <T> тип редактируемой сущности.
 *
 * @author Oleg Brizhevatikh
 */
public class EntityEditingEvent<T extends AbstractIdEntity> {

    private T entity;

    public EntityEditingEvent(T entity) {
        this.entity = entity;
    }

    public T getEntity() {
        return entity;
    }
}

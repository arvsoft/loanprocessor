package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Lists;
import com.vaadin.data.Binder;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.NumberService;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.service.DepartmentAccountService;
import ru.loanpro.global.Settings;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.directory.AccountType;
import ru.loanpro.global.directory.OverdueDaysCalculationType;
import ru.loanpro.global.directory.PaymentSplitType;
import ru.loanpro.global.directory.RecalculationType;
import ru.loanpro.global.directory.RepaymentOrderScheme;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.service.UserService;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.service.SequenceService;
import ru.loanpro.server.ui.module.client.details.AccountDetailsLayout;
import ru.loanpro.server.ui.module.client.details.ContentChangeListener;
import ru.loanpro.server.ui.module.component.BindedGrid;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.server.ui.module.event.UpdateDepartmentEvent;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Вкладка редактирования отдела.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(Department.class)
public class DepartmentTab extends BaseTab implements ContentChangeListener {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private UserService userService;

    @Autowired
    private DepartmentAccountService departmentAccountService;

    @Autowired
    NumberService numberService;

    @Autowired
    DepartmentTabConfigurationHelper departmentTabConfigurationHelper;

    private Department department;
    private Binder<Department> binderMainInfo;

    private int hashCode;
    private int accountsHashCode;
    private int additionalHashCode;

    private ComboBox<UiTheme> theme;
    private ComboBox<ZoneId> timeZone;
    private ComboBox<SystemLocale> locale;
    private ComboBox<GridStyle> gridStyle;
    private BindedGrid<Currency> currencyGrid;

    private Button save;
    private Button saveAndClose;
    private Button financySave;
    private Button financySaveAndClose;
    private Button formSave;
    private Button formSaveAndClose;
    private Sequence departmentAccountSequence;
    private AccountDetailsLayout<DepartmentAccount> accountDetailsLayout;
    private Set<DepartmentAccount> departmentAccounts = new HashSet<>();
    private Binder<DepartmentTabConfigurationHelper> binderAdditional;

    public DepartmentTab(Department department) {
        this.department = (department == null) ? new Department() : department;
    }

    @PostConstruct
    public void init() {
        departmentAccountSequence = configurationService.getDepartmentAccountSequence();
        hashCode = this.department.hashCode();
        accountsHashCode = departmentAccounts.hashCode();

        binderAdditional = new Binder<>();

        if (department.getId() == null) {
            //генерируем предварительный (draft) номер счета отдела
            DepartmentAccount account = new DepartmentAccount();
            //получаем грязный номер
            account.setNumber(numberService.getDepartmentAccountSequenceDraftNumber());
            account.setBalance(new BigDecimal(0));
            account.setTypeAccount(AccountType.INNER_ACCOUNT);
            account.setRegistrationDate(chronometerService.getCurrentTime().toLocalDate());
            account.setCurrency(configurationService.getCurrencies().get(0));
            account.setDefaultAccount(false);
            account.setCashAccount(true);
            departmentAccounts.add(account);
        } else {
            departmentAccounts = departmentAccountService.findAllByDepartment(department);
        }
        departmentTabConfigurationHelper.load(department, configurationService.getCurrencies());
        additionalHashCode = departmentTabConfigurationHelper.hashCode();

        setSpacing(true);
        setMargin(true);
        setSizeFull();

        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleNames(ValoTheme.TABSHEET_FRAMED, ValoTheme.TABSHEET_PADDED_TABBAR);
        tabSheet.setWidth(100, Unit.PERCENTAGE);
        tabSheet.setHeight(100, Unit.PERCENTAGE);
        tabSheet.addTab(getMainInfo(), i18n.get("department.tab.mainInfo"));
        tabSheet.addTab(getFinancyInfo(), i18n.get("department.tab.financyInfo"));
        tabSheet.addTab(getFormInfo(), i18n.get("department.tab.formInfo"));

        addComponent(tabSheet);
    }

    private Layout getMainInfo() {

        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setWidth("800px");
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);
        addComponent(form);
        setComponentAlignment(form, Alignment.TOP_CENTER);

        Label section = new Label(i18n.get("department.departmentInfo"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        TextField name = new TextField(i18n.get("department.name"));
        form.addComponent(name);

        TextField description = new TextField(i18n.get("department.description"));
        form.addComponent(description);

        ComboBox<Department> parentDepartment = new ComboBox<>(i18n.get("department.parent"));
        // Список всех отделов
        List<Department> departments = departmentService.findAll();
        // Исключаем текущий отдел и всех его потомков
        List<Department> departmentExclude = Stream.concat(
            Stream.of(department), department.getChilds().stream().flatMap(Department::flattened))
            .collect(Collectors.toList());
        departments.removeAll(departmentExclude);

        parentDepartment.setItems(departments);
        parentDepartment.setItemCaptionGenerator(Department::getName);
        form.addComponent(parentDepartment);

        ComboBox<Sequence> loanSequence = new ComboBox<>(i18n.get("department.loanSequence"));
        loanSequence.setItemCaptionGenerator(Sequence::getName);
        loanSequence.setEmptySelectionAllowed(false);
        loanSequence.setItems(sequenceService.findAll());
        form.addComponent(loanSequence);

        ComboBox<Sequence> requestSequence = new ComboBox<>(i18n.get("department.requestSequence"));
        requestSequence.setItemCaptionGenerator(Sequence::getName);
        requestSequence.setEmptySelectionAllowed(false);
        requestSequence.setItems(sequenceService.findAll());
        form.addComponent(requestSequence);

        ComboBox<Sequence> accountSequence = new ComboBox<>(i18n.get("department.accountClientSequence"));
        accountSequence.setItemCaptionGenerator(Sequence::getName);
        accountSequence.setEmptySelectionAllowed(false);
        accountSequence.setItems(sequenceService.findAll());
        form.addComponent(accountSequence);

        ComboBox<Sequence> prolongSequence = new ComboBox<>(i18n.get("department.prolongSequence"));
        prolongSequence.setItemCaptionGenerator(Sequence::getName);
        prolongSequence.setEmptySelectionAllowed(false);
        prolongSequence.setItems(sequenceService.findAll());
        form.addComponent(prolongSequence);

        ComboBox<Sequence> operationSequence = new ComboBox<>(i18n.get("department.operationSequence"));
        operationSequence.setItemCaptionGenerator(Sequence::getName);
        operationSequence.setEmptySelectionAllowed(false);
        operationSequence.setItems(sequenceService.findAll());
        form.addComponent(operationSequence);

        ComboBox<User> director = new ComboBox<>(i18n.get("department.director"));
        director.setItemCaptionGenerator(User::getUsername);
        director.setItems(userService.findAllUsers());
        form.addComponent(director);

        TextField address = new TextField(i18n.get("department.address"));
        form.addComponent(address);

        TextField phone = new TextField(i18n.get("department.phone"));
        form.addComponent(phone);

        TextField email = new TextField(i18n.get("department.email"));
        form.addComponent(email);

        section = new Label(i18n.get("department.settings"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        // Наследование настроек
        CheckBox inheritance = new CheckBox(i18n.get("department.settings.inheritance"));
        inheritance.addValueChangeListener(event -> updateDepartmentInheritanceSettings(event.getValue()));
        form.addComponent(inheritance);

        // Тема интерфейса
        theme = new ComboBox<>(i18n.get("security.user.theme"));
        theme.setItems(UiTheme.values());
        theme.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        theme.setWidth(50, Unit.PERCENTAGE);
        theme.setEmptySelectionAllowed(false);
        form.addComponent(theme);

        // Часовой пояс
        timeZone = new ComboBox<>(i18n.get("settings.timezone"));
        List<ZoneId> timeZoneItemList = chronometerService.getAllZoneIds();
        timeZone.setItems(timeZoneItemList);
        timeZone.setPageLength(timeZoneItemList.size());
        timeZone.setWidth(50, Unit.PERCENTAGE);
        timeZone.setEmptySelectionAllowed(false);
        timeZone.setItemCaptionGenerator(item -> String.format("%s %s",
            ZonedDateTime.now(item).getOffset().getId().replace("Z", "+00:00"), item));
        form.addComponent(timeZone);

        // Язык системы
        locale = new ComboBox<>(i18n.get("settings.locale"));
        locale.setItems(SystemLocale.values());
        locale.setEmptySelectionAllowed(false);
        locale.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        locale.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(locale);

        currencyGrid = new BindedGrid<>(i18n.get("settings.currencyCode.grid"));
        currencyGrid.addColumn(item -> String.format(
            "%s %s", item.getCurrencyCode(), item.getDisplayName(locale.getLocale())))
            .setCaption(i18n.get("settings.currencyCode.grid.name"));
        currencyGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        currencyGrid.setItems(Arrays.stream(settingsService.get(String.class, Settings.CURRENCIES, "RUB,USD,EUR")
            .split(","))
            .map(Currency::getInstance)
            .collect(Collectors.toSet()));
        form.addComponent(currencyGrid);

        save = new Button(i18n.get("department.save"));
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        save.setEnabled(false);
        saveAndClose = new Button(i18n.get("department.saveAndClose"));
        saveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        saveAndClose.setEnabled(false);

        financySave = new Button(i18n.get("department.save"));
        financySave.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        financySave.setEnabled(false);
        financySaveAndClose = new Button(i18n.get("department.saveAndClose"));
        financySaveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        financySaveAndClose.setEnabled(false);

        formSave = new Button(i18n.get("department.save"));
        formSave.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        formSave.setEnabled(false);
        formSaveAndClose = new Button(i18n.get("department.saveAndClose"));
        formSaveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        formSaveAndClose.setEnabled(false);

        binderMainInfo = new Binder<>();
        binderMainInfo.forField(name)
            .asRequired(i18n.get("department.name.error"))
            .bind(Department::getName, Department::setName);

        binderMainInfo.bind(description, Department::getDescription, Department::setDescription);

        binderMainInfo.bind(parentDepartment, Department::getParent, Department::setParent);

        binderMainInfo.forField(loanSequence)
            .asRequired(i18n.get("department.loanSequence.error"))
            .bind(Department::getLoanSequence, Department::setLoanSequence);

        binderMainInfo.forField(requestSequence)
            .asRequired(i18n.get("department.loanSequence.error"))
            .bind(Department::getRequestSequence, Department::setRequestSequence);

        binderMainInfo.forField(accountSequence)
            .asRequired(i18n.get("department.accountSequence.error"))
            .bind(Department::getAccountSequence, Department::setAccountSequence);

        binderMainInfo.forField(prolongSequence)
            .asRequired(i18n.get("department.prolongSequence.error"))
            .bind(Department::getProlongSequence, Department::setProlongSequence);

        binderMainInfo.forField(operationSequence)
            .asRequired(i18n.get("department.prolongSequence.error"))
            .bind(Department::getOperationSequence, Department::setOperationSequence);

        binderMainInfo.bind(director, Department::getDirector, Department::setDirector);
        binderMainInfo.bind(address, Department::getAddress, Department::setAddress);
        binderMainInfo.bind(phone, Department::getPhone, Department::setPhone);
        binderMainInfo.bind(email, Department::getEmail, Department::setEmail);

        binderMainInfo.bind(inheritance, Department::getInheritance, Department::setInheritance);

        if (!department.getInheritance())
            updateDepartmentInheritanceSettings(false);

        binderMainInfo.setBean(department);

        binderMainInfo.addValueChangeListener(event -> contentChanged());

        save.addClickListener(event -> {
            department = departmentService.save(binderMainInfo.getBean());
            sessionEventBus.post(new UpdateDepartmentEvent(department));
            hashCode = department.hashCode();
            accountsHashCode = departmentAccounts.hashCode();

            departmentTabConfigurationHelper.save(department);
            additionalHashCode = departmentTabConfigurationHelper.hashCode();

            contentChanged();

            //сохраняем счета
            if (department.getId() != null) {
                departmentAccountService.saveAll(departmentAccounts, department, departmentAccountSequence);
            }
        });

        financySave.addClickListener(event -> save.click());
        formSave.addClickListener(event -> save.click());

        saveAndClose.addClickListener(event -> {
            save.click();
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        financySaveAndClose.addClickListener(event -> saveAndClose.click());
        formSaveAndClose.addClickListener(event -> saveAndClose.click());

        HorizontalLayout hlButtons = new HorizontalLayout(save, saveAndClose);
        hlButtons.setSpacing(true);

        VerticalLayout layout = new VerticalLayout(hlButtons, form);
        layout.setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
        layout.setComponentAlignment(form, Alignment.TOP_CENTER);

        return layout;
    }

    private Layout getFinancyInfo() {
        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setWidth("800px");
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);
        addComponent(form);
        setComponentAlignment(form, Alignment.TOP_CENTER);

        Label section = new Label(i18n.get("department.financyInfo.currency"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        ComboBox<Currency> defaultCurrency = new ComboBox<>(i18n.get("department.financyInfo.field.defaultCurrency"));
        defaultCurrency.setItems(configurationService.getCurrencies());
        defaultCurrency.setWidth(200, Unit.PIXELS);
        defaultCurrency.setEmptySelectionAllowed(false);
        form.addComponent(defaultCurrency);

        section = new Label(i18n.get("department.financyInfo.prolongation"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        ComboBox<RecalculationType> recalculationType = new ComboBox<>(
            i18n.get("department.financyInfo.field.recalculationType"),
            Lists.newArrayList(RecalculationType.values()));
        recalculationType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        recalculationType.setEmptySelectionAllowed(false);
        recalculationType.setValue(RecalculationType.RECALCULATION_ON_NEW_TERM);
        form.addComponent(recalculationType);

        section = new Label(i18n.get("department.financyInfo.payment"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        ComboBox<OverdueDaysCalculationType> overdueCalculationType = new ComboBox<>(
            i18n.get("department.financyInfo.field.overdueDaysCalculationType"),
            Lists.newArrayList(OverdueDaysCalculationType.values()));
        overdueCalculationType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        overdueCalculationType.setEmptySelectionAllowed(false);
        overdueCalculationType.setValue(OverdueDaysCalculationType.FROM_DATE_ACCORDING_SCHEDULE);
        form.addComponent(overdueCalculationType);

        ComboBox<RepaymentOrderScheme> repaymentOrderScheme = new ComboBox<>(
            i18n.get("department.financyInfo.field.repaymentOrderScheme"),
            Lists.newArrayList(RepaymentOrderScheme.values()));
        repaymentOrderScheme.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        repaymentOrderScheme.setEmptySelectionAllowed(false);
        repaymentOrderScheme.setValue(RepaymentOrderScheme.ORDER1);
        form.addComponent(repaymentOrderScheme);

        CheckBox summationPenalty = new CheckBox(i18n.get("department.financyInfo.field.summationPenalty"));
        form.addComponent(summationPenalty);
        CheckBox summationFine = new CheckBox(i18n.get("department.financyInfo.field.summationFine"));
        form.addComponent(summationFine);

        section = new Label(i18n.get("department.financyInfo.cashdesk"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        CheckBox keepSameOperationDateWhenSaveFromPreparedToProcessed = new CheckBox(i18n.get("department.financyInfo.field.keepSameOperationDate"));
        form.addComponent(keepSameOperationDateWhenSaveFromPreparedToProcessed);

        section = new Label(i18n.get("department.financyInfo.label.autoOperationLoanIssued"));
        section.addStyleName(MaterialTheme.LABEL_BOLD);
        form.addComponent(section);

        CheckBox autoOperationIssueMoneyLoanIssued = new CheckBox(i18n.get("department.financyInfo.field.autoOperationIssueMoneyLoanIssued"));
        form.addComponent(autoOperationIssueMoneyLoanIssued);
        CheckBox autoOperationIssueMoneyLoanIssuedProcessed = new CheckBox(i18n.get("department.financyInfo.field.autoOperationSetStatusToProcessed"));
        autoOperationIssueMoneyLoanIssuedProcessed.addStyleName(ValoTheme.CHECKBOX_SMALL);
        Label gap = new Label();gap.setWidth(20, Unit.PIXELS);
        HorizontalLayout gapLayout = new HorizontalLayout(gap, autoOperationIssueMoneyLoanIssuedProcessed);
        form.addComponent(gapLayout);
        section = new Label(i18n.get("department.financyInfo.label.autoOperationLoanPayment"));
        section.addStyleName(MaterialTheme.LABEL_BOLD);
        form.addComponent(section);

        CheckBox autoOperationReplenishMoneyLoanPayment = new CheckBox(i18n.get("department.financyInfo.field.autoOperationReplenishMoneyLoanPayment"));
        form.addComponent(autoOperationReplenishMoneyLoanPayment);
        CheckBox autoOperationReplenishMoneyLoanPaymentProcessed = new CheckBox(i18n.get("department.financyInfo.field.autoOperationSetStatusToProcessed"));
        autoOperationReplenishMoneyLoanPaymentProcessed.addStyleName(ValoTheme.CHECKBOX_SMALL);
        gap = new Label();gap.setWidth(20, Unit.PIXELS);
        gapLayout = new HorizontalLayout(gap, autoOperationReplenishMoneyLoanPaymentProcessed);
        form.addComponent(gapLayout);

        ComboBox<PaymentSplitType> paymentSplitType = new ComboBox<>(
            i18n.get("department.financyInfo.field.paymentSplitType"),
            Lists.newArrayList(PaymentSplitType.values()));
        paymentSplitType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        paymentSplitType.setEmptySelectionAllowed(false);
        paymentSplitType.setValue(PaymentSplitType.ONE_TRANSACTION);
        form.addComponent(paymentSplitType);

        section = new Label(i18n.get("department.financyInfo.accounts"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        accountDetailsLayout = new AccountDetailsLayout<>(department, departmentAccounts, i18n, configurationService,
            numberService, chronometerService.getCurrentTime().toLocalDate(), securityService);
        accountDetailsLayout.addContentChangeListener(this);

        binderAdditional.bind(defaultCurrency,
            DepartmentTabConfigurationHelper::getCurrencyDefault,
            DepartmentTabConfigurationHelper::setCurrencyDefault);

        binderAdditional.bind(recalculationType,
            DepartmentTabConfigurationHelper::getRecalculationType,
            DepartmentTabConfigurationHelper::setRecalculationType);

        binderAdditional.bind(overdueCalculationType,
            DepartmentTabConfigurationHelper::getOverdueDaysCalculationType,
            DepartmentTabConfigurationHelper::setOverdueDaysCalculationType);

        binderAdditional.bind(repaymentOrderScheme,
            DepartmentTabConfigurationHelper::getRepaymentOrderScheme,
            DepartmentTabConfigurationHelper::setRepaymentOrderScheme);

        binderAdditional.bind(summationPenalty,
            DepartmentTabConfigurationHelper::isSummationPenalty,
            DepartmentTabConfigurationHelper::setSummationFine);

        binderAdditional.bind(summationFine,
            DepartmentTabConfigurationHelper::isSummationFine,
            DepartmentTabConfigurationHelper::setSummationFine);

        binderAdditional.bind(autoOperationIssueMoneyLoanIssued,
            DepartmentTabConfigurationHelper::isAutoOperationIssueMoneyLoanIssued,
            DepartmentTabConfigurationHelper::setAutoOperationIssueMoneyLoanIssued);

        binderAdditional.bind(autoOperationReplenishMoneyLoanPayment,
            DepartmentTabConfigurationHelper::isAutoOperationReplenishMoneyLoanPayment,
            DepartmentTabConfigurationHelper::setAutoOperationReplenishMoneyLoanPayment);

        binderAdditional.bind(autoOperationIssueMoneyLoanIssuedProcessed,
            DepartmentTabConfigurationHelper::isAutoOperationIssueMoneyLoanIssuedProcessed,
            DepartmentTabConfigurationHelper::setAutoOperationIssueMoneyLoanIssuedProcessed);

        binderAdditional.bind(autoOperationReplenishMoneyLoanPaymentProcessed,
            DepartmentTabConfigurationHelper::isAutoOperationReplenishMoneyLoanPaymentProcessed,
            DepartmentTabConfigurationHelper::setAutoOperationReplenishMoneyLoanPaymentProcessed);

        binderAdditional.bind(paymentSplitType,
            DepartmentTabConfigurationHelper::getPaymentSplitType,
            DepartmentTabConfigurationHelper::setPaymentSplitType);

        binderAdditional.bind(keepSameOperationDateWhenSaveFromPreparedToProcessed,
            DepartmentTabConfigurationHelper::isKeepSameOperationDateWhenSaveFromPreparedToProcessed,
            DepartmentTabConfigurationHelper::setKeepSameOperationDateWhenSaveFromPreparedToProcessed);

        binderAdditional.setBean(departmentTabConfigurationHelper);
        binderAdditional.addValueChangeListener(event -> contentChanged());

        HorizontalLayout hlButtons = new HorizontalLayout(financySave, financySaveAndClose);
        hlButtons.setSpacing(true);

        VerticalLayout layout = new VerticalLayout(hlButtons, form, accountDetailsLayout);
        layout.setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
        layout.setComponentAlignment(form, Alignment.TOP_CENTER);

        return layout;
    }

    private Layout getFormInfo(){
        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setWidth("800px");
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);
        addComponent(form);
        setComponentAlignment(form, Alignment.TOP_CENTER);

        Label section = new Label(i18n.get("department.formInfo.gridSettings"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        gridStyle = new ComboBox<>(i18n.get("department.formInfo.gridStyle"));
        gridStyle.setItems(GridStyle.values());
        gridStyle.setEmptySelectionAllowed(false);
        gridStyle.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        gridStyle.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(gridStyle);

        binderAdditional.bind(gridStyle,
            DepartmentTabConfigurationHelper::getGridStyle,
            DepartmentTabConfigurationHelper::setGridStyle);

        HorizontalLayout hlButtons = new HorizontalLayout(formSave, formSaveAndClose);
        hlButtons.setSpacing(true);

        VerticalLayout layout = new VerticalLayout(hlButtons, form);
        layout.setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
        layout.setComponentAlignment(form, Alignment.TOP_CENTER);

        return layout;
    }

    private void updateDepartmentInheritanceSettings(Boolean inheritance) {
        theme.setVisible(!inheritance);
        timeZone.setVisible(!inheritance);
        locale.setVisible(!inheritance);
        currencyGrid.setVisible(!inheritance);

        if (!inheritance) {
            if (department.getTheme() == null) {
                department.setTheme(settingsService.get(UiTheme.class, Settings.THEME, UiTheme.LIGHT));
                department.setZoneId(ZoneId.of(settingsService.get(String.class, Settings.TIME_ZONE, "UCT")));
                department.setLocale(settingsService.get(SystemLocale.class, Settings.LOCALE, SystemLocale.ENGLISH));
                department.setCurrencies(Arrays.stream(settingsService.get(String.class, Settings.CURRENCIES, "RUB,USD,EUR").split(","))
                    .map(Currency::getInstance)
                    .collect(Collectors.toSet()));
            }

            binderMainInfo.bind(theme, Department::getTheme, Department::setTheme);
            binderMainInfo.bind(timeZone, Department::getZoneId, Department::setZoneId);
            binderMainInfo.bind(locale, Department::getLocale, Department::setLocale);
            binderMainInfo.bind(currencyGrid, Department::getCurrencies, Department::setCurrencies);
        } else {
            binderMainInfo.removeBinding(theme);
            binderMainInfo.removeBinding(timeZone);
            binderMainInfo.removeBinding(locale);
            binderMainInfo.removeBinding(currencyGrid);
        }
    }

    @Override
    public void contentChanged() {
        save.setEnabled(binderMainInfo.isValid() &&
            (department.hashCode() != hashCode ||
                accountsHashCode != departmentAccounts.hashCode() ||
                additionalHashCode != departmentTabConfigurationHelper.hashCode()));
        saveAndClose.setEnabled(save.isEnabled());
        financySave.setEnabled(save.isEnabled());
        financySaveAndClose.setEnabled(save.isEnabled());
        formSave.setEnabled(save.isEnabled());
        formSaveAndClose.setEnabled(save.isEnabled());
    }

    @Override
    public String getTabName() {
        if (department.getId() == null)
            return i18n.get("department.tabName.new");
        else
            return String.format(i18n.get("department.tabName.edit"), department.getName());
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.DEEP_PURPLE;
    }

    @Override
    public void destroy() {
        super.destroy();

        departmentAccounts.forEach(item -> {
            if (item.getDepartment() == null) {
                numberService.clearDepartmentAccountSequenceDraftNumber(item.getNumber());
            }
        });
        accountDetailsLayout.clearDepartmentAccountSequenceDraftNumber();
    }
}

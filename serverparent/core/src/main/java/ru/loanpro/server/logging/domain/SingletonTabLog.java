package ru.loanpro.server.logging.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Сущность содержащая поля для логов работы с вкладками.
 *
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public abstract class SingletonTabLog extends AbstractLog {

    /**
     * Имя класса вкладки, для которой пишется лог.
     */
    @Column(name = "target_class")
    private String tClass;

    public String gettClass() {
        return tClass;
    }

    public void settClass(String tClass) {
        this.tClass = tClass;
    }
}

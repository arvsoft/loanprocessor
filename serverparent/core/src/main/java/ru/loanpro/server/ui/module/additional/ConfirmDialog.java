package ru.loanpro.server.ui.module.additional;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Resource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * Диалоговое окно для подтверждения или отклонения действий.
 *
 * @author Oleg Brizhevatikh
 */
public class ConfirmDialog extends Window {

    private String caption;
    private Component content;
    private ConfirmCallback confirmCallback;
    private DeclineCallback declineCallback;
    private boolean closable = false;

    private Button confirm = new Button(VaadinIcons.CHECK);
    private Button decline = new Button(VaadinIcons.CLOSE);

    /**
     * Интерфейс обратного вызова для вынесения положительного решения.
     */
    public interface ConfirmCallback {
        void confirm();

    }

    /**
     * Интерфейс обратного вызова для вынесения отрицательного решения.
     */
    public interface DeclineCallback {
        void decline();

    }

    /**
     * Создаёт диалоговое окно с заголовком.
     *
     * @param caption заголовок окна.
     */
    public ConfirmDialog(String caption) {
        this.caption = caption;
    }

    /**
     * Создаёт диалоговое окно с заголовком и текстом.
     *
     * @param caption заголовок окна.
     * @param description текст окна.
     */
    public ConfirmDialog(String caption, String description) {
        this(caption);
        content = new Label(description);
    }

    /**
     * Создаёт диалоговое окно с заголовком и {@link Component} в качестве содержимого.
     *
     * @param caption заголовок окна.
     * @param content компонент с содержимым окна.
     */
    public ConfirmDialog(String caption, Component content) {
        this(caption);
        this.content = content;
    }

    /**
     * Создаёт диалоговое окно с заголовком, текстом и кнопкой подтверждения, для которой передаётся
     * метод обратного вызова.
     *
     * @param caption заголовок окна.
     * @param description текст окна.
     * @param confirmCallback метод обратного вызова для обработки подтверждения.
     */
    public ConfirmDialog(String caption, String description, ConfirmCallback confirmCallback) {
        this(caption, description);
        this.confirmCallback = confirmCallback;
    }

    /**
     * Создаёт диалоговое окно с заголовком, текстом и кнопками подтверждения и отклонения, для
     * которых передаются соответствующие методы обратного вызова.
     *
     * @param caption заголовок окна.
     * @param description текст окна.
     * @param confirmCallback метод обратного вызова для обработки подтверждения.
     * @param declineCallback метод обратного вызова для обработки отклонения.
     */
    public ConfirmDialog(String caption, String description, ConfirmCallback confirmCallback,
                         DeclineCallback declineCallback) {
        this(caption, description, confirmCallback);
        this.declineCallback = declineCallback;
    }

    /**
     * Создаёт диалоговое окно с заголовком, {@link Component} в качестве содержимого окна и
     * кнопкой подтверждения, для которой передаётся метод обратного вызова.
     *
     * @param caption заголовок окна.
     * @param content компонент с содержимым окна.
     * @param confirmCallback метод обратного вызова для обработки подтверждения.
     */
    public ConfirmDialog(String caption, Component content, ConfirmCallback confirmCallback) {
        this(caption, content);
        this.confirmCallback = confirmCallback;
    }

    /**
     * Создаёт диалоговое окно с заголовком, {@link Component} в качестве содержимого окна и
     * кнопками подтверждения и отклонения, для которых передаются соответствующие методы обратного вызова.
     *
     * @param caption заголовок окна.
     * @param content компонент с содержимым окна.
     * @param confirmCallback метод обратного вызова для обработки подтверждения.
     * @param declineCallback метод обратного вызова для обработки отклонения.
     */
    public ConfirmDialog(String caption, Component content, ConfirmCallback confirmCallback,
                         DeclineCallback declineCallback) {
        this(caption, content, confirmCallback);
        this.declineCallback = declineCallback;
    }

    /**
     * Выводит на экран диалоговое окно.
     */
    public void show() {
        setCaption(caption);
        confirm.setWidth(150, Unit.PIXELS);
        decline.setWidth(150, Unit.PIXELS);

        HorizontalLayout actionLayout = new HorizontalLayout();

        if (confirmCallback != null) {
            confirm.addStyleName(MaterialTheme.BUTTON_FRIENDLY);
            confirm.addClickListener(event -> {
                confirmCallback.confirm();
                close();
            });
            actionLayout.addComponent(confirm);
        }

        if (declineCallback != null) {
            decline.addStyleName(MaterialTheme.BUTTON_DANGER);
            decline.addClickListener(event -> {
                declineCallback.decline();
                close();
            });
            actionLayout.addComponent(decline);
        }

        VerticalLayout layout = new VerticalLayout(content, actionLayout);
        layout.setExpandRatio(content, 1);
        layout.setComponentAlignment(content, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(actionLayout, Alignment.MIDDLE_RIGHT);

        setContent(layout);
        setResizable(false);
        super.setClosable(closable);

        setModal(true);
        center();

        UI.getCurrent().addWindow(this);
    }

    /**
     * Изменяет стандартную иконку кнопки подтверждения.
     *
     * @param icon новая иконка.
     */
    public void setConfirmButtonIcon(Resource icon) {
        confirm.setIcon(icon);
    }

    /**
     * Устанавливает текст кнопки подтверждения.
     *
     * @param caption текст кнопки.
     */
    public void setConfirmButtonCaption(String caption) {
        confirm.setCaption(caption);
    }

    /**
     * Изменяет стандартную иконку кнопки отклонения.
     *
     * @param icon новая иконка.
     */
    public void setDeclineButtonIcon(Resource icon) {
        decline.setIcon(icon);
    }

    /**
     * Устанавливает текст кнопки отклонения.
     *
     * @param caption текст кнопки.
     */
    public void setDeclineButtonCaption(String caption) {
        decline.setCaption(caption);
    }

    /**
     * Устанавливает, можно ли закрыть окно без нажатия на кнопку подтверждения или
     * отклонения. По-умолчанию нельзя.
     *
     * @param closable можно ли закрыть окно.
     */
    public void setClosable(boolean closable) {
        this.closable = closable;
    }

    /**
     * Устанавливает обработчик нажатия кнопки подтверждения.
     *
     * @param confirmCallback обработчик.
     */
    public void setConfirmCallback(ConfirmCallback confirmCallback) {
        this.confirmCallback = confirmCallback;
    }

    /**
     * Устанавливает обработчик нажатия кнопки отклонения.
     *
     * @param declineCallback обработчик.
     */
    public void setDeclineCallback(DeclineCallback declineCallback) {
        this.declineCallback = declineCallback;
    }
}

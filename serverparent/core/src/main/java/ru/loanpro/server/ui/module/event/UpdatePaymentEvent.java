package ru.loanpro.server.ui.module.event;

/**
 * Класс события сохранения платежа (расчета) по займу
 *
 * @author Maksim Askhaev
 */
public class UpdatePaymentEvent {

    private Object object;

    public UpdatePaymentEvent(Object object) {
        this.object = object;
    }

    public Object getObject() {
        return object;
    }
}

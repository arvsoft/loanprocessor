package ru.loanpro.server.ui.module.additional;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class InstantLocalDateConverter implements Converter<LocalDate, Instant>{

    ZoneId zoneId;

    public InstantLocalDateConverter(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public Result<Instant> convertToModel(LocalDate value, ValueContext context) {
        return Result.ok(value.atStartOfDay(zoneId).toInstant());
    }

    @Override
    public LocalDate convertToPresentation(Instant value, ValueContext context) {
        return LocalDateTime.ofInstant(value, zoneId).toLocalDate();
    }

}

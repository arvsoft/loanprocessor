package ru.loanpro.server.configuration.spring;

import com.vaadin.ui.AbstractComponent;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import ru.loanpro.security.service.SecurityService;
import ru.loanpro.global.annotation.Enable;
import ru.loanpro.global.annotation.Visible;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * BeanPostProcessor выполняющий функцию диспетчера безопасности в пользовательском
 * интерфейсе Vaadin. Изменяет видимость и доступность компонентов на основании
 * аннотаций над полями бинов.
 *
 * @author Oleg Brizhevatikh
 */
@Component
public class SecurityBeanPostProcessor implements BeanPostProcessor {

    @Lazy
    @Autowired
    private SecurityService security;

    @Nullable
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Field[] fields = bean.getClass().getDeclaredFields();
        Arrays.stream(fields).forEach(field -> {
            Annotation[] annotations = field.getDeclaredAnnotations();
            if (annotations.length == 0) return;

            field.setAccessible(true);
            for (Annotation annotation : annotations) {
                try {
                    if (annotation instanceof Enable) {
                        String role = field.getAnnotation(Enable.class).value();
                        ((AbstractComponent) field.get(bean)).setEnabled(security.hasAuthority(role));
                    }

                    if (annotation instanceof Visible) {
                        String role = field.getAnnotation(Visible.class).value();
                        ((AbstractComponent) field.get(bean)).setVisible(security.hasAuthority(role));
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        return bean;
    }
}

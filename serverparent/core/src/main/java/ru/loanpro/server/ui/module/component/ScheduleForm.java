package ru.loanpro.server.ui.module.component;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Lists;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.schedule.Calculator;
import ru.loanpro.base.schedule.CalculatorPSK;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.LoanTermType;
import ru.loanpro.global.directory.PenaltyType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.additional.PercentageConverter;
import ru.loanpro.server.ui.module.event.ScheduleRecalculatedEvent;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Компонент VerticalLayout, выполнящий функцию расчета и отображения графика рассчитанных платежей
 * Содержит визуальные компоненты - параметры расчета и отображения графика платежей, кнопку расчета
 * Может быть использован в соответствующих формах, выполняющих расчет графика платежей
 *
 * @author Oleg Brizhevatikh
 * @author Maksim Askhaev
 */
public class ScheduleForm<T extends BaseLoan> extends VerticalLayout {

    //должны быть заменены из настроек
    private final static long MAX_LOAN_VALUE = 1_000_000_000;
    private final static long MAX_INTEREST_VALUE = 1_000;
    private final static int MAX_LOAN_TERM = 1_000;
    private final static long MAX_PENALTY_VALUE = 100;
    private final static long MAX_FINE_VALUE = 10000;
    private final T entity;
    private final Binder<T> binder;
    private final I18N i18n;
    private final int scaleValue = 2;
    private final int scaleMode = BigDecimal.ROUND_HALF_EVEN;

    private ComboBox<CalcScheme> cbCalcScheme;
    private TextField tfLoanTerm;
    private ComboBox<ReturnTerm> cbReturnTerm;
    private MoneyField tfInterestValue;
    private ComboBox<InterestType> cbInterestType;
    private ComboBox<LoanTermType> cbLoanTermType;
    private CheckBox cbxReplaceDate;
    private TextField tfReplaceDate;
    private CheckBox cbxReplaceOverMonth;
    private CheckBox cbxPrincipalLastPay;
    private CheckBox cbxInterestBalance;
    private CheckBox cbxNoInterestFD;
    private TextField tfNoInterestFDValue;
    private CheckBox cbxIncludeIssueDate;

    private List<Schedule> scheduleList;
    private Grid<Schedule> gridSchedule;

    private LocalDate issueDate;
    private BigDecimal currentBalance;

    private ComboBox<PenaltyType> cbPenaltyType;
    private MoneyField tfPenaltyValue;
    private CheckBox cbxChargeFine;
    private MoneyField tfFineValue;
    private CheckBox cbxFineOneTime;
    private CheckBox cbxInterestStrictly;
    private CheckBox cbxInterestOverdue;

    private CheckBox cbxInterestFirstDays;
    private TextField tfInterestFirstDays;
    private Label lbInterestFirstDays;

    private Label psk;
    private Label lastPayDate;
    private Label interestTotal;
    private final DateTimeFormatter dateFormatter;
    private final DecimalFormat moneyFormat;
    private final Button btCalculate;

    private final boolean isInProlongationProcess;
    private int scheduleHash;

    public ScheduleForm(T entity, Binder<T> binder, I18N i18n, ChronometerService chronometerService,
                        ConfigurationService configurationService, SessionEventBus sessionEventBus) {
        this(entity, binder, i18n, chronometerService, configurationService, sessionEventBus, false);
    }

    public ScheduleForm(T entity, Binder<T> binder, I18N i18n, ChronometerService chronometerService,
                        ConfigurationService configurationService, SessionEventBus sessionEventBus,
                        boolean isInProlongationProcess) {
        dateFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());
        moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        DecimalFormat percentFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        percentFormat.setMinimumFractionDigits(3);
        percentFormat.setMaximumFractionDigits(3);

        this.entity = entity;
        this.binder = binder;
        this.i18n = i18n;
        this.isInProlongationProcess = isInProlongationProcess;

        currentBalance = entity.getLoanBalance();

        if (entity.getClass() == Request.class)
            issueDate = ((Request) entity).getRequestIssueDate().atZone(chronometerService.getCurrentZoneId())
                .toLocalDate();
        else if (entity.getClass() == Loan.class) {

            if (!this.isInProlongationProcess) {
                issueDate = ((Loan) entity).getLoanIssueDate().atZone(chronometerService.getCurrentZoneId())
                    .toLocalDate();
            } else {
                issueDate = ((Loan) entity).getProlongDate();
                currentBalance = ((Loan) entity).getProlongValue();
            }
        } else
            issueDate = chronometerService.getCurrentTime().toLocalDate();

        // Блок 1
        cbCalcScheme = new ComboBox<>(
            i18n.get("scheduleForm.combobox.calcscheme"), Lists.newArrayList(CalcScheme.values()));
        cbCalcScheme.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbCalcScheme.setEmptySelectionAllowed(false);
        cbCalcScheme.setSizeFull();

        cbReturnTerm = new ComboBox<>(
            i18n.get("scheduleForm.combobox.returnterm"), Lists.newArrayList(ReturnTerm.values()));
        cbReturnTerm.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbReturnTerm.setEmptySelectionAllowed(false);
        cbReturnTerm.setSizeFull();

        GridLayout block1 = new GridLayout(2, 1, cbCalcScheme, cbReturnTerm);
        block1.setSpacing(true);
        block1.setSizeFull();
        block1.addStyleName(MaterialTheme.CARD_2);

        // Блок 2
        MoneyField tfLoanBalance = new MoneyField(
            i18n.get("scheduleForm.textfield.loanbalance"), configurationService.getLocale());
        tfLoanBalance.setSizeFull();

        ComboBox<Currency> cbCurrency = new ComboBox<>(i18n.get("scheduleForm.combobox.currency"));
        cbCurrency.setItems(configurationService.getCurrencies());
        cbCurrency.setItemCaptionGenerator(Currency::getCurrencyCode);
        cbCurrency.setEmptySelectionAllowed(false);
        cbCurrency.setSizeFull();

        tfInterestValue = new MoneyField(i18n.get("scheduleForm.textfield.interestvalue"),
            configurationService.getLocale(), 3);
        tfInterestValue.addStyleName("align-right");
        tfInterestValue.setSizeFull();

        cbInterestType = new ComboBox<>(
            i18n.get("scheduleForm.combobox.interesttype"), Lists.newArrayList(InterestType.values()));
        cbInterestType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbInterestType.setEmptySelectionAllowed(false);
        cbInterestType.setSizeFull();

        tfLoanTerm = new TextField(i18n.get("scheduleForm.textfield.loanterm"));
        tfLoanTerm.addStyleName("align-right");
        tfLoanTerm.setSizeFull();

        tfLoanTerm.addBlurListener(event -> {
            if (Integer.valueOf(tfLoanTerm.getValue()) < Integer.valueOf(tfNoInterestFDValue.getValue())) {
                tfNoInterestFDValue.setValue(tfLoanTerm.getValue());
            }
            if (Integer.valueOf(tfLoanTerm.getValue()) < Integer.valueOf(tfInterestFirstDays.getValue())) {
                tfInterestFirstDays.setValue(tfLoanTerm.getValue());
            }
        });

        cbLoanTermType = new ComboBox<>(
            i18n.get("scheduleForm.combobox.loantermtype"), Lists.newArrayList(LoanTermType.values()));
        cbLoanTermType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbLoanTermType.setEmptySelectionAllowed(false);
        cbLoanTermType.setSizeFull();

        GridLayout block2 = new GridLayout(2, 3, tfLoanBalance, cbCurrency, tfInterestValue, cbInterestType, tfLoanTerm,
            cbLoanTermType);
        block2.setSpacing(true);
        block2.setSizeFull();
        block2.addStyleName(MaterialTheme.CARD_2);
        block2.setColumnExpandRatio(0, 1);
        block2.setColumnExpandRatio(1, 1);

        // Блок 3
        cbxReplaceDate = new CheckBox(i18n.get("scheduleForm.checkbox.replacedate"));
        cbxReplaceDate.addStyleName(ValoTheme.CHECKBOX_SMALL);

        tfReplaceDate = new TextField();
        tfReplaceDate.setWidth(50, Unit.PIXELS);

        HorizontalLayout hlReplaceDate = new HorizontalLayout(cbxReplaceDate, tfReplaceDate);
        hlReplaceDate.setComponentAlignment(cbxReplaceDate, Alignment.MIDDLE_LEFT);

        cbxReplaceOverMonth = new CheckBox(i18n.get("scheduleForm.checkbox.replaceovermonth"));
        cbxReplaceOverMonth.addStyleName(ValoTheme.CHECKBOX_SMALL);

        cbxPrincipalLastPay = new CheckBox(i18n.get("scheduleForm.checkbox.principallastpay"));
        cbxPrincipalLastPay.addStyleName(ValoTheme.CHECKBOX_SMALL);

        cbxInterestBalance = new CheckBox(i18n.get("scheduleForm.checkbox.interestbalance"));
        cbxInterestBalance.addStyleName(ValoTheme.CHECKBOX_SMALL);

        cbxNoInterestFD = new CheckBox(i18n.get("scheduleForm.checkbox.nointerestfd"));
        cbxNoInterestFD.addStyleName(ValoTheme.CHECKBOX_SMALL);

        tfNoInterestFDValue = new TextField();
        tfNoInterestFDValue.setWidth(50, Unit.PIXELS);

        HorizontalLayout hlNoInterest = new HorizontalLayout(cbxNoInterestFD, tfNoInterestFDValue);
        hlNoInterest.setComponentAlignment(cbxNoInterestFD, Alignment.MIDDLE_CENTER);

        cbxIncludeIssueDate = new CheckBox(i18n.get("scheduleForm.checkbox.includeissuedate"));
        cbxIncludeIssueDate.addStyleName(ValoTheme.CHECKBOX_SMALL);

        VerticalLayout block3 = new VerticalLayout(hlReplaceDate, cbxReplaceOverMonth, cbxPrincipalLastPay,
            cbxInterestBalance, hlNoInterest, cbxIncludeIssueDate);
        block3.setSizeFull();
        block3.addStyleName(MaterialTheme.CARD_2);

        // Блок 4
        cbPenaltyType = new ComboBox<>(
            i18n.get("scheduleForm.combobox.penaltytype"), Lists.newArrayList(PenaltyType.values()));
        cbPenaltyType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbPenaltyType.setEmptySelectionAllowed(false);

        tfPenaltyValue = new MoneyField(
            i18n.get("scheduleForm.textfield.penaltyvalue"), configurationService.getLocale(), 3);
        tfPenaltyValue.setWidth(70, Unit.PIXELS);

        HorizontalLayout hlPenalty = new HorizontalLayout(cbPenaltyType, tfPenaltyValue);

        cbxChargeFine = new CheckBox(i18n.get("scheduleForm.checkbox.chargefine"));
        cbxChargeFine.addStyleName(ValoTheme.CHECKBOX_SMALL);

        tfFineValue = new MoneyField(null, configurationService.getLocale());
        tfFineValue.setWidth(100, Unit.PIXELS);

        cbxFineOneTime = new CheckBox(i18n.get("scheduleForm.checkbox.fineonetime"));
        cbxFineOneTime.addStyleName(ValoTheme.CHECKBOX_SMALL);

        HorizontalLayout hlFine = new HorizontalLayout(cbxChargeFine, tfFineValue, cbxFineOneTime);
        hlFine.setComponentAlignment(cbxChargeFine, Alignment.MIDDLE_CENTER);
        hlFine.setComponentAlignment(cbxFineOneTime, Alignment.MIDDLE_CENTER);

        cbxInterestStrictly = new CheckBox(i18n.get("scheduleForm.checkbox.intereststrictly"));
        cbxInterestStrictly.addStyleName(ValoTheme.CHECKBOX_SMALL);

        cbxInterestOverdue = new CheckBox(i18n.get("scheduleForm.checkbox.interestoverdue"));
        cbxInterestOverdue.addStyleName(ValoTheme.CHECKBOX_SMALL);

        cbxInterestFirstDays = new CheckBox(i18n.get("scheduleForm.checkbox.interestFirstDays1"));
        cbxInterestFirstDays.addStyleName(ValoTheme.CHECKBOX_SMALL);

        tfInterestFirstDays = new TextField();
        tfInterestFirstDays.addStyleName("align-right");
        tfInterestFirstDays.setWidth(50, Unit.PIXELS);

        lbInterestFirstDays = new Label(i18n.get("scheduleForm.checkbox.interestFirstDays2"));

        HorizontalLayout hlInterestFirstDays = new HorizontalLayout(cbxInterestFirstDays, tfInterestFirstDays,
            lbInterestFirstDays);
        hlInterestFirstDays.setComponentAlignment(cbxInterestFirstDays, Alignment.MIDDLE_CENTER);
        hlInterestFirstDays.setComponentAlignment(lbInterestFirstDays, Alignment.MIDDLE_CENTER);

        VerticalLayout block4 = new VerticalLayout(hlPenalty, hlFine, cbxInterestStrictly, cbxInterestOverdue,
            hlInterestFirstDays);
        block4.setSizeFull();
        block4.addStyleName(MaterialTheme.CARD_2);

        psk = new Label(i18n.get("zone.russia.scheduleForm.textfield.psk").concat(": 0.000%"));
        lastPayDate = new Label(i18n.get("scheduleForm.field.lastpaydate").concat(":-"));
        interestTotal = new Label(i18n.get("scheduleForm.field.interesttotal").concat(":0.00"));

        VerticalLayout block5 = new VerticalLayout(psk, lastPayDate, interestTotal);
        block5.addStyleName(MaterialTheme.CARD_2);

        VerticalLayout leftBlock = new VerticalLayout(block1, block2, block5);
        VerticalLayout rightBlock = new VerticalLayout(block3, block4);

        HorizontalLayout blocksLayout = new HorizontalLayout(leftBlock, rightBlock);
        blocksLayout.setSizeFull();
        blocksLayout.setMargin(false);

        btCalculate = new Button(i18n.get("scheduleForm.button.calculate"));
        btCalculate.setIcon(VaadinIcons.TABLE);
        btCalculate.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        HorizontalLayout calculateLayout = new HorizontalLayout(btCalculate);
        calculateLayout.setWidth(100, Unit.PERCENTAGE);
        calculateLayout.addComponents();
        calculateLayout.setComponentAlignment(btCalculate, Alignment.TOP_RIGHT);

        // Layout со всеми блоками
        VerticalLayout scheduleLayout = new VerticalLayout(blocksLayout, calculateLayout);
        scheduleLayout.setMargin(false);

        gridSchedule = new Grid<>();
        gridSchedule.setSizeFull();
        Grid.Column column1 = gridSchedule.addColumn(Schedule::getOrdNo)
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.order"));
        column1.setWidth(100);
        Grid.Column column2 = gridSchedule.addColumn(item -> i18n.get(item.getStatus().getName()))
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.status"));
        column2.setWidth(150);
        gridSchedule.addColumn(item -> dateFormatter.format(item.getDate()))
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.date"));
        gridSchedule.addColumn(Schedule::getTerm)
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.term"));
        gridSchedule.addColumn(item -> moneyFormat.format(item.getPrincipal()))
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.principal"))
            .setStyleGenerator(item -> "v-align-right");
        gridSchedule.addColumn(item -> moneyFormat.format(item.getInterest()))
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.interest"))
            .setStyleGenerator(item -> "v-align-right");
        gridSchedule.addColumn(item -> moneyFormat.format(item.getPayment()))
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.payment"))
            .setStyleGenerator(item -> "v-align-right");
        gridSchedule.addColumn(item -> moneyFormat.format(item.getBalance()))
            .setCaption(i18n.get("scheduleForm.schedule.grid.title.balance"))
            .setStyleGenerator(item -> "v-align-right");

        if ((entity.getClass() == Loan.class) || (entity.getClass() == Request.class)) {//для займа или заявки
            if (entity.getReturnTerm() == ReturnTerm.INDIVIDUALLY) {//индивидуальный
                if (entity.getId() == null) {

                    if ((entity.getClass() == Loan.class && ((Loan) entity).getLoanProduct() != null) ||
                        (entity.getClass() == Loan.class && ((Loan) entity).getRequest() != null) ||
                        (entity.getClass() == Request.class && ((Request) entity).getLoanProduct() != null)) {
                        scheduleList = entity.getSchedules();
                    } else scheduleList = new ArrayList<>();
                } else
                    scheduleList = entity.getSchedules();
            } else {
                if (entity.getId() == null) {
                    scheduleList = calculateScheduleList(entity, issueDate, currentBalance);
                    setSchedule();
                } else
                    scheduleList = entity.getSchedules();
            }
        } else {//для кредитного продукта
            if (entity.getReturnTerm() == ReturnTerm.INDIVIDUALLY) {//индивидуальный
                if (entity.getId() == null) {
                    scheduleList = new ArrayList<>();
                } else
                    scheduleList = entity.getSchedules();
            } else {//не индивидуальный
                scheduleList = calculateScheduleList(entity, issueDate, currentBalance);
            }
        }

        gridSchedule.setItems(scheduleList);

        if (scheduleList.size() > 0) {
            entity.setInterestTotal(calculateInterestTotal(scheduleList));
            entity.setLastPayDate(scheduleList.get(scheduleList.size() - 1).getDate());
            //переделать дату выдачи
            CalculatorPSK calculatorPSK = new CalculatorPSK(
                entity.getLoanBalance(), issueDate, scheduleList);

            BigDecimal pskValue = calculatorPSK.calculate();
            entity.setPskValue(pskValue);

            psk.setValue(i18n.get("zone.russia.scheduleForm.textfield.psk").concat(": ")
                .concat(pskValue.toString()).concat("%"));
            lastPayDate.setValue(i18n.get("scheduleForm.field.lastpaydate").concat(": ")
                .concat(dateFormatter.format(entity.getLastPayDate())));
            interestTotal.setValue(i18n.get("scheduleForm.field.interesttotal").concat(": ")
                .concat(moneyFormat.format(entity.getInterestTotal())));
        }

        // Размещаем все компоненты на форме
        addComponents(scheduleLayout, gridSchedule);
        setSizeFull();

        // Настройка биндера
        binder.bind(cbCalcScheme, BaseLoan::getCalcScheme, BaseLoan::setCalcScheme);
        binder.bind(cbReturnTerm, BaseLoan::getReturnTerm, BaseLoan::setReturnTerm);
        binder.forField(tfLoanBalance)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(BaseLoan::getLoanBalance, BaseLoan::setLoanBalance);
        binder.forField(tfInterestValue)
            .withConverter(new PercentageConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number from 1 to 1000", BigDecimal.ZERO, new BigDecimal(MAX_INTEREST_VALUE)))
            .bind(BaseLoan::getInterestValue, BaseLoan::setInterestValue);
        binder.forField(tfLoanTerm)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .withValidator(new IntegerRangeValidator("Must be a number from 0 to 1000", 0, MAX_LOAN_TERM))
            .bind(BaseLoan::getLoanTerm, BaseLoan::setLoanTerm);
        binder.bind(cbInterestType, BaseLoan::getInterestType, BaseLoan::setInterestType);
        binder.bind(cbLoanTermType, BaseLoan::getLoanTermType, BaseLoan::setLoanTermType);
        binder.forField(cbCurrency)
            .bind(BaseLoan::getCurrency, BaseLoan::setCurrency);
        binder.bind(cbxReplaceDate, BaseLoan::isReplaceDate, BaseLoan::setReplaceDate);
        binder.forField(tfReplaceDate)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .withValidator(day -> day >= 0 && day <= 20, "Must be within 1 and 20")
            .bind(BaseLoan::getReplaceDateValue, BaseLoan::setReplaceDateValue);
        binder.bind(cbxReplaceOverMonth, BaseLoan::isReplaceOverMonth, BaseLoan::setReplaceOverMonth);
        binder.bind(cbxPrincipalLastPay, BaseLoan::isPrincipalLastPay, BaseLoan::setPrincipalLastPay);
        binder.bind(cbxInterestBalance, BaseLoan::isInterestBalance, BaseLoan::setInterestBalance);
        binder.bind(cbxNoInterestFD, BaseLoan::isNoInterestFD, BaseLoan::setNoInterestFD);
        binder.forField(tfNoInterestFDValue)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .withValidator(day -> day >= 0 && day <= Integer.valueOf(tfLoanTerm.getValue()), "Must be within 1 and loan term")
            .bind(BaseLoan::getNoInterestFDValue, BaseLoan::setNoInterestFDValue);
        binder.bind(cbxIncludeIssueDate, BaseLoan::isIncludeIssueDate, BaseLoan::setIncludeIssueDate);

        binder.bind(cbPenaltyType, BaseLoan::getPenaltyType, BaseLoan::setPenaltyType);
        binder.forField(tfPenaltyValue)
            .withConverter(new PercentageConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number from 1 to 100", BigDecimal.ZERO, BigDecimal.valueOf(MAX_PENALTY_VALUE)))
            .bind(BaseLoan::getPenaltyValue, BaseLoan::setPenaltyValue);
        binder.bind(cbxChargeFine, BaseLoan::isChargeFine, BaseLoan::setChargeFine);
        binder.forField(tfFineValue)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number from 1 to 10000", BigDecimal.ZERO, BigDecimal.valueOf(MAX_FINE_VALUE)))
            .bind(BaseLoan::getFineValue, BaseLoan::setFineValue);
        binder.bind(cbxFineOneTime, BaseLoan::isFineOneTime, BaseLoan::setFineOneTime);
        binder.bind(cbxInterestStrictly, BaseLoan::isInterestStrictly, BaseLoan::setInterestStrictly);
        binder.bind(cbxInterestOverdue, BaseLoan::isInterestOverdue, BaseLoan::setInterestOverdue);

        binder.bind(cbxInterestFirstDays, BaseLoan::isInterestFirstDays, BaseLoan::setInterestFirstDays);
        binder.forField(tfInterestFirstDays)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .withValidator(day -> day >= 0 && day <= Integer.valueOf(tfLoanTerm.getValue()), "Must be within 1 and loan term")
            .bind(BaseLoan::getInterestNumberFirstDays, BaseLoan::setInterestNumberFirstDays);

        // Включаем обработку выбора компонентов
        binder.addValueChangeListener(event -> componentStatusManager(event.getComponent(), event.getValue()));

        btCalculate.addClickListener(event -> {
            if (cbReturnTerm.getValue() == ReturnTerm.INDIVIDUALLY) {
                scheduleList = new ArrayList<>();
                setSchedule();
                gridSchedule.setItems(new ArrayList<>());

                ScheduleManual<T> scheduleManual;

                if (!isInProlongationProcess) {
                    scheduleManual = new ScheduleManual<>(this, entity, i18n, configurationService, getLoanBalance(),
                        getInterestValue(), getLoanIssueDate(), cbxInterestBalance.getValue(), cbxIncludeIssueDate.getValue(),
                        sessionEventBus);
                } else {
                    scheduleManual = new ScheduleManual<>(this, entity, i18n, configurationService, getProlongValue(),
                        getInterestValue(), getProlongDate(), cbxInterestBalance.getValue(), cbxIncludeIssueDate.getValue(),
                        sessionEventBus);
                }
                UI.getCurrent().addWindow(scheduleManual);
            } else {
                if (!this.isInProlongationProcess) {
                    scheduleList = calculateScheduleList(entity, issueDate, getLoanBalance());
                } else scheduleList = calculateScheduleList(entity, issueDate, getProlongValue());
                setSchedule();
                gridSchedule.setItems(scheduleList);
                setInfo();
            }

            calculateScheduleHash();
            sessionEventBus.post(new ScheduleRecalculatedEvent<>(entity));
            Notification.show(i18n.get("scheduleForm.notification.message.scheduleCalculated"), Notification.Type.TRAY_NOTIFICATION);
        });
    }

    public int getScheduleHash() {
        return scheduleHash;
    }

    public void calculateScheduleHash() {
        scheduleHash = entity.scheduleHashCode();
    }

    public void setSchedule() {
        entity.setSchedules(scheduleList);
    }

    public Binder<T> getBinder() {
        return binder;
    }

    public void setInfo() {
        if (scheduleList.size() > 0) {
            entity.setInterestTotal(calculateInterestTotal(scheduleList));
            entity.setLastPayDate(scheduleList.get(scheduleList.size() - 1).getDate());
            binder.readBean(entity);

            CalculatorPSK calculatorPSK = new CalculatorPSK(
                entity.getLoanBalance(), issueDate, scheduleList);
            BigDecimal pskValue = calculatorPSK.calculate();
            entity.setPskValue(pskValue);
            psk.setValue(i18n.get("zone.russia.scheduleForm.textfield.psk").concat(": ").concat(
                pskValue.toString()).concat("%"));
            lastPayDate.setValue(i18n.get("scheduleForm.field.lastpaydate").concat(": ")
                .concat(dateFormatter.format(entity.getLastPayDate())));
            interestTotal.setValue(i18n.get("scheduleForm.field.interesttotal").concat(": ")
                .concat(moneyFormat.format(entity.getInterestTotal())));
        }
    }

    private BigDecimal calculateInterestTotal(List<Schedule> list) {
        BigDecimal interestTotal = new BigDecimal(0);
        if (list.size() > 0) {
            for (Schedule schedule : list) {
                interestTotal = interestTotal.add(schedule.getInterest());
            }
        }
        return interestTotal;
    }

    private void componentStatusManager(Component component, Object value) {

        if (component.equals(cbCalcScheme)) {
            List<State> states = State.findAllByCalcScheme((CalcScheme) value);
            List<ReturnTerm> list = states.stream().map(State::getReturnTerm).distinct().collect(Collectors.toList());
            cbReturnTerm.setItems(list);
            ReturnTerm returnTerm = list.contains(entity.getReturnTerm())
                ? entity.getReturnTerm()
                : states.get(0).getReturnTerm();
            cbReturnTerm.setSelectedItem(returnTerm);
        }

        if (component.equals(cbReturnTerm)) {
            List<State> states = State.findAllByCalcScheme(cbCalcScheme.getValue()).stream()
                .filter(item -> item.getReturnTerm() == value)
                .distinct().collect(Collectors.toList());
            List<InterestType> list = states.stream().map(State::getInterestType).distinct().collect(Collectors.toList());
            cbInterestType.setItems(list);
            InterestType interestType = list.contains(entity.getInterestType())
                ? entity.getInterestType()
                : states.get(0).getInterestType();
            cbInterestType.setSelectedItem(interestType);
        }

        if (component.equals(cbInterestType)) {
            List<State> states = State.findAllByCalcScheme(cbCalcScheme.getValue()).stream()
                .filter(item -> item.getReturnTerm() == cbReturnTerm.getValue())
                .filter(item -> item.getInterestType() == value)
                .distinct().collect(Collectors.toList());
            List<LoanTermType> list = states.stream().map(State::getLoanTermType).distinct().collect(Collectors.toList());
            cbLoanTermType.setItems(list);
            LoanTermType loanTermType = list.contains(entity.getLoanTermType())
                ? entity.getLoanTermType()
                : states.get(0).getLoanTermType();
            cbLoanTermType.setSelectedItem(loanTermType);
        }

        if (component.equals(cbLoanTermType)) {
            List<State> states = State.findAllByCalcScheme(cbCalcScheme.getValue()).stream()
                .filter(item -> item.getReturnTerm() == cbReturnTerm.getValue())
                .filter(item -> item.getInterestType() == cbInterestType.getValue())
                .filter(item -> item.getLoanTermType() == value)
                .distinct().collect(Collectors.toList());

            cbxReplaceDate.setEnabled(Lists.newArrayList(State.A, State.B, State.C, State.F, State.G, State.H, State.I)
                .stream().anyMatch(states::contains));
            cbxReplaceDate.setValue(cbxReplaceDate.isEnabled() ? cbxReplaceDate.getValue() : false);

            tfReplaceDate.setEnabled(cbxReplaceDate.getValue());
            if (!cbxReplaceDate.getValue()) tfReplaceDate.setValue("0");

            cbxReplaceOverMonth.setEnabled(Lists.newArrayList(State.A, State.B, State.C, State.F, State.G, State.H, State.I)
                .stream().anyMatch(states::contains) && cbxReplaceDate.getValue());

            cbxPrincipalLastPay.setEnabled(Lists.newArrayList(State.F, State.G, State.H, State.I, State.J, State.K)
                .stream().anyMatch(states::contains));
            cbxPrincipalLastPay.setValue(cbxPrincipalLastPay.isEnabled() ? cbxPrincipalLastPay.getValue() : false);

            cbxInterestBalance.setEnabled(Lists.newArrayList(State.F, State.G, State.H, State.I, State.J, State.K)
                .stream().anyMatch(states::contains));
            cbxInterestBalance.setValue(cbxInterestBalance.isEnabled() ? cbxInterestBalance.getValue() : false);

            cbxNoInterestFD.setEnabled(Lists.newArrayList(State.L, State.M, State.N)
                .stream().anyMatch(states::contains));
            cbxNoInterestFD.setValue(cbxNoInterestFD.isEnabled() ? cbxNoInterestFD.getValue() : false);

            tfNoInterestFDValue.setEnabled(cbxNoInterestFD.getValue());
            if (!cbxNoInterestFD.getValue()) tfNoInterestFDValue.setValue("0");

            cbxIncludeIssueDate.setEnabled(Lists.newArrayList(State.L, State.M, State.N, State.O)
                .stream().anyMatch(states::contains));
            cbxIncludeIssueDate.setValue(cbxIncludeIssueDate.isEnabled() ? cbxIncludeIssueDate.getValue() : false);

            cbxInterestFirstDays.setEnabled(Lists.newArrayList(State.L, State.M, State.N, State.O)
                .stream().anyMatch(states::contains));
            cbxInterestFirstDays.setValue(cbxInterestFirstDays.isEnabled() ? cbxInterestFirstDays.getValue() : false);

            tfInterestFirstDays.setEnabled(cbxInterestFirstDays.getValue());
            if (!cbxInterestFirstDays.getValue()) tfInterestFirstDays.setValue("0");

            if (cbPenaltyType.getValue() == PenaltyType.NOT_DEFINED) {
                tfPenaltyValue.setEnabled(false);
            } else {
                tfPenaltyValue.setEnabled(true);
            }
            cbxFineOneTime.setEnabled(cbxChargeFine.getValue());
            tfFineValue.setEnabled(cbxChargeFine.getValue());
        }

        if (component.equals(cbxReplaceDate)) {
            List<State> states = State.findAllByCalcScheme(cbCalcScheme.getValue()).stream()
                .filter(item -> item.getReturnTerm() == cbReturnTerm.getValue())
                .filter(item -> item.getInterestType() == cbInterestType.getValue())
                .filter(item -> item.getLoanTermType() == cbLoanTermType.getValue())
                .distinct().collect(Collectors.toList());

            tfReplaceDate.setEnabled(cbxReplaceDate.getValue());
            if (!cbxReplaceDate.getValue()) tfReplaceDate.setValue("0");

            cbxReplaceOverMonth.setValue(Lists.newArrayList(State.B).stream().anyMatch(states::contains)
                && cbxReplaceDate.getValue());
            cbxReplaceOverMonth.setEnabled(Lists.newArrayList(State.A, State.B, State.C, State.G, State.I).stream().anyMatch(states::contains)
                && cbxReplaceDate.getValue());
        }

        if (component.equals(cbxPrincipalLastPay)) {
            if (cbxPrincipalLastPay.getValue()) cbxInterestBalance.setValue(false);
        }
        if (component.equals(cbxInterestBalance)) {
            if (cbxInterestBalance.getValue()) cbxPrincipalLastPay.setValue(false);
        }

        if (component.equals(cbxNoInterestFD)) {
            tfNoInterestFDValue.setEnabled(cbxNoInterestFD.getValue());
            if (cbxNoInterestFD.getValue()) {
                cbxInterestFirstDays.setEnabled(false);
                cbxInterestFirstDays.setValue(false);
                tfInterestFirstDays.setEnabled(false);
                tfInterestFirstDays.setValue("0");
            } else {
                if (cbReturnTerm.getValue() == ReturnTerm.IN_THE_END_OF_TERM ||
                    cbReturnTerm.getValue() == ReturnTerm.INDIVIDUALLY) {
                    cbxInterestFirstDays.setEnabled(true);
                    tfInterestFirstDays.setEnabled(true);
                    tfNoInterestFDValue.setValue("0");
                }
            }
        }

        if (component.equals(cbxInterestFirstDays)) {
            tfInterestFirstDays.setEnabled(cbxInterestFirstDays.getValue());

            if (cbxInterestFirstDays.getValue()) {
                cbxNoInterestFD.setEnabled(false);
                cbxNoInterestFD.setValue(false);
                tfNoInterestFDValue.setEnabled(false);
                tfNoInterestFDValue.setValue("0");
            } else {
                if (cbReturnTerm.getValue() == ReturnTerm.IN_THE_END_OF_TERM ||
                    cbReturnTerm.getValue() == ReturnTerm.INDIVIDUALLY) {
                    cbxNoInterestFD.setEnabled(true);
                    tfNoInterestFDValue.setEnabled(true);
                    tfInterestFirstDays.setValue("0");
                }
            }
        }

        //блок неустойки и штрафов
        if (component.equals(cbPenaltyType)) {
            if (cbPenaltyType.getValue() == PenaltyType.NOT_DEFINED) {
                tfPenaltyValue.setEnabled(false);
                tfPenaltyValue.setValue("0.000");
            } else {
                tfPenaltyValue.setEnabled(true);
            }
        }
        if (component.equals(cbxChargeFine)) {
            cbxFineOneTime.setEnabled(cbxChargeFine.getValue());
            cbxFineOneTime.setValue(false);
            tfFineValue.setEnabled(cbxChargeFine.getValue());
            tfFineValue.setValue("0.00");
        }

        if (component.equals(cbxInterestStrictly)) {
            if (cbxInterestStrictly.getValue()) cbxInterestOverdue.setValue(false);
        }

        if (component.equals(cbxInterestOverdue)) {
            if (cbxInterestOverdue.getValue()) cbxInterestStrictly.setValue(false);
        }
    }

    private List<Schedule> calculateScheduleList(BaseLoan entity, LocalDate calcDate, BigDecimal currentBalance) {

        Calculator calculator = Calculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setLoanIssueDate(calcDate)
            .setLoanBalance(currentBalance)
            .setReplaceDate(entity.isReplaceDate())
            .setReplaceDateValue(entity.getReplaceDateValue())
            .setReplaceOverMonth(entity.isReplaceOverMonth())
            .setPrincipalLastPay(entity.isPrincipalLastPay())
            .setInterestBalance(entity.isInterestBalance())
            .setNoInterestFD(entity.isNoInterestFD())
            .setNoInterestFDValue(entity.getNoInterestFDValue())
            .setIncludeIssueDate(entity.isIncludeIssueDate())
            .setDaysInYear(365)
            .setCalcScheme(entity.getCalcScheme())
            .setReturnTerm(entity.getReturnTerm())
            .setInterestRate(entity.getInterestValue())
            .setInterestRateType(entity.getInterestType())
            .setLoanTerm(entity.getLoanTerm())
            .build();

        return calculator.calculate();
    }


    private enum State {
        A(CalcScheme.ANNUITY, ReturnTerm.MONTHLY, InterestType.PERCENT_A_MONTH, LoanTermType.MONTHS),
        B(CalcScheme.ANNUITY, ReturnTerm.MONTHLY, InterestType.PERCENT_A_MONTH, LoanTermType.MONTHS),
        C(CalcScheme.ANNUITY, ReturnTerm.MONTHLY, InterestType.PERCENT_A_YEAR, LoanTermType.MONTHS),
        D(CalcScheme.ANNUITY, ReturnTerm.WEEKLY, InterestType.PERCENT_A_WEEK, LoanTermType.WEEKS),
        E(CalcScheme.ANNUITY, ReturnTerm.WEEKLY, InterestType.PERCENT_A_YEAR, LoanTermType.WEEKS),
        F(CalcScheme.DIFFERENTIAL, ReturnTerm.MONTHLY, InterestType.PERCENT_A_MONTH, LoanTermType.MONTHS),
        G(CalcScheme.DIFFERENTIAL, ReturnTerm.MONTHLY, InterestType.PERCENT_A_MONTH, LoanTermType.MONTHS),
        H(CalcScheme.DIFFERENTIAL, ReturnTerm.MONTHLY, InterestType.PERCENT_A_YEAR, LoanTermType.MONTHS),
        I(CalcScheme.DIFFERENTIAL, ReturnTerm.MONTHLY, InterestType.PERCENT_A_YEAR, LoanTermType.MONTHS),
        J(CalcScheme.DIFFERENTIAL, ReturnTerm.WEEKLY, InterestType.PERCENT_A_WEEK, LoanTermType.WEEKS),
        K(CalcScheme.DIFFERENTIAL, ReturnTerm.WEEKLY, InterestType.PERCENT_A_YEAR, LoanTermType.WEEKS),
        L(CalcScheme.DIFFERENTIAL, ReturnTerm.IN_THE_END_OF_TERM, InterestType.PERCENT_A_DAY, LoanTermType.DAYS),
        M(CalcScheme.DIFFERENTIAL, ReturnTerm.IN_THE_END_OF_TERM, InterestType.PERCENT_A_YEAR, LoanTermType.DAYS),
        N(CalcScheme.DIFFERENTIAL, ReturnTerm.IN_THE_END_OF_TERM, InterestType.PERCENT_A_WHOLE_PERIOD, LoanTermType.DAYS),
        O(CalcScheme.DIFFERENTIAL, ReturnTerm.INDIVIDUALLY, InterestType.PERCENT_A_DAY, LoanTermType.DAYS);

        private CalcScheme calcScheme;
        private ReturnTerm returnTerm;
        private InterestType interestType;
        private LoanTermType loanTermType;

        State(CalcScheme calcScheme, ReturnTerm returnTerm, InterestType interestType, LoanTermType loanTermType) {
            this.calcScheme = calcScheme;
            this.returnTerm = returnTerm;
            this.interestType = interestType;
            this.loanTermType = loanTermType;
        }


        public CalcScheme getCalcScheme() {
            return calcScheme;
        }

        public ReturnTerm getReturnTerm() {
            return returnTerm;
        }

        public InterestType getInterestType() {
            return interestType;
        }

        public LoanTermType getLoanTermType() {
            return loanTermType;
        }

        public static List<State> findAllByCalcScheme(CalcScheme calcScheme) {
            return Arrays.stream(State.values())
                .filter(item -> item.getCalcScheme() == calcScheme)
                .collect(Collectors.toList());
        }
    }

    public TextField getTfLoanTerm() {
        return tfLoanTerm;
    }

    public MoneyField getTfInterestValue() {
        return tfInterestValue;
    }

    public MoneyField getTfPenaltyValue() {
        return tfPenaltyValue;
    }

    public LocalDate getLoanIssueDate() {
        return issueDate;
    }

    public void setLoanIssueDate(LocalDate loanIssueDate) {
        this.issueDate = loanIssueDate;
    }

    public BigDecimal getLoanBalance() {
        return entity.getLoanBalance();
    }

    public BigDecimal getProlongValue() {
        return ((Loan) entity).getProlongValue();
    }

    public LocalDate getProlongDate() {
        return ((Loan) entity).getProlongDate();
    }

    public BigDecimal getInterestValue() {
        return entity.getInterestValue();
    }

    public boolean isIncludeIssueDate() {
        return cbxIncludeIssueDate.getValue();
    }

    public void setVisible(boolean visible) {
        setVisible(visible);
    }

    public List<Schedule> getScheduleList() {
        return scheduleList;
    }

    public Grid<Schedule> getGridSchedule() {
        return gridSchedule;
    }

    public Button getBtCalculate() {
        return btCalculate;
    }

    @Override
    public void attach() {
        super.attach();
        componentStatusManager(cbCalcScheme, entity.getCalcScheme());
        componentStatusManager(cbReturnTerm, entity.getReturnTerm());
        componentStatusManager(cbInterestType, entity.getInterestType());
        componentStatusManager(cbLoanTermType, entity.getLoanTermType());
    }
}

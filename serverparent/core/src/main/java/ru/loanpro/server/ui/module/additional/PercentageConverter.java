package ru.loanpro.server.ui.module.additional;

import java.util.Locale;

/**
 * Конвертер процентов для текстового поля. Учитывает локаль процентов, но устанавливает
 * количество дробных знаков принудительно в 3.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public class PercentageConverter extends BigDecimalConverter {

    /**
     * Конструктор, принимающий локаль и сообщение об ошибке, если не удаётся
     * преобразовать строку к <code>BigDecimal</code>, с учётом переданной
     * локали.
     *
     * @param locale локаль денежной единицы.
     * @param errorMessage сообщение об ошибке.
     */
    public PercentageConverter(Locale locale, String errorMessage) {
        super(locale, errorMessage);
        format.setMaximumFractionDigits(3);
    }
}

package ru.loanpro.server.ui.module.additional;

import java.util.Locale;

/**
 * Конвертер денежных единиц для текстового поля. Учитывает локаль денежной единицы.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public class MoneyConverter extends BigDecimalConverter {

    /**
     * Конструктор, принимающий локаль денежной единицы и сообщение об ошибке, если
     * не удаётся преобразовать строку к <code>BigDecimal</code>, с учётом переданной
     * локали.
     *
     * @param locale локаль денежной единицы.
     * @param errorMessage сообщение об ошибке.
     */
    public MoneyConverter(Locale locale, String errorMessage){
        super(locale, errorMessage);
    }
}
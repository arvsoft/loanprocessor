package ru.loanpro.server.logging.domain;

import ru.loanpro.server.logging.annotation.LogName;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

/**
 * Лог работы с сущностями системы.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("ENTITY")
@LogName("log.entity")
public class LogEntity extends AbstractLog {

    /**
     * Имя класса сущности, для которой пишется лог.
     */
    @Column(name = "target_class")
    private String tClass;

    /**
     * Идентификатор сущности.
     */
    @Column(name = "entity_id")
    private UUID entityId;

    /**
     * Действие, выполняемое с сущностью.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "action")
    private EntityActions action;

    public String gettClass() {
        return tClass;
    }

    public void settClass(String tClass) {
        this.tClass = tClass;
    }

    public UUID getEntityId() {
        return entityId;
    }

    public void setEntityId(UUID entityId) {
        this.entityId = entityId;
    }

    public EntityActions getAction() {
        return action;
    }

    public void setAction(EntityActions action) {
        this.action = action;
    }
}

package ru.loanpro.server.logging.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ru.loanpro.server.logging.annotation.LogName;

/**
 * Лог закрытия вкладки.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("TAB_CLOSE")
@LogName("log.closeTab")
public class LogCloseTab extends SingletonTabLog {
}

package ru.loanpro.server.ui.module.event;

import ru.loanpro.base.loan.domain.BaseLoan;

/**
 * Класс события сохранения базовой сущности займа.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public class UpdateBaseLoanEvent<T extends BaseLoan> {

    T entity;

    public UpdateBaseLoanEvent(T entity) {
        this.entity = entity;
    }

    public T getEntity() {
        return entity;
    }
}


package ru.loanpro.server.ui.module;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityName;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.global.domain.BaseQueryDSL;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.UserService;
import ru.loanpro.server.logging.annotation.LogName;
import ru.loanpro.server.logging.domain.AbstractLog;
import ru.loanpro.server.logging.domain.LogCloseTab;
import ru.loanpro.server.logging.domain.LogEntity;
import ru.loanpro.server.logging.domain.LogEntityPrint;
import ru.loanpro.server.logging.domain.LogOpenTab;
import ru.loanpro.server.logging.domain.LogTimeShift;
import ru.loanpro.server.logging.domain.QAbstractLog;
import ru.loanpro.server.logging.domain.SingletonTabLog;
import ru.loanpro.server.logging.service.LogService;
import ru.loanpro.server.logging.service.Pair;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.AddTabEvent;

import javax.annotation.PostConstruct;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@PrototypeScope
@SingletonTab("core.menu.logs")
public class LogsTab extends BaseTab {

    @Autowired
    private LogService service;
    @Autowired
    private UserService userService;
    @Autowired
    private ApplicationContext context;

    private final Map<String, Pair<Class<? extends BaseTab>, String>> singletonTabMap = new HashMap<>();
    private final Map<String, Pair<Class<? extends BaseTab>, String>> entityTabMap = new HashMap<>();
    private final Map<String, Pair<Class<?>, String>> entityMap = new HashMap<>();
    private final Map<Class, String> logMap = new HashMap<>();

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    private ComboBox<User> user;
    private DateTimeField dateFrom;
    private DateTimeField dateTo;
    private Grid<AbstractLog> grid;

    @PostConstruct
    public void init() {
        initLogMetaData();
        initGrid();

        user = new ComboBox<>(i18n.get("logs.user.comboBox"), userService.findAllActiveUsers());
        user.setEmptySelectionCaption(i18n.get("logs.user.all"));
        user.setItemCaptionGenerator(User::getUsername);
        user.addValueChangeListener(event -> updateFilter());

        dateFrom = new DateTimeField(i18n.get("logs.dateTime.from"));
        dateFrom.setDateFormat("dd.MM.yyyy HH:mm");
        dateFrom.addValueChangeListener(event -> updateFilter());

        dateTo = new DateTimeField(i18n.get("logs.dateTime.to"));
        dateTo.setDateFormat("dd.MM.yyyy HH:mm");
        dateTo.addValueChangeListener(event -> updateFilter());

        Button refresh = new Button(VaadinIcons.REFRESH);
        refresh.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        refresh.addClickListener(event -> updateFilter());

        HorizontalLayout horizontalLayout = new HorizontalLayout(user, dateFrom, dateTo, refresh);
        horizontalLayout.setComponentAlignment(refresh, Alignment.BOTTOM_LEFT);

        VerticalLayout layout = new VerticalLayout(horizontalLayout, grid);
        layout.setExpandRatio(grid, 1);
        layout.setSizeFull();

        addComponent(layout);
        setSizeFull();
        updateFilter();
    }

    private void initGrid() {
        grid = new Grid<>(AbstractLog.class);
        grid.removeAllColumns();
        grid.setSizeFull();

        grid.addColumn(item -> item.getUser() == null ? "" : item.getUser().getUsername())
            .setSortProperty("user")
            .setCaption(i18n.get("logs.grid.title.user"));

        Grid.Column<AbstractLog, String> instantColumn = grid.addColumn(item ->
            chronometerService.zonedDateTimeToString(item.getInstant(), formatter))
            .setSortProperty("instant");
        instantColumn.setCaption(i18n.get("logs.grid.title.instant"));

        /*grid.addColumn(AbstractLog::getIp)
            .setSortProperty("ip")
            .setCaption(i18n.get("logs.grid.title.ip"));*/

        grid
            .addColumn(item -> {
                if (item instanceof LogEntity)
                    return getEntityLog(item);

                else if (item instanceof LogOpenTab || item instanceof LogCloseTab)
                    return getActionTabLog(item);

                else
                    return i18n.get(item.getClass().getAnnotation(LogName.class).value());
            })
            .setRenderer(new HtmlRenderer())
            .setCaption(i18n.get("logs.grid.title.logType"));

        grid.addComponentColumn(item -> {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setSpacing(true);

            if (item instanceof LogEntity && ((LogEntity) item).getEntityId() != null) {
                Button button = new Button(VaadinIcons.EYE);
                button.addStyleNames(ValoTheme.BUTTON_BORDERLESS_COLORED, ValoTheme.BUTTON_SMALL);
                button.addClickListener(event -> {
                    Class aClass = entityMap.get(((LogEntity) item).gettClass()).getKey();
                    Class<? extends BaseTab> targetTab = entityTabMap.values().stream()
                        .map(Pair::getKey)
                        .filter(clazz -> clazz.getAnnotation(EntityTab.class).value() == aClass)
                        .findFirst().get();
                    JpaRepository repository = (JpaRepository) new Repositories(context).getRepositoryFor(aClass).get();
                    sessionEventBus.post(new AddTabEvent(targetTab,
                        repository.findById(((LogEntity) item).getEntityId()).get()));
                });
                layout.addComponents(button);
            }
            if (item instanceof LogEntityPrint) {
                Button button = new Button(VaadinIcons.PRINT);
                button.addStyleNames(ValoTheme.BUTTON_BORDERLESS_COLORED, ValoTheme.BUTTON_SMALL);
                button.addClickListener(event -> getUI().getPage().open(
                    String.format("/print/%s", ((LogEntityPrint) item).getFileName()), "_blank"));
                layout.addComponents(button);
            }
            if (item instanceof LogTimeShift) {
                layout.addComponent(new Label(
                    chronometerService.zonedDateTimeToString(((LogTimeShift) item).getTimeShift(), formatter)));
            }
            return layout;
        }).setCaption(i18n.get("logs.grid.title.action"));


        grid.sort(instantColumn, SortDirection.DESCENDING);
    }

    private void updateFilter() {
        QAbstractLog log = QAbstractLog.abstractLog;

        Predicate predicate = new BooleanBuilder()
            .and(BaseQueryDSL.eq(log.user, user))
            .and(BaseQueryDSL.gt(log.instant, dateFrom, configurationService.getZoneId()))
            .and(BaseQueryDSL.lt(log.instant, dateTo, configurationService.getZoneId()))
            .getValue();

        grid.setDataProvider(
            (sortOrder, offset, limit) -> service.findAll(predicate, sortOrder, offset, limit),
            () -> service.count(predicate));
    }

    private void initLogMetaData() {
        ClassPathScanningCandidateComponentProvider scanner =
            new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(SingletonTab.class));
        scanner.addIncludeFilter(new AnnotationTypeFilter(EntityTab.class));
        scanner.addIncludeFilter(new AnnotationTypeFilter(EntityName.class));
        scanner.addIncludeFilter(new AnnotationTypeFilter(LogName.class));

        Set<Class<?>> classSet = scanner.findCandidateComponents("ru.loanpro").stream()
            .map(bean -> {
                try {
                    return Class.forName(bean.getBeanClassName());
                } catch (ClassNotFoundException e) {
                    return null;
                }
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());

        classSet.stream()
            .filter(clazz -> clazz.isAnnotationPresent(LogName.class))
            .forEach(clazz ->
                logMap.put(clazz, i18n.get(clazz.getAnnotation(LogName.class).value())));

        classSet.stream()
            .filter(clazz -> clazz.isAnnotationPresent(SingletonTab.class))
            .forEach(clazz ->
                singletonTabMap.put(clazz.getName(), new Pair<Class<? extends BaseTab>, String>(
                    (Class<? extends BaseTab>) clazz, i18n.get(clazz.getAnnotation(SingletonTab.class).value()))));

        classSet.stream()
            .filter(clazz -> clazz.isAnnotationPresent(EntityName.class))
            .forEach(clazz ->
                entityMap.put(clazz.getName(),
                    new Pair<>(clazz, i18n.get(clazz.getAnnotation(EntityName.class).value()))));

        classSet.stream()
            .filter(clazz -> clazz.isAnnotationPresent(EntityTab.class))
            .forEach(clazz -> {
                Class<?> key = entityMap.get(clazz.getAnnotation(EntityTab.class).value().getName()).getKey();
                entityTabMap.put(clazz.getName(), new Pair<>(
                    (Class<? extends BaseTab>) clazz, i18n.get(key.getAnnotation(EntityName.class).value())));
            });

    }

    private String getEntityLog(AbstractLog item) {
        return String.format(
            i18n.get(((LogEntity) item).getAction().getName()),
            entityMap.get(((LogEntity) item).gettClass()).getValue());
    }

    private String getActionTabLog(AbstractLog item) {
        return String.format(
            logMap.get(item.getClass()),
            singletonTabMap.get(((SingletonTabLog) item).gettClass()).getValue());
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.logs");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.YELLOW;
    }
}

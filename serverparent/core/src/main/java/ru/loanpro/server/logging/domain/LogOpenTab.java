package ru.loanpro.server.logging.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ru.loanpro.server.logging.annotation.LogName;

/**
 * Лог открытия вкладки.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("TAB_OPEN")
@LogName("log.openTab")
public class LogOpenTab extends SingletonTabLog {
}

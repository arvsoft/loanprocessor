package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Document;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.directory.domain.DocumentType;
import ru.loanpro.directory.service.DocumentTypeService;
import ru.loanpro.global.directory.DocumentStatus;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.component.DirectoryComboBox;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования документов физического лица.
 *
 * @author Oleg Brizhevatikh
 */
public class DocumentDetailsLayout extends HorizontalLayout {
    private final Set<Document> documents;
    private final I18N i18n;
    private final DocumentTypeService documentTypeService;
    private final ConfigurationService configurationService;
    private final StorageService storageService;
    private final User user;
    private final ChronometerService chronometerService;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Document> grid;
    private ContentChangeListener listener;

    public DocumentDetailsLayout(User user, ChronometerService chronometerService, Set<Document> documents, I18N i18n,
                                 DocumentTypeService documentTypeService, ConfigurationService configurationService,
                                 StorageService storageService) {
        DateTimeFormatter dateFormatter =
            DateTimeFormatter.ofPattern(configurationService.getDateFormat(), configurationService.getLocale());

        this.user = user;
        this.chronometerService = chronometerService;
        this.i18n = i18n;
        this.documents = documents;
        this.documentTypeService = documentTypeService;
        this.configurationService = configurationService;
        this.storageService = storageService;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(documents);
        grid.addColumn(item -> i18n.get(item.getStatus().getName())).setCaption(i18n.get("client.documentdetails.field.status"));
        grid.addColumn(item -> item.getDocumentType().getName()).setCaption(i18n.get("client.documentdetails.field.documenttype"));
        grid.addColumn(Document::getSeries).setCaption(i18n.get("client.documentdetails.field.series"));
        grid.addColumn(Document::getNumber).setCaption(i18n.get("client.documentdetails.field.number"));
        grid.addColumn(item -> item.getIssueDate() == null ? "" :
            dateFormatter.format(item.getIssueDate())).setCaption(i18n.get("client.documentdetails.field.issue_date"));
        grid.addColumn(Document::getIssuePlace).setCaption(i18n.get("client.documentdetails.field.issue_place"));

        grid.addItemClickListener(event -> {
            closeForm();

            Document item = event.getItem();
            if (item != null) {
                form = getDocumentForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.documentdetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Document document = new Document();
            document.setStatus(DocumentStatus.ACTUAL);
            document.setScans(new HashSet<>());
            form = getDocumentForm(document);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getDocumentForm(Document document) {

        ComboBox<DocumentStatus> status = new ComboBox<>(i18n.get("client.documentdetails.field.status"));
        status.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        status.setEmptySelectionAllowed(false);
        status.setItems(DocumentStatus.values());
        status.setSelectedItem(document.getStatus());
        DirectoryComboBox<DocumentType, DocumentTypeService> documentType =
            new DirectoryComboBox<>(i18n.get("client.documentdetails.field.documenttype"), documentTypeService);
        documentType.setEmptySelectionAllowed(false);
        TextField series = new TextField(i18n.get("client.documentdetails.field.series"));
        TextField number = new TextField(i18n.get("client.documentdetails.field.number"));
        DateField issueDate = new DateField(i18n.get("client.documentdetails.field.issue_date"));
        issueDate.setDateFormat(configurationService.getDateFormat());
        TextArea place = new TextArea(i18n.get("client.documentdetails.field.issue_place"));
        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        Button save = new Button(i18n.get("client.documentdetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.documentdetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(status, documentType, series, number, issueDate, place, scansUpload, layout);
        formLayout.setSizeFull();

        Binder<Document> binder = new Binder<>();

        binder.forField(status)
            .asRequired()
            .bind(Document::getStatus, Document::setStatus);

        binder.forField(documentType)
            .asRequired()
            .bind(Document::getDocumentType, Document::setDocumentType);

        binder.forField(series)
            .bind(Document::getSeries, Document::setSeries);

        binder.forField(number)
            .asRequired()
            .bind(Document::getNumber, Document::setNumber);

        binder.forField(issueDate)
            .bind(Document::getIssueDate, Document::setIssueDate);

        binder.forField(place)
            .bind(Document::getIssuePlace, Document::setIssuePlace);

        binder.forField(scansUpload)
            .bind(Document::getScans, Document::setScans);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(document);

        save.addClickListener(event -> {
            if (documents.stream().noneMatch(item -> item.hashCode() == document.hashCode()))
                documents.add(document);

            grid.setItems(documents);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}
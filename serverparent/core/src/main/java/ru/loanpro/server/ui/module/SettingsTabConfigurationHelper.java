package ru.loanpro.server.ui.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.Settings;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.security.settings.exceptions.SettingsUnsupportedTypeException;
import ru.loanpro.security.settings.service.SettingsService;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.service.SequenceService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Хэлпер для вкладки настроек. Эмулирует отображение настроек системы в виде сущности
 * для биндера. Позволяет осуществлять проверку изменений в настроек сравнением хэш-кодов.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
public class SettingsTabConfigurationHelper {

    private final SettingsService settingsService;

    private UiTheme theme;
    private ZoneId timeZone;
    private SystemLocale locale;
    private String dateFormat;
    private String customDateFormat;
    private Set<Currency> currencySet;
    private Sequence accountDepartmentSequence;

    @Autowired
    public SettingsTabConfigurationHelper(SettingsService settingsService, SequenceService sequenceService) {
        this.settingsService = settingsService;

        theme = settingsService.get(UiTheme.class, Settings.THEME, UiTheme.LIGHT);
        timeZone = ZoneId.of(settingsService.get(String.class, Settings.TIME_ZONE, "UCT"));
        locale = settingsService.get(SystemLocale.class, Settings.LOCALE, SystemLocale.ENGLISH);

        dateFormat = settingsService.get(String.class, Settings.DATE_FORMAT, getDateFormats().get(0));
        customDateFormat = settingsService.get(String.class, Settings.DATE_FORMAT, "yyyy-MM-dd");

        currencySet = Arrays.stream(
            settingsService.get(String.class, Settings.CURRENCIES, "RUB,USD,EUR").split(","))
            .map(Currency::getInstance)
            .collect(Collectors.toSet());

        accountDepartmentSequence = sequenceService.getOneFull(UUID.fromString(settingsService.get(String.class, Settings.DEPARTMENT_ACCOUNT,
            sequenceService.findAll().get(0).getId().toString())));
    }

    public void save() {
        try {
            settingsService.set(Settings.THEME, theme);
            settingsService.set(Settings.TIME_ZONE, timeZone.getId());
            settingsService.set(Settings.LOCALE, locale);
            settingsService.set(Settings.CURRENCIES, currencySet.stream()
                .map(Currency::getCurrencyCode)
                .collect(Collectors.joining(",")));

            if (dateFormat == null)
                settingsService.set(Settings.DATE_FORMAT, customDateFormat);
            else
                settingsService.set(Settings.DATE_FORMAT, dateFormat);

            settingsService.set(Settings.DEPARTMENT_ACCOUNT, accountDepartmentSequence.getId().toString());
        } catch (SettingsUnsupportedTypeException e) {
            e.printStackTrace();
        }
    }

    public UiTheme getTheme() {
        return theme;
    }

    public void setTheme(UiTheme theme) {
        this.theme = theme;
    }

    public ZoneId getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(ZoneId timeZone) {
        this.timeZone = timeZone;
    }

    public SystemLocale getLocale() {
        return locale;
    }

    public void setLocale(SystemLocale locale) {
        this.locale = locale;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getCustomDateFormat() {
        return customDateFormat;
    }

    public void setCustomDateFormat(String customDateFormat) {
        this.customDateFormat = customDateFormat;
    }

    public Set<Currency> getCurrencySet() {
        return currencySet;
    }

    public void setCurrencySet(Set<Currency> currencySet) {
        this.currencySet = currencySet;
    }

    public List<String> getDateFormats() {
        return Stream.of(DateFormat.FULL, DateFormat.LONG, DateFormat.MEDIUM, DateFormat.SHORT)
            .map(item -> ((SimpleDateFormat) DateFormat.getDateInstance(item, locale.getLocale())).toPattern())
            .collect(Collectors.toList());
    }

    public Sequence getAccountDepartmentSequence() {
        return accountDepartmentSequence;
    }

    public void setAccountDepartmentSequence(Sequence accountDepartmentSequence) {
        this.accountDepartmentSequence = accountDepartmentSequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SettingsTabConfigurationHelper that = (SettingsTabConfigurationHelper) o;
        return Objects.equals(settingsService, that.settingsService) &&
            theme == that.theme &&
            Objects.equals(timeZone, that.timeZone) &&
            locale == that.locale &&
            Objects.equals(dateFormat, that.dateFormat) &&
            Objects.equals(customDateFormat, that.customDateFormat) &&
            Objects.equals(currencySet, that.currencySet) &&
            Objects.equals(accountDepartmentSequence, that.accountDepartmentSequence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(settingsService, theme, timeZone, locale, dateFormat, customDateFormat, currencySet, accountDepartmentSequence);
    }
}
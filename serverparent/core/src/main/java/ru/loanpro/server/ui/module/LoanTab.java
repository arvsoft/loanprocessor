package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.MenuItem;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import fr.opensagres.xdocreport.core.XDocReportException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.repository.LoanRepository;
import ru.loanpro.base.loan.service.LoanProductService;
import ru.loanpro.base.payment.PaymentCalculator;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.base.requesthistory.serivce.RequestHistoryService;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.base.storage.domain.PrintTemplateFile;
import ru.loanpro.base.storage.domain.PrintedFile;
import ru.loanpro.base.storage.service.PrintTemplateFileService;
import ru.loanpro.base.storage.service.PrintedFileService;
import ru.loanpro.directory.service.LoanTypeService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.Enable;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.security.event.UpdateDepartmentLoanSequenceEvent;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.server.ui.module.additional.ConfirmDialog;
import ru.loanpro.server.ui.module.additional.InstantLocalDateConverter;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.event.UpdatePaymentEvent;
import ru.loanpro.uibasis.event.AddTabEvent;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Вкладка работы с займом
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(Loan.class)
public class LoanTab extends BaseLoanTab<Loan, LoanRepository> {

    @Autowired
    LoanTypeService loanTypeService;

    @Autowired
    LoanProductService loanProductService;

    @Autowired
    RequestHistoryService requestHistoryService;

    @Autowired
    private PrintTemplateFileService printTemplateFileService;

    @Autowired
    PrintedFileService printedFileService;

    @Visible(Roles.Loan.PRINT)
    protected Button print;

    protected Button prolongation;
    protected Label prolongInfo;
    private VerticalLayout prolongationLayout;

    @Enable(Roles.Loan.CHANGE_NUMBER)
    private TextField loanNumber;
    private TextField status;

    private boolean isLoanClosed;
    private boolean isPaymentsExists;
    private boolean isLoanProlonged;//есть ли пролонгации по займу

    private int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    private int scaleValue = 2;

    public LoanTab(Loan loan) {
        entity = (loan == null) ? new Loan() : loan;
    }

    @PostConstruct
    @Override
    public void init() {
        super.init();
        applicationEventBus.register(this);

        isLoanClosed = Stream.of(
            LoanStatus.CLOSED_ANNULED,
            LoanStatus.CLOSED_FROZEN,
            LoanStatus.CLOSED_NO_OVERDUE,
            LoanStatus.CLOSED_WERE_OVERDUES
        ).anyMatch(item -> item == entity.getLoanStatus());

        isPaymentsExists = entity.getPayments().size() > 0;
        isLoanProlonged = entity.getProlongOrdNo() > 0;
    }

    @Override
    protected Layout initBaseInformation() {

        Label mainInfo = new Label(i18n.get("loan.panel.maininfo.title"));

        print = new Button(i18n.get("loan.button.print"), VaadinIcons.PRINT);
        print.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        print.addClickListener(event -> {
            ContextMenu contextMenu = new ContextMenu(print, true);

            Map<PrintDocumentType, List<PrintTemplateFile>> templates = printTemplateFileService.getActiveTemplates(
                PrintDocumentType.LOAN_AGREEMENT,
                PrintDocumentType.PHYSICAL_CLIENT_PROFILE);
            // Добавляем шаблоны договоров
            addContextMenuItems(contextMenu, templates.get(PrintDocumentType.LOAN_AGREEMENT),
                entity, printedFileService.findLoanPrintedFiles(entity));

            // Добавляем шаблоны анкеты клиента
            addContextMenuItems(contextMenu, templates.get(PrintDocumentType.PHYSICAL_CLIENT_PROFILE),
                entity.getBorrower(), printedFileService.findClientPrintedFiles(entity.getBorrower()));

            contextMenu.open(event.getClientX(), event.getClientY());
        });

        delete = new Button(i18n.get("loan.button.delete"), VaadinIcons.TRASH);
        delete.setEnabled(baseLoanService.canBeDeleted(entity));
        delete.addStyleNames(ValoTheme.BUTTON_BORDERLESS_COLORED);
        delete.addClickListener(event -> {
            ConfirmDialog dialog = new ConfirmDialog(
                i18n.get("loan.dialogDelete.caption"),
                i18n.get(entity.getProlongOrdNo()!=0 ?
                    "loan.dialogDeleteProlongation.description" : "loan.dialogDelete.description"),
                () -> {
                    try {
                        baseLoanService.delete(entity);
                        Notification.show(i18n.get("loan.notification.deleteSuccessful"), Type.TRAY_NOTIFICATION);
                        sessionEventBus.post(new RemoveTabEvent<>(this));
                    } catch (EntityDeleteException e) {
                        Notification.show(i18n.get("loan.notification.deletedError"),
                            i18n.get(e.getMessage()), Type.ERROR_MESSAGE);
                    }
                });
            dialog.setConfirmButtonIcon(VaadinIcons.TRASH);
            dialog.setClosable(true);
            dialog.show();
        });

        prolongation = new Button(i18n.get("loan.button.prolongation"), VaadinIcons.CLOCK);
        prolongation.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        prolongation.setEnabled(securityService.hasAuthority(Roles.Loan.PROLONGATION));
        prolongation.addClickListener(event -> {
            Loan loan = new Loan(entity);
            loan.setParent(entity);

            List<Schedule> scheduleList = new ArrayList<>();
            loan.setSchedules(scheduleList);
            List<Payment> paymentList = new ArrayList<>(entity.getPayments());
            paymentList.forEach(item -> {
                item.setId(null);
                item.setLoan(loan);
            });
            loan.setPayments(paymentList);

            boolean isLoanOverdue = Stream.of(
                LoanStatus.OPENED_CURRENT_OVERDUE,
                LoanStatus.OPENED_WERE_OVERDUES
            ).anyMatch(item -> item == entity.getLoanStatus());
            loan.setLoanStatus(isLoanOverdue ? LoanStatus.OPENED_WERE_OVERDUES : LoanStatus.OPENED_NO_OVERDUE);

            loan.setIsProlonged(false);
            loan.setProlongOrdNo(entity.getProlongOrdNo() + 1);
            loan.setProlongDepartment(securityService.getDepartment());
            loan.setProlongDate(chronometerService.getCurrentTime().toLocalDate());
            loan.setRecalculationType(configurationService.getRecalculationType());
            Payment payment = calculatePayment(entity);
            BigDecimal calc = payment.getInterest().add(payment.getPenalty().add(payment.getFine()));
            loan.setProlongCalculation(calc);
            loan.setProlongValue(entity.getCurrentBalance().add(calc));
            sessionEventBus.post(new AddTabEvent(LoanProlongationTab.class, entity, loan, prolongedLoans));
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        HorizontalLayout hlButtonAll = getActionButtonsLayout(delete, prolongation, print);
        edit.setVisible(securityService.hasAuthority(Roles.Loan.EDIT));
        delete.setVisible(securityService.hasAuthority(Roles.Loan.DELETE));
        save.addClickListener(event -> delete.setEnabled(baseLoanService.canBeDeleted(entity)));

        HorizontalLayout hlActions = new HorizontalLayout(mainInfo, hlButtonAll);
        hlActions.setWidth(100, Unit.PERCENTAGE);
        hlActions.setExpandRatio(hlButtonAll, 1.0f);

        Label currentDepartment = new Label(String.format(
            i18n.get("loan.label.department"), entity.getDepartment().getName()));

        status = new TextField(i18n.get("loan.textfield.status"));
        status.setValue(i18n.get(entity.getLoanStatus().getName()));
        status.setEnabled(false);

        loanNumber = new TextField(i18n.get("loan.textfield.loannumber"));
        loanNumber.setWidth(130, Unit.PIXELS);
        loanNumber.setStyleName("v-align-right");

        DateField issueDate = new DateField(i18n.get("loan.datefield.issuedate"));
        issueDate.setWidth(200, Unit.PIXELS);
        issueDate.setDateFormat(configurationService.getDateFormat());

        issueDate.setValue(chronometerService.getCurrentTime().toLocalDate());
        issueDate.setReadOnly(false);

        HorizontalLayout statusLayout = new HorizontalLayout(status, loanNumber, issueDate);

        TextField prolongOrdNo = new TextField(i18n.get("loan.prolong.field.prolongOrdNo"));
        prolongOrdNo.setWidth(80, Unit.PIXELS);
        prolongOrdNo.setReadOnly(true);

        DateField prolongDate = new DateField(i18n.get("loan.prolong.field.prolongDate"));
        prolongDate.setWidth(200, Unit.PIXELS);
        prolongDate.setDateFormat(configurationService.getDateFormat());
        prolongDate.setReadOnly(true);

        TextField prolongAgrNumber = new TextField(i18n.get("loan.prolong.field.prolongAgrNumber"));
        prolongAgrNumber.setWidth(200, Unit.PIXELS);
        prolongAgrNumber.setStyleName("v-align-right");
        prolongAgrNumber.setReadOnly(true);

        MoneyField tfProlongValue = new MoneyField(i18n.get("loan.prolong.field.prolongValue"),
            configurationService.getLocale());
        tfProlongValue.addStyleName(MaterialTheme.LABEL_COLORED);
        tfProlongValue.setWidth(200, Unit.PIXELS);
        tfProlongValue.addStyleName("align-right");
        tfProlongValue.setVisible(!isInProlongationProcess);
        tfProlongValue.setReadOnly(true);

        HorizontalLayout prolongActionLayout = new HorizontalLayout(
            prolongOrdNo, prolongDate, prolongAgrNumber, tfProlongValue);

        prolongInfo = new Label(i18n.get("loan.prolong.field.prolongPastInfo"));

        Label prolongDepartment = new Label(String.format(
            i18n.get("loan.label.department"),
            entity.getProlongDepartment() == null ? "" : entity.getProlongDepartment().getName()));

        prolongationLayout = new VerticalLayout(prolongInfo, prolongDepartment, prolongActionLayout);
        prolongationLayout.setVisible(entity.getProlongOrdNo() > 0 || isInProlongationProcess);
        prolongActionLayout.setMargin(false);

        VerticalLayout layout = new VerticalLayout(hlActions, currentDepartment, statusLayout, prolongationLayout);
        layout.setSizeFull();

        binder.bind(loanNumber, Loan::getLoanNumber, Loan::setLoanNumber);

        binder.forField(issueDate)
            .withConverter(new InstantLocalDateConverter(chronometerService.getCurrentZoneId()))
            .bind(Loan::getLoanIssueDate, Loan::setLoanIssueDate);

        //биндим данные о пролонгации
        binder.forField(prolongOrdNo)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .bind(Loan::getProlongOrdNo, Loan::setProlongOrdNo);

        binder.forField(prolongDate)
            .withNullRepresentation(LocalDate.now())
            .bind(Loan::getProlongDate, Loan::setProlongDate);

        binder.bind(prolongAgrNumber, Loan::getProlongAgrNumber, Loan::setProlongAgrNumber);

        binder.forField(tfProlongValue)
            .withNullRepresentation(new BigDecimal(0).setScale(scaleValue, scaleMode).toString())
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(Loan::getProlongValue, Loan::setProlongValue);

        return layout;
    }

    private <T extends AbstractIdEntity, P extends PrintedFile> void addContextMenuItems(
        ContextMenu contextMenu, List<PrintTemplateFile> templateList, T object, List<P> printedFiles) {
        if (templateList.size() == 0)
            return;

        String templateName = templateList.get(0).getTemplateType().getTemplateName();

        MenuItem menuItem = contextMenu.addItem(i18n.get(templateName), null);
        templateList.forEach(template ->
            menuItem.addItem(template.getDisplayName(), event -> addTemplateItem(template, object)));

        menuItem.addSeparator();

        printedFiles.forEach(printedFile ->
            menuItem.addItem(String.format("%s - %s", printedFile.getDisplayName(),
                chronometerService.zonedDateTimeToString(printedFile.getDateTime())),
                event ->
                    getUI().getPage().open(String.format("/print/%s", printedFile.getFileName()), "_blank")));
    }

    private void addTemplateItem(PrintTemplateFile template, Object printTarget) {
        try {
            String fileName = printService.printTemplate(template, printTarget);
            getUI().getPage().open(String.format("/print/%s", fileName), "_blank");
        } catch (IOException | XDocReportException exception) {
            showErrorMessage(exception);
        }
    }

    @Override
    protected void buttonsStatusUpdate() {
        super.buttonsStatusUpdate();

        save.setEnabled(scheduleForm.getScheduleHash() == entity.scheduleHashCode() && isScheduledCorrect &&
            binder.isValid() && (entity.hashCode() != entityHash || entity.getId() == null));
        edit.setEnabled(entity.getId() != null && entityIsReadOnly && !isLoanClosed
            && (!isLoanProlonged || !isPaymentsExists));
        print.setEnabled(entity.getId() != null);
        prolongation.setEnabled(entity.getId() != null && !isLoanClosed);
    }

    @Override
    public void destroy() {
        super.destroy();

        applicationEventBus.unregister(this);

        if (entity.getId() == null)
            numberService.clearLoanSequenceDraftNumber(entity.getLoanNumber());
    }

    @Override
    public String getTabName() {
        if (entity.getId() == null) {
            return i18n.get("loan.tab.add.caption");
        } else {
            return i18n.get("loan.tab.edit.caption") + entity.getLoanNumber();
        }
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIGHT_GREEN;
    }

    /**
     * Обработчик обновления последовательности номеров. Проверяет, если обновлённая последовательность
     * принадлежит текущему отделу, то запрашивает новый временный номер для займа.  Распространяется только
     * на создаваемые займы.
     *
     * @param event событие обновления последовательности.
     */
    @Subscribe
    private void updateSequenceEventHandler(UpdateSequenceEvent event) {
        if (entity.getId() != null)
            return;
        if (securityService.getDepartment().getLoanSequence().getId().equals(event.getSequence().getId())) {
            loanNumber.setValue(numberService.getLoanSequenceDraftNumber());
            binder.setBean(binder.getBean());
        }
    }

    /**
     * Обработчик изменения последовательности у отдела. Проверяет, если отдел совпадает с текущим отделом,
     * то запрашивает новый временный номер для займа. Распространяется только на создаваемые займы.
     *
     * @param event событие изменения последовательности у отдела.
     */
    @Subscribe
    private void updateDepartmentLoanSequenceEventHandler(UpdateDepartmentLoanSequenceEvent event) {
        if (entity.getId() != null)
            return;
        if (securityService.getDepartment().getId().equals(event.getDepartment().getId())) {
            loanNumber.setValue(numberService.getLoanSequenceDraftNumber());
            binder.setBean(binder.getBean());
        }
    }

    /**
     * Обработчик сохранения платежа по займу. Если окно открыто,
     * то обновляет список фактически рассчитанных платежей по займу
     *
     * @param event событие сохранения платежа по займу
     */
    @Subscribe
    private void updatePaymentEventHandler(UpdatePaymentEvent event) {
        if (event.getObject().getClass() == Loan.class) {
            if (((Loan) event.getObject()).getId().equals(entity.getId())) {
                entity = baseLoanService.getOneFull(entity.getId());
                entityHash = entity.hashCode();

                status.setValue(i18n.get(entity.getLoanStatus().getName()));
                scheduleForm.getGridSchedule().setItems(entity.getSchedules());
                paymentsForm.setItems(entity.getPayments());

                isLoanClosed = Stream.of(
                    LoanStatus.CLOSED_ANNULED,
                    LoanStatus.CLOSED_FROZEN,
                    LoanStatus.CLOSED_NO_OVERDUE,
                    LoanStatus.CLOSED_WERE_OVERDUES
                ).anyMatch(item -> item == entity.getLoanStatus());
                buttonsStatusUpdate();
                binder.setBean(binder.getBean());
            }
        }
    }

    /**
     * Вычисляет автоматический платеж по умолчанию исходя из свойств займа, графиков платежей и текущей даты.
     *
     * @return - новый автоматический платеж
     */
    private Payment calculatePayment(Loan loan) {
        PaymentCalculator paymentCalculator = PaymentCalculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setLoanIssueDate(loan.getProlongOrdNo() == 0 ?
                LocalDateTime.ofInstant(loan.getLoanIssueDate(), chronometerService.getCurrentZoneId()).toLocalDate()
                : loan.getProlongDate())
            .setLoanBalance(loan.getLoanBalance())
            .setReplaceDate(loan.isReplaceDate())
            .setReplaceDateValue(loan.getReplaceDateValue())
            .setReplaceOverMonth(loan.isReplaceOverMonth())
            .setPrincipalLastPay(loan.isPrincipalLastPay())
            .setInterestBalance(loan.isInterestBalance())
            .setNoInterestFD(loan.isNoInterestFD())
            .setNoInterestFDValue(loan.getNoInterestFDValue())
            .setIncludeIssueDate(loan.isIncludeIssueDate())
            .setDaysInYear(365)
            .setCalcScheme(loan.getCalcScheme())
            .setReturnTerm(loan.getReturnTerm())
            .setInterestRate(loan.getInterestValue())
            .setInterestRateType(loan.getInterestType())
            .setLoanTerm(loan.getLoanTerm())
            .setReplaceDate(loan.isReplaceDate())
            .setReplaceDateValue(loan.getReplaceDateValue())
            .setReplaceOverMonth(loan.isReplaceOverMonth())
            .setSummationFines(configurationService.getSummationFine())
            .setSummationPenalties(configurationService.getSummationPenalty())
            .setOverdueDaysCalculationType(configurationService.getOverdueDaysCalculationType())
            .setCalculationDate(chronometerService.getCurrentTime().toLocalDate())
            .setPenaltyType(loan.getPenaltyType())
            .setPenaltyValue(loan.getPenaltyValue())
            .setFine(loan.isChargeFine())
            .setFineValue(loan.getFineValue())
            .setFineOneTime(loan.isFineOneTime())
            .setInterestStrictly(loan.isInterestStrictly())
            .setInterestOverdue(loan.isInterestOverdue())
            .build();
        paymentCalculator.setSchedules(loan.getSchedules());
        paymentCalculator.setPayments(loan.getPayments());

        return paymentCalculator.calculate();
    }

}

package ru.loanpro.server.ui.module.client.details;

/**
 * Интерфейс, выполняющий роль колбека для передачи события изменения контента из элементов
 * интерфейса во вкладки физического и юридического лица.
 *
 * @author Oleg Brizhevatikh
 */
public interface ContentChangeListener {
    /**
     * Индикатор изменения контента.
     */
    void contentChanged();
}

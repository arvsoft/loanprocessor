package ru.loanpro.server.ui.module.report.reports;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.base.report.domain.reports.ClientReport1;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.global.directory.Gender;
import ru.loanpro.server.ui.module.report.BaseReportTab;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Окно отчета по клиентам.
 * Отчет о распределении статусов клиентов.
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
public class ClientReport1Tab extends BaseReportTab {

    private Grid<ClientReport1> grid;
    private CheckBox cbxNotShowEmptyGroups;
    private Button prepare;

    private List<ClientReport1> clientReport1List;

    public ClientReport1Tab(Report report) {
        super(report);
    }

    @PostConstruct
    @Override
    public void init() {
        super.init();
    }

    @Override
    protected String getLabelDateDescription() {
        return i18n.get("reports.loan.report1.field.dateDescription");
    }

    @Override
    protected boolean getVisibleBaseSearch() {
        return false;
    }

    @Override
    protected Layout initReportInformation() {
        cbxNotShowEmptyGroups = new CheckBox(i18n.get("reports.loan.report2.checkbox.notShowEmptyGroups"));
        cbxNotShowEmptyGroups.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxNotShowEmptyGroups.setValue(true);

        grid = new Grid<>();
        grid.setSizeFull();
        gridColumnBuild();

        VerticalLayout layout = new VerticalLayout(cbxNotShowEmptyGroups, grid);
        layout.setSizeFull();
        layout.addStyleName(MaterialTheme.CARD_2);
        layout.setMargin(false);
        layout.setSpacing(true);

        layout.setExpandRatio(grid, 1);
        layout.setSizeFull();

        return layout;
    }

    protected Layout getActionLayout(Report report) {
        Label mainInfo = new Label(i18n.get(report.getName()));

        prepare = new Button(i18n.get("reporttab.button.prepare"), VaadinIcons.LINE_BAR_CHART);
        prepare.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        HorizontalLayout actionLayout = new HorizontalLayout(mainInfo, prepare);
        actionLayout.setWidth(100, Unit.PERCENTAGE);
        actionLayout.setExpandRatio(mainInfo, 1);

        prepare.addClickListener(e -> {

            clientReport1List = new ArrayList<>();
            LocalDate currentDate = chronometerService.getCurrentTime().toLocalDate();

            for (ClientRating rating : ClientRating.values()) {
                int count = reportService.countFindAllClientsByRating(rating);

                int pageSize = 100;
                int pageCount = 1;
                if (pageSize < count) {
                    int temp = (int) (Math.floor((double) count / pageSize));
                    pageCount = count % pageSize == 0 ? temp : temp + 1;
                }

                int number = 0;
                int maleNumber = 0;
                int femaleNumber = 0;
                int otherNumber = 0;
                int number25 = 0;
                int number26_35 = 0;
                int number36_50 = 0;
                int number51 = 0;

                ClientReport1 clientReport1 = new ClientReport1();
                clientReport1.setRating(rating);

                for (int i=0;i<pageCount;i++){
                    List<PhysicalClient> clientList = getClients(rating, PageRequest.of(i, pageSize, Sort.Direction.ASC, "id"));

                    number += clientList.size();
                    for (PhysicalClient client : clientList) {
                        if (client.getGender() == Gender.MALE) maleNumber++;
                        if (client.getGender() == Gender.FEMALE) femaleNumber++;
                        if (client.getGender() == Gender.OTHER) otherNumber++;
                        long age = ChronoUnit.YEARS.between(client.getBirthDate(), currentDate);
                        if (age < 26) number25++;
                        if (age >= 26 && age < 36) number26_35++;
                        if (age >= 36 && age < 51) number36_50++;
                        if (age >= 51) number51++;
                    }

                    clientReport1.setNumber(number);
                    clientReport1.setMaleNumber(maleNumber);
                    clientReport1.setFemaleNumber(femaleNumber);
                    clientReport1.setOtherNumber(otherNumber);
                    clientReport1.setNumber25(number25);
                    clientReport1.setNumber26_35(number26_35);
                    clientReport1.setNumber36_50(number36_50);
                    clientReport1.setNumber51(number51);
                }

                if (cbxNotShowEmptyGroups.getValue()) {
                    if (clientReport1.getNumber() > 0) {
                        clientReport1List.add(clientReport1);
                    }
                } else clientReport1List.add(clientReport1);
            }

            grid.setItems(clientReport1List);
        });

        return actionLayout;
    }

    private List<PhysicalClient> getClients(ClientRating rating, PageRequest pageRequest) {
        return reportService.findAllClientsByRating(rating, pageRequest);
    }

    private void gridColumnBuild() {
        grid.addColumn(item -> item.getRating() == null ? "" : i18n.get(item.getRating().getName()))
            .setCaption(i18n.get("report.clientReport1.grid.title.clientRating"));

        grid.addColumn(ClientReport1::getNumber)
            .setCaption(i18n.get("report.clientReport1.grid.title.clientNumber"));

        grid.addColumn(ClientReport1::getMaleNumber)
            .setCaption(i18n.get("report.clientReport1.grid.title.maleNumber"));

        grid.addColumn(ClientReport1::getFemaleNumber)
            .setCaption(i18n.get("report.clientReport1.grid.title.femaleNumber"));

        grid.addColumn(ClientReport1::getOtherNumber)
            .setCaption(i18n.get("report.clientReport1.grid.title.otherNumber"));

        grid.addColumn(ClientReport1::getNumber25)
            .setCaption(i18n.get("report.clientReport1.grid.title.number25"));

        grid.addColumn(ClientReport1::getNumber26_35)
            .setCaption(i18n.get("report.clientReport1.grid.title.number26_35"));

        grid.addColumn(ClientReport1::getNumber36_50)
            .setCaption(i18n.get("report.clientReport1.grid.title.number36_50"));

        grid.addColumn(ClientReport1::getNumber51)
            .setCaption(i18n.get("report.clientReport1.grid.title.number51"));
    }

    @Override
    public String getTabName() {
        return i18n.get(report.getName());
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIME;
    }
}

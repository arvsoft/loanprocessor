package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileDownloader;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.GridContextClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.base.storage.IStorageService;
import ru.loanpro.base.storage.domain.PrintTemplateFile;
import ru.loanpro.base.storage.service.PrintTemplateFileService;
import ru.loanpro.global.Settings;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.settings.utils.CurrencyComparator;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.service.SequenceService;
import ru.loanpro.server.ui.module.component.BindedGrid;
import ru.loanpro.server.ui.module.component.uploader.PrintTemplateUploadLayout;
import ru.loanpro.server.ui.module.event.UpdateDepartmentEvent;
import ru.loanpro.server.ui.module.event.UpdateSequenceEvent;
import ru.loanpro.server.ui.module.util.DefaultCallback;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.AddTabEvent;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.time.DateTimeException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Вкладка настроек.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.settings")
public class SettingsTab extends BaseTab {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private PrintTemplateFileService printTemplateFileService;

    @Autowired
    private IStorageService storageService;

    @Autowired
    private SettingsTabConfigurationHelper tabConfigurationHelper;

    @Autowired
    private SettingsTabInformationHelper tabInformationHelper;

    private ComboBox<Sequence> accountSequence;
    private TreeGrid<Department> treeGrid;
    private Grid<Sequence> sequenceGrid;
    private ComboBox<String> dateFormat;
    private TextField customDateFormat;
    private VerticalLayout templateEditLayout;

    private int informationHashCode;
    private int configurationHashCode;

    @PostConstruct
    public void init() {
        setSpacing(true);
        setMargin(false);
        setSizeFull();

        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleNames(ValoTheme.TABSHEET_FRAMED, ValoTheme.TABSHEET_PADDED_TABBAR);
        tabSheet.setWidth(100, Unit.PERCENTAGE);
        tabSheet.setHeight(100, Unit.PERCENTAGE);
        //tabSheet.setHeightUndefined();
        tabSheet.addTab(getBaseSettings(), i18n.get("settings.tab.systemSettings"));
        tabSheet.addTab(getBaseInformation(), i18n.get("settings.tab.baseInformation"));
        tabSheet.addTab(getDepartmentSettings(), i18n.get("settings.tab.departments"));
        tabSheet.addTab(getSequenceSettings(), i18n.get("settings.tab.sequenceSettings"));
        tabSheet.addTab(getPrintTemplateSettings(), i18n.get("settings.tab.printTemplates"));

        addComponent(tabSheet);
    }

    private Layout getBaseInformation() {
        informationHashCode = tabConfigurationHelper.hashCode();
        Binder<SettingsTabInformationHelper> binder = new Binder<>();

        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setSpacing(true);
        form.setWidth(800, Unit.PIXELS);
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);

        Label section = new Label(i18n.get("settings.baseInfo"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);
        Label gap = new Label();
        form.addComponent(gap);

        TextField name = new TextField(i18n.get("settings.baseInformation.name"));
        form.addComponent(name);

        TextField fullName = new TextField(i18n.get("settings.baseInformation.fullName"));
        form.addComponent(fullName);

        TextField regNumber1 = new TextField(i18n.get("settings.baseInformation.regNumber1"));
        form.addComponent(regNumber1);

        TextField regNumber2 = new TextField(i18n.get("settings.baseInformation.regNumber2"));
        form.addComponent(regNumber2);

        TextField regNumber3 = new TextField(i18n.get("settings.baseInformation.regNumber3"));
        form.addComponent(regNumber3);

        TextField regNumber4 = new TextField(i18n.get("settings.baseInformation.regNumber4"));
        form.addComponent(regNumber4);

        TextField regNumber5 = new TextField(i18n.get("settings.baseInformation.regNumber5"));
        form.addComponent(regNumber5);

        TextField regNumber6 = new TextField(i18n.get("settings.baseInformation.regNumber6"));
        form.addComponent(regNumber6);

        TextField phones = new TextField(i18n.get("settings.baseInformation.phones"));
        form.addComponent(phones);

        TextField juridicalAddress = new TextField(i18n.get("settings.baseInformation.juridicalAddress"));
        form.addComponent(juridicalAddress);

        TextField actualAddress = new TextField(i18n.get("settings.baseInformation.actualAddress"));
        form.addComponent(actualAddress);

        TextField email = new TextField(i18n.get("settings.baseInformation.email"));
        form.addComponent(email);

        TextField bankAccount1 = new TextField(i18n.get("settings.baseInformation.bankAccount1"));
        form.addComponent(bankAccount1);

        TextField bankAccount2 = new TextField(i18n.get("settings.baseInformation.bankAccount2"));
        form.addComponent(bankAccount2);

        TextField director = new TextField(i18n.get("settings.baseInformation.director"));
        form.addComponent(director);

        TextField directorName = new TextField(i18n.get("settings.baseInformation.directorName"));
        form.addComponent(directorName);

        TextField assistant1 = new TextField(i18n.get("settings.baseInformation.assistant1"));
        form.addComponent(assistant1);

        TextField assistantName1 = new TextField(i18n.get("settings.baseInformation.assistantName1"));
        form.addComponent(assistantName1);

        TextField assistant2 = new TextField(i18n.get("settings.baseInformation.assistant2"));
        form.addComponent(assistant2);

        TextField assistantName2 = new TextField(i18n.get("settings.baseInformation.assistantName2"));
        form.addComponent(assistantName2);

        form.addComponent(new Label());

        Button save = new Button(i18n.get("settings.save"));
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        save.setEnabled(false);
        Button saveAndClose = new Button(i18n.get("settings.saveAndClose"));
        saveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        saveAndClose.setEnabled(false);

        binder.bind(name,
            SettingsTabInformationHelper::getName, SettingsTabInformationHelper::setName);
        binder.bind(fullName,
            SettingsTabInformationHelper::getFullName, SettingsTabInformationHelper::setFullName);
        binder.bind(regNumber1,
            SettingsTabInformationHelper::getRegNumber1, SettingsTabInformationHelper::setRegNumber1);
        binder.bind(regNumber2,
            SettingsTabInformationHelper::getRegNumber2, SettingsTabInformationHelper::setRegNumber2);
        binder.bind(regNumber3,
            SettingsTabInformationHelper::getRegNumber3, SettingsTabInformationHelper::setRegNumber3);
        binder.bind(regNumber4,
            SettingsTabInformationHelper::getRegNumber4, SettingsTabInformationHelper::setRegNumber4);
        binder.bind(regNumber5,
            SettingsTabInformationHelper::getRegNumber5, SettingsTabInformationHelper::setRegNumber5);
        binder.bind(regNumber6,
            SettingsTabInformationHelper::getRegNumber6, SettingsTabInformationHelper::setRegNumber6);
        binder.bind(phones,
            SettingsTabInformationHelper::getPhones, SettingsTabInformationHelper::setPhones);
        binder.bind(juridicalAddress,
            SettingsTabInformationHelper::getJuridicalAddress, SettingsTabInformationHelper::setJuridicalAddress);
        binder.bind(actualAddress,
            SettingsTabInformationHelper::getActualAddress, SettingsTabInformationHelper::setActualAddress);
        binder.bind(email,
            SettingsTabInformationHelper::getEmail, SettingsTabInformationHelper::setEmail);
        binder.bind(bankAccount1,
            SettingsTabInformationHelper::getBankAccount1, SettingsTabInformationHelper::setBankAccount1);
        binder.bind(bankAccount2,
            SettingsTabInformationHelper::getBankAccount2, SettingsTabInformationHelper::setBankAccount2);
        binder.bind(director,
            SettingsTabInformationHelper::getDirector, SettingsTabInformationHelper::setDirector);
        binder.bind(directorName,
            SettingsTabInformationHelper::getDirectorName, SettingsTabInformationHelper::setDirectorName);
        binder.bind(assistant1,
            SettingsTabInformationHelper::getAssistant1, SettingsTabInformationHelper::setAssistant1);
        binder.bind(assistantName1,
            SettingsTabInformationHelper::getAssistantName1, SettingsTabInformationHelper::setAssistantName1);
        binder.bind(assistant2,
            SettingsTabInformationHelper::getAssistant2, SettingsTabInformationHelper::setAssistant2);
        binder.bind(assistantName2,
            SettingsTabInformationHelper::getAssistantName2, SettingsTabInformationHelper::setAssistantName2);

        binder.setBean(tabInformationHelper);

        save.addClickListener(event -> {
            tabInformationHelper.save();
            informationHashCode = tabInformationHelper.hashCode();
            save.setEnabled(false);
            saveAndClose.setEnabled(false);
            Notification.show(i18n.get("settings.saved"), Type.TRAY_NOTIFICATION);
        });

        saveAndClose.addClickListener(event -> {
            save.click();
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        binder.addValueChangeListener(event -> {
            save.setEnabled(tabInformationHelper.hashCode() != informationHashCode);
            saveAndClose.setEnabled(save.isEnabled());
        });

        HorizontalLayout hlButtons = new HorizontalLayout(save, saveAndClose);
        hlButtons.setSpacing(true);

        HorizontalLayout hlGap = new HorizontalLayout();
        hlGap.setHeight(150, Unit.PIXELS);

        VerticalLayout layout = new VerticalLayout(hlButtons, form, hlGap);
        layout.setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
        layout.setComponentAlignment(form, Alignment.TOP_CENTER);
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.setWidth(100, Unit.PERCENTAGE);

        return layout;
    }

    private Layout getBaseSettings() {
        configurationHashCode = tabConfigurationHelper.hashCode();
        Binder<SettingsTabConfigurationHelper> binder = new Binder<>();

        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setSpacing(true);
        form.setWidth(800, Unit.PIXELS);
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);

        Label section = new Label(i18n.get("settings.globalInfo"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);
        Label gap = new Label();
        form.addComponent(gap);

        ComboBox<UiTheme> theme = new ComboBox<>(i18n.get(Settings.THEME));
        theme.setItems(UiTheme.values());
        theme.setEmptySelectionAllowed(false);
        theme.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        theme.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(theme);

        ComboBox<ZoneId> timeZone = new ComboBox<>(i18n.get(Settings.TIME_ZONE));
        List<ZoneId> timeZoneItemList = chronometerService.getAllZoneIds();
        timeZone.setItems(timeZoneItemList);
        timeZone.setPageLength(timeZoneItemList.size());
        timeZone.setWidth(50, Unit.PERCENTAGE);
        timeZone.setEmptySelectionAllowed(false);
        timeZone.setItemCaptionGenerator(item -> String.format("%s %s",
            ZonedDateTime.now(item).getOffset().getId().replace("Z", "+00:00"), item));
        form.addComponent(timeZone);

        ComboBox<SystemLocale> locale = new ComboBox<>(i18n.get(Settings.LOCALE));
        locale.setItems(SystemLocale.values());
        locale.setEmptySelectionAllowed(false);
        locale.setWidth(50, Unit.PERCENTAGE);
        locale.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        form.addComponent(locale);

        dateFormat = new ComboBox<>(i18n.get(Settings.DATE_FORMAT));
        dateFormat.setItems(tabConfigurationHelper.getDateFormats());
        dateFormat.setWidth(50, Unit.PERCENTAGE);
        dateFormat.setEmptySelectionCaption(i18n.get("settings.dateFormat.customFormat"));
        dateFormat.setItemCaptionGenerator(item ->
            chronometerService.getCurrentTime().format(DateTimeFormatter.ofPattern(item, locale.getValue().getLocale())));
        form.addComponent(dateFormat);

        customDateFormat = new TextField();
        customDateFormat.setVisible(dateFormat.getValue() == null);
        customDateFormat.setWidth(50, Unit.PERCENTAGE);

        HorizontalLayout dateFormatLayout = initDateFormatFields(binder, locale);
        form.addComponent(dateFormatLayout);

        BindedGrid<Currency> currencyGrid = new BindedGrid<>(i18n.get("settings.currency.grid"));
        currencyGrid.addColumn(item -> String.format(
            "%s %s", item.getCurrencyCode(), item.getDisplayName(locale.getLocale())))
            .setCaption(i18n.get("settings.currency.grid.name"));
        currencyGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        currencyGrid.setItems(Currency.getAvailableCurrencies().stream().sorted(new CurrencyComparator()));
        form.addComponent(currencyGrid);

        Label currencyResult = new Label();
        currencyResult.setCaption(i18n.get("settings.currency.list"));
        form.addComponent(currencyResult);

        accountSequence = new ComboBox<>(i18n.get("settings.accountSequenceDepartment"));
        accountSequence.setItemCaptionGenerator(Sequence::getName);
        accountSequence.setEmptySelectionAllowed(false);
        accountSequence.setItems(sequenceService.findAll());
        form.addComponent(accountSequence);

        currencyGrid.addValueChangeListener(event -> {
            currencyResult.setValue(event.getValue().stream()
                .sorted(new CurrencyComparator(locale.getValue()))
                .map(Currency::getCurrencyCode)
                .collect(Collectors.joining(", ")));
        });

        locale.addValueChangeListener(event -> {
            currencyResult.setValue(tabConfigurationHelper.getCurrencySet().stream()
                .sorted(new CurrencyComparator(locale.getValue()))
                .map(Currency::getCurrencyCode)
                .collect(Collectors.joining(", ")));
        });

        binder.bind(theme, SettingsTabConfigurationHelper::getTheme, SettingsTabConfigurationHelper::setTheme);
        binder.bind(timeZone, SettingsTabConfigurationHelper::getTimeZone, SettingsTabConfigurationHelper::setTimeZone);
        binder.bind(locale, SettingsTabConfigurationHelper::getLocale, SettingsTabConfigurationHelper::setLocale);
        binder.bind(dateFormat, SettingsTabConfigurationHelper::getDateFormat, SettingsTabConfigurationHelper::setDateFormat);
        binder.forField(currencyGrid)
            .asRequired(i18n.get("settings.currency.list.error"))
            .bind(SettingsTabConfigurationHelper::getCurrencySet, SettingsTabConfigurationHelper::setCurrencySet);
        binder.bind(accountSequence, SettingsTabConfigurationHelper::getAccountDepartmentSequence, SettingsTabConfigurationHelper::setAccountDepartmentSequence);

        binder.setBean(tabConfigurationHelper);

        Button save = new Button(i18n.get("settings.save"));
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        Button saveAndClose = new Button(i18n.get("settings.saveAndClose"));
        saveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        save.addClickListener(event -> {
            tabConfigurationHelper.save();
            configurationHashCode = tabConfigurationHelper.hashCode();
            save.setEnabled(false);
            saveAndClose.setEnabled(false);
            Notification.show(i18n.get("settings.saved"), Type.TRAY_NOTIFICATION);
        });
        saveAndClose.addClickListener(event -> {
            save.click();
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        binder.addValueChangeListener(event -> {
            save.setEnabled(binder.isValid() && tabConfigurationHelper.hashCode() != configurationHashCode);
            saveAndClose.setEnabled(save.isEnabled());
        });

        HorizontalLayout hlButtons = new HorizontalLayout(save, saveAndClose);
        hlButtons.setSpacing(true);

        HorizontalLayout hlGap = new HorizontalLayout();
        hlGap.setHeight(150, Unit.PIXELS);

        VerticalLayout layout = new VerticalLayout(hlButtons, form, hlGap);
        layout.setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
        layout.setComponentAlignment(form, Alignment.TOP_CENTER);
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.setWidth(100, Unit.PERCENTAGE);

        return layout;
    }

    private Layout getDepartmentSettings() {
        Button create = new Button(i18n.get("baseloans.button.add"));
        create.setIcon(VaadinIcons.PLUS);
        create.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        create.addClickListener(event -> sessionEventBus.post(new AddTabEvent(DepartmentTab.class)));

        Button view = new Button(i18n.get("baseloans.button.view"));
        view.setIcon(VaadinIcons.EDIT);
        view.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        view.setEnabled(false);

        treeGrid = new TreeGrid<>();
        treeGrid.setSizeFull();
        List<Department> departments = departmentService.getRootDepartments();
        treeGrid.setItems(departments, Department::getChilds);
        treeGrid.expand(departments.stream().flatMap(Department::flattened).collect(Collectors.toList()));

        Grid.Column<Department, String> nameColumn = treeGrid.addColumn(Department::getName)
            .setCaption(i18n.get("settings.department.grid.column.name"));
        treeGrid.sort(nameColumn);
        nameColumn.setWidth(300);

        Grid.Column<Department, String> directorColumn = treeGrid.addColumn(item->
            departmentService.getDepartmentDirector(item).orElseGet(User::new).getUsername())
            .setCaption(i18n.get("settings.department.grid.column.director"));
        directorColumn.setWidth(200);

        Grid.Column<Department, String> departColumn = treeGrid.addColumn(Department::getAddress)
            .setCaption(i18n.get("settings.department.grid.column.address"));
        departColumn.setWidth(200);

        Grid.Column<Department, String> phoneColumn = treeGrid.addColumn(Department::getPhone)
            .setCaption(i18n.get("settings.department.grid.column.phone"));
        phoneColumn.setWidth(200);

        Grid.Column<Department, String> emailColumn = treeGrid.addColumn(Department::getEmail)
            .setCaption(i18n.get("settings.department.grid.column.email"));
        emailColumn.setWidth(200);

        treeGrid.addColumn(item -> item.getLoanSequence().getName())
            .setCaption(i18n.get("settings.department.grid.column.loanSequence"));

        treeGrid.addColumn(item -> item.getRequestSequence().getName())
            .setCaption(i18n.get("settings.department.grid.column.requestSequence"));

        treeGrid.addColumn(item -> item.getAccountSequence().getName())
            .setCaption(i18n.get("settings.department.grid.column.accountSequence"));

        treeGrid.addColumn(item -> item.getProlongSequence().getName())
            .setCaption(i18n.get("settings.department.grid.column.prolongSequence"));

        treeGrid.addContextClickListener(event -> {
            GridContextClickEvent<Department> contextEvent = (GridContextClickEvent<Department>) event;

            ContextMenu contextMenu = new ContextMenu(treeGrid, true);
            contextMenu.removeItems();
            contextMenu.addItem(i18n.get("settings.department.grid.context.remove"), e -> contextMenu.remove());

            Department department = contextEvent.getItem();
            if (department != null)
                contextMenu.addItem(i18n.get("settings.department.grid.context.edit"), e -> {
                    sessionEventBus.post(
                        new AddTabEvent(DepartmentTab.class, departmentService.findOneFull(department.getId())));
                    contextMenu.remove();
                });

            contextMenu.addItem(i18n.get("settings.department.grid.context.create"), e -> {
                sessionEventBus.post(new AddTabEvent(DepartmentTab.class));
                contextMenu.remove();
            });
            contextMenu.open(event.getClientX(), event.getClientY());
        });

        view.addClickListener(event -> {
            Department department = treeGrid.asSingleSelect().getValue();
            sessionEventBus.post(new AddTabEvent(DepartmentTab.class, department));
        });

        treeGrid.addSelectionListener(event -> view.setEnabled(event.getFirstSelectedItem().isPresent()));

        HorizontalLayout hlActions = new HorizontalLayout(create, view);

        VerticalLayout layout = new VerticalLayout(hlActions, treeGrid);
        layout.setComponentAlignment(hlActions, Alignment.TOP_RIGHT);
        layout.setExpandRatio(treeGrid, 1);
        layout.setSizeFull();
        layout.setMargin(true);
        layout.setSpacing(true);

        return layout;
    }

    private HorizontalLayout initDateFormatFields(Binder<SettingsTabConfigurationHelper> binder, ComboBox<SystemLocale> locale) {
        Label customDateFormatResult = new Label();
        customDateFormatResult.setVisible(tabConfigurationHelper.getDateFormat() == null);

        HorizontalLayout dateFormatLayout = new HorizontalLayout(customDateFormat, customDateFormatResult);
        dateFormatLayout.setVisible(tabConfigurationHelper.getDateFormat() == null);
        dateFormatLayout.setComponentAlignment(customDateFormatResult, Alignment.MIDDLE_CENTER);

        dateFormat.addValueChangeListener(event -> {
            boolean isCustomFormat = event.getValue() == null;

            customDateFormat.setVisible(isCustomFormat);
            customDateFormatResult.setVisible(isCustomFormat);
            dateFormatLayout.setVisible(isCustomFormat);

            if (isCustomFormat) {
                customDateFormat.focus();
                binder.forField(customDateFormat)
                    .withValidator((Validator<String>) (value, context) -> {
                        try {
                            customDateFormatResult.setValue(
                                chronometerService.getCurrentTime().format(
                                    DateTimeFormatter.ofPattern(value, locale.getValue().getLocale())));
                            return ValidationResult.ok();
                        } catch (DateTimeException | IllegalArgumentException e) {
                            return ValidationResult.error(i18n.get("settings.dateFormat.error"));
                        }
                    })
                    .bind(SettingsTabConfigurationHelper::getCustomDateFormat, SettingsTabConfigurationHelper::setCustomDateFormat);
            } else {
                binder.removeBinding(customDateFormat);
            }
        });

        return dateFormatLayout;
    }

    private VerticalLayout getSequenceSettings() {
        sequenceGrid = new Grid<>();
        sequenceGrid.setSizeFull();
        sequenceGrid.setItems(sequenceService.findAll());
        sequenceGrid.addColumn(Sequence::getName).setCaption(i18n.get("settings.sequence.grid.name"));
        sequenceGrid.addColumn(Sequence::getCurrentValue).setCaption(i18n.get("settings.sequence.grid.currentValue"));

        sequenceGrid.addContextClickListener(event -> {
            GridContextClickEvent<Department> contextEvent = (GridContextClickEvent<Department>) event;

            ContextMenu contextMenu = new ContextMenu(sequenceGrid, true);

            if (contextEvent.getItem() != null) {
                contextMenu.addItem(i18n.get("settings.sequence.grid.context.remove"), e -> {
                    contextMenu.remove();
                });

                contextMenu.addItem(i18n.get("settings.sequence.grid.context.edit"), e -> {
                    sessionEventBus.post(new AddTabEvent(SequenceTab.class, contextEvent.getItem()));
                    contextMenu.remove();
                });
            }

            contextMenu.addItem(i18n.get("settings.sequence.grid.context.create"), e -> {
                sessionEventBus.post(new AddTabEvent(SequenceTab.class));
                contextMenu.remove();
            });
            contextMenu.open(event.getClientX(), event.getClientY());
        });

        VerticalLayout layout = new VerticalLayout(sequenceGrid);
        layout.setSizeFull();
        return layout;
    }

    private VerticalLayout getPrintTemplateSettings() {
        ComboBox<PrintDocumentType> documentType = new ComboBox<>(i18n.get("settings.printTemplate.templateType"));
        documentType.setItemCaptionGenerator(item -> i18n.get(item.getTemplateName()));
        documentType.setItems(PrintDocumentType.values());

        Grid<PrintTemplateFile> templateGrid = new Grid<>();
        templateGrid.setItems(printTemplateFileService.findAll());
        templateGrid.setSizeFull();

        templateGrid.addColumn(item -> i18n.get(item.getTemplateType().getTemplateName()))
            .setCaption(i18n.get("settings.printTemplate.grid.type"));

        templateGrid.addColumn(PrintTemplateFile::getDisplayName)
            .setCaption(i18n.get("settings.printTemplate.grid.name"));

        templateGrid.addComponentColumn(item -> {
            CheckBox checkBox = new CheckBox();
            checkBox.setValue(item.isEnabled());
            checkBox.setEnabled(false);

            return checkBox;
        }).setCaption(i18n.get("settings.printTemplate.grid.isEnabled"));

        templateGrid.addComponentColumn(item -> {
            try {
                Button download = new Button(VaadinIcons.DOWNLOAD);
                download.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

                FileDownloader downloader = new FileDownloader(
                    storageService.getPrintTemplateStreamResource(item.getFileName()));
                downloader.extend(download);

                return download;
            } catch (FileNotFoundException e) {
                Notification.show(i18n.get("settings.printTemplate.fileNotFound"), Type.ERROR_MESSAGE);
                return null;
            }
        }).setCaption(i18n.get("settings.printTemplate.grid.fileDownload"));

        templateEditLayout = new VerticalLayout();
        templateEditLayout.setVisible(false);

        GridContextMenu<PrintTemplateFile> panelMenu = new GridContextMenu<>(templateGrid);
        panelMenu.addGridBodyContextMenuListener(event -> {
            ContextMenu contextMenu = event.getContextMenu();
            contextMenu.removeItems();
            PrintTemplateFile template = (PrintTemplateFile) event.getItem();

            if (template != null) {
                if (template.isEnabled())
                    contextMenu.addItem(i18n.get("settings.printTemplate.grid.context.deactivate"), VaadinIcons.CLOSE,
                        editEvent -> {
                            template.setEnabled(false);
                            printTemplateFileService.save(template);
                            templateGrid.setItems(printTemplateFileService.findAll());
                        });
                else
                    contextMenu.addItem(i18n.get("settings.printTemplate.grid.context.activate"), VaadinIcons.CHECK,
                        editEvent -> {
                            template.setEnabled(true);
                            printTemplateFileService.save(template);
                            templateGrid.setItems(printTemplateFileService.findAll());
                        });

                event.getContextMenu().addItem(i18n.get("settings.printTemplate.grid.context.edit"), VaadinIcons.EDIT,
                    editEvent -> {
                        templateEditLayout.setVisible(true);
                        templateEditLayout.removeAllComponents();
                        templateEditLayout.addComponent(getTemplateEditForm(template,
                            () -> {
                                templateEditLayout.removeAllComponents();
                                templateGrid.setItems(printTemplateFileService.findAll());
                            }, templateEditLayout::removeAllComponents));
                    });

                event.getContextMenu().addItem(i18n.get("settings.printTemplate.grid.context.delete"), VaadinIcons.TRASH,
                    editEvent -> {
                        printTemplateFileService.delete(template);
                        templateGrid.setItems(printTemplateFileService.findAll());
                    });
            }

            event.getContextMenu().addItem(i18n.get("settings.printTemplate.grid.context.create"), VaadinIcons.PLUS,
                editEvent -> {
                    templateEditLayout.setVisible(true);
                    templateEditLayout.removeAllComponents();
                    templateEditLayout.addComponent(getTemplateEditForm(null,
                        () -> {
                            templateEditLayout.removeAllComponents();
                            templateGrid.setItems(printTemplateFileService.findAll());
                            templateGrid.setItems(printTemplateFileService.findAll());
                        }, templateEditLayout::removeAllComponents));
                });
        });

        documentType.addValueChangeListener(event ->
            ((ListDataProvider<PrintTemplateFile>) templateGrid.getDataProvider())
                .setFilter(PrintTemplateFile::getTemplateType,
                    printDocumentType -> event.getValue() == null || event.getValue().equals(printDocumentType)));

        VerticalLayout layout = new VerticalLayout(documentType, templateGrid, templateEditLayout);
        layout.setSizeFull();
        layout.setExpandRatio(templateGrid, 1);

        return layout;
    }

    private FormLayout getTemplateEditForm(PrintTemplateFile template, DefaultCallback saveCallback,
                                           DefaultCallback cancelCallback) {
        if (template == null) {
            template = new PrintTemplateFile();
            template.setTemplateType(PrintDocumentType.values()[0]);
            template.setEnabled(true);
        }

        int hashCode = template.hashCode();

        FormLayout form = new FormLayout();

        ComboBox<PrintDocumentType> type = new ComboBox<>(i18n.get("settings.printTemplate.templateType"));
        type.setWidth(400, Unit.PIXELS);
        type.setItemCaptionGenerator(item -> i18n.get(item.getTemplateName()));
        type.setItems(PrintDocumentType.values());
        type.setEmptySelectionAllowed(false);
        form.addComponent(type);

        TextField name = new TextField(i18n.get("settings.printTemplate.templateName"));
        name.setWidth(400, Unit.PIXELS);
        form.addComponent(name);

        PrintTemplateUploadLayout uploadLayout = new PrintTemplateUploadLayout(storageService, i18n);
        form.addComponent(uploadLayout);

        Button save = new Button(i18n.get("settings.printTemplate.save"));
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        save.setEnabled(false);
        Button cancel = new Button(i18n.get("settings.printTemplate.cancel"));
        cancel.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        form.addComponent(new HorizontalLayout(save, cancel));

        Binder<PrintTemplateFile> binder = new Binder<>();
        binder.forField(type)
            .bind(PrintTemplateFile::getTemplateType, PrintTemplateFile::setTemplateType);
        binder.forField(name)
            .asRequired(i18n.get("settings.printTemplate.nameNotEmpty"))
            .bind(PrintTemplateFile::getDisplayName, PrintTemplateFile::setDisplayName);
        binder.forField(uploadLayout)
            .asRequired(i18n.get("settings.printTemplate.fileNotEmpty"))
            .bind(PrintTemplateFile::getFileName, PrintTemplateFile::setFileName);

        binder.setBean(template);
        binder.addValueChangeListener(event ->
            save.setEnabled(binder.isValid() && binder.getBean().hashCode() != hashCode));

        save.addClickListener(event -> {
            PrintTemplateFile bean = binder.getBean();
            bean.setUser(securityService.getUser());
            bean.setDateTime(chronometerService.getCurrentInstant());
            printTemplateFileService.save(bean);
            templateEditLayout.setVisible(false);
            saveCallback.run();
        });
        cancel.addClickListener(event -> {
            templateEditLayout.setVisible(false);
            cancelCallback.run();
        });

        return form;
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.settings");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.PURPLE;
    }

    @Subscribe
    private void updateDepartmentEventHandler(UpdateDepartmentEvent event) {
        List<Department> departments = departmentService.getRootDepartments();

        treeGrid.setItems(departments, Department::getChilds);
        treeGrid.expand(departments.stream().flatMap(Department::flattened).collect(Collectors.toList()));
    }

    @Subscribe
    private void updateSequenceEventHandler(UpdateSequenceEvent event) {
        List<Sequence> sequenceList = sequenceService.findAll();
        sequenceGrid.setItems(sequenceList);
        accountSequence.setItems(sequenceList);
    }
}

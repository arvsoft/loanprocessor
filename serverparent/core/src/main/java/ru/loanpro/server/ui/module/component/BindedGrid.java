package ru.loanpro.server.ui.module.component;

import com.vaadin.event.selection.MultiSelectionListener;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Grid;
import com.vaadin.ui.MultiSelect;

import java.util.HashSet;
import java.util.Set;

/**
 * Компонент, расширяющий {@link Grid<T>} и имплементирующий интерфейс {@link MultiSelect<T>}
 * для возможности биндинга поля сущности, содержащих коллекцию элементов <code>T</code>.
 *
 * @param <T> тип сущности, отображаемой гридом.
 *
 * @author Oleg Brizhevatikh
 */
public class BindedGrid<T> extends Grid<T> implements MultiSelect<T> {

    private Set<T> values;
    private boolean isRequired;

    /**
     * Конструктор, принимающий название компонента.
     *
     * @param title название компонента.
     */
    public BindedGrid(String title) {
        super(title);
    }

    /**
     * Устанавливает коллекцию текущих значений компонента. Все установленные значения будут
     * выбраны в таблице.
     *
     * @param values коллекция текущих значений.
     */
    @Override
    public void setValue(Set<T> values) {
        this.values = values;
        values.forEach(this::select);
    }

    /**
     * Возвращает коллекцию отмеченных в таблице элементов.
     *
     * @return коллекция отмеченных элементов.
     */
    @Override
    public Set<T> getValue() {
        return values;
    }

    @Override
    public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {
        isRequired = requiredIndicatorVisible;
        super.setRequiredIndicatorVisible(isRequired);
    }

    @Override
    public boolean isRequiredIndicatorVisible() {
        return isRequired;
    }

    @Override
    public void setReadOnly(boolean readOnly) {
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    /**
     * Добавляет обработчик изменений состояний компонентов. При добавлении или снятии отметки
     * выбора с элемента таблицы, в колбек отправляется событие изменения контента таблицы.
     *
     * @param valueChangeListener колбек изменения контента таблицы.
     * @return сообщение об успешной регистрации колбека.
     */
    @Override
    public Registration addValueChangeListener(ValueChangeListener<Set<T>> valueChangeListener) {
        super.addSelectionListener(event -> {
            Set<T> oldSelection = new HashSet<>(values);
            values = event.getAllSelectedItems();
            valueChangeListener.valueChange(new ValueChangeEvent<>(this, this, oldSelection, false));
        });

        return (Registration) () -> {};
    }

    /**
     * При программном обновлении компонентов в таблице, обновляет состояние текущей коллекции
     * выбранных элементов.
     *
     * @param addedItems коллекция элементов, добавляемая к выбранным.
     * @param removedItems коллекция элементов, с которых снято выделение.
     */
    @Override
    public void updateSelection(Set<T> addedItems, Set<T> removedItems) {
        values.removeAll(removedItems);
        values.addAll(addedItems);
    }

    @Override
    public Registration addSelectionListener(MultiSelectionListener<T> multiSelectionListener) {
        return (Registration) () -> {};
    }
}

package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.Label;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;

/**
 * Вкладка с информацией о том, что функциональность данного модуля в разработке.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.inProgress")
public class InProgressTab extends BaseTab {

    @PostConstruct
    public void init() {
        Label label = new Label("Данный модуль в разработке.");
        label.addStyleNames(MaterialTheme.LABEL_H2, MaterialTheme.LABEL_COLORED);

        addComponent(label);
    }

    @Override
    public String getTabName() {
        return "В разработке";
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.GREY;
    }
}

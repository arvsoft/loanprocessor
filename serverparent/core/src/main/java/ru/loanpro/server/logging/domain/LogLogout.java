package ru.loanpro.server.logging.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ru.loanpro.server.logging.annotation.LogName;

/**
 * Лог выхода пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("LOGOUT")
@LogName("log.logout")
public class LogLogout extends AbstractLog {
}

package ru.loanpro.server.ui;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.ImmutableList;
import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Page;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.storage.IStorageService;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.global.Roles;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.exceptions.NotHavePermissionToAccessException;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.security.service.SecurityService;
import ru.loanpro.server.ui.module.CashDeskTab;
import ru.loanpro.server.ui.module.ChronometerTab;
import ru.loanpro.server.ui.module.ClientsTab;
import ru.loanpro.server.ui.module.DirectoriesTab;
import ru.loanpro.server.ui.module.InProgressTab;
import ru.loanpro.server.ui.module.LoanProductsTab;
import ru.loanpro.server.ui.module.LoansTab;
import ru.loanpro.server.ui.module.LogsTab;
import ru.loanpro.server.ui.module.RequestsTab;
import ru.loanpro.server.ui.module.SecurityTab;
import ru.loanpro.server.ui.module.SettingsTab;
import ru.loanpro.server.ui.module.UserTab;
import ru.loanpro.server.ui.module.event.GlobalTimeTickEvent;
import ru.loanpro.server.ui.module.event.TimeTickEvent;
import ru.loanpro.server.ui.module.ReportsTab;
import ru.loanpro.uibasis.AbstractMainUI;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.uibasis.component.TabList;
import ru.loanpro.uibasis.event.AddTabEvent;

import javax.annotation.PostConstruct;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Основной интерфейс системы.
 *
 * @author Oleg Brizhevatikh
 */
@SpringUI
public class CoreApplicationUI extends AbstractMainUI {

    private static Logger LOGGER = Logger.getLogger(CoreApplicationUI.class.getName());

    private final ApplicationEventBus applicationEventBus;

    private final SecurityService securityService;

    private final ConfigurationService configurationService;

    private final I18N i18n;

    private final TabList tabList;

    private final ChronometerService chronometerService;

    private final IStorageService storageService;

    @Value("${system.version}")
    private String systemVersion;

    @Visible(Roles.Core.MENU_CLIENTS)
    private Button clients;

    @Visible(Roles.Core.MENU_LOAN_PRODUCTS)
    private Button loanProducts;

    @Visible(Roles.Core.MENU_REQUESTS)
    private Button requests;

    @Visible(Roles.Core.MENU_LOANS)
    private Button loans;

    @Visible(Roles.Core.MENU_DEPOSITS)
    private Button deposits;

    @Visible(Roles.Core.MENU_CASHDESK)
    private Button cashdesk;

    @Visible(Roles.Core.MENU_NOTIFICATIONS)
    private Button notifications;

    @Visible(Roles.Core.MENU_REPORTS)
    private Button reports;

    @Visible(Roles.Core.MENU_DIRECTORIES)
    private Button directories;

    @Visible(Roles.Core.MENU_SETTINGS)
    private Button settings;

    @Visible(Roles.Core.MENU_SECURITY)
    private Button security;

    @Visible(Roles.Core.MENU_LOGS)
    private Button logs;

    private PageSize pageSize;
    private ImmutableList<Button> menuButtons;
    private Label logo;
    private Button dateTimeButton;

    @Autowired
    public CoreApplicationUI(SecurityService securityService, SpringViewProvider springViewProvider, I18N i18n,
                             ApplicationEventBus applicationEventBus, SessionEventBus sessionEventBus, TabList tabList,
                             ChronometerService chronometerService, ConfigurationService configurationService,
                             IStorageService storageService) {
        super(sessionEventBus, springViewProvider);

        this.applicationEventBus = applicationEventBus;
        this.securityService = securityService;
        this.i18n = i18n;
        this.tabList = tabList;
        this.chronometerService = chronometerService;
        this.configurationService = configurationService;
        this.storageService = storageService;

        setLocale(configurationService.getLocale());
    }

    @PostConstruct
    public void init() {
        applicationEventBus.register(this);

        clients = newMenuButton(VaadinIcons.USERS);
        clients.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(ClientsTab.class)));

        loanProducts = newMenuButton(VaadinIcons.SUITCASE);
        loanProducts.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(LoanProductsTab.class)));

        requests = newMenuButton(VaadinIcons.USER_CLOCK);
        requests.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(RequestsTab.class)));

        loans = newMenuButton(VaadinIcons.WALLET);
        loans.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(LoansTab.class)));

        deposits = newMenuButton(VaadinIcons.MONEY_DEPOSIT);
        deposits.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(InProgressTab.class)));

        cashdesk = newMenuButton(VaadinIcons.MONEY_EXCHANGE);
        cashdesk.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(CashDeskTab.class)));

        notifications = newMenuButton(VaadinIcons.ENVELOPE);
        notifications.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(InProgressTab.class)));

        reports = newMenuButton(VaadinIcons.LINE_BAR_CHART);
        reports.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(ReportsTab.class)));

        directories = newMenuButton(VaadinIcons.CLUSTER);
        directories.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(DirectoriesTab.class)));

        settings = newMenuButton(VaadinIcons.COGS);
        settings.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(SettingsTab.class)));

        security = newMenuButton(VaadinIcons.SHIELD);
        security.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(SecurityTab.class)));

        logs = newMenuButton(VaadinIcons.STOCK);
        logs.addClickListener(e ->
            sessionEventBus.post(new AddTabEvent(LogsTab.class)));

        menuButtons = ImmutableList.of(clients, loanProducts, requests, loans,
            cashdesk, reports, directories, settings, security, logs);
    }

    @Override
    protected Layout getMenuHeader() {
        logo = new Label();
        logo.setWidth(100, Unit.PERCENTAGE);
        logo.addStyleName("menu-logo");

        VerticalLayout layout = new VerticalLayout(logo, getUserMenuItem());
        layout.setMargin(false);
        layout.setSpacing(false);

        return layout;
    }

    @Override
    protected Layout getMenuContent() {
        clients.setCaption(i18n.get("core.menu.clients"));
        loanProducts.setCaption(i18n.get("core.menu.loanProducts"));
        requests.setCaption(i18n.get("core.menu.requests"));
        loans.setCaption(i18n.get("core.menu.loans"));
        deposits.setCaption(i18n.get("core.menu.deposits"));
        cashdesk.setCaption(i18n.get("core.menu.cashdesk"));
        notifications.setCaption(i18n.get("core.menu.notifications"));
        reports.setCaption(i18n.get("core.menu.reports"));
        settings.setCaption(i18n.get("core.menu.settings"));
        directories.setCaption(i18n.get("core.menu.directories"));
        security.setCaption(i18n.get("core.menu.security"));
        logs.setCaption(i18n.get("core.menu.logs"));

        VerticalLayout layout = new VerticalLayout(clients,
            loanProducts,
            requests,
            loans,
            cashdesk,
            reports,
            directories,
            settings,
            security,
            logs);
        layout.setSpacing(false);
        layout.setMargin(false);

        return layout;
    }

    @Override
    protected Layout getMenuFooter() {
        dateTimeButton = new Button("time <span>date</span>");
        dateTimeButton.setCaptionAsHtml(true);
        dateTimeButton.setWidth(100, Unit.PERCENTAGE);
        dateTimeButton.addStyleNames(MaterialTheme.BUTTON_BORDERLESS, "text-align-center");

        if (securityService.hasAuthority(Roles.Core.TIME_CHANGE))
            dateTimeButton.addClickListener(event -> {
                ContextMenu contextMenu = new ContextMenu(dateTimeButton, true);
                contextMenu.addItem(i18n.get("core.menu.dateTime.change"),
                    e -> sessionEventBus.post(new AddTabEvent(ChronometerTab.class)));

                if (chronometerService.isTimeShifted())
                    contextMenu.addItem(i18n.get("core.menu.dateTime.restore"), e -> {
                        chronometerService.clearShiftDateTime();
                        sessionEventBus.post(new TimeTickEvent(chronometerService.getCurrentTime()));
                    });

                contextMenu.open(event.getClientX(), event.getClientY());
            });

        updateDateTime();

        Label versionLabel = new Label(String.format("v. %s", systemVersion));
        versionLabel.addStyleName(MaterialTheme.LABEL_TINY);

        VerticalLayout versionLayout = new VerticalLayout(versionLabel);
        versionLayout.setWidth(100, Unit.PERCENTAGE);
        versionLayout.setComponentAlignment(versionLabel, Alignment.BOTTOM_CENTER);

        VerticalLayout layout = new VerticalLayout(dateTimeButton, versionLayout);
        layout.setSpacing(false);

        return layout;
    }

    private Button newMenuButton(Resource icon) {
        Button button = new Button(icon);
        button.setWidth(100, Unit.PERCENTAGE);
        button.addStyleName(MaterialTheme.BUTTON_BORDERLESS);

        return button;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        super.init(vaadinRequest);
        setTheme(configurationService.getTheme());
        if (Page.getCurrent().getBrowserWindowWidth() > 1024)
            setLargeMenu();
        else
            setSmallMenu();
    }

    @Override
    public void close() {
        applicationEventBus.unregister(this);
        super.close();
    }

    private MenuBar getUserMenuItem() {
        MenuBar menuBar = new MenuBar();
        menuBar.addStyleName("user-menu");

        User user = securityService.getUser();
        MenuItem menuItem = menuBar.addItem(
            user.getUsername(), storageService.getAvatar(user.getAvatarFileName()), null);

        menuItem.addItem(i18n.get("core.menu.user.editProfile"),
            event -> sessionEventBus.post(new AddTabEvent(UserTab.class, securityService.getUser())));

        if (securityService.getDepartments().size() > 0) {
            MenuItem menuItemChangeDepartment = menuItem.addItem(i18n.get("core.menu.user.changeDepartment"), null);
            securityService.getDepartments().forEach(department -> {
                boolean isCurrentDepartment = securityService.getDepartment().equals(department);

                MenuItem departmentItem = menuItemChangeDepartment.addItem(
                    department.getName(),
                    isCurrentDepartment ? VaadinIcons.CHECK : null,
                    event -> {
                        try {
                            securityService.changeDepartment(department.getId());
                            chronometerService.update();
                            tabList.destroy();
                            getUI().getPage().reload();
                        } catch (NotHavePermissionToAccessException e) {
                            LOGGER.log(Level.ALL, e.getMessage());
                            securityService.logout();
                        }
                    });
                departmentItem.setEnabled(!isCurrentDepartment);
            });
        }

        menuItem.addSeparator();
        menuItem.addItem(i18n.get("core.menu.logout"), event -> {
            tabList.getMap().keySet().forEach(sessionEventBus::unregister);
            tabList.destroy();
            securityService.logout();
        });

        return menuBar;
    }

    @Override
    protected void browserWindowResizeHandler(BrowserWindowResizeEvent resizeEvent) {
        if (resizeEvent.getWidth() <= 1024 && pageSize.equals(PageSize.LARGE))
            setSmallMenu();
        else if (resizeEvent.getWidth() > 1024 && pageSize.equals(PageSize.SMALL))
            setLargeMenu();
    }

    private void setLargeMenu() {
        logo.setValue(i18n.get("core.menu.title"));
        logo.setContentMode(ContentMode.HTML);

        menuButtons.forEach(button -> button.removeStyleName(MaterialTheme.BUTTON_ICON_ALIGN_TOP));
        navigationBar.removeStyleName("menu-container-small");
        navigationBar.addStyleName("menu-container-large");
        navigationBar.setWidth(220, Unit.PIXELS);
        pageSize = PageSize.LARGE;
    }

    private void setSmallMenu() {
        logo.setValue(i18n.get("core.menu.titleSmall"));
        logo.setContentMode(ContentMode.HTML);

        menuButtons.forEach(button -> button.addStyleName(MaterialTheme.BUTTON_ICON_ALIGN_TOP));
        navigationBar.addStyleName("menu-container-small");
        navigationBar.removeStyleName("menu-container-large");
        navigationBar.setWidth(110, Unit.PIXELS);
        pageSize = PageSize.SMALL;
    }

    private enum PageSize {
        LARGE, SMALL
    }

    private void updateDateTime() {
        sessionEventBus.post(new TimeTickEvent(chronometerService.getCurrentTime()));
    }

    @Subscribe
    private void timeTickEventHandler(TimeTickEvent event) {
        getUI().access(() -> {
            ZonedDateTime currentTime = event.getTime();

            dateTimeButton.setCaption(String.format("%s <span>%s</span>",
                currentTime.format(DateTimeFormatter.ofPattern("HH:mm")),
                currentTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))));

            if (chronometerService.isTimeShifted())
                dateTimeButton.addStyleName(MaterialTheme.BUTTON_DANGER);
            else
                dateTimeButton.removeStyleName(MaterialTheme.BUTTON_DANGER);
        });
    }

    @Subscribe
    private void globalTimeTickEventHandler(GlobalTimeTickEvent event) {
        updateDateTime();
    }
}

package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.MenuItem;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import fr.opensagres.xdocreport.core.XDocReportException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.NumberService;
import ru.loanpro.base.account.domain.BaseAccount;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.domain.JuridicalAccount;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.account.exceptions.AccountNotFoundException;
import ru.loanpro.base.account.exceptions.InsufficientAccountBalanceException;
import ru.loanpro.base.account.service.DepartmentAccountService;
import ru.loanpro.base.account.service.PhysicalAccountService;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.client.service.PhysicalClientService;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.operation.event.UpdateOperationEvent;
import ru.loanpro.base.operation.exceptions.OperationNotSavedException;
import ru.loanpro.base.operation.service.OperationService;
import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.base.printer.PrintService;
import ru.loanpro.base.storage.domain.connector.OperationPrintedFile;
import ru.loanpro.base.storage.service.PrintTemplateFileService;
import ru.loanpro.base.storage.service.PrintedFileService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.directory.AccountType;
import ru.loanpro.global.directory.OperationStatus;
import ru.loanpro.global.directory.PayerType;
import ru.loanpro.global.directory.PaymentMethod;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.service.SecurityService;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.server.ui.module.additional.BigDecimalConverter;
import ru.loanpro.server.ui.module.additional.InstantLocalDateConverter;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.event.EntityEditingEvent;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Вкладка работы с операциями
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
@EntityTab(Operation.class)
public class OperationTab extends BaseTab {

    @Autowired
    private PhysicalClientService clientService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private PhysicalAccountService physicalAccountService;

    @Autowired
    private PhysicalClientService physicalClientService;

    @Autowired
    private DepartmentAccountService departmentAccountService;

    @Autowired
    private PrintTemplateFileService printTemplateFileService;

    @Autowired
    protected NumberService numberService;

    @Autowired
    PrintedFileService printedFileService;

    @Autowired
    protected PrintService printService;

    @Autowired
    private OperationService operationService;

    @Autowired
    private SecurityService securityService;

    private MoneyField operationValue;
    private Operation entity;
    private Binder<Operation> binder;
    private int operationHashCode;

    private ComboBox<PayerType> payerType;
    private ComboBox<PhysicalClient> payerClient;
    private ComboBox<Department> payerDepartment;
    private ComboBox<BaseAccount> payerAccount;
    private ComboBox<Currency> payerCurrency;
    private MoneyField payerBalance;

    private ComboBox<PayerType> recipientType;
    private ComboBox<PhysicalClient> recipientClient;
    private ComboBox<Department> recipientDepartment;
    private ComboBox<BaseAccount> recipientAccount;
    private ComboBox<PaymentMethod> paymentMethod;
    private ComboBox<Currency> recipientCurrency;
    private ComboBox<Currency> operationCurrency;
    private TextField notes;
    private DateField operationDate;
    private ComboBox<OperationStatus> status;
    private TextField operationNumber;

    private MoneyField recipientBalance;
    private boolean loading;

    @Visible(Roles.Operation.EDIT)
    private Button edit;
    @Visible(Roles.Operation.PREPARE)
    private Button prepare;
    @Visible(Roles.Operation.PROCESS)
    private Button process;
    @Visible(Roles.Operation.PRINT)
    private Button print;

    public OperationTab(Operation operation) {
        entity = operation == null ? new Operation() : operation;
    }

    @PostConstruct
    public void init() {
        loading = true;

        if (entity.getId() == null) {
            initInitialEntityState();
        }

        operationHashCode = entity.hashCode();
        binder = new Binder<>();
        binder.setBean(entity);

        //Блок Основная информация
        Label mainInfo = new Label(i18n.get("operation.panel.mainInfo.title"));

        edit = new Button(i18n.get("operation.button.edit"), VaadinIcons.EDIT);
        edit.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        edit.addClickListener(event -> {
            applicationEventBus.post(new EntityEditingEvent<>(entity));
            binder.setReadOnly(false);
            setReadOnlyNonBinderFields(false);
            edit.setEnabled(false);
            prepare.setEnabled(true);
        });

        prepare = new Button(i18n.get("operation.button.prepare"), VaadinIcons.HOURGLASS);
        prepare.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        prepare.addClickListener(event -> {
            try {
                entity.setUser(securityService.getUser());
                operationService.savePrepared(entity, chronometerService.getCurrentInstant());
                operationHashCode = entity.hashCode();
                binder.setBean(binder.getBean());

                buttonsStatusUpdate();
                binder.setReadOnly(true);
                setReadOnlyNonBinderFields(true);

                applicationEventBus.post(new UpdateOperationEvent(entity));
                Notification.show(i18n.get("operation.message.operationPrepared"), Notification.Type.TRAY_NOTIFICATION);
            } catch (OperationNotSavedException e) {
                Notification.show(i18n.get(e.getMessage()), Notification.Type.WARNING_MESSAGE);
                e.printStackTrace();
            }
        });

        process = new Button(i18n.get("operation.button.process"), VaadinIcons.CHECK);
        process.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        process.addClickListener(event -> {
            try {
                entity.setUser(securityService.getUser());

                //если операция новая, то берем текущее время,
                //если операция не новая (в статусе PREPARED),
                //то если опция KeepSameOperationDateWhenSaveFromPreparedToProcessed включена,
                //то сохраняем дату такой, какой она была в статусе PREPARED,
                //если опция отключена, то берем текущую дату
                Instant date = entity.getStatus() == null ? chronometerService.getCurrentInstant() :
                    (configurationService.getKeepSameOperationDateWhenSaveFromPreparedToProcessed() ?
                        entity.getOperationDateTime() : chronometerService.getCurrentInstant());

                operationService.save(entity, date);
                operationHashCode = entity.hashCode();
                binder.setBean(binder.getBean());

                if (payerAccount.getValue() != null) {
                    payerBalance.setValue(
                        getBalanceFromBigDecimal(departmentAccountService.getBalanceForAccount(payerAccount.getValue())));
                }
                if (recipientAccount.getValue() != null) {
                    recipientBalance.setValue(
                        getBalanceFromBigDecimal(departmentAccountService.getBalanceForAccount(recipientAccount.getValue())));
                }
                buttonsStatusUpdate();
                binder.setReadOnly(true);
                setReadOnlyNonBinderFields(true);

                applicationEventBus.post(new UpdateOperationEvent(entity));
                Notification.show(i18n.get("operation.message.operationProcessed"), Notification.Type.TRAY_NOTIFICATION);
            } catch (InsufficientAccountBalanceException | OperationNotSavedException e) {
                Notification.show(i18n.get(e.getMessage()), Notification.Type.WARNING_MESSAGE);
                e.printStackTrace();
            }
        });

        print = new Button(i18n.get("operation.button.print"), VaadinIcons.PRINT);
        print.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        print.addClickListener(event -> {
            ContextMenu contextMenu = new ContextMenu(print, true);

            List<OperationPrintedFile> printedFiles = printedFileService.findOperationPrintedFiles(entity);
            if (printedFiles.size() > 0) {
                MenuItem printedItem = contextMenu.addItem(i18n.get("operation.button.printed"), null);
                printedFiles.forEach(printed ->
                    printedItem.addItem(
                        String.format("%s %s", printed.getDisplayName(),
                            chronometerService.zonedDateTimeToString(printed.getDateTime())),
                        e -> getUI().getPage().open(String.format("/print/%s", printed.getFileName()), "_blank")));
            }

            printTemplateFileService.getActiveTemplates(PrintDocumentType.OPERATION)
                .forEach(template ->
                    contextMenu.addItem(template.getDisplayName(), e -> {
                        try {
                            String fileName = printService.printTemplate(template, entity);
                            getUI().getPage().open(String.format("/print/%s", fileName), "_blank");
                        } catch (IOException | XDocReportException exception) {
                            showErrorMessage(exception);
                        }
                    }));

            contextMenu.open(event.getClientX(), event.getClientY());
        });

        HorizontalLayout hlButtonAll = new HorizontalLayout(edit, prepare, process, print);
        hlButtonAll.setStyleName("flex-wrap-right-alignment-layout");

        HorizontalLayout hlActions = new HorizontalLayout(mainInfo, hlButtonAll);
        hlActions.setWidth(100, Unit.PERCENTAGE);
        hlActions.setExpandRatio(hlButtonAll, 1.0f);

        Label currentDepartment = new Label(String.format(
            i18n.get("operation.field.operationDepartment"), entity.getDepartment().getName()));

        status = new ComboBox<>(
            i18n.get("operation.field.status"), Lists.newArrayList(OperationStatus.values()));
        status.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        status.setWidth(200, Unit.PIXELS);
        status.setEmptySelectionAllowed(false);
        status.setEnabled(false);

        operationNumber = new TextField(i18n.get("operation.field.operationNumber"));
        operationNumber.setStyleName("v-align-right");
        operationNumber.setEnabled(false);

        HorizontalLayout hlStatus = new HorizontalLayout(status, operationNumber);

        operationDate = new DateField(i18n.get("operation.datefield.operationDate"));
        operationDate.setWidth(200, Unit.PIXELS);
        operationDate.setDateFormat(configurationService.getDateFormat());
        operationDate.setEnabled(false);

        paymentMethod = new ComboBox<>(
            i18n.get("operation.field.method"), Lists.newArrayList(PaymentMethod.values()));
        paymentMethod.setSizeFull();
        paymentMethod.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        paymentMethod.setEmptySelectionAllowed(false);
        paymentMethod.setSelectedItem(PaymentMethod.CASHLESS);

        operationValue = new MoneyField(i18n.get("operation.field.operationValue"), configurationService.getLocale());
        operationValue.addStyleName(MaterialTheme.LABEL_COLORED);
        operationValue.setWidth(200, Unit.PIXELS);
        operationValue.addStyleName("align-right");

        operationCurrency = new ComboBox<>(i18n.get("operation.field.currency"));
        operationCurrency.setEmptySelectionAllowed(false);
        operationCurrency.setWidth(100, Unit.PIXELS);
        operationCurrency.setItems(configurationService.getCurrencies());
        operationCurrency.setValue(configurationService.getCurrencyDefault());

        GridLayout block1 = new GridLayout(4, 1, operationDate, operationCurrency, operationValue, paymentMethod);
        block1.setSpacing(true);

        notes = new TextField(i18n.get("operation.field.notes"));
        notes.setSizeFull();

        VerticalLayout vlHeader = new VerticalLayout(hlActions, currentDepartment, hlStatus, block1, notes);
        vlHeader.setWidth(100, Unit.PERCENTAGE);
        vlHeader.addStyleName(MaterialTheme.CARD_2);
        vlHeader.setSizeFull();
        vlHeader.setSpacing(true);
        addComponents(vlHeader);

        VerticalLayout vlPayerHeader = new VerticalLayout();
        vlPayerHeader.setWidth(100, Unit.PERCENTAGE);
        vlPayerHeader.addStyleName(MaterialTheme.CARD_2);
        vlPayerHeader.setSizeFull();
        vlPayerHeader.setSpacing(true);

        if (entity.getLoan() == null) {
            vlPayerHeader.addComponents(vlHeader, getPayerInfo(), getRecipientInfo());
        } else {
            vlPayerHeader.addComponents(vlHeader, getPayerInfo(), getRecipientInfo(), getLoanInfo());
        }

        operationCurrency.addValueChangeListener(event -> {
            if (!loading) {
                vlPayerHeader.removeAllComponents();
                if (entity.getLoan() == null) {
                    vlPayerHeader.addComponents(vlHeader, getPayerInfo(), getRecipientInfo());
                } else {
                    vlPayerHeader.addComponents(vlHeader, getPayerInfo(), getRecipientInfo(), getLoanInfo());
                }

                entity.setAccountPayer(null);
                entity.setAccountRecipient(null);
                payerCurrency.setValue(null);
                recipientCurrency.setValue(null);

                bind();
                selectPaymentMethod(paymentMethod.getValue());
                buttonsStatusUpdate();
            }
        });

        paymentMethod.addValueChangeListener(event -> {
            if (!loading) {
                selectPaymentMethod(event.getValue());
                buttonsStatusUpdate();
            }
        });

        addComponents(vlPayerHeader);

        bind();
        selectPaymentMethod(entity.getPaymentMethod());

        if (entity.getId() != null) {
            if (entity.getAccountPayer() != null && entity.getAccountPayer().getClass() == PhysicalAccount.class) {
                UUID id = entity.getAccountPayer().getId();
                PhysicalAccount physicalAccount = physicalAccountService.getOneFull(id);
                PhysicalClientDetails clientDetails = physicalAccount.getClientDetails();
                PhysicalClient client = physicalClientService.findOneByPhysicalClientDetails(clientDetails);
                payerType.setValue(PayerType.CLIENT);
                payerClient.setVisible(true);
                payerClient.setValue(client);
                payerDepartment.setVisible(false);
            }
            if (entity.getAccountPayer() != null && entity.getAccountPayer().getClass() == DepartmentAccount.class) {
                UUID uuid = entity.getAccountPayer().getId();
                DepartmentAccount departmentAccount = departmentAccountService.getOneFull(uuid);
                Department department = departmentAccount.getDepartment();
                payerType.setValue(PayerType.DEPARTMENT);
                payerDepartment.setVisible(true);
                payerDepartment.setValue(department);
                payerClient.setVisible(false);
            }
            if (entity.getAccountPayer() != null && entity.getAccountPayer().getClass() == JuridicalAccount.class) {
            }
            if (entity.getAccountRecipient() != null && entity.getAccountRecipient().getClass() == PhysicalAccount.class) {
                UUID id = entity.getAccountRecipient().getId();
                PhysicalAccount physicalAccount = physicalAccountService.getOneFull(id);
                PhysicalClientDetails clientDetails = physicalAccount.getClientDetails();
                PhysicalClient client = physicalClientService.findOneByPhysicalClientDetails(clientDetails);
                recipientType.setValue(PayerType.CLIENT);
                recipientClient.setVisible(true);
                recipientClient.setValue(client);
                recipientDepartment.setVisible(false);
            }
            if (entity.getAccountRecipient() != null && entity.getAccountRecipient().getClass() == DepartmentAccount.class) {
                UUID uuid = entity.getAccountRecipient().getId();
                DepartmentAccount departmentAccount = departmentAccountService.getOneFull(uuid);
                Department department = departmentAccount.getDepartment();
                recipientType.setValue(PayerType.DEPARTMENT);
                recipientDepartment.setVisible(true);
                recipientDepartment.setValue(department);
                recipientClient.setVisible(false);
            }
            if (entity.getAccountRecipient() != null && entity.getAccountRecipient().getClass() == JuridicalAccount.class) {
            }
            setReadOnlyNonBinderFields(true);
        }

        buttonsStatusUpdate();
        binder.setReadOnly(entity.getId() != null);
        loading = false;
    }

    private void bind() {
        binder = new Binder<>();
        binder.setBean(entity);

        binder.bind(status, Operation::getStatus, Operation::setStatus);

        binder.bind(operationNumber, Operation::getOperationNumber, Operation::setOperationNumber);

        binder.forField(operationDate)
            .withConverter(new InstantLocalDateConverter(chronometerService.getCurrentZoneId()))
            .bind(Operation::getOperationDateTime, Operation::setOperationDateTime);

        binder.forField(operationValue)
            .asRequired()
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.valueOf(0.01), BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(Operation::getOperationValue, Operation::setOperationValue);

        binder.bind(paymentMethod, Operation::getPaymentMethod, Operation::setPaymentMethod);

        binder.bind(notes, Operation::getNotes, Operation::setNotes);

        binder.addValueChangeListener(event -> {
            buttonsStatusUpdate();
        });
    }

    //Блок Плательщик
    private Layout getPayerInfo() {
        Label payerInfo = new Label(i18n.get("operation.panel.payerInfo.title"));

        HorizontalLayout hlActions = new HorizontalLayout(payerInfo);
        hlActions.setWidth(100, Unit.PERCENTAGE);

        payerType = new ComboBox<>(
            i18n.get("operation.field.payerType"), Lists.newArrayList(PayerType.values()));
        payerType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        payerType.setEmptySelectionAllowed(false);
        payerType.setSelectedItem(PayerType.CLIENT);

        payerClient = new ComboBox<>(i18n.get("operation.field.payerClient"));
        payerClient.setSizeFull();
        payerClient.setItemCaptionGenerator(Client::getDisplayName);
        payerClient.setEmptySelectionAllowed(false);
        payerClient.setDataProvider(
            clientService::findAll,
            clientService::count);

        payerDepartment = new ComboBox<>(i18n.get("operation.field.payerDepartment"));
        payerDepartment.setSizeFull();
        payerDepartment.setItemCaptionGenerator(Department::getName);
        payerDepartment.setEmptySelectionAllowed(false);
        payerDepartment.setVisible(false);

        if (securityService.hasAuthority(Roles.Operation.SELECT_DEPARTMENT_PAYER)) {
            payerDepartment.setItems(departmentService.findAll());
        } else {
            payerDepartment.setItems(securityService.getDepartments());
        }

        HorizontalLayout hlPayer1 = new HorizontalLayout(payerType, payerClient, payerDepartment);
        hlPayer1.setSizeFull();
        hlPayer1.setExpandRatio(payerClient, 1.0f);
        hlPayer1.setExpandRatio(payerDepartment, 1.0f);

        payerAccount = new ComboBox<>(i18n.get("operation.field.account"));
        payerAccount.setSizeFull();
        payerAccount.setItemCaptionGenerator(BaseAccount::getNumber);
        payerAccount.setEmptySelectionAllowed(false);

        payerCurrency = new ComboBox<>(i18n.get("operation.field.currency"));
        payerCurrency.setItems(configurationService.getCurrencies());
        payerCurrency.setReadOnly(true);

        payerBalance = new MoneyField(i18n.get("operation.field.balance"), configurationService.getLocale());
        payerBalance.setReadOnly(true);

        HorizontalLayout hlPayer2 = new HorizontalLayout(payerAccount, payerBalance, payerCurrency);
        hlPayer2.setSizeFull();
        hlPayer2.setExpandRatio(payerAccount, 1.0f);

        payerType.addValueChangeListener(event -> {
            if (!loading) {
                entity.setAccountPayer(null);
                binder.removeBinding(payerAccount);
                binder.forField(payerAccount)
                    .asRequired()
                    .bind(Operation::getAccountPayer, Operation::setAccountPayer);

                switch (event.getValue()) {
                    case CLIENT:
                        payerDepartment.setVisible(false);
                        payerClient.setVisible(true);
                        break;
                    case DEPARTMENT:
                        payerClient.setVisible(false);
                        payerDepartment.setVisible(true);
                        break;
                }
                binderSetup(paymentMethod.getValue());
                buttonsStatusUpdate();
            }
        });

        payerClient.addValueChangeListener(event -> {
            if (!loading) {
                if (event.getValue() == null)
                    return;
                try {
                    addPhysicalAccounts(operationCurrency.getValue(), payerAccount, event.getValue().getDetails(),
                        paymentMethod.getValue() != PaymentMethod.CASHLESS ? null : recipientAccount.getValue());
                } catch (AccountNotFoundException e) {
                    Notification.show(i18n.get(e.getMessage()), Notification.Type.WARNING_MESSAGE);
                    e.printStackTrace();
                }
                buttonsStatusUpdate();
            }
        });

        payerDepartment.addValueChangeListener(event -> {
            if (!loading) {
                if (event.getValue() == null)
                    return;
                try {
                    addDepartmentAccounts(operationCurrency.getValue(), payerAccount, event,
                        paymentMethod.getValue() != PaymentMethod.CASHLESS ? null : recipientAccount.getValue());
                } catch (AccountNotFoundException e) {
                    Notification.show(i18n.get(e.getMessage()), Notification.Type.WARNING_MESSAGE);
                    e.printStackTrace();
                }
                buttonsStatusUpdate();
            }
        });

        payerAccount.addValueChangeListener(event -> {
            if (!loading) {
                if (event.getValue() == null)
                    return;
                payerBalance.setValue(getBalanceFromBigDecimal(event.getValue().getBalance()));
                payerCurrency.setSelectedItem(event.getValue().getCurrency());
                buttonsStatusUpdate();
            }
        });

        VerticalLayout layout = new VerticalLayout(hlActions, hlPayer1, hlPayer2);
        layout.addStyleName(MaterialTheme.CARD_2);
        layout.setMargin(false);
        layout.setSpacing(true);

        return layout;
    }

    private Layout getRecipientInfo() {
        Label recipientInfo = new Label(i18n.get("operation.panel.recipientInfo.title"));

        HorizontalLayout hlActions = new HorizontalLayout(recipientInfo);
        hlActions.setWidth(100, Unit.PERCENTAGE);

        recipientType = new ComboBox<>(
            i18n.get("operation.field.recipientType"), Lists.newArrayList(PayerType.values()));
        recipientType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        recipientType.setEmptySelectionAllowed(false);
        recipientType.setSelectedItem(PayerType.DEPARTMENT);

        recipientClient = new ComboBox<>(i18n.get("operation.field.recipientClient"));
        recipientClient.setSizeFull();
        recipientClient.setItemCaptionGenerator(Client::getDisplayName);
        recipientClient.setEmptySelectionAllowed(false);
        recipientClient.setVisible(false);
        recipientClient.setDataProvider(
            clientService::findAll,
            clientService::count);

        recipientDepartment = new ComboBox<>(i18n.get("operation.field.recipientDepartment"));
        recipientDepartment.setSizeFull();
        recipientDepartment.setItemCaptionGenerator(Department::getName);
        recipientDepartment.setEmptySelectionAllowed(false);
        recipientDepartment.setItems(departmentService.findAll());

        HorizontalLayout hlRecipient1 = new HorizontalLayout(recipientType, recipientClient, recipientDepartment);
        hlRecipient1.setSizeFull();
        hlRecipient1.setExpandRatio(recipientClient, 1.0f);
        hlRecipient1.setExpandRatio(recipientDepartment, 1.0f);

        recipientAccount = new ComboBox<>(i18n.get("operation.field.account"));
        recipientAccount.setSizeFull();
        recipientAccount.setItemCaptionGenerator(BaseAccount::getNumber);
        recipientAccount.setEmptySelectionAllowed(false);

        recipientCurrency = new ComboBox<>(i18n.get("operation.field.currency"));
        recipientCurrency.setItems(configurationService.getCurrencies());
        recipientCurrency.setReadOnly(true);

        recipientBalance = new MoneyField(i18n.get("operation.field.balance"), configurationService.getLocale());
        recipientBalance.setReadOnly(true);

        HorizontalLayout hlRecipient2 = new HorizontalLayout(recipientAccount, recipientBalance, recipientCurrency);
        hlRecipient2.setSizeFull();
        hlRecipient2.setExpandRatio(recipientAccount, 1.0f);

        recipientType.addValueChangeListener(event -> {
            if (!loading) {
                entity.setAccountRecipient(null);
                binder.removeBinding(recipientAccount);
                binder.forField(recipientAccount)
                    .asRequired()
                    .bind(Operation::getAccountRecipient, Operation::setAccountRecipient);

                switch (event.getValue()) {
                    case CLIENT:
                        recipientDepartment.setVisible(false);
                        recipientClient.setVisible(true);
                        break;
                    case DEPARTMENT:
                        recipientDepartment.setVisible(true);
                        recipientClient.setVisible(false);
                        break;
                }
                binderSetup(paymentMethod.getValue());
            }
        });

        recipientClient.addValueChangeListener(event -> {
            if (!loading) {
                if (event.getValue() == null)
                    return;
                try {
                    addPhysicalAccounts(operationCurrency.getValue(), recipientAccount, event.getValue().getDetails(),
                        paymentMethod.getValue() != PaymentMethod.CASHLESS ? null : payerAccount.getValue());
                } catch (AccountNotFoundException e) {
                    Notification.show(i18n.get(e.getMessage()), Notification.Type.WARNING_MESSAGE);
                    e.printStackTrace();
                    buttonsStatusUpdate();
                }
                buttonsStatusUpdate();
            }
        });

        recipientDepartment.addValueChangeListener(event -> {
            if (!loading) {
                if (event.getValue() == null)
                    return;
                try {
                    addDepartmentAccounts(operationCurrency.getValue(), recipientAccount, event,
                        paymentMethod.getValue() != PaymentMethod.CASHLESS ? null : payerAccount.getValue());
                } catch (AccountNotFoundException e) {
                    Notification.show(i18n.get(e.getMessage()), Notification.Type.WARNING_MESSAGE);
                    e.printStackTrace();
                }
                buttonsStatusUpdate();
            }
        });

        recipientAccount.addValueChangeListener(event -> {
            if (!loading) {
                if (event.getValue() == null)
                    return;
                recipientBalance.setValue(getBalanceFromBigDecimal(event.getValue().getBalance()));
                recipientCurrency.setSelectedItem(event.getValue().getCurrency());
                buttonsStatusUpdate();
            }
        });

        VerticalLayout layout = new VerticalLayout(hlActions, hlRecipient1, hlRecipient2);
        layout.addStyleName(MaterialTheme.CARD_2);
        layout.setMargin(false);
        layout.setSpacing(true);

        return layout;
    }

    private String getBalanceFromBigDecimal(BigDecimal value) {
        return new BigDecimalConverter(configurationService.getLocale(), "")
            .convertToPresentation(value == null ? BigDecimal.ZERO : value, null);
    }

    private void selectPaymentMethod(PaymentMethod paymentMethod) {
        switch (paymentMethod) {
            case CASH_REPLENISH:
                payerType.setValue(PayerType.CLIENT);
                payerType.setReadOnly(true);
                payerClient.setVisible(true);
                payerDepartment.setVisible(false);
                payerAccount.setVisible(false);
                payerBalance.setVisible(false);
                payerCurrency.setVisible(false);

                recipientType.setValue(PayerType.DEPARTMENT);
                recipientType.setReadOnly(false);
                recipientDepartment.setVisible(true);
                recipientClient.setVisible(false);
                recipientAccount.setVisible(true);
                recipientBalance.setVisible(true);
                recipientCurrency.setVisible(true);
                break;
            case CASH_ISSUE:
                payerType.setValue(PayerType.DEPARTMENT);
                payerType.setReadOnly(false);
                payerDepartment.setVisible(true);
                payerClient.setVisible(false);
                payerAccount.setVisible(true);
                payerBalance.setVisible(true);
                payerCurrency.setVisible(true);

                recipientType.setValue(PayerType.CLIENT);
                recipientType.setReadOnly(true);
                recipientClient.setVisible(true);
                recipientDepartment.setVisible(false);
                recipientAccount.setVisible(false);
                recipientBalance.setVisible(false);
                recipientCurrency.setVisible(false);
                break;
            case CASHLESS:
                payerType.setValue(PayerType.CLIENT);
                payerType.setReadOnly(true);
                payerClient.setVisible(true);
                payerDepartment.setVisible(false);
                payerAccount.setVisible(true);
                payerBalance.setVisible(true);
                payerCurrency.setVisible(true);

                recipientType.setValue(PayerType.CLIENT);
                recipientType.setReadOnly(true);
                recipientClient.setVisible(true);
                recipientDepartment.setVisible(false);
                recipientAccount.setVisible(true);
                recipientBalance.setVisible(true);
                recipientCurrency.setVisible(true);
                break;
        }
        binderSetup(paymentMethod);
    }

    private void setReadOnlyNonBinderFields(boolean isReadOnly) {
        payerType.setReadOnly(isReadOnly);
        payerClient.setReadOnly(isReadOnly);
        payerDepartment.setReadOnly(isReadOnly);

        recipientType.setReadOnly(isReadOnly);
        recipientClient.setReadOnly(isReadOnly);
        recipientDepartment.setReadOnly(isReadOnly);
    }

    private void binderSetup(PaymentMethod method) {
        switch (method) {
            case CASH_REPLENISH:
                binder.forField(payerClient)
                    .asRequired()
                    .bind(Operation::getPayer, Operation::setPayer);
                binder.removeBinding(payerDepartment);
                payerDepartment.setRequiredIndicatorVisible(false);

                binder.forField(recipientAccount)
                    .asRequired()
                    .bind(Operation::getAccountRecipient, Operation::setAccountRecipient);

                binder.removeBinding(payerAccount);
                binder.removeBinding(recipientClient);
                binder.removeBinding(recipientDepartment);

                entity.setAccountPayer(null);
                //entity.setRecipient(null);

                payerAccount.setRequiredIndicatorVisible(false);
                recipientClient.setRequiredIndicatorVisible(false);
                recipientDepartment.setRequiredIndicatorVisible(false);
                break;
            case CASH_ISSUE:
                binder.forField(recipientClient)
                    .asRequired()
                    .bind(Operation::getRecipient, Operation::setRecipient);
                binder.removeBinding(recipientDepartment);
                recipientDepartment.setRequiredIndicatorVisible(false);

                binder.forField(payerAccount)
                    .asRequired()
                    .bind(Operation::getAccountPayer, Operation::setAccountPayer);

                binder.removeBinding(recipientAccount);
                binder.removeBinding(payerClient);
                binder.removeBinding(payerDepartment);

                //entity.setPayer(null);
                entity.setAccountRecipient(null);

                recipientAccount.setRequiredIndicatorVisible(false);
                payerClient.setRequiredIndicatorVisible(false);
                payerDepartment.setRequiredIndicatorVisible(false);
                break;
            case CASHLESS:
                binder.forField(payerAccount)
                    .asRequired()
                    .bind(Operation::getAccountPayer, Operation::setAccountPayer);
                binder.forField(recipientAccount)
                    .asRequired()
                    .bind(Operation::getAccountRecipient, Operation::setAccountRecipient);
                binder.removeBinding(payerClient);
                binder.removeBinding(payerDepartment);
                binder.removeBinding(recipientClient);
                binder.removeBinding(recipientDepartment);

                //entity.setPayer(null);
                //entity.setRecipient(null);

                payerClient.setRequiredIndicatorVisible(false);
                payerDepartment.setRequiredIndicatorVisible(false);
                recipientClient.setRequiredIndicatorVisible(false);
                recipientDepartment.setRequiredIndicatorVisible(false);
                break;
        }
    }

    private void addPhysicalAccounts(Currency currency, ComboBox<BaseAccount> accountField,
                                     PhysicalClientDetails clientDetails, BaseAccount oppositeAccount)
        throws AccountNotFoundException {
        UUID oppositeId = oppositeAccount == null ? null : oppositeAccount.getId();
        List<PhysicalAccount> accounts = physicalAccountService.getAccounts(clientDetails).stream()
            .filter(item -> !item.getId().equals(oppositeId) && item.getCurrency() == currency &&
                item.getTypeAccount() == AccountType.INNER_ACCOUNT)
            .collect(Collectors.toList());
        accountField.setItems(accounts.stream().map(BaseAccount.class::cast));

        PhysicalAccount account = accounts.stream()
            .max((o1, o2) -> Boolean.compare(o1.isDefaultAccount(), o2.isDefaultAccount()))
            .orElseThrow(() -> new AccountNotFoundException("exception.message.clientAccountNotFound"));
        accountField.setSelectedItem(account);
    }

    private void addDepartmentAccounts(Currency currency, ComboBox<BaseAccount> accountField,
                                       HasValue.ValueChangeEvent<Department> event, BaseAccount oppositeAccount)
        throws AccountNotFoundException {
        UUID oppositeId = oppositeAccount == null ? null : oppositeAccount.getId();
        List<DepartmentAccount> accounts = departmentAccountService.getAccounts(event.getValue()).stream()
            .filter(item -> !item.getId().equals(oppositeId) && item.getCurrency() == currency &&
                item.isCashAccount())
            .collect(Collectors.toList());
        accountField.setItems(accounts.stream().map(BaseAccount.class::cast));

        DepartmentAccount account = accounts.stream()
            .findFirst()
            .orElseThrow(() -> new AccountNotFoundException("exception.message.departmentCashAccountNotFound"));
        accountField.setSelectedItem(account);
    }

    private Layout getLoanInfo() {
        Label loanInfo = new Label(i18n.get("operation.panel.loanInfo.title"));

        Label loanIssueDepartment = new Label(String.format(
            i18n.get("operation.field.loanIssueDepartment"),
            entity == null ? "" : entity.getLoan().getDepartment().getName()));
        Label loanPaymentDepartment = new Label(String.format(
            i18n.get("operation.field.loanPaymentDepartment"),
            entity.getPayment() == null ? "" : entity.getPayment().getDepartmentPayment().getName()));

        TextField loanNumber = new TextField(i18n.get("operation.field.loanNumber"),
            entity == null ? "" : entity.getLoan().getLoanNumber());
        TextField paymentNumber = new TextField(i18n.get("operation.field.paymentNumber"),
            entity.getPayment() == null ? "" : String.valueOf(entity.getPayment().getOrdNo()));
        TextField paymentPart = new TextField(i18n.get("operation.field.paymentPart"),
            entity.getPaymentPart() == null ? "" : entity.getPaymentPart());
        paymentPart.setSizeFull();

        GridLayout block = new GridLayout(2, 1, loanNumber, paymentNumber);
        block.setSpacing(true);

        VerticalLayout layout = new VerticalLayout(loanInfo, loanIssueDepartment, loanPaymentDepartment, block, paymentPart);
        layout.addStyleName(MaterialTheme.CARD_2);
        layout.setMargin(false);
        layout.setSpacing(true);

        return layout;
    }

    private void buttonsStatusUpdate() {
        edit.setEnabled(entity.getId() != null && operationHashCode == entity.hashCode() &&
            entity.getStatus() == OperationStatus.PREPARED);

        prepare.setEnabled(binder.isValid() && operationHashCode != entity.hashCode());
        process.setEnabled(binder.isValid() && (operationHashCode != entity.hashCode() ||
            (operationHashCode == entity.hashCode() && entity.getStatus() == OperationStatus.PREPARED)));
        print.setEnabled(entity.getId() != null && operationHashCode == entity.hashCode() &&
            entity.getStatus() == OperationStatus.PROCESSED);
    }

    private void initInitialEntityState() {
        entity.setStatus(null);
        entity.setOperationDateTime(chronometerService.getCurrentInstant());
        entity.setDepartment(securityService.getDepartment());
        entity.setOperationValue(new BigDecimal(0));
        entity.setCurrency(configurationService.getCurrencyDefault());
        entity.setPaymentMethod(PaymentMethod.CASHLESS);
        entity.setPayerFixed(false);
        entity.setRecipientFixed(false);
    }

    @Subscribe
    private void updateSequenceEventHandler(UpdateSequenceEvent event) {
        if (entity.getId() != null)
            return;
        if (entity.getDepartment().getOperationSequence().getId().equals(event.getSequence().getId())) {
            binder.setBean(binder.getBean());
        }
    }


    @Override
    public String getTabName() {
        if (entity.getId() == null) {
            return i18n.get("operation.tab.add.caption");
        } else {
            return i18n.get("operation.tab.edit.caption");
        }
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIME;
    }
}

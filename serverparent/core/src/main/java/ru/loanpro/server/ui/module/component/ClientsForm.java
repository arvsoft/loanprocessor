package ru.loanpro.server.ui.module.component;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.service.AbstractClientService;
import ru.loanpro.base.loan.domain.AbstractRequest;
import ru.loanpro.eventbus.SessionEventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Форма отображения и работы с карточками клиента.
 *
 * @author Oleg Brizhevatikh
 */
public class ClientsForm extends VerticalLayout {

    private final I18N i18n;
    private final AbstractClientService clientService;
    private final SessionEventBus sessionEventBus;
    private final List<Client> guarantors;
    private final Binder<? extends AbstractRequest> binder;
    private final Button addGuarantor;
    private final HorizontalLayout hlLayout2;

    private Map<ClientCard, Client> map = new HashMap<>();

    public <T extends AbstractRequest> ClientsForm(T entity, Binder<T> binder, I18N i18n,
                                                   AbstractClientService clientService, SessionEventBus sessionEventBus) {

        this.binder = binder;
        this.i18n = i18n;
        this.clientService = clientService;
        this.sessionEventBus = sessionEventBus;

        // Карточки заёмщика и созаёмщика всегда находятся в форме
        ClientCard borrowerCard = new ClientCard(
            i18n.get("clientCard.borrower"), clientService, i18n, sessionEventBus);
        ClientCard coBorrowerCard = new ClientCard(
            i18n.get("clientCard.coBorrower"), clientService, i18n, sessionEventBus);

        binder.forField(borrowerCard)
            .asRequired(i18n.get("clientCard.borrower.error"))
            .bind(AbstractRequest::getBorrower, AbstractRequest::setBorrower);
        binder.bind(coBorrowerCard, AbstractRequest::getCoBorrower, AbstractRequest::setCoBorrower);

        HorizontalLayout hlLayout1 = new HorizontalLayout();
        hlLayout1.setSpacing(true);
        hlLayout1.addStyleName("flex-wrap-layout");
        hlLayout1.addComponents(borrowerCard, coBorrowerCard);

        addComponent(hlLayout1);

        hlLayout2 = new HorizontalLayout();
        hlLayout2.setSpacing(true);
        hlLayout2.addStyleName("flex-wrap-layout");

        // Карточки поручителей добавляются динамически при наличии поручителей
        guarantors = entity.getGuarantors();
        guarantors.forEach(item ->
            hlLayout2.addComponent(newGuarantorCard(i18n.get("clientCard.guarantor"), item)));

        // Кнопка добавления поручителя на форму
        addGuarantor = new Button(i18n.get("clientCard.guarantor.add"));
        addGuarantor.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        addGuarantor.setIcon(VaadinIcons.PLUS);

        addGuarantor.addClickListener(event -> {
            hlLayout2.addComponent(newGuarantorCard(i18n.get("clientCard.guarantor"), null), hlLayout2.getComponentCount() - 1);
        });

        VerticalLayout vlButton = new VerticalLayout();
        vlButton.setWidth(400, Unit.PIXELS);
        vlButton.setHeight(150, Unit.PIXELS);
        vlButton.addComponent(addGuarantor);
        vlButton.setComponentAlignment(addGuarantor, Alignment.MIDDLE_CENTER);

        hlLayout2.addComponent(vlButton);
        addComponents(hlLayout2);
        setSpacing(true);

    }

    private ClientCard newGuarantorCard(String title, Client client) {
        ClientCard card = new ClientCard(title, clientService, i18n, sessionEventBus);

        if (addGuarantor != null) addGuarantor.setEnabled(false);

        // Добавляем в мапу текущую карточку и поручителя
        map.put(card, client);
        binder.bind(card, request -> map.get(card),
            (request, client1) -> {
                // Удаляем поручителя из списка
                guarantors.remove(map.get(card));
                // Заменяем поручителя в мапе по карточке
                map.put(card, client1);
                // Если новый поручитель не null, добавляем его в список поручителей
                if (client1 != null)
                    guarantors.add(client1);
                addGuarantor.setEnabled(true);
            });

        // Обработка удаления поручителя
        card.addRemoveClickChangeListener(event -> {
            // Удаляем карточку с формы
            hlLayout2.removeComponent(card);

            // Удаляем поручителя из мапы и списка поручителей
            map.remove(card);
            guarantors.remove(map.get(card));

            addGuarantor.setEnabled(true);
        });
        return card;
    }
}

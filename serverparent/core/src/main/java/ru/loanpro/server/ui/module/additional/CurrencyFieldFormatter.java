package ru.loanpro.server.ui.module.additional;

import com.vaadin.ui.TextField;
import org.vaadin.textfieldformatter.NumeralFieldFormatter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Форматтер для {@link TextField}, обеспечивающий форматирование значения
 * текстового поля к валюте переданной локали.
 *
 * @author Oleg Brizhevatikh
 */
public class CurrencyFieldFormatter extends NumeralFieldFormatter {

    /**
     * Конструктор, принимающий локаль валюты, для которой требуется форматирование.
     *
     * @param locale локаль валюты.
     */
    public CurrencyFieldFormatter(Locale locale) {
        super(getSeparator1((DecimalFormat) NumberFormat.getCurrencyInstance(locale)),
            getSeparator2((DecimalFormat) NumberFormat.getCurrencyInstance(locale)),
            getScale((DecimalFormat) NumberFormat.getCurrencyInstance(locale)));
    }

    /**
     * Конструктор, принимающий локаль валюты и количество знаков дробной части.
     *
     * @param locale локаль вылюты.
     * @param decimalScale количество знаков дробной части.
     */
    public CurrencyFieldFormatter(Locale locale, int decimalScale) {
        super(getSeparator1((DecimalFormat) NumberFormat.getCurrencyInstance(locale)),
            getSeparator2((DecimalFormat) NumberFormat.getCurrencyInstance(locale)),
            decimalScale);
    }

    /**
     * Конструктор, принимающий разделители и количество знаков дробной части.
     *
     * @param delimiter разделитеть тысячной части.
     * @param decimalMark разделитель основной и дробной части.
     * @param decimalScale количество знаков дробной части.
     */
    public CurrencyFieldFormatter(String delimiter, String decimalMark, int decimalScale) {
        super(delimiter, decimalMark, decimalScale);
    }

    /**
     * Возвращает разделитель тысячной части из денежного формата.
     *
     * @param format формат.
     * @return разделитель.
     */
    protected static String getSeparator1(DecimalFormat format) {
        DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();

        return String.valueOf(symbols.getGroupingSeparator());
    }

    /**
     * Возвращает разделитель дробной части из денежного формата.
     *
     * @param format формат.
     * @return разделитель.
     */
    protected static String getSeparator2(DecimalFormat format) {
        DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();

        return String.valueOf(symbols.getDecimalSeparator());
    }

    /**
     * Возвращает количество знаков дробной части из денежного формата.
     *
     * @param format формат.
     * @return количество знаков дробной части.
     */
    protected static int getScale(DecimalFormat format) {
        return format.getMaximumFractionDigits();
    }
}

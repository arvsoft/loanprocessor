package ru.loanpro.server.logging.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ru.loanpro.server.logging.annotation.LogName;

/**
 * Лог авторизации пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("LOGIN")
@LogName("log.login")
public class LogLogin extends AbstractLog {
}

package ru.loanpro.server.logging.service;

import com.vaadin.server.VaadinSession;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.event.EntityEditingEvent;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PreDestroy;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@EnableAspectJAutoProxy
@Configuration
@Service
public class LogConfiguration {

    private final ApplicationContext context;
    private final LogService service;

    private Map<Integer, Pair<User, String>> sessionUserMap = new HashMap<>();

    @Autowired
    public LogConfiguration(ApplicationContext context, LogService service) {
        this.context = context;
        this.service = service;
    }

    @PreDestroy
    public void destroy() {
        sessionUserMap.forEach((session, pair) -> service.logLogout(pair.getKey(), session, pair.getValue()));
    }

    @AfterReturning(pointcut = "execution(* org.vaadin.spring.security.VaadinSecurity.getAuthentication())",
        returning = "result")
    public void login(Authentication result) {
        VaadinSession session = VaadinSession.getCurrent();
        String ip = session.getBrowser().getAddress();

        Integer sessionHash = session.hashCode();

        User user = (User) result.getPrincipal();
        sessionUserMap.put(sessionHash, new Pair<>(user, ip));

        service.logLogin(user, sessionHash, ip);
    }

    @Before("execution(* org.vaadin.spring.security.VaadinSecurity.logout())")
    public void logout() {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);
        sessionUserMap.remove(session);
        service.logLogout(pair.getKey(), session, pair.getValue());
    }

    @Before("execution(* ru.loanpro.uibasis.component.TabList.addTab(..)) && args(tab, objectList, ..)")
    public void openTab(BaseTab tab, List<Object> objectList) {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);

        if (tab.getClass().isAnnotationPresent(SingletonTab.class)) {
            service.logOpenTab(pair.getKey(), session, pair.getValue(), tab);
        } else {
            service.logOpenEntity(pair.getKey(), session, pair.getValue(), tab, objectList);
        }
    }

    @Before("execution(* ru.loanpro.uibasis.component.TabList.removeTab(..)) && args(tab)")
    public void closeTab(BaseTab tab) {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);

        if (tab.getClass().isAnnotationPresent(SingletonTab.class)) {
            service.logCloseTab(pair.getKey(), session, pair.getValue(), tab);
        } else {
            service.logEntityClose(pair.getKey(), session, pair.getValue(), tab);
        }
    }

    @Pointcut("execution(* ru.loanpro.base.*.service.*.save(..))")
    public void entitySaveBase() {}

    @Pointcut("execution(* ru.loanpro.security.service.UserService.save(..))")
    public void entitySaveSecurity() {}

    @Pointcut("execution(* ru.loanpro.sequence.service.SequenceService.save(..))")
    public void entitySaveSequence() {}

    @AfterReturning(pointcut = "entitySaveBase() || entitySaveSecurity() || entitySaveSequence()",
        returning = "result")
    public void entitySave(AbstractIdEntity result) {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);
        service.logEntitySave(pair.getKey(), session, pair.getValue(), result);
    }

    @Before("execution(* com.google.common.eventbus.EventBus.post(..)) && args(event)")
    public void entityEdit(EntityEditingEvent event) {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);

        service.logEntityEdit(pair.getKey(), session, pair.getValue(), event.getEntity());
    }

    @Around("execution(* ru.loanpro.base.printer.PrintService.printTemplate(..))")
    public String entityPrint(ProceedingJoinPoint joinPoint) {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);

        Object[] args = joinPoint.getArgs();
        String fileName = null;
        try {
            fileName = (String) joinPoint.proceed(args);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        service.logEntityPrint(pair.getKey(), session, pair.getValue(), (AbstractIdEntity) args[1], fileName);

        return fileName;
    }

    @Before("execution(* ru.loanpro.security.service.ChronometerService.setShiftDateTime(..)) " +
        "&& args(targetDateTime, ..)")
    public void timeShift(LocalDateTime targetDateTime) {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);
        ConfigurationService configurationService = context.getBean(ConfigurationService.class);

        service.logTimeShift(pair.getKey(), session, pair.getValue(),
            configurationService.getZoneId(), targetDateTime);
    }

    @Before("execution(* ru.loanpro.security.service.ChronometerService.clearShiftDateTime())")
    public void timeShiftClear() {
        Integer session = VaadinSession.getCurrent().hashCode();
        Pair<User, String> pair = sessionUserMap.get(session);

        service.logTimeShiftClear(pair.getKey(), session, pair.getValue());
    }
}

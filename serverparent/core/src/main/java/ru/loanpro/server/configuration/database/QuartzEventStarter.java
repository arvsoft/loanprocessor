package ru.loanpro.server.configuration.database;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import ru.loanpro.base.schedule.service.ScheduleService;

import java.time.LocalDate;

/**
 * Конфигурация системы, обеспечивающая выполение кода при старте системы и по расписанию.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
public class QuartzEventStarter implements InitializingBean {

    private final ScheduleService scheduleService;

    @Autowired
    public QuartzEventStarter(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    /**
     * Вызывает методы, после старта системы.
     */
    @Override
    public void afterPropertiesSet() {
        // Обновление статусов просроченных платежей и займов
        scheduleService.updateOverdueScheduleStatuses(LocalDate.now());
    }

    /**
     * Вызывается ежедневно в 00:00:01. Обновляет статусы просроченных платежей и займов.
     */
    @Scheduled(cron = "1 0 0 * * *")
    public void updateOverdueSchedulePayments() {
        scheduleService.updateOverdueScheduleStatuses(LocalDate.now());
    }
}

package ru.loanpro.server.configuration.database;

import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Конфигурационный файл, настраивающий компоненты и модули базы данных в
 * зависимости от конкретной базы данных.
 *
 * @author Oleg Brizhevatikh
 */
@Configuration
public class DatabaseVendorConfiguration {

    @Value("${system.profile}")
    private String profile;

    /**
     * Выбирает стратегию именования схем, таблиц и полей базы данных в
     * зависимости от используемой базы данных.
     *
     * @return стратегия именования схем, таблиц и полей базы данных.
     */
    @Bean
    public PhysicalNamingStrategy getPhysicalNamingStrategy() {
        if (profile.equals("MySQL"))
            return new MySQLPhysicalNamingStrategy();
        else
            return new SpringPhysicalNamingStrategy();
    }
}

package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Note;

import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования заметок о физическом лице
 *
 * @author Maksim Askhaev
 */
public class NoteDetailsLayout extends HorizontalLayout {
    private final Set<Note> notes;
    private final I18N i18n;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Note> grid;
    private ContentChangeListener listener;

    public NoteDetailsLayout(Set<Note> notes, I18N i18n) {

        this.i18n = i18n;
        this.notes = notes;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(notes);
        grid.addColumn(Note::getContent).setCaption(i18n.get("client.notedetails.field.content"));

        grid.addItemClickListener(event -> {
            closeForm();

            Note item = event.getItem();
            if (item != null) {
                form = getNoteForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.notedetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Note note = new Note();
            form = getNoteForm(note);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getNoteForm(Note note) {

        TextField content = new TextField(i18n.get("client.notedetails.field.content"));

        Button save = new Button(i18n.get("client.notedetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.notedetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(content, layout);
        formLayout.setSizeFull();

        Binder<Note> binder = new Binder<>();

        binder.forField(content)
            .bind(Note::getContent, Note::setContent);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(note);

        save.addClickListener(event -> {
            if (notes.stream().noneMatch(item -> item.hashCode() == note.hashCode()))
                notes.add(note);

            grid.setItems(notes);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}



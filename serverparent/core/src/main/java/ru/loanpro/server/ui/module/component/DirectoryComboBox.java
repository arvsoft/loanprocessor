package ru.loanpro.server.ui.module.component;

import com.vaadin.ui.ComboBox;
import ru.loanpro.directory.domain.AbstractIdDirectory;
import ru.loanpro.directory.service.DirectoryService;

import java.util.Optional;

/**
 * Выпадающий список справочных сущностей. Принимает в качестве параметов объект
 * справочной сущности {@link ru.loanpro.directory.domain} и соответствующий
 * данному объекту сервис {@link ru.loanpro.directory.service}. Формирует список
 * сущностей из сервиса и отображает его в виде выпадающего списка {@link ComboBox}.
 *
 * @param <D> Справочная сущность.
 * @param <S> Сервис справочной сущности.
 *
 * @author Oleg Brizhevatikh
 */
public class DirectoryComboBox<D extends AbstractIdDirectory, S extends DirectoryService> extends ComboBox<D> {

    private final S service;

    private Optional<D> entity;

    /**
     * Конструктор выпадающего списка справочных сущностей.
     *
     * @param caption Название объекта для отображения на форме.
     * @param service Инстанс сервиса.
     */
    public DirectoryComboBox(String caption, S service) {
        this(caption, service, null);
    }

    /**
     * Конструктор выпадающего списка справочных сущностей.
     *
     * @param caption Название объекта для отображения на форме.
     * @param service Инстанс сервиса.
     * @param entity Инстанс справочной сущности для начального выбора её в
     *               выпадающем списке.
     */
    public DirectoryComboBox(String caption, S service, D entity) {
        super(caption);
        this.service = service;
        this.entity = Optional.ofNullable(entity);

        setItemCaptionGenerator(D::getName);
        addSelectionListener(e -> this.entity = Optional.ofNullable(e.getValue()));
    }

    /**
     * Геттер для справочной сущности.
     *
     * @return Возвращает справочную сущность или null, если сущность не выбрана.
     */
    public D getEntity() {
        return entity.get();
    }

    /**
     * Сеттер для справочной сущности.
     *
     * @param entity Принимает справочную сущность для автоматического выбора в
     *               выпадающем списке или null, если требуется выбрать пустое
     *               значение.
     */
    public void setEntity(D entity) {
        this.entity = Optional.ofNullable(entity);
    }

    @Override
    public void attach() {
        super.attach();
        setItems(service.getAll());
        entity.ifPresent(d -> setSelectedItem(d));
    }
}

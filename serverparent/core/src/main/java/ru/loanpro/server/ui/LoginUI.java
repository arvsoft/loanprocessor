package ru.loanpro.server.ui;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.security.shared.VaadinSharedSecurity;
import ru.loanpro.global.Settings;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.security.settings.service.SettingsService;
import ru.loanpro.uibasis.AbstractLoginUI;

/**
 * Интерфейс авторизации.
 *
 * @author Oleg Brizhevatikh
 */
@SpringUI(path = "/login")
public class LoginUI extends AbstractLoginUI {

    private final SettingsService settingsService;

    @Autowired
    public LoginUI(VaadinSharedSecurity vaadinSecurity, I18N i18N, SettingsService settingsService) {
        super(vaadinSecurity, i18N);
        this.settingsService = settingsService;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setTheme(settingsService.get(UiTheme.class, Settings.THEME, UiTheme.DARK).getTheme());
        setLocale(settingsService.get(SystemLocale.class, Settings.LOCALE, SystemLocale.ENGLISH).getLocale());
        getPage().setTitle(i18N.get("title"));
        addStyleName("colorful");

        super.init(vaadinRequest);
    }
}

package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.base.NumberService;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.domain.JuridicalClient;
import ru.loanpro.base.client.domain.JuridicalClientDetails;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.client.repository.ClientRepository;
import ru.loanpro.base.client.service.AbstractClientService;
import ru.loanpro.base.printer.PrintService;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.directory.service.DocumentTypeService;
import ru.loanpro.directory.service.EducationLevelService;
import ru.loanpro.directory.service.MaritalStatusService;
import ru.loanpro.directory.service.RelativeTypeService;
import ru.loanpro.global.directory.AccountType;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.global.directory.Gender;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.client.details.ContentChangeListener;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

@NoRepositoryBean
public abstract class ClientTab<T extends Client, R extends ClientRepository<T>> extends BaseTab
    implements ContentChangeListener {

    @Autowired
    protected AbstractClientService<T, R> service;

    @Autowired
    protected MaritalStatusService maritalStatusService;

    @Autowired
    protected DocumentTypeService documentTypeService;

    @Autowired
    protected EducationLevelService educationLevelService;

    @Autowired
    protected RelativeTypeService relativeTypeService;

    @Autowired
    protected ConfigurationService configurationService;

    @Autowired
    protected StorageService storageService;

    @Autowired
    protected NumberService numberService;

    @Autowired
    protected PrintService printService;

    protected Binder<T> binder;

    protected T entity;
    protected int entityHash;

    protected TabSheet tabsheet = new TabSheet();

    protected abstract Layout initBaseInformation();

    @PostConstruct
    public void init() {
        if (entity.getId() == null)
            initInitialEntityState();
        else
            entity = service.getOneFull(entity.getId());

        binder = new Binder<>();
        binder.setBean(entity);
        entityHash = entity.hashCode();

        Layout baseLayout = initBaseInformation();
        baseLayout.setSizeFull();
        baseLayout.addStyleName(MaterialTheme.CARD_2);

        tabsheet.setSizeFull();
        tabsheet.addStyleNames(ValoTheme.TABSHEET_FRAMED, ValoTheme.TABSHEET_PADDED_TABBAR);

        addComponents(baseLayout, tabsheet);
        setExpandRatio(tabsheet, 1);
    }

    private void initInitialEntityState() {
        // Предварительная настройка новой сущности. Поля должны браться из настроек.
        if (entity instanceof PhysicalClient) {
            PhysicalClient client = (PhysicalClient) entity;
            client.setRating(ClientRating.NEW);
            client.setBirthDate(LocalDate.now().minus(Period.ofYears(18)));
            client.setGender(Gender.MALE);
            client.setRegistrationDate(chronometerService.getCurrentTime().toLocalDate());

            PhysicalClientDetails details = new PhysicalClientDetails();
            details.setYearIncome(new BigDecimal(0));
            client.setDetails(details);
            client.getDetails().setMaritalStatus(maritalStatusService.getAll().get(0));

            //генерируем предварительный (draft) номер счета клиента
            PhysicalAccount account = new PhysicalAccount();
            //получаем грязный номер
            account.setNumber(numberService.getAccountSequenceDraftNumber());
            account.setBalance(new BigDecimal(0));
            account.setTypeAccount(AccountType.INNER_ACCOUNT);
            account.setRegistrationDate(chronometerService.getCurrentTime().toLocalDate());
            account.setCurrency(configurationService.getCurrencies().get(0));
            account.setDefaultAccount(true);
            client.getDetails().getAccounts().add(account);
        }

        if (entity instanceof JuridicalClient) {
            JuridicalClient client = (JuridicalClient) entity;
            client.setRegistrationDate(chronometerService.getCurrentTime().toLocalDate());

            JuridicalClientDetails details = new JuridicalClientDetails();
            client.setDetails(details);
        }
    }

}

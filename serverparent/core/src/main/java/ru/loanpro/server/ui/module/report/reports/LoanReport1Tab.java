package ru.loanpro.server.ui.module.report.reports;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.client.service.PhysicalClientService;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.PaymentCalculator;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.base.report.domain.reports.LoanReport1;
import ru.loanpro.global.UiTheme;
import ru.loanpro.server.ui.module.report.BaseReportTab;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Окно отчета по займам.
 * Отчет о просроченных займах.
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
public class LoanReport1Tab extends BaseReportTab {

    @Autowired
    PhysicalClientService physicalClientService;

    private int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    private int scaleValue = 2;

    private Binder<OverdueDays> binder;
    private Grid<LoanReport1> grid;
    private TextField tfFrom;
    private TextField tfTo;
    private CheckBox cbxDays;
    private Button prepare;

    private LoanReport1 loanReport;
    private List<LoanReport1> loanReport1List;

    public LoanReport1Tab(Report report) {
        super(report);
    }

    @PostConstruct
    @Override
    public void init() {
        super.init();
    }

    @Override
    protected String getLabelDateDescription() {
        return i18n.get("reports.loan.report1.field.dateDescription");
    }

    @Override
    protected boolean getVisibleBaseSearch() { return true;}

    @Override
    protected Layout initReportInformation() {

        binder = new Binder<>();

        cbxDays = new CheckBox(i18n.get("reports.loan.report1.checkbox.days"));
        cbxDays.addStyleName(ValoTheme.CHECKBOX_SMALL);

        Label lbFrom = new Label(i18n.get("reports.loan.report1.field.from"));
        Label lbTo = new Label(i18n.get("reports.loan.report1.field.to"));
        tfFrom = new TextField();
        tfFrom.setWidth(50, Unit.PIXELS);
        tfTo = new TextField();
        tfTo.setWidth(50, Unit.PIXELS);
        tfFrom.setReadOnly(true);
        tfTo.setReadOnly(true);

        cbxDays.addValueChangeListener(e -> {
            tfFrom.setReadOnly(!e.getValue());
            tfTo.setReadOnly(!e.getValue());
            tfFrom.focus();

            if (e.getValue()) {
                binder.forField(tfFrom)
                    .asRequired()
                    .withConverter(new StringToLongConverter("Must be a number"))
                    .bind(OverdueDays::getDaysFrom, OverdueDays::setDaysFrom);

                binder.forField(tfTo)
                    .asRequired()
                    .withConverter(new StringToLongConverter("Must be a number"))
                    .bind(OverdueDays::getDaysTo, OverdueDays::setDaysTo);
            } else {
                binder.removeBinding(tfFrom);
                binder.removeBinding(tfTo);
            }

            buttonStatusUpdate();
        });

        tfFrom.addValueChangeListener(e -> {
            buttonStatusUpdate();
        });

        tfTo.addValueChangeListener(e -> {
            buttonStatusUpdate();
        });

        GridLayout filterLayout1 = new GridLayout();
        filterLayout1.setSpacing(true);
        filterLayout1.removeAllComponents();
        filterLayout1.setColumns(5);
        filterLayout1.setRows(1);
        filterLayout1.addComponent(cbxDays, 0, 0);
        filterLayout1.addComponent(lbFrom, 1, 0);
        filterLayout1.addComponent(tfFrom, 2, 0);
        filterLayout1.addComponent(lbTo, 3, 0);
        filterLayout1.addComponent(tfTo, 4, 0);

        com.vaadin.ui.Component gridCell;
        for (int i = 0; i < 5; i++) {
            gridCell = filterLayout1.getComponent(i, 0);
            if (gridCell != null) filterLayout1.setComponentAlignment(gridCell, Alignment.MIDDLE_CENTER);
        }

        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.addStyleName(MaterialTheme.CARD_2);
        layout.setMargin(false);
        layout.setSpacing(true);

        grid = new Grid<>();
        grid.setSizeFull();
        gridColumnBuild();

        layout.addComponents(filterLayout1, grid);
        layout.setExpandRatio(grid, 1);

        layout.setSizeFull();

        return layout;
    }

    protected Layout getActionLayout(Report report) {
        Label mainInfo = new Label(i18n.get(report.getName()));

        prepare = new Button(i18n.get("reporttab.button.prepare"), VaadinIcons.LINE_BAR_CHART);
        prepare.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        HorizontalLayout actionLayout = new HorizontalLayout(mainInfo, prepare);
        actionLayout.setWidth(100, Unit.PERCENTAGE);
        actionLayout.setExpandRatio(mainInfo, 1);

        prepare.addClickListener(e -> {
            boolean overdueDays = cbxDays.getValue();
            long daysFrom = 0, daysTo = 0;

            boolean process = true;

            if (overdueDays) {
                try {
                    daysFrom = Long.valueOf(tfFrom.getValue());
                    daysTo = Long.valueOf(tfTo.getValue());
                } catch (Exception ex) {
                    process = false;
                    Notification.show(i18n.get("loanReport1.exeption1.message"), Notification.Type.ERROR_MESSAGE);
                    ex.printStackTrace();
                }
            }

            if (process) {
                loanReport1List = new ArrayList<>();

                //получаем список просроченных займов
                List<Loan> loanList = getOverdueLoans();

                //организуем цикл по каждому займу
                for (Loan loan : loanList) {
                    loan = loanService.getOneFull(loan.getId());//в целях инициализации lazy-полей

                    List<Payment> paymentList = calculatePayments(loan);

                    if (paymentList.size() > 0) {
                        int k = 1;
                        for (Payment payment : paymentList) {

                            if (overdueDays) {
                                if (payment.getOverdueTerm() >= daysFrom && payment.getOverdueTerm() <= daysTo)
                                    process = true;
                                else process = false;
                            }

                            if (process) {
                                loanReport = new LoanReport1();
                                loanReport.setLoanDepartment(loan.getDepartment());
                                loanReport.setLoanNumber(loan.getLoanNumber());
                                loanReport.setLoanIssueDate(LocalDateTime.ofInstant(loan.getLoanIssueDate(),
                                    chronometerService.getCurrentZoneId()).toLocalDate());
                                loanReport.setLoanBalance(loan.getLoanBalance());
                                loanReport.setCurrentBalance(loan.getCurrentBalance());
                                loanReport.setScheduleCount(paymentList.size());
                                loanReport.setScheduleOrdNo(k);
                                loanReport.setOverdueDays(payment.getOverdueTerm());
                                loanReport.setScheduleDate(payment.getPayDate());
                                loanReport.setIndebtednessPrincipal(payment.getPrincipal());
                                loanReport.setIndebtednessInterest(payment.getInterest());
                                loanReport.setIndebtednessPenalty(payment.getPenalty());
                                loanReport.setIndebtednessFine(payment.getFine());
                                loanReport.setIndebtednessSum(payment.getPrincipal().add(
                                    payment.getInterest().add(
                                        payment.getPenalty().add(
                                            payment.getFine()))));

                                PhysicalClient client = physicalClientService.getOneFull(loan.getBorrower().getId());
                                loanReport.setBorrower(client.getDisplayName());
                                PhysicalClientDetails details = client.getDetails();
                                loanReport.setBorrowerDetails(String.format("%s %s %s %s",
                                    details.getMobilePhone() == null ? "" : details.getMobilePhone(),
                                    details.getHomePhone() == null ? "" : details.getHomePhone(),
                                    details.getWorkPhone() == null ? "" : details.getWorkPhone(),
                                    details.getEmail() == null ? "" : details.getEmail()).trim());

                                loanReport1List.add(loanReport);
                            }
                            k++;
                        }
                    }
                }
                grid.setItems(loanReport1List);
            }
        });

        return actionLayout;
    }

    /**
     * Вычисляет автоматический платеж по умолчанию исходя из свойств займа, графиков платежей и текущей даты.
     *
     * @return - новый автоматический платеж
     */
    private List<Payment> calculatePayments(Loan loan) {

        PaymentCalculator paymentCalculator = PaymentCalculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setLoanIssueDate(
                LocalDateTime.ofInstant(loan.getLoanIssueDate(), chronometerService.getCurrentZoneId()).toLocalDate())
            .setLoanBalance(loan.getLoanBalance())
            .setReplaceDate(loan.isReplaceDate())
            .setReplaceDateValue(loan.getReplaceDateValue())
            .setReplaceOverMonth(loan.isReplaceOverMonth())
            .setPrincipalLastPay(loan.isPrincipalLastPay())
            .setInterestBalance(loan.isInterestBalance())
            .setNoInterestFD(loan.isNoInterestFD())
            .setNoInterestFDValue(loan.getNoInterestFDValue())
            .setIncludeIssueDate(loan.isIncludeIssueDate())
            .setDaysInYear(365)
            .setCalcScheme(loan.getCalcScheme())
            .setReturnTerm(loan.getReturnTerm())
            .setInterestRate(loan.getInterestValue())
            .setInterestRateType(loan.getInterestType())
            .setLoanTerm(loan.getLoanTerm())
            .setReplaceDate(loan.isReplaceDate())
            .setReplaceDateValue(loan.getReplaceDateValue())
            .setReplaceOverMonth(loan.isReplaceOverMonth())
            .setSummationFines(configurationService.getSummationFine())
            .setSummationPenalties(configurationService.getSummationPenalty())
            .setOverdueDaysCalculationType(configurationService.getOverdueDaysCalculationType())
            .setCalculationDate(chronometerService.getCurrentTime().toLocalDate())
            .setPenaltyType(loan.getPenaltyType())
            .setPenaltyValue(loan.getPenaltyValue())
            .setFine(loan.isChargeFine())
            .setFineValue(loan.getFineValue())
            .setFineOneTime(loan.isFineOneTime())
            .setInterestStrictly(loan.isInterestStrictly())
            .setInterestOverdue(loan.isInterestOverdue())
            .build();
        paymentCalculator.setSchedules(loan.getSchedules());
        paymentCalculator.setPayments(loan.getPayments());

        return paymentCalculator.calculateOverduePayments();
    }

    private class OverdueDays {
        private long daysFrom;
        private long daysTo;

        public OverdueDays() {
        }

        public long getDaysFrom() {
            return daysFrom;
        }

        public void setDaysFrom(long daysFrom) {
            this.daysFrom = daysFrom;
        }

        public long getDaysTo() {
            return daysTo;
        }

        public void setDaysTo(long daysTo) {
            this.daysTo = daysTo;
        }
    }

    private List<Loan> getOverdueLoans() {

        if (!cbxUseDepartment.getValue() && !cbxUseMainDate.getValue()) {
            return reportService.findOverdueLoans();
        }

        if (cbxUseDepartment.getValue() && !cbxUseMainDate.getValue()) {
            return reportService.findOverdueLoansByDepartment(cbDepartment.getValue());
        }

        if (!cbxUseDepartment.getValue() && cbxUseMainDate.getValue()) {
            Instant minDate = dfDateMin.getValue().atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
            Instant maxDate = dfDateMax.getValue().plusDays(1).atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();

            return reportService.findOverdueLoansByDates(minDate, maxDate);
        }

        if (cbxUseDepartment.getValue() && cbxUseMainDate.getValue()) {
            Instant minDate = dfDateMin.getValue().atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
            Instant maxDate = dfDateMax.getValue().plusDays(1).atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();

            return reportService.findOverdueLoansByDepartmentAndDates(cbDepartment.getValue(), minDate, maxDate);
        }

        return new ArrayList<>();
    }

    private void buttonStatusUpdate() {
        if (cbxDays.getValue()) {
            prepare.setEnabled(binder.isValid());
        } else {
            prepare.setEnabled(true);
        }
    }

    private void gridColumnBuild() {
        grid.addColumn(item -> item.getLoanDepartment().getName())
            .setCaption(i18n.get("report.loanReport1.grid.title.loanDepartment"));

        grid.addColumn(LoanReport1::getLoanNumber)
            .setCaption(i18n.get("report.loanReport1.grid.title.loanNumber"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> item.getLoanIssueDate().format(dateTimeFormatter))
            .setCaption(i18n.get("report.loanReport1.grid.title.issuedate"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> moneyFormat.format(item.getLoanBalance()))
            .setCaption(i18n.get("report.loanReport1.grid.title.loanBalance"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> moneyFormat.format(item.getCurrentBalance()))
            .setCaption(i18n.get("report.loanReport1.grid.title.currentBalance"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(LoanReport1::getScheduleCount)
            .setCaption(i18n.get("report.loanReport1.grid.title.scheduleCount"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(LoanReport1::getScheduleOrdNo)
            .setCaption(i18n.get("report.loanReport1.grid.title.scheduleOrdNo"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> item.getScheduleDate().format(dateTimeFormatter))
            .setCaption(i18n.get("report.loanReport1.grid.title.scheduleDate"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(LoanReport1::getOverdueDays)
            .setCaption(i18n.get("report.loanReport1.grid.title.overdueDays"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> moneyFormat.format(item.getIndebtednessPrincipal()))
            .setCaption(i18n.get("report.loanReport1.grid.title.indebtednessPrincipal"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> moneyFormat.format(item.getIndebtednessInterest()))
            .setCaption(i18n.get("report.loanReport1.grid.title.indebtednessInterest"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> moneyFormat.format(item.getIndebtednessPenalty()))
            .setCaption(i18n.get("report.loanReport1.grid.title.indebtednessPenalty"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> moneyFormat.format(item.getIndebtednessFine()))
            .setCaption(i18n.get("report.loanReport1.grid.title.indebtednessFine"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> moneyFormat.format(item.getIndebtednessSum()))
            .setCaption(i18n.get("report.loanReport1.grid.title.indebtednessSum"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(LoanReport1::getBorrower)
            .setCaption(i18n.get("report.loanReport1.grid.title.borrower"));

        grid.addColumn(LoanReport1::getBorrowerDetails)
            .setCaption(i18n.get("report.loanReport1.grid.title.borrowerDetails"));
    }

    @Override
    public String getTabName() {
        return i18n.get(report.getName());
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIME;
    }
}

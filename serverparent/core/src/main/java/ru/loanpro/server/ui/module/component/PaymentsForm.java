package ru.loanpro.server.ui.module.component;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.LoanPaymentTab;
import ru.loanpro.uibasis.event.AddTabEvent;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class PaymentsForm extends VerticalLayout {

    private final DateTimeFormatter dateFormatter;

    private Grid<Payment> grid = new Grid<>();

    public PaymentsForm(I18N i18n, SessionEventBus sessionEventBus, ConfigurationService configurationService) {

        dateFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());

        Button show = new Button(i18n.get("loan.payment.button.show"), VaadinIcons.BROWSER);
        show.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        show.setEnabled(false);

        HorizontalLayout hlAction = new HorizontalLayout();
        hlAction.setWidth(100, Unit.PERCENTAGE);
        hlAction.addComponent(show);
        hlAction.setComponentAlignment(show, Alignment.TOP_RIGHT);

        grid.setSizeFull();
        grid.addColumn(Payment::getOrdNo).setCaption(i18n.get("loan.payment.grid.title.order"));
        grid.addColumn(Payment::getProlongOrdNo).setCaption(i18n.get("loan.payment.grid.title.prolongOrder"));
        grid.addColumn(
            item -> i18n.get(item.getStatus().getName())).setCaption(i18n.get("loan.payment.grid.title.status"));
        grid.addColumn(item -> dateFormatter.format(item.getPayDate())).setCaption(i18n.get("loan.payment.grid.title.date"));
        grid.addColumn(Payment::getTerm).setCaption(i18n.get("loan.payment.grid.title.term"));
        grid.addColumn(Payment::getPrincipal).setCaption(i18n.get("loan.payment.grid.title.principal"));
        grid.addColumn(Payment::getInterest).setCaption(i18n.get("loan.payment.grid.title.interest"));
        grid.addColumn(Payment::getPenalty).setCaption(i18n.get("loan.payment.grid.title.penalty"));
        grid.addColumn(Payment::getFine).setCaption(i18n.get("loan.payment.grid.title.fine"));
        grid.addColumn(Payment::getPayment).setCaption(i18n.get("loan.payment.grid.title.payment"));
        grid.addColumn(Payment::getBalanceAfter).setCaption(i18n.get("loan.payment.grid.title.balance"));

        grid.addSelectionListener(event ->
            show.setEnabled(event.getFirstSelectedItem().isPresent()));

        show.addClickListener(event -> {
            Payment payment = grid.asSingleSelect().getValue();

            Loan loan = payment.getLoan();
            sessionEventBus.post(new AddTabEvent(LoanPaymentTab.class, loan, payment));
        });

        addComponents(hlAction, grid);
        setSizeFull();
    }

    public void setItems(List<Payment> paymentList){
        grid.setItems(paymentList);
    }
}

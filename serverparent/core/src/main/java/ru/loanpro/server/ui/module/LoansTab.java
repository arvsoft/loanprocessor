package ru.loanpro.server.ui.module;

import com.google.common.collect.ImmutableSet;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.MenuItem;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.client.domain.ClientQueryDSL;
import ru.loanpro.base.loan.details.LoanNote;
import ru.loanpro.base.loan.domain.AbstractLoan;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.domain.QLoan;
import ru.loanpro.base.loan.repository.LoanRepository;
import ru.loanpro.base.loan.service.LoanNoteService;
import ru.loanpro.base.loan.service.LoanService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.domain.BaseQueryDSL;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.component.ContentChangeListener;
import ru.loanpro.server.ui.module.component.LoanNotesForm;
import ru.loanpro.uibasis.event.AddTabEvent;

import java.util.Collection;
import java.util.List;

/**
 * Вкладка для работы с займами.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.loans")
public class LoansTab extends BaseLoansTab<Loan> implements ContentChangeListener {

    private final DepartmentService departmentService;
    private final LoanNoteService loanNoteService;

    @Visible(Roles.Loan.PAYMENT_CALCULATION)
    private Button payment;

    private TextField loanNumber;
    private ComboBox<LoanStatus> loanStatus;
    private DateField loanIssueDate;
    private TextField clientName;
    private MoneyField loanBalance;
    private MoneyField currentBalance;
    private ComboBox<Department> department;

    @Autowired
    public LoansTab(LoanService loanService, DepartmentService departmentService, LoanNoteService loanNoteService) {
        super(loanService);
        this.departmentService = departmentService;
        this.loanNoteService = loanNoteService;
    }

    @Override
    protected void gridColumnsBuild() {
        float k = 1.0f;
        if (configurationService.getGridStyle() == GridStyle.SMALL) {
            grid.addStyleName("v-grid-style-small");
            grid.setRowHeight(40f);
        } else k = 1.3f;

        Grid.Column<Loan, String> colDepartment = grid.addColumn(item -> item.getDepartment().getName());
        colDepartment
            .setSortProperty("department")
            .setCaption(i18n.get("loans.grid.title.department"))
            .setWidth(250*k);

        Grid.Column<Loan, String> colLoanStatus = grid.addColumn(item -> i18n.get(item.getLoanStatus().getName()));
        colLoanStatus
            .setSortProperty("loanStatus")
            .setCaption(i18n.get("loans.grid.title.loanstatus"))
            .setStyleGenerator(item -> item.getLoanStatus().getColor())
            .setWidth(150*k);

        Grid.Column<Loan, String> colLoanNumber = grid.addColumn(Loan::getLoanNumber);
        colLoanNumber
            .setSortProperty("loanNumber")
            .setCaption(i18n.get("loans.grid.title.loannumber"))
            .setWidth(100*k);

        Grid.Column<Loan, String> colLoanIssueDate = grid.addColumn(item ->
            chronometerService.zonedDateTimeToString(item.getLoanIssueDate(), dateTimeFormatter));
        colLoanIssueDate
            .setSortProperty("loanIssueDate")
            .setCaption(i18n.get("loans.grid.title.issuedate"))
            .setWidth(120*k);

        Grid.Column<Loan, String> colClientName = grid.addColumn(item -> item.getBorrower().getDisplayName());
        colClientName
            .setSortProperty("loanIssueDate")
            .setCaption(i18n.get("loans.grid.title.borrower"))
            .setWidth(250*k);

        Grid.Column<Loan, String> colLoanBalance = grid.addColumn(item -> moneyFormat.format(item.getLoanBalance()));
        colLoanBalance
            .setSortProperty("loanBalance")
            .setCaption(i18n.get("loans.grid.title.loanbalance"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(120*k);

        Grid.Column<Loan, String> colCurrentBalance = grid.addColumn(item ->
            item.getCurrentBalance() == null ? "" : moneyFormat.format(item.getCurrentBalance()));
        colCurrentBalance
            .setSortProperty("currentBalance")
            .setCaption(i18n.get("loans.grid.title.currentbalance"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(120*k);

        Grid.Column<Loan, String> colCurrency = grid.addColumn(item -> item.getCurrency().getCurrencyCode());
        colCurrency
            .setCaption(i18n.get("loans.grid.title.currency"))
            .setWidth(70*k);

        grid.addColumn(AbstractLoan::getProlongOrdNo)
            .setCaption(i18n.get("loans.grid.title.prolongOrdNo"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(70*k);

        grid.addColumn(item -> percentFormat.format(item.getInterestValue()))
            .setCaption(i18n.get("loans.grid.title.interestvalue"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(100*k);

        grid.addColumn(item -> i18n.get(item.getInterestType().getName()))
            .setCaption(i18n.get("loans.grid.title.interesttype"))
            .setWidth(100*k);

        grid.addColumn(Loan::getLoanTerm)
            .setCaption(i18n.get("loans.grid.title.loanterm"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(70*k);

        grid.addColumn(item -> i18n.get(item.getLoanTermType().getName()))
            .setCaption(i18n.get("loans.grid.title.loantermtype"))
            .setWidth(100*k);

        grid.addColumn(item -> i18n.get(item.getReturnTerm().getName()))
            .setCaption(i18n.get("loans.grid.title.returnterm"))
            .setWidth(100*k);

        grid.sort(colLoanIssueDate, SortDirection.DESCENDING);

        grid.addContextClickListener(event -> {
            Grid.GridContextClickEvent<Loan> contextEvent = (Grid.GridContextClickEvent<Loan>) event;

            ContextMenu contextMenu = new ContextMenu(grid, true);
            contextMenu.removeItems();

            Loan loan = contextEvent.getItem();

            MenuItem noteItem = contextMenu.addItem(i18n.get("loans.grid.context.addNote"), e -> {
                if (contextMenu.getParent() != null) contextMenu.remove();
                LoanNotesForm loanNotesForm = new LoanNotesForm(contextMenu, loan, i18n, chronometerService, loanNoteService, securityService,
                    dateTimeFormatter, this);
                UI.getCurrent().addWindow(loanNotesForm);
            });
            noteItem.setEnabled(securityService.hasAuthority(Roles.Loan.LOANNOTE_ADDING));

            contextMenu.open(event.getClientX(), event.getClientY());
        });

        grid.setDescriptionGenerator(item -> {
            List<LoanNote> loanNotes = loanNoteService.findAllByLoan(item);
            String text = "<b>".concat(i18n.get("loans.description.popup.loanNumberCaption")
                .concat(item.getLoanNumber())
                .concat("</b>"));
            if (loanNotes.size() > 0) {
                for (LoanNote note : loanNotes) {
                    text = text.concat("<br>").concat(note.getNote());
                }
                return text;
            } else return null;
        }, ContentMode.HTML);

        HeaderRow headerRow = grid.addHeaderRowAt(1);

        department = new ComboBox();
        department.setItemCaptionGenerator(Department::getName);
        department.setItems(departmentService.findAll());
        department.setWidth(String.valueOf(colDepartment.getWidth()*0.9f));
        headerRow.getCell(colDepartment).setComponent(department);

        loanStatus = new ComboBox<>();
        loanStatus.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        loanStatus.setItems(LoanStatus.values());
        loanStatus.setWidth(String.valueOf(colLoanStatus.getWidth()*0.9f));
        headerRow.getCell(colLoanStatus).setComponent(loanStatus);

        loanNumber = new TextField();
        loanNumber.setWidth(String.valueOf(colLoanNumber.getWidth()*0.9f));
        headerRow.getCell(colLoanNumber).setComponent(loanNumber);

        loanIssueDate = new DateField();
        loanIssueDate.setDateFormat(configurationService.getDateFormat());
        loanIssueDate.setWidth(String.valueOf(colLoanIssueDate.getWidth()*0.9f));
        headerRow.getCell(colLoanIssueDate).setComponent(loanIssueDate);

        clientName = new TextField();
        clientName.setWidth(String.valueOf(colClientName.getWidth()*0.9f));
        headerRow.getCell(colClientName).setComponent(clientName);

        loanBalance = new MoneyField(configurationService.getLocale());
        loanBalance.setWidth(String.valueOf(colLoanBalance.getWidth()*0.9f));
        headerRow.getCell(colLoanBalance).setComponent(loanBalance);

        currentBalance = new MoneyField(configurationService.getLocale());
        currentBalance.setWidth(String.valueOf(colCurrentBalance.getWidth()*0.9f));
        headerRow.getCell(colCurrentBalance).setComponent(currentBalance);

        headerRow.getComponents().stream()
            .filter(item -> item instanceof HasValue)
            .map(item -> (HasValue) item)
            .forEach(item -> item.addValueChangeListener(event -> updateGridContent()));

        grid.addColumnResizeListener(event -> {
            if (event.getColumn() == colDepartment) {
                department.setWidth(String.valueOf(colDepartment.getWidth()*0.9f));
            } else if (event.getColumn() == colLoanStatus) {
                loanStatus.setWidth(String.valueOf(colLoanStatus.getWidth()*0.9f));
            } else if (event.getColumn() == colLoanNumber) {
                loanNumber.setWidth(String.valueOf(colLoanNumber.getWidth()*0.9f));
            } else if (event.getColumn() == colLoanIssueDate) {
                loanIssueDate.setWidth(String.valueOf(colLoanIssueDate.getWidth()*0.9f));
            } else if (event.getColumn() == colClientName) {
                clientName.setWidth(String.valueOf(colClientName.getWidth()*0.9f));
            } else if (event.getColumn() == colLoanBalance) {
                loanBalance.setWidth(String.valueOf(colLoanBalance.getWidth()*0.9f));
            } else if (event.getColumn() == colCurrentBalance) {
                currentBalance.setWidth(String.valueOf(colCurrentBalance.getWidth()*0.9f));
            }
        });
    }

    @Override
    protected Predicate getPredicate() {
        QLoan loan = QLoan.loan;

        return new BooleanBuilder()
            .and(BaseQueryDSL.eq(loan.loanNumber, loanNumber))
            .and(BaseQueryDSL.eq(loan.loanStatus, loanStatus))
            .and(BaseQueryDSL.eq(loan.loanIssueDate, loanIssueDate))
            .and(ClientQueryDSL.displayNameLike(loan.borrower, clientName))
            .and(BaseQueryDSL.eq(loan.loanBalance, loanBalance, configurationService.getLocale()))
            .and(BaseQueryDSL.eq(loan.currentBalance, currentBalance, configurationService.getLocale()))
            .and(BaseQueryDSL.eq(loan.department, department))
            .getValue();
    }

    @Override
    protected String getCreateButtonRole() {
        return Roles.Loan.CREATE;
    }

    @Override
    protected Class<? extends BaseLoanTab<Loan, LoanRepository>> getEntityTabClass() {
        return LoanTab.class;
    }

    @Override
    protected Collection<Button> initExtendedButtons() {
        payment = new Button(i18n.get("loans.button.payment"));
        payment.setIcon(VaadinIcons.COIN_PILES);
        payment.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        payment.setEnabled(false);

        grid.addSelectionListener(event ->
            payment.setEnabled(event.getFirstSelectedItem().isPresent()));

        payment.addClickListener(event -> {
            Loan loan = grid.asSingleSelect().getValue();
            sessionEventBus.post(new AddTabEvent(LoanPaymentTab.class, loan));
        });

        return ImmutableSet.of(payment);
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.loans");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.GREEN;
    }

    @Override
    public void contentChanged() {
        updateGridContent();
    }
}

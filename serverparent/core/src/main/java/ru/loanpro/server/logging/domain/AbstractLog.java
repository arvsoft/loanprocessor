package ru.loanpro.server.logging.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ru.loanpro.global.DatabaseSchema;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.security.domain.User;

/**
 * Базовая сущность лога.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@Table(name = "logs", schema = DatabaseSchema.CORE)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "entity_type",
    discriminatorType = DiscriminatorType.STRING)
public abstract class AbstractLog extends AbstractIdEntity {

    /**
     * Пользователь, событие которого пишется в лог.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * Сессия пользователя, в которой произошло событие.
     */
    @Column(name = "session")
    private Integer session;

    @Column(name = "ip")
    private String ip;

    /**
     * Время события.
     */
    @Column(name = "instant", nullable = false)
    private Instant instant;

    public AbstractLog() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getSession() {
        return session;
    }

    public void setSession(Integer session) {
        this.session = session;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }
}

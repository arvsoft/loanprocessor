package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Estate;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.global.directory.EstateType;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования недвижимости
 *
 * @author Maksim Askhaev
 */
public class EstateDetailsLayout extends HorizontalLayout {
    private final Set<Estate> estates;
    private final I18N i18n;
    private final ConfigurationService configurationService;
    private final StorageService storageService;
    private final User user;
    private final ChronometerService chronometerService;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Estate> grid;
    private ContentChangeListener listener;

    public EstateDetailsLayout(User user, ChronometerService chronometerService, Set<Estate> estates, I18N i18n,
                               ConfigurationService configurationService, StorageService storageService) {

        this.user = user;
        this.chronometerService = chronometerService;
        this.i18n = i18n;
        this.estates = estates;
        this.configurationService = configurationService;
        this.storageService = storageService;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(estates);
        grid.addColumn(item -> item.getEstateType() == null ? "" : i18n.get(item.getEstateType().getName()))
            .setCaption(i18n.get("client.estatedetails.field.type"));
        grid.addColumn(Estate::getDescription).setCaption(i18n.get("client.estatedetails.field.description"));
        grid.addColumn(Estate::getAddress).setCaption(i18n.get("client.estatedetails.field.address"));

        grid.addItemClickListener(event -> {
            closeForm();

            Estate item = event.getItem();
            if (item != null) {
                form = getEstateForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.estatedetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Estate estate = new Estate();
            estate.setEstimatedValue(new BigDecimal(0));
            form = getEstateForm(estate);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getEstateForm(Estate estate) {

        ComboBox<EstateType> estateType = new ComboBox<>(i18n.get("client.estatedetails.field.type"));
        estateType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        estateType.setEmptySelectionAllowed(false);
        estateType.setItems(EstateType.values());
        estateType.setSelectedItem(estate.getEstateType());
        TextField description = new TextField(i18n.get("client.estatedetails.field.description"));
        TextField address = new TextField(i18n.get("client.estatedetails.field.address"));
        TextField inventoryNumber = new TextField(i18n.get("client.estatedetails.field.inventorynumber"));
        TextField document = new TextField(i18n.get("client.estatedetails.field.document"));
        MoneyField estimatedValue = new MoneyField(i18n.get("client.estatedetails.field.estimatedvalue"),
            configurationService.getLocale());

        CheckBox pledge = new CheckBox(i18n.get("client.estatedetails.field.pledge"));
        pledge.addStyleName(ValoTheme.CHECKBOX_SMALL);
        TextField pledgeHostage = new TextField(i18n.get("client.estatedetails.field.pledgehostage"));
        TextField pledgeDocument = new TextField(i18n.get("client.estatedetails.field.pledgedocument"));
        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        Button save = new Button(i18n.get("client.estatedetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.estatedetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(estateType, description, address, inventoryNumber, document, estimatedValue,
            pledge, pledgeHostage, pledgeDocument, scansUpload, layout);
        formLayout.setSizeFull();

        Binder<Estate> binder = new Binder<>();

        binder.forField(estateType)
            .bind(Estate::getEstateType, Estate::setEstateType);
        binder.forField(description)
            .bind(Estate::getDescription, Estate::setDescription);
        binder.forField(address)
            .bind(Estate::getAddress, Estate::setAddress);
        binder.forField(inventoryNumber)
            .bind(Estate::getInventoryNumber, Estate::setInventoryNumber);
        binder.forField(document)
            .bind(Estate::getDocument, Estate::setDocument);

        binder.forField(estimatedValue)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(Estate::getEstimatedValue, Estate::setEstimatedValue);

        binder.forField(pledge)
            .bind(Estate::isPledge, Estate::setPledge);
        binder.forField(pledgeHostage)
            .bind(Estate::getPledgeHostage, Estate::setPledgeHostage);
        binder.forField(pledgeDocument)
            .bind(Estate::getPledgeDocument, Estate::setPledgeDocument);
        binder.forField(scansUpload)
            .bind(Estate::getScans, Estate::setScans);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(estate);

        save.addClickListener(event -> {
            if (estates.stream().noneMatch(item -> item.hashCode() == estate.hashCode()))
                estates.add(estate);

            grid.setItems(estates);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}

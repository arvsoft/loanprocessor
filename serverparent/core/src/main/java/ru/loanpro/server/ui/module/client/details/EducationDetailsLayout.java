package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Education;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.directory.domain.EducationLevel;
import ru.loanpro.directory.service.EducationLevelService;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.server.ui.module.component.DirectoryComboBox;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования образования физического лица
 *
 * @author Maksim Askhaev
 */
public class EducationDetailsLayout extends HorizontalLayout {
    private final Set<Education> educations;
    private final I18N i18n;
    private final EducationLevelService educationLevelService;
    private final StorageService storageService;
    private final User user;
    private final ChronometerService chronometerService;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Education> grid;
    private ContentChangeListener listener;

    public EducationDetailsLayout(User user, ChronometerService chronometerService, Set<Education> educations,
                                  I18N i18n, EducationLevelService educationLevelService,
                                  StorageService storageService) {

        this.user = user;
        this.chronometerService = chronometerService;
        this.i18n = i18n;
        this.educations = educations;
        this.educationLevelService = educationLevelService;
        this.storageService = storageService;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(educations);
        grid.addColumn(item -> item.getLevel() == null ? "" : item.getLevel().getName())
            .setCaption(i18n.get("client.educationdetails.field.level"));
        grid.addColumn(Education::getSpecialty).setCaption(i18n.get("client.educationdetails.field.specialty"));
        grid.addColumn(Education::getInstitution).setCaption(i18n.get("client.educationdetails.field.institution"));
        grid.addColumn(Education::getIssueYear).setCaption(i18n.get("client.educationdetails.field.issueyear"));

        grid.addItemClickListener(event -> {
            closeForm();

            Education item = event.getItem();
            if (item != null) {
                form = getEducationForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.educationdetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Education education = new Education();
            form = getEducationForm(education);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getEducationForm(Education education) {

        DirectoryComboBox<EducationLevel, EducationLevelService> level =
            new DirectoryComboBox<>(i18n.get("client.educationdetails.field.level"), educationLevelService);
        level.setEmptySelectionAllowed(false);

        TextField specialty = new TextField(i18n.get("client.educationdetails.field.specialty"));

        TextField institution = new TextField(i18n.get("client.educationdetails.field.institution"));

        TextField issueYear = new TextField(i18n.get("client.educationdetails.field.issueyear"));

        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        Button save = new Button(i18n.get("client.educationdetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.educationdetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(level, specialty, institution, issueYear, scansUpload, layout);
        formLayout.setSizeFull();

        Binder<Education> binder = new Binder<>();

        binder.forField(level)
            .bind(Education::getLevel, Education::setLevel);
        binder.forField(specialty)
            .bind(Education::getSpecialty, Education::setSpecialty);
        binder.forField(institution)
            .bind(Education::getInstitution, Education::setInstitution);
        binder.forField(issueYear)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .bind(Education::getIssueYear, Education::setIssueYear);
        binder.forField(scansUpload)
            .bind(Education::getScans, Education::setScans);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(education);

        save.addClickListener(event -> {
            if (educations.stream().noneMatch(item -> item.hashCode() == education.hashCode()))
                educations.add(education);

            grid.setItems(educations);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}

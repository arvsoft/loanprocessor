package ru.loanpro.server.controller;

import java.io.FileNotFoundException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ru.loanpro.base.storage.IStorageService;

@RestController
@RequestMapping("/scan")
public class ScanController {

    @Autowired
    private IStorageService storageService;

    @RequestMapping(path = "/{fileName}", method = RequestMethod.GET, produces = "application/image")
//    @Secured(Roles.Loan.PRINT)
    public ResponseEntity<InputStreamResource> getImage(@PathVariable("fileName") String fileName) throws FileNotFoundException {
        InputStream stream = storageService.getScanFileStream(fileName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/image"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "filename=" + fileName);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        headers.setContentLength(storageService.getScanFileLength(fileName));
        return new ResponseEntity<>(new InputStreamResource(stream), headers, HttpStatus.OK);
    }
}

package ru.loanpro.server.ui.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.Settings;
import ru.loanpro.security.settings.exceptions.SettingsUnsupportedTypeException;
import ru.loanpro.security.settings.service.SettingsService;

import java.util.Objects;

/**
 * Хэлпер для вкладки настроек. Эмулирует отображение основной информации о системе в виде сущности
 * для биндера. Позволяет осуществлять проверку изменений информации сравнением хэш-кодов.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
public class SettingsTabInformationHelper {

    private final SettingsService settingsService;

    private String name;
    private String fullName;
    private String regNumber1;
    private String regNumber2;
    private String regNumber3;
    private String regNumber4;
    private String regNumber5;
    private String regNumber6;
    private String phones;
    private String juridicalAddress;
    private String actualAddress;
    private String email;
    private String bankAccount1;
    private String bankAccount2;
    private String director;
    private String directorName;
    private String assistant1;
    private String assistantName1;
    private String assistant2;
    private String assistantName2;

    @Autowired
    public SettingsTabInformationHelper(SettingsService settingsService) {
        this.settingsService = settingsService;

        name = settingsService.get(String.class, Settings.INFO_NAME, "");
        fullName = settingsService.get(String.class, Settings.INFO_FULL_NAME, "");
        regNumber1 = settingsService.get(String.class, Settings.INFO_REG_NUMBER_1, "");
        regNumber2 = settingsService.get(String.class, Settings.INFO_REG_NUMBER_2, "");
        regNumber3 = settingsService.get(String.class, Settings.INFO_REG_NUMBER_3, "");
        regNumber4 = settingsService.get(String.class, Settings.INFO_REG_NUMBER_4, "");
        regNumber5 = settingsService.get(String.class, Settings.INFO_REG_NUMBER_5, "");
        regNumber6 = settingsService.get(String.class, Settings.INFO_REG_NUMBER_6, "");
        phones = settingsService.get(String.class, Settings.INFO_PHONES, "");
        juridicalAddress = settingsService.get(String.class, Settings.INFO_JURIDICAL_ADDRESS, "");
        actualAddress = settingsService.get(String.class, Settings.INFO_ACTUAL_ADDRESS, "");
        email = settingsService.get(String.class, Settings.INFO_EMAIL, "");
        bankAccount1 = settingsService.get(String.class, Settings.INFO_BANK_ACCOUNT_1, "");
        bankAccount2 = settingsService.get(String.class, Settings.INFO_BANK_ACCOUNT_2, "");
        director = settingsService.get(String.class, Settings.INFO_DIRECTOR, "");
        directorName = settingsService.get(String.class, Settings.INFO_DIRECTOR_NAME, "");
        assistant1 = settingsService.get(String.class, Settings.INFO_ASSISTANT_1, "");
        assistantName1 = settingsService.get(String.class, Settings.INFO_ASSISTANT_NAME_1, "");
        assistant2 = settingsService.get(String.class, Settings.INFO_ASSISTANT_2, "");
        assistantName2 = settingsService.get(String.class, Settings.INFO_ASSISTANT_NAME_2, "");
    }

    public void save() {
        try {
            settingsService.set(Settings.INFO_NAME, name);
            settingsService.set(Settings.INFO_FULL_NAME, fullName);
            settingsService.set(Settings.INFO_REG_NUMBER_1, regNumber1);
            settingsService.set(Settings.INFO_REG_NUMBER_2, regNumber2);
            settingsService.set(Settings.INFO_REG_NUMBER_3, regNumber3);
            settingsService.set(Settings.INFO_REG_NUMBER_4, regNumber4);
            settingsService.set(Settings.INFO_REG_NUMBER_5, regNumber5);
            settingsService.set(Settings.INFO_REG_NUMBER_6, regNumber6);
            settingsService.set(Settings.INFO_PHONES, phones);
            settingsService.set(Settings.INFO_JURIDICAL_ADDRESS, juridicalAddress);
            settingsService.set(Settings.INFO_ACTUAL_ADDRESS, actualAddress);
            settingsService.set(Settings.INFO_EMAIL, email);
            settingsService.set(Settings.INFO_BANK_ACCOUNT_1, bankAccount1);
            settingsService.set(Settings.INFO_BANK_ACCOUNT_2, bankAccount2);
            settingsService.set(Settings.INFO_DIRECTOR, director);
            settingsService.set(Settings.INFO_DIRECTOR_NAME, directorName);
            settingsService.set(Settings.INFO_ASSISTANT_1, assistant1);
            settingsService.set(Settings.INFO_ASSISTANT_NAME_1, assistantName1);
            settingsService.set(Settings.INFO_ASSISTANT_2, assistant2);
            settingsService.set(Settings.INFO_ASSISTANT_NAME_2, assistantName2);
        } catch (SettingsUnsupportedTypeException e) {
            e.printStackTrace();
        }
    }

    public SettingsService getSettingsService() {
        return settingsService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRegNumber1() {
        return regNumber1;
    }

    public void setRegNumber1(String regNumber1) {
        this.regNumber1 = regNumber1;
    }

    public String getRegNumber2() {
        return regNumber2;
    }

    public void setRegNumber2(String regNumber2) {
        this.regNumber2 = regNumber2;
    }

    public String getRegNumber3() {
        return regNumber3;
    }

    public void setRegNumber3(String regNumber3) {
        this.regNumber3 = regNumber3;
    }

    public String getRegNumber4() {
        return regNumber4;
    }

    public void setRegNumber4(String regNumber4) {
        this.regNumber4 = regNumber4;
    }

    public String getRegNumber5() {
        return regNumber5;
    }

    public void setRegNumber5(String regNumber5) {
        this.regNumber5 = regNumber5;
    }

    public String getRegNumber6() {
        return regNumber6;
    }

    public void setRegNumber6(String regNumber6) {
        this.regNumber6 = regNumber6;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }

    public String getJuridicalAddress() {
        return juridicalAddress;
    }

    public void setJuridicalAddress(String juridicalAddress) {
        this.juridicalAddress = juridicalAddress;
    }

    public String getActualAddress() {
        return actualAddress;
    }

    public void setActualAddress(String actualAddress) {
        this.actualAddress = actualAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBankAccount1() {
        return bankAccount1;
    }

    public void setBankAccount1(String bankAccount1) {
        this.bankAccount1 = bankAccount1;
    }

    public String getBankAccount2() {
        return bankAccount2;
    }

    public void setBankAccount2(String bankAccount2) {
        this.bankAccount2 = bankAccount2;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getAssistant1() {
        return assistant1;
    }

    public void setAssistant1(String assistant1) {
        this.assistant1 = assistant1;
    }

    public String getAssistantName1() {
        return assistantName1;
    }

    public void setAssistantName1(String assistantName1) {
        this.assistantName1 = assistantName1;
    }

    public String getAssistant2() {
        return assistant2;
    }

    public void setAssistant2(String assistant2) {
        this.assistant2 = assistant2;
    }

    public String getAssistantName2() {
        return assistantName2;
    }

    public void setAssistantName2(String assistantName2) {
        this.assistantName2 = assistantName2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SettingsTabInformationHelper that = (SettingsTabInformationHelper) o;
        return Objects.equals(settingsService, that.settingsService) &&
            Objects.equals(name, that.name) &&
            Objects.equals(fullName, that.fullName) &&
            Objects.equals(regNumber1, that.regNumber1) &&
            Objects.equals(regNumber2, that.regNumber2) &&
            Objects.equals(regNumber3, that.regNumber3) &&
            Objects.equals(regNumber4, that.regNumber4) &&
            Objects.equals(regNumber5, that.regNumber5) &&
            Objects.equals(regNumber6, that.regNumber6) &&
            Objects.equals(phones, that.phones) &&
            Objects.equals(juridicalAddress, that.juridicalAddress) &&
            Objects.equals(actualAddress, that.actualAddress) &&
            Objects.equals(email, that.email) &&
            Objects.equals(bankAccount1, that.bankAccount1) &&
            Objects.equals(bankAccount2, that.bankAccount2) &&
            Objects.equals(director, that.director) &&
            Objects.equals(directorName, that.directorName) &&
            Objects.equals(assistant1, that.assistant1) &&
            Objects.equals(assistantName1, that.assistantName1) &&
            Objects.equals(assistant2, that.assistant2) &&
            Objects.equals(assistantName2, that.assistantName2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(settingsService, name, fullName, regNumber1, regNumber2, regNumber3, regNumber4, regNumber5,
            regNumber6, phones, juridicalAddress, actualAddress, email, bankAccount1, bankAccount2, director,
            directorName, assistant1, assistantName1, assistant2, assistantName2);
    }
}

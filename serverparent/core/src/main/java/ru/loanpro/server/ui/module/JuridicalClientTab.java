package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Lists;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.client.domain.JuridicalClient;
import ru.loanpro.base.client.repository.JuridicalClientRepository;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.server.ui.module.event.UpdateClientEvent;

/**
 * Вкладка юридического лица {@link JuridicalClient}.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
public class JuridicalClientTab extends ClientTab<JuridicalClient, JuridicalClientRepository> {

    private Button btSave;

    public JuridicalClientTab(JuridicalClient client) {
        entity = (client == null) ? new JuridicalClient() : client;
    }

    @Override
    protected Layout initBaseInformation() {
        Label lbMainInfo = new Label(i18n.get("client.panel.maininfo.title"));
        btSave = new Button(i18n.get("loan.button.save"));
        btSave.setIcon(VaadinIcons.ADD_DOCK);
        btSave.addStyleName(MaterialTheme.BUTTON_BORDERLESS_COLORED);

        Button btPrint = new Button(i18n.get("loan.button.print"));
        btPrint.setIcon(VaadinIcons.PRINT);
        btPrint.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        HorizontalLayout hlActions = new HorizontalLayout(lbMainInfo, btSave, btPrint);
        hlActions.setExpandRatio(lbMainInfo, 1);
        hlActions.setSizeFull();

        ComboBox<ClientRating> cbClientRating = new ComboBox<>(
            i18n.get("client.field.rating"), Lists.newArrayList(ClientRating.values()));
        cbClientRating.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbClientRating.setEmptySelectionAllowed(false);
        cbClientRating.setReadOnly(true);
        TextField tfFullName = new TextField(i18n.get("client.field.fullName"));
        TextField tfShortName = new TextField(i18n.get("client.field.shortName"));
        DateField dfRegisterDate = new DateField(i18n.get("client.field.registerDate"));

        HorizontalLayout hlBlockNames = new HorizontalLayout();
        hlBlockNames.addComponents(tfFullName, tfShortName, dfRegisterDate);
        VerticalLayout vlMainInfo = new VerticalLayout(hlActions, cbClientRating, hlBlockNames);
        vlMainInfo.setComponentAlignment(hlActions, Alignment.TOP_RIGHT);
        vlMainInfo.setSizeFull();

        binder.bind(cbClientRating, JuridicalClient::getRating, JuridicalClient::setRating);
        binder.forField(tfFullName)
            .asRequired()
            .bind(JuridicalClient::getFullName, JuridicalClient::setFullName);

        binder.bind(tfShortName, JuridicalClient::getShortName, JuridicalClient::setShortName);
        binder.bind(dfRegisterDate, JuridicalClient::getRegisterDate, JuridicalClient::setRegisterDate);

        binder.addValueChangeListener(event -> {
            btSave.setEnabled(binder.isValid() && entity.hashCode() != entityHash);
        });

        btSave.addClickListener(e ->{
            service.save(entity);

            sessionEventBus.post(new UpdateClientEvent<>(entity));

            Notification.show("Client saved", Notification.Type.TRAY_NOTIFICATION);
        });

        return vlMainInfo;
    }

    @Override
    public String getTabName() {
        if (entity.getId() == null) {
            return i18n.get("client.tab.add.caption");
        } else {
            return i18n.get("client.tab.edit.caption") + entity.getShortName();
        }
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIGHT_BLUE;
    }

    @Override
    public void contentChanged() {
        btSave.setEnabled(binder.isValid() && entity.hashCode() != entityHash);
    }
}

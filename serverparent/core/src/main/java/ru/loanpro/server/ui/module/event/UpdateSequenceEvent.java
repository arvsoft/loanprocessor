package ru.loanpro.server.ui.module.event;

import ru.loanpro.sequence.domain.Sequence;

public class UpdateSequenceEvent {

    private Sequence sequence;

    public UpdateSequenceEvent(Sequence sequence) {
        this.sequence = sequence;
    }

    public Sequence getSequence() {
        return sequence;
    }
}

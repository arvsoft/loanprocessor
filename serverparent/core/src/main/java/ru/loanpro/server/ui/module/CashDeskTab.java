package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.eventbus.Subscribe;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.MenuItem;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.service.DepartmentAccountService;
import ru.loanpro.base.client.domain.PhysicalClientQueryDSL;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.operation.domain.Balance;
import ru.loanpro.base.operation.domain.BalanceFixation;
import ru.loanpro.base.operation.domain.Operation;
import ru.loanpro.base.operation.domain.QOperation;
import ru.loanpro.base.operation.event.UpdateOperationEvent;
import ru.loanpro.base.operation.exceptions.BalanceAlreadyFixedException;
import ru.loanpro.base.operation.exceptions.DeleteLastFixationException;
import ru.loanpro.base.operation.exceptions.FixBalanceException;
import ru.loanpro.base.operation.exceptions.ForbidToFixBalanceException;
import ru.loanpro.base.operation.service.BalanceFixationService;
import ru.loanpro.base.operation.service.OperationService;
import ru.loanpro.base.unload.service.UnloadCashBookService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.global.directory.OperationStatus;
import ru.loanpro.global.directory.PaymentMethod;
import ru.loanpro.global.domain.BaseQueryDSL;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.service.UserService;
import ru.loanpro.server.ui.module.additional.ConfirmDialog;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.component.LoanNotesForm;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.AddTabEvent;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * Вкладка для работы с кассой (с операциями)
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.cashdesk")
public class CashDeskTab extends BaseTab {

    private final OperationService operationService;
    private final DepartmentAccountService departmentAccountService;
    private final BalanceFixationService balanceFixationService;
    private final DepartmentService departmentService;
    private final UserService userService;
    private final UnloadCashBookService unloadCashBookService;

    @Visible(Roles.Cashdesk.CREATE)
    private Button create;

    @Visible(Roles.Cashdesk.VIEW)
    private Button view;

    @Visible(Roles.Operation.FIX_BALANCE)
    private Button fixBalance;

    @Visible(Roles.Cashdesk.UNLOAD_CASHBOOK)
    private Button prepareCashBook;

    private int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    private int scaleValue = 2;

    private Button deleteLastFixation;
    private ComboBox<OperationStatus> status;
    private ComboBox<Department> department;
    private DateField operationDateTime;
    private MoneyField operationValue;
    private ComboBox<PaymentMethod> paymentMethod;
    private TextField accountPayerNumber;
    private TextField payerDisplayName;
    private TextField accountRecipientNumber;
    private TextField recipientDisplayName;
    private TextField loanNumber;

    private DateTimeFormatter dateTimeFormatter;
    private DateTimeFormatter dateTimeFormatter2;
    private DecimalFormat moneyFormat;
    private DecimalFormat percentFormat;
    private Grid<Operation> grid;

    private ComboBox<Department> departmentCashState;

    private HorizontalLayout hlAction;
    private Grid<DepartmentAccount> accountGrid;
    private Grid<Balance> balanceGrid;
    private Grid<BalanceFixation> fixationGrid;
    private List<DepartmentAccount> accountList;

    private Set<Currency> currencyList;
    private List<Balance> balanceList;
    private List<BalanceFixation> balanceFixationList;
    private TextField paymentOrdNo;
    private ComboBox<User> user;

    @Autowired
    public CashDeskTab(OperationService operationService, DepartmentAccountService departmentAccountService,
                       BalanceFixationService balanceFixationService, DepartmentService departmentService,
                       UserService userService, UnloadCashBookService unloadCashBookService) {
        this.operationService = operationService;
        this.departmentAccountService = departmentAccountService;
        this.balanceFixationService = balanceFixationService;
        this.departmentService = departmentService;
        this.userService = userService;
        this.unloadCashBookService = unloadCashBookService;
    }

    @PostConstruct
    public void init() {
        applicationEventBus.register(this);

        dateTimeFormatter2 =
            DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        dateTimeFormatter =
            DateTimeFormatter.ofPattern(configurationService.getDateFormat(), configurationService.getLocale());

        moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        percentFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        percentFormat.setMinimumFractionDigits(3);
        percentFormat.setMaximumFractionDigits(3);

        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleNames(ValoTheme.TABSHEET_FRAMED, ValoTheme.TABSHEET_PADDED_TABBAR);

        tabSheet.setWidth(100, Unit.PERCENTAGE);
        tabSheet.setSizeFull();
        tabSheet.addTab(getOperationLayout(), i18n.get("cashdesk.tab.operation"));
        tabSheet.addTab(getCashdeskLayout(), i18n.get("cashdesk.tab.cashdesk"));
        addComponent(tabSheet);
        setSizeFull();
    }

    private Layout getOperationLayout() {
        create = new Button(i18n.get("cashdesk.button.process"));
        create.setIcon(VaadinIcons.PLUS);
        create.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        create.addClickListener(event -> sessionEventBus.post(new AddTabEvent(OperationTab.class)));

        view = new Button(i18n.get("cashdesk.button.view"));
        view.setIcon(VaadinIcons.EDIT);
        view.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        view.setEnabled(false);

        view.addClickListener(event -> {
            Operation operation = grid.asSingleSelect().getValue();
            sessionEventBus.post(new AddTabEvent(OperationTab.class, operation));
        });

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        float k = 1.0f;
        if (configurationService.getGridStyle() == GridStyle.SMALL) {
            grid.addStyleName("v-grid-style-small");
            grid.setRowHeight(40f);
        } else k = 1.3f;

        Grid.Column<Operation, String> colOperationNumber = grid.addColumn(Operation::getOperationNumber);
        colOperationNumber
            .setSortProperty("operationNumber")
            .setCaption(i18n.get("cashdesk.grid.title.operationnumber"))
            .setWidth(100 * k);

        Grid.Column<Operation, String> colDepartment = grid.addColumn(item -> item.getDepartment().getName());
        colDepartment
            .setSortProperty("department")
            .setCaption(i18n.get("cashdesk.grid.title.department"))
            .setWidth(250 * k);

        Grid.Column<Operation, String> colStatus = grid.addColumn(item -> i18n.get(item.getStatus().getName()));
        colStatus
            .setSortProperty("status")
            .setCaption(i18n.get("cashdesk.grid.title.status"))
            .setStyleGenerator(item -> item.getStatus().getColor())
            .setWidth(150 * k);

        Grid.Column<Operation, String> colOperationDateTime = grid.addColumn(item ->
            chronometerService.zonedDateTimeToString(item.getOperationDateTime(), dateTimeFormatter2));
        colOperationDateTime
            .setSortProperty("operationDateTime")
            .setCaption(i18n.get("cashdesk.grid.title.datetime"))
            .setWidth(150 * k);

        Grid.Column<Operation, String> colOperationValue = grid.addColumn(item ->
            moneyFormat.format(item.getOperationValue()));
        colOperationValue
            .setSortProperty("operationValue")
            .setCaption(i18n.get("cashdesk.grid.title.value"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(150 * k);

        grid.addColumn(item -> item.getCurrency().getCurrencyCode())
            .setSortProperty("currency")
            .setCaption(i18n.get("cashdesk.grid.title.currency"))
            .setWidth(70 * k);

        Grid.Column<Operation, String> colPaymentMethod = grid.addColumn(item ->
            i18n.get(item.getPaymentMethod().getName()));
        colPaymentMethod
            .setSortProperty("paymentMethod")
            .setCaption(i18n.get("cashdesk.grid.title.paymentmethod"))
            .setWidth(100 * k);

        Grid.Column<Operation, String> colAccountPayerNumber = grid.addColumn(item ->
            item.getAccountPayer() == null ? "" : item.getAccountPayer().getNumber());
        colAccountPayerNumber
            .setSortProperty("accountPayer.number")
            .setCaption(i18n.get("cashdesk.grid.title.accountpayer"))
            .setWidth(100 * k);

        Grid.Column colPayerDisplayName = grid.addColumn(item ->
            item.getPayer() == null ? "" : item.getPayer().getDisplayName());
        colPayerDisplayName
            .setSortProperty("payer.lastName")
            .setCaption(i18n.get("cashdesk.grid.title.payer"))
            .setWidth(250 * k);

        Grid.Column<Operation, String> colAccountRecipientNumber = grid.addColumn(item ->
            item.getAccountRecipient() == null ? "" : item.getAccountRecipient().getNumber());
        colAccountRecipientNumber
            .setSortProperty("accountRecipient.number")
            .setCaption(i18n.get("cashdesk.grid.title.accountrecipient"))
            .setWidth(100 * k);

        Grid.Column<Operation, String> colRecipientDisplayName = grid.addColumn(item ->
            item.getRecipient() == null ? "" : item.getRecipient().getDisplayName());
        colRecipientDisplayName
            .setSortProperty("recipient.lastName")
            .setCaption(i18n.get("cashdesk.grid.title.recipient"))
            .setWidth(250 * k);

        Grid.Column<Operation, String> colLoanNumber = grid.addColumn(item ->
            item.getLoan() == null ? "" : item.getLoan().getLoanNumber());
        colLoanNumber
            .setSortProperty("loan.loanNumber")
            .setCaption(i18n.get("cashdesk.grid.title.loan"))
            .setWidth(100 * k);

        Grid.Column<Operation, ? extends Serializable> colPaymentOrdNo = grid.addColumn(item ->
            item.getPayment() == null ? "" : item.getPayment().getOrdNo());
        colPaymentOrdNo
            .setSortProperty("payment.ordNo")
            .setCaption(i18n.get("cashdesk.grid.title.payment"))
            .setWidth(70 * k);

        grid.addColumn(item -> item.getNotes() == null ? "" : item.getNotes())
            .setCaption(i18n.get("cashdesk.grid.title.notes"))
            .setWidth(200 * k);

        Grid.Column<Operation, String> colUser = grid.addColumn(item ->
            item.getUser() == null ? "" : item.getUser().getUsername());
        colUser
            .setSortProperty("user.username")
            .setCaption(i18n.get("cashdesk.grid.title.userName"))
            .setWidth(100 * k);

        grid.addSelectionListener(event -> view.setEnabled(event.getFirstSelectedItem().isPresent()));

        grid.addContextClickListener(event -> {
            Grid.GridContextClickEvent<Operation> contextEvent = (Grid.GridContextClickEvent<Operation>) event;

            ContextMenu contextMenu = new ContextMenu(grid, true);
            contextMenu.removeItems();

            Operation operation = contextEvent.getItem();

            MenuItem deleteItem = contextMenu.addItem(i18n.get("cashdesk.grid.context.deleteOperation"), e -> {
                if (contextMenu.getParent() != null) contextMenu.remove();

                if (operation.isPayerFixed() || operation.isRecipientFixed()){
                    Notification.show(i18n.get("cashdesk.notification.deleteErrorFixationExists"), Notification.Type.WARNING_MESSAGE);
                } else {
                    ConfirmDialog dialog = new ConfirmDialog(
                        i18n.get("cashdesk.dialogDelete.caption"),
                        i18n.get("cashdesk.dialogDeleteOperation.description"),
                        () -> {
                            try {
                                operationService.deleteOne(operation);
                                updateGridContent();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Notification.show(i18n.get("cashdesk.notification.deleteError"), Notification.Type.WARNING_MESSAGE);
                            }
                        });
                    dialog.setConfirmButtonIcon(VaadinIcons.TRASH);
                    dialog.setClosable(true);
                    dialog.show();
                }
            });
            deleteItem.setEnabled(securityService.hasAuthority(Roles.Cashdesk.DELETE_OPERATION));

            contextMenu.open(event.getClientX(), event.getClientY());
        });

        grid.sort(colOperationDateTime, SortDirection.DESCENDING);

        HeaderRow headerRow = grid.addHeaderRowAt(1);

        status = new ComboBox<>();
        status.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        status.setItems(OperationStatus.values());
        status.setWidth(String.valueOf(colStatus.getWidth() * 0.9f));
        headerRow.getCell(colStatus).setComponent(status);

        department = new ComboBox<>();
        department.setItemCaptionGenerator(Department::getName);
        department.setItems(departmentService.findAll());
        department.setWidth(String.valueOf(colDepartment.getWidth() * 0.9f));
        headerRow.getCell(colDepartment).setComponent(department);

        operationDateTime = new DateField();
        operationDateTime.setWidth(String.valueOf(colOperationDateTime.getWidth() * 0.9f));
        headerRow.getCell(colOperationDateTime).setComponent(operationDateTime);

        operationValue = new MoneyField(configurationService.getLocale());
        operationValue.setWidth(String.valueOf(colOperationValue.getWidth() * 0.9f));
        headerRow.getCell(colOperationValue).setComponent(operationValue);

        paymentMethod = new ComboBox<>();
        paymentMethod.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        paymentMethod.setItems(PaymentMethod.values());
        paymentMethod.setWidth(String.valueOf(colPaymentMethod.getWidth() * 0.9f));
        headerRow.getCell(colPaymentMethod).setComponent(paymentMethod);

        accountPayerNumber = new TextField();
        accountPayerNumber.setWidth(String.valueOf(colAccountPayerNumber.getWidth() * 0.9f));
        headerRow.getCell(colAccountPayerNumber).setComponent(accountPayerNumber);

        payerDisplayName = new TextField();
        payerDisplayName.setWidth(String.valueOf(colPayerDisplayName.getWidth() * 0.9f));
        headerRow.getCell(colPayerDisplayName).setComponent(payerDisplayName);

        accountRecipientNumber = new TextField();
        accountRecipientNumber.setWidth(String.valueOf(colAccountRecipientNumber.getWidth() * 0.9f));
        headerRow.getCell(colAccountRecipientNumber).setComponent(accountRecipientNumber);

        recipientDisplayName = new TextField();
        recipientDisplayName.setWidth(String.valueOf(colRecipientDisplayName.getWidth() * 0.9f));
        headerRow.getCell(colRecipientDisplayName).setComponent(recipientDisplayName);

        loanNumber = new TextField();
        loanNumber.setWidth(String.valueOf(colLoanNumber.getWidth() * 0.9f));
        headerRow.getCell(colLoanNumber).setComponent(loanNumber);

        paymentOrdNo = new TextField();
        paymentOrdNo.setWidth(String.valueOf(colPaymentOrdNo.getWidth() * 0.9f));
        headerRow.getCell(colPaymentOrdNo).setComponent(paymentOrdNo);

        user = new ComboBox<>();
        user.setItemCaptionGenerator(User::getUsername);
        user.setItems(userService.findAllActiveUsers());
        user.setWidth(String.valueOf(colUser.getWidth() * 0.9f));
        headerRow.getCell(colUser).setComponent(user);



        grid.addColumnResizeListener(event -> {
            if (event.getColumn() == colDepartment) {
                department.setWidth(String.valueOf(colDepartment.getWidth() * 0.9f));
            } else if (event.getColumn() == colStatus) {
                status.setWidth(String.valueOf(colStatus.getWidth() * 0.9f));
            } else if (event.getColumn() == colOperationDateTime) {
                operationDateTime.setWidth(String.valueOf(colOperationDateTime.getWidth() * 0.9f));
            } else if (event.getColumn() == colOperationValue) {
                operationValue.setWidth(String.valueOf(colOperationValue.getWidth() * 0.9f));
            } else if (event.getColumn() == colPaymentMethod) {
                paymentMethod.setWidth(String.valueOf(colPaymentMethod.getWidth() * 0.9f));
            } else if (event.getColumn() == colAccountPayerNumber) {
                accountPayerNumber.setWidth(String.valueOf(colAccountPayerNumber.getWidth() * 0.9f));
            } else if (event.getColumn() == colPayerDisplayName) {
                payerDisplayName.setWidth(String.valueOf(colPayerDisplayName.getWidth() * 0.9f));
            } else if (event.getColumn() == colAccountRecipientNumber) {
                accountRecipientNumber.setWidth(String.valueOf(colAccountRecipientNumber.getWidth() * 0.9f));
            } else if (event.getColumn() == colRecipientDisplayName) {
                recipientDisplayName.setWidth(String.valueOf(colRecipientDisplayName.getWidth() * 0.9f));
            } else if (event.getColumn() == colLoanNumber) {
                loanNumber.setWidth(String.valueOf(colLoanNumber.getWidth() * 0.9f));
            } else if (event.getColumn() == colPaymentOrdNo) {
                paymentOrdNo.setWidth(String.valueOf(colPaymentOrdNo.getWidth() * 0.9f));
            } else if (event.getColumn() == colUser) {
                user.setWidth(String.valueOf(colUser.getWidth() * 0.9f));
            }
        });

        headerRow.getComponents().stream()
            .filter(item -> item instanceof HasValue)
            .map(item -> (HasValue) item)
            .forEach(item -> item.addValueChangeListener(event -> updateGridContent()));

        updateGridContent();

        HorizontalLayout hlButtons = new HorizontalLayout(create, view);

        VerticalLayout layout = new VerticalLayout(hlButtons, grid);
        layout.setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
        layout.setExpandRatio(grid, 1);
        layout.setSizeFull();

        return layout;
    }

    private void updateGridContent() {
        QOperation operation = QOperation.operation;

        Predicate predicate = new BooleanBuilder()
            .and(BaseQueryDSL.eq(operation.status, status))
            .and(BaseQueryDSL.eq(operation.department, department))
            .and(BaseQueryDSL.eq(operation.operationDateTime, operationDateTime, configurationService.getZoneId()))
            .and(BaseQueryDSL.eq(operation.operationValue, operationValue, configurationService.getLocale()))
            .and(BaseQueryDSL.eq(operation.paymentMethod, paymentMethod))
            .and(BaseQueryDSL.contains(operation.accountPayer.number, accountPayerNumber))
            .and(PhysicalClientQueryDSL.fullNameLike(operation.payer, payerDisplayName))
            .and(BaseQueryDSL.contains(operation.accountRecipient.number, accountRecipientNumber))
            .and(PhysicalClientQueryDSL.fullNameLike(operation.recipient, recipientDisplayName))
            .and(BaseQueryDSL.eq(operation.loan.loanNumber, loanNumber))
            .and(BaseQueryDSL.eq(operation.payment.ordNo, paymentOrdNo))
            .and(BaseQueryDSL.eq(operation.user, user))
            .getValue();

        grid.setDataProvider(
            (sortOrder, offset, limit) -> operationService.findAll(predicate, sortOrder, offset, limit),
            () -> operationService.count(predicate));
    }

    private Layout getCashdeskLayout() {
        VerticalLayout layout = new VerticalLayout();

        departmentCashState = new ComboBox<>(i18n.get("cashdesk.field.department"));
        departmentCashState.setWidth(400, Unit.PIXELS);
        departmentCashState.setItemCaptionGenerator(Department::getName);
        departmentCashState.setEmptySelectionAllowed(false);
        departmentCashState.setItems(securityService.getDepartments());
        departmentCashState.setValue(securityService.getDepartment());

        getDepartmentData(departmentCashState.getValue());

        departmentCashState.addValueChangeListener(event -> {
            getDepartmentData(departmentCashState.getValue());
            accountList = new ArrayList<>(getDepartmentAccounts(event.getValue()));
            accountGrid.setItems(accountList);
            balanceGrid.setItems(balanceList);
            balanceFixationList = balanceFixationService.findAllBalanceFixationsByDepartment(departmentCashState.getValue());
            fixationGrid.setItems(balanceFixationList);
            deleteLastFixation.setEnabled(balanceFixationList.size() > 0);
        });

        fixBalance = new Button(i18n.get("cashdesk.button.fixBalance"));
        fixBalance.setIcon(VaadinIcons.ANCHOR);
        fixBalance.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        fixBalance.setEnabled(securityService.hasAuthority(Roles.Operation.FIX_BALANCE));

        fixBalance.addClickListener(event -> {
            getDepartmentData(departmentCashState.getValue());
            accountGrid.setItems(accountList);
            balanceGrid.setItems(balanceList);

            fixBalance(departmentCashState.getValue());
        });

        HorizontalLayout hlActions = new HorizontalLayout(departmentCashState, fixBalance);
        hlActions.setWidth(100, Unit.PERCENTAGE);
        hlActions.setExpandRatio(departmentCashState, 1);

        //*****************************************************************************************//
        accountGrid = new Grid<>();
        accountGrid.setHeight(300, Unit.PIXELS);
        accountGrid.setSelectionMode(Grid.SelectionMode.SINGLE);

        accountGrid.addColumn(item -> i18n.get(item.getNumber()))
            .setCaption(i18n.get("cashdesk.accountGrid.title.number"));

        Grid.Column column1 = accountGrid.addColumn(item -> item.getCurrency().getCurrencyCode())
            .setCaption(i18n.get("cashdesk.accountGrid.title.currency"));
        column1.setWidth(120);

        accountGrid.addColumn(item -> item.getBalance() == null ? "" : moneyFormat.format(item.getBalance()))
            .setCaption(i18n.get("cashdesk.accountGrid.title.balance"))
            .setStyleGenerator(item -> "v-align-right");
        accountGrid.setItems(accountList);

        Label label1 = new Label(i18n.get("cashdesk.label.accountInfo"));

        VerticalLayout accountLayout = new VerticalLayout(label1, accountGrid);
        accountLayout.addStyleName(MaterialTheme.CARD_2);

        //*****************************************************************************************//
        balanceGrid = new Grid<>();
        balanceGrid.setSizeFull();
        balanceGrid.setHeight(300, Unit.PIXELS);
        balanceGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        BigDecimal compare = new BigDecimal(0).setScale(scaleValue, scaleMode);

        Grid.Column column2 = balanceGrid.addColumn(item -> item.getCurrency().getCurrencyCode())
            .setCaption(i18n.get("cashdesk.balanceGrid.title.currency"));
        column2.setWidth(120);

        balanceGrid.addColumn(item -> item.getBalance() == null ? "" : moneyFormat.format(item.getBalance()))
            .setCaption(i18n.get("cashdesk.balanceGrid.title.balance"))
            .setStyleGenerator(item -> "v-align-right");

        balanceGrid.addColumn(item -> item.getIncome() == null ? "" :
            item.getIncome().compareTo(compare) != 0 ?
                String.format("+%s", moneyFormat.format(item.getIncome())) : moneyFormat.format(item.getIncome()))
            .setCaption(i18n.get("cashdesk.balanceGrid.title.income"))
            .setStyleGenerator(item -> item.getIncome().compareTo(compare) != 0 ? "income-grid-color" : "v-align-right");

        balanceGrid.addColumn(item -> item.getExpense() == null ? "" :
            item.getExpense().compareTo(compare) != 0 ?
                String.format("-%s", moneyFormat.format(item.getExpense())) : moneyFormat.format(item.getExpense()))
            .setCaption(i18n.get("cashdesk.balanceGrid.title.expense"))
            .setStyleGenerator(item -> item.getExpense().compareTo(compare) != 0 ? "expense-grid-color" : "v-align-right");

        balanceGrid.setItems(balanceList);

        String date = chronometerService.getSystemTime().toLocalDate().format(dateTimeFormatter);
        Label label2 = new Label(i18n.get("cashdesk.label.balanceInfo").concat(" ").concat(date));

        VerticalLayout balanceLayout = new VerticalLayout(label2, balanceGrid);
        balanceLayout.setWidth(750, Unit.PIXELS);
        balanceLayout.addStyleName(MaterialTheme.CARD_2);

        HorizontalLayout hrGrid = new HorizontalLayout(accountLayout, balanceLayout);
        hrGrid.setStyleName("flex-wrap-left-alignment-layout");

        //*****************************************************************************************//
        fixationGrid = new Grid<>();
        fixationGrid.setWidth(100, Unit.PERCENTAGE);
        fixationGrid.setSelectionMode(Grid.SelectionMode.SINGLE);

        Grid.Column column3 = fixationGrid.addColumn(item -> item.getCurrency().getCurrencyCode())
            .setCaption(i18n.get("cashdesk.fixationGrid.title.currency"));
        column3.setWidth(120);

        Grid.Column<BalanceFixation, String> colDate = fixationGrid.addColumn(item -> item.getFixationDate().format(dateTimeFormatter))
            .setCaption(i18n.get("cashdesk.fixationGrid.title.fixationDate"));

        fixationGrid.addColumn(item -> item.getBalanceStart() == null ? "" : moneyFormat.format(item.getBalanceStart()))
            .setCaption(i18n.get("cashdesk.fixationGrid.title.balanceStart"))
            .setStyleGenerator(item -> "v-align-right");

        fixationGrid.addColumn(item -> item.getBalanceEnd() == null ? "" : moneyFormat.format(item.getBalanceEnd()))
            .setCaption(i18n.get("cashdesk.fixationGrid.title.balanceEnd"))
            .setStyleGenerator(item -> "v-align-right");

        fixationGrid.addColumn(item -> item.getIncomePeriod() == null ? "" :
            item.getIncomePeriod().compareTo(compare) != 0 ?
                String.format("+%s", moneyFormat.format(item.getIncomePeriod())) : moneyFormat.format(item.getIncomePeriod()))
            .setCaption(i18n.get("cashdesk.fixationGrid.title.incomePeriod"))
            .setStyleGenerator(item -> item.getIncomePeriod().compareTo(compare) != 0 ? "income-grid-color" : "v-align-right");

        fixationGrid.addColumn(item -> item.getExpensePeriod() == null ? "" :
            item.getExpensePeriod().compareTo(compare) != 0 ?
                String.format("-%s", moneyFormat.format(item.getExpensePeriod())) : moneyFormat.format(item.getExpensePeriod()))
            .setCaption(i18n.get("cashdesk.fixationGrid.title.expensePeriod"))
            .setStyleGenerator(item -> item.getExpensePeriod().compareTo(compare) != 0 ? "expense-grid-color" : "v-align-right");

        fixationGrid.addColumn(item -> item.getUser() == null ? "" : item.getUser().getUsername())
            .setCaption(i18n.get("cashdesk.fixationGrid.title.userName"))
            .setStyleGenerator(item -> "v-align-right");

        fixationGrid.addComponentColumn(item -> {
            try {
                Button download = new Button(VaadinIcons.DOWNLOAD);
                download.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

                String fileInPath = unloadCashBookService.getFileInPath(dateTimeFormatter, item.getDepartment(),
                    item.getFixationDate());
                File file = new File(fileInPath);
                //if (!file.exists()) file.createNewFile();

                InputStream inputStream = new FileInputStream(file);

                int idx = fileInPath.replaceAll("\\\\", "/").lastIndexOf("/");
                String fileName = idx >= 0 ? fileInPath.substring(idx + 1) : fileInPath;

                StreamResource streamResource = new StreamResource(() -> inputStream, fileName);
                FileDownloader downloader = new FileDownloader(streamResource);
                downloader.extend(download);

                return download;
            } catch (IOException e) {
                //Notification.show(i18n.get("cashdesk.download.fileNotFound"), Notification.Type.ERROR_MESSAGE);
                return null;
            }
        }).setCaption(i18n.get("cashdesk.fixationGrid.title.fileDownload"));


        fixationGrid.sort(colDate, SortDirection.DESCENDING);

        balanceFixationList = balanceFixationService.findAllBalanceFixationsByDepartment(departmentCashState.getValue());
        fixationGrid.setItems(balanceFixationList);

        Label label3 = new Label(i18n.get("cashdesk.label.fixationInfo"));

        deleteLastFixation = new Button(i18n.get("cashdesk.button.deleteLastFixation"), VaadinIcons.TRASH);
        deleteLastFixation.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        deleteLastFixation.setVisible(securityService.hasAuthority(Roles.Operation.DELETE_LAST_FIXATION));

        deleteLastFixation.setEnabled(balanceFixationList.size() > 0);

        deleteLastFixation.addClickListener(event -> {
                ConfirmDialog dialog = new ConfirmDialog(
                    i18n.get("cashdesk.deletelast.dialog.caption"),
                    i18n.get("cashdesk.deletelast.dialog.question"),
                    () -> deleteDeleteLastFixation(departmentCashState.getValue()),
                    () -> {
                    }
                );
                dialog.setClosable(true);
                dialog.show();
            }
        );

        prepareCashBook = new Button(i18n.get("cashdesk.button.prepareCashBook"), VaadinIcons.COMPILE);
        prepareCashBook.setEnabled(false);
        prepareCashBook.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        prepareCashBook.addClickListener(event -> {
            unloadCashBookService.createUnload(configurationService,
                chronometerService, departmentCashState.getValue(), fixationGrid.asSingleSelect().getValue());
            fixationGrid.setItems(balanceFixationList);
        });

        fixationGrid.addSelectionListener(event -> {
            prepareCashBook.setEnabled(event.getFirstSelectedItem().isPresent());
        });

        hlAction = new HorizontalLayout(label3, deleteLastFixation, prepareCashBook);
        hlAction.setWidth(100, Unit.PERCENTAGE);
        hlAction.setExpandRatio(label3, 1);

        VerticalLayout fixationLayout = new VerticalLayout(hlAction, fixationGrid);
        fixationLayout.addStyleName(MaterialTheme.CARD_2);

        layout.addComponents(hlActions, hrGrid, fixationLayout);

        return layout;
    }

    private Set<DepartmentAccount> getDepartmentAccounts(Department department) {
        return departmentAccountService.findAllByDepartment(department);
    }

    private void getDepartmentData(Department department) {
        accountList = new ArrayList<>(departmentAccountService.findAllByDepartment(department));
        balanceList = new ArrayList<>();
        currencyList = new LinkedHashSet<>();

        LocalDate systemDate = chronometerService.getSystemTime().toLocalDate();
        Instant dateMin = systemDate.atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
        Instant dateMax = systemDate.plusDays(1).atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
        //список расходных операций по отделу за указанный период (день)
        List<Operation> expenseOperationList = operationService.getExpenseOperationsForPeriod(dateMin, dateMax, department);
        List<Operation> incomeOperationList = operationService.getIncomeOperationsForPeriod(dateMin, dateMax, department);

        //список валют
        accountList.forEach(item -> currencyList.add(item.getCurrency()));

        for (Currency currency : currencyList) {

            Balance balance = new Balance();
            balance.setCurrency(currency);
            BigDecimal value = new BigDecimal(0).setScale(scaleValue, scaleMode);

            for (DepartmentAccount account : accountList) {
                if (account.getCurrency() == currency) {
                    value = value.add(account.getBalance());
                }
            }
            balance.setBalance(value);

            BigDecimal expense = new BigDecimal(0);
            if (expenseOperationList != null && expenseOperationList.size() > 0) {
                for (Operation operation : expenseOperationList) {
                    if (operation.getCurrency() == currency)
                        expense = expense.add(operation.getOperationValue());
                }
            }
            BigDecimal income = new BigDecimal(0);
            if (incomeOperationList != null && incomeOperationList.size() > 0) {
                for (Operation operation : incomeOperationList) {
                    if (operation.getCurrency() == currency)
                        income = income.add(operation.getOperationValue());
                }
            }

            balance.setExpense(expense);
            balance.setIncome(income);
            balanceList.add(balance);
        }
    }

    private void fixBalance(Department department) {

        //цикл по балансам валют отдела
        for (Balance balance : balanceList) {
            try {
                balanceFixationService.fixBalance(department, chronometerService.getSystemTime().toLocalDate(),
                    chronometerService.getCurrentZoneId(), balance, securityService.getUser());
                getDepartmentData(department);
                balanceFixationList = balanceFixationService.findAllBalanceFixationsByDepartment(department);
                fixationGrid.setItems(balanceFixationList);
                deleteLastFixation.setEnabled(balanceFixationList.size() > 0);

                Notification.show(i18n.get("cashdesk.message.balanceFixed"), Notification.Type.TRAY_NOTIFICATION);

            } catch (ForbidToFixBalanceException | BalanceAlreadyFixedException e) {
                e.printStackTrace();
                Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.WARNING_MESSAGE);
            } catch (FixBalanceException e) {
                e.printStackTrace();
                Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    private void deleteDeleteLastFixation(Department department) {
        for (Balance balance : balanceList) {
            try {
                balanceFixationService.deleleLastFixation(balance, department, chronometerService.getCurrentZoneId());
                getDepartmentData(department);
                balanceFixationList = balanceFixationService.findAllBalanceFixationsByDepartment(department);
                fixationGrid.setItems(balanceFixationList);
                deleteLastFixation.setEnabled(balanceFixationList.size() > 0);

                Notification.show(i18n.get("cashdesk.message.balanceFixationDeleted"), Notification.Type.TRAY_NOTIFICATION);

            } catch (DeleteLastFixationException e) {
                e.printStackTrace();
                Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void destroy() {
        super.destroy();

        applicationEventBus.unregister(this);
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.cashdesk");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIME;
    }

    @Subscribe
    public void updateOperationEventHandler(UpdateOperationEvent event) {
        updateGridContent();
        getDepartmentData(departmentCashState.getValue());
        accountGrid.setItems(accountList);
        balanceGrid.setItems(balanceList);
    }
}

package ru.loanpro.server.ui.module;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.loan.domain.AbstractLoanProduct;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.domain.LoanProduct;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.loan.repository.LoanProductRepository;
import ru.loanpro.directory.domain.LoanType;
import ru.loanpro.directory.service.LoanTypeService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.server.ui.module.additional.ConfirmDialog;
import ru.loanpro.server.ui.module.component.DirectoryComboBox;
import ru.loanpro.uibasis.event.AddTabEvent;
import ru.loanpro.uibasis.event.RemoveTabEvent;

/**
 * Вкладка работы с кредитным продуктом
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(LoanProduct.class)
public class LoanProductTab extends BaseLoanTab<LoanProduct, LoanProductRepository> {

    @Visible(Roles.LoanProduct.CREATE_REQUEST)
    private Button createRequest;

    @Visible(Roles.LoanProduct.CREATE_LOAN)
    private Button createLoan;

    private TextField loanProductName;

    public LoanProductTab(LoanProduct loanProduct) {
        entity = (loanProduct == null) ? new LoanProduct() : loanProduct;
    }

    @Override
    protected Layout initBaseInformation() {

        loanProductName = new TextField(i18n.get("loanproduct.textfield.loanproductname"));
        loanProductName.setWidth(400, Unit.PIXELS);
        loanProductName.focus();

        DirectoryComboBox<LoanType, LoanTypeService> loanType = new DirectoryComboBox<>(
            i18n.get("scheduleForm.combobox.loantype"), loanTypeService);
        loanType.setEmptySelectionAllowed(false);

        HorizontalLayout horizontalLayout = new HorizontalLayout(loanProductName, loanType);

        TextField loanProductDescription = new TextField(i18n.get("loanproduct.textfield.loanproductdescription"));
        loanProductDescription.setSizeFull();

        Label mainInfo = new Label(i18n.get("loanproduct.panel.maininfo.title"));

        delete = new Button(i18n.get("loan.button.delete"), VaadinIcons.TRASH);
        delete.setEnabled(baseLoanService.canBeDeleted(entity));
        delete.addStyleNames(ValoTheme.BUTTON_BORDERLESS_COLORED);
        delete.addClickListener(event -> {
            ConfirmDialog dialog = new ConfirmDialog(
                i18n.get("loanproduct.dialogDelete.caption"),
                i18n.get("loanproduct.dialogDelete.description"),
                () -> {
                    try {
                        baseLoanService.delete(entity);
                        Notification.show(i18n.get("loanproduct.notification.deleteSuccessful"), Type.TRAY_NOTIFICATION);
                        sessionEventBus.post(new RemoveTabEvent<>(this));
                    } catch (EntityDeleteException e) {
                        Notification.show(i18n.get("loanproduct.notification.deletedError"), Type.ERROR_MESSAGE);
                    }
                });
            dialog.setConfirmButtonIcon(VaadinIcons.TRASH);
            dialog.setClosable(true);
            dialog.show();
        });

        createRequest = new Button(i18n.get("loan.button.createRequest"));
        createRequest.setIcon(VaadinIcons.USER_CLOCK);
        createRequest.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        createRequest.addClickListener(event -> {
            sessionEventBus.post(new AddTabEvent(RequestTab.class, new Request(entity)));
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        createLoan = new Button(i18n.get("loan.button.createLoan"));
        createLoan.setIcon(VaadinIcons.WALLET);
        createLoan.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        createLoan.addClickListener(event -> {
            sessionEventBus.post(new AddTabEvent(LoanTab.class, new Loan(entity)));
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        HorizontalLayout hlButtonAll = getActionButtonsLayout(delete, createRequest, createLoan);
        edit.setVisible(securityService.hasAuthority(Roles.LoanProduct.EDIT));
        delete.setVisible(securityService.hasAuthority(Roles.LoanProduct.DELETE));
        save.addClickListener(event -> save.setEnabled(baseLoanService.canBeDeleted(entity)));

        HorizontalLayout hlActions = new HorizontalLayout(mainInfo, hlButtonAll);
        hlActions.setWidth(100, Unit.PERCENTAGE);
        hlActions.setExpandRatio(hlButtonAll, 1.0f);

        VerticalLayout layout = new VerticalLayout(hlActions, horizontalLayout, loanProductDescription);
        layout.setSizeFull();

        binder.forField(loanProductName)
            .asRequired()
            .bind(AbstractLoanProduct::getName, AbstractLoanProduct::setName);

        binder.bind(loanProductDescription, AbstractLoanProduct::getDescription, AbstractLoanProduct::setDescription);
        binder.bind(loanType, AbstractLoanProduct::getLoanType, AbstractLoanProduct::setLoanType);

        return layout;
    }

    @Override
    protected void buttonsStatusUpdate() {
        super.buttonsStatusUpdate();

        createRequest.setEnabled(entity.getId() != null && entityIsReadOnly);
        createLoan.setEnabled(entity.getId() != null && entityIsReadOnly);
    }

    @Override
    public String getTabName() {
        if (entity.getId() == null) {
            return i18n.get("loanproduct.tab.add.caption");
        } else {
            return i18n.get("loanproduct.tab.edit.caption") + entity.getName();
        }
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIGHT_GREEN;
    }
}

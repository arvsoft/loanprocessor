package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Lists;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.UserError;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.sequence.SequenceElementType;
import ru.loanpro.sequence.domain.Sequence;
import ru.loanpro.sequence.domain.SequenceElement;
import ru.loanpro.sequence.domain.SequenceElementConstant;
import ru.loanpro.sequence.domain.SequenceElementDay;
import ru.loanpro.sequence.domain.SequenceElementMonth;
import ru.loanpro.sequence.domain.SequenceElementNumber;
import ru.loanpro.sequence.domain.SequenceElementYear;
import ru.loanpro.sequence.service.SequenceService;
import ru.loanpro.server.ui.module.event.UpdateSequenceEvent;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.util.Optional;

/**
 * Вкладка редактирования последовательности номеров.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(Sequence.class)
public class SequenceTab extends BaseTab {

    @Autowired
    private SequenceService sequenceService;

    private Sequence sequence;

    private VerticalLayout sequenceElementsLayout;
    private Grid<String> exampleGrid;
    private Binder<Sequence> binder;
    private Button save;
    private Button saveAndClose;

    public SequenceTab(Sequence sequence) {
        this.sequence = (sequence == null) ? new Sequence() : sequence;
    }

    @PostConstruct
    public void init() {
        setSpacing(true);
        setMargin(true);
        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setSpacing(true);
        form.setWidth(800, Unit.PIXELS);
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);

        Label section = new Label(i18n.get("sequence.baseInfo"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        TextField name = new TextField(i18n.get("sequence.name"));
        form.addComponent(name);

        TextField currentValue = new TextField(i18n.get("sequence.currentValue"));
        currentValue.setWidth(200, Unit.PIXELS);
        form.addComponent(currentValue);

        section = new Label(i18n.get("sequence.elementList"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        sequenceElementsLayout = new VerticalLayout();
        sequenceElementsLayout.setSpacing(true);
        sequenceElementsLayout.setMargin(true);
        form.addComponent(sequenceElementsLayout);

        section = new Label(i18n.get("sequence.exampleGrid"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        exampleGrid = new Grid<>();
        exampleGrid.addColumn(String::toString).setCaption(i18n.get("sequence.grid.values")).setSortable(false);
        form.addComponent(exampleGrid);

        save = new Button(i18n.get("sequence.save"));
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        save.setEnabled(false);
        saveAndClose = new Button(i18n.get("sequence.saveAndClose"));
        saveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        saveAndClose.setEnabled(false);

        Optional.of(sequence.getElements())
            .ifPresent(list -> list.forEach(element -> {
                sequenceElementsLayout.addComponent(getElementLayout(element));
                updateExampleGrid();
            }));

        Button buttonAdd = new Button(VaadinIcons.PLUS);
        buttonAdd.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        buttonAdd.addClickListener(event -> {
            ContextMenu contextMenu = new ContextMenu(buttonAdd, true);
            Lists.newArrayList(SequenceElementType.CURRENT_YEAR, SequenceElementType.CURRENT_MONTH, SequenceElementType.CURRENT_DAY)
                .forEach(item -> contextMenu.addItem(i18n.get(item.getName()), e -> addSequenceItem(item)));

            contextMenu.addItem(i18n.get(SequenceElementType.CONSTANT.getName()), e -> addSequenceItem(SequenceElementType.CONSTANT));
            contextMenu.addItem(i18n.get(SequenceElementType.NUMBER.getName()), e -> addSequenceItem(SequenceElementType.NUMBER));

            contextMenu.open(event.getClientX(), event.getClientY());
        });
        sequenceElementsLayout.addComponent(buttonAdd);

        binder = new Binder<>();
        binder.setBean(sequence);
        binder.forField(name)
            .asRequired(i18n.get("sequence.name.error"))
            .bind(Sequence::getName, Sequence::setName);

        binder.forField(currentValue)
            .withConverter(new StringToLongConverter(0L, i18n.get("sequence.currentValue.error")))
            .bind(Sequence::getCurrentValue, Sequence::setCurrentValue);

        binder.addValueChangeListener(event -> {
            updateExampleGrid();
            validateSequence();
        });

        save.addClickListener(event -> {
            sequence = sequenceService.save(binder.getBean());
            sessionEventBus.post(new UpdateSequenceEvent(sequence));
        });

        saveAndClose.addClickListener(event -> {
            save.click();
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        HorizontalLayout hlButtons = new HorizontalLayout(save, saveAndClose);
        hlButtons.setSpacing(true);

        addComponents(hlButtons, form);
        setComponentAlignment(form, Alignment.TOP_CENTER);
        setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
    }

    private void addSequenceItem(SequenceElementType element) {
        SequenceElement sequenceElement = null;
        try
        {
            sequenceElement = element.getAClass().newInstance();
        }
        catch (InstantiationException | IllegalAccessException e)
        {
            e.printStackTrace();
        }

        sequenceElementsLayout.addComponent(
            getElementLayout(sequenceElement), sequenceElementsLayout.getComponentCount() - 1);
        sequence.getElements().add(sequenceElement);
        updateExampleGrid();
        validateSequence();
    }

    private HorizontalLayout getElementLayout(SequenceElement element) {
        HorizontalLayout layout = new HorizontalLayout();
        layout.addStyleName(MaterialTheme.CARD_2);
        layout.setSpacing(true);
        layout.setMargin(new MarginInfo(true, false, false, true));

        if (element instanceof SequenceElementYear) {
            Label label = new Label(i18n.get(SequenceElementType.CURRENT_YEAR.getName()));
            label.setWidth(200, Unit.PIXELS);

            layout.addComponent(label);
            layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
        }

        if (element instanceof SequenceElementMonth) {
            Label label = new Label(i18n.get(SequenceElementType.CURRENT_MONTH.getName()));
            label.setWidth(200, Unit.PIXELS);

            layout.addComponent(label);
            layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
        }

        if (element instanceof SequenceElementDay) {
            Label label = new Label(i18n.get(SequenceElementType.CURRENT_DAY.getName()));
            label.setWidth(200, Unit.PIXELS);

            layout.addComponent(label);
            layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
        }

        if (element instanceof SequenceElementConstant) {
            SequenceElementConstant constant = (SequenceElementConstant) element;

            TextField textField = new TextField();
            textField.setValue(constant.getValue() == null ? "-" : constant.getValue());
            textField.setWidth(200, Unit.PIXELS);
            textField.addValueChangeListener(event -> {
                constant.setValue(event.getValue());
                updateExampleGrid();
                validateSequence();
            });

            layout.addComponent(textField);
            layout.setComponentAlignment(textField, Alignment.MIDDLE_CENTER);
        }

        if (element instanceof SequenceElementNumber) {
            SequenceElementNumber number = (SequenceElementNumber) element;

            CheckBox checkBox = new CheckBox(i18n.get("sequence.element.number.fixLength"));
            checkBox.setValue(number.isFixed());

            TextField textField = new TextField();
            textField.setValue(String.valueOf(number.getLength()));
            textField.setWidth(50, Unit.PIXELS);
            textField.setVisible(number.isFixed());

            textField.addValueChangeListener(event -> {
                if (!event.getValue().matches("[0-9]+") || Integer.parseInt(event.getValue()) <= 0) {
                    textField.setComponentError(new UserError(i18n.get("sequence.element.number.error")));
                    return;
                }
                textField.setComponentError(null);
                number.setLength(Integer.parseInt(event.getValue()));
                updateExampleGrid();
                validateSequence();
            });
            checkBox.addValueChangeListener(event -> {
                textField.setVisible(event.getValue());
                number.setFixed(event.getValue());
                updateExampleGrid();
                validateSequence();
            });

            layout.addComponents(checkBox, textField);
            layout.setComponentAlignment(checkBox, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(textField, Alignment.MIDDLE_CENTER);
        }

        Button removeElement = new Button(VaadinIcons.CLOSE_SMALL);
        removeElement.addStyleNames(MaterialTheme.BUTTON_BORDERLESS, MaterialTheme.BUTTON_SMALL);
        removeElement.addClickListener(event -> {
            sequenceElementsLayout.removeComponent(layout);
            sequence.getElements().remove(element);
            updateExampleGrid();
            validateSequence();
        });
        layout.addComponent(removeElement);

        return layout;
    }

    private void updateExampleGrid() {
        exampleGrid.setItems(sequenceService.generateExampleValues(sequence.getCurrentValue() ,sequence.getElements()));
    }

    private void validateSequence() {
        save.setEnabled(binder.isValid());
        saveAndClose.setEnabled(binder.isValid());
    }

    @Override
    public String getTabName() {
        if (sequence.getId() == null)
            return i18n.get("sequence.tabName.new");
        else
            return String.format(i18n.get("sequence.tabName.edit"), sequence.getName());
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.INDIGO;
    }
}

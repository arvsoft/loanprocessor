package ru.loanpro.server.ui.module.component;

import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.loan.domain.Loan;

import java.util.List;

public class ProlongationsForm extends VerticalLayout {

    private Grid<Loan> grid = new Grid<>();

    public ProlongationsForm(I18N i18n, List<Loan> prolongedLoans) {
        grid.setSizeFull();
        grid.addColumn(item -> item.getRecalculationType() == null ? "" : i18n.get(item.getRecalculationType().getName()))
            .setCaption(i18n.get("prolongationsForm.grid.title.RecalculationType"));
        Grid.Column columnOrNo = grid.addColumn(Loan::getProlongOrdNo)
            .setCaption(i18n.get("prolongationsForm.grid.title.ordNo"));
        grid.addColumn(item -> item.getDepartment() == null ? "" : i18n.get(item.getDepartment().getName()))
            .setCaption(i18n.get("prolongationsForm.grid.title.department"));
        grid.addColumn(Loan::getProlongDate)
            .setCaption(i18n.get("prolongationsForm.grid.title.date"));
        grid.addColumn(Loan::getProlongAgrNumber)
            .setCaption(i18n.get("prolongationsForm.grid.title.prolongAgrNumber"));
        grid.addColumn(Loan::getCurrentBalance)
            .setCaption(i18n.get("prolongationsForm.grid.title.CurrentBalance"));
        grid.addColumn(Loan::getProlongCalculation)
            .setCaption(i18n.get("prolongationsForm.grid.title.CalculationValue"));
        grid.addColumn(Loan::getProlongValue)
            .setCaption(i18n.get("prolongationsForm.grid.title.ProlongValue"));
        grid.addColumn(Loan::getInterestValue)
            .setCaption(i18n.get("prolongationsForm.grid.title.InterestValue"));
        grid.addColumn(Loan::getPenaltyValue)
            .setCaption(i18n.get("prolongationsForm.grid.title.PenaltyValue"));

        grid.setItems(prolongedLoans);
        grid.sort(columnOrNo);

        addComponents(grid);
        setSizeFull();
    }

    public void setItems(List<Loan> prolongedLoans){
        grid.setItems(prolongedLoans);
    }

}

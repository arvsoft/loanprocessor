package ru.loanpro.server.logging.domain;

import ru.loanpro.server.logging.annotation.LogName;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Лог печати сущности.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("ENTITY_PRINT")
@LogName("log.entity.print")
public class LogEntityPrint extends LogEntity {

    /**
     * Имя распечатанного документа в хранилище документов.
     */
    @Column(name = "result")
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}

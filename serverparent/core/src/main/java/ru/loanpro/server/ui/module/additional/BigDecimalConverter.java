package ru.loanpro.server.ui.module.additional;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Конвертер типа данных {@link BigDecimal} для текстового поля. Учитывает локаль при форматировании.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public class BigDecimalConverter implements Converter<String, BigDecimal> {

    private final String errorMessage;
    protected final DecimalFormat format;

    /**
     * Конструктор, принимающий локаль и сообщение об ошибке, если не удаётся
     * преобразовать строку к <code>BigDecimal</code>, с учётом переданной
     * локали. Количество дробных знаков по-умолчанию 2.
     *
     * @param locale локаль для форматирования.
     * @param errorMessage сообщение об ошибке.
     */
    public BigDecimalConverter(Locale locale, String errorMessage) {
        this(locale, 2, errorMessage);
    }

    /**
     * Конструктор, принимающий локаль, количество дробных знаков и сообщение об
     * ошибке, если не удаётся преобразовать строку к <code>BigDecimal</code>, с
     * учётом переданной локали.
     *
     * @param locale локаль для форматирования.
     * @param decimalScale количество дробных знаков.
     * @param errorMessage сообщение об ошибке.
     */
    public BigDecimalConverter(Locale locale, int decimalScale, String errorMessage) {
        this.errorMessage = errorMessage;

        format = (DecimalFormat) NumberFormat.getInstance(locale);
        format.setMinimumFractionDigits(decimalScale);
        format.setMaximumFractionDigits(decimalScale);
    }

    /**
     * Преобразует строку к <code>BigDecimal</code>.
     *
     * @param value преобразуемая строка.
     * @param context контекст перобразования.
     * @return преобразованное значение.
     */
    @Override
    public Result<BigDecimal> convertToModel(String value, ValueContext context) {
        try {
            format.setParseBigDecimal(true);
            BigDecimal result = BigDecimal.valueOf(format.parse(value).doubleValue());

            return Result.ok(result);
        } catch (NumberFormatException | ParseException e) {
            // error is a static helper method that creates a Result
            return Result.error(errorMessage);
        }
    }

    /**
     * Конвертирует <code>BigDecimal</code> в строку с учётом локали.
     *
     * @param value конвертируемое значение.
     * @param context контекст конвертации.
     * @return сконвертированное в строку значение.
     */
    @Override
    public String convertToPresentation(BigDecimal value, ValueContext context) {
        return format.format(value);
    }
}

package ru.loanpro.server.ui.module.util;

import com.google.common.reflect.TypeToken;

/**
 * Вспомогательный класс, содержащий методы для определения класса параметра по
 * типу, указанному в параметрах наследника этого класса.
 *
 * @author Oleg Brizhevatikh
 */
public final class ParameterTypeResolver {

    private ParameterTypeResolver() {
    }

    /**
     * Находит класс параметра по типу, указанному в первом параметре наследника
     * этого класса.
     *
     * @param type
     *        класс-наследник
     * @param baseType
     *        базовый (унаследованный) класс с дженериками.
     * @param <R>
     *        тип параметра.
     * @return класс параметра.
     */
    @SuppressWarnings("unchecked")
    public static <R> Class<R> resolve(Class<?> type, Class<?> baseType) {
        return resolve(type, baseType, 0);
    }

    /**
     * Находит класс параметра по типу, указанному в параметрах наследника этого
     * класса.
     *
     * @param type
     *        класс-наследник
     * @param baseType
     *        базовый (унаследованный) класс с дженериками.
     * @param parameterIndex
     *        индекс параметра типа в классе.
     * @param <R>
     *        тип параметра.
     * @return класс параметра.
     */
    @SuppressWarnings("unchecked")
    public static <R> Class<R> resolve(Class<?> type, Class<?> baseType, int parameterIndex) {
        TypeToken<?> typeToken = TypeToken.of(type);

        return (Class<R>) typeToken.resolveType(
            baseType.getTypeParameters()[parameterIndex]).getRawType();
    }
}

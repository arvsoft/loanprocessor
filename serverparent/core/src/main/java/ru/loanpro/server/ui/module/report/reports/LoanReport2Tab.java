package ru.loanpro.server.ui.module.report.reports;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.payment.service.PaymentService;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.base.report.domain.reports.LoanReport2;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.base.schedule.service.ScheduleService;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.server.ui.module.report.BaseReportTab;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/**
 * Окно отчета по займам.
 * Отчет о распределении статусов займов.
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
public class LoanReport2Tab extends BaseReportTab {

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private PaymentService paymentService;

    private int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    private int scaleValue = 3;

    private CheckBox cbxNotShowEmptyGroups;

    private Grid<LoanReport2> grid;

    private Button prepare;

    private List<LoanReport2> loanReport2List;

    public LoanReport2Tab(Report report) {
        super(report);
    }

    @PostConstruct
    @Override
    public void init() {
        super.init();
    }

    @Override
    protected String getLabelDateDescription() {
        return i18n.get("reports.loan.report2.field.dateDescription");
    }

    @Override
    protected boolean getVisibleBaseSearch() { return true;}

    @Override
    protected Layout initReportInformation() {

        cbxNotShowEmptyGroups = new CheckBox(i18n.get("reports.loan.report2.checkbox.notShowEmptyGroups"));
        cbxNotShowEmptyGroups.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxNotShowEmptyGroups.setValue(true);

        grid = new Grid<>();
        grid.setSizeFull();
        gridColumnBuild();

        VerticalLayout layout = new VerticalLayout(cbxNotShowEmptyGroups, grid);
        layout.setSizeFull();
        layout.addStyleName(MaterialTheme.CARD_2);
        layout.setMargin(false);
        layout.setSpacing(true);

        layout.setExpandRatio(grid, 1);
        layout.setSizeFull();

        return layout;
    }

    protected Layout getActionLayout(Report report) {
        Label mainInfo = new Label(i18n.get(report.getName()));

        prepare = new Button(i18n.get("reporttab.button.prepare"), VaadinIcons.LINE_BAR_CHART);
        prepare.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        HorizontalLayout actionLayout = new HorizontalLayout(mainInfo, prepare);
        actionLayout.setWidth(100, Unit.PERCENTAGE);
        actionLayout.setExpandRatio(mainInfo, 1);

        prepare.addClickListener(e -> {
            loanReport2List = new ArrayList<>();

            List<Currency> loanCurrencies = getLoanCurrencies();

            int loansCount = 0;

            if (loanCurrencies.size() > 0) {
                for (Currency currency : loanCurrencies) {
                    for (LoanStatus status : LoanStatus.values()) {

                        BigDecimal sumLoanBalance = new BigDecimal(0);
                        BigDecimal sumCurrentBalance = new BigDecimal(0);
                        BigDecimal sumScheduleBalance = new BigDecimal(0);
                        BigDecimal sumPaymentBalance = new BigDecimal(0);

                        List<Loan> loanList = getLoans(currency, status);
                        loansCount += loanList.size();

                        for (Loan loan : loanList) {
                            sumLoanBalance = sumLoanBalance.add(loan.getLoanBalance());
                            sumCurrentBalance = sumCurrentBalance.add(loan.getCurrentBalance());
                            sumScheduleBalance = sumScheduleBalance.add(getScheduleBalance(loan));
                            sumPaymentBalance = sumPaymentBalance.add(getPaymentBalance(loan));
                        }

                        LoanReport2 loanReport2 = new LoanReport2();
                        loanReport2.setCurrency(currency);
                        loanReport2.setLoanStatus(status);
                        loanReport2.setNumberLoans(loanList.size());
                        loanReport2.setSumLoanBalance(sumLoanBalance);
                        loanReport2.setSumCurrentBalance(sumCurrentBalance);
                        loanReport2.setSumScheduleBalance(sumScheduleBalance);
                        loanReport2.setSumPaymentBalance(sumPaymentBalance);

                        if (cbxNotShowEmptyGroups.getValue()) {
                            if (loanReport2.getNumberLoans() > 0) {
                                loanReport2List.add(loanReport2);
                            }
                        } else loanReport2List.add(loanReport2);
                    }
                }

                int index = 0;
                for (LoanReport2 loanReport2 : loanReport2List) {

                    BigDecimal percentage = new BigDecimal(loanReport2.getNumberLoans() * 100d / loansCount).setScale(scaleValue, scaleMode);
                    loanReport2.setPercentage(percentage);
                    loanReport2List.set(index, loanReport2);
                    index++;
                }

                grid.setItems(loanReport2List);
            }
        });

        return actionLayout;
    }

    private BigDecimal getScheduleBalance(Loan loan) {
        List<Schedule> list = scheduleService.findAllByLoan(loan);
        BigDecimal result = new BigDecimal(0);

        for (Schedule schedule : list) {
            result = result.add(schedule.getPayment());
        }

        return result;
    }

    private BigDecimal getPaymentBalance(Loan loan) {
        List<Payment> list = paymentService.findAllByLoan(loan);
        BigDecimal result = new BigDecimal(0);

        for (Payment payment : list) {
            result = result.add(payment.getPayment());
        }

        return result;
    }

    private List<Loan> getLoans(Currency currency, LoanStatus status) {

        if (!cbxUseDepartment.getValue() && !cbxUseMainDate.getValue()) {
            return reportService.findAllLoansByCurrencyAndStatus(currency, status);
        }

        if (cbxUseDepartment.getValue() && !cbxUseMainDate.getValue()) {
            return reportService.findAllLoansByCurrencyAndStatusAndDepartment(currency, status, cbDepartment.getValue());
        }

        if (!cbxUseDepartment.getValue() && cbxUseMainDate.getValue()) {
            Instant minDate = dfDateMin.getValue().atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
            Instant maxDate = dfDateMax.getValue().plusDays(1).atStartOfDay(
                chronometerService.getCurrentZoneId()).toInstant();

            return reportService.findAllLoansByCurrencyAndStatusAndDates(currency, status, minDate, maxDate);
        }

        if (cbxUseDepartment.getValue() && cbxUseMainDate.getValue()) {
            Instant minDate = dfDateMin.getValue().atStartOfDay(chronometerService.getCurrentZoneId()).toInstant();
            Instant maxDate = dfDateMax.getValue().plusDays(1).atStartOfDay(
                chronometerService.getCurrentZoneId()).toInstant();

            return reportService.findAllLoansByCurrencyAndStatusAndDepartmentAndDates(
                currency, status, cbDepartment.getValue(), minDate, maxDate);
        }

        return new ArrayList<>();
    }

    private void gridColumnBuild() {
        grid.addColumn(item -> item.getCurrency() == null ? "" : item.getCurrency().getCurrencyCode())
            .setCaption(i18n.get("report.loanReport2.grid.title.currency"));

        grid.addColumn(item -> item.getLoanStatus() == null ? "" : i18n.get(item.getLoanStatus().getName()))
            .setCaption(i18n.get("report.loanReport2.grid.title.loanStatus"));

        grid.addColumn(LoanReport2::getNumberLoans)
            .setCaption(i18n.get("report.loanReport2.grid.title.numberLoans"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> item.getSumLoanBalance() == null ? "" : moneyFormat.format(item.getSumLoanBalance()))
            .setCaption(i18n.get("report.loanReport2.grid.title.sumLoanBalance"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> item.getSumCurrentBalance() == null ? "" : moneyFormat.format(item.getSumCurrentBalance()))
            .setCaption(i18n.get("report.loanReport2.grid.title.sumCurrentBalance"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> item.getSumScheduleBalance() == null ? "" : moneyFormat.format(item.getSumScheduleBalance()))
            .setCaption(i18n.get("report.loanReport2.grid.title.sumScheduleBalance"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> item.getSumPaymentBalance() == null ? "" : moneyFormat.format(item.getSumPaymentBalance()))
            .setCaption(i18n.get("report.loanReport2.grid.title.sumPaymentBalance"))
            .setStyleGenerator(item -> "v-align-right");

        grid.addColumn(item -> item.getPercentage() == null ? "" : percentFormat.format(item.getPercentage()))
            .setCaption(i18n.get("report.loanReport2.grid.title.percentage"))
            .setStyleGenerator(item -> "v-align-right");
    }

    @Override
    public String getTabName() {
        return i18n.get(report.getName());
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIME;
    }
}

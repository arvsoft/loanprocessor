package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.base.NumberService;
import ru.loanpro.base.account.exceptions.AccountNotFoundException;
import ru.loanpro.base.account.exceptions.InsufficientAccountBalanceException;
import ru.loanpro.base.client.service.PhysicalClientService;
import ru.loanpro.base.loan.domain.AbstractLoan;
import ru.loanpro.base.loan.domain.AbstractRequest;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.domain.LoanProduct;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.loan.exceptions.ObjectNotSavedException;
import ru.loanpro.base.loan.repository.BaseLoanRepository;
import ru.loanpro.base.loan.service.BaseLoanService;
import ru.loanpro.base.loan.service.LoanService;
import ru.loanpro.base.operation.event.UpdateOperationEvent;
import ru.loanpro.base.operation.exceptions.OperationNotSavedException;
import ru.loanpro.base.printer.PrintService;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.base.schedule.service.ScheduleService;
import ru.loanpro.directory.service.LoanTypeService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.directory.LoanTermType;
import ru.loanpro.global.directory.PenaltyType;
import ru.loanpro.global.directory.RequestStatus;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.server.ui.module.component.ClientsForm;
import ru.loanpro.server.ui.module.component.PaymentsForm;
import ru.loanpro.server.ui.module.component.ProlongationsForm;
import ru.loanpro.server.ui.module.component.ScheduleForm;
import ru.loanpro.server.ui.module.event.EntityEditingEvent;
import ru.loanpro.server.ui.module.event.ScheduleRecalculatedEvent;
import ru.loanpro.server.ui.module.event.UpdateBaseLoanEvent;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Базовая вкладка для кредитных продуктов, займов и заявок.
 *
 * @author Oleg Brizhevatikh
 */
@NoRepositoryBean
public abstract class BaseLoanTab<T extends BaseLoan, R extends BaseLoanRepository<T>> extends BaseTab {

    @Autowired
    protected LoanService loanService;

    @Autowired
    protected PhysicalClientService clientService;

    @Autowired
    protected LoanTypeService loanTypeService;

    @Autowired
    protected BaseLoanService<T, R> baseLoanService;

    @Autowired
    protected ScheduleService scheduleService;

    @Autowired
    protected NumberService numberService;

    @Autowired
    protected PrintService printService;

    protected Button save;
    protected Button edit;
    protected Button delete;

    protected Boolean entityIsReadOnly;
    protected boolean isScheduledCorrect;

    protected Binder<T> binder;

    protected T entity;
    protected int entityHash;

    protected Loan loanProlonged;//заем пролонгируемый

    protected TabSheet tabsheet = new TabSheet();

    protected String draftNumber;

    protected ScheduleForm<T> scheduleForm;

    protected PaymentsForm paymentsForm;

    protected boolean isInProlongationProcess = false;
    protected ProlongationsForm prolongationsForm;
    protected List<Loan> prolongedLoans;

    protected abstract Layout initBaseInformation();

    public BaseLoanTab() {
    }

    @PostConstruct
    public void init() {
        if (entity.getId() == null) {
            if (entity.getClass() == LoanProduct.class ||
                (entity.getClass() == Request.class && ((Request) entity).getLoanProduct() == null) ||
                (entity.getClass() == Loan.class && ((Loan) entity).getLoanProduct() == null &&
                    ((Loan) entity).getRequest() == null)) {//новый КП, заявка или заем
                initInitialEntityState();
            } else {//переход из кп в заявку, в заем, из заявки в заем

                if (entity.getClass() == Request.class && ((Request) entity).getLoanProduct() != null) {
                    entity.getSchedules().addAll(((Request) entity).getLoanProduct().getSchedules());
                    entity.getSchedules().forEach(item -> item.setId(null));
                }
                if (entity.getClass() == Loan.class && ((Loan) entity).getLoanProduct() != null) {
                    entity.getSchedules().addAll(((Loan) entity).getLoanProduct().getSchedules());
                    entity.getSchedules().forEach(item -> item.setId(null));
                }
                if (entity.getClass() == Loan.class && ((Loan) entity).getRequest() != null) {
                    entity.getSchedules().addAll(((Loan) entity).getRequest().getSchedules());
                    entity.getSchedules().forEach(item -> item.setId(null));
                }
                initInitialEntityState();
                recalculateScheduleDates();
            }
        } else {//существует в базе данных
            entity = baseLoanService.getOneFull(entity.getId());

            //Для кредитного продукта и заявки пересчитываем даты в индивидуальном графике на текущую дату
            if ((entity.getClass() == LoanProduct.class) &&
                (entity.getReturnTerm() == ReturnTerm.INDIVIDUALLY)) {
                recalculateScheduleDates();
            }
        }

        binder = new Binder<>();
        binder.setBean(entity);
        entityIsReadOnly = entity.getId() != null;

        Layout baseLayout = initBaseInformation();
        baseLayout.setSizeFull();
        baseLayout.addStyleName(MaterialTheme.CARD_2);

        scheduleForm = new ScheduleForm<>(
            entity, binder, i18n, chronometerService, configurationService, sessionEventBus);

        tabsheet.setSizeFull();
        tabsheet.addStyleNames(ValoTheme.TABSHEET_FRAMED, ValoTheme.TABSHEET_PADDED_TABBAR);
        tabsheet.addTab(scheduleForm, i18n.get("loan.tabsheet.caption.schedule"), VaadinIcons.TABLE);

        if (entity instanceof AbstractRequest) {
            ClientsForm clientsForm = new ClientsForm(
                (AbstractRequest) entity, (Binder<AbstractRequest>) binder, i18n, clientService, sessionEventBus);
            tabsheet.addTab(clientsForm, i18n.get("loan.tabsheet.caption.client"));
        }

        if (entity instanceof AbstractLoan) {
            paymentsForm = new PaymentsForm(i18n, sessionEventBus, configurationService);
            tabsheet.addTab(paymentsForm, i18n.get("loan.tabsheet.caption.payment"));
            paymentsForm.setItems(((Loan) entity).getPayments());

            prolongedLoans = entity.getId() == null ? new ArrayList<>() : loanService.getProlongedLoans((Loan) entity);
            prolongationsForm = new ProlongationsForm(i18n, prolongedLoans);
            tabsheet.addTab(prolongationsForm, i18n.get("loan.tabsheet.caption.prolongation"));
        }

        addComponents(baseLayout, tabsheet);
        setExpandRatio(tabsheet, 1);

        entityHash = entity.hashCode();
        binder.setReadOnly(entity.getId() != null);
        scheduleForm.calculateScheduleHash();

        binder.addValueChangeListener(event -> {
            buttonsStatusUpdate();
        });
    }

    protected HorizontalLayout getActionButtonsLayout(Button... buttons) {
        edit = new Button(i18n.get("loanproduct.button.edit"));
        edit.setIcon(VaadinIcons.EDIT);
        edit.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        edit.addClickListener(event -> {
            applicationEventBus.post(new EntityEditingEvent<>(entity));
            entityIsReadOnly = false;
            binder.setReadOnly(false);
            buttonsStatusUpdate();
        });

        save = new Button(i18n.get("loanproduct.button.save"));
        save.setIcon(VaadinIcons.ADD_DOCK);
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        save.addClickListener(event -> {
            if (!isInProlongationProcess) {//если пролонгация, то см. LoanProlongationTab (в нем событие переопределено)
                try {
                    baseLoanService.save(entity, draftNumber);

                    entityHash = entity.hashCode();
                    binder.setBean(binder.getBean());
                    binder.setReadOnly(true);
                    entityIsReadOnly = true;
                    buttonsStatusUpdate();
                    Notification.show(i18n.get("baseloan.message.recordsaved"), Notification.Type.TRAY_NOTIFICATION);
                    applicationEventBus.post(new UpdateBaseLoanEvent<>(entity));

                } catch (ObjectNotSavedException e) {
                    entity = null;
                    Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.ERROR_MESSAGE);
                    e.printStackTrace();
                }
                if (entity != null && entity.getClass() == Loan.class) {
                    if (securityService.hasAuthority(Roles.Loan.AUTOOPERATION)){
                        try {
                            loanService.saveOperations((Loan) entity,
                                configurationService.getAutoOperationIssuedMoneyLoanIssued(),
                                configurationService.getAutoOperationIssuedMoneyLoanIssuedProcessed(),
                                chronometerService.getCurrentInstant(),
                                securityService.getUser(),
                                securityService.getDepartment());

                            applicationEventBus.post(new UpdateOperationEvent(entity));
                        } catch (AccountNotFoundException |
                            InsufficientAccountBalanceException | OperationNotSavedException e) {
                            Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.WARNING_MESSAGE);
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        HorizontalLayout layout = new HorizontalLayout(edit, save);
        layout.addComponents(buttons);
        layout.setStyleName("flex-wrap-right-alignment-layout");
        layout.setSpacing(false);

        return layout;
    }

    private void initInitialEntityState() {
        // Проверяем сущность на пустоту, сравнивая с новой сущностью того же типа
        if ((entity instanceof LoanProduct && entity.equals(new LoanProduct()))
            || (entity instanceof Request && entity.equals(new Request()))
            || (entity instanceof Loan && entity.equals(new Loan()))) {

            entity.setCalcScheme(CalcScheme.DIFFERENTIAL);
            entity.setReturnTerm(ReturnTerm.MONTHLY);
            entity.setLoanBalance(new BigDecimal(1000.00));
            entity.setCurrency(configurationService.getCurrencies().get(0));
            entity.setInterestValue(new BigDecimal(1.000));
            entity.setInterestType(InterestType.PERCENT_A_YEAR);
            entity.setLoanTerm(3);
            entity.setLoanTermType(LoanTermType.MONTHS);
            entity.setReplaceDate(false);
            entity.setReplaceDateValue(0);
            entity.setReplaceOverMonth(false);
            entity.setPrincipalLastPay(false);
            entity.setInterestBalance(false);
            entity.setNoInterestFD(false);
            entity.setNoInterestFDValue(0);
            entity.setIncludeIssueDate(false);
            entity.setInterestBalance(false);
            entity.setInterestNumberFirstDays(0);
            entity.setPenaltyType(PenaltyType.FROM_LOAN_VALUE);
            entity.setPenaltyValue(new BigDecimal(1));
            entity.setChargeFine(false);
            entity.setFineValue(new BigDecimal(0));
            entity.setFineOneTime(false);
            entity.setUser(securityService.getUser());
            entity.setInterestTotal(new BigDecimal(0));
        }

        if (entity.getClass() == Request.class) {
            Request request = (Request) entity;

            draftNumber = numberService.getRequestSequenceDraftNumber();
            request.setRequestNumber(draftNumber);
            request.setRequestIssueDate(chronometerService.getCurrentInstant());
            request.setRequestStatus(RequestStatus.NEW);
            request.setDepartment(securityService.getDepartment());
            request.setUser(securityService.getUser());
        }

        if (entity.getClass() == Loan.class) {
            Loan loan = (Loan) entity;
            loan.setCurrentBalance(entity.getLoanBalance());
            draftNumber = numberService.getLoanSequenceDraftNumber();
            loan.setLoanNumber(draftNumber);
            loan.setLoanIssueDate(chronometerService.getCurrentInstant());
            loan.setLoanStatus(LoanStatus.OPENED_NO_OVERDUE);
            loan.setDepartment(securityService.getDepartment());
            loan.setUser(securityService.getUser());
            loan.setProlongOrdNo(0);
            loan.setProlongValue(new BigDecimal(0));
        }
    }

    private void recalculateScheduleDates() {
        LocalDate currentLocalDate = chronometerService.getCurrentTime().toLocalDate();

        List<Schedule> schedules = entity.getSchedules();
        long term = 0;
        for (Schedule schedule : schedules) {
            term += schedule.getTerm();
            schedule.setDate(currentLocalDate.plusDays(term));
        }
    }

    protected void buttonsStatusUpdate() {
        isScheduledCorrect = entity.getReturnTerm() != ReturnTerm.INDIVIDUALLY
            || entity.getSchedules().stream()
            .map(Schedule::getBalance)
            .map(BigDecimal::doubleValue)
            .anyMatch(item -> item == 0.0);

        boolean isLoanIssuedForRequest = false;
        if (entity.getClass() == Request.class)
            isLoanIssuedForRequest = ((Request) entity).getRequestStatus() == RequestStatus.LOAN_ISSUED;

        save.setEnabled(scheduleForm.getScheduleHash() == entity.scheduleHashCode() && isScheduledCorrect &&
            binder.isValid() && entity.hashCode() != entityHash);
        edit.setEnabled(entity.getId() != null && entityIsReadOnly && !isLoanIssuedForRequest);
        scheduleForm.getBtCalculate().setEnabled(!entityIsReadOnly && !isLoanIssuedForRequest);
    }

    @Subscribe
    public void scheduleRecalculatedEventHandler(ScheduleRecalculatedEvent<T> event) {
        if (entity == event.getEntity())
            buttonsStatusUpdate();
    }
}

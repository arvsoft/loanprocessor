package ru.loanpro.server.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.server.ui.module.event.GlobalTimeTickEvent;

@Configuration
public class GlobalEventsConfiguration {

    private final ApplicationEventBus eventBus;

    @Autowired
    public GlobalEventsConfiguration(ApplicationEventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Scheduled(cron = "0 * * * * *")
    public void timeTickEvent() {
        eventBus.post(new GlobalTimeTickEvent());
    }
}

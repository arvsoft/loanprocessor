package ru.loanpro.server.ui.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.Settings;
import ru.loanpro.global.directory.OverdueDaysCalculationType;
import ru.loanpro.global.directory.PaymentSplitType;
import ru.loanpro.global.directory.RecalculationType;
import ru.loanpro.global.directory.RepaymentOrderScheme;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.security.settings.exceptions.SettingsUnsupportedTypeException;
import ru.loanpro.security.settings.service.SettingsService;
import ru.loanpro.global.directory.GridStyle;

import java.util.Currency;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Хэлпер для вкладки Финансовые настройки окна настроек отдела.
 * Эмулирует отображение настроек в виде сущности для биндера.
 * Позволяет осуществлять проверку изменений в настройках сравнением хэш-кодов.
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
public class DepartmentTabConfigurationHelper {

    private final SettingsService settingsService;

    private Currency currencyDefault;

    private RecalculationType recalculationType;
    private OverdueDaysCalculationType overdueDaysCalculationType;
    private RepaymentOrderScheme repaymentOrderScheme;
    private GridStyle gridStyle;

    private boolean summationPenalty;
    private boolean summationFine;

    private boolean autoOperationIssueMoneyLoanIssued;
    private boolean autoOperationReplenishMoneyLoanPayment;

    private boolean autoOperationIssueMoneyLoanIssuedProcessed;
    private boolean autoOperationReplenishMoneyLoanPaymentProcessed;

    private boolean keepSameOperationDateWhenSaveFromPreparedToProcessed;

    private PaymentSplitType paymentSplitType;

    @Autowired
    public DepartmentTabConfigurationHelper(SettingsService settingsService, ConfigurationService configurationService) {
        this.settingsService = settingsService;
    }

    public void load(Department department, List<Currency> currencies) {

        if (currencies != null && currencies.size() > 0) {
            currencyDefault = Stream.of(
                settingsService.get(department, String.class,
                Settings.CURRENCY_DEFAULT, currencies.get(0).getCurrencyCode()))
                .map(Currency::getInstance)
                .findFirst().get();
        }

        gridStyle = settingsService.get(department, GridStyle.class, Settings.GRID_STYLE, GridStyle.NORMAL);

        recalculationType = settingsService.get(department, RecalculationType.class,
            Settings.RECALCULATION_TYPE, RecalculationType.RECALCULATION_ON_NEW_TERM);

        overdueDaysCalculationType = settingsService.get(department, OverdueDaysCalculationType.class,
            Settings.OVERDUE_DAYS_CALCULATION_TYPE, OverdueDaysCalculationType.FROM_DATE_ACCORDING_SCHEDULE);

        repaymentOrderScheme = settingsService.get(department, RepaymentOrderScheme.class,
            Settings.REPAYMENT_ORDER_SCHEME, RepaymentOrderScheme.ORDER1);

        summationPenalty = settingsService.get(department, Boolean.class, Settings.SUMMATION_PENALTY, true);

        summationFine = settingsService.get(department, Boolean.class, Settings.SUMMATION_FINE, true);

        autoOperationIssueMoneyLoanIssued = settingsService.get(department, Boolean.class,
            Settings.AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED, false);

        autoOperationReplenishMoneyLoanPayment = settingsService.get(department, Boolean.class,
            Settings.AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT, false);

        autoOperationIssueMoneyLoanIssuedProcessed = settingsService.get(department, Boolean.class,
            Settings.AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED_PROCESSED, false);

        autoOperationReplenishMoneyLoanPaymentProcessed = settingsService.get(department, Boolean.class,
            Settings.AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT_PROCESSED, false);

        paymentSplitType = settingsService.get(department, PaymentSplitType.class,
            Settings.PAYMENT_SPLIT_TYPE, PaymentSplitType.ONE_TRANSACTION);

        keepSameOperationDateWhenSaveFromPreparedToProcessed = settingsService.get(department, Boolean.class,
            Settings.KEEP_SAME_OPERATION_DATE_WHEN_TURN_FROM_PREPARED_TO_PROCESSED, false);
    }

    public void save(Department department) {
        try {
            if (currencyDefault != null) {
                settingsService.set(department, Settings.CURRENCY_DEFAULT, currencyDefault.getCurrencyCode());
            }

            settingsService.set(department, Settings.GRID_STYLE, gridStyle);
            settingsService.set(department, Settings.RECALCULATION_TYPE, recalculationType);
            settingsService.set(department, Settings.OVERDUE_DAYS_CALCULATION_TYPE, overdueDaysCalculationType);
            settingsService.set(department, Settings.REPAYMENT_ORDER_SCHEME, repaymentOrderScheme);
            settingsService.set(department, Settings.SUMMATION_PENALTY, summationPenalty);
            settingsService.set(department, Settings.SUMMATION_FINE, summationFine);

            settingsService.set(department, Settings.AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED, autoOperationIssueMoneyLoanIssued);
            settingsService.set(department, Settings.AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT, autoOperationReplenishMoneyLoanPayment);

            settingsService.set(department, Settings.AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED_PROCESSED, autoOperationIssueMoneyLoanIssuedProcessed);
            settingsService.set(department, Settings.AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT_PROCESSED, autoOperationReplenishMoneyLoanPaymentProcessed);

            settingsService.set(department, Settings.PAYMENT_SPLIT_TYPE, paymentSplitType);

            settingsService.set(department, Settings.KEEP_SAME_OPERATION_DATE_WHEN_TURN_FROM_PREPARED_TO_PROCESSED, keepSameOperationDateWhenSaveFromPreparedToProcessed);
        } catch (SettingsUnsupportedTypeException e) {
            e.printStackTrace();
        }
    }

    public Currency getCurrencyDefault() {
        return currencyDefault;
    }

    public void setCurrencyDefault(Currency currencyDefault) {
        this.currencyDefault = currencyDefault;
    }

    public GridStyle getGridStyle() {
        return gridStyle;
    }

    public void setGridStyle(GridStyle gridStyle) {
        this.gridStyle = gridStyle;
    }

    public boolean isAutoOperationIssueMoneyLoanIssued() {
        return autoOperationIssueMoneyLoanIssued;
    }

    public void setAutoOperationIssueMoneyLoanIssued(boolean autoOperationIssueMoneyLoanIssued) {
        this.autoOperationIssueMoneyLoanIssued = autoOperationIssueMoneyLoanIssued;
    }

    public boolean isAutoOperationReplenishMoneyLoanPayment() {
        return autoOperationReplenishMoneyLoanPayment;
    }

    public void setAutoOperationReplenishMoneyLoanPayment(boolean autoOperationReplenishMoneyLoanPayment) {
        this.autoOperationReplenishMoneyLoanPayment = autoOperationReplenishMoneyLoanPayment;
    }

    public boolean isAutoOperationIssueMoneyLoanIssuedProcessed() {
        return autoOperationIssueMoneyLoanIssuedProcessed;
    }

    public void setAutoOperationIssueMoneyLoanIssuedProcessed(boolean autoOperationIssueMoneyLoanIssuedProcessed) {
        this.autoOperationIssueMoneyLoanIssuedProcessed = autoOperationIssueMoneyLoanIssuedProcessed;
    }

    public boolean isAutoOperationReplenishMoneyLoanPaymentProcessed() {
        return autoOperationReplenishMoneyLoanPaymentProcessed;
    }

    public void setAutoOperationReplenishMoneyLoanPaymentProcessed(boolean autoOperationReplenishMoneyLoanPaymentProcessed) {
        this.autoOperationReplenishMoneyLoanPaymentProcessed = autoOperationReplenishMoneyLoanPaymentProcessed;
    }

    public SettingsService getSettingsService() {
        return settingsService;
    }

    public RecalculationType getRecalculationType() {
        return recalculationType;
    }

    public void setRecalculationType(RecalculationType recalculationType) {
        this.recalculationType = recalculationType;
    }

    public OverdueDaysCalculationType getOverdueDaysCalculationType() {
        return overdueDaysCalculationType;
    }

    public void setOverdueDaysCalculationType(OverdueDaysCalculationType overdueDaysCalculationType) {
        this.overdueDaysCalculationType = overdueDaysCalculationType;
    }

    public RepaymentOrderScheme getRepaymentOrderScheme() {
        return repaymentOrderScheme;
    }

    public void setRepaymentOrderScheme(RepaymentOrderScheme repaymentOrderScheme) {
        this.repaymentOrderScheme = repaymentOrderScheme;
    }

    public boolean isSummationPenalty() {
        return summationPenalty;
    }

    public void setSummationPenalty(boolean summationPenalty) {
        this.summationPenalty = summationPenalty;
    }

    public boolean isSummationFine() {
        return summationFine;
    }

    public void setSummationFine(boolean summationFine) {
        this.summationFine = summationFine;
    }

    public PaymentSplitType getPaymentSplitType() {
        return paymentSplitType;
    }

    public void setPaymentSplitType(PaymentSplitType paymentSplitType) {
        this.paymentSplitType = paymentSplitType;
    }

    public boolean isKeepSameOperationDateWhenSaveFromPreparedToProcessed() {
        return keepSameOperationDateWhenSaveFromPreparedToProcessed;
    }

    public void setKeepSameOperationDateWhenSaveFromPreparedToProcessed(boolean keepSameOperationDateWhenSaveFromPreparedToProcessed) {
        this.keepSameOperationDateWhenSaveFromPreparedToProcessed = keepSameOperationDateWhenSaveFromPreparedToProcessed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentTabConfigurationHelper that = (DepartmentTabConfigurationHelper) o;
        return recalculationType == that.recalculationType &&
            gridStyle == that.gridStyle &&
            overdueDaysCalculationType == that.overdueDaysCalculationType &&
            repaymentOrderScheme == that.repaymentOrderScheme &&
            summationPenalty == that.summationPenalty &&
            summationFine == that.summationFine &&
            autoOperationIssueMoneyLoanIssued == that.autoOperationIssueMoneyLoanIssued &&
            autoOperationReplenishMoneyLoanPayment == that.autoOperationReplenishMoneyLoanPayment;
    }

    @Override
    public int hashCode() {

        return Objects.hash(recalculationType, gridStyle, overdueDaysCalculationType, summationPenalty, summationFine,
            autoOperationIssueMoneyLoanIssued, autoOperationReplenishMoneyLoanPayment);
    }
}

package ru.loanpro.server.ui.module;

import com.google.common.eventbus.Subscribe;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.vaadin.data.HasValue;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.client.domain.ClientQueryDSL;
import ru.loanpro.base.loan.domain.QRequest;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.loan.event.UpdateRequestStatusEvent;
import ru.loanpro.base.loan.repository.RequestRepository;
import ru.loanpro.base.loan.service.RequestService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.global.directory.RequestStatus;
import ru.loanpro.global.domain.BaseQueryDSL;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.server.ui.module.additional.MoneyField;

import javax.annotation.PostConstruct;
import java.time.format.DateTimeFormatter;

/**
 * Вкладка для работы с заявками.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.requests")
public class RequestsTab extends BaseLoansTab<Request> {

    private final DepartmentService departmentService;

    private TextField requestNumber;
    private ComboBox<RequestStatus> requestStatus;
    private DateField requestIssueDate;
    private TextField clientName;
    private MoneyField loanBalance;
    private ComboBox<Department> department;
    private DateTimeFormatter dateTimeFormatter2;

    @Autowired
    public RequestsTab(RequestService requestService, DepartmentService departmentService) {
        super(requestService);
        this.departmentService = departmentService;
    }

    @Override
    protected void gridColumnsBuild() {
        dateTimeFormatter2 =
            DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

        float k = 1.0f;
        if (configurationService.getGridStyle() == GridStyle.SMALL) {
            grid.addStyleName("v-grid-style-small");
            grid.setRowHeight(40f);
        } else k = 1.3f;

        Grid.Column<Request, String> colDepartment = grid.addColumn(item -> item.getDepartment().getName());
        colDepartment
            .setSortProperty("department")
            .setCaption(i18n.get("requests.grid.title.department"))
            .setWidth(250 * k);

        Grid.Column<Request, String> colRequestStatus = grid.addColumn(item ->
            i18n.get(item.getRequestStatus().getName()));
        colRequestStatus
            .setSortProperty("requestStatus")
            .setCaption(i18n.get("requests.grid.title.requeststatus"))
            .setStyleGenerator(item -> item.getRequestStatus().getColor())
            .setWidth(150 * k);

        Grid.Column<Request, String> colRequestNumber = grid.addColumn(Request::getRequestNumber);
        colRequestNumber
            .setSortProperty("requestNumber")
            .setCaption(i18n.get("requests.grid.title.requestnumber"))
            .setWidth(100 * k);

        Grid.Column<Request, String> colRequestIssueDate = grid.addColumn(item ->
            item.getRequestIssueDate() == null ? ""
                : chronometerService.zonedDateTimeToString(item.getRequestIssueDate(), dateTimeFormatter2));
        colRequestIssueDate
            .setSortProperty("requestIssueDate")
            .setCaption(i18n.get("requests.grid.title.issuedate"))
            .setWidth(140 * k);

        Grid.Column<Request, String> colClientName = grid.addColumn(item -> item.getBorrower().getDisplayName())
            .setCaption(i18n.get("requests.grid.title.borrower"))
            .setWidth(220 * k);

        Grid.Column<Request, String> colLoanBalance = grid.addColumn(item ->
            moneyFormat.format(item.getLoanBalance()));
        colLoanBalance
            .setSortProperty("loanBalance")
            .setCaption(i18n.get("requests.grid.title.loanbalance"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(120 * k);

        grid.addColumn(item -> item.getCurrency().getCurrencyCode())
            .setCaption(i18n.get("requests.grid.title.currency"))
            .setWidth(70 * k);

        grid.addColumn(item -> percentFormat.format(item.getInterestValue()))
            .setCaption(i18n.get("requests.grid.title.interestvalue"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(100 * k);

        grid.addColumn(item -> i18n.get(item.getInterestType().getName()))
            .setCaption(i18n.get("requests.grid.title.interesttype"))
            .setWidth(100 * k);

        grid.addColumn(Request::getLoanTerm)
            .setCaption(i18n.get("requests.grid.title.loanterm"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(70 * k);

        grid.addColumn(item -> i18n.get(item.getLoanTermType().getName()))
            .setCaption(i18n.get("requests.grid.title.loantermtype"))
            .setWidth(100 * k);

        grid.addColumn(item -> i18n.get(item.getReturnTerm().getName()))
            .setCaption(i18n.get("requests.grid.title.returnterm"))
            .setWidth(100 * k);

        grid.sort(colRequestIssueDate);

        HeaderRow headerRow = grid.addHeaderRowAt(1);

        department = new ComboBox();
        department.setItemCaptionGenerator(Department::getName);
        department.setItems(departmentService.findAll());
        department.setWidth(String.valueOf(colDepartment.getWidth() * 0.9f));
        headerRow.getCell(colDepartment).setComponent(department);

        requestStatus = new ComboBox<>();
        requestStatus.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        requestStatus.setItems(RequestStatus.values());
        requestStatus.setWidth(String.valueOf(colRequestStatus.getWidth() * 0.9f));
        headerRow.getCell(colRequestStatus).setComponent(requestStatus);

        requestNumber = new TextField();
        requestNumber.setWidth(String.valueOf(colRequestNumber.getWidth() * 0.9f));
        headerRow.getCell(colRequestNumber).setComponent(requestNumber);

        requestIssueDate = new DateField();
        requestIssueDate.setDateFormat(configurationService.getDateFormat());
        requestIssueDate.setWidth(String.valueOf(colRequestIssueDate.getWidth() * 0.9f));
        headerRow.getCell(colRequestIssueDate).setComponent(requestIssueDate);

        clientName = new TextField();
        clientName.setWidth(String.valueOf(colClientName.getWidth() * 0.9f));
        headerRow.getCell(colClientName).setComponent(clientName);

        loanBalance = new MoneyField(configurationService.getLocale());
        loanBalance.setWidth(String.valueOf(colLoanBalance.getWidth() * 0.9f));
        headerRow.getCell(colLoanBalance).setComponent(loanBalance);

        grid.addColumnResizeListener(event -> {
            if (event.getColumn() == colDepartment) {
                department.setWidth(String.valueOf(colDepartment.getWidth() * 0.9f));
            } else if (event.getColumn() == colRequestStatus) {
                requestStatus.setWidth(String.valueOf(colRequestStatus.getWidth() * 0.9f));
            } else if (event.getColumn() == colRequestNumber) {
                requestNumber.setWidth(String.valueOf(colRequestNumber.getWidth() * 0.9f));
            } else if (event.getColumn() == colRequestIssueDate) {
                requestIssueDate.setWidth(String.valueOf(colRequestIssueDate.getWidth() * 0.9f));
            } else if (event.getColumn() == colClientName) {
                clientName.setWidth(String.valueOf(colClientName.getWidth() * 0.9f));
            } else if (event.getColumn() == colLoanBalance) {
                loanBalance.setWidth(String.valueOf(colLoanBalance.getWidth() * 0.9f));
            }
        });

        headerRow.getComponents().stream()
            .filter(item -> item instanceof HasValue)
            .map(item -> (HasValue) item)
            .forEach(item -> item.addValueChangeListener(event -> updateGridContent()));
    }

    @Override
    protected Predicate getPredicate() {
        QRequest request = QRequest.request;

        return new BooleanBuilder()
            .and(BaseQueryDSL.eq(request.requestNumber, requestNumber))
            .and(BaseQueryDSL.eq(request.requestStatus, requestStatus))
            .and(BaseQueryDSL.eq(request.requestIssueDate, requestIssueDate))
            .and(ClientQueryDSL.displayNameLike(request.borrower, clientName))
            .and(BaseQueryDSL.eq(request.loanBalance, loanBalance, configurationService.getLocale()))
            .and(BaseQueryDSL.eq(request.department, department))
            .getValue();
    }

    @Override
    protected String getCreateButtonRole() {
        return Roles.Request.CREATE;
    }

    @Override
    protected Class<? extends BaseLoanTab<Request, RequestRepository>> getEntityTabClass() {
        return RequestTab.class;
    }

    @PostConstruct
    public void init() {
        super.init();

        applicationEventBus.register(this);
    }

    @Override
    public void destroy() {
        super.destroy();

        applicationEventBus.unregister(this);
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.requests");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.PURPLE;
    }

    @Subscribe
    public void updateRequestStatusEventHandler(UpdateRequestStatusEvent event) {
        grid.getDataProvider().refreshItem(event.getRequest());
    }
}

package ru.loanpro.server.ui.module;

import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.license.client.service.LicenseService;
import ru.loanpro.security.domain.Credentials;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.Group;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.UserService;
import ru.loanpro.server.ui.module.component.LicenseForm;
import ru.loanpro.server.ui.module.event.UpdateSecurityGroupEvent;
import ru.loanpro.server.ui.module.event.UpdateUserEvent;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.AddTabEvent;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;

/**
 * Вкладка настройки пользователей системы.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.security")
public class SecurityTab extends BaseTab {

    @Autowired
    private UserService userService;

    @Autowired
    private LicenseForm licenseForm;

    @Autowired
    private LicenseService licenseService;

    private Grid<User> usersGrid;
    private Grid<Group> groupsGrid;

    @PostConstruct
    public void init() {
        setMargin(false);
        usersGrid = initUsersGrid();
        groupsGrid = initGroupsGrid();

        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleNames(ValoTheme.TABSHEET_FRAMED, ValoTheme.TABSHEET_PADDED_TABBAR);

        tabSheet.setSizeFull();
        tabSheet.addTab(usersGrid, i18n.get("security.users.table"), VaadinIcons.USER);
        tabSheet.addTab(groupsGrid, i18n.get("security.groups.table"), VaadinIcons.GROUP);
        tabSheet.addTab(licenseForm, i18n.get("security.security.form"), VaadinIcons.KEY);

        addComponent(tabSheet);
        setSizeFull();
    }

    private Grid<User> initUsersGrid() {
        Grid<User> grid = new Grid<>("", userService.findAllUsers());
        grid.setSizeFull();

        Grid.Column<User, String> usernameColumn = grid.addColumn(User::getUsername);
        usernameColumn.setCaption(i18n.get("security.column.username"));
        grid.sort(usernameColumn);

        grid.addColumn(item ->
            item.getCredentials()
                .stream()
                .map(Credentials::getDepartment)
                .map(Department::getName)
                .collect(Collectors.joining(", ")))
            .setCaption(i18n.get("security.column.department"));

        grid.addComponentColumn(item -> {
            CheckBox checkBox = new CheckBox();
            checkBox.setValue(item.isEnabled());
            checkBox.setEnabled(false);

            return checkBox;
        }).setCaption(i18n.get("security.column.enabled"));


        GridContextMenu<User> panelMenu = new GridContextMenu<>(grid);
        panelMenu.addGridBodyContextMenuListener(event -> {
            event.getContextMenu().removeItems();

            User user = (User) event.getItem();
            Integer activeUserCount = userService.getActiveUserCount();
            Integer licenseCount = licenseService.getUserCount();

            if (securityService.hasAuthority(Roles.Security.USER_EDIT) && user != null && user.isEnabled())
                event.getContextMenu()
                    .addItem(i18n.get("security.context.user.disable"), VaadinIcons.CLOSE_CIRCLE, disableEvent -> {
                        userService.setDisable(user);
                        updateUserList(grid);
                    });

            if (securityService.hasAuthority(Roles.Security.USER_EDIT) && user != null && !user.isEnabled()
                && activeUserCount < licenseCount) {
                event.getContextMenu()
                    .addItem(i18n.get("security.context.user.enable"), VaadinIcons.CHECK_CIRCLE, enableEvent -> {
                        userService.setEnable(user);
                        updateUserList(grid);
                    });
            }

            if (securityService.hasAuthority(Roles.Security.USER_EDIT) && event.getItem() != null)
                event.getContextMenu()
                    .addItem(i18n.get("security.context.user.edit"), VaadinIcons.EDIT,
                        editEvent -> sessionEventBus.post(new AddTabEvent(UserTab.class, event.getItem())));

            if (securityService.hasAuthority(Roles.Security.USER_CREATE))
                event.getContextMenu().addItem(i18n.get("security.context.user.new"), VaadinIcons.PLUS,
                    addEvent -> {
                    if (activeUserCount < licenseCount)
                        sessionEventBus.post(new AddTabEvent(UserTab.class));
                    else
                        Notification.show(i18n.get("security.context.user.notEnoughLicense"),
                            i18n.get("security.context.user.notEnoughLicenseDescription"), Type.ERROR_MESSAGE);
                    });
        });

        return grid;
    }

    private void updateUserList(Grid<User> grid) {
        grid.setItems(userService.findAllUsers());
    }

    private Grid<Group> initGroupsGrid() {
        Grid<Group> grid = new Grid<>("", userService.findAllGroups());
        grid.setSizeFull();

        Grid.Column<Group, String> nameColumn = grid.addColumn(Group::getName)
            .setCaption(i18n.get("security.column.groupName"))
            .setExpandRatio(1);
        grid.sort(nameColumn, SortDirection.ASCENDING);

        grid.addColumn(Group::getDescription)
            .setCaption(i18n.get("security.column.groupDescription"))
            .setExpandRatio(2);

        GridContextMenu<Group> panelMenu = new GridContextMenu<>(grid);
        panelMenu.addGridBodyContextMenuListener(event -> {
            event.getContextMenu().removeItems();
            if (securityService.hasAuthority(Roles.Security.GROUP_EDIT) && event.getItem() != null)
                event.getContextMenu()
                    .addItem(i18n.get("security.context.group.edit"), VaadinIcons.EDIT,
                        editEvent -> sessionEventBus.post(new AddTabEvent(GroupTab.class, event.getItem())));

            if (securityService.hasAuthority(Roles.Security.GROUP_CREATE))
                event.getContextMenu().addItem(i18n.get("security.context.group.new"), VaadinIcons.PLUS,
                    addEvent -> sessionEventBus.post(new AddTabEvent(GroupTab.class)));
        });

        return grid;
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.security");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.RED;
    }

    @Subscribe
    private void updateUserEventHandler(UpdateUserEvent event) {
        usersGrid.setItems(userService.findAllUsers());
    }

    @Subscribe
    private void updateSecurityGroupEventHandler(UpdateSecurityGroupEvent event) {
        groupsGrid.setItems(userService.findAllGroups());
    }
}

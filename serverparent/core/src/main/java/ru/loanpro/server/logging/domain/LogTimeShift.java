package ru.loanpro.server.logging.domain;

import ru.loanpro.server.logging.annotation.LogName;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.Instant;

/**
 * Лог изменения текущего времени сессии пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Entity
@DiscriminatorValue("TIME_SHIFT")
@LogName("log.timeShift")
public class LogTimeShift extends AbstractLog {

    /**
     * Установленное пользователем время.
     */
    @Column(name = "time_shift")
    private Instant timeShift;

    public Instant getTimeShift() {
        return timeShift;
    }

    public void setTimeShift(Instant timeShift) {
        this.timeShift = timeShift;
    }
}

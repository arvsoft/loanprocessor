package ru.loanpro.server.ui.module.event;

import java.time.ZonedDateTime;

/**
 * Событие тика времени. Происходит раз в минуту и распространяется шиной событий
 * или в момент изменения текущего времени пользователем. Должно быть обработано
 * во всех подсистемах зависящих от текущего времени сессии пользователя.
 *
 * @author Oleg Brizhevatikh
 */
public class TimeTickEvent {

    private ZonedDateTime time;

    /**
     * Конструктор, принимающий текущее время сесии.
     *
     * @param time текущее время сессии.
     */
    public TimeTickEvent(ZonedDateTime time) {
        this.time = time;
    }

    /**
     * Возвращает текущее время сессии.
     *
     * @return текущее время сессии.
     */
    public ZonedDateTime getTime() {
        return time;
    }
}

package ru.loanpro.server.ui.module;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.contextmenu.GridContextMenu.GridContextMenuOpenListener;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.directory.domain.AbstractIdDirectory;
import ru.loanpro.directory.domain.EducationLevel;
import ru.loanpro.directory.service.DirectoryService;
import ru.loanpro.global.UiTheme;
import ru.loanpro.server.ui.module.util.ParameterTypeResolver;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Вкладка для работы со справочниками.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.directories")
public class DirectoriesTab extends BaseTab {

    @Autowired
    private List<DirectoryService> services;

    @PostConstruct
    public void init() {
        VerticalLayout leftLayout = new VerticalLayout();
        leftLayout.setSizeFull();
        leftLayout.setMargin(false);

        VerticalLayout rightLayout = new VerticalLayout();
        rightLayout.setSizeFull();
        rightLayout.setMargin(false);

        Grid<DirectoryService> directoriesGrid = new Grid<>(i18n.get("core.menu.directories"), services);
        directoriesGrid.setSizeFull();
        leftLayout.addComponent(directoriesGrid);

        Grid.Column<DirectoryService, String> columnName = directoriesGrid
            .addColumn(col -> i18n.get("directory." + col.getClass().getSimpleName()))
            .setCaption(i18n.get("directory.directory_name"));

        directoriesGrid.sort(columnName, SortDirection.ASCENDING);

        HorizontalSplitPanel splitPanel = new HorizontalSplitPanel(leftLayout, rightLayout);
        splitPanel.setSplitPosition(30, Unit.PERCENTAGE);
        splitPanel.setSizeFull();
        addComponent(splitPanel);
        setSizeFull();

        directoriesGrid.addItemClickListener(e -> {
            DirectoryService service = e.getItem();
            if (service == null) return;

            DirectoryGridConstructor directoryGridConstructor =
                new DirectoryGridConstructor(i18n.get("directory." + service.getClass().getSimpleName()), service);
            Grid grid = directoryGridConstructor.getGrid();
            grid.setSizeFull();

            rightLayout.removeAllComponents();
            rightLayout.addComponent(grid);
        });
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.directories");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.DEEP_ORANGE;
    }

    private class DirectoryGridConstructor<D extends AbstractIdDirectory, S extends DirectoryService> {
        private final Grid<D> grid;
        private S service;
        private final Class<AbstractIdDirectory> aClass;

        DirectoryGridConstructor(String caption, S service) {
            this.service = service;
            aClass = ParameterTypeResolver.resolve(service.getClass(), DirectoryService.class);

            grid = new Grid<>(caption);
            grid.setSizeFull();
            grid.setItems(service.getAll());

            Binder binder = new Binder<>(aClass);
            grid.getEditor().setBinder(binder);

            TextField nameEditor = new TextField();

            grid.addColumn(D::getName)
                .setCaption(i18n.get("directory.field.name"))
                .setEditorBinding(binder.forField(nameEditor)
                    .asRequired(i18n.get("directory.field.error.must_be_not_null"))
                    .bind("name"));


            if (aClass.equals(EducationLevel.class)) {
                TextField levelEditor = new TextField();

                grid.addColumn(item -> ((EducationLevel) item).getLevel())
                    .setCaption(i18n.get("directory.EducationService.field.level"))
                    .setEditorBinding(binder.forField(levelEditor)
                        .withConverter(new StringToIntegerConverter(i18n.get("directory.field.error.mast_be_number")))
                        .bind("level"));
            }

            grid.getEditor().setEnabled(true);

            grid.getEditor().addOpenListener(event -> nameEditor.focus());

            grid.getEditor().addSaveListener(event -> {
                service.save(event.getBean());
                grid.setItems(service.getAll());
            });

            grid.getEditor().addCancelListener(event -> grid.setItems(service.getAll()));

            GridContextMenu<D> panelMenu = new GridContextMenu<>(grid);
            panelMenu.addGridBodyContextMenuListener(this::gridContextEvents);
        }

        Grid<D> getGrid() {
            return grid;
        }

        void newItem() {
            List all = service.getAll();
            try {
                AbstractIdDirectory newItem = aClass.newInstance();
                all.add(newItem);
                grid.setItems(all);
                grid.getEditor().editRow(all.size() - 1);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }

        }

        private void gridContextEvents(GridContextMenuOpenListener.GridContextMenuOpenEvent<D> event) {
            event.getContextMenu().removeItems();
            if (event.getItem() != null)
                event.getContextMenu().addItem(i18n.get("directory.context.removeItem"), VaadinIcons.TRASH, removeEvent -> {});

            event.getContextMenu().addItem(i18n.get("directory.context.newItem"), VaadinIcons.PLUS, addEvent -> newItem());
        }
    }
}

package ru.loanpro.server.ui.module.additional;

import com.vaadin.ui.TextField;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Поле ввода для финансовых значений. Обеспечивает форматирование согласно указанных
 * локали и количества знаков после запятой.
 *
 * @author Oleg Brizhevatikh
 */
public class MoneyField extends TextField {

    private final DecimalFormat decimalFormat;
    private final Locale locale;

    /**
     * Создаёт поле ввода финансовых значений.
     *
     * @param caption заголовок поля ввода.
     * @param locale локаль.
     * @param decimalScale количество дробных знаков.
     */
    public MoneyField(String caption, Locale locale, int decimalScale) {
        super(caption);
        this.locale = locale;

        decimalFormat = (DecimalFormat) NumberFormat.getInstance(locale);
        decimalFormat.setMaximumFractionDigits(decimalScale);
        decimalFormat.setMinimumFractionDigits(decimalScale);

        addStyleName("align-right");

        new CurrencyFieldFormatter(locale, decimalScale).extend(this);

        addBlurListener(event -> setValue(formatValue(getValue())));
    }

    /**
     * Создаёт поле ввода финансовых значений с заголовком и двумя знаками после запятой.
     *
     * @param caption заголовок поля ввода.
     * @param locale локаль.
     */
    public MoneyField(String caption, Locale locale) {
        this(caption, locale, 2);
    }

    /**
     * Создаёт поле ввода финансовых значений с двумя знаками после запятой.
     *
     * @param locale локаль.
     */
    public MoneyField(Locale locale) {
        this(null, locale);
    }

    @Override
    public void setValue(String value) {
        value = formatValue(value);
        super.setValue(value);
    }

    private String formatValue(String value) {
        if (locale == null)
            return value;

        try {
            return decimalFormat.format(BigDecimal.valueOf(decimalFormat.parse(value).doubleValue()));
        } catch (ParseException e) {
            return "";
        }
    }
}

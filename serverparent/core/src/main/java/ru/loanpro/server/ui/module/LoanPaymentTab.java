package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Lists;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.MenuItem;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import fr.opensagres.xdocreport.core.XDocReportException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.account.exceptions.AccountNotFoundException;
import ru.loanpro.base.account.exceptions.InsufficientAccountBalanceException;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.exceptions.ObjectNotSavedException;
import ru.loanpro.base.loan.service.LoanService;
import ru.loanpro.base.operation.event.UpdateOperationEvent;
import ru.loanpro.base.operation.exceptions.OperationNotSavedException;
import ru.loanpro.base.payment.PaymentCalculator;
import ru.loanpro.base.payment.domain.Payment;
import ru.loanpro.base.payment.event.PaymentDeletedException;
import ru.loanpro.base.payment.service.PaymentService;
import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.base.printer.PrintService;
import ru.loanpro.base.schedule.service.ScheduleService;
import ru.loanpro.base.storage.domain.connector.PaymentPrintedFile;
import ru.loanpro.base.storage.service.PrintTemplateFileService;
import ru.loanpro.base.storage.service.PrintedFileService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.directory.LoanStatus;
import ru.loanpro.global.directory.PaymentStatus;
import ru.loanpro.global.directory.RepaymentOrderScheme;
import ru.loanpro.global.directory.ScheduleStatus;
import ru.loanpro.server.ui.module.additional.ConfirmDialog;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.event.UpdateBaseLoanEvent;
import ru.loanpro.server.ui.module.event.UpdatePaymentEvent;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Вкладка работы с оплатой по займу
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
@EntityTab(Payment.class)
public class LoanPaymentTab extends BaseTab {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    protected PrintService printService;

    @Autowired
    private PrintTemplateFileService printTemplateFileService;

    @Autowired
    private LoanService loanService;

    @Autowired
    PrintedFileService printedFileService;

    @Autowired
    private ScheduleService scheduleService;

    private Loan loan;
    private Payment payment;
    private Payment recalculationPayment = new Payment();
    private Payment tempPayment = new Payment();
    private LoanStatus recalculationLoanStatusAfter;
    private ScheduleStatus recalculationScheduleStatusAfter;
    private boolean isRecalculated = false;

    private int scaleMode = BigDecimal.ROUND_HALF_EVEN;
    private int scaleValue = 2;
    private Binder<Payment> binder;

    private MoneyField tfPrincipal;
    private MoneyField tfInterest;
    private MoneyField tfPenalty;
    private MoneyField tfFine;
    private MoneyField tfPayment;

    private MoneyField tfBalanceBefore;
    private MoneyField tfBalanceAfter;

    private CheckBox cbxPrincipal;
    private CheckBox cbxInterest;
    private CheckBox cbxPenalty;
    private CheckBox cbxFine;
    private CheckBox cbxPayment;

    @Visible(Roles.Payment.SAVE)
    private Button save;

    @Visible(Roles.Payment.RECALCULATION)
    private Button recalculate;

    @Visible(Roles.Payment.RECALCULATION)
    private Button reset;

    @Visible(Roles.Payment.PRINT)
    private Button print;

    @Visible(Roles.Payment.DELETELASTPAYMENT)
    private Button deleteLast;

    private Label loanStatus;

    private VerticalLayout vlPaymentBlock;
    private VerticalLayout vlPaymentBlockBE;

    private int numberSchedule;
    private PaymentStatus paymentStatus;
    private LoanStatus loanStatusBefore;
    private LoanStatus loanStatusAfter;
    private ScheduleStatus scheduleStatusBefore;
    private ScheduleStatus scheduleStatusAfter;
    private double sumBalanceAfter;

    private TextField orderNumber;

    private boolean isViewFromLoan;
    private boolean isLoanClosed;
    private boolean isDeleted;
    private int paymentHash;

    private Grid<Payment> gridPayment;

    public LoanPaymentTab(Loan loan) {
        this(loan, null);
    }

    public LoanPaymentTab(Loan loan, Payment payment) {
        this.loan = loan;
        this.payment = payment;
    }

    @PostConstruct
    public void init() {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(
            configurationService.getDateFormat(), configurationService.getLocale());
        DecimalFormat moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);

        isDeleted = false;
        isLoanClosed = isLoanClosed(loan);
        isViewFromLoan = payment != null;

        loan = loanService.getOneFull(loan.getId());//в целях инициализации lazy-полей

        if (payment == null) {
            if (!isLoanClosed) {
                scheduleService.updateOverdueScheduleStatusesByLoan(
                    chronometerService.getCurrentTime().toLocalDate(), loan);
                payment = calculatePayment();
                paymentHash = payment.hashCode();
                clonePayment();
            } else {
                Optional<Payment> p = loan.getPayments().stream().reduce((first, second) -> second);
                p.ifPresent(payment1 -> {
                    payment = payment1;
                });
            }
        }

        binder = new Binder<>();
        binder.setBean(payment);

        //******************блок vlHeader******************
        Label mainInfo = new Label(i18n.get("loanpayment.panel.maininfo.title"));

        save = new Button(i18n.get("loanpayment.button.save"), VaadinIcons.ADD_DOCK);
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        save.addClickListener(event -> {
            if (isRecalculated) {
                getFromRecalculation();
                loan.setLoanStatus(recalculationLoanStatusAfter);
                loan.getSchedules().get(numberSchedule - 1).setStatus(recalculationScheduleStatusAfter);
                loan.setCurrentBalance(recalculationPayment.getBalanceAfter());
            } else {
                loan.setLoanStatus(loanStatusAfter);
                loan.getSchedules().get(numberSchedule - 1).setStatus(scheduleStatusAfter);
                loan.setCurrentBalance(new BigDecimal(sumBalanceAfter).setScale(scaleValue, scaleMode));
            }
            //проверяем, есть ли просроченные платежи с более поздними датами
            boolean b = loan.getSchedules().stream().skip(numberSchedule).anyMatch(
                v -> v.getStatus() == ScheduleStatus.NOT_PAID_OVERDUE);
            if (b) {
                loan.setLoanStatus(LoanStatus.OPENED_CURRENT_OVERDUE);
            }

            try {
                paymentService.save(payment, loan.getSchedules().get(numberSchedule - 1), loan);
                loan = loanService.getOneFull(loan.getId());//в целях инициализации lazy-полей
                loanStatus.setValue(String.format(
                    i18n.get("loanpayment.field.loanstatus"), i18n.get(loan.getLoanStatus().getName())));

                paymentHash = payment.hashCode();
                buttonsStatusUpdate();
                setEnabledPaymentBlock(false);
                gridPayment.setItems(loan.getPayments());
                deleteLast.setEnabled(!loan.getPayments().isEmpty());

                sessionEventBus.post(new UpdatePaymentEvent(loan));
                sessionEventBus.post(new UpdateBaseLoanEvent<>(loan));
                Notification.show(i18n.get("loanpayment.message.paymentsaved"), Notification.Type.TRAY_NOTIFICATION);
            } catch (ObjectNotSavedException e) {
                save.setEnabled(false);
                print.setEnabled(false);
                Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
            }

            if (securityService.hasAuthority(Roles.Payment.AUTOOPERATION)) {
                try {
                    paymentService.saveOperations(payment, loan,
                        configurationService.getAutoOperationReplenishMoneyLoanPayment(),
                        configurationService.getAutoOperationReplenishMoneyLoanPaymentProcessed(),
                        configurationService.getPaymentSplitType(),
                        chronometerService.getCurrentInstant(),
                        securityService.getUser(),
                        securityService.getDepartment());

                    applicationEventBus.post(new UpdateOperationEvent(loan));
                } catch (OperationNotSavedException | AccountNotFoundException | InsufficientAccountBalanceException e) {
                    Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.WARNING_MESSAGE);
                    e.printStackTrace();
                }
            }
        });

        print = new Button(i18n.get("loanpayment.button.print"), VaadinIcons.PRINT);
        print.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        print.addClickListener(event -> {
            ContextMenu contextMenu = new ContextMenu(print, true);

            List<PaymentPrintedFile> printedFiles = printedFileService.findPaymentPrintedFiles(payment);
            if (printedFiles.size() > 0) {
                MenuItem printedItem = contextMenu.addItem(i18n.get("loanpayment.button.printed"), null);
                printedFiles.forEach(printed ->
                    printedItem.addItem(
                        String.format("%s %s", printed.getDisplayName(),
                            chronometerService.zonedDateTimeToString(printed.getDateTime())),
                        e -> getUI().getPage().open(String.format("/print/%s", printed.getFileName()), "_blank")));
            }

            printTemplateFileService.getActiveTemplates(PrintDocumentType.LOAN_PAYMENT)
                .forEach(template ->
                    contextMenu.addItem(template.getDisplayName(), e -> {
                        try {
                            String fileName = printService.printTemplate(template, payment);
                            getUI().getPage().open(String.format("/print/%s", fileName), "_blank");
                        } catch (IOException | XDocReportException exception) {
                            showErrorMessage(exception);
                        }
                    }));

            contextMenu.open(event.getClientX(), event.getClientY());
        });

        HorizontalLayout hlButtonAll = new HorizontalLayout(save, print);
        hlButtonAll.setStyleName("flex-wrap-right-alignment-layout");

        HorizontalLayout hlActions = new HorizontalLayout(mainInfo, hlButtonAll);
        hlActions.setWidth(100, Unit.PERCENTAGE);
        hlActions.setExpandRatio(hlButtonAll, 1.0f);

        loanStatus = new Label(String.format(
            i18n.get("loanpayment.field.loanstatus"), i18n.get(loan.getLoanStatus().getName())));
        Label loanDepartment = new Label(String.format(
            i18n.get("loanpayment.field.loandepartment"), loan.getDepartment().getName()));
        Label paymentDepartment = new Label(String.format(
            i18n.get("loanpayment.field.paymentdepartment"), securityService.getDepartment().getName()));

        orderNumber = new TextField(i18n.get("loanpayment.field.orderNumber"));
        orderNumber.setWidth(120, Unit.PIXELS);
        orderNumber.setReadOnly(true);

        TextField scheduleNumber = new TextField(i18n.get("loanpayment.field.scheduleNumber"));
        scheduleNumber.setWidth(120, Unit.PIXELS);
        scheduleNumber.setReadOnly(true);

        DateField payDate = new DateField(i18n.get("loanpayment.field.paydate"));
        payDate.setWidth(200, Unit.PIXELS);
        payDate.setDateFormat(configurationService.getDateFormat());
        payDate.setReadOnly(true);

        TextField days = new TextField(i18n.get("loanpayment.field.days"));
        days.setWidth(150, Unit.PIXELS);
        days.setReadOnly(true);

        TextField overdueDays = new TextField(i18n.get("loanpayment.field.overduedays"));
        overdueDays.setWidth(120, Unit.PIXELS);
        overdueDays.setReadOnly(true);

        HorizontalLayout hlDays = new HorizontalLayout(orderNumber, scheduleNumber, payDate, days, overdueDays);

        //блок пролонгации
        Label prolongInfo = new Label(i18n.get("loanpayment.prolong.panel.maininfo.title"));

        TextField prolongOrdNo = new TextField(i18n.get("loanpayment.prolong.field.prolongOrdNo"));
        prolongOrdNo.setWidth(80, Unit.PIXELS);
        prolongOrdNo.setReadOnly(true);
        prolongOrdNo.setValue(String.valueOf(loan.getProlongOrdNo()));

        DateField prolongDate = new DateField(i18n.get("loanpayment.prolong.field.prolongDate"));
        prolongDate.setWidth(200, Unit.PIXELS);
        prolongDate.setDateFormat(configurationService.getDateFormat());
        prolongDate.setReadOnly(true);
        prolongDate.setValue(loan.getProlongDate());

        HorizontalLayout hlProlong = new HorizontalLayout(prolongOrdNo, prolongDate);
        VerticalLayout vlProlong = new VerticalLayout(prolongInfo, hlProlong);

        VerticalLayout vlHeader = new VerticalLayout(hlActions, loanStatus, loanDepartment, paymentDepartment, hlDays, vlProlong);
        vlHeader.setWidth(100, Unit.PERCENTAGE);
        vlHeader.addStyleName(MaterialTheme.CARD_2);
        vlHeader.setSpacing(true);

        //**************Блок Фактически к оплате********************
        recalculate = new Button(i18n.get("loanpayment.button.recalculate"), VaadinIcons.ABACUS);
        recalculate.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        recalculate.addStyleName(ValoTheme.BUTTON_SMALL);
        recalculate.addClickListener(event -> {
            recalculatePaymentParts(configurationService.getRepaymentOrderScheme());
            getFromRecalculation();
            binder.setBean(payment);
            isRecalculated = true;
        });

        reset = new Button(i18n.get("loanpayment.button.reset"), VaadinIcons.REFRESH);
        reset.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        reset.addStyleName(ValoTheme.BUTTON_SMALL);
        reset.addClickListener(event -> {
            setReadOnlyPayment1(true);
            setCheckedPayment1(false);
            setReadOnlyPayment2(true);
            setCheckedPayment2(false);
            checkState();
            getFromClone();
            binder.setBean(payment);
            isRecalculated = false;
        });

        HorizontalLayout hlButtonPayment = new HorizontalLayout(reset, recalculate);
        hlButtonPayment.setStyleName("flex-wrap-right-alignment-layout");

        Label paymentInfo = new Label(i18n.get("loanpayment.panel.paymentinfo.title"));

        ComboBox<PaymentStatus> cbPaymentStatus = new ComboBox<>(
            i18n.get("loanpayment.field.paymentstatus"), Lists.newArrayList(PaymentStatus.values()));
        cbPaymentStatus.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbPaymentStatus.setEmptySelectionAllowed(false);
        cbPaymentStatus.setWidth(400, Unit.PIXELS);
        cbPaymentStatus.setReadOnly(true);
        VerticalLayout vlPaymentStatus = new VerticalLayout(cbPaymentStatus);

        HorizontalLayout hlActionsPayment = new HorizontalLayout(paymentInfo, cbPaymentStatus);
        hlActionsPayment.setWidth(100, Unit.PERCENTAGE);
        hlActionsPayment.setExpandRatio(paymentInfo, 1.0f);

        tfPrincipal = new MoneyField(i18n.get("loanpayment.field.principal"), configurationService.getLocale());
        tfPrincipal.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPrincipal.setWidth(150, Unit.PIXELS);
        cbxPrincipal = new CheckBox(i18n.get("loanpayment.checkbox.principal"));
        cbxPrincipal.setWidth(150, Unit.PIXELS);
        cbxPrincipal.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxPrincipal.addValueChangeListener(value -> {
            if (value.getValue()) {
                tfPrincipal.setReadOnly(false);
                tfPrincipal.focus();
                setCheckedPayment2(false);
            } else {
                tfPrincipal.setReadOnly(true);
                resetCalculation();
            }
            checkState();
        });

        VerticalLayout vlPrincipal = new VerticalLayout(tfPrincipal, cbxPrincipal);
        vlPrincipal.setSpacing(false);

        tfInterest = new MoneyField(i18n.get("loanpayment.field.interest"), configurationService.getLocale());
        tfInterest.setWidth(150, Unit.PIXELS);
        cbxInterest = new CheckBox(i18n.get("loanpayment.checkbox.interest"));
        cbxInterest.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxInterest.setWidth(150, Unit.PIXELS);
        cbxInterest.addValueChangeListener(value -> {
            if (value.getValue()) {
                tfInterest.setReadOnly(false);
                tfInterest.focus();
                setCheckedPayment2(false);
            } else {
                tfInterest.setReadOnly(true);
                resetCalculation();
            }
            checkState();
        });

        VerticalLayout vlInterest = new VerticalLayout(tfInterest, cbxInterest);
        vlInterest.setSpacing(false);

        tfPenalty = new MoneyField(i18n.get("loanpayment.field.penalty"), configurationService.getLocale());
        tfPenalty.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPenalty.setWidth(150, Unit.PIXELS);
        tfPenalty.addStyleName("align-right");
        cbxPenalty = new CheckBox(i18n.get("loanpayment.checkbox.penalty"));
        cbxPenalty.setWidth(150, Unit.PIXELS);
        cbxPenalty.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxPenalty.addValueChangeListener(value -> {
            if (value.getValue()) {
                tfPenalty.setReadOnly(false);
                tfPenalty.focus();
                setCheckedPayment2(false);
            } else {
                tfPenalty.setReadOnly(true);
                resetCalculation();
            }
            checkState();
        });

        VerticalLayout vlPenalty = new VerticalLayout(tfPenalty, cbxPenalty);
        vlPenalty.setSpacing(false);

        tfFine = new MoneyField(i18n.get("loanpayment.field.fine"), configurationService.getLocale());
        tfFine.addStyleName(MaterialTheme.LABEL_COLORED);
        tfFine.setWidth(150, Unit.PIXELS);
        tfFine.addStyleName("align-right");
        cbxFine = new CheckBox(i18n.get("loanpayment.checkbox.fine"));
        cbxFine.setWidth(150, Unit.PIXELS);
        cbxFine.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxFine.addValueChangeListener(value -> {
            if (value.getValue()) {
                tfFine.setReadOnly(false);
                tfFine.focus();
                setCheckedPayment2(false);
            } else {
                tfFine.setReadOnly(true);
                resetCalculation();
            }
            checkState();
        });

        VerticalLayout vlFine = new VerticalLayout(tfFine, cbxFine);
        vlFine.setSpacing(false);

        tfPayment = new MoneyField(i18n.get("loanpayment.field.payment"), configurationService.getLocale());
        tfPayment.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPayment.setWidth(150, Unit.PIXELS);
        tfPayment.addStyleName("align-right");
        cbxPayment = new CheckBox(i18n.get("loanpayment.checkbox.payment"));
        cbxPayment.setWidth(150, Unit.PIXELS);
        cbxPayment.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxPayment.addValueChangeListener(value -> {
            if (value.getValue()) {
                setReadOnlyPayment2(false);
                tfPayment.focus();
                setReadOnlyPayment1(true);
                setCheckedPayment1(false);
            } else {
                tfPayment.setReadOnly(true);
                reset.click();
            }
            checkState();
        });

        VerticalLayout vlPayment = new VerticalLayout(tfPayment, cbxPayment);
        vlPayment.setSpacing(false);

        HorizontalLayout hlPayment = new HorizontalLayout(vlPrincipal, vlInterest, vlPenalty, vlFine, vlPayment);
        hlPayment.setStyleName("flex-wrap-left-alignment-layout");
        hlPayment.setSpacing(false);

        vlPaymentBlock = new VerticalLayout(hlActionsPayment, vlPaymentStatus, hlPayment, hlButtonPayment);
        vlPaymentBlock.setWidth(100, Unit.PERCENTAGE);
        vlPaymentBlock.addStyleName(MaterialTheme.CARD_2);
        vlPaymentBlock.setSpacing(false);

        //Остаток до и после платежа
        tfBalanceBefore = new MoneyField(i18n.get("loanpayment.field.balanceBefore"), configurationService.getLocale());
        tfBalanceBefore.addStyleName(MaterialTheme.LABEL_COLORED);
        tfBalanceBefore.setWidth(150, Unit.PIXELS);
        tfBalanceBefore.addStyleName("align-right");
        tfBalanceBefore.setReadOnly(true);
        tfBalanceAfter = new MoneyField(i18n.get("loanpayment.field.balanceAfter"), configurationService.getLocale());
        tfBalanceAfter.addStyleName(MaterialTheme.LABEL_COLORED);
        tfBalanceAfter.setWidth(150, Unit.PIXELS);
        tfBalanceAfter.addStyleName("align-right");
        tfBalanceAfter.setReadOnly(true);
        HorizontalLayout hlBalance = new HorizontalLayout(tfBalanceBefore, tfBalanceAfter);
        hlBalance.setStyleName("flex-wrap-left-alignment-layout");
        hlBalance.setSpacing(false);

        //Долг на начало платежа (буква B)
        Label paymentInfoB = new Label(i18n.get("loanpayment.panel.paymentinfoB.title"));

        MoneyField tfPrincipalB = new MoneyField(i18n.get("loanpayment.field.principalB"), configurationService.getLocale());
        tfPrincipalB.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPrincipalB.setWidth(150, Unit.PIXELS);
        tfPrincipalB.addStyleName("align-right");
        tfPrincipalB.setReadOnly(true);

        MoneyField tfInterestB = new MoneyField(i18n.get("loanpayment.field.interestB"), configurationService.getLocale());
        tfInterestB.addStyleName(MaterialTheme.LABEL_COLORED);
        tfInterestB.setWidth(150, Unit.PIXELS);
        tfInterestB.addStyleName("align-right");
        tfInterestB.setReadOnly(true);

        MoneyField tfPenaltyB = new MoneyField(i18n.get("loanpayment.field.penaltyB"), configurationService.getLocale());
        tfPenaltyB.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPenaltyB.setWidth(150, Unit.PIXELS);
        tfPenaltyB.addStyleName("align-right");
        tfPenaltyB.setReadOnly(true);

        MoneyField tfFineB = new MoneyField(i18n.get("loanpayment.field.fineB"), configurationService.getLocale());
        tfFineB.addStyleName(MaterialTheme.LABEL_COLORED);
        tfFineB.setWidth(150, Unit.PIXELS);
        tfFineB.addStyleName("align-right");
        tfFineB.setReadOnly(true);

        MoneyField tfPaymentB = new MoneyField(i18n.get("loanpayment.field.paymentB"), configurationService.getLocale());
        tfPaymentB.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPaymentB.setWidth(150, Unit.PIXELS);
        tfPaymentB.addStyleName("align-right");
        tfPaymentB.setReadOnly(true);

        HorizontalLayout hlPaymentB = new HorizontalLayout(tfPrincipalB, tfInterestB, tfPenaltyB, tfFineB, tfPaymentB);
        hlPaymentB.setStyleName("flex-wrap-left-alignment-layout");
        hlPaymentB.setSpacing(false);

        //Долг на конец платежа (буква E)
        Label paymentInfoE = new Label(i18n.get("loanpayment.panel.paymentinfoE.title"));

        MoneyField tfPrincipalE = new MoneyField(i18n.get("loanpayment.field.principalE"), configurationService.getLocale());
        tfPrincipalE.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPrincipalE.setWidth(150, Unit.PIXELS);
        tfPrincipalE.addStyleName("align-right");
        tfPrincipalE.setReadOnly(true);

        MoneyField tfInterestE = new MoneyField(i18n.get("loanpayment.field.interestE"), configurationService.getLocale());
        tfInterestE.addStyleName(MaterialTheme.LABEL_COLORED);
        tfInterestE.setWidth(150, Unit.PIXELS);
        tfInterestE.addStyleName("align-right");
        tfInterestE.setReadOnly(true);

        MoneyField tfPenaltyE = new MoneyField(i18n.get("loanpayment.field.penaltyE"), configurationService.getLocale());
        tfPenaltyE.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPenaltyE.setWidth(150, Unit.PIXELS);
        tfPenaltyE.addStyleName("align-right");
        tfPenaltyE.setReadOnly(true);

        MoneyField tfFineE = new MoneyField(i18n.get("loanpayment.field.fineE"), configurationService.getLocale());
        tfFineE.addStyleName(MaterialTheme.LABEL_COLORED);
        tfFineE.setWidth(150, Unit.PIXELS);
        tfFineE.addStyleName("align-right");
        tfFineE.setReadOnly(true);

        MoneyField tfPaymentE = new MoneyField(i18n.get("loanpayment.field.paymentE"), configurationService.getLocale());
        tfPaymentE.addStyleName(MaterialTheme.LABEL_COLORED);
        tfPaymentE.setWidth(150, Unit.PIXELS);
        tfPaymentE.addStyleName("align-right");
        tfPaymentE.setReadOnly(true);

        HorizontalLayout hlPaymentE = new HorizontalLayout(tfPrincipalE, tfInterestE, tfPenaltyE, tfFineE, tfPaymentE);
        hlPaymentE.setStyleName("flex-wrap-left-alignment-layout");
        hlPaymentE.setSpacing(false);

        vlPaymentBlockBE = new VerticalLayout(hlBalance, paymentInfoB, hlPaymentB, paymentInfoE, hlPaymentE);
        vlPaymentBlockBE.setWidth(100, Unit.PERCENTAGE);
        vlPaymentBlockBE.addStyleName(MaterialTheme.CARD_2);
        vlPaymentBlockBE.setSpacing(true);

        Label gridInfo = new Label(i18n.get("loanpayment.grid.info.title"));

        deleteLast = new Button(i18n.get("loanpayment.button.deletelast"), VaadinIcons.TRASH);
        deleteLast.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        deleteLast.setEnabled(!loan.getPayments().isEmpty());
        deleteLast.addClickListener(event -> {
            ConfirmDialog dialog = new ConfirmDialog(
                i18n.get("loanpayment.deletelast.dialog.caption"),
                i18n.get("loanpayment.deletelast.dialog.question"),
                () -> {
                    try {
                        paymentService.deleteLast(loan);
                        gridPayment.setItems(loan.getPayments());
                        loan = loanService.getOneFull(loan.getId());//в целях инициализации lazy-полей

                    } catch (PaymentDeletedException e) {
                        Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.ERROR_MESSAGE);
                        e.printStackTrace();
                    }
                    loanStatus.setValue(String.format(
                        i18n.get("loanpayment.field.loanstatus"), i18n.get(loan.getLoanStatus().getName())));
                    deleteLast.setEnabled(!loan.getPayments().isEmpty());
                    isDeleted = true;

                    //Находим последний платеж в списке после удаления
                    Optional<Payment> p = loan.getPayments().stream().reduce((first, second) -> second);
                    //если он есть, обновляем сущность и обновляем компоненты
                    p.ifPresent(payment1 -> {
                        payment = payment1;
                        binder.setBean(payment);
                    });

                    if (!p.isPresent()) {
                        payment = new Payment(new BigDecimal(sumBalanceAfter).setScale(scaleValue, scaleMode));
                        binder.setBean(payment);

                        vlPaymentBlock.setVisible(false);
                        vlPaymentBlockBE.setVisible(false);
                    }
                    buttonsStatusUpdate();
                    sessionEventBus.post(new UpdatePaymentEvent(loan));
                    sessionEventBus.post(new UpdateBaseLoanEvent<>(loan));
                    Notification.show(i18n.get("loanpayment.message.lastpaymentdeleted"), Notification.Type.TRAY_NOTIFICATION);
                },
                () -> {
                }
            );
            dialog.setClosable(true);
            dialog.show();
        });

        HorizontalLayout hlButtonGridAll = new HorizontalLayout(deleteLast);
        hlButtonGridAll.setStyleName("flex-wrap-right-alignment-layout");

        HorizontalLayout hlGridActions = new HorizontalLayout(gridInfo, hlButtonGridAll);
        hlGridActions.setWidth(100, Unit.PERCENTAGE);
        hlGridActions.setExpandRatio(hlButtonGridAll, 1.0f);

        gridPayment = new Grid<>();
        gridPayment.setSizeFull();
        Grid.Column column1 = gridPayment.addColumn(Payment::getOrdNo)
            .setCaption(i18n.get("loanpayment.grid.title.order"));
        column1.setWidth(100);
        Grid.Column column1_1 = gridPayment.addColumn(Payment::getScheduleOrdNo)
            .setCaption(i18n.get("loanpayment.grid.title.scheduleorder"));
        column1_1.setWidth(100);
        Grid.Column column1_2 = gridPayment.addColumn(Payment::getProlongOrdNo)
            .setCaption(i18n.get("loanpayment.grid.title.prolongorder"));
        column1_2.setWidth(100);
        Grid.Column column2 = gridPayment.addColumn(item -> i18n.get(item.getStatus().getName()))
            .setCaption(i18n.get("loanpayment.grid.title.status"));
        column2.setWidth(150);
        gridPayment.addColumn(item -> dateFormatter.format(item.getPayDate()))
            .setCaption(i18n.get("loanpayment.grid.title.date"));
        gridPayment.addColumn(Payment::getTerm)
            .setCaption(i18n.get("loanpayment.grid.title.term"));
        gridPayment.addColumn(Payment::getOverdueTerm)
            .setCaption(i18n.get("loanpayment.grid.title.overdueterm"));
        gridPayment.addColumn(item -> moneyFormat.format(item.getPrincipal()))
            .setCaption(i18n.get("loanpayment.grid.title.principal"))
            .setStyleGenerator(item -> "v-align-right");
        gridPayment.addColumn(item -> moneyFormat.format(item.getInterest()))
            .setCaption(i18n.get("loanpayment.grid.title.interest"))
            .setStyleGenerator(item -> "v-align-right");
        gridPayment.addColumn(item -> moneyFormat.format(item.getPenalty()))
            .setCaption(i18n.get("loanpayment.grid.title.penalty"))
            .setStyleGenerator(item -> "v-align-right");
        gridPayment.addColumn(item -> moneyFormat.format(item.getFine()))
            .setCaption(i18n.get("loanpayment.grid.title.fine"))
            .setStyleGenerator(item -> "v-align-right");
        gridPayment.addColumn(item -> moneyFormat.format(item.getPayment()))
            .setCaption(i18n.get("loanpayment.grid.title.payment"))
            .setStyleGenerator(item -> "v-align-right");
        gridPayment.addColumn(item -> moneyFormat.format(item.getBalanceAfter()))
            .setCaption(i18n.get("loanpayment.grid.title.balance"))
            .setStyleGenerator(item -> "v-align-right");
        gridPayment.setItems(paymentService.findAllByLoan(loan));

        gridPayment.addContextClickListener(event -> {
            Grid.GridContextClickEvent<Payment> contextEvent = (Grid.GridContextClickEvent<Payment>) event;
            ContextMenu contextMenu = new ContextMenu(gridPayment, true);

            if (contextEvent.getItem() != null) {
                contextMenu.addItem(i18n.get("loanpayment.grid.context.viewpayment"), e -> {
                    payment = contextEvent.getItem();
                    binder.setBean(payment);
                    contextMenu.remove();
                });
            }
        });

        VerticalLayout vlGrid = new VerticalLayout(hlGridActions, gridPayment);
        vlGrid.setWidth(100, Unit.PERCENTAGE);
        vlGrid.addStyleName(MaterialTheme.CARD_2);
        vlGrid.setSpacing(true);
        vlGrid.setSizeFull();

        //заголовок
        binder.forField(orderNumber)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .bind(Payment::getOrdNo, Payment::setOrdNo);
        binder.forField(scheduleNumber)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .bind(Payment::getScheduleOrdNo, Payment::setScheduleOrdNo);
        binder.forField(payDate)
            .bind(Payment::getPayDate, Payment::setPayDate);
        binder.forField(days)
            .withConverter(new StringToLongConverter("Must be a number"))
            .bind(Payment::getTerm, Payment::setTerm);
        binder.forField(overdueDays)
            .withConverter(new StringToLongConverter("Must be a number"))
            .bind(Payment::getOverdueTerm, Payment::setOverdueTerm);
        //к оплате
        binder.bind(cbPaymentStatus, Payment::getStatus, Payment::setStatus);
        binder.forField(tfPrincipal)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPrincipal, Payment::setPrincipal);
        binder.forField(tfInterest)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getInterest, Payment::setInterest);
        binder.forField(tfPenalty)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPenalty, Payment::setPenalty);
        binder.forField(tfFine)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getFine, Payment::setFine);
        binder.forField(tfPayment)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPayment, Payment::setPayment);
        //долг на начало
        binder.forField(tfBalanceBefore)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getBalanceBefore, Payment::setBalanceBefore);
        binder.forField(tfBalanceAfter)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getBalanceAfter, Payment::setBalanceAfter);
        binder.forField(tfPrincipalB)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPrincipalB, Payment::setPrincipalB);
        binder.forField(tfInterestB)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getInterestB, Payment::setInterestB);
        binder.forField(tfPenaltyB)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPenaltyB, Payment::setPenaltyB);
        binder.forField(tfFineB)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getFineB, Payment::setFineB);
        binder.forField(tfPaymentB)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPaymentB, Payment::setPaymentB);
        //долг на конец
        binder.forField(tfPrincipalE)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPrincipalE, Payment::setPrincipalE);
        binder.forField(tfInterestE)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getInterestE, Payment::setInterestE);
        binder.forField(tfPenaltyE)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPenaltyE, Payment::setPenaltyE);
        binder.forField(tfFineE)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getFineE, Payment::setFineE);
        binder.forField(tfPaymentE)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .bind(Payment::getPaymentE, Payment::setPaymentE);

        setReadOnlyPayment1(true);
        setReadOnlyPayment2(true);
        setCheckedPayment1(false);
        setCheckedPayment2(false);
        checkState();

        if (isLoanClosed) {
            setEnabledPaymentBlock(false);
        }

        addComponents(vlHeader, vlPaymentBlock, vlPaymentBlockBE, vlGrid);

        buttonsStatusUpdate();
    }

    /**
     * Вычисляет автоматический платеж по умолчанию исходя из свойств займа, графиков платежей и текущей даты.
     *
     * @return - новый автоматический платеж
     */
    private Payment calculatePayment() {
        PaymentCalculator paymentCalculator = PaymentCalculator.instance()
            .setScaleValue(scaleValue)
            .setScaleMode(scaleMode)
            .setLoanIssueDate(loan.getProlongOrdNo() == 0 ?
                LocalDateTime.ofInstant(loan.getLoanIssueDate(), chronometerService.getCurrentZoneId()).toLocalDate()
                : loan.getProlongDate())
            .setLoanBalance(loan.getLoanBalance())
            .setReplaceDate(loan.isReplaceDate())
            .setReplaceDateValue(loan.getReplaceDateValue())
            .setReplaceOverMonth(loan.isReplaceOverMonth())
            .setPrincipalLastPay(loan.isPrincipalLastPay())
            .setInterestBalance(loan.isInterestBalance())
            .setNoInterestFD(loan.isNoInterestFD())
            .setNoInterestFDValue(loan.getNoInterestFDValue())
            .setIncludeIssueDate(loan.isIncludeIssueDate())
            .setDaysInYear(365)
            .setCalcScheme(loan.getCalcScheme())
            .setReturnTerm(loan.getReturnTerm())
            .setInterestRate(loan.getInterestValue())
            .setInterestRateType(loan.getInterestType())
            .setLoanTerm(loan.getLoanTerm())
            .setReplaceDate(loan.isReplaceDate())
            .setReplaceDateValue(loan.getReplaceDateValue())
            .setReplaceOverMonth(loan.isReplaceOverMonth())
            .setSummationFines(configurationService.getSummationFine())
            .setSummationPenalties(configurationService.getSummationPenalty())
            .setOverdueDaysCalculationType(configurationService.getOverdueDaysCalculationType())
            .setCalculationDate(chronometerService.getCurrentTime().toLocalDate())
            .setPenaltyType(loan.getPenaltyType())
            .setPenaltyValue(loan.getPenaltyValue())
            .setFine(loan.isChargeFine())
            .setFineValue(loan.getFineValue())
            .setFineOneTime(loan.isFineOneTime())
            .setInterestStrictly(loan.isInterestStrictly())
            .setInterestOverdue(loan.isInterestOverdue())
            .setInterestFirstDays(loan.isInterestFirstDays())
            .setInterestFirstDaysValue(loan.getInterestNumberFirstDays())
            .build();
        paymentCalculator.setSchedules(loan.getSchedules());
        paymentCalculator.setPayments(loan.getPayments());

        loanStatusBefore = loan.getLoanStatus();

        Payment payment = paymentCalculator.calculate();

        numberSchedule = paymentCalculator.getNumberSchedule();
        paymentStatus = paymentCalculator.getPaymentStatus();
        scheduleStatusBefore = paymentCalculator.getScheduleStatusBefore();
        scheduleStatusAfter = calculateScheduleStatusAfterPayment(payment, scheduleStatusBefore);
        sumBalanceAfter = paymentCalculator.getSumBalanceAfter();
        loanStatusAfter = calculateLoanStatus(sumBalanceAfter, loanStatusBefore);

        payment.setLoan(loan);
        payment.setProlongOrdNo(loan.getProlongOrdNo());
        payment.setLoanStatusBefore(loanStatusBefore);
        payment.setScheduleStatusBefore(scheduleStatusBefore);
        payment.setDepartmentPayment(loan.getDepartment());

        return payment;
    }

    /**
     * Пересчет платежа при ручном способе указания составляющих платежа
     *
     * @param scheme - тип схемы списания
     */
    private void recalculatePaymentParts(RepaymentOrderScheme scheme) {
        double lSumBalanceAfter;
        double delta;

        double lSumBalanceBefore = payment.getBalanceBefore().doubleValue();
        String sPrincipal = tfPrincipal.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
        double lSumPrincipal = Double.valueOf(sPrincipal);
        String sInterest = tfInterest.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
        double lSumInterest = Double.valueOf(sInterest);
        String sPenalty = tfPenalty.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
        double lSumPenalty = Double.valueOf(sPenalty);
        String sFine = tfFine.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
        double lSumFine = Double.valueOf(sFine);
        String sPayment = tfPayment.getValue().replace(",", ".").replaceAll("[\\s\\u00A0]", "");
        double lSumPayment = Double.valueOf(sPayment);

        if (cbxPayment.getValue()) {//схема 1
            if (scheme == RepaymentOrderScheme.ORDER1) {//очередность списания - штраф, неустойка, проценты, осн. часть
                delta = lSumPayment - (lSumInterest + lSumPenalty + lSumFine);
                if (delta >= 0) {
                    lSumPrincipal = delta;
                } else {
                    delta = lSumPayment - (lSumPenalty + lSumFine);
                    if (delta >= 0) {
                        lSumPrincipal = 0;
                        lSumInterest = delta;
                    } else {
                        delta = lSumPayment - lSumFine;
                        if (delta >= 0) {
                            lSumPrincipal = 0;
                            lSumInterest = 0;
                            lSumPenalty = delta;
                        } else {
                            delta = lSumPayment;
                            if (delta >= 0) {
                                lSumPrincipal = 0;
                                lSumInterest = 0;
                                lSumPenalty = 0;
                                lSumFine = delta;
                            }
                        }
                    }
                }
            } else if (scheme == RepaymentOrderScheme.ORDER2) {//схема 2
                delta = lSumPayment - (lSumPrincipal + lSumInterest + lSumFine);
                if (delta >= 0) {
                    lSumPenalty = delta;
                } else {
                    delta = lSumPayment - (lSumPrincipal + lSumInterest);
                    if (delta >= 0) {
                        lSumPenalty = 0;
                        lSumFine = delta;
                    } else {
                        delta = lSumPayment - lSumInterest;
                        if (delta >= 0) {
                            lSumPenalty = 0;
                            lSumFine = 0;
                            lSumPrincipal = delta;
                        } else {
                            delta = lSumPayment;
                            if (delta >= 0) {
                                lSumPenalty = 0;
                                lSumFine = 0;
                                lSumPrincipal = 0;
                                lSumInterest = delta;
                            }
                        }
                    }
                }
            } else {//
                delta = lSumPayment - (lSumInterest + lSumPenalty + lSumFine);
                if (delta >= 0) {
                    lSumPrincipal = delta;
                } else {
                    delta = lSumPayment - (lSumPenalty + lSumFine);
                    if (delta >= 0) {
                        lSumPrincipal = 0;
                        lSumInterest = delta;
                    } else {
                        delta = lSumPayment - lSumFine;
                        if (delta >= 0) {
                            lSumPrincipal = 0;
                            lSumInterest = 0;
                            lSumPenalty = delta;
                        } else {
                            delta = lSumPayment;
                            if (delta >= 0) {
                                lSumPrincipal = 0;
                                lSumInterest = 0;
                                lSumPenalty = 0;
                                lSumFine = delta;
                            }
                        }
                    }
                }
            }

            lSumBalanceAfter = lSumBalanceBefore - lSumPrincipal;
            if (lSumBalanceAfter < 0) {
                lSumPayment = lSumPayment + lSumBalanceAfter;
                lSumPrincipal = lSumPrincipal + lSumBalanceAfter;
                lSumBalanceAfter = lSumBalanceBefore - lSumPrincipal;
                Notification.show(i18n.get("loanpayment.message.principalexceedbalance"), Notification.Type.TRAY_NOTIFICATION);
            }
        } else {//ручной выбор составлящих платежа
            if (lSumPrincipal > lSumBalanceBefore) {
                lSumPrincipal = lSumBalanceBefore;
                Notification.show(i18n.get("loanpayment.message.principalexceedbalance"), Notification.Type.TRAY_NOTIFICATION);
            }
            lSumPayment = lSumPrincipal + lSumInterest + lSumPenalty + lSumFine;
        }

        lSumBalanceAfter = lSumBalanceBefore - lSumPrincipal;

        recalculationLoanStatusAfter = calculateLoanStatus(lSumBalanceAfter, loan.getLoanStatus());
        recalculationPayment.setStatus(PaymentStatus.PAYMENT_BY_SCHEDULE_MANUAL);
        recalculationPayment.setBalanceAfter(new BigDecimal(lSumBalanceAfter).setScale(scaleValue, scaleMode));
        recalculationPayment.setPrincipal(new BigDecimal(lSumPrincipal).setScale(scaleValue, scaleMode));
        recalculationPayment.setInterest(new BigDecimal(lSumInterest).setScale(scaleValue, scaleMode));
        recalculationPayment.setPenalty(new BigDecimal(lSumPenalty).setScale(scaleValue, scaleMode));
        recalculationPayment.setFine(new BigDecimal(lSumFine).setScale(scaleValue, scaleMode));
        recalculationPayment.setPayment(new BigDecimal(lSumPayment).setScale(scaleValue, scaleMode));

        double lSumPrincipalE = payment.getPrincipalS().doubleValue() - lSumPrincipal;
        if (lSumPrincipalE < 0) lSumPrincipalE = 0;
        double lSumInterestE = payment.getInterestS().doubleValue() - lSumInterest;
        if (lSumInterestE < 0) lSumInterestE = 0;
        double lSumPenaltyE = payment.getPenaltyS().doubleValue() - lSumPenalty;
        if (lSumPenaltyE < 0) lSumPenaltyE = 0;
        double lSumFineE = payment.getFineS().doubleValue() - lSumFine;
        if (lSumFineE < 0) lSumFineE = 0;
        double lSumPaymentE = lSumPrincipalE + lSumInterestE + lSumPenaltyE + lSumFineE;

        recalculationPayment.setPrincipalE(new BigDecimal(lSumPrincipalE).setScale(scaleValue, scaleMode));
        recalculationPayment.setInterestE(new BigDecimal(lSumInterestE).setScale(scaleValue, scaleMode));
        recalculationPayment.setPenaltyE(new BigDecimal(lSumPenaltyE).setScale(scaleValue, scaleMode));
        recalculationPayment.setFineE(new BigDecimal(lSumFineE).setScale(scaleValue, scaleMode));
        recalculationPayment.setPaymentE(new BigDecimal(lSumPaymentE).setScale(scaleValue, scaleMode));

        recalculationScheduleStatusAfter = calculateScheduleStatusAfterPayment(recalculationPayment, scheduleStatusBefore);
    }

    /**
     * Вычисление статуса займа для нового платежа
     *
     * @param sumBalance       - сумма остатка долга перед новым платежом
     * @param loanStatusBefore - статус займа перед новым платежом
     * @return - новый статус займа
     */
    private LoanStatus calculateLoanStatus(double sumBalance, LoanStatus loanStatusBefore) {
        //код состояния займа
        //1-открыт нет просрочек,
        //2-открыт были просрочки,
        //3-открыт есть текущая просрочка
        LoanStatus loanStatusAfter;
        switch (loanStatusBefore) {
            case OPENED_NO_OVERDUE:
                //если займ открыт нет просрочек
                //если остаток >0, то займ открыт нет просрочек, иначе (остаток =0) закрыт нет просрочек
                if (sumBalance > 0.01) loanStatusAfter = LoanStatus.OPENED_NO_OVERDUE;
                else
                    loanStatusAfter = LoanStatus.CLOSED_NO_OVERDUE;
                break;
            case OPENED_WERE_OVERDUES:
                if (sumBalance > 0.01) loanStatusAfter = LoanStatus.OPENED_WERE_OVERDUES;
                else
                    loanStatusAfter = LoanStatus.CLOSED_WERE_OVERDUES;
                break;
            case OPENED_CURRENT_OVERDUE:
                if (sumBalance > 0.01) loanStatusAfter = LoanStatus.OPENED_WERE_OVERDUES;
                else
                    loanStatusAfter = LoanStatus.CLOSED_WERE_OVERDUES;
                break;
            default:
                loanStatusAfter = loanStatusBefore;
        }
        return loanStatusAfter;
    }

    /**
     * Копируем необходимые поля расчета во временный объект
     */
    private void clonePayment() {
        tempPayment.setStatus(payment.getStatus());
        tempPayment.setBalanceAfter(payment.getBalanceAfter());
        tempPayment.setPrincipal(payment.getPrincipal());
        tempPayment.setInterest(payment.getInterest());
        tempPayment.setPenalty(payment.getPenalty());
        tempPayment.setFine(payment.getFine());
        tempPayment.setPayment(payment.getPayment());
        tempPayment.setPrincipalE(payment.getPrincipalE());
        tempPayment.setInterestE(payment.getInterestE());
        tempPayment.setPenaltyE(payment.getPenaltyE());
        tempPayment.setFineE(payment.getFineE());
        tempPayment.setPaymentE(payment.getPaymentE());
    }

    /**
     * Копируем необходимые поля расчета из временного объекта
     */
    private void getFromClone() {
        payment.setStatus(tempPayment.getStatus());
        payment.setBalanceAfter(tempPayment.getBalanceAfter());
        payment.setPrincipal(tempPayment.getPrincipal());
        payment.setInterest(tempPayment.getInterest());
        payment.setPenalty(tempPayment.getPenalty());
        payment.setFine(tempPayment.getFine());
        payment.setPayment(tempPayment.getPayment());
        payment.setPrincipalE(tempPayment.getPrincipalE());
        payment.setInterestE(tempPayment.getInterestE());
        payment.setPenaltyE(tempPayment.getPenaltyE());
        payment.setFineE(tempPayment.getFineE());
        payment.setPaymentE(tempPayment.getPaymentE());
    }

    private void getFromRecalculation() {
        payment.setStatus(recalculationPayment.getStatus());
        payment.setBalanceAfter(recalculationPayment.getBalanceAfter());
        payment.setPrincipal(recalculationPayment.getPrincipal());
        payment.setInterest(recalculationPayment.getInterest());
        payment.setPenalty(recalculationPayment.getPenalty());
        payment.setFine(recalculationPayment.getFine());
        payment.setPayment(recalculationPayment.getPayment());
        payment.setPrincipalE(recalculationPayment.getPrincipalE());
        payment.setInterestE(recalculationPayment.getInterestE());
        payment.setPenaltyE(recalculationPayment.getPenaltyE());
        payment.setFineE(recalculationPayment.getFineE());
        payment.setPaymentE(recalculationPayment.getPaymentE());
    }

    /**
     * Выполняет определение статуса графика рассчитанного платежа
     * по факту расчета платежа
     *
     * @param scheduleStatusBefore - статус перед оплатой
     * @return - статус после оплаты
     */
    private ScheduleStatus calculateScheduleStatusAfterPayment(Payment payment, ScheduleStatus scheduleStatusBefore) {
        boolean b;
        ScheduleStatus status = scheduleStatusBefore;

        boolean s = Stream.of(
            PaymentStatus.PAYMENT_BY_SCHEDULE_FULL,
            PaymentStatus.PAYMENT_BY_SCHEDULE_OVERDUE,
            PaymentStatus.PAYMENT_BY_SCHEDULE_SURCHARGE,
            PaymentStatus.PAYMENT_BY_SCHEDULE_MANUAL,
            PaymentStatus.PAYMENT_FROZEN).anyMatch(e -> e == paymentStatus);

        if (s) {
            b = !(payment.getPrincipalE().doubleValue() >= 0.01d || payment.getInterestE().doubleValue() >= 0.01d ||
                payment.getPenaltyE().doubleValue() >= 0.01d || payment.getFineE().doubleValue() >= 0.01d);

            if (b) {
                status = ScheduleStatus.PAID_FULLY;
            } else {
                if (payment.getPrincipalE().doubleValue() >= 0.01d)
                    status = ScheduleStatus.PAID_PARTIALLY_PRINCIPAL_DEBT;
                if (payment.getInterestE().doubleValue() >= 0.01d)
                    status = ScheduleStatus.PAID_PARTIALLY_INTEREST_DEBT;
                if (payment.getPenaltyE().doubleValue() >= 0.01d)
                    status = ScheduleStatus.PAID_PARTIALLY_PENALTY_DEBT;
                if (payment.getFineE().doubleValue() >= 0.01d) status = ScheduleStatus.PAID_PARTIALLY_FINE_DEBT;
            }
        }

        if (paymentStatus == PaymentStatus.PAYMENT_ADDITIONAL) {/*status = scheduleStatusBefore;*/}

        if (paymentStatus == PaymentStatus.PAYMENT_EARLY_CLOSING) {/*status = ScheduleStatus.NOT_PAID_OVERDUE;*/}

        return status;
    }

    public void resetCalculation() {
        if (!cbxPrincipal.getValue() && !cbxInterest.getValue()
            && !cbxPenalty.getValue() && !cbxFine.getValue()) {
            reset.click();
        }
    }

    private void checkState() {
        recalculate.setEnabled(cbxPrincipal.getValue() || (cbxInterest.getValue() || cbxPenalty.getValue() || cbxFine.getValue() ||
            cbxPayment.getValue()));
    }

    private void setReadOnlyPayment1(boolean isReadOnly) {
        tfPrincipal.setReadOnly(isReadOnly);
        tfInterest.setReadOnly(isReadOnly);
        tfPenalty.setReadOnly(isReadOnly);
        tfFine.setReadOnly(isReadOnly);
    }

    private void setReadOnlyPayment2(boolean isReadOnly) {
        tfPayment.setReadOnly(isReadOnly);
    }

    private void setCheckedPayment1(boolean isChecked) {
        cbxPrincipal.setValue(isChecked);
        cbxInterest.setValue(isChecked);
        cbxPenalty.setValue(isChecked);
        cbxFine.setValue(isChecked);
    }

    private void setCheckedPayment2(boolean isChecked) {
        cbxPayment.setValue(isChecked);
    }

    private void setEnabledPaymentBlock(boolean isEnabled) {
        cbxPrincipal.setEnabled(isEnabled);
        cbxInterest.setEnabled(isEnabled);
        cbxPenalty.setEnabled(isEnabled);
        cbxFine.setEnabled(isEnabled);
        cbxPayment.setEnabled(isEnabled);

        reset.setEnabled(isEnabled);
    }

    private boolean isLoanClosed(Loan loan) {
        return Stream.of(
            LoanStatus.CLOSED_ANNULED,
            LoanStatus.CLOSED_FROZEN,
            LoanStatus.CLOSED_NO_OVERDUE,
            LoanStatus.CLOSED_WERE_OVERDUES
        ).anyMatch(item -> item == loan.getLoanStatus());
    }

    private void clearComponents() {
        orderNumber.setValue("");
        tfPrincipal.setValue("0");
    }

    private void buttonsStatusUpdate() {
        save.setEnabled(!isViewFromLoan && !isLoanClosed && !isDeleted &&
            (payment.getId() == null || (payment.getId() != null && paymentHash != payment.hashCode())));
        print.setEnabled(payment.getId() != null);
    }

    @Override
    public String getTabName() {
        return i18n.get("loanpayment.tab.caption") + loan.getLoanNumber();
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIME;
    }

}

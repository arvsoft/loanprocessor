package ru.loanpro.server.logging.repository;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.lang.Nullable;
import ru.loanpro.global.repository.AbstractIdEntityRepository;
import ru.loanpro.server.logging.domain.AbstractLog;

/**
 * Репозиторий сущности {@link AbstractLog}.
 *
 * @author Oleg Brizhevatikh
 */
public interface AbstractLogRepository extends AbstractIdEntityRepository<AbstractLog> {

    /**
     * {@inheritDoc}
     */
    @EntityGraph(attributePaths = "user")
    @Override
    Page<AbstractLog> findAll(@Nullable Predicate predicate, @Nullable Pageable pageable);
}

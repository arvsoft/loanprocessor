package ru.loanpro.server.ui.module.component;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.vaadin.spring.i18n.I18N;

import ru.loanpro.base.storage.IStorageService;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.base.storage.exception.FileSystemErrorException;

/**
 * Окно, являющееся галереей изображений. Отображает сканированные образы или фотографии.
 * Может отображать список изображений и поддерживает их перелистывание без перезагрузки окна.
 *
 * @author Oleg Brizhevatikh
 */
public class GalleryWindow extends Window {

    private static final int DEFAULT_IMAGE_WIDTH = 700;
    private final I18N i18n;
    private final IStorageService storageService;

    private final Image image = new Image();
    private int imageWidth = 0;

    private List<ScannedDocument> documentListBefore = new ArrayList<>();
    private List<ScannedDocument> documentListAfter = new ArrayList<>();
    private final Button buttonLeft;
    private final Button buttonRight;
    private Registration registrationButtonLeft;
    private Registration registrationButtonRight;

    /**
     * Создаёт галлерею изображений.
     *
     * @param storageService интерфейс для работы с хранилищами файлов.
     * @param i18n сервис локализации интерфейса.
     * @param documents список сканированных документов для отображения.
     * @param document текущий документ, который необходимо показать первым.
     */
    public GalleryWindow(IStorageService storageService, I18N i18n, List<ScannedDocument> documents,
                         ScannedDocument document) {

        super(document.getDisplayName());
        this.i18n = i18n;
        this.storageService = storageService;

        // Положение скана в списке сканов
        int position = documents.indexOf(document);
        // Все документы до этой позиции складываем в первый список
        for (int i = 0; i < position; i++)
            documentListBefore.add(documents.get(i));
        // Остальные документы во второй список
        for (int i = position + 1; i < documents.size(); i++)
            documentListAfter.add(documents.get(i));

        buttonLeft = new Button(VaadinIcons.ARROW_LEFT);
        buttonRight = new Button(VaadinIcons.ARROW_RIGHT);

        displayDocument(document);

        HorizontalLayout horizontalLayout = new HorizontalLayout(buttonLeft, image, buttonRight);
        horizontalLayout.setExpandRatio(image, 1);
        horizontalLayout.setComponentAlignment(buttonLeft, Alignment.MIDDLE_LEFT);
        horizontalLayout.setComponentAlignment(image, Alignment.MIDDLE_CENTER);
        horizontalLayout.setComponentAlignment(buttonRight, Alignment.MIDDLE_RIGHT);

        Button download = new Button(VaadinIcons.DOWNLOAD);
        download.addClickListener(event -> {
            getUI().getPage().open(String.format("/scan/%s", document.getFileName()), "_blank");
        });
        HorizontalLayout actionsLayout = new HorizontalLayout(download);

        VerticalLayout layout = new VerticalLayout(horizontalLayout, actionsLayout);
        layout.setExpandRatio(horizontalLayout, 1);
        layout.setComponentAlignment(actionsLayout, Alignment.BOTTOM_CENTER);

        setContent(layout);
        setModal(true);
    }

    private void displayDocument(ScannedDocument document) {
        buttonLeft.setEnabled(documentListBefore.size() > 0);
        buttonRight.setEnabled(documentListAfter.size() > 0);

        // Удаляем обработчики кнопок, если они есть
        if (registrationButtonLeft != null) {
            registrationButtonLeft.remove();
            registrationButtonRight.remove();
        }

        // Создаём новые обработчики кнопок, обеспечивающие перемещение изображений из двух списков
        // друг в друга и отображающие нужное изображение.
        registrationButtonLeft = buttonLeft.addClickListener(event -> {
            ScannedDocument newDocument = documentListBefore.get(documentListBefore.size() - 1);
            documentListAfter.add(0, document);
            documentListBefore.remove(newDocument);
            displayDocument(newDocument);
        });

        registrationButtonRight = buttonRight.addClickListener(event -> {
            ScannedDocument newDocument = documentListAfter.get(0);
            documentListBefore.add(document);
            documentListAfter.remove(newDocument);
            displayDocument(newDocument);
        });


        try {
            FileResource resource = storageService.getScannedFile(document.getFileName());
            image.setSource(resource);
            imageWidth = ImageIO.read(resource.getSourceFile()).getWidth();
            image.setWidth(imageWidth > DEFAULT_IMAGE_WIDTH ? DEFAULT_IMAGE_WIDTH : imageWidth, Unit.PIXELS);

        } catch (FileSystemErrorException | IOException e) {
            Notification.show(String.format(i18n.get("component.upload.fileSystemError"), e.getMessage()), Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }

        // Обработчик нажатия на изображение. Изменяет размер изображения со стандартного до фактического размера
        // и обратно.
        image.addClickListener(event -> {
            if (image.getWidth() == DEFAULT_IMAGE_WIDTH)
                image.setWidth(imageWidth, Unit.PIXELS);
            else
                image.setWidth(DEFAULT_IMAGE_WIDTH, Unit.PIXELS);

            center();
        });

        center();
    }
}

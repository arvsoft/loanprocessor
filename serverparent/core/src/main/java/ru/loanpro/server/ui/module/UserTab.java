package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.Binder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.Settings;
import ru.loanpro.global.SystemLocale;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.Enable;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.security.domain.Credentials;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.domain.Group;
import ru.loanpro.security.domain.Role;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.DepartmentService;
import ru.loanpro.security.service.UserService;
import ru.loanpro.security.settings.exceptions.SettingsUnsupportedTypeException;
import ru.loanpro.server.ui.module.component.uploader.AvatarUploadWindow;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.server.ui.module.event.UpdateUserEvent;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Профиль пользователя. Может быть вызван пользователем для своей учётной записи
 * или администратором для любой учётной записи.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(User.class)
public class UserTab extends BaseTab {

    private static Logger LOGGER = Logger.getLogger(UserTab.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private AvatarUploadWindow avatarUploadWindow;

    private User user;
    private User me;

    @Enable(Roles.Security.USER_EDIT)
    private TextField username;

    private TabSheet tabSheet;

    private Map<Department, TabSheet.Tab> credentialsTabMap;

    private List<Group> allGroups;

    private List<Role> allRoles;

    private Boolean credentialsValid;
    private Button save;
    private Button saveAndClose;
    private Binder<User> binder;
    private Binder<User> binderUsername;
    private Binder<User> binderPassword;
    private TreeGrid<Department> departmentGrid = new TreeGrid<>();
    private ComboBox<UiTheme> theme;
    private ComboBox<ZoneId> timeZone;
    private ComboBox<SystemLocale> locale;
    private ComboBox<GridStyle> gridStyle;
    private String originalUsername = "";

    public UserTab(User user) {
        this.user = (user == null) ? new User() : user;
    }

    @PostConstruct
    public void init() {
        me = securityService.getUser();
        allGroups = userService.findAllGroups();
        allRoles = userService.findAllRoles();

        credentialsValid = user.getId() != null;

        if (user.getId() == null) {
            user.setEnabled(true);
            user.setAccountNonExpired(true);
            user.setAccountNonLocked(true);
            user.setCredentialsNonExpired(true);
        } else {
            // Иначе скрываем пароль и запоминаем логин
            user.setPassword("");
            originalUsername = user.getUsername();
        }

        setSpacing(true);
        setMargin(true);

        FormLayout form = new FormLayout();
        form.setMargin(true);
        form.setSpacing(true);
        form.setWidth("800px");
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);

        Label section = new Label(i18n.get("security.user.baseInfo"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        Image avatar = new Image("", storageService.getAvatar(user.getAvatarFileName()));
        avatar.setWidth(128, Unit.PIXELS);
        avatar.setHeight(128, Unit.PIXELS);
        avatar.addStyleName("cursor-pointer");
        avatar.addClickListener(event -> {
            avatarUploadWindow.setAvatarUploadCallbackHandler(fileName -> {
                user.setAvatarFileName(fileName);
                avatar.setSource(storageService.getAvatar(user.getAvatarFileName()));
                setSaveButtonsState();
            });
            UI.getCurrent().addWindow(avatarUploadWindow);
        });
        VerticalLayout avatarLayout = new VerticalLayout(avatar);
        avatarLayout.setMargin(true);
        form.addComponent(avatarLayout);

        username = new TextField(i18n.get("security.user.username"));
        username.setWidth(50, Unit.PERCENTAGE);
        username.setEnabled(user.getId() == null || securityService.hasAuthority(Roles.UserProfile.CHANGE_LOGIN));
        form.addComponent(username);

        PasswordField password = new PasswordField(i18n.get("security.user.password"));
        password.setWidth(50, Unit.PERCENTAGE);
        password.setEnabled(securityService.hasAuthority(Roles.UserProfile.CHANGE_PASSWORD));
        form.addComponent(password);

        PasswordField passwordAgain = new PasswordField(i18n.get("security.user.passwordAgain"));
        passwordAgain.setWidth(50, Unit.PERCENTAGE);
        passwordAgain.setEnabled(password.isEnabled());
        form.addComponent(passwordAgain);

        TextField lastName = new TextField(i18n.get("security.user.lastName"));
        lastName.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(lastName);

        TextField firstName = new TextField(i18n.get("security.user.firstName"));
        firstName.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(firstName);

        TextField middleName = new TextField(i18n.get("security.user.middleName"));
        middleName.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(middleName);

        TextField phone = new TextField(i18n.get("security.user.phone"));
        phone.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(phone);

        TextField email = new TextField(i18n.get("security.user.email"));
        email.setWidth(50, Unit.PERCENTAGE);
        form.addComponent(email);

        section = new Label(i18n.get("security.user.settings"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        // Наследование настроек
        boolean isCanChangeSettings = securityService.hasAuthority(Roles.UserProfile.CHANGE_SETTINGS);

        CheckBox inheritance = new CheckBox(i18n.get("user.settings.inheritance"));
        inheritance.addValueChangeListener(event -> updateUserSettingsFieldsState(event.getValue()));
        inheritance.setEnabled(isCanChangeSettings);
        form.addComponent(inheritance);

        theme = new ComboBox<>(i18n.get("security.user.theme"));
        theme.setItems(UiTheme.values());
        theme.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        theme.setWidth(50, Unit.PERCENTAGE);
        theme.setEmptySelectionAllowed(false);
        theme.setEnabled(isCanChangeSettings);
        form.addComponent(theme);

        timeZone = new ComboBox<>(i18n.get("settings.timezone"));
        List<ZoneId> timeZoneItemList = chronometerService.getAllZoneIds();
        timeZone.setItems(timeZoneItemList);
        timeZone.setPageLength(timeZoneItemList.size());
        timeZone.setWidth(50, Unit.PERCENTAGE);
        timeZone.setEmptySelectionAllowed(false);
        timeZone.setItemCaptionGenerator(item -> String.format("%s %s",
            ZonedDateTime.now(item).getOffset().getId().replace("Z", "+00:00"), item));
        timeZone.setEnabled(isCanChangeSettings);
        form.addComponent(timeZone);

        locale = new ComboBox<>(i18n.get("security.user.locale"));
        locale.setItems(SystemLocale.values());
        locale.setEmptySelectionAllowed(false);
        locale.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        locale.setWidth(50, Unit.PERCENTAGE);
        locale.setEnabled(isCanChangeSettings);
        form.addComponent(locale);

        gridStyle = new ComboBox<>(i18n.get("security.user.gridStyle"));
        gridStyle.setItems(GridStyle.values());
        gridStyle.setEmptySelectionAllowed(false);
        gridStyle.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        gridStyle.setWidth(50, Unit.PERCENTAGE);
        gridStyle.setEnabled(isCanChangeSettings);
        form.addComponent(gridStyle);

        if (securityService.hasAuthority(Roles.UserProfile.CHANGE_LOCATION_AND_PRIVILEGES)) {

            section = new Label(i18n.get("security.user.location"));
            section.addStyleName(MaterialTheme.LABEL_H3);
            section.addStyleName(MaterialTheme.LABEL_COLORED);
            form.addComponent(section);

            departmentGrid.addStyleName(MaterialTheme.CARD_2);
            //departmentGrid.addStyleName("layout-margin-top");
            departmentGrid.setWidth(100, Unit.PERCENTAGE);
            departmentGrid.setHeightByRows(5L);
            departmentGrid.setSelectionMode(Grid.SelectionMode.MULTI);

            List<Department> departments = departmentService.getRootDepartments();
            List<Department> flatDepartments = departments.stream()
                .flatMap(department -> department.flattened())
                .collect(Collectors.toList());

            departmentGrid.setItems(departments, Department::getChilds);
            departmentGrid.expand(departments.stream().flatMap(Department::flattened).collect(Collectors.toList()));

            Grid.Column<Department, String> columnName = departmentGrid
                .addColumn(Department::getName).setCaption(i18n.get("settings.department.grid.column.name"));
            departmentGrid.addColumn(Department::getDescription).setCaption(i18n.get("settings.department.grid.column.description"));
            departmentGrid.sort(columnName);

            user.getCredentials().stream()
                .map(Credentials::getDepartment)
                .forEach(item -> {
                    credentialsValid = true;
                    Department selectedDepartment = flatDepartments.stream()
                        .filter(department -> department.getId().equals(item.getId()))
                        .findFirst()
                        .get();

                    departmentGrid.select(selectedDepartment);
                });

            VerticalLayout vlDepartment = new VerticalLayout(departmentGrid);
            vlDepartment.setMargin(true);
            vlDepartment.setSpacing(false);
            form.addComponents(vlDepartment);

            tabSheet = new TabSheet();
            tabSheet.setWidth(100, Unit.PERCENTAGE);
            tabSheet.addStyleNames(MaterialTheme.TABSHEET_FRAMED, MaterialTheme.TABSHEET_CENTERED_TABS,
                ValoTheme.TABSHEET_PADDED_TABBAR);
            credentialsTabMap = new HashMap<>();
            user.getCredentials().forEach(this::addCredentialsTab);
        }

        save = new Button(i18n.get("security.user.save"));
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        save.setEnabled(false);
        saveAndClose = new Button(i18n.get("security.user.saveAndClose"));
        saveAndClose.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        saveAndClose.setEnabled(false);

        binderUsername = new Binder<>();
        binderUsername.setBean(user);

        binderUsername.forField(username)
            .asRequired()
            .withValidator(value -> value.matches("[\\w\\s]+"), i18n.get("security.user.wrongUsername"))
            .withValidator(value -> {
                // Для установки или изменения имени пользователя, новое имя пользователя должно
                // быть свободно и не совпадать с текущим именем пользователя.
                return originalUsername.equals(value) || userService.usernameIsVacant(value);
            }, i18n.get("security.user.alreadyExists"))
            .bind(User::getUsername, User::setUsername);

        binderPassword = new Binder<>();
        binderPassword.setBean(user);

        binderPassword.forField(password)
            .withValidator(value -> {
                // Для нового пользователя пароль должен быть больше 5 символов.
                // Для редактируемого пользователя пароль должен быть или пустым или больше 5 символов.
                return (user.getId() == null && value.length() > 5)
                    || (user.getId() != null && (value.length() == 0 || value.length() > 5));
            }, i18n.get("security.user.passwordLength"))
            .bind(User::getPassword, User::setPassword);

        binderPassword.forField(passwordAgain)
            .withValidator(value -> value.equals(password.getValue()), i18n.get("security.user.passwordRetypeError"))
            .bind(User::getPassword, User::setPassword);

        binder = new Binder<>();
        binder.setBean(user);

        binder.bind(inheritance, User::getInheritance, User::setInheritance);
        binder.bind(locale, User::getLocale, User::setLocale);
        binder.bind(timeZone, User::getZoneId, User::setZoneId);
        binder.bind(theme, User::getTheme, User::setTheme);
        binder.bind(firstName, User::getFirstName, User::setFirstName);
        binder.bind(lastName, User::getLastName, User::setLastName);
        binder.bind(middleName, User::getMiddleName, User::setMiddleName);
        binder.bind(phone, User::getPhone, User::setPhone);
        binder.bind(email, User::getEmail, User::setEmail);

        updateUserSettingsFieldsState(user.getInheritance());

        binder.addValueChangeListener(event -> setSaveButtonsState());
        binderUsername.addValueChangeListener(event -> setSaveButtonsState());
        binderPassword.addValueChangeListener(event -> setSaveButtonsState());

        departmentGrid.addSelectionListener(event -> {
            if (user.getCredentials().size() < event.getAllSelectedItems().size()) {
                // Был добавлен отдел
                Set<UUID> uuids = user.getCredentials()
                    .stream()
                    .map(Credentials::getDepartment)
                    .map(Department::getId)
                    .collect(Collectors.toSet());

                Department addedDepartment = event.getAllSelectedItems()
                    .stream()
                    .filter(item -> !uuids.contains(item.getId()))
                    .findFirst()
                    .get();

                Credentials credential = new Credentials();
                credential.setId(UUID.randomUUID());
                credential.setUser(user);
                credential.setDepartment(addedDepartment);
                credential.setDefault(false);
                user.getCredentials().add(credential);

                addCredentialsTab(credential);
            } else {
                // Был убран отдел
                Set<UUID> uuidSet = event.getAllSelectedItems().stream()
                    .map(Department::getId)
                    .collect(Collectors.toSet());

                Credentials removedCredential = user.getCredentials().stream()
                    .filter(item -> !uuidSet.contains(item.getDepartment().getId()))
                    .findFirst()
                    .get();

                user.getCredentials().remove(removedCredential);
                tabSheet.removeTab(credentialsTabMap.get(removedCredential.getDepartment()));
            }

            // Проверка наличия хотя бы одного выбранного отдела
            credentialsValid = event.getAllSelectedItems().size() != 0;

            setSaveButtonsState();
        });

        save.addClickListener(event -> {
            user = userService.save(user);

            originalUsername = user.getUsername();
            user.setPassword("");

            binder.setBean(user);
            binderUsername.setBean(user);
            binderPassword.setBean(user);
            sessionEventBus.post(new UpdateUserEvent());

            setSaveButtonsState();

            try {
                GridStyle style = gridStyle.getValue();
                settingsService.set(user, Settings.GRID_STYLE, style);
            } catch (SettingsUnsupportedTypeException e) {
                Notification.show(i18n.get("security.user.saved"), Type.WARNING_MESSAGE);
                e.printStackTrace();
            }

            Notification.show(i18n.get("security.user.saved"), Type.TRAY_NOTIFICATION);
        });

        saveAndClose.addClickListener(event -> {
            save.click();
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        HorizontalLayout hlButtons = new HorizontalLayout(save, saveAndClose);
        hlButtons.setSpacing(true);

        if (tabSheet != null) addComponents(hlButtons, form, tabSheet); else addComponents(hlButtons, form);

        setComponentAlignment(hlButtons, Alignment.TOP_RIGHT);
        setComponentAlignment(form, Alignment.TOP_CENTER);
    }

    private void updateUserSettingsFieldsState(Boolean inheritance) {
        theme.setVisible(!inheritance);
        timeZone.setVisible(!inheritance);
        locale.setVisible(!inheritance);
        gridStyle.setVisible(!inheritance);
        gridStyle.setValue(settingsService.get(user, GridStyle.class, Settings.GRID_STYLE, GridStyle.NORMAL));

        if (!inheritance) {
            if (user.getTheme() == null) {
                user.setTheme(settingsService.get(UiTheme.class, Settings.THEME, UiTheme.LIGHT));
                user.setZoneId(ZoneId.of(settingsService.get(String.class, Settings.TIME_ZONE, "UCT")));
                user.setLocale(settingsService.get(SystemLocale.class, Settings.LOCALE, SystemLocale.ENGLISH));
            }
            binder.bind(theme, User::getTheme, User::setTheme);
            binder.bind(timeZone, User::getZoneId, User::setZoneId);
            binder.bind(locale, User::getLocale, User::setLocale);
        } else {
            binder.removeBinding(theme);
            binder.removeBinding(timeZone);
            binder.removeBinding(locale);
        }
    }

    private void addCredentialsTab(Credentials credential) {
        HorizontalLayout layout = new HorizontalLayout(getGroupGrid(credential), getRoleGrid(credential));
        layout.setMargin(true);
        layout.setWidth(100, Unit.PERCENTAGE);
        TabSheet.Tab tab = tabSheet.addTab(layout, credential.getDepartment().getName());
        credentialsTabMap.put(credential.getDepartment(), tab);
    }

    private Grid<Group> getGroupGrid(Credentials credential) {
        Grid<Group> grid = new Grid<>(i18n.get("security.user.grid.groups"), allGroups);
        grid.setHeightByRows(7);
        grid.setWidth(100, Unit.PERCENTAGE);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        grid.addColumn(Group::getName).setCaption(i18n.get("security.user.grid.name"));
        grid.addColumn(Group::getDescription).setCaption(i18n.get("security.user.grid.description"));

        if (credential != null)
            credential.getGroups().forEach(grid::select);

        grid.addSelectionListener(event -> {
            credential.setGroups(event.getAllSelectedItems());
            credentialsValid = true;
            setSaveButtonsState();
        });

        return grid;
    }

    private Grid<Role> getRoleGrid(Credentials credential) {
        Grid<Role> grid = new Grid<>(i18n.get("security.user.grid.roles"), allRoles);
        grid.setHeightByRows(7);
        grid.setWidth(100, Unit.PERCENTAGE);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        grid.addColumn(Role::getDescription).setCaption(i18n.get("security.user.grid.description"));
        Grid.Column<Role, String> column = grid.addColumn(Role::getAuthority).setCaption(i18n.get("security.user.grid.name"));
        grid.sort(column, SortDirection.ASCENDING);

        if (credential != null)
            credential.getRoles().forEach(grid::select);

        grid.addSelectionListener(event -> {
            credential.setRoles(event.getAllSelectedItems());
            credentialsValid = true;
            setSaveButtonsState();
        });

        return grid;
    }

    private void setSaveButtonsState() {
        boolean enable = binder.isValid()
            && binderUsername.isValid()
            && binderPassword.isValid()
            && credentialsValid;

        save.setEnabled(enable);
        saveAndClose.setEnabled(enable);
    }

    @Override
    public String getTabName() {
        if (user.getId() == null)
            return i18n.get("user.tabName.new");
        else if (user.getId().equals(me.getId()))
            return String.format(i18n.get("user.tabName.profile"), user.getUsername());
        else
            return String.format(i18n.get("user.tabName.edit"), user.getUsername());
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.PURPLE;
    }
}

package ru.loanpro.server.ui.module.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.MenuItem;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.loan.details.LoanNote;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.service.LoanNoteService;
import ru.loanpro.global.Roles;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.SecurityService;

import java.time.format.DateTimeFormatter;

/**
 * Окно формы добавления заметки к займу
 * Окно вызывается из формы списка займов
 *
 * @author Maksim Askhaev
 */
public class LoanNotesForm extends Window {

    private TextField note;
    private Button save;
    private Grid<LoanNote> grid;
    private ContentChangeListener listener;

    public LoanNotesForm(ContextMenu contextMenu1, Loan loan, I18N i18n, ChronometerService chronometerService, LoanNoteService loanNoteService,
                         SecurityService securityService, DateTimeFormatter dateTimeFormatter, ContentChangeListener listener) {
        super(i18n.get("loanNotes.window.caption").concat(" ").concat(loan.getLoanNumber()));

        this.listener = listener;
        if (contextMenu1.getParent() != null) contextMenu1.remove();

        center();
        setModal(true);
        setResizable(false);

        note = new TextField(i18n.get("loanNotes.field.note"));
        note.focus();
        note.setSizeFull();

        save = new Button(i18n.get("loanNotes.button.save"));
        save.setWidth(180, Unit.PIXELS);
        save.setIcon(VaadinIcons.ADD_DOCK);
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setWidth(670, Unit.PIXELS);
        grid.setHeight(300, Unit.PIXELS);

        Grid.Column columnNote = grid.addColumn(LoanNote::getNote).setCaption(i18n.get("loanNotes.grid.title.note"));
        columnNote.setWidth(600);
        grid.addColumn(item -> item.getNoteDate().format(dateTimeFormatter))
            .setCaption(i18n.get("loanNotes.grid.title.date"));
        grid.addColumn(item -> item.getUser().getUsername()).setCaption(i18n.get("loanNotes.grid.title.user"));
        grid.setItems(loanNoteService.findAllByLoan(loan));

        grid.addContextClickListener(event->{
            Grid.GridContextClickEvent<LoanNote> contextEvent = (Grid.GridContextClickEvent<LoanNote>) event;

            ContextMenu contextMenu = new ContextMenu(grid, true);
            contextMenu.removeItems();

            LoanNote loanNote = contextEvent.getItem();

            MenuItem deleteIitem = contextMenu.addItem(i18n.get("loanNotes.grid.context.deleteNote"), e -> {
                loanNoteService.delete(loanNote);
                grid.setItems(loanNoteService.findAllByLoan(loan));
                listener.contentChanged();
                contextMenu.remove();
            });
            deleteIitem.setEnabled(securityService.hasAuthority(Roles.Loan.LOANNOTE_DELETION));

            contextMenu.open(event.getClientX(), event.getClientY());

        });

        HorizontalLayout hlButtons = new HorizontalLayout(save);

        VerticalLayout layout = new VerticalLayout();
        layout.setSizeUndefined();
        layout.setMargin(true);
        layout.addComponents(note, hlButtons, grid);
        layout.setComponentAlignment(hlButtons, Alignment.BOTTOM_RIGHT);

        save.addClickListener(event -> {
            if (!note.getValue().isEmpty()) {
                LoanNote loanNote = new LoanNote();
                loanNote.setLoan(loan);
                loanNote.setNote(note.getValue());
                loanNote.setNoteDate(chronometerService.getCurrentTime().toLocalDate());
                loanNote.setUser(securityService.getUser());
                loanNoteService.save(loanNote);
                grid.setItems(loanNoteService.findAllByLoan(loan));
                listener.contentChanged();
                Notification.show(i18n.get("loanNotes.message.saved"), Notification.Type.TRAY_NOTIFICATION);
            } else {
                Notification.show(i18n.get("loanNotes.message.noteEmpty"), Notification.Type.WARNING_MESSAGE);
            }
        });

        //стиль ко всей странице
        addStyleName(MaterialTheme.LABEL_COLORED);
        setContent(layout);
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }

}

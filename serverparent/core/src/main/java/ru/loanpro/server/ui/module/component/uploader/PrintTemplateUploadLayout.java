package ru.loanpro.server.ui.module.component.uploader;

import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;

import ru.loanpro.base.storage.IStorageService;
import ru.loanpro.base.storage.exception.FileSystemErrorException;

/**
 * Панель загрузки и отображения шаблонов печатаемых документов. Может биндиться к сущности с помощью
 * стандартного {@link com.vaadin.data.Binder}.
 *
 * @author Oleg Brizhevatikh
 */
public class PrintTemplateUploadLayout extends VerticalLayout implements HasValue<String> {

    private final I18N i18n;

    private ValueChangeListener<String> listener;

    private final VerticalLayout layout;
    private final UploadPane uploadPane;
    private String templateFileName;
    private boolean isRequired;

    public PrintTemplateUploadLayout(IStorageService storageService, I18N i18n) {
        this.i18n = i18n;

        uploadPane = new UploadPane((fileName, bas) -> {
            try {
                FileResource resource = storageService.savePrintTemplate(bas);
                String oldTemplateFileName = templateFileName;
                templateFileName = resource.getFilename();
                showFile();

                listener.valueChange(new ValueChangeEvent<>(this, this, oldTemplateFileName, false));
            } catch (FileSystemErrorException e) {
                Notification.show(String.format(i18n.get("component.upload.fileSystemError"),
                    e.getMessage()), Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
            }
        }, i18n, UploadPane.SelectType.SINGLE, UploadPane.FileExtension.ODT);

        layout = new VerticalLayout();
        addComponents(layout, uploadPane);
        setExpandRatio(uploadPane, 1);
    }

    private void showFile() {
        if (templateFileName == null) {
            uploadPane.setVisible(true);
            layout.setVisible(false);
        } else {
            uploadPane.setVisible(false);

            Label label = new Label(String.format(i18n.get("component.upload.templateExists"), templateFileName));
            Button delete = new Button(i18n.get("component.upload.templateRemove"), VaadinIcons.TRASH);
            delete.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

            delete.addClickListener(event -> {
                templateFileName = null;
                showFile();
            });

            layout.removeAllComponents();
            layout.addComponents(label, delete);
            layout.setVisible(true);
        }
    }

    @Override
    public void setValue(String fileName) {
        this.templateFileName = fileName;
        showFile();
    }

    @Override
    public String getValue() {
        return templateFileName;
    }

    @Override
    public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {
        this.isRequired = requiredIndicatorVisible;
    }

    @Override
    public boolean isRequiredIndicatorVisible() {
        return isRequired;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<String> listener) {
        this.listener = listener;

        return () -> {};
    }
}

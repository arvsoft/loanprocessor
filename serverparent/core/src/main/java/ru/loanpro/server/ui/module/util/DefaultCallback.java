package ru.loanpro.server.ui.module.util;

/**
 * Функциональный интерфейс обратного вызова, обеспечиващий выполенние операции.
 * Не принимает и не возвращает ни каких параметров.
 *
 * @author Oleg Brizhevatikh
 */
@FunctionalInterface
public interface DefaultCallback {
    void run();
}

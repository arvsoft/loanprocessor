package ru.loanpro.server.ui.module.component.uploader;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.vaadin.spring.i18n.I18N;

import ru.loanpro.base.storage.StorageService;
import ru.loanpro.base.storage.domain.ScannedDocument;
import ru.loanpro.base.storage.exception.FileSystemErrorException;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.server.ui.module.component.GalleryWindow;
import ru.loanpro.server.ui.module.component.uploader.UploadPane.FileExtension;

/**
 * Панель загрузки и отображения сканированных документов. Может биндиться к сущности с помощью
 * стандартного {@link com.vaadin.data.Binder}.
 *
 * @author Oleg Brizhevatikh
 */
public class ScansUploadLayout extends VerticalLayout implements HasValue<Set<ScannedDocument>> {

    private final VerticalLayout layout;
    private final StorageService storageService;
    private final I18N i18n;
    private List<ScannedDocument> documents;
    private ValueChangeListener<Set<ScannedDocument>> listener;

    public ScansUploadLayout(User user, StorageService storageService, ChronometerService chronometerService,
                             I18N i18n) {
        this.storageService = storageService;
        this.i18n = i18n;
        UploadPane dropPane = new UploadPane((fileName, bas) -> {
            try {
                FileResource resource = storageService.saveScannedFile(fileName, bas);

                ScannedDocument document = new ScannedDocument();
                document.setFileName(resource.getFilename());
                document.setDisplayName(fileName);
                document.setUser(user);
                document.setDateTime(chronometerService.getCurrentInstant());
                documents.add(document);

                addDocumentToLayout(document);

                Set<ScannedDocument> oldDocuments = new HashSet<>(documents);
                listener.valueChange(new ValueChangeEvent<>(this, this, oldDocuments, false));
            } catch (FileSystemErrorException e) {
                Notification.show(String.format(i18n.get("component.upload.fileSystemError"),
                    e.getMessage()), Type.ERROR_MESSAGE);
                e.printStackTrace();
            }
        }, i18n, UploadPane.SelectType.MULTI, FileExtension.JPG);

        layout = new VerticalLayout();
        addComponents(layout, dropPane);
    }

    private void addDocumentToLayout(ScannedDocument item) {
        String originalName = item.getDisplayName();
        if (originalName.length() > 25)
            originalName = originalName.substring(0, 20) + "...";

        Button button = new Button(originalName, VaadinIcons.PICTURE);
        button.addStyleName(MaterialTheme.BUTTON_BORDERLESS);
        button.addClickListener(event ->
            UI.getCurrent().addWindow(
                new GalleryWindow(storageService, i18n, new ArrayList<>(documents), item)));
        layout.addComponent(button);
    }

    @Override
    public void setValue(Set<ScannedDocument> value) {
        layout.removeAllComponents();

        if (value != null) {
            documents = value.stream()
                .sorted((o1, o2) -> o1.getDisplayName().compareToIgnoreCase(o2.getDisplayName()))
                .collect(Collectors.toList());
            value.stream()
                .sorted((o1, o2) -> o1.getDisplayName().compareToIgnoreCase(o2.getDisplayName()))
                .forEach(this::addDocumentToLayout);
        } else {
            documents = new ArrayList<>();
        }
    }

    @Override
    public Set<ScannedDocument> getValue() {
        return new HashSet<>(documents);
    }

    @Override
    public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {

    }

    @Override
    public boolean isRequiredIndicatorVisible() {
        return false;
    }

    @Override
    public void setReadOnly(boolean readOnly) {

    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<Set<ScannedDocument>> listener) {
        this.listener = listener;

        return (Registration) () -> {};
    }
}

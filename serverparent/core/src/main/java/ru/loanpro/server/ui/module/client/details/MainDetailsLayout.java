package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.textfieldformatter.CustomStringBlockFormatter;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.directory.domain.MaritalStatus;
import ru.loanpro.directory.service.MaritalStatusService;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.additional.CurrencyFieldFormatter;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.component.DirectoryComboBox;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.math.BigDecimal;

/**
 * Панель с основной информацией о клиенте - физическом лице.
 *
 * @author Maksim Askhaev
 */
public class MainDetailsLayout extends VerticalLayout {

    private ContentChangeListener listener;
    private Binder<PhysicalClientDetails> binderDetails;

    public MainDetailsLayout(User user, ChronometerService chronometerService, I18N i18n,
                             PhysicalClientDetails details, MaritalStatusService maritalStatusService,
                             ConfigurationService configurationService, StorageService storageService) {

        binderDetails = new Binder<>();

        TextField birthPlace = new TextField(i18n.get("client.field.birthplace"));
        birthPlace.setWidth(450, Unit.PIXELS);
        HorizontalLayout layoutGender = new HorizontalLayout(birthPlace);
        layoutGender.addStyleName("layout-margin-bottom");

        DirectoryComboBox<MaritalStatus, MaritalStatusService> marital =
            new DirectoryComboBox<>(i18n.get("client.field.marital"), maritalStatusService);
        marital.setItemCaptionGenerator(MaritalStatus::getName);
        marital.setEmptySelectionAllowed(false);
        marital.setSelectedItem(details.getMaritalStatus());
        TextField kids = new TextField(i18n.get("client.field.kids"));
        HorizontalLayout layoutMarital = new HorizontalLayout(marital, kids);
        layoutMarital.addStyleName("layout-margin-bottom");

        TextField mobilePhone = new TextField(i18n.get("client.field.mobilephone"));
        mobilePhone.setIcon(VaadinIcons.MOBILE);
        TextField homePhone = new TextField(i18n.get("client.field.homephone"));
        homePhone.setIcon(VaadinIcons.PHONE);
        TextField workPhone = new TextField(i18n.get("client.field.workphone"));
        workPhone.setIcon(VaadinIcons.PHONE);
        HorizontalLayout layoutPhones = new HorizontalLayout(mobilePhone, homePhone, workPhone);
        layoutPhones.addStyleName("layout-margin-bottom");

        getPhoneFormatter().extend(mobilePhone);
        getPhoneFormatter().extend(homePhone);
        getPhoneFormatter().extend(workPhone);

        TextField email = new TextField(i18n.get("client.field.email"));
        email.setIcon(VaadinIcons.MAILBOX);
        TextField ssn = new TextField(i18n.get("client.field.ssn"));
        TextField tax = new TextField(i18n.get("client.field.tax"));
        TextField yearIncome = new TextField(i18n.get("client.field.yearincome"));
        yearIncome.addStyleName("align-right");
        new CurrencyFieldFormatter(configurationService.getLocale()).extend(yearIncome);

        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        VerticalLayout verticalLayout = new VerticalLayout(email, ssn, tax, yearIncome);
        HorizontalLayout horizontalLayout = new HorizontalLayout(verticalLayout, scansUpload);

        VerticalLayout layout = new VerticalLayout(
            layoutGender, layoutMarital, layoutPhones, horizontalLayout);

        binderDetails.bind(birthPlace, PhysicalClientDetails::getBirthPlace, PhysicalClientDetails::setBirthPlace);
        binderDetails.bind(marital, PhysicalClientDetails::getMaritalStatus, PhysicalClientDetails::setMaritalStatus);
        binderDetails.forField(kids)
            .withConverter(new StringToIntegerConverter(i18n.get("client.field.mustBeANumber")))
            .withNullRepresentation(0)
            .bind(PhysicalClientDetails::getKids, PhysicalClientDetails::setKids);

        binderDetails.bind(mobilePhone, PhysicalClientDetails::getMobilePhone, PhysicalClientDetails::setMobilePhone);
        binderDetails.bind(homePhone, PhysicalClientDetails::getHomePhone, PhysicalClientDetails::setHomePhone);
        binderDetails.bind(workPhone, PhysicalClientDetails::getWorkPhone, PhysicalClientDetails::setWorkPhone);

        binderDetails.bind(ssn, PhysicalClientDetails::getSSNNumber, PhysicalClientDetails::setSSNNumber);
        binderDetails.bind(tax, PhysicalClientDetails::getTaxNumber, PhysicalClientDetails::setTaxNumber);

        binderDetails.forField(email)
            .withNullRepresentation("")
            .withValidator(new EmailValidator(i18n.get("client.field.email.error")))
            .bind(PhysicalClientDetails::getEmail, PhysicalClientDetails::setEmail);

        binderDetails.forField(yearIncome)
            .withConverter(new MoneyConverter(configurationService.getLocale(), i18n.get("client.field.mustBeANumber")))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(PhysicalClientDetails::getYearIncome, PhysicalClientDetails::setYearIncome);

        binderDetails.forField(scansUpload)
            .bind(PhysicalClientDetails::getScans, PhysicalClientDetails::setScans);

        binderDetails.setBean(details);

        binderDetails.addValueChangeListener(event -> {
            if (binderDetails.isValid()) {
                listener.contentChanged();
            }
        });

        addComponents(layout);
        setSizeFull();
    }

    private CustomStringBlockFormatter getPhoneFormatter() {
        return new CustomStringBlockFormatter.Builder()
            .blocks(1, 3, 3, 2, 2)
            .delimiters(" ")
            .prefix("+", " ")
            .numeric()
            .build();
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }

}

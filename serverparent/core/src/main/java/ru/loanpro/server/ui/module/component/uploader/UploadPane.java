package ru.loanpro.server.ui.module.component.uploader;

import com.google.common.collect.Sets;
import com.vaadin.server.Sizeable;
import com.vaadin.server.StreamVariable;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.dnd.FileDropTarget;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.vaadin.spring.i18n.I18N;

import static com.vaadin.ui.Upload.Receiver;

/**
 * Панель загрузки файлов. Обеспечивает загрузку файла из браузера пользователя. Возвращает через обратный вызов
 * {@link ByteArrayOutputStream} с содержимым загружаемого файла.
 *
 * @author Oleg Brizhevatikh
 */
public class UploadPane extends VerticalLayout {

    private static final int FILE_SIZE_LIMIT = 4 * 1024 * 1024; // 4MB

    private final ProgressBar progress = new ProgressBar();

    private final List<String> correctExtensions;
    private final I18N i18n;

    /**
     * Интерфейс обратного вызова для уведомления класса, ожтдающего загрузки файла, о том, что началась
     * загрузка файла.
     */
    public interface UploadCallback {
        /**
         * Загрузка файла из браузера клиента.
         *
         * @param fileName имя загружаемого файла.
         * @param bas {@link ByteArrayOutputStream} загружаемого файла.
         */
        void uploading(String fileName, ByteArrayOutputStream bas);
    }

    /**
     * Перечисление типов загрузки файлов. Загружен может быть один или несколько файлов за раз.
     */
    public enum SelectType {
        SINGLE, MULTI
    }

    /**
     * Коллекция расширений загружаемых фалов.
     */
    public enum FileExtension {
        JPG("JPG", "JPEG"), PNG("PNG"), ODT("ODT");

        String[] extension;

        FileExtension(String... extension) {
            this.extension = extension;
        }

        public Set<String> getExtensions() {
            return Sets.newHashSet(extension);
        }
    }

    /**
     * Базовый конструктор. Создаёт форму и подготавливает инструменты для загрузки файлов.
     *
     * @param uploadCallback экземпляр класса, реализующего интерфейс обратного вызова {@link UploadCallback},
     *                       ожидающий загрузки файла.
     * @param i18n класс локализации интерфейса.
     * @param selectType тип загрузки файлов.
     * @param fileExtensions массив расширений фалов, которые могут быть загружены.
     */
    public UploadPane(UploadCallback uploadCallback, I18N i18n, SelectType selectType, FileExtension... fileExtensions) {
        this.i18n = i18n;

        // Извлечение разрешённых расширений файлов
        correctExtensions = Stream.of(fileExtensions)
            .map(FileExtension::getExtensions)
            .flatMap(Collection::stream)
            .sorted()
            .collect(Collectors.toList());

        final Label infoLabel = new Label(i18n.get("component.upload.dropHere"));
        infoLabel.setWidth(240.0f, Sizeable.Unit.PIXELS);

        UploadReceiver uploadReceiver = new UploadReceiver();
        Upload upload = new Upload("", uploadReceiver);
        upload.setButtonCaption(i18n.get("component.upload.selectHere"));
        upload.addStartedListener(event -> {
            if (event.getContentLength() > FILE_SIZE_LIMIT) {
                // Проверка на превышение установленного размера файла
                Notification.show(i18n.get("component.upload.singleFile"), Type.TRAY_NOTIFICATION);
                upload.interruptUpload();
            } else if (!validateExtension(event.getFilename())) {
                // Проверка расширения файла на наличие в списке разрешённых расширений
                upload.interruptUpload();
            } else {
                progress.setVisible(true);
            }
        });
        upload.addSucceededListener(event -> {
            uploadCallback.uploading(event.getFilename(), uploadReceiver.getByteArrayOutputStream());
            progress.setVisible(false);
        });

        addComponents(infoLabel, upload);
        setComponentAlignment(infoLabel, Alignment.MIDDLE_CENTER);
        setComponentAlignment(upload, Alignment.MIDDLE_LEFT);
        addStyleName("drop-area");
        setSizeUndefined();

        progress.setIndeterminate(true);
        progress.setVisible(false);
        addComponent(progress);

        new FileDropTarget<>(this, fileDropEvent -> {
            if (selectType == SelectType.SINGLE && fileDropEvent.getFiles().size() > 1) {
                Notification.show(i18n.get("component.upload.singleFile"), Type.TRAY_NOTIFICATION);
                return;
            }

            fileDropEvent.getFiles().forEach(html5File -> {
                final String fileName = html5File.getFileName();

                if (html5File.getFileSize() > FILE_SIZE_LIMIT) {
                    Notification.show(i18n.get("component.upload.rejectedBySize"), Type.WARNING_MESSAGE);
                } else if (validateExtension(fileName)) {
                    final ByteArrayOutputStream bas = new ByteArrayOutputStream();
                    final StreamVariable streamVariable = new StreamVariable() {

                        @Override
                        public OutputStream getOutputStream() {
                            return bas;
                        }

                        @Override
                        public boolean listenProgress() {
                            return false;
                        }

                        @Override
                        public void onProgress(
                            final StreamingProgressEvent event) {
                        }

                        @Override
                        public void streamingStarted(
                            final StreamingStartEvent event) {
                        }

                        @Override
                        public void streamingFinished(
                            final StreamingEndEvent event) {
                            progress.setVisible(false);
                            uploadCallback.uploading(fileName, bas);
                        }

                        @Override
                        public void streamingFailed(
                            final StreamingErrorEvent event) {
                            progress.setVisible(false);
                        }

                        @Override
                        public boolean isInterrupted() {
                            return false;
                        }
                    };
                    html5File.setStreamVariable(streamVariable);
                    progress.setVisible(true);
                }
            });
        });
    }

    /**
     * Производит валидацию расширения файла, проверяя наличие расширения в списке разрешённых
     * к загрузке расширений.
     *
     * @param fileName имя проверяемого файла.
     * @return валидно ли расширение файла.
     */
    private boolean validateExtension(String fileName) {
        // Извлечение расширения файла и перевод в верхний регистр
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1).toUpperCase();

        if (correctExtensions.contains(extension)) {
            return true;
        } else {
            Notification.show(String.format(i18n.get("component.upload.rejectedByExtension"), fileName, String.join(", ", correctExtensions)),
                Type.TRAY_NOTIFICATION);

            return false;
        }
    }

    /**
     * Ресивер загрузчика файлов. Создаёт {@link OutputStream} в который будет произведена запись загружаемого файла.
     */
    private class UploadReceiver implements Receiver {

        private ByteArrayOutputStream bas = new ByteArrayOutputStream();

        public OutputStream receiveUpload(String filename, String mimeType) {
            bas = new ByteArrayOutputStream();

            return bas;
        }

        public ByteArrayOutputStream getByteArrayOutputStream() {
            return bas;
        }
    }
}

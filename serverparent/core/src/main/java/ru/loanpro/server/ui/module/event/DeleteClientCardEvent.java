package ru.loanpro.server.ui.module.event;

import ru.loanpro.server.ui.module.component.ClientCard;

/**
 * Событие удаления карточки клиента (отвязки клиента от займа, заявки)
 * Передаем объект карточки.
 * Объект карточки позволяет идентифицировать конкретное окно в котором удаляется карточка
 */
public class DeleteClientCardEvent {

    ClientCard clientCard;

    public DeleteClientCardEvent(ClientCard clientCard) {
        this.clientCard = clientCard;
    }

    public ClientCard getClientCard() {
        return clientCard;
    }
}

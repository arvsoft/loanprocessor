package ru.loanpro.server.logging.domain;

public enum EntityActions {
    NEW("log.entity.new"),
    OPEN("log.entity.open"),
    CLOSE("log.entity.close"),
    EDIT("log.entity.edit"),
    SAVE("log.entity.save"),
    DELETE("log.entity.delete"),
    PRINT("log.entity.print");

    private String name;

    EntityActions(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

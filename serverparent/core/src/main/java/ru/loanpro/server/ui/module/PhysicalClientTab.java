package ru.loanpro.server.ui.module;

import com.google.common.eventbus.Subscribe;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.contextmenu.MenuItem;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import fr.opensagres.xdocreport.core.XDocReportException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.domain.PhysicalClientDetails;
import ru.loanpro.base.client.repository.PhysicalClientRepository;
import ru.loanpro.base.printer.PrintDocumentType;
import ru.loanpro.base.storage.domain.connector.ClientPrintedFile;
import ru.loanpro.base.storage.service.PrintTemplateFileService;
import ru.loanpro.base.storage.service.PrintedFileService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.annotation.Visible;
import ru.loanpro.global.directory.ClientRating;
import ru.loanpro.global.directory.Gender;
import ru.loanpro.global.exceptions.EntityDeleteException;
import ru.loanpro.sequence.event.UpdateSequenceEvent;
import ru.loanpro.server.ui.module.additional.ConfirmDialog;
import ru.loanpro.server.ui.module.client.details.AccountDetailsLayout;
import ru.loanpro.server.ui.module.client.details.AddressDetailsLayout;
import ru.loanpro.server.ui.module.client.details.DocumentDetailsLayout;
import ru.loanpro.server.ui.module.client.details.EducationDetailsLayout;
import ru.loanpro.server.ui.module.client.details.EstateDetailsLayout;
import ru.loanpro.server.ui.module.client.details.JobDetailsLayout;
import ru.loanpro.server.ui.module.client.details.MainDetailsLayout;
import ru.loanpro.server.ui.module.client.details.NoteDetailsLayout;
import ru.loanpro.server.ui.module.client.details.RelativeDetailsLayout;
import ru.loanpro.server.ui.module.client.details.VehicleDetailsLayout;
import ru.loanpro.server.ui.module.component.uploader.AvatarUploadWindow;
import ru.loanpro.server.ui.module.component.uploader.AvatarUploadWindow.UploadImageType;
import ru.loanpro.server.ui.module.event.UpdateClientEvent;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

/**
 * Вкладка работы с клиентом
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(PhysicalClient.class)
public class PhysicalClientTab extends ClientTab<PhysicalClient, PhysicalClientRepository> {

    @Autowired
    private AvatarUploadWindow avatarUploadWindow;

    @Autowired
    private PrintTemplateFileService printTemplateFileService;

    @Autowired
    private PrintedFileService printedFileService;

    private Button save;
    private Button print;

    @Visible(Roles.Client.DELETE)
    private Button delete;

    private AccountDetailsLayout<PhysicalAccount> accountDetailsLayout;
    private PhysicalClientDetails details;

    public PhysicalClientTab(PhysicalClient client) {
        entity = (client == null) ? new PhysicalClient() : client;
    }

    @PostConstruct
    public void init() {
        super.init();
        applicationEventBus.register(this);

        details = entity.getDetails();

        MainDetailsLayout mainDetailsLayout = new MainDetailsLayout(
            securityService.getUser(), chronometerService, i18n, details, maritalStatusService, configurationService,
            storageService);
        mainDetailsLayout.addContentChangeListener(this);

        DocumentDetailsLayout documentDetailsLayout = new DocumentDetailsLayout(
            securityService.getUser(), chronometerService, details.getDocuments(), i18n, documentTypeService,
            configurationService, storageService);
        documentDetailsLayout.addContentChangeListener(this);

        AddressDetailsLayout addressDetailsLayout = new AddressDetailsLayout(
            securityService.getUser(), chronometerService, details.getAddresses(), i18n, configurationService,
            storageService);
        addressDetailsLayout.addContentChangeListener(this);

        VerticalLayout layoutDocumentAddress = new VerticalLayout(documentDetailsLayout, addressDetailsLayout);

        EducationDetailsLayout educationDetailsLayout = new EducationDetailsLayout(
            securityService.getUser(), chronometerService, details.getEducations(), i18n, educationLevelService,
            storageService);
        educationDetailsLayout.addContentChangeListener(this);

        JobDetailsLayout jobDetailsLayout = new JobDetailsLayout(
            securityService.getUser(), chronometerService, details.getJobs(), i18n, configurationService,
            storageService);
        jobDetailsLayout.addContentChangeListener(this);

        VerticalLayout layoutEducationJob = new VerticalLayout(educationDetailsLayout, jobDetailsLayout);

        EstateDetailsLayout estateDetailsLayout = new EstateDetailsLayout(
            securityService.getUser(), chronometerService, details.getEstates(), i18n, configurationService,
            storageService);
        estateDetailsLayout.addContentChangeListener(this);

        VehicleDetailsLayout vehicleDetailsLayout = new VehicleDetailsLayout(
            securityService.getUser(), chronometerService, details.getVehicles(), i18n, configurationService,
            storageService);
        vehicleDetailsLayout.addContentChangeListener(this);

        VerticalLayout layoutEstateVehicle = new VerticalLayout(estateDetailsLayout, vehicleDetailsLayout);

        RelativeDetailsLayout relativeDetailsLayout = new RelativeDetailsLayout(
            securityService.getUser(), chronometerService, details.getRelatives(), i18n, relativeTypeService,
            storageService);
        relativeDetailsLayout.addContentChangeListener(this);

        NoteDetailsLayout noteDetailsLayout = new NoteDetailsLayout(details.getNotes(), i18n);
        noteDetailsLayout.addContentChangeListener(this);

        VerticalLayout layoutRelativeNote = new VerticalLayout(relativeDetailsLayout, noteDetailsLayout);

        accountDetailsLayout = new AccountDetailsLayout<>(entity, details.getAccounts(), i18n, configurationService,
            numberService, entity.getRegistrationDate(), securityService);
        accountDetailsLayout.addContentChangeListener(this);

        VerticalLayout layoutAccount = new VerticalLayout(accountDetailsLayout);

        tabsheet.addTab(mainDetailsLayout, i18n.get("client.tabsheet.caption.page1"), VaadinIcons.USER_CARD);
        tabsheet.addTab(layoutDocumentAddress, i18n.get("client.tabsheet.caption.page2"), VaadinIcons.USER_CARD);
        tabsheet.addTab(layoutEducationJob, i18n.get("client.tabsheet.caption.page3"), VaadinIcons.USER_CARD);
        tabsheet.addTab(layoutEstateVehicle, i18n.get("client.tabsheet.caption.page4"), VaadinIcons.USER_CARD);
        tabsheet.addTab(layoutRelativeNote, i18n.get("client.tabsheet.caption.page5"), VaadinIcons.USER_CARD);
        tabsheet.addTab(layoutRelativeNote, i18n.get("client.tabsheet.caption.page5"), VaadinIcons.USER_CARD);
        tabsheet.addTab(layoutAccount, i18n.get("client.tabsheet.caption.page6"), VaadinIcons.USER_CARD);
    }

    @Override
    protected Layout initBaseInformation() {
        Label lbMainInfo = new Label(i18n.get("client.panel.maininfo.title"));

        save = new Button(i18n.get("loan.button.save"));
        save.setEnabled(false);
        save.setIcon(VaadinIcons.ADD_DOCK);
        save.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        print = new Button(i18n.get("loan.button.print"));
        print.setEnabled(entity.getId() != null);
        print.setIcon(VaadinIcons.PRINT);
        print.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        print.addClickListener(event -> {
            ContextMenu contextMenu = new ContextMenu(print, true);

            List<ClientPrintedFile> printedFiles = printedFileService.findClientPrintedFiles(entity);
            if (printedFiles.size() > 0) {
                MenuItem printedItem = contextMenu.addItem(i18n.get("loan.button.printed"), null);
                printedFiles.forEach(printed ->
                    printedItem.addItem(
                        String.format("%s %s", printed.getDisplayName(),
                            chronometerService.zonedDateTimeToString(printed.getDateTime())),
                        e -> getUI().getPage().open(String.format("/print/%s", printed.getFileName()), "_blank")));
            }

            printTemplateFileService.getActiveTemplates(PrintDocumentType.PHYSICAL_CLIENT_PROFILE)
                .forEach(template ->
                    contextMenu.addItem(template.getDisplayName(), e -> {
                        try {
                            String fileName = printService.printTemplate(template, entity);
                            getUI().getPage().open(String.format("/print/%s", fileName), "_blank");
                        } catch (IOException | XDocReportException exception) {
                            showErrorMessage(exception);
                        }
                    }));

            contextMenu.open(event.getClientX(), event.getClientY());
        });

        delete = new Button(i18n.get("loan.button.delete"), VaadinIcons.TRASH);
        delete.setEnabled(service.canBeDeleted(entity));
        delete.addStyleNames(ValoTheme.BUTTON_BORDERLESS_COLORED);
        delete.addClickListener(event -> {
            ConfirmDialog dialog = new ConfirmDialog(
                i18n.get("client.dialogDelete.caption"),
                i18n.get("client.dialogDelete.description"),
                () -> {
                    try
                    {
                        service.delete(entity);
                        Notification.show(i18n.get("client.notification.deleteSuccessful"), Type.TRAY_NOTIFICATION);
                        sessionEventBus.post(new RemoveTabEvent<>(this));
                    }
                    catch (EntityDeleteException e)
                    {
                        Notification.show(i18n.get("client.notification.deletedError"), i18n.get(e.getMessage()),
                            Type.ERROR_MESSAGE);
                    }
                });
            dialog.setConfirmButtonIcon(VaadinIcons.TRASH);
            dialog.setClosable(true);
            dialog.show();
        });

        HorizontalLayout hlActions = new HorizontalLayout(lbMainInfo, save, print, delete);
        hlActions.setSizeFull();
        hlActions.setExpandRatio(lbMainInfo, 1);

        Image photo = new Image("", storageService.getScannedFileOrDefault(entity.getPhotoFileName()));
        photo.setWidth(entity.getPhotoFileName() == null ? 64 : 256, Unit.PIXELS);
        photo.addStyleName("cursor-pointer");
        photo.addClickListener(event -> {
            avatarUploadWindow.setAvatarUploadCallbackHandler(fileName -> {
                entity.setPhotoFileName(fileName);
                photo.setSource(storageService.getScannedFileOrDefault(entity.getPhotoFileName()));
                photo.setWidth(256, Unit.PIXELS);
                contentChanged();
            });
            avatarUploadWindow.setUploadImageType(UploadImageType.CLIENT_PHOTO);
            UI.getCurrent().addWindow(avatarUploadWindow);
        });

        ComboBox<ClientRating> rating = new ComboBox<>(i18n.get("client.field.rating"));
        rating.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        rating.setEmptySelectionAllowed(false);
        rating.setItems(ClientRating.values());
        rating.setSelectedItem(entity.getRating());

        TextField lastName = new TextField(i18n.get("client.field.lastname"));
        lastName.focus();
        TextField firstName = new TextField(i18n.get("client.field.firstname"));
        TextField middleName = new TextField(i18n.get("client.field.middlename"));

        HorizontalLayout nameLayout = new HorizontalLayout();
        nameLayout.addComponents(lastName, firstName, middleName);

        DateField birthDate = new DateField(i18n.get("client.field.birthdate"));
        birthDate.setDateFormat(configurationService.getDateFormat());

        ComboBox<Gender> gender = new ComboBox<>(i18n.get("client.field.gender"));
        gender.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        gender.setEmptySelectionAllowed(false);
        gender.setItems(Gender.values());
        gender.setSelectedItem(entity.getGender());
        HorizontalLayout birthDateLayout = new HorizontalLayout();
        birthDateLayout.addComponents(birthDate, gender);

        VerticalLayout vlMainInfo = new VerticalLayout(hlActions, photo, rating, nameLayout, birthDateLayout);
        vlMainInfo.setComponentAlignment(hlActions, Alignment.TOP_RIGHT);
        vlMainInfo.setSizeFull();

        binder.bind(rating, PhysicalClient::getRating, PhysicalClient::setRating);
        binder.forField(lastName)
            .asRequired()
            .bind(PhysicalClient::getLastName, PhysicalClient::setLastName);
        binder.bind(firstName, PhysicalClient::getFirstName, PhysicalClient::setFirstName);
        binder.bind(middleName, PhysicalClient::getMiddleName, PhysicalClient::setMiddleName);
        binder.forField(birthDate)
            .bind(PhysicalClient::getBirthDate, PhysicalClient::setBirthDate);

        binder.bind(gender, PhysicalClient::getGender, PhysicalClient::setGender);
        binder.bind(gender, PhysicalClient::getGender, PhysicalClient::setGender);

        binder.addValueChangeListener(event -> {
            save.setEnabled(binder.isValid() && entity.hashCode() != entityHash);
            print.setEnabled(!save.isEnabled());
        });

        save.addClickListener(e -> {
            service.save(entity, securityService.getDepartment());

            sessionEventBus.post(new UpdateClientEvent<>(entity));
            entityHash = entity.hashCode();

            save.setEnabled(false);
            print.setEnabled(true);
            delete.setEnabled(service.canBeDeleted(entity));
            Notification.show(i18n.get("client.message.clientsaved"), Type.TRAY_NOTIFICATION);
        });

        lastName.addBlurListener(event -> {
            String value = lastName.getValue().trim();
            if (value.length() > 0) lastName.setValue(value.substring(0, 1).toUpperCase() + value.substring(1));
        });
        firstName.addBlurListener(event -> {
            String value = firstName.getValue().trim();
            if (value.length() > 0) firstName.setValue(value.substring(0, 1).toUpperCase() + value.substring(1));
        });
        middleName.addBlurListener(event -> {
            String value = middleName.getValue().trim();
            if (value.length() > 0) middleName.setValue(value.substring(0, 1).toUpperCase() + value.substring(1));
        });

        return vlMainInfo;
    }

    @Override
    public Object getObject() {
        return entity;
    }

    @Override
    public String getTabName() {
        if (entity.getId() == null) {
            return i18n.get("client.tab.add.caption");
        } else {
            return i18n.get("client.tab.edit.caption") + entity.getLastName();
        }
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIGHT_BLUE;
    }

    /**
     * Когда произошло событие изменения контента в элементах клиента, не привязанных к
     * биндеру, изменяем состояние кнопки сохранить.
     */
    @Override
    public void contentChanged() {
        save.setEnabled(binder.isValid() && entity.hashCode() != entityHash);
        print.setEnabled(!save.isEnabled());
        delete.setEnabled(service.canBeDeleted(entity));
    }

    /**
     * Обработчик события сохранения первого счета у клиента
     * Должен обновить грид счетов
     *
     * @param event событие обновления последовательности.
     */
    @Subscribe
    private void updateSequenceEventHandler(UpdateSequenceEvent event) {
        accountDetailsLayout.refreshGrid();
    }

    @Override
    public void destroy() {
        super.destroy();
        applicationEventBus.unregister(this);

        details.getAccounts().forEach(item -> {
            if (item.getClientDetails() == null) {
                numberService.clearAccountSequenceDraftNumber(item.getNumber());
            }
        });
        accountDetailsLayout.clearAccountSequenceDraftNumber();
    }
}

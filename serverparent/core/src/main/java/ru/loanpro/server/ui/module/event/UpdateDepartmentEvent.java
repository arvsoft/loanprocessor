package ru.loanpro.server.ui.module.event;

import ru.loanpro.security.domain.Department;

/**
 * Событие обновления отдела. Содержит обновлённый отдел.
 *
 * @author Oleg Brizhevatikh
 */
public class UpdateDepartmentEvent {

    private Department department;

    /**
     * Конструктор события.
     *
     * @param department Обновлённый отдел.
     */
    public UpdateDepartmentEvent(Department department) {
        this.department = department;
    }

    /**
     * Возвращает обновлённый отдел.
     *
     * @return Обновлённый отдел.
     */
    public Department getDepartment() {
        return department;
    }
}

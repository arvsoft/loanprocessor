package ru.loanpro.server.ui.module;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.vaadin.data.HasValue;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.loan.domain.LoanProduct;
import ru.loanpro.base.loan.domain.QLoanProduct;
import ru.loanpro.base.loan.repository.LoanProductRepository;
import ru.loanpro.base.loan.service.LoanProductService;
import ru.loanpro.directory.domain.LoanType;
import ru.loanpro.directory.service.LoanTypeService;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.global.directory.GridStyle;
import ru.loanpro.global.domain.BaseQueryDSL;
import ru.loanpro.server.ui.module.additional.MoneyField;

/**
 * Вкладка для работы с кредитными продуктами
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.loanProducts")
public class LoanProductsTab extends BaseLoansTab<LoanProduct> {

    private final LoanTypeService loanTypeService;
    private TextField name;
    private TextField description;
    private ComboBox<LoanType> loanType;
    private MoneyField loanBalance;

    @Autowired
    public LoanProductsTab(LoanProductService loanProductService, LoanTypeService loanTypeService) {
        super(loanProductService);
        this.loanTypeService = loanTypeService;
    }

    @Override
    protected void gridColumnsBuild() {
        float k = 1.0f;
        if (configurationService.getGridStyle() == GridStyle.SMALL) {
            grid.addStyleName("v-grid-style-small");
            grid.setRowHeight(40f);
        } else k = 1.3f;

        Grid.Column<LoanProduct, String> colName = grid.addColumn(LoanProduct::getName);
        colName
            .setSortProperty("name")
            .setCaption(i18n.get("loanproducts.grid.title.name"))
            .setWidth(200 * k);

        Grid.Column<LoanProduct, String> colDescription = grid.addColumn(LoanProduct::getDescription);
        colDescription
            .setSortProperty("description")
            .setCaption(i18n.get("loanproducts.grid.title.description"))
            .setWidth(200 * k);

        Grid.Column<LoanProduct, String> colLoanType = grid.addColumn(item ->
            item.getLoanType() == null ? "" : item.getLoanType().getName());
        colLoanType
            .setSortProperty("loanType")
            .setCaption(i18n.get("loanproducts.grid.title.loantype"))
            .setWidth(100 * k);

        Grid.Column<LoanProduct, String> colLoanBalance = grid.addColumn(item ->
            moneyFormat.format(item.getLoanBalance()));
        colLoanBalance
            .setSortProperty("loanBalance")
            .setCaption(i18n.get("loanproducts.grid.title.loanbalance"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(120 * k);

        grid.addColumn(item -> item.getCurrency().getCurrencyCode())
            .setCaption(i18n.get("loanproducts.grid.title.currency"))
            .setWidth(70 * k);

        grid.addColumn(item -> percentFormat.format(item.getInterestValue()))
            .setCaption(i18n.get("loanproducts.grid.title.interestvalue"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(100 * k);

        grid.addColumn(item -> i18n.get(item.getInterestType().getName()))
            .setCaption(i18n.get("loanproducts.grid.title.interesttype"))
            .setWidth(100 * k);

        grid.addColumn(item -> i18n.get(item.getCalcScheme().getName()))
            .setCaption(i18n.get("loanproducts.grid.title.calcscheme"))
            .setWidth(100 * k);

        grid.addColumn(item -> i18n.get(item.getReturnTerm().getName()))
            .setCaption(i18n.get("loanproducts.grid.title.returnterm"))
            .setWidth(100 * k);

        grid.addColumn(LoanProduct::getLoanTerm)
            .setCaption(i18n.get("loanproducts.grid.title.loanterm"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(70 * k);

        grid.addColumn(item -> i18n.get(item.getLoanTermType().getName()))
            .setCaption(i18n.get("loanproducts.grid.title.loantermtype"))
            .setWidth(100 * k);

        grid.addColumn(item -> percentFormat.format(item.getPenaltyValue()))
            .setCaption(i18n.get("loanproducts.grid.title.penalty"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(100 * k);

        grid.addColumn(item -> i18n.get(item.getPenaltyType().getName()))
            .setCaption(i18n.get("loanproducts.grid.title.penaltytype"))
            .setWidth(100 * k);

        grid
            .addComponentColumn(item -> {
                CheckBox checkBox = new CheckBox();
                checkBox.addStyleName(ValoTheme.CHECKBOX_SMALL);
                checkBox.setValue(item.isChargeFine());
                checkBox.setReadOnly(true);
                return checkBox;
            })
            .setCaption(i18n.get("loanproducts.grid.title.chargefine"))
            .setStyleGenerator(item -> "v-align-center")
            .setWidth(70 * k);

        grid.addColumn(item -> moneyFormat.format(item.getFineValue()))
            .setCaption(i18n.get("loanproducts.grid.title.finevalue"))
            .setStyleGenerator(item -> "v-align-right")
            .setWidth(70 * k);

        grid
            .addComponentColumn(item -> {
                CheckBox checkBox = new CheckBox();
                checkBox.addStyleName(ValoTheme.CHECKBOX_SMALL);
                checkBox.setValue(item.isFineOneTime());
                checkBox.setReadOnly(true);
                return checkBox;
            })
            .setCaption(i18n.get("loanproducts.grid.title.fineonetime"))
            .setStyleGenerator(item -> "v-align-center")
            .setWidth(70 * k);

        grid.sort(colName);

        HeaderRow headerRow = grid.addHeaderRowAt(1);

        name = new TextField();
        headerRow.getCell(colName).setComponent(name);

        description = new TextField();
        headerRow.getCell(colDescription).setComponent(description);

        loanType = new ComboBox<>();
        loanType.setItemCaptionGenerator(LoanType::getName);
        loanType.setItems(loanTypeService.getAll());
        loanType.setWidth(String.valueOf(colLoanType.getWidth()*0.9f));
        headerRow.getCell(colLoanType).setComponent(loanType);

        loanBalance = new MoneyField(configurationService.getLocale());
        loanBalance.setWidth(String.valueOf(colLoanBalance.getWidth()*0.9f));
        headerRow.getCell(colLoanBalance).setComponent(loanBalance);

        grid.addColumnResizeListener(event -> {
            if (event.getColumn() == colName) {
                name.setWidth(String.valueOf(colName.getWidth()*0.9f));
            } else if (event.getColumn() == colDescription) {
                description.setWidth(String.valueOf(colDescription.getWidth()*0.9f));
            } else if (event.getColumn() == colLoanBalance) {
                loanBalance.setWidth(String.valueOf(colLoanBalance.getWidth()*0.9f));
            } else if (event.getColumn() == colLoanType) {
                loanType.setWidth(String.valueOf(colLoanType.getWidth()*0.9f));
            }
        });

        headerRow.getComponents().stream()
            .filter(item -> item instanceof HasValue)
            .map(item -> (HasValue) item)
            .forEach(item -> item.addValueChangeListener(event -> updateGridContent()));
    }

    @Override
    protected Predicate getPredicate() {
        QLoanProduct loanProduct = QLoanProduct.loanProduct;

        return new BooleanBuilder()
            .and(BaseQueryDSL.contains(loanProduct.name, name))
            .and(BaseQueryDSL.contains(loanProduct.description, description))
            .and(BaseQueryDSL.eq(loanProduct.loanType, loanType))
            .and(BaseQueryDSL.eq(loanProduct.loanBalance, loanBalance, configurationService.getLocale()))
            .getValue();
    }

    @Override
    protected String getCreateButtonRole() {
        return Roles.LoanProduct.CREATE;
    }

    @Override
    protected Class<? extends BaseLoanTab<LoanProduct, LoanProductRepository>> getEntityTabClass() {
        return LoanProductTab.class;
    }

    @Override
    public String getTabName() {
        return i18n.get("core.menu.loanproducts");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.GREEN;
    }
}

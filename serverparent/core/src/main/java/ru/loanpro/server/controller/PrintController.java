package ru.loanpro.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.loanpro.base.storage.IStorageService;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Контроллер для передачи клиенту .pdf-файла с сервера.
 *
 * @author Oleg Brizhevatikh
 */
@RestController
@RequestMapping("/print")
public class PrintController {

    @Autowired
    private IStorageService storageService;

    /**
     * Возвращает .pdf-файл с сервера по имени. Ограничивает доступ к файлу по
     * стандартной авторизации в системе и роли пользователя.
     *
     * @param fileName имя файла.
     * @return .pdf-файл.
     * @throws IOException исключение, если при чтении файла произошла ошибка
     *                     ввода-вывода.
     */
    @RequestMapping(path = "/{fileName}", method = RequestMethod.GET, produces = "application/pdf")
//    @Secured(Roles.Loan.PRINT)
    public ResponseEntity<InputStreamResource> getPDF(@PathVariable("fileName") String fileName) throws FileNotFoundException
    {
        InputStream stream = storageService.getPrintedFileStream(fileName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "filename=" + fileName);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        headers.setContentLength(storageService.getPrintedFileLength(fileName));
        return new ResponseEntity<>(new InputStreamResource(stream), headers, HttpStatus.OK);
    }
}

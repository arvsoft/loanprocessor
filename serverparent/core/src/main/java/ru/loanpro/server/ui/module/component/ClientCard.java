package ru.loanpro.server.ui.module.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.SingleSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.domain.JuridicalClient;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.base.client.service.AbstractClientService;
import ru.loanpro.eventbus.SessionEventBus;
import ru.loanpro.server.ui.module.JuridicalClientTab;
import ru.loanpro.server.ui.module.PhysicalClientTab;
import ru.loanpro.uibasis.event.AddTabEvent;

import java.time.format.DateTimeFormatter;

/**
 * Карточка выбора и отображения клиента. Содержит краткие сведения о клиенте,
 * его роль в заявке или займе.
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
public class ClientCard extends VerticalLayout implements SingleSelect<Client> {

    private final SessionEventBus sessionEventBus;
    private Client client;
    private String clientType;
    private AbstractClientService clientService;
    private I18N i18N;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private ValueChangeListener<Client> valueChangeListener;
    private ClickListener removeListener;

    ComboBox<Client> cbClient = new ComboBox<>();
    private Button btDelete = new Button(VaadinIcons.TRASH);

    private boolean isRequire;
    private boolean readOnly;

    public ClientCard(String clientType, AbstractClientService clientService, I18N i18N, SessionEventBus sessionEventBus) {
        this.clientType = clientType;
        this.clientService = clientService;
        this.i18N = i18N;
        this.sessionEventBus = sessionEventBus;

        setWidth(400, Unit.PIXELS);
        addStyleNames(MaterialTheme.CARD_2, MaterialTheme.CARD_NO_PADDING, MaterialTheme.CARD_HOVERABLE);
        addStyleName("layout-margin-bottom");
        setMargin(true);
        setSpacing(false);
    }

    private void setNotSelectedState() {
        Label type = new Label(clientType);
        cbClient.setSizeFull();
        cbClient.setItemCaptionGenerator(Client::getDisplayName);
        cbClient.setEmptySelectionAllowed(false);
        cbClient.setRequiredIndicatorVisible(isRequire);

        cbClient.setDataProvider(
            clientService::findAll,
            clientService::count);

        if (client != null)
            cbClient.setSelectedItem(client);

        Button btApply = new Button(VaadinIcons.CHECK);
        btApply.setEnabled(false);
        btApply.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        btApply.addStyleName(ValoTheme.BUTTON_SMALL);

        HorizontalLayout layoutActions = new HorizontalLayout(btApply);

        VerticalLayout layout = new VerticalLayout(type, cbClient, layoutActions);
        layout.setComponentAlignment(layoutActions, Alignment.MIDDLE_RIGHT);

        cbClient.addValueChangeListener(event -> btApply.setEnabled(event.getValue() != null));

        btApply.addClickListener(event -> {
            Client oldValue = client;
            client = cbClient.getValue();
            valueChangeListener.valueChange(new ValueChangeEvent<>(this, this, oldValue, false));
            setSelectedState();
        });

        removeAllComponents();
        addComponent(layout);
    }

    private void setSelectedState() {
        Label type = new Label(clientType);
        Label firstLabel = new Label(client.getDisplayName());
        Label secondLabel = new Label(
            client instanceof PhysicalClient
                ? ((PhysicalClient) client).getBirthDate().format(formatter)
                : ((JuridicalClient) client).getInn() + ", " +
                ((JuridicalClient) client).getRegisterDate().format(formatter));

        VerticalLayout leftLayout = new VerticalLayout(type, firstLabel, secondLabel);

        Button btView = new Button(VaadinIcons.EYE);
        btView.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        btView.addStyleName(ValoTheme.BUTTON_SMALL);
        btView.addClickListener(event -> {
            if (client instanceof PhysicalClient)
                sessionEventBus.post(new AddTabEvent(PhysicalClientTab.class, client));
            else
                sessionEventBus.post(new AddTabEvent(JuridicalClientTab.class, client));
        });

        btDelete.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        btDelete.addStyleName(ValoTheme.BUTTON_SMALL);
        btDelete.addClickListener(event -> {
            cbClient.setSelectedItem(null);

            //Убираем клиента и переводим карточку в состояние выбора клиента
            Client oldClient = client;
            client = null;
            setNotSelectedState();

            // Оправляем событие изменения клиента
            valueChangeListener.valueChange(new ValueChangeEvent<>(this, this, oldClient, false));

            // Если определён слушатель кнопки удаления - отправляем в него событие
            if (removeListener != null)
                removeListener.buttonClick(new ClickEvent(this));
        });

        VerticalLayout rightLayout = new VerticalLayout(btView, btDelete);
        rightLayout.setWidth(btDelete.getWidth(), Unit.PIXELS);
        rightLayout.setMargin(false);

        HorizontalLayout layout = new HorizontalLayout(leftLayout, rightLayout);
        layout.setSizeFull();
        layout.setExpandRatio(leftLayout, 6);
        layout.setExpandRatio(rightLayout, 1);
        layout.setSpacing(false);
        layout.setMargin(false);

        removeAllComponents();
        addComponent(layout);
    }

    @Override
    public void setValue(Client value) {
        this.client = value;

        if (value == null) {
            setNotSelectedState();
        } else {
            setSelectedState();
        }
    }

    @Override
    public void setRequiredIndicatorVisible(boolean requiredIndicatorVisible) {
        isRequire = requiredIndicatorVisible;
    }

    @Override
    public boolean isRequiredIndicatorVisible() {
        return isRequire;
    }

    @Override
    public Client getValue() {
        return client;
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<Client> valueChangeListener) {
        this.valueChangeListener = valueChangeListener;

        return (Registration) () -> {
        };
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        btDelete.setEnabled(!readOnly);
        cbClient.setReadOnly(readOnly);
    }

    @Override
    public boolean isReadOnly() {
        return readOnly;
    }

    public void addRemoveClickChangeListener(ClickListener removeListener) {
        this.removeListener = removeListener;
    }
}

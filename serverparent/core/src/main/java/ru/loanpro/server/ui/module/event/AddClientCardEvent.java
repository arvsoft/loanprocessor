package ru.loanpro.server.ui.module.event;

import ru.loanpro.server.ui.module.component.ClientCard;

/**
 * Событие добавления новой карточки (привязки нового клиента к займу, заявке)
 * Передаем объект карточки.
 * Объект карточки позволяет идентифицировать конкретное окно в котором создается карточка
 */
public class AddClientCardEvent {

    ClientCard clientCard;

    public AddClientCardEvent(ClientCard clientCard) {
        this.clientCard = clientCard;
    }

    public ClientCard getClientCard() {
        return clientCard;
    }

}

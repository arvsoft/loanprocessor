package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Vehicle;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.global.directory.VehicleCategory;
import ru.loanpro.global.directory.VehicleType;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования транспортных средств
 *
 * @author Maksim Askhaev
 */
public class VehicleDetailsLayout extends HorizontalLayout {
    private final Set<Vehicle> vehicles;
    private final I18N i18n;
    private final ConfigurationService configurationService;
    private final StorageService storageService;
    private final User user;
    private final ChronometerService chronometerService;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Vehicle> grid;
    private ContentChangeListener listener;

    public VehicleDetailsLayout(User user, ChronometerService chronometerService, Set<Vehicle> vehicles, I18N i18n,
                                ConfigurationService configurationService, StorageService storageService) {

        this.user = user;
        this.chronometerService = chronometerService;
        this.i18n = i18n;
        this.vehicles = vehicles;
        this.configurationService = configurationService;
        this.storageService = storageService;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(vehicles);
        grid.addColumn(item -> item.getVehicleType() == null ? "" : i18n.get(item.getVehicleType().getName()))
            .setCaption(i18n.get("client.vehicledetails.field.type"));
        grid.addColumn(Vehicle::getModel).setCaption(i18n.get("client.vehicledetails.field.model"));
        grid.addColumn(Vehicle::getManufactureYear).setCaption(i18n.get("client.vehicledetails.field.manufactureyear"));

        grid.addItemClickListener(event -> {
            closeForm();

            Vehicle item = event.getItem();
            if (item != null) {
                form = getVehicleForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.vehicledetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Vehicle vehicle = new Vehicle();
            vehicle.setEstimatedValue(new BigDecimal(0));
            form = getVehicleForm(vehicle);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getVehicleForm(Vehicle vehicle) {

        ComboBox<VehicleType> vehicleType = new ComboBox<>(i18n.get("client.vehicledetails.field.type"));
        vehicleType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        vehicleType.setEmptySelectionAllowed(false);
        vehicleType.setItems(VehicleType.values());
        vehicleType.setSelectedItem(vehicle.getVehicleType());
        ComboBox<VehicleCategory> vehicleCategory = new ComboBox<>(i18n.get("client.vehicledetails.field.category"));
        vehicleCategory.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        vehicleCategory.setEmptySelectionAllowed(false);
        vehicleCategory.setItems(VehicleCategory.values());
        vehicleCategory.setSelectedItem(vehicle.getVehicleCategory());

        TextField model = new TextField(i18n.get("client.vehicledetails.field.model"));
        TextField manufactureYear = new TextField(i18n.get("client.vehicledetails.field.manufactureyear"));
        manufactureYear.addStyleName("align-right");
        TextField color = new TextField(i18n.get("client.vehicledetails.field.color"));
        TextField registrationNumber = new TextField(i18n.get("client.vehicledetails.field.registrationnumber"));
        TextField vinNumber = new TextField(i18n.get("client.vehicledetails.field.vinnumber"));
        TextField chassisNumber = new TextField(i18n.get("client.vehicledetails.field.chassisnumber"));
        TextField bodyNumber = new TextField(i18n.get("client.vehicledetails.field.bodynumber"));
        TextField certificateNumber = new TextField(i18n.get("client.vehicledetails.field.certificatenumber"));
        DateField certificateDate = new DateField(i18n.get("client.vehicledetails.field.certificatedate"));
        TextField certificateOrganization = new TextField(i18n.get("client.vehicledetails.field.certificateorganization"));
        MoneyField estimatedValue = new MoneyField(i18n.get("client.vehicledetails.field.estimatedvalue"),
            configurationService.getLocale());
        CheckBox pledge = new CheckBox(i18n.get("client.vehicledetails.field.pledge"));
        pledge.addStyleName(ValoTheme.CHECKBOX_SMALL);
        TextField pledgeHostage = new TextField(i18n.get("client.vehicledetails.field.pledgehostage"));
        TextField pledgeDocument = new TextField(i18n.get("client.vehicledetails.field.pledgedocument"));
        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        Button save = new Button(i18n.get("client.vehicledetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.vehicledetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(vehicleType, vehicleCategory, model, manufactureYear, color, registrationNumber,
            vinNumber, chassisNumber, bodyNumber, certificateNumber, certificateDate, certificateOrganization, estimatedValue,
            pledge, pledgeHostage, pledgeDocument, scansUpload, layout);
        formLayout.setSizeFull();

        Binder<Vehicle> binder = new Binder<>();

        binder.forField(vehicleType)
            .bind(Vehicle::getVehicleType, Vehicle::setVehicleType);
        binder.forField(vehicleCategory)
            .bind(Vehicle::getVehicleCategory, Vehicle::setVehicleCategory);
        binder.forField(model)
            .bind(Vehicle::getModel, Vehicle::setModel);
        binder.forField(manufactureYear)
            .withConverter(new StringToIntegerConverter("Must be a number"))
            .withValidator(new IntegerRangeValidator("Must be a number from 0 to 3000", 0, 3000))
            .bind(Vehicle::getManufactureYear, Vehicle::setManufactureYear);
        binder.forField(color)
            .bind(Vehicle::getColor, Vehicle::setColor);
        binder.forField(registrationNumber)
            .bind(Vehicle::getRegistrationNumber, Vehicle::setRegistrationNumber);
        binder.forField(vinNumber)
            .bind(Vehicle::getVinNumber, Vehicle::setVinNumber);
        binder.forField(chassisNumber)
            .bind(Vehicle::getChassisNumber, Vehicle::setChassisNumber);
        binder.forField(bodyNumber)
            .bind(Vehicle::getBodyNumber, Vehicle::setBodyNumber);
        binder.forField(certificateNumber)
            .bind(Vehicle::getCertificateNumber, Vehicle::setCertificateNumber);
        binder.forField(certificateDate)
            .bind(Vehicle::getCertificateDate, Vehicle::setCertificateDate);
        binder.forField(certificateOrganization)
            .bind(Vehicle::getCertificateOrganization, Vehicle::setCertificateOrganization);
        binder.forField(estimatedValue)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(Vehicle::getEstimatedValue, Vehicle::setEstimatedValue);
        binder.forField(pledge)
            .bind(Vehicle::isPledge, Vehicle::setPledge);
        binder.forField(pledgeHostage)
            .bind(Vehicle::getPledgeHostage, Vehicle::setPledgeHostage);
        binder.forField(pledgeDocument)
            .bind(Vehicle::getPledgeDocument, Vehicle::setPledgeDocument);
        binder.forField(scansUpload)
            .bind(Vehicle::getScans, Vehicle::setScans);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(vehicle);

        save.addClickListener(event -> {
            if (vehicles.stream().noneMatch(item -> item.hashCode() == vehicle.hashCode()))
                vehicles.add(vehicle);

            grid.setItems(vehicles);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}


package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Job;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования места работы физического лица
 *
 * @author Maksim Askhaev
 */
public class JobDetailsLayout extends HorizontalLayout {
    private final Set<Job> jobs;
    private final I18N i18n;
    private final StorageService storageService;
    private final User user;
    private final ChronometerService chronometerService;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Job> grid;
    private ContentChangeListener listener;
    private final ConfigurationService configurationService;

    public JobDetailsLayout(User user, ChronometerService chronometerService, Set<Job> jobs, I18N i18n,
                            ConfigurationService configurationService, StorageService storageService) {

        this.configurationService = configurationService;
        this.storageService = storageService;

        DateTimeFormatter dateFormatter =
            DateTimeFormatter.ofPattern(configurationService.getDateFormat(), configurationService.getLocale());

        this.user = user;
        this.chronometerService = chronometerService;
        this.i18n = i18n;
        this.jobs = jobs;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(jobs);
        grid.addColumn(Job::getOccupation).setCaption(i18n.get("client.jobdetails.field.occupation"));
        grid.addColumn(Job::getOrganization).setCaption(i18n.get("client.jobdetails.field.organization"));
        grid.addColumn(Job::getReceptionPhone).setCaption(i18n.get("client.jobdetails.field.receptionphone"));
        grid.addColumn(item -> item.getEmploymentDate() == null ? "" : dateFormatter.format(item.getEmploymentDate()))
            .setCaption(i18n.get("client.jobdetails.field.employmentdate"));

        grid.addItemClickListener(event -> {
            closeForm();

            Job item = event.getItem();
            if (item != null) {
                form = getJobForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.jobdetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Job job = new Job();
            form = getJobForm(job);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getJobForm(Job job) {

        TextField occupation = new TextField(i18n.get("client.jobdetails.field.occupation"));
        TextField organization = new TextField(i18n.get("client.jobdetails.field.organization"));
        TextField receptionPhone = new TextField(i18n.get("client.jobdetails.field.receptionphone"));
        DateField startDate = new DateField(i18n.get("client.jobdetails.field.employmentdate"));
        startDate.setDateFormat(configurationService.getDateFormat());
        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        Button save = new Button(i18n.get("client.jobdetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.jobdetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(occupation, organization, receptionPhone, startDate, scansUpload, layout);
        formLayout.setSizeFull();

        Binder<Job> binder = new Binder<>();

        binder.forField(occupation)
            .bind(Job::getOccupation, Job::setOccupation);
        binder.forField(organization)
            .bind(Job::getOrganization, Job::setOrganization);
        binder.forField(receptionPhone)
            .bind(Job::getReceptionPhone, Job::setReceptionPhone);
        binder.forField(startDate)
            .bind(Job::getEmploymentDate, Job::setEmploymentDate);
        binder.forField(scansUpload)
            .bind(Job::getScans, Job::setScans);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(job);

        save.addClickListener(event -> {
            if (jobs.stream().noneMatch(item -> item.hashCode() == job.hashCode()))
                jobs.add(job);

            grid.setItems(jobs);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}

package ru.loanpro.server.ui.module.event;

import ru.loanpro.base.client.domain.Client;

/**
 * Класс события сохранения клиента (нажатие на кнопку Сохранить)
 *
 * @author Maksim Askhaev
 */
public class UpdateClientEvent<T extends Client> {

    private T client;

    public UpdateClientEvent(T object) {
        this.client = object;
    }

    public T getClient() {
        return client;
    }
}

package ru.loanpro.server.ui.module.component;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.loan.domain.Request;
import ru.loanpro.base.loan.event.UpdateRequestStatusEvent;
import ru.loanpro.base.requesthistory.domain.RequestHistory;
import ru.loanpro.base.requesthistory.serivce.RequestHistoryService;
import ru.loanpro.eventbus.ApplicationEventBus;
import ru.loanpro.global.directory.RequestStatus;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.security.service.SecurityService;

/**
 * Окно формы принятия решения по заявке.
 * Окно вызывается из формы заявки и выполняет функцию принятия решения (изменения статуса заявки)
 *
 * @author Maksim Askhaev
 */
public class RequestDecision extends Window {

    private final RequestHistoryService requestHistoryService;
    private final ChronometerService chronometerService;
    private final SecurityService securityService;
    private final ApplicationEventBus applicationEventBus;
    private TextField basement;
    private Button approve;
    private Button decline;
    private Button froze;
    private Grid<RequestHistory> grid;
    private Request request;

    public RequestDecision(I18N i18n, Request request, RequestHistoryService requestHistoryService,
                           ChronometerService chronometerService, SecurityService securityService,
                           ApplicationEventBus applicationEventBus) {
        super(i18n.get("requestDecision.window.caption"));

        this.request = request;
        this.requestHistoryService = requestHistoryService;
        this.chronometerService = chronometerService;
        this.securityService = securityService;
        this.applicationEventBus = applicationEventBus;

        center();
        setModal(true);
        setResizable(false);

        basement = new TextField(i18n.get("requestDecision.textfield.basement"));
        basement.focus();
        basement.setSizeFull();

        approve = new Button(i18n.get("requestDecision.button.approve"));
        approve.setWidth(180, Unit.PIXELS);
        approve.setIcon(VaadinIcons.THUMBS_UP_O);
        approve.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        approve.setEnabled(request.getRequestStatus() != RequestStatus.APPROVED);

        decline = new Button(i18n.get("requestDecision.button.decline"));
        decline.setWidth(180, Unit.PIXELS);
        decline.setIcon(VaadinIcons.THUMBS_DOWN_O);
        decline.addStyleName(ValoTheme.BUTTON_DANGER);
        decline.setEnabled(request.getRequestStatus() != RequestStatus.DECLINED);

        froze = new Button(i18n.get("requestDecision.button.froze"));
        froze.setWidth(180, Unit.PIXELS);
        froze.setIcon(VaadinIcons.TIMER);
        froze.addStyleName(ValoTheme.BUTTON_QUIET);
        froze.setEnabled(request.getRequestStatus() != RequestStatus.FROZEN);

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setWidth(570, Unit.PIXELS);
        grid.setHeight(300, Unit.PIXELS);

        grid.addColumn(item -> chronometerService.zonedDateToString(item.getDecisionDate()))
            .setCaption(i18n.get("requestDecision.grid.title.date"));
        grid.addColumn(RequestHistory::getBasement).setCaption(i18n.get("requestDecision.grid.title.basement"));
        grid.addColumn(item -> i18n.get(item.getRequestStatus().getName()))
            .setCaption(i18n.get("requestDecision.grid.title.status"));
        grid.addColumn(item -> item.getLoan() == null ? "" : item.getLoan().getLoanNumber())
            .setCaption(i18n.get("requestDecision.grid.title.loannumber"));
        grid.addColumn(item -> item.getUser().getUsername()).setCaption(i18n.get("requestDecision.grid.title.user"));
        grid.setItems(requestHistoryService.findAllByRequest(request));

        HorizontalLayout hlButtons = new HorizontalLayout(approve, decline, froze);

        VerticalLayout layout = new VerticalLayout();
        layout.setSizeUndefined();
        layout.setMargin(true);
        layout.addComponents(basement, hlButtons, grid);
        layout.setComponentAlignment(hlButtons, Alignment.BOTTOM_RIGHT);

        approve.addClickListener(event -> {
            processRequestDecision(RequestStatus.APPROVED);
            Notification.show(i18n.get("requestDecision.message.approved"), Notification.Type.TRAY_NOTIFICATION);
        });

        decline.addClickListener(event -> {
            processRequestDecision(RequestStatus.DECLINED);
            Notification.show(i18n.get("requestDecision.message.declined"), Notification.Type.TRAY_NOTIFICATION);
        });

        froze.addClickListener(event -> {
            processRequestDecision(RequestStatus.FROZEN);
            Notification.show(i18n.get("requestDecision.message.frozen"), Notification.Type.TRAY_NOTIFICATION);
        });

        //стиль ко всей странице
        addStyleName(MaterialTheme.LABEL_COLORED);
        setContent(layout);
    }

    private void processRequestDecision(RequestStatus requestStatus) {
        request.setRequestStatus(requestStatus);
        RequestHistory requestHistory = new RequestHistory();
        requestHistory.setRequestStatus(requestStatus);
        requestHistory.setDecisionDate(chronometerService.getCurrentInstant());
        requestHistory.setBasement(basement.getValue());
        requestHistory.setUser(securityService.getUser());
        requestHistory.setRequest(request);
        requestHistoryService.save(requestHistory);
        grid.setItems(requestHistoryService.findAllByRequest(request));
        approve.setEnabled(false);
        decline.setEnabled(false);
        froze.setEnabled(false);
        applicationEventBus.post(new UpdateRequestStatusEvent(request));
    }

}

package ru.loanpro.server.ui.module;

import com.google.common.eventbus.Subscribe;
import com.querydsl.core.types.Predicate;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.base.loan.domain.BaseLoan;
import ru.loanpro.base.loan.repository.BaseLoanRepository;
import ru.loanpro.base.loan.service.BaseLoanService;
import ru.loanpro.server.ui.module.event.UpdateBaseLoanEvent;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.AddTabEvent;

import javax.annotation.PostConstruct;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

/**
 * Базовая вкладка для отображения списока базовых сущностей займа.
 *
 * @param <T> тип базовой сущности займа.
 *
 * @author Oleg Brizhevatikh.
 */
@NoRepositoryBean
public abstract class BaseLoansTab<T extends BaseLoan> extends BaseTab {

    protected final BaseLoanService<T, ? extends BaseLoanRepository<T>> service;

    protected Grid<T> grid;

    protected DateTimeFormatter dateTimeFormatter;
    protected DecimalFormat moneyFormat;
    protected DecimalFormat percentFormat;

    /**
     * Создаёт вкладку.
     *
     * @param service сервис для работы с базовой сущностью займа.
     */
    public <S extends BaseLoanService<T, ?>> BaseLoansTab(S service) {
        this.service = service;
    }

    /**
     * Создаёт в поле Grid требуемые колонки, зависящие от типа базовой сущности займа.
     */
    protected abstract void gridColumnsBuild();

    protected abstract Predicate getPredicate();

    /**
     * Возвращает роль, требуемую для создания новой сущности типа <code>T</code>.
     *
     * @return роль.
     */
    protected abstract String getCreateButtonRole();

    /**
     * Возвращает класс вкладки, для создания или отображения сущности типа <code>T</code>.
     *
     * @return класс вкладки.
     */
    protected abstract Class<? extends BaseLoanTab<T, ? extends BaseLoanRepository<T>>> getEntityTabClass();

    /**
     * Возвращает коллекцию дополнительных кнопок, зависящих от типа сущности.
     *
     * @return коллекция кнопок.
     */
    protected Collection<Button> initExtendedButtons() {
        return Collections.emptySet();
    }

    @PostConstruct
    public void init() {
        dateTimeFormatter = DateTimeFormatter.ofPattern(configurationService.getDateFormat(), configurationService.getLocale());
        moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);
        percentFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        percentFormat.setMinimumFractionDigits(3);
        percentFormat.setMaximumFractionDigits(3);

        Button create = new Button(i18n.get("baseloans.button.add"));
        create.setIcon(VaadinIcons.PLUS);
        create.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        create.setEnabled(securityService.hasAuthority(getCreateButtonRole()));

        Button view = new Button(i18n.get("baseloans.button.view"));
        view.setIcon(VaadinIcons.EDIT);
        view.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        view.setEnabled(false);

        Button refresh = new Button();
        refresh.setId("refreshButton");
        refresh.setIcon(VaadinIcons.REFRESH);
        refresh.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        refresh.addClickListener(event -> {
           updateGridContent();
        });

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        gridColumnsBuild();
        updateGridContent();

        grid.addSelectionListener(event -> view.setEnabled(event.getFirstSelectedItem().isPresent()));

        create.addClickListener(event ->
            sessionEventBus.post(new AddTabEvent(getEntityTabClass())));

        view.addClickListener(event -> {
            T entity = grid.asSingleSelect().getValue();
            sessionEventBus.post(new AddTabEvent(getEntityTabClass(), entity));
        });

        HorizontalLayout layout = new HorizontalLayout(create, view, refresh);
        initExtendedButtons().forEach(layout::addComponent);

        addComponents(layout, grid);
        setComponentAlignment(layout, Alignment.TOP_RIGHT);
        setExpandRatio(grid, 1);
        setSizeFull();
    }

    protected void updateGridContent() {
        Predicate predicate = getPredicate();

        grid.setDataProvider(
            (sortOrder, offset, limit) -> service.findAll(predicate, sortOrder, offset, limit),
            () -> service.count(predicate));
    }

    @Subscribe
    public void updateBaseLoanEventHandler(UpdateBaseLoanEvent<T> event) {
        grid.getDataProvider().refreshItem(event.getEntity());
    }
}

package ru.loanpro.server.ui.module.additional;

import com.vaadin.ui.TextField;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Форматтер для {@link TextField}, обеспечивающий форматирование значения
 * текстового поля к процентам в переданной локали.
 *
 * @author Oleg Brizhevatikh
 */
public class PercentageFieldFormatter extends CurrencyFieldFormatter {

    /**
     * Конструктор, принимающий локаль процентов, для которой требуется
     * форматирование. Устанавливает количество дробных знаков в 3.
     *
     * @param locale локаль процентов.
     */
    public PercentageFieldFormatter(Locale locale) {
        super(getSeparator1((DecimalFormat) NumberFormat.getCurrencyInstance(locale)),
            getSeparator2((DecimalFormat) NumberFormat.getCurrencyInstance(locale)),
            3);
    }
}

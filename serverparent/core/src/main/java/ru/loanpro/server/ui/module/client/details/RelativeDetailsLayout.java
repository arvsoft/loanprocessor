package ru.loanpro.server.ui.module.client.details;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.client.domain.details.Relative;
import ru.loanpro.base.storage.StorageService;
import ru.loanpro.directory.domain.RelativeType;
import ru.loanpro.directory.service.RelativeTypeService;
import ru.loanpro.security.domain.User;
import ru.loanpro.security.service.ChronometerService;
import ru.loanpro.server.ui.module.component.DirectoryComboBox;
import ru.loanpro.server.ui.module.component.uploader.ScansUploadLayout;

import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования отношений физического лица
 *
 * @author Maksim Askhaev
 */
public class RelativeDetailsLayout extends HorizontalLayout {
    private final Set<Relative> relatives;
    private final I18N i18n;
    private final RelativeTypeService relativeTypeService;
    private final StorageService storageService;
    private final User user;
    private final ChronometerService chronometerService;

    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<Relative> grid;
    private ContentChangeListener listener;

    public RelativeDetailsLayout(User user, ChronometerService chronometerService, Set<Relative> relatives, I18N i18n,
                                 RelativeTypeService relativeTypeService, StorageService storageService) {

        this.user = user;
        this.chronometerService = chronometerService;
        this.i18n = i18n;
        this.relatives = relatives;
        this.relativeTypeService = relativeTypeService;
        this.storageService = storageService;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(relatives);
        grid.addColumn(item -> item.getRelativeType() == null ? "" : item.getRelativeType().getName())
            .setCaption(i18n.get("client.relativedetails.field.relativetype"));
        grid.addColumn(Relative::getName).setCaption(i18n.get("client.relativedetails.field.name"));
        grid.addColumn(Relative::getContacts).setCaption(i18n.get("client.relativedetails.field.contacts"));

        grid.addItemClickListener(event -> {
            closeForm();

            Relative item = event.getItem();
            if (item != null) {
                form = getRelativeForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.relativedetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            Relative relative = new Relative();
            form = getRelativeForm(relative);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getRelativeForm(Relative relative) {

        DirectoryComboBox<RelativeType, RelativeTypeService> relativeType =
            new DirectoryComboBox<>(i18n.get("client.relativedetails.field.relativetype"), relativeTypeService);
        relativeType.setEmptySelectionAllowed(false);

        TextField name = new TextField(i18n.get("client.relativedetails.field.name"));
        TextField contacts = new TextField(i18n.get("client.relativedetails.field.contacts"));
        ScansUploadLayout scansUpload = new ScansUploadLayout(user, storageService, chronometerService, i18n);

        Button save = new Button(i18n.get("client.relativedetails.button.save"));
        save.setEnabled(false);
        Button cancel = new Button(i18n.get("client.relativedetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout = new FormLayout(relativeType, name, contacts, scansUpload, layout);
        formLayout.setSizeFull();

        Binder<Relative> binder = new Binder<>();

        binder.forField(relativeType)
            .bind(Relative::getRelativeType, Relative::setRelativeType);
        binder.forField(name)
            .bind(Relative::getName, Relative::setName);
        binder.forField(contacts)
            .bind(Relative::getContacts, Relative::setContacts);
        binder.forField(scansUpload)
            .bind(Relative::getScans, Relative::setScans);

        binder.addValueChangeListener(event -> save.setEnabled(binder.isValid()));
        binder.setBean(relative);

        save.addClickListener(event -> {
            if (relatives.stream().noneMatch(item -> item.hashCode() == relative.hashCode()))
                relatives.add(relative);

            grid.setItems(relatives);
            closeForm();
            listener.contentChanged();
        });
        cancel.addClickListener(event -> closeForm());

        return formLayout;
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}


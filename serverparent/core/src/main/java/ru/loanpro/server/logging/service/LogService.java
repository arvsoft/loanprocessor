package ru.loanpro.server.logging.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.loanpro.global.annotation.EntityTab;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.global.service.AbstractIdEntityService;
import ru.loanpro.security.domain.User;
import ru.loanpro.server.logging.domain.AbstractLog;
import ru.loanpro.server.logging.domain.EntityActions;
import ru.loanpro.server.logging.domain.LogCloseTab;
import ru.loanpro.server.logging.domain.LogEntity;
import ru.loanpro.server.logging.domain.LogEntityPrint;
import ru.loanpro.server.logging.domain.LogError;
import ru.loanpro.server.logging.domain.LogLogin;
import ru.loanpro.server.logging.domain.LogLogout;
import ru.loanpro.server.logging.domain.LogOpenTab;
import ru.loanpro.server.logging.domain.LogTimeShift;
import ru.loanpro.server.logging.repository.AbstractLogRepository;
import ru.loanpro.uibasis.component.BaseTab;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

/**
 * Сервис для работы с логами системы.
 *
 * @author Oleg Brizhevatikh
 */
@Service
public class LogService extends AbstractIdEntityService<AbstractLog, AbstractLogRepository> {


    @Autowired
    public LogService(AbstractLogRepository repository) {
        super(repository);
    }

    /**
     * Лог успешного входа пользователя.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     */
    public void logLogin(User user, Integer session, String ip) {
        LogLogin log = new LogLogin();
        log.setUser(user);
        log.setSession(session);
        log.setIp(ip);
        log.setInstant(Instant.now());
        repository.save(log);
    }

    /**
     * Лог выхода пользователя из системы.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     */
    public void logLogout(User user, Integer session, String ip) {
        LogLogout log = new LogLogout();
        log.setUser(user);
        log.setSession(session);
        log.setIp(ip);
        log.setInstant(Instant.now());
        repository.save(log);
    }

    /**
     * Лог открытия вкладки пользователем.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     * @param tab     открываемая вкладка.
     */
    public void logOpenTab(User user, Integer session, String ip, BaseTab tab) {
        LogOpenTab log = newLogItem(LogOpenTab.class, user, session, ip);
        assert log != null;
        log.settClass(tab.getClass().getName());

        repository.save(log);
    }

    /**
     * Лог закрытия вкладки.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     * @param tab     закрываемая вкладка.
     */
    public void logCloseTab(User user, Integer session, String ip, BaseTab tab) {
        LogCloseTab log = newLogItem(LogCloseTab.class, user, session, ip);
        assert log != null;
        log.settClass(tab.getClass().getName());

        repository.save(log);
    }

    /**
     * Лог открытия вкладки с сущностью.
     *
     * @param user       пользователь.
     * @param session    сессия пользователя.
     * @param ip         ip адрес пользователя.
     * @param tab        открываемая вкладка.
     * @param objectList список параметров открываемой вкладки.
     */
    public void logOpenEntity(User user, Integer session, String ip, BaseTab tab, List<Object> objectList) {
        LogEntity log = newLogItem(LogEntity.class, user, session, ip);
        assert log != null;

        EntityTab entityTab = tab.getClass().getAnnotation(EntityTab.class);
        if (entityTab == null) return;

        log.settClass(entityTab.value().getName());
        if (objectList.size() == 0 || ((AbstractIdEntity) objectList.get(0)).getId() == null) {
            log.setAction(EntityActions.NEW);
        } else {
            log.setAction(EntityActions.OPEN);
            log.setEntityId(((AbstractIdEntity) objectList.get(0)).getId());
        }

        repository.save(log);
    }

    /**
     * Лог закрытия вкладки с сущностью.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     * @param tab     закрываемая вкладка.
     */
    public void logEntityClose(User user, Integer session, String ip, BaseTab tab) {
        LogEntity log = newLogItem(LogEntity.class, user, session, ip);
        if (log == null) return;

        EntityTab entityTab = tab.getClass().getAnnotation(EntityTab.class);
        if (entityTab == null) return;

        log.settClass(entityTab.value().getName());
        log.setAction(EntityActions.CLOSE);
        AbstractIdEntity tabObject = (AbstractIdEntity) tab.getObject();
        log.setEntityId(tabObject == null ? null : tabObject.getId());

        repository.save(log);
    }

    /**
     * Лог сохранения сущности.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     * @param entity  сохраняемая сущность.
     */
    public void logEntitySave(User user, Integer session, String ip, AbstractIdEntity entity) {
        LogEntity log = newLogItem(LogEntity.class, user, session, ip);
        if (log == null) return;

        log.settClass(Hibernate.unproxy(entity).getClass().getName());
        log.setAction(EntityActions.SAVE);
        log.setEntityId(entity.getId());

        repository.save(log);
    }

    /**
     * Лог начала редактирования сущности.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     * @param entity  редактируемая сущность.
     */
    public void logEntityEdit(User user, int session, String ip, AbstractIdEntity entity) {
        LogEntity log = newLogItem(LogEntity.class, user, session, ip);
        if (log == null) return;

        log.settClass(entity.getClass().getName());
        log.setAction(EntityActions.EDIT);
        log.setEntityId(entity.getId());

        repository.save(log);
    }

    /**
     * Лог печати сущности.
     *
     * @param user     пользователь.
     * @param session  сессия пользователя.
     * @param ip       ip адрес пользователя.
     * @param entity   печатаемая сущность.
     * @param fileName имя полученного при печати файла.
     */
    public void logEntityPrint(User user, Integer session, String ip, AbstractIdEntity entity, String fileName) {
        LogEntityPrint log = newLogItem(LogEntityPrint.class, user, session, ip);
        if (log == null) return;

        log.settClass(entity.getClass().getName());
        log.setAction(EntityActions.PRINT);
        log.setEntityId(entity.getId());
        log.setFileName(fileName);

        repository.save(log);
    }

    /**
     * Лог изменения пользователем рабочего времени.
     *
     * @param user           пользователь.
     * @param session        сессия пользователя.
     * @param ip             ip адрес пользователя.
     * @param zoneId         часовая зона пользователя.
     * @param targetDateTime время, на которое было изменено текущее время.
     */
    public void logTimeShift(User user, Integer session, String ip, ZoneId zoneId, LocalDateTime targetDateTime) {
        LogTimeShift log = new LogTimeShift();
        log.setUser(user);
        log.setSession(session);
        log.setIp(ip);
        log.setInstant(Instant.now());
        log.setTimeShift(targetDateTime.atZone(zoneId).toInstant());

        repository.save(log);
    }

    /**
     * Лог восстановления пользователем текущего рабочего времени.
     *
     * @param user    пользователь.
     * @param session сессия пользователя.
     * @param ip      ip адрес пользователя.
     */
    public void logTimeShiftClear(User user, Integer session, String ip) {
        LogTimeShift log = new LogTimeShift();
        log.setUser(user);
        log.setSession(session);
        log.setIp(ip);
        log.setInstant(Instant.now());
        log.setTimeShift(Instant.now());

        repository.save(log);
    }

    private <T extends AbstractLog> T newLogItem(Class<T> tClass, User user, Integer session, String ip) {
        try {
            T log = tClass.newInstance();
            log.setUser(user);
            log.setSession(session);
            log.setIp(ip);
            log.setInstant(Instant.now());

            return log;
        } catch (InstantiationException | IllegalAccessException e) {
            LogError logError = new LogError(Instant.now(), e.getMessage());
            repository.save(logError);
            return null;
        }
    }
}

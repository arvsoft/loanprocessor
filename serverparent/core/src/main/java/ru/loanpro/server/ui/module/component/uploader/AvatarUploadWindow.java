package ru.loanpro.server.ui.module.component.uploader;

import com.vaadin.server.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.spring.i18n.I18N;

import ru.loanpro.base.storage.StorageService;
import ru.loanpro.base.storage.exception.FileSystemErrorException;
import ru.loanpro.server.ui.module.component.uploader.UploadPane.FileExtension;
import ru.loanpro.server.ui.module.component.uploader.UploadPane.SelectType;

import java.io.ByteArrayOutputStream;

/**
 * Окно загрузки аватара пользователя и фотографии клиента.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
public class AvatarUploadWindow extends Window implements UploadPane.UploadCallback {

    private final StorageService storageService;
    private final I18N i18n;

    private final Image image;
    private final Button save;
    private String avatarName;
    private AvatarUploadCallback avatarUploadCallback;
    private UploadImageType uploadImageType = UploadImageType.USER_AVATAR;

    /**
     * Интерфейс обратного вызова, для информирования системы о выборе изображения.
     */
    public interface AvatarUploadCallback {
        /**
         * Событие выбора изображения.
         *
         * @param fileName имя изображения.
         */
        void avatarSelected(String fileName);
    }

    /**
     * Перечисление видов загружаемых объектов. Аватар пользователя или фотография клиента.
     * TODO разделить на два отдельных класса.
     */
    public enum UploadImageType {
        USER_AVATAR, CLIENT_PHOTO
    }

    @Autowired
    public AvatarUploadWindow(StorageService storageService, I18N i18n) {
        super(i18n.get("component.upload.avatarWindow.title"));
        this.i18n = i18n;
        image = new Image();
        image.setVisible(false);
        image.setWidth(256, Unit.PIXELS);

        UploadPane dropPane = new UploadPane(this, i18n, SelectType.SINGLE, FileExtension.JPG);

        save = new Button(i18n.get("component.upload.avatarWindow.select"));
        save.setVisible(false);
        save.addClickListener(event -> {
            avatarUploadCallback.avatarSelected(avatarName);
            close();
        });

        VerticalLayout layout = new VerticalLayout(image, dropPane, save);
        layout.setComponentAlignment(image, Alignment.TOP_CENTER);
        layout.setComponentAlignment(save, Alignment.BOTTOM_CENTER);
        setContent(layout);
        setResizable(false);
        setModal(true);
        center();
        this.storageService = storageService;
    }

    public void setAvatarUploadCallbackHandler(AvatarUploadCallback avatarUploadCallback) {
        this.avatarUploadCallback = avatarUploadCallback;
    }

    public void setUploadImageType(UploadImageType imageType) {
        this.uploadImageType = imageType;
    }

    @Override
    public void uploading(String fileName, ByteArrayOutputStream bas) {
        try {
            FileResource resource = null;
            switch (uploadImageType) {
                case USER_AVATAR:
                    resource = storageService.saveAvatar(bas);
                    break;
                case CLIENT_PHOTO:
                    resource = storageService.saveScannedFile(fileName, bas);
                    break;
            }

            avatarName = resource.getFilename();
            image.setSource(resource);
            image.setVisible(true);

            save.setVisible(true);

            center();
        } catch (FileSystemErrorException e) {
            Notification.show(String.format(i18n.get("component.upload.fileSystemError"), e.getMessage()), Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}

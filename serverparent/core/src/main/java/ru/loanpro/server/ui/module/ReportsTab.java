package ru.loanpro.server.ui.module;

import com.google.common.collect.Lists;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.report.domain.Report;
import ru.loanpro.base.report.service.ReportService;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.directory.ReportGroup;
import ru.loanpro.server.ui.module.report.ReportCard;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.uibasis.component.BaseTab;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Вкладка работы с отчетами
 *
 * @author Maksim Askhaev
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.reports")
public class ReportsTab  extends BaseTab{

    private List<Report> reportList;

    private final ReportService service;

    @Autowired
    public ReportsTab(ReportService reportService) {
        this.service = reportService;
    }

    @PostConstruct
    public void init() {

        ComboBox<ReportGroup> reportGroupFilter = new ComboBox<>(i18n.get("reports.combobox.reportGroup"),
            Lists.newArrayList(ReportGroup.values()));
        reportGroupFilter.setItemCaptionGenerator(item -> i18n.get(item.getName()));

        HorizontalLayout reportLayout = new HorizontalLayout();
        reportLayout.setSpacing(true);
        reportLayout.addStyleName("flex-wrap-layout");

        reportList = service.findAll();

        reportList.forEach(item->{
            ReportCard card = new ReportCard(item, service, i18n, sessionEventBus);
            card.setVisible(securityService.hasAuthority(card.getRole()));
            reportLayout.addComponent(card);
        });

        addComponents(reportGroupFilter, reportLayout);
        setComponentAlignment(reportGroupFilter, Alignment.TOP_LEFT);
    }

    @Override
    public String getTabName() {
        return i18n.get("report.tab.caption");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.LIME;
    }
}

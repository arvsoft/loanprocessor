package ru.loanpro.server.ui.module;

import com.github.appreciated.material.MaterialTheme;
import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Binder;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateTimeField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.global.UiTheme;
import ru.loanpro.server.ui.module.event.TimeTickEvent;
import ru.loanpro.global.annotation.SingletonTab;
import ru.loanpro.uibasis.component.BaseTab;
import ru.loanpro.uibasis.event.RemoveTabEvent;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Вкладка для работы с текущим временем пользователя.
 *
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@SingletonTab("core.menu.chronometer")
public class ChronometerTab extends BaseTab {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    private Label currentTimeLabel;

    @PostConstruct
    public void init() {
        setSpacing(true);
        setMargin(true);

        ChronometerSettings settings = new ChronometerSettings();
        settings.setTimeShifted(chronometerService.isTimeShifted());
        settings.setDateTime(chronometerService.getCurrentTime().toLocalDateTime());
        settings.setTimeFrozen(chronometerService.isTimeFrozen());

        ZonedDateTime systemTime = chronometerService.getSystemTime();

        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setSpacing(true);
        form.setWidth(800, Unit.PIXELS);
        form.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);
        addComponent(form);
        setComponentAlignment(form, Alignment.TOP_CENTER);

        Label section = new Label(i18n.get("chronometer.globalInfo"));
        section.addStyleName(MaterialTheme.LABEL_H3);
        section.addStyleName(MaterialTheme.LABEL_COLORED);
        form.addComponent(section);

        currentTimeLabel = new Label(systemTime.format(DATE_TIME_FORMATTER));
        currentTimeLabel.setCaption(i18n.get("chronometer.systemTime"));
        currentTimeLabel.addStyleName(MaterialTheme.LABEL_LARGE);
        form.addComponent(currentTimeLabel);

        CheckBox changeTime = new CheckBox(i18n.get("chronometer.timeShifted"));
        form.addComponent(changeTime);

        DateTimeField dateTimeField = new DateTimeField(i18n.get("chronometer.newDateTime"));
        dateTimeField.setWidth(50, Unit.PERCENTAGE);
        dateTimeField.setDateFormat("dd.MM.yyyy HH:mm");
        dateTimeField.setEnabled(settings.isTimeShifted());
        form.addComponent(dateTimeField);

        CheckBox freezeTime = new CheckBox(i18n.get("chronometer.timeFrozen"));
        freezeTime.setEnabled(settings.isTimeShifted());
        form.addComponent(freezeTime);

        Button save = new Button(i18n.get("chronometer.save"));
        Button saveAndClose = new Button(i18n.get("chronometer.saveAndClose"));
        form.addComponent(new HorizontalLayout(save, saveAndClose));

        Binder<ChronometerSettings> binder = new Binder<>();
        binder.setBean(settings);
        binder.bind(changeTime, ChronometerSettings::isTimeShifted, ChronometerSettings::setTimeShifted);
        binder.bind(dateTimeField, ChronometerSettings::getDateTime, ChronometerSettings::setDateTime);
        binder.bind(freezeTime, ChronometerSettings::isTimeFrozen, ChronometerSettings::setTimeFrozen);

        binder.addValueChangeListener(event -> {
            save.setEnabled(binder.isValid());
            saveAndClose.setEnabled(binder.isValid());

            dateTimeField.setEnabled(settings.isTimeShifted());
            freezeTime.setEnabled(settings.isTimeShifted());
        });

        save.addClickListener(event -> {
            if (settings.isTimeShifted())
                chronometerService.setShiftDateTime(settings.getDateTime(), settings.isTimeFrozen());
            else
                chronometerService.clearShiftDateTime();

            save.setEnabled(false);
            saveAndClose.setEnabled(false);
            sessionEventBus.post(new TimeTickEvent(chronometerService.getCurrentTime()));
        });

        saveAndClose.addClickListener(event -> {
            save.click();
            sessionEventBus.post(new RemoveTabEvent<>(this));
        });

        addComponent(form);
        setComponentAlignment(form, Alignment.TOP_CENTER);
        setSizeFull();
    }

    @Override
    public String getTabName() {
        return i18n.get("chronometer.tabName");
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.PINK;
    }

    @Subscribe
    private void timeTickEventHandler(TimeTickEvent event) {
        getUI().access(() ->
            currentTimeLabel.setValue(chronometerService.getSystemTime().format(DATE_TIME_FORMATTER)));
    }

    private class ChronometerSettings {
        private boolean isTimeShifted;
        private LocalDateTime dateTime;
        private boolean isTimeFrozen;

        boolean isTimeShifted() {
            return isTimeShifted;
        }

        void setTimeShifted(boolean timeShifted) {
            this.isTimeShifted = timeShifted;
        }

        LocalDateTime getDateTime() {
            return dateTime;
        }

        void setDateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
        }

        boolean isTimeFrozen() {
            return isTimeFrozen;
        }

        void setTimeFrozen(boolean timeFrozen) {
            this.isTimeFrozen = timeFrozen;
        }
    }
}

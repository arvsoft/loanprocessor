package ru.loanpro.server.ui.module;


import com.github.appreciated.material.MaterialTheme;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.stereotype.Component;
import org.vaadin.spring.annotation.PrototypeScope;
import ru.loanpro.base.loan.domain.Loan;
import ru.loanpro.base.loan.exceptions.LoanNotSavedException;
import ru.loanpro.base.schedule.domain.Schedule;
import ru.loanpro.global.Roles;
import ru.loanpro.global.UiTheme;
import ru.loanpro.global.directory.CalcScheme;
import ru.loanpro.global.directory.InterestType;
import ru.loanpro.global.directory.RecalculationType;
import ru.loanpro.global.directory.ReturnTerm;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;
import ru.loanpro.server.ui.module.component.ClientsForm;
import ru.loanpro.server.ui.module.component.PaymentsForm;
import ru.loanpro.server.ui.module.component.ProlongationsForm;
import ru.loanpro.server.ui.module.component.ScheduleForm;
import ru.loanpro.server.ui.module.event.ScheduleRecalculatedEvent;
import ru.loanpro.server.ui.module.event.UpdateBaseLoanEvent;
import ru.loanpro.global.annotation.EntityTab;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Вкладка работы с пролонгированным займом
 *
 * @author Maksim Askhaev
 * @author Oleg Brizhevatikh
 */
@Component
@PrototypeScope
@EntityTab(Loan.class)
public class LoanProlongationTab extends LoanTab {

    private MoneyField tfCurrentBalanceValue;
    private MoneyField tfProlongCalculationValue;
    private MoneyField tfProlongValue;
    private CheckBox cbxInterestManual;
    private CheckBox cbxPenaltyManual;
    private CheckBox cbxLoanTermManual;
    private CheckBox cbxCalculation;
    private ComboBox<RecalculationType> cbRecalculationType;

    private int originalTerm;
    private LocalDate lastPayDate;
    private LocalDate prolongDate;
    private CalcScheme calcScheme;
    private ReturnTerm returnTerm;
    private BigDecimal initialPaymentValue;

    private int prolongationHashCode;

    public LoanProlongationTab(Loan loanProlonged, Loan loan, List<Loan> prolongedLoans) {
        super(loan);
        this.loanProlonged = loanProlonged;
        this.isInProlongationProcess = true;
        this.isScheduledCorrect = false;
        this.prolongedLoans = prolongedLoans;

        this.originalTerm = loanProlonged.getLoanTerm();
        this.lastPayDate = loanProlonged.getLastPayDate();
        this.calcScheme = loanProlonged.getCalcScheme();
        this.returnTerm = loanProlonged.getReturnTerm();
        this.prolongDate = loan.getProlongDate();

        Optional<Schedule> schedule = this.loanProlonged.getSchedules().stream().filter(item -> item.getOrdNo() == 1).findFirst();
        if (loanProlonged.getCalcScheme() == CalcScheme.DIFFERENTIAL) {
            schedule.ifPresent(schedule1 -> initialPaymentValue = schedule1.getPrincipal());
        }
        if (loanProlonged.getCalcScheme() == CalcScheme.ANNUITY) {
            schedule.ifPresent(schedule1 -> initialPaymentValue = schedule1.getPayment());
        }
    }

    @PostConstruct
    @Override
    public void init() {
        applicationEventBus.register(this);

        binder = new Binder<>();
        binder.setBean(entity);
        binder.addValueChangeListener(event -> buttonsStatusUpdate());
        entityIsReadOnly = entity.getId() != null;

        Layout baseLayout = initBaseInformation();
        baseLayout.setSizeFull();
        baseLayout.addStyleName(MaterialTheme.CARD_2);

        scheduleForm = new ScheduleForm<>(
            entity, binder, i18n, chronometerService, configurationService, sessionEventBus, isInProlongationProcess);

        tabsheet.setSizeFull();
        tabsheet.addStyleNames(ValoTheme.TABSHEET_FRAMED, ValoTheme.TABSHEET_PADDED_TABBAR);
        tabsheet.addTab(scheduleForm, i18n.get("loan.tabsheet.caption.schedule"), VaadinIcons.TABLE);

        ClientsForm clientsForm = new ClientsForm(
            entity, binder, i18n, clientService, sessionEventBus);
        tabsheet.addTab(clientsForm, i18n.get("loan.tabsheet.caption.client"));

        paymentsForm = new PaymentsForm(i18n, sessionEventBus, configurationService);
        tabsheet.addTab(paymentsForm, i18n.get("loan.tabsheet.caption.payment"));
        paymentsForm.setItems((entity).getPayments());

        if (prolongedLoans == null) prolongedLoans = new ArrayList<>();
        prolongationsForm = new ProlongationsForm(i18n, prolongedLoans);
        tabsheet.addTab(prolongationsForm, i18n.get("loan.tabsheet.caption.prolongation"));

        addComponents(baseLayout, tabsheet);
        setExpandRatio(tabsheet, 1);

        entityHash = entity.hashCode();
        binder.setReadOnly(true);
        scheduleForm.calculateScheduleHash();
        prolongationHashCode = Objects.hash(tfProlongCalculationValue.getValue(), cbRecalculationType.getValue());
    }

    @Override
    protected Layout initBaseInformation() {
        Layout layout = super.initBaseInformation();
        draftNumber = numberService.getProlongSequenceDraftNumber();
        entity.setProlongAgrNumber(draftNumber);
        entity.setUser(securityService.getUser());
        prolongation.setVisible(false);
        edit.setVisible(false);
        prolongInfo.setValue(i18n.get("loan.prolong.field.prolongNewInfo"));

        layout.addComponent(initProlongBlock());
        binder.setBean(binder.getBean());
        binder.setReadOnly(true);

        save.addClickListener(event -> {
            try {
                loanService.saveProlongedLoan(loanProlonged, entity, draftNumber);

                sessionEventBus.post(new UpdateBaseLoanEvent<>(entity));

                entityHash = entity.hashCode();
                binder.setBean(binder.getBean());
                binder.setReadOnly(true);
                entityIsReadOnly = true;
                buttonsStatusUpdate();

                cbxLoanTermManual.setReadOnly(true);
                cbxCalculation.setReadOnly(true);
                cbxInterestManual.setReadOnly(true);
                cbxPenaltyManual.setReadOnly(true);
                cbRecalculationType.setReadOnly(true);

                prolongedLoans.add(entity);
                prolongationsForm.setItems(prolongedLoans);

                Notification.show(i18n.get("baseloan.message.recordsaved"), Notification.Type.TRAY_NOTIFICATION);
                delete.setEnabled(baseLoanService.canBeDeleted(entity));
            } catch (LoanNotSavedException e) {
                Notification.show(i18n.get(e.getLocalizedMessage()), Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
            }
        });

        return layout;
    }

    private Layout initProlongBlock() {

        tfCurrentBalanceValue = new MoneyField(i18n.get("scheduleForm.prolong.field.balanceValue"), configurationService.getLocale());
        tfCurrentBalanceValue.addStyleName(MaterialTheme.LABEL_COLORED);
        tfCurrentBalanceValue.setWidth(150, Unit.PIXELS);
        tfCurrentBalanceValue.addStyleName("align-right");
        tfCurrentBalanceValue.setReadOnly(true);

        VerticalLayout vlBalance = new VerticalLayout(tfCurrentBalanceValue);
        vlBalance.setMargin(false);

        tfProlongCalculationValue = new MoneyField(i18n.get("scheduleForm.prolong.field.calculationValue"), configurationService.getLocale());
        tfProlongCalculationValue.addStyleName(MaterialTheme.LABEL_COLORED);
        tfProlongCalculationValue.setWidth(150, Unit.PIXELS);
        tfProlongCalculationValue.addStyleName("align-right");
        tfProlongCalculationValue.setReadOnly(true);

        tfProlongCalculationValue.addBlurListener(event -> {
            entity.setProlongValue(entity.getCurrentBalance().add(entity.getProlongCalculation()));
            binder.setBean(binder.getBean());
        });

        cbxCalculation = new CheckBox(i18n.get("scheduleForm.prolong.checkbox.manual"));
        cbxCalculation.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxCalculation.setEnabled(securityService.hasAuthority(Roles.Loan.CHANGECALCULATIONVALUE));
        cbxCalculation.addValueChangeListener(value -> {
            if (value.getValue()) {
                tfProlongCalculationValue.setReadOnly(false);
                tfProlongCalculationValue.focus();
            } else {
                tfProlongCalculationValue.setReadOnly(true);
            }
        });

        VerticalLayout vlCalculation = new VerticalLayout(tfProlongCalculationValue, cbxCalculation);
        vlCalculation.setMargin(false);

        tfProlongValue = new MoneyField(i18n.get("scheduleForm.prolong.field.prolongValue"), configurationService.getLocale());
        tfProlongValue.addStyleName(MaterialTheme.LABEL_COLORED);
        tfProlongValue.setWidth(150, Unit.PIXELS);
        tfProlongValue.addStyleName("align-right");
        tfProlongValue.setReadOnly(true);

        VerticalLayout vlProlong = new VerticalLayout(tfProlongValue);
        vlProlong.setMargin(false);

        HorizontalLayout hlCalculation = new HorizontalLayout(vlBalance, vlCalculation, vlProlong);
        hlCalculation.setMargin(false);

        cbRecalculationType = new ComboBox<>(
            i18n.get("scheduleForm.prolong.combobox.recalculationType"), Lists.newArrayList(RecalculationType.values()));
        cbRecalculationType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbRecalculationType.setEnabled(securityService.hasAuthority(Roles.Loan.CHANGERECALCULATIONTYPE));
        cbRecalculationType.setEmptySelectionAllowed(false);
        cbRecalculationType.setWidth(500, Unit.PIXELS);
        cbRecalculationType.setValue(entity.getRecalculationType());

        cbRecalculationType.addValueChangeListener(event -> {
            int term = calculateTerm(returnTerm, calcScheme, prolongDate, lastPayDate, originalTerm, event.getValue());
            entity.setLoanTerm(term);
            entity.setRecalculationType(event.getValue());
            binder.setBean(binder.getBean());
            buttonsStatusUpdate();
        });

        cbxLoanTermManual = new CheckBox(i18n.get("scheduleForm.prolong.checkbox.loanTerm.manual"));
        cbxLoanTermManual.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxLoanTermManual.setEnabled(securityService.hasAuthority(Roles.Loan.CHANGELOANTERM));
        cbxLoanTermManual.addValueChangeListener(event -> {
            scheduleForm.getTfLoanTerm().setReadOnly(!event.getValue());
            scheduleForm.getTfLoanTerm().focus();
        });

        cbxInterestManual = new CheckBox(i18n.get("scheduleForm.prolong.checkbox.interest.manual"));
        cbxInterestManual.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxInterestManual.setEnabled(securityService.hasAuthority(Roles.Loan.CHANGEINTERESTRATE));
        cbxInterestManual.addValueChangeListener(event -> {
            scheduleForm.getTfInterestValue().setReadOnly(!event.getValue());
            scheduleForm.getTfInterestValue().focus();
        });

        cbxPenaltyManual = new CheckBox(i18n.get("scheduleForm.prolong.checkbox.penalty.manual"));
        cbxPenaltyManual.addStyleName(ValoTheme.CHECKBOX_SMALL);
        cbxPenaltyManual.setEnabled(securityService.hasAuthority(Roles.Loan.CHANGEPENALTYRATE));
        cbxPenaltyManual.addValueChangeListener(event -> {
            scheduleForm.getTfPenaltyValue().setReadOnly(!event.getValue());
            scheduleForm.getTfPenaltyValue().focus();
        });

        HorizontalLayout checkboxLayout = new HorizontalLayout(cbxInterestManual, cbxPenaltyManual);

        VerticalLayout layout = new VerticalLayout(hlCalculation, cbRecalculationType, cbxLoanTermManual, checkboxLayout);
        layout.setSpacing(true);
        layout.setMargin(false);
        layout.setSizeFull();

        binder.forField(tfCurrentBalanceValue)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(Loan::getCurrentBalance, Loan::setCurrentBalance);

        binder.forField(tfProlongCalculationValue)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(Loan::getProlongCalculation, Loan::setProlongCalculation);

        binder.forField(tfProlongValue)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(Loan::getProlongValue, Loan::setProlongValue);

        return layout;
    }

    private int calculateTerm(ReturnTerm returnTerm, CalcScheme calcScheme,
                              LocalDate prolongDate, LocalDate lastPayDate, int originalTerm,
                              RecalculationType recalculationType) {

        int result = originalTerm;

        if (recalculationType == RecalculationType.RECALCULATION_ON_NEW_TERM) {
            result = originalTerm;
        }

        if (recalculationType == RecalculationType.REDUCE_REPAYMENTS_REMAIN_TERM) {
            if (returnTerm == ReturnTerm.MONTHLY) {
                result = (int) ChronoUnit.MONTHS.between(prolongDate, lastPayDate.plusDays(1));
            }
            if (returnTerm == ReturnTerm.WEEKLY) {
                result = (int) ChronoUnit.WEEKS.between(prolongDate, lastPayDate.plusDays(1));
            }
        }

        if (recalculationType == RecalculationType.REDUCE_TERM_REMAIN_REPAYMENTS) {
            double n1, cProcent_Stavka = 1, cq, cSumZaim_Period, cSumZaim;
            BigDecimal ii = entity.getProlongValue();
            double initialCurrentBalance = ii.doubleValue();
            double cProcent = entity.getInterestValue().doubleValue();

            if (returnTerm == ReturnTerm.MONTHLY) {
                if (calcScheme == CalcScheme.DIFFERENTIAL) {
                    n1 = initialCurrentBalance / initialPaymentValue.doubleValue();
                    result = (int) Math.round(n1);
                }
                if (calcScheme == CalcScheme.ANNUITY) {
                    if (entity.getInterestType() == InterestType.PERCENT_A_YEAR) {
                        cProcent_Stavka = cProcent / (100.0 * 12);
                    }
                    if (entity.getInterestType() == InterestType.PERCENT_A_MONTH) {
                        cProcent_Stavka = cProcent / 100.0;
                    }
                    cq = 1 + cProcent_Stavka;
                    cSumZaim_Period = initialPaymentValue.doubleValue();
                    cSumZaim = initialCurrentBalance;
                    double a = cq;
                    double b = (cSumZaim * cProcent_Stavka / (cSumZaim_Period - cSumZaim * cProcent_Stavka)) + 1;
                    n1 = Math.log10(b) / Math.log10(a);
                    result = (int) Math.round(n1);
                }
            }
            if (returnTerm == ReturnTerm.WEEKLY) {
                if (calcScheme == CalcScheme.DIFFERENTIAL) {
                    n1 = loanProlonged.getProlongValue().doubleValue() / initialPaymentValue.doubleValue();
                    result = (int) Math.round(n1);
                }
                if (calcScheme == CalcScheme.ANNUITY) {
                    cProcent = entity.getInterestValue().doubleValue();
                    if (entity.getInterestType() == InterestType.PERCENT_A_YEAR) {
                        cProcent_Stavka = cProcent / (100 * 365.0);
                    }
                    if (entity.getInterestType() == InterestType.PERCENT_A_MONTH) {
                        cProcent_Stavka = cProcent / 100.0;
                    }
                    cq = 1 + cProcent_Stavka;
                    cSumZaim_Period = initialPaymentValue.doubleValue();
                    cSumZaim = initialCurrentBalance;
                    double a = cq;
                    double b = (cSumZaim * cProcent_Stavka / (cSumZaim_Period - cSumZaim * cProcent_Stavka)) + 1;
                    n1 = Math.log10(b) / Math.log10(a);
                    result = (int) Math.round(n1);
                }
            }
        }

        if (result <= 0) result = originalTerm;

        return result;
    }

    @Override
    protected void buttonsStatusUpdate() {
        isScheduledCorrect = entity.getReturnTerm() != ReturnTerm.INDIVIDUALLY
            || entity.getSchedules().stream()
            .map(Schedule::getBalance)
            .map(BigDecimal::doubleValue)
            .anyMatch(item -> item == 0.0);

        int hashCode = Objects.hash(tfProlongCalculationValue.getValue(), cbRecalculationType.getValue());
        save.setEnabled(prolongationHashCode == hashCode && scheduleForm.getScheduleHash() == entity.scheduleHashCode() && isScheduledCorrect &&
            binder.isValid() && (entity.hashCode() != entityHash || entity.getId() == null));
        print.setEnabled(entity.getId() != null);
    }

    @Override
    public void destroy() {
        super.destroy();

        if (entity.getId() == null)
            numberService.clearProlongSequenceDraftNumber(entity.getProlongAgrNumber());
    }

    @Override
    public String getTabName() {
        return i18n.get("loanProlongation.tab.caption") + entity.getLoanNumber();
    }

    @Override
    public UiTheme.TabStyle getTabStyle() {
        return UiTheme.TabStyle.INDIGO;
    }

    @Subscribe
    public void scheduleRecalculatedEventHandlerEx(ScheduleRecalculatedEvent<Loan> event) {
        if (entity == event.getEntity()) {
            prolongationHashCode = Objects.hash(tfProlongCalculationValue.getValue(), cbRecalculationType.getValue());
            buttonsStatusUpdate();
        }
    }
}

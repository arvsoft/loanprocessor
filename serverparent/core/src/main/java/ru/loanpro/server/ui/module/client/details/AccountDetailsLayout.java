package ru.loanpro.server.ui.module.client.details;

import com.google.common.collect.Lists;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.BigDecimalRangeValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import ru.loanpro.base.NumberService;
import ru.loanpro.base.account.domain.BaseAccount;
import ru.loanpro.base.account.domain.DepartmentAccount;
import ru.loanpro.base.account.domain.JuridicalAccount;
import ru.loanpro.base.account.domain.PhysicalAccount;
import ru.loanpro.base.client.domain.Client;
import ru.loanpro.base.client.domain.PhysicalClient;
import ru.loanpro.global.Roles;
import ru.loanpro.global.directory.AccountType;
import ru.loanpro.security.domain.Department;
import ru.loanpro.security.service.ConfigurationService;
import ru.loanpro.security.service.SecurityService;
import ru.loanpro.server.ui.module.additional.MoneyConverter;
import ru.loanpro.server.ui.module.additional.MoneyField;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Set;

/**
 * Панель с информацией и инструментами редактирования счетов клиента
 *
 * @author Maksim Askhaev
 */
public class AccountDetailsLayout<T extends BaseAccount> extends HorizontalLayout {
    private final Set<T> accounts;
    private final I18N i18n;
    private final NumberService numberService;
    private final ConfigurationService configurationService;
    private final SecurityService securityService;
    private String accountDraftNumber;
    private final Object object;
    private FormLayout form;
    private VerticalLayout layout;
    private final Grid<T> grid;
    private ContentChangeListener listener;

    public AccountDetailsLayout(Object object, Set<T> accounts, I18N i18n, ConfigurationService configurationService,
                                NumberService numberService, LocalDate registrationDate, SecurityService securityService) {
        DecimalFormat moneyFormat = (DecimalFormat) NumberFormat.getInstance(configurationService.getLocale());
        moneyFormat.setMinimumFractionDigits(2);

        this.object = object;
        this.i18n = i18n;
        this.accounts = accounts;
        this.configurationService = configurationService;
        this.numberService = numberService;
        this.securityService = securityService;

        grid = new Grid<>();
        grid.setSizeFull();
        grid.setHeight(300, Unit.PIXELS);
        grid.setItems(accounts);
        grid.addColumn(item -> i18n.get(item.getTypeAccount().getName()))
            .setCaption(i18n.get("client.accountDetails.field.accountType"));
        grid.addColumn(T::getNumber).setCaption(i18n.get("client.accountDetails.field.accountNumber"));
        grid.addColumn(T::getRegistrationDate).setCaption(i18n.get("client.accountDetails.field.accountDate"));
        grid.addColumn(item -> item.getBalance() == null ? "" : moneyFormat.format(item.getBalance()))
            .setCaption(i18n.get("client.accountDetails.field.accountBalance"))
            .setStyleGenerator(item -> "v-align-right");
        grid.addColumn(T::getCurrency).setCaption(i18n.get("client.accountDetails.field.accountCurrency"));
        grid.addColumn(T::getBankDetails).setCaption(i18n.get("client.accountDetails.field.bankDetails"));

        if (object instanceof Department) {
            grid.addComponentColumn(item -> {
                CheckBox checkBox = new CheckBox();
                checkBox.setValue(item.isCashAccount());
                checkBox.setReadOnly(true);
                return checkBox;
            }).setCaption(i18n.get("client.accountDetails.field.cashAccount"));
        } else {
            grid.addComponentColumn(item -> {
                CheckBox checkBox = new CheckBox();
                checkBox.setValue(item.isDefaultAccount());
                checkBox.setReadOnly(true);
                return checkBox;
            }).setCaption(i18n.get("client.accountDetails.field.defaultAccount"));
        }

        grid.addItemClickListener(event -> {
            closeForm();

            T item = event.getItem();
            if (item != null) {
                form = getAccountForm(item);
                addComponent(form);
                setExpandRatio(layout, 2);
                setExpandRatio(form, 2);
            }
        });

        Button add = new Button(i18n.get("client.accountDetails.button.add"));
        add.setIcon(VaadinIcons.PLUS);
        add.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        add.addClickListener(event -> {
            closeForm();

            T account = null;
            if (object instanceof PhysicalClient) {
                //получаем грязный номер
                accountDraftNumber = numberService.getAccountSequenceDraftNumber();
                account = (T) new PhysicalAccount();
            }
            if (object instanceof JuridicalAccount) {
                //получаем грязный номер
                accountDraftNumber = numberService.getAccountSequenceDraftNumber();
                account = (T) new JuridicalAccount();
            }
            if (object instanceof Department) {
                //получаем грязный номер
                accountDraftNumber = numberService.getDepartmentAccountSequenceDraftNumber();
                account = (T) new DepartmentAccount();
            }

            account.setNumber(accountDraftNumber);
            account.setRegistrationDate(registrationDate);
            account.setTypeAccount(AccountType.INNER_ACCOUNT);
            account.setCurrency(configurationService.getCurrencies().get(0));
            account.setBalance(new BigDecimal(0));

            final BaseAccount baseAccount = account;

            if (object instanceof Department) {
                boolean b = accounts.stream()
                    .noneMatch(item -> item.getCurrency() == baseAccount.getCurrency() && item.isCashAccount());
                account.setCashAccount(b);
                account.setDefaultAccount(false);
            } else {
                boolean b = accounts.stream()
                    .noneMatch(item -> item.getCurrency() == baseAccount.getCurrency() && item.isDefaultAccount());
                account.setDefaultAccount(b);
                account.setCashAccount(false);
            }

            form = getAccountForm(account);
            addComponent(form);
            setExpandRatio(layout, 2);
            setExpandRatio(form, 2);
        });

        layout = new VerticalLayout(add, grid);
        layout.setSizeFull();
        layout.setExpandRatio(grid, 1);
        layout.setComponentAlignment(add, Alignment.TOP_RIGHT);

        addComponents(layout);
        setSizeFull();
    }

    private FormLayout getAccountForm(T account) {
        AccountType accountTypeSaved = account.getTypeAccount();
        Currency currencySaved = account.getCurrency();
        BigDecimal balanceSaved = account.getBalance();
        boolean cashAccountSaved = account.isCashAccount();
        String bankDetailsSaved = account.getBankDetails();
        boolean defaultAccountSaved = account.isDefaultAccount();

        ComboBox<AccountType> cbAccountType = new ComboBox<>(
            i18n.get("client.accountDetails.field.accountType"), Lists.newArrayList(AccountType.values()));
        cbAccountType.setItemCaptionGenerator(item -> i18n.get(item.getName()));
        cbAccountType.setEmptySelectionAllowed(false);

        ComboBox<Currency> accountCurrency = new ComboBox<>(i18n.get("client.accountDetails.field.accountCurrency"));
        accountCurrency.setItems(configurationService.getCurrencies());
        accountCurrency.setItemCaptionGenerator(Currency::getCurrencyCode);
        accountCurrency.setEmptySelectionAllowed(false);

        CheckBox defaultAccount = new CheckBox(i18n.get("client.accountDetails.field.defaultAccount"));
        defaultAccount.addStyleName(ValoTheme.CHECKBOX_SMALL);

        CheckBox cashAccount = new CheckBox(i18n.get("client.accountDetails.field.cashAccount"));
        defaultAccount.addStyleName(ValoTheme.CHECKBOX_SMALL);

        TextField accountNumber = new TextField(i18n.get("client.accountDetails.field.accountNumber"));
        accountNumber.setStyleName("v-align-right");
        accountNumber.setReadOnly(true);

        TextField bankDetails = new TextField(i18n.get("client.accountDetails.field.bankDetails"));
        bankDetails.setStyleName("v-align-right");
        bankDetails.setReadOnly(true);

        MoneyField balance = new MoneyField(i18n.get("client.accountDetails.field.AccountBalance"),
            configurationService.getLocale());
        balance.setReadOnly(!securityService.hasAuthority(Roles.UserProfile.CHANGE_ACCOUNT_BALANCE));

        Button save = new Button(i18n.get("client.accountDetails.button.save"));
        Button cancel = new Button(i18n.get("client.accountDetails.button.cancel"));
        HorizontalLayout layout = new HorizontalLayout(save, cancel);

        FormLayout formLayout;
        if (object instanceof Department) {
            formLayout = new FormLayout(cbAccountType, accountCurrency, accountNumber, bankDetails, balance, cashAccount, layout);
        } else {
            formLayout = new FormLayout(cbAccountType, accountCurrency, accountNumber, bankDetails, balance, defaultAccount, layout);
        }
        formLayout.setSizeFull();

        Binder<T> binder = new Binder<>();

        binder.forField(cbAccountType)
            .bind(T::getTypeAccount, T::setTypeAccount);

        binder.forField(accountNumber)
            .bind(T::getNumber, T::setNumber);

        binder.forField(accountCurrency)
            .bind(T::getCurrency, T::setCurrency);

        binder.forField(bankDetails)
            .bind(T::getBankDetails, T::setBankDetails);

        binder.forField(balance)
            .withConverter(new MoneyConverter(configurationService.getLocale(), "Must be a number"))
            .withValidator(new BigDecimalRangeValidator(
                "Must be a number greater than zero", BigDecimal.ZERO, BigDecimal.valueOf(Integer.MAX_VALUE)))
            .bind(T::getBalance, T::setBalance);

        binder.forField(defaultAccount)
            .bind(T::isDefaultAccount, T::setDefaultAccount);

        binder.forField(cashAccount)
            .bind(T::isCashAccount, T::setCashAccount);

        binder.setBean(account);

        binder.addValueChangeListener(event -> {
            if (cbAccountType.getValue() == AccountType.INNER_ACCOUNT) {
                bankDetails.setReadOnly(true);
                bankDetails.setValue("");
            } else bankDetails.setReadOnly(false);

            save.setEnabled(binder.isValid());
        });

        cbAccountType.addValueChangeListener(event -> {
            if (event.getValue() == AccountType.EXTERNAL_ACCOUNT) {
                if (object instanceof Department) {
                    cashAccount.setValue(false);
                    cashAccount.setReadOnly(true);
                } else {
                    defaultAccount.setValue(false);
                    defaultAccount.setReadOnly(true);
                }
            }
        });

        accountCurrency.addValueChangeListener(event -> {

            if (object instanceof Department) {
                boolean b = account.getId() != null ? account.isCashAccount() : accounts.stream()
                    .noneMatch(item -> item.getCurrency() == event.getValue() && item.isCashAccount());
                cashAccount.setValue(b);
            } else {
                boolean b = account.getId() != null ? account.isDefaultAccount() : accounts.stream()
                    .noneMatch(item -> item.getCurrency() == event.getValue() && item.isDefaultAccount());
                defaultAccount.setValue(b);
            }
        });

        save.addClickListener((Button.ClickEvent event) -> {

            if (object instanceof Department) {
                if (cashAccount.getValue()) {
                    accounts.forEach(item -> {
                        if (item.getCurrency() == accountCurrency.getValue()) {
                            item.setCashAccount(false);
                            account.setCashAccount(true);
                        }
                    });
                }
            } else {
                if (defaultAccount.getValue()) {
                    accounts.forEach(item -> {
                        if (item.getCurrency() == accountCurrency.getValue()) {
                            item.setDefaultAccount(false);
                            account.setDefaultAccount(true);
                        }
                    });
                }
            }

            if (accounts.stream().noneMatch(item -> item.hashCode() == account.hashCode())) {
                accounts.add(account);
            }

            grid.setItems(accounts);
            closeForm();
            listener.contentChanged();
        });

        cancel.addClickListener(event -> {
            account.setTypeAccount(accountTypeSaved);
            account.setCurrency(currencySaved);
            account.setBalance(balanceSaved);
            account.setCashAccount(cashAccountSaved);
            account.setBankDetails(bankDetailsSaved);
            account.setDefaultAccount(defaultAccountSaved);

            if (object instanceof Client) {
                clearAccountSequenceDraftNumber();
            } else if (object instanceof Department) {
                clearDepartmentAccountSequenceDraftNumber();
            }
            closeForm();
        });

        return formLayout;
    }

    public void clearAccountSequenceDraftNumber() {
        numberService.clearAccountSequenceDraftNumber(accountDraftNumber);
    }

    public void clearDepartmentAccountSequenceDraftNumber() {
        numberService.clearDepartmentAccountSequenceDraftNumber(accountDraftNumber);
    }

    public void refreshGrid() {
        grid.setItems(accounts);
    }

    private void closeForm() {
        if (form != null) {
            removeComponent(form);
            form = null;
        }
    }

    public void addContentChangeListener(ContentChangeListener listener) {
        this.listener = listener;
    }
}

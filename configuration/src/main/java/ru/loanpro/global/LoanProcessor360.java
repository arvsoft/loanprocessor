package ru.loanpro.global;

/**
 * Версии продукта - LoanProcessor360.
 *
 * @author Oleg Brizhevatikh
 */
public enum LoanProcessor360 {
    UNKNOWN("unknown", "unknown"),
    H2("h2", "org.h2.Driver"),
    MYSQL("mysql", "com.mysql.cj.jdbc.Driver"),
    POSTGRES("postgres", "org.postgresql.Driver");

    private String version;
    private String driverName;

    LoanProcessor360(String version, String driverName) {
        this.version = version;
        this.driverName = driverName;
    }

    /**
     * Возвращает версию продукта.
     *
     * @return версия продукта.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Возвращает имя драйвера.
     *
     * @return имя драйвера
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * Возвращает версию продукта по имени используемого драйвера базы данных.
     *
     * @param driverName имя драйвера.
     * @return версия продукта.
     */
    public LoanProcessor360 getByDriverName(String driverName) {
        for (LoanProcessor360 version : LoanProcessor360.values())
            if (version.getDriverName().equals(driverName))
                return version;

        return UNKNOWN;
    }
}

package ru.loanpro.global;

import java.util.Currency;
import java.util.Locale;

/**
 * Перечисление поддерживаемых языков.
 *
 * @author Oleg Brizhevatikh
 */
public enum SystemLocale {
    RUSSIAN(Locale.forLanguageTag("ru-RU"), "system.locale.russian", Currency.getInstance(Locale.forLanguageTag("ru-RU"))),
    ENGLISH(Locale.US, "system.locale.english", Currency.getInstance(Locale.US));

    private Locale locale;
    private String name;
    private Currency currency;

    SystemLocale(Locale locale, String name, Currency currency) {
        this.locale = locale;
        this.name = name;
        this.currency = currency;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getName() {
        return name;
    }

    public Currency getCurrency() {
        return currency;
    }
}

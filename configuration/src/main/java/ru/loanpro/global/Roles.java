package ru.loanpro.global;

/**
 * Коллекция ролей системы.
 *
 * @author Oleg Brizhevatikh
 */
public interface Roles {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";

    public interface Core {
        public static final String MENU_CLIENTS = "core.menu.clients";
        public static final String MENU_LOAN_PRODUCTS = "core.menu.loanProducts";
        public static final String MENU_REQUESTS = "core.menu.requests";
        public static final String MENU_LOANS = "core.menu.loans";
        public static final String MENU_DEPOSITS = "core.menu.deposits";
        public static final String MENU_CASHDESK = "core.menu.cashdesk";
        public static final String MENU_NOTIFICATIONS = "core.menu.notifications";
        public static final String MENU_REPORTS = "core.menu.reports";
        public static final String MENU_DIRECTORIES = "core.menu.directories";
        public static final String MENU_SETTINGS = "core.menu.settings";
        public static final String MENU_SECURITY = "core.menu.security";
        public static final String MENU_LOGS = "core.menu.logs";

        public static final String TIME_CHANGE = "core.time.change";
        public static final String TIME_FREEZE = "core.time.freeze";
    }

    public interface Client {
        public static final String CREATE = "client.create";
        public static final String DELETE = "client.create";
    }

    public interface LoanProduct {
        public static final String CREATE = "loanProduct.create";
        public static final String EDIT = "loanProduct.edit";
        public static final String CREATE_REQUEST = "loanProduct.createRequest";
        public static final String CREATE_LOAN = "loanProduct.createLoan";
        public static final String DELETE = "loanProduct.delete";
    }

    public interface Request {
        public static final String CREATE = "request.create";
        public static final String EDIT = "request.edit";
        public static final String PRINT = "request.print";
        public static final String DECISION = "request.decision";
        public static final String CHANGE_NUMBER = "request.changeNumber";
        public static final String CREATE_LOAN = "request.createLoan";
        public static final String DELETE = "request.delete";
    }

    public interface Loan {
        public static final String CREATE = "loan.create";
        public static final String EDIT = "loan.edit";
        public static final String PRINT = "loan.print";
        public static final String CHANGE_NUMBER = "loan.changeNumber";
        public static final String PAYMENT_CALCULATION = "loan.paymentCalculation";
        public static final String AUTOOPERATION = "loan.autoOperation";

        public static final String PROLONGATION = "loan.prolongation";
        public static final String CHANGELOANTERM = "loan.prolongation.changeLoanTerm";
        public static final String CHANGECALCULATIONVALUE = "loan.prolongation.changeCalculationValue";
        public static final String CHANGERECALCULATIONTYPE = "loan.prolongation.changeRecalculationType";
        public static final String CHANGEINTERESTRATE = "loan.prolongation.changeInterestRate";
        public static final String CHANGEPENALTYRATE = "loan.prolongation.changePenaltyRate";
        public static final String LOANNOTE_ADDING = "loan.loanNote.adding";
        public static final String LOANNOTE_DELETION = "loan.loanNote.deletion";
        public static final String DELETE = "loan.delete";
    }

    public interface Payment {
        public static final String SAVE = "payment.save";
        public static final String PRINT = "payment.print";
        public static final String RECALCULATION = "payment.recalculation";
        public static final String DELETELASTPAYMENT = "payment.deleteLastPayment";
        public static final String AUTOOPERATION = "payment.autoOperation";
    }

    public interface Cashdesk {
        public static final String CREATE = "cashdesk.create";
        public static final String VIEW = "cashdesk.view";
        public static final String UNLOAD_CASHBOOK = "cashdesk.unloadCashBook";
        public static final String DELETE_OPERATION = "cashdesk.deleteOperation";
    }

    public interface Operation {
        public static final String EDIT = "operation.edit";
        public static final String PREPARE = "operation.prepare";
        public static final String PROCESS = "operation.process";
        public static final String PRINT = "operation.print";
        public static final String SELECT_DEPARTMENT_PAYER = "operation.selectDepartmentPayer";
        public static final String FIX_BALANCE = "operation.fixBalance";
        public static final String DELETE_LAST_FIXATION = "operation.deleteLastFixation";
    }

    public interface Security {
        public static String USER_CREATE = "security.user.create";
        public static String USER_EDIT = "security.user.edit";
        public static String GROUP_CREATE = "security.group.create";
        public static String GROUP_EDIT = "security.group.edit";
    }

    public interface Report {
        public static String LOANS_REPORT1 = "report.loans.report1";
        public static String LOANS_REPORT2_ = "report.loans.report2";
        public static String CLIENTS_REPORT1 = "report.clients.report1";
        public static String OPERATIONS_REPORT1 = "report.operations.report1";
    }

    public interface UserProfile {
        public static String CHANGE_LOGIN = "userProfile.changeLogin";
        public static String CHANGE_PASSWORD = "userProfile.changePassword";
        public static String CHANGE_SETTINGS = "userProfile.changeSettings";
        public static String CHANGE_LOCATION_AND_PRIVILEGES = "userProfile.changeLocationAndPrivileges";
        public static String CHANGE_ACCOUNT_BALANCE = "userProfile.changeAccountBalance";
    }
}

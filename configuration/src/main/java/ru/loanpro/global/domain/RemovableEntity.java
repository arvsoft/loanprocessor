package ru.loanpro.global.domain;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Базовая сущность, с возможностью удаления.
 *
 * @author Oleg Brizhevatikh
 */
@MappedSuperclass
public abstract class RemovableEntity extends AbstractIdEntity {

    @Column(name = "is_deleted", nullable = false)
    private Boolean deleted = false;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        RemovableEntity that = (RemovableEntity) o;
        return Objects.equals(deleted, that.deleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), deleted);
    }
}

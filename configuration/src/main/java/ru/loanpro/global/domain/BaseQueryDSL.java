package ru.loanpro.global.domain;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.vaadin.data.HasValue;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

/**
 * Базовый помощник для генерации набора предикатов.
 *
 * @author Oleg Brizhevatikh
 */
public abstract class BaseQueryDSL {

    /**
     * Возвращает предикат с проверкой наличия строки в поле сущности.
     *
     * @param path  поле сущности.
     * @param field поле ввода строки.
     * @return предикат.
     */
    public static Predicate contains(StringPath path, HasValue<String> field) {
        if (field.getValue() == null || field.getValue().equals(""))
            return null;

        return path.likeIgnoreCase("%" + field.getValue() + "%");
    }

    /**
     * Возвращает предикат с проверкой соответствия строки с полем сущности.
     *
     * @param path  поле сущности.
     * @param field поле ввода строки.
     * @return предикат.
     */
    public static Predicate eq(StringPath path, HasValue<String> field) {
        if (field.getValue() == null || field.getValue().equals(""))
            return null;

        return path.eq(field.getValue());
    }

    /**
     * Возвращает предикат с проверкой соответствия элемента перечисления с полем сущности.
     *
     * @param path  поле сущности.
     * @param field поле ввода с выбором элемента перечисления.
     * @param <T>   тип перечисления.
     * @return предикат.
     */
    public static <T extends Enum<T>> Predicate eq(EnumPath<T> path, HasValue<T> field) {
        if (field.getValue() == null)
            return null;

        return path.eq(field.getValue());
    }

    /**
     * Возвращает предикат с проверкой соответствия {@link LocalDate} с полем сущности типа {@link Instant}.
     *
     * @param path  поле сущности.
     * @param field поле ввода типа {@link LocalDate}.
     * @return предикат.
     */
    public static Predicate eq(DateTimePath<Instant> path, HasValue<LocalDate> field) {
        if (field.getValue() == null)
            return null;

        LocalDate value = field.getValue();
        Instant instant = value.atStartOfDay().toInstant(ZoneOffset.UTC);
        return path.between(instant, instant.plus(1, ChronoUnit.DAYS));
    }

    /**
     * Возвращает предикат с проверкой соответствия поля ввода и поля сущности типа {@link LocalDate}.
     *
     * @param path  поле сущности.
     * @param field поле ввода.
     * @return предикат.
     */
    public static Predicate eq(DatePath<LocalDate> path, HasValue<LocalDate> field) {
        if (field.getValue() == null)
            return null;

        return path.eq(field.getValue());
    }

    /**
     * Возвращает предикат с проверкой больше ли поле сущности типа {@link Instant} чем поле ввода типа
     * {@link LocalDateTime}.
     *
     * @param path   поле сущности.
     * @param field  поле ввода.
     * @param zoneId часовая зона для приведения {@link LocalDateTime} к {@link Instant}.
     * @return предикат.
     */
    public static Predicate gt(DateTimePath<Instant> path, HasValue<LocalDateTime> field, ZoneId zoneId) {
        if (field.getValue() == null)
            return null;

        return path.gt(field.getValue().atZone(zoneId).toInstant());
    }

    /**
     * Возвращает предикат с проверкой меньше ли поле сущности типа {@link Instant} чем поле ввода типа
     * {@link LocalDateTime}.
     *
     * @param path   поле сущности.
     * @param field  поле ввода.
     * @param zoneId часовая зона для приведения {@link LocalDateTime} к {@link Instant}.
     * @return предикат.
     */
    public static Predicate lt(DateTimePath<Instant> path, HasValue<LocalDateTime> field, ZoneId zoneId) {
        if (field.getValue() == null)
            return null;

        return path.lt(field.getValue().atZone(zoneId).toInstant());
    }

    /**
     * Возвращает предикат с проверкой соответствия поля ввода типа {@link LocalDate} и поля сущности
     * типа {@link Instant}.
     *
     * @param path   поле сущности.
     * @param field  поле ввода.
     * @param zoneId часовая зона для приведения {@link LocalDate} к {@link Instant}.
     * @return предикат.
     */
    public static Predicate eq(DateTimePath<Instant> path, HasValue<LocalDate> field, ZoneId zoneId) {
        if (field.getValue() == null)
            return null;

        LocalDate value = field.getValue();
        return path.between(value.atStartOfDay(zoneId).toInstant(), value.atStartOfDay(zoneId).plusDays(1).toInstant());
    }

    /**
     * Возвращает предикат с проверкой соответствия поля ввода типа {@link String} и поля сущности типа
     * {@link BigDecimal}.
     *
     * @param path   поле сущности.
     * @param field  поле ввода.
     * @param locale локаль для конвертации типа {@link String} в тип {@link BigDecimal}.
     * @return предикат.
     */
    public static Predicate eq(NumberPath<BigDecimal> path, HasValue<String> field, Locale locale) {
        if (field.getValue() == null || field.getValue().equals(""))
            return null;

        BigDecimal result = null;
        try {
            DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(locale);
            format.setParseBigDecimal(true);
            result = BigDecimal.valueOf(format.parse(field.getValue()).doubleValue());
        } catch (NumberFormatException | ParseException e) {
            e.printStackTrace();
        }

        return path.eq(result == null ? BigDecimal.ZERO : result);
    }

    /**
     * Возвращает предикат с проверкой соответствия поля ввода типа {@link String} и поля сущности типа
     * {@link Integer}.
     *
     * @param path   поле сущности.
     * @param field  поле ввода.
     * @return предикат.
     */
    public static Predicate eq(NumberPath<Integer> path, HasValue<String> field) {
        if (field.getValue() == null || field.getValue().equals(""))
            return null;

        int value = Integer.parseInt(field.getValue());

        return path.eq(value);
    }

    /**
     * Возвращает предикат с проверкой соответствия поля ввода со значением типа {@link E} и поля сущности
     * типа {@link Q}.
     *
     * @param path  поле сущности.
     * @param field поле ввода.
     * @param <E>   тип поля ввода.
     * @param <Q>   тип поля сущности.
     * @return предикат.
     */
    public static <E, Q extends EntityPathBase<E>> Predicate eq(Q path, HasValue<E> field) {
        if (field.getValue() == null)
            return null;

        return path.eq(field.getValue());
    }
}

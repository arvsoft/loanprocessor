package ru.loanpro.global;

/**
 * Константы схем БД.
 *
 * @author Oleg Brizhevatikh
 */
public interface DatabaseSchema {

    /**
     * Основная схема с данными о клиентах и займах.
     */
    String CORE = "CORE";

    /**
     * Схема с данными о пользователях, правах и настройках безопасности.
     */
    String SECURITY = "SECURITY";

    /**
     * Схема со справочниками.
     */
    String DIRECTORY = "DIRECTORY";

    /**
     * Схема с данными о клиентах.
     */
    String CLIENT = "CLIENT";
    /**
     * Схема с данными о займах.
     */
    String LOAN = "LOAN";

    /**
     * Схема с операциями
     */
    String OPERATION = "OPERATION";

    /**
     * Схема с данными об отчетах
     */
    String REPORT = "REPORT";

    /**
     * Константы с именами таблиц схемы CORE.
     */
    interface Core {

        /**
         * Таблица с информацией о файлах в файловом хранилище.
         */
        String STORAGE_FILE = "storage_file";

        /**
         * Таблица с шаблонами печати, расширяющая таблицу <b>STORAGE_FILE</b>.
         */
        String PRINT_TEMPLATE_FILE = "print_template_file";
    }

    /**
     * Константы с именами таблиц схемы Client.
     */
    interface Client {

        /**
         * Таблица с документами, распечатанными по клиенту.
         */
        String CLIENTS_PRINTED = "clients_printed";
    }

    /**
     * Константы с именами таблиц схемы Loan.
     */
    interface Loan {

        /**
         * Таблица с документами, распечатанными по займу.
         */
        String LOANS_PRINTED = "loans_printed";

        /**
         * Таблица с документами, распечатанными по платежу.
         */
        String PAYMENT_PRINTED = "payments_printed";
    }

    /**
     * Константы с именами таблиц схемы OPERATION.
     */
    interface Operation {

        /**
         * Таблица с документами, распечатанными по операциям.
         */
        String OPERATION_PRINTED = "operations_printed";
    }

}

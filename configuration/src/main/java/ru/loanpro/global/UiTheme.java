package ru.loanpro.global;

/**
 * Перечисление поддерживаемых стилей интерфейса.
 *
 * @author Oleg Brizhevatikh
 */
public enum UiTheme {
    LIGHT("system.theme.light", "light"),
    DARK("system.theme.dark", "dark");

    private String name;
    private String theme;

    UiTheme(String name, String theme) {
        this.name = name;
        this.theme = theme;
    }

    public String getName() {
        return name;
    }

    public String getTheme() {
        return theme;
    }

    /**
     * Стили вкладок для обработки диспетчером вкладок.
     */
    public enum TabStyle {
        DEFAULT("default"),
        RED("red"),
        PINK("pink"),
        PURPLE("purple"),
        DEEP_PURPLE("deep-purple"),
        INDIGO("indigo"),
        BLUE("blue"),
        LIGHT_BLUE("light-blue"),
        CYAN("cyan"),
        TEAL("teal"),
        GREEN("green"),
        LIGHT_GREEN("light-green"),
        LIME("lime"),
        YELLOW("yellow"),
        AMBER("amber"),
        ORANGE("orange"),
        DEEP_ORANGE("deep-orange"),
        BROWN("brown"),
        GREY("grey"),
        BLUE_GREY("blue-grey");

        private String name;

        TabStyle(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}

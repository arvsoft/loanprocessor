package ru.loanpro.global.repository;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import ru.loanpro.global.domain.AbstractIdEntity;

import javax.annotation.Nullable;
import java.util.UUID;

/**
 * Базовый репозиторий сущностей, наследующихся от {@link AbstractIdEntity}.
 *
 * @param <T> тип сущности репозитория.
 * @author Oleg Brizhevatikh
 */
public interface AbstractIdEntityRepository<T extends AbstractIdEntity>
    extends JpaRepository<T, UUID>, QuerydslPredicateExecutor<T> {

    /**
     * Возвращает страницу с сущностями типа {@link T}, отфильтрованную переданным {@link Predicate}, отсортированную
     * переданным {@link Pageable}.
     *
     * @param predicate предикат фильтрации.
     * @param pageable  параметры фильтрации и разбивки на страницы.
     * @return страница сущностей.
     */
    @Override
    Page<T> findAll(@Nullable Predicate predicate, @Nullable Pageable pageable);
}

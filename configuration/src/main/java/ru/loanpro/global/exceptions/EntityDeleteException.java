package ru.loanpro.global.exceptions;

/**
 * Исключение, возникающее в случае ошибки удаления сущности.
 *
 * @author Oleg Brizhevatikh
 */
public class EntityDeleteException extends Exception {

    /**
     * Создаёт исключение.
     *
     * @param message текст ошибки.
     */
    public EntityDeleteException(String message) {
        super(message);
    }
}

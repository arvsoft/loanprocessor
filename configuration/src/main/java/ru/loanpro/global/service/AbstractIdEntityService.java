package ru.loanpro.global.service;

import com.querydsl.core.types.Predicate;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import ru.loanpro.global.domain.AbstractIdEntity;
import ru.loanpro.global.repository.AbstractIdEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Базовый сервис для работы с наследниками сущности {@link AbstractIdEntity}.
 * Реализует базовые методы для работы со всеми сущностями.
 *
 * @param <T> тип сущности сервиса.
 * @param <R> репозиторий сущности типа {@link T}
 * @author Oleg Brizhevatikh
 */
@NoRepositoryBean
public abstract class AbstractIdEntityService<T extends AbstractIdEntity, R extends AbstractIdEntityRepository<T>> {

    protected final R repository;

    public AbstractIdEntityService(R repository) {
        this.repository = repository;
    }

    /**
     * Возвращает {@link Stream} отсортированных и разбитых на страницы сущностей типа {@link T}, полученных
     * с помощью стандартного метода {@link QuerydslPredicateExecutor#findAll(Predicate, Sort)}.
     *
     * @param predicate предикат.
     * @param sortOrder список полей с порядком сортировки.
     * @param offset    смещение страниц.
     * @param limit     размер страницы.
     * @return страница отфильтрованных и отсортированных сущностей со сдвигом.
     */
    public Stream<T> findAll(Predicate predicate,
                             List<QuerySortOrder> sortOrder, int offset, int limit) {

        return findAllByMethod(predicate, sortOrder, offset, limit,
            repository::findAll);
    }

    /**
     * Возвращает {@link Stream} отсортированных и разбитых на страницы сущностей типа {@link T}, полученных
     * с помощью переданного метода репозитория.
     *
     * @param predicate        предикат.
     * @param sortOrder        список полей с порядком сортировки.
     * @param offset           смещение страниц.
     * @param limit            размер страницы.
     * @param repositoryMethod метод репозитория, из которого необходимо запросить данные.
     * @return страница отфильтрованных и отсортированных сущностей со сдвигом.
     */
    public Stream<T> findAllByMethod(Predicate predicate, List<QuerySortOrder> sortOrder,
                                     int offset, int limit,
                                     BiFunction<Predicate, Pageable, Page<T>> repositoryMethod) {

        final int startPage = (int) Math.floor((double) offset / limit);
        final int endPage = (int) Math.floor((double) (offset + limit - 1) / limit);

        Logger.getLogger(this.getClass().getName()).info(
            String.format("offset: %s, pageSize: %s, startPage: %s endPage: %s",
                offset, limit, startPage, endPage));

        final Sort.Direction sortDirection = sortOrder.isEmpty()
            || sortOrder.get(0).getDirection() == SortDirection.ASCENDING
            ? Sort.Direction.ASC
            : Sort.Direction.DESC;

        final String sortProperty = sortOrder.isEmpty() ? "id" : sortOrder.get(0).getSorted();
        if (startPage != endPage) {
            List<T> page0 = repositoryMethod.apply(predicate,
                PageRequest.of(startPage, limit, sortDirection, sortProperty)).stream()
                .collect(Collectors.toList());

            page0 = page0.subList(offset % limit, page0.size());
            List<T> page1 = repositoryMethod.apply(predicate,
                PageRequest.of(endPage, limit, sortDirection, sortProperty)).stream()
                .collect(Collectors.toList());

            page1 = page1.subList(0, limit - page0.size());
            List<T> result = new ArrayList<>(page0);

            result.addAll(page1);

            return result.stream();
        } else {
            return repositoryMethod.apply(predicate, PageRequest.of(startPage, limit, sortDirection, sortProperty))
                .stream();
        }
    }

    /**
     * Возвращает количество сущностей в базе данных, соответствующих спецификации.
     * Значение получает из базы данных с помощью стандартного метода
     * {@link QuerydslPredicateExecutor#count(Predicate)}.
     *
     * @param predicate предикат.
     * @return количество сущностей.
     */
    public Integer count(Predicate predicate) {

        return countByMethod(predicate, repository::count);
    }

    /**
     * Возвращает количество сущностей в базе данных, соответствующих спецификации.
     * Значение получает из базы данных с помощью переданного метода репозитория.
     *
     * @param predicate        предикат.
     * @param repositoryMethod метод репозитория.
     * @return количество сущностей.
     */
    public Integer countByMethod(Predicate predicate,
                                 Function<Predicate, Long> repositoryMethod) {

        return Math.toIntExact(repositoryMethod.apply(predicate));
    }
}

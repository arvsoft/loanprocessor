package ru.loanpro.global.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация диспетчера безопасности. Изменяет видимость Vaadin-компонента в
 * зависимости от наличия у пользователя права на использования компонента.
 * Устанавливается на поля объекта пользовательского интерфейса.
 *
 * @author Oleg Brizhevatikh
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface Visible {

    /**
     * Данное значение содержит имя права на использование компонента.
     *
     * @return Имя права.
     */
    String value();
}

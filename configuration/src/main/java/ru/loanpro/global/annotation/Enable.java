package ru.loanpro.global.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация диспетчера безопасности. Изменяет доступность Vaadin-компонента в
 * зависимости от наличия у пользователя права использования компонента.
 * Устанавливается на поля объекта пользовательского интерфейса.
 *
 * @author Oleg Brizhevatikh
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface Enable {

    /**
     * Данное значение содержит имя права на использование компонента.
     *
     * @return Имя права.
     */
    String value();
}

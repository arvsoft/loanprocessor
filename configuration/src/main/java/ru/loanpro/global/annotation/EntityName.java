package ru.loanpro.global.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация логгера. Определяет имя сущности отображаемое в интерфейсе.
 *
 * @author Oleg Brizhevatikh
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface EntityName {
    String value();
}

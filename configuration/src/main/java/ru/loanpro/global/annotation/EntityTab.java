package ru.loanpro.global.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация диспетчера вкладок. Устанавливается на вкладку, являющуюся отображением
 * сущности в интерфейсе.
 *
 * @author Oleg Brizhevatikh
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface EntityTab {
    Class value();
}

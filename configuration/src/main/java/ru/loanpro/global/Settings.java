package ru.loanpro.global;

/**
 * Коллекция ключей настроек системы.
 *
 * @author Oleg Brizhevatikh
 */
public interface Settings {
    String LOCALE = "settings.locale";
    String THEME = "settings.theme";
    String DATE_FORMAT = "settings.dateFormat";
    String CURRENCIES = "settings.currencies";
    String TIME_ZONE = "settings.timeZone";
    String DEPARTMENT_ACCOUNT = "settings.departmentAccount";
    String GRID_STYLE = "settings.gridStyle";

    String INFO_NAME = "settings.info.name";
    String INFO_FULL_NAME = "settings.info.fullName";
    String INFO_REG_NUMBER_1 = "settings.info.regNumber1";
    String INFO_REG_NUMBER_2 = "settings.info.regNumber2";
    String INFO_REG_NUMBER_3 = "settings.info.regNumber3";
    String INFO_REG_NUMBER_4 = "settings.info.regNumber4";
    String INFO_REG_NUMBER_5 = "settings.info.regNumber5";
    String INFO_REG_NUMBER_6 = "settings.info.regNumber6";
    String INFO_PHONES = "settings.info.phones";
    String INFO_JURIDICAL_ADDRESS = "settings.info.juridicalAddress";
    String INFO_ACTUAL_ADDRESS = "settings.info.actualAddress";
    String INFO_EMAIL = "settings.info.email";
    String INFO_BANK_ACCOUNT_1 = "settings.info.bankAccount1";
    String INFO_BANK_ACCOUNT_2 = "settings.info.bankAccount2";
    String INFO_DIRECTOR = "settings.info.director";
    String INFO_DIRECTOR_NAME = "settings.info.directorName";
    String INFO_ASSISTANT_1 = "settings.info.assistant1";
    String INFO_ASSISTANT_NAME_1 = "settings.info.assistantName1";
    String INFO_ASSISTANT_2 = "settings.info.assistant2";
    String INFO_ASSISTANT_NAME_2 = "settings.info.assistantName2";

    String CURRENCY_DEFAULT="settings.financy.currencyDefault";
    String RECALCULATION_TYPE="settings.financy.recalculationType";
    String OVERDUE_DAYS_CALCULATION_TYPE="settings.financy.overdueDaysCalculationType";
    String REPAYMENT_ORDER_SCHEME="settings.financy.repaymentOrderScheme";
    String SUMMATION_PENALTY = "settings.financy.summationPenalty";
    String SUMMATION_FINE = "settings.financy.summationFine";

    String AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED="settings.financy.autoOperationIssueMoneyLoanIssued";
    String AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT="settings.financy.autoOperationReplenishMoneyLoanPayment";
    String PAYMENT_SPLIT_TYPE="settings.financy.paymentSplitType";

    String AUTO_OPERATION_ISSUED_MONEY_LOAN_ISSUED_PROCESSED="settings.financy.autoOperationIssueMoneyLoanIssuedProcessed";
    String AUTO_OPERATION_REPLENISH_MONEY_LOAN_PAYMENT_PROCESSED="settings.financy.autoOperationReplenishMoneyLoanPaymentProcessed";

    String KEEP_SAME_OPERATION_DATE_WHEN_TURN_FROM_PREPARED_TO_PROCESSED="settings.financy.keepSameOperationDateWhenSaveFromPreparedToProcessed";
}

package ru.loanpro.global.directory;

public enum EstateType {
    FLAT("directory.enum.EstateType.flat"),
    HOUSE("directory.enum.EstateType.house"),
    LOFT("directory.enum.EstateType.loft"),
    STEAD("directory.enum.EstateType.stead"),
    GARAGE("directory.enum.EstateType.garage"),
    OTHER("directory.enum.EstateType.other");

    private String name;

    EstateType(String name) { this.name = name;}

    public String getName(){ return name;}
}

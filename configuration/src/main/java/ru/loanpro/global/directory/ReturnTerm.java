package ru.loanpro.global.directory;

public enum ReturnTerm {
    WEEKLY("directory.enum.ReturnTerm.weekly"),
    MONTHLY("directory.enum.ReturnTerm.monthly"),
    IN_THE_END_OF_TERM("directory.enum.ReturnTerm.in_the_end_of_term"),
    INDIVIDUALLY("directory.enum.ReturnTerm.individually");

    private String name;

    ReturnTerm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

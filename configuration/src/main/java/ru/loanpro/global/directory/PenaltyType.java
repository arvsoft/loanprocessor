package ru.loanpro.global.directory;

public enum PenaltyType {
    NOT_DEFINED("directory.enum.PenaltyType.not_defined"),
    FROM_LOAN_VALUE("directory.enum.PenaltyType.from_loan_value"),
    FROM_PAYMENT("directory.enum.PenaltyType.from_payment"),
    FROM_BALANCE("directory.enum.PenaltyType.from_balance"),
    FROM_SUM_OF_INDEBTEDNESS("directory.enum.PenaltyType.from_sum_of_indebtedness");

    private String name;

    PenaltyType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

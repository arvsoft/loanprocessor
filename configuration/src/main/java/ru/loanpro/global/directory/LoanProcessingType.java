package ru.loanpro.global.directory;

public enum LoanProcessingType {
    LOAN_ISSUING("directory.enum.LoanProcessingType.loan_issuing"),
    LOAN_PAYMENT("directory.enum.LoanProcessingType.loan payment"),
    NON_LOAN_OPERATIONS("directory.enum.LoanProcessingType.non_loan_operations");

    private String name;

    LoanProcessingType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

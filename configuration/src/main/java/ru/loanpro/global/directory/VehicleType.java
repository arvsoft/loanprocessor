package ru.loanpro.global.directory;

public enum VehicleType {
    CAR("directory.enum.VehicleType.car"),
    TRUCK("directory.enum.VehicleType.truck"),
    FREIGHT_CAR("directory.enum.VehicleType.freight_car"),
    BUS("directory.enum.VehicleType.bus"),
    TRAILER("directory.enum.VehicleType.trailer"),
    TRACTOR("directory.enum.VehicleType.tractor"),
    MOTOCYCLE("directory.enum.VehicleType.motorcycle"),
    ATV("directory.enum.VehicleType.atv"),
    SNOWMOBILE("directory.enum.VehicleType.snowmobile"),
    BIKE("directory.enum.VehicleType.bike"),
    SPECIAL_VEHICLE("directory.enum.VehicleType.special_vehicle"),
    YACHT("directory.enum.VehicleType.yacht"),
    OTHER("directory.enum.VehicleType.other");

    private String name;

    VehicleType(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }
}

package ru.loanpro.global.directory;

/**
 * Enum, содержащий статусы использования адресов физическими лицами
 *
 * @author Maksim Askhaev
 */
public enum AddressUseStatus {
    LIVING("directory.enum.AddressUseStatus.living"),
    REGISTRATION("directory.enum.AddressUseStatus.registration"),
    LIVING_AND_REGISTRATION("directory.enum.AddressUseStatus.living_and_registration"),
    ADDITIONAL("directory.enum.AddressUseStatus.additional"),
    EXPIRED("directory.enum.AddressUseStatus.expired");

    private String name;

    AddressUseStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}

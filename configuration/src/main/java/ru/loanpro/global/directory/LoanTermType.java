package ru.loanpro.global.directory;

public enum LoanTermType {
    DAYS("directory.enum.LoanTermType.days"),
    WEEKS("directory.enum.LoanTermType.weeks"),
    MONTHS("directory.enum.LoanTermType.months");

    private String name;

    LoanTermType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

package ru.loanpro.global.directory;

public enum LoanStatus {
    OPENED_NO_OVERDUE("directory.enum.LoanStatus.opened_no_overdue", "loan-status-color-opened-no-overdue"),
    OPENED_WERE_OVERDUES("directory.enum.LoanStatus.opened_were_overdues", "loan-status-color-opened-were-overdues"),
    OPENED_CURRENT_OVERDUE("directory.enum.LoanStatus.opened_current_overdue", "loan-status-color-opened-current-overdue"),
    CLOSED_NO_OVERDUE("directory.enum.LoanStatus.closed_no_overdue", "loan-status-color-closed-no-overdue"),
    CLOSED_WERE_OVERDUES("directory.enum.LoanStatus.closed_were_overdues", "loan-status-color-closed-were-overdues"),
    CLOSED_ANNULED("directory.enum.LoanStatus.closed_annulled", "loan-status-color-closed-annuled"),
    CLOSED_FROZEN("directory.enum.LoanStatus.closed_frozen", "loan-status-color-closed-frozen");

    private String name;
    private String color;

    LoanStatus(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}

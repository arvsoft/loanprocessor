package ru.loanpro.global.directory;

/**
 * Enum, содержащий информацию о том, проводить ли платеж по займу единой операцией
 * или разделять составляющие платежа (осн. часть, проценты, пени, штраф)
 * на отдельные операции по кассе
 *
 * @author Maksim Askhaev
 */
public enum PaymentSplitType {
    ONE_TRANSACTION("directory.enum.PaymentSplitType.one_transaction"),
    TWO_TRANSACTION1("directory.enum.PaymentSplitType.two_transactions1"),
    TWO_TRANSACTION2("directory.enum.PaymentSplitType.two_transactions2"),
    THREE_TRANSACTION("directory.enum.PaymentSplitType.three_transactions"),
    FOUR_TRANSACTION("directory.enum.PaymentSplitType.four_transactions");

    private String name;

    PaymentSplitType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

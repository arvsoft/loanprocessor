package ru.loanpro.global.directory;

public enum RepaymentOrderScheme {
    ORDER1("directory.enum.RepaymentOrderScheme.order1"),
    ORDER2("directory.enum.RepaymentOrderScheme.order2");

    private String name;

    RepaymentOrderScheme(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

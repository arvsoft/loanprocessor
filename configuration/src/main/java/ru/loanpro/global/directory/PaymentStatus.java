package ru.loanpro.global.directory;

public enum PaymentStatus {
    NOT_PAID("directory.enum.PaymentStatus.not_paid"),
    PAYMENT_BY_SCHEDULE_FULL("directory.enum.PaymentStatus.payment_by_schedule_full"),
    PAYMENT_BY_SCHEDULE_OVERDUE("directory.enum.PaymentStatus.payment_by_schedule_overdue"),
    PAYMENT_BY_SCHEDULE_SURCHARGE("directory.enum.PaymentStatus.payment_by_schedule_surcharge"),
    PAYMENT_ADDITIONAL("directory.enum.PaymentStatus.payment_additional"),
    PAYMENT_BY_SCHEDULE_MANUAL("directory.enum.PaymentStatus.payment_by_schedule_manual"),
    PAYMENT_EARLY_CLOSING("directory.enum.PaymentStatus.payment_early_closing"),
    PAYMENT_FROZEN("directory.enum.PaymentStatus.payment_frozen");

    private String name;

    PaymentStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

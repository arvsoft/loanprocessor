package ru.loanpro.global.directory;

/**
 * Enum, содержащий типы пересчета графика платежей при пролонгации
 *
 * @author Maksim Askhaev
 */
public enum RecalculationType {
    RECALCULATION_ON_NEW_TERM("directory.enum.RecalculationType.recalculate_on_new_term"),
    REDUCE_REPAYMENTS_REMAIN_TERM("directory.enum.RecalculationType.reduce_repayments_and_remain_term"),
    REDUCE_TERM_REMAIN_REPAYMENTS("directory.enum.RecalculationType.reduce_term_and_remain_repayments");

    private String name;

    RecalculationType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

package ru.loanpro.global.directory;

public enum TransferMethod {
    CASH_WITHDRAWAL("directory.enum.TransferMethod.cash_withdrawal"),
    CASH_REPLENISHMENT("directory.enum.TransferMethod.cash_replenishment"),
    TRANSFER_INNER("directory.enum.TransferMethod.transfer_between_inner_accounts"),
    TRANSFER_TO_EXTERNAL("directory.enum.TransferMethod.transfer_to_external_account"),
    TRANSFER_FROM_EXTERNAL("directory.enum.TransferMethod.transfer_from_external_account");

    private String name;

    TransferMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

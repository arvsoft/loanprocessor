package ru.loanpro.global.directory;

public enum ScheduleStatus {
    NOT_PAID_NO_OVERDUE("directory.enum.ScheduleStatus.not_paid_no_overdue"),
    PAID_PARTIALLY_PENALTY_DEBT("directory.enum.ScheduleStatus.paid_partially_penalty_debt"),
    PAID_PARTIALLY_FINE_DEBT("directory.enum.ScheduleStatus.paid_partially_fine_debt"),
    PAID_PARTIALLY_INTEREST_DEBT("directory.enum.ScheduleStatus.paid_partially_interest_debt"),
    PAID_PARTIALLY_PRINCIPAL_DEBT("directory.enum.ScheduleStatus.paid_partially_principal_debt"),
    NOT_PAID_OVERDUE("directory.enum.ScheduleStatus.not_paid_overdue"),
    PAID_FULLY("directory.enum.ScheduleStatus.paid_fully"),
    EARLY_CLOSED("directory.enum.ScheduleStatus.early_closed");

    private String name;

    ScheduleStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

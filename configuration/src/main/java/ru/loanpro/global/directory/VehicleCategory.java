package ru.loanpro.global.directory;

public enum VehicleCategory {

    A("directory.enum.VehicleCategory.A"),
    B("directory.enum.VehicleCategory.B"),
    C("directory.enum.VehicleCategory.C"),
    D("directory.enum.VehicleCategory.D"),
    TRAIL("directory.enum.VehicleCategory.TRAIL"),
    OTHER("directory.enum.VehicleCategory.OTHER");

    private String name;

    VehicleCategory(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }

}

package ru.loanpro.global.directory;

/**
 * Enum, содержащий группы отчетов
 *
 * @author Maksim Askhaev
 */
public enum ReportGroup {
    CLIENTS("directory.enum.ReportGroup.clients"),
    LOANS("directory.enum.ReportGroup.loans"),
    REQUESTS("directory.enum.ReportGroup.requests"),
    PAYMENTS("directory.enum.ReportGroup.payments"),
    OPERATIONS("directory.enum.ReportGroup.operations"),
    OTHERS("directory.enum.ReportGroup.others");

    String name;

    ReportGroup(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

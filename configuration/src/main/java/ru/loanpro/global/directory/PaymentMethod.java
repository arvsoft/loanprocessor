package ru.loanpro.global.directory;

public enum PaymentMethod {
    CASH_REPLENISH("directory.enum.PaymentMethod.cashReplenish"),
    CASH_ISSUE("directory.enum.PaymentMethod.cashIssue"),
    CASHLESS("directory.enum.PaymentMethod.cashless");

    private String name;

    PaymentMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

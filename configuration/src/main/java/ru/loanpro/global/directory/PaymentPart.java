package ru.loanpro.global.directory;

/**
 * Enum, содержащий перечень составляющих платежа по займу
 *
 * @author Maksim Askhaev
 */
public enum PaymentPart {
    PAYMENT_PRINCIPAL("directory.enum.payment_part.principal"),
    PAYMENT_INTEREST("directory.enum.payment_part.interest"),
    PAYMENT_PENALTY("directory.enum.payment_part.penalty"),
    PAYMENT_FINE("directory.enum.payment_part.fine"),
    PAYMENT_FEE("directory.enum.payment_part.fee");

    private String name;

    PaymentPart(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

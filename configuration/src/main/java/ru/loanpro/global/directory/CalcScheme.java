package ru.loanpro.global.directory;

public enum CalcScheme {
    ANNUITY("directory.enum.CalcScheme.annunity"),
    DIFFERENTIAL("directory.enum.CalcScheme.differential");

    private String name;

    CalcScheme(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

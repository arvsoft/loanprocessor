package ru.loanpro.global.directory;

/**
 * Enum, содержащий типы счетов
 *
 * @author Maksim Askhaev
 */
public enum AccountType {
    INNER_ACCOUNT("directory.enum.AccountType.inner_account"),
    EXTERNAL_ACCOUNT("directory.enum.AccountType.external_account");

    private String name;

    AccountType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
package ru.loanpro.global.directory;

/**
 * Enum, содержащий стили для грида
 *
 * @author Maksim Askhaev
 */
public enum GridStyle {
    NORMAL("gridStyleEnum.normal"),
    SMALL("gridStyleEnum.small");

    private String name;

    GridStyle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

package ru.loanpro.global.directory;

public enum PayerType {
    DEPARTMENT("directory.enum.PayerType.department"),
    CLIENT("directory.enum.PayerType.client");

    private String name;

    PayerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

package ru.loanpro.global.directory;

public enum ClientRole {
    BORROWER("directory.enum.ClientRole.borrower"),
    CO_BORROWER("directory.enum.ClientRole.co_borrower"),
    GUARANTOR("directory.enum.ClientRole.guarantor");

    private String name;

    ClientRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

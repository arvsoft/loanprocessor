package ru.loanpro.global.directory;

public enum  OperationStatus {
    PREPARED("directory.enum.MoneyOperationStatus.prepared", "operation-status-prepared-color"),
    PROCESSED("directory.enum.MoneyOperationStatus.processed", "operation-status-processed-color");

    private String name;
    private String color;

    OperationStatus(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }
    public String getColor() {
        return color;
    }
}

package ru.loanpro.global.directory;

public enum RequestStatus {
    NEW("directory.enum.RequestStatus.new", "request-status-new-color"),
    APPROVED("directory.enum.RequestStatus.approved", "request-status-approved-color"),
    DECLINED("directory.enum.RequestStatus.declined", "request-status-declined-color"),
    FROZEN("directory.enum.RequestStatus.frozen", "request-status-frozen-color"),
    LOAN_ISSUED("directory.enum.RequestStatus.loan_issued", "request-status-issued-color");

    private String name;
    private String color;

    RequestStatus(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}

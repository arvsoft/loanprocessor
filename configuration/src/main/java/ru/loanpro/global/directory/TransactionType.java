package ru.loanpro.global.directory;

public enum TransactionType {
    LOAN_ISSUE("directory.enum.TransactionType.loan_issue"),
    LOAN_PAYMENT("directory.enum.TransactionType.loan_payment"),
    OTHER_TRANSACTION("directory.enum.TransactionType.other_operation");

    private String name;

    TransactionType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

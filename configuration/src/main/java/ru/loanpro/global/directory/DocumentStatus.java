package ru.loanpro.global.directory;

/**
 * Enum, содержащий статусы актуальности документов
 *
 * @author Maksim Askhaev
 */
public enum DocumentStatus {
    ACTUAL("directory.enum.DocumentStatus.actual"),
    EXPIRED("directory.enum.DocumentStatus.expired"),
    LOST("directory.enum.DocumentStatus.lost");

    private String name;

    DocumentStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

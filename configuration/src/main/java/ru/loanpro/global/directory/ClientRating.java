package ru.loanpro.global.directory;

public enum ClientRating {
    NEW("directory.enum.ClientRating.new", "client-rating-new-color"),
    DECLINED("directory.enum.ClientRating.declined", "client-rating-declined-color"),
    APPROVED("directory.enum.ClientRating.approved", "client-rating-approved-color"),
    BAD("directory.enum.ClientRating.bad", "client-rating-bad-color"),
    GOOD("directory.enum.ClientRating.good", "client-rating-good-color"),
    EXCELLENT("directory.enum.ClientRating.excellent", "client-rating-excellent-color");

    private String name;
    private String color;

    ClientRating(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}

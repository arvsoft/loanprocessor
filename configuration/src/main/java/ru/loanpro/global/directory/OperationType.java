package ru.loanpro.global.directory;

public enum OperationType {
    INCOME("directory.enum.MoneyOperationType.income"),
    EXPENSE("directory.enum.MoneyOperationType.expense");

    private String name;

    OperationType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

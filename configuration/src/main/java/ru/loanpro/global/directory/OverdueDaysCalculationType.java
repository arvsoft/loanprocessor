package ru.loanpro.global.directory;

public enum OverdueDaysCalculationType {
    FROM_DATE_ACCORDING_SCHEDULE("directory.enum.OverdueDaysCalculationType.schedule"),
    FROM_DATE_OF_LAST_PAYMENT("directory.enum.OverdueDaysCalculationType.lastpayment");

    private String name;

    OverdueDaysCalculationType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

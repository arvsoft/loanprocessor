package ru.loanpro.global.directory;

public enum InterestType {
    PERCENT_A_DAY("directory.enum.InterestType.percent_a_day", "directory.enum.InterestType.l.percent_a_day"),
    PERCENT_A_WEEK("directory.enum.InterestType.percent_a_week", "directory.enum.InterestType.l.percent_a_week"),
    PERCENT_A_MONTH("directory.enum.InterestType.percent_a_month", "directory.enum.InterestType.l.percent_a_month"),
    PERCENT_A_YEAR("directory.enum.InterestType.percent_a_year", "directory.enum.InterestType.l.percent_a_year"),
    PERCENT_A_WHOLE_PERIOD("directory.enum.InterestType.percent_a_whole_period", "directory.enum.InterestType.l.percent_a_whole_period");

    private String name;
    private String name2;

    InterestType(String name, String name2) {
        this.name = name;
        this.name2 = name2;
    }

    public String getName() {
        return name;
    }
    public String getName2() {
        return name2;
    }
}

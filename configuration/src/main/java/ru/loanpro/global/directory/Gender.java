package ru.loanpro.global.directory;

public enum Gender {
    MALE("directory.enum.Gender.male"),
    FEMALE("directory.enum.Gender.female"),
    OTHER("directory.enum.Gender.other");

    private String name;

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

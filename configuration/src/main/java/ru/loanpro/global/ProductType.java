package ru.loanpro.global;

/**
 * Тип продукта для активации.
 *
 * @author Oleg Brizhevatikh
 */
public enum ProductType {
    LOAN_PROCESSOR_360("loanprocessor360");

    /**
     * Возвращает имя продукта для url-строки сервера.
     */
    private String productName;

    ProductType(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }
}
